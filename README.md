# BOBJ Frontend Project

## Tech Stack

-   ReactJS (with TypeScript)
-   SCSS
-   Apollo Provider
-   React-Router
-   Lodash
-   Classnames
-   ...

# Complex forms flow

## Complex form structure

test

```typescript jsx
    ...
    // state should an object that holds all states of included forms
    // interface state {
    //     simpleForm1: ISimpleForm1State,
    //     simpleForm2: ISimpleForm2State,
    //     ...
    // }
    const [state, dispatch] = useReducer(ComplexFormReducer, complexFormDefaultState);
    ...
    // submit is preforming with useSubmit hook.
    const { startSubmit, stopSubmit } = useSubmit(onSelfSubmit);
    ...
    return (
        ...
            <SimpleForm1
                // pass initial values from state
                initialValues={state.simpleForm1}
                // pass onSubmit and saveAndContinue (if necessary) methods to simple form base
                onSubmit={onSimpleForm1Submit}
                saveAndContinue={onSimpleFormSaveAndContinue}
            />
        ...
    )
```

## Simple form structure

```typescript jsx
    // Yup validation schema according to ISimpleFormState
    const SimpleFormSchema = Yup.object().shape({...});

    // simple form state which shape the form values
    export interface ISimpleFormState {
        value1: string;
        ...
    }

    // form base is actual form that displays inputs and connects them to formik
    // props interface should always extend FormikProps<ISimpleFormState>
    interface ISimpleFormBaseProps extends FormikProps<ISimpleFormState>, IFormFooterWithSaveProps(if necessary) {}

    const SimpleFormBase: FunctionComponent<ISimpleFormBaseProps> = ({saveAndContinue, ...formikProps}) => {
        return (
            ...
        )
    }

    // formik wrapper around form base that gets actually exported
    // props interface should always extend IFormWithFormik<ISimpleFormState>
    interface IBuildingSearchProps extends IFormWithFormik<ISimpleFormState>, IFormFooterWithSaveProps (if necessary){}

    export const BuildingSearch: FunctionComponent<IBuildingSearchProps> = ({ initialValues, onSubmit, saveAndContinue }) => {
        return (
            <Formik
                initialValues={initialValues}
                onSubmit={onSubmit}
                validationSchema={SimpleFormSchema}
                render={(props) => <SimpleFormBase saveAndContinue={saveAndContinue} {...props} />}
            />
        );
    };
```

## Simple form with dynamicInfoRows

```typescript jsx
    // to use DynamicInfoRows following changes are required

    // interface ISimpleFormState should extend IDynamicInfoRowsState, which brings rows property to the state
    export interface ISimpleFormState extends IDynamicInfoRowsState{...}
    ...
    // DynamicsInfoRows get all formikProps as props
    const SimpleFormBase: FunctionComponent<ISimpleFormBaseProps> = ({saveAndContinue, ...formikProps}) => {
        return (
            ...
            // type prop tells which metafields to display
            <DynamicInfoRows {...formikProps} type="building" />
            ...
        )
    }
```

## Simple form with multiple dynamicInfoRows

```typescript jsx
    // if you need to place multiple dynamicInfoRows in one form following changes are required
    // interface ISimpleFormState should not extend IDynamicInfoRowsState,
    // instead it should contain rows field with the following structure
    export interface ISimpleFormState {
        rows: {
            ...
            rowOneSuffix:  IInfoRowParams[];
            rowTwoSuffix:  IInfoRowParams[];
            ...
        }
    }
    ...
    const SimpleFormBase: FunctionComponent<ISimpleFormBaseProps> = ({saveAndContinue, ...formikProps}) => {
        return (
            ...
            // in addition to formikProps and type you should pass suffix props corresponding to field inside rows.
            <DynamicInfoRows {...formikProps} type="building" suffix=".rowOneSuffix" />
            <DynamicInfoRows {...formikProps} type="building" suffix=".rowTwoSuffix" />
            ...
        )
    }
```
