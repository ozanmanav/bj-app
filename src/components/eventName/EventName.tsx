import React, { FunctionComponent } from "react";
import { Icon } from "../ui/icons";
import "./EventName.scss";
import { Link } from "react-router-dom";

interface IEventNameProps {
    name: string;
    type: string;
    id?: string;
    typeLabel?: string;
}

export const EventName: FunctionComponent<IEventNameProps> = ({ name, type, id }) => {
    const nameElClassName = `_font-bold ${type === "company" ? "_text-primary" : "_text-black"} b-event-name__name`;

    return (
        <Link className="flex" to={`/app/event/${id}`}>
            <Icon icon={type} className="b-event-name__type-icon _small" />
            <div className="flex flex-column justify-center">
                <p className={nameElClassName} title={name}>
                    {name}
                </p>
            </div>
        </Link>
    );
};
