import React, { FunctionComponent } from "react";
import { useRoleCheck } from "../../hooks";
import { TRole, TUserType } from "../../config/types";

interface IUserRoleCheckProps {
    availableForRoles?: TRole[];
    availableForAdminRoles?: TUserType[];
    skipCheck?: boolean;
}

export const UserRoleCheck: FunctionComponent<IUserRoleCheckProps> = ({
    availableForRoles,
    availableForAdminRoles,
    skipCheck,
    children,
}) => {
    const hasAccess = useRoleCheck(availableForRoles, availableForAdminRoles);

    if (!hasAccess && !skipCheck) {
        return null;
    }

    return <>{children}</>;
};
