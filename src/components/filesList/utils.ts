import { TFile } from "../../config/types";
import { showErrorToast } from "../ui/toasts";
import ApolloClient from "apollo-client";
import gql from "graphql-tag";
import get from "lodash.get";

export function groupFilesByName(array: TFile[] = []) {
    return Object.values(
        array.reduce(
            (acc, item) => ({
                ...acc,
                [item.name]: [...(acc[item.name] || []), item],
            }),
            {} as { [key: string]: TFile[] }
        )
    );
}

const FILES = gql`
    query Files($id: String!) {
        files(id: $id) {
            data {
                id
                type {
                    id
                }
                name
                url
                filename
                extension
                size
                created_at
                updated_at
            }
        }
        details(model: OBJECT, model_id: $id) {
            data {
                name
                value
                type
            }
        }
    }
`;

function fetchFiles(ids: string[], apolloClient: ApolloClient<any>) {
    return Promise.all(
        ids.map((id) =>
            apolloClient.query({
                query: FILES,
                variables: {
                    id,
                },
            })
        )
    );
}

export async function filesChangeCallback(ids: string[], setFieldValue: Function, apolloClient: ApolloClient<any>) {
    // TODO: is it a a best way?
    try {
        const res = await fetchFiles(ids, apolloClient);

        const uploadedFilesArray = res.map((item) => get(item, "data.files.data[0]")).filter(Boolean);

        setFieldValue("files", uploadedFilesArray);
    } catch (e) {
        showErrorToast(e.message);
    }
}

export function getFileDetails(files: TFile[]): { [key: string]: string } {
    const sourceDate = get(files[0], "details[0].meta.source_date");

    return {
        group_updated_at: new Date(get(files[0], "group_updated_at")).toLocaleDateString(),
        group_created_at: new Date(get(files[0], "group_created_at")).toLocaleDateString(),
        author: get(files[0], "details[0].meta.author.name"),
        modified_by: get(files[0], "details[0].meta.last_modification.modifier.name"),
        value: get(files[0], "details[0].value"),
        type: get(files[0], "type.name"),
        context: get(files[0], "details[0].context"),
        source: get(files[0], "details[0].meta.source"),
        source_url: get(files[0], "details[0].meta.source_url"),
        source_date: sourceDate ? new Date(sourceDate).toLocaleDateString() : "",
        verification: get(files[0], "details[0].meta.verification") ? "YES" : "",
        verification_by: get(files[0], "details[0].meta.verification_by"),
    };
}

const EXTENTIONS_TO_PREVIEW = ["jpg", "jpeg", "png", "svg", "gif", "bmp", "pdf", "txt", "md"];

export function shouldPreviewFile(extension: string): boolean {
    return EXTENTIONS_TO_PREVIEW.includes(extension.toLowerCase());
}
