import { ICheckboxDropdownItemState } from "../../components/ui/checkboxDropdown";
import { TRole } from "../../config/types";

export const FILE_LIST_DEFAULT_COLUMNS: ICheckboxDropdownItemState[] = [
    {
        name: "type",
        label: "Type",
        sortable: false,
        value: true,
    },
    {
        name: "value",
        label: "Value",
        sortable: false,
        value: false,
    },
    {
        name: "context",
        label: "Context",
        sortable: false,
        value: false,
    },
    {
        name: "source",
        label: "Source",
        sortable: false,
        value: false,
    },
    {
        name: "source_url",
        label: "Source url",
        sortable: false,
        value: false,
    },
    {
        name: "source_date",
        label: "Source Date",
        sortable: false,
        value: false,
    },
    {
        name: "verification",
        label: "Verification",
        sortable: false,
        value: false,
    },
    {
        name: "verification_by",
        label: "Verified by",
        sortable: false,
        value: false,
    },
    {
        name: "group_created_at",
        label: "Date Created",
        sortable: true,
        value: false,
    },
    {
        name: "author",
        label: "Author",
        sortable: false,
        value: true,
    },
    {
        name: "group_updated_at",
        label: "Date Modified",
        sortable: true,
        value: false,
    },
    {
        name: "modified_by",
        label: "Modified by",
        sortable: false,
        value: false,
    },
];

export const EDIT_AVAILABLE_FOR_ROLES: TRole[] = [
    "owner_administrator",
    "manufacturer_administrator",
    "manufacturer_brand_manager",
    "manufacturer_object_manager",
    "property_manager_administrator",
    "property_manager_building_manager",
    "service_provider_administrator",
    "service_provider_service_manager",
];
