import React, { FunctionComponent, HTMLAttributes, useState } from "react";
import { RouteComponentProps, withRouter } from "react-router";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";
import get from "lodash.get";
import orderBy from "lodash.orderby";

import "./FilesList.scss";

import { FILE_LIST_DEFAULT_COLUMNS } from "./config";

import { TFile } from "../../config/types";

import { getFileTypesOptions } from "../../utils";
import { groupFilesByName } from "./utils";

import { useSortingTable } from "../../hooks";

import { useDynamicTableColumns } from "../../hooks/useDynamicTableColumns";

import { Loader } from "../ui/loader";
import { SortButton } from "../ui/buttons";

import { PlusButton, FilterDropdown, CheckboxDropdown, useModal } from "../ui/";
import { TFilterDropDownOption } from "../ui/filter/FilterDropdown";

import { AddFileModal } from "./addFileModal";
import FileListItem from "./FilesListItem";

import { UserRoleCheck } from "../userRoleCheck";

type TFilesListVariants = "object" | "newContract" | "contract" | "organization";

interface IFilesListProps extends HTMLAttributes<HTMLDivElement>, RouteComponentProps<{ id: string }> {
    files?: TFile[] | any[]; // TODO: update type after every files list will be implemented
    variant?: TFilesListVariants;
    addFilesTo?: string;
    filesChangeCallback?: Function;
    filesDeleteCallback?: Function;
    parentID?: string;
    objectID?: string;
    organizationID?: string;
    hideFilter?: boolean;
    label?: string;
    loading?: boolean;
    readOnly?: boolean;
    fromSearch?: boolean;
    eventRefetch?: Function;
}

const FILE_TYPES = gql`
    query GetFileTypes {
        entity_types(is_file: true) {
            data {
                id
                name
                type
                groups {
                    name
                }
            }
        }
    }
`;

const FilesListBase: FunctionComponent<IFilesListProps> = ({
    className,
    variant,
    files = [],
    match,
    location,
    addFilesTo,
    filesChangeCallback,
    filesDeleteCallback,
    objectID,
    organizationID,
    hideFilter,
    label,
    readOnly,
    fromSearch,
    eventRefetch,
    loading,
}) => {
    const { isOpen, open, hide } = useModal();
    const [filterType, setFilterType] = useState<string>("");
    const { data } = useQuery(FILE_TYPES);

    const fileTypes = get(data, "entity_types.data") || [];
    const filteredFileTypes = getFileTypesOptions(fileTypes) as TFilterDropDownOption[];

    const filteredFiles = filterType.trim().length > 0 ? files.filter(({ type }) => type === filterType) : files;
    const filesToRender = groupFilesByName(filteredFiles || []);

    /**
     * Exposing group_updated_at and group_created_at for files table
     */
    const filesList = filesToRender.map((filesGroup) => {
        const group_updated_at = orderBy(filesGroup, "updated_at", "desc")[0].updated_at;
        const group_created_at = orderBy(filesGroup, "created_at", "asc")[0].created_at;

        return filesGroup.map((obj) => ({ ...obj, group_updated_at, group_created_at }));
    });

    const { sortBy, sortingOrder, changeSortBy, getSortOrderForField } = useSortingTable({ localStorageKey: "files" });
    const sortedFiles = orderBy(filesList, [(filesGroup) => get(filesGroup[0], sortBy) || ""], [sortingOrder]);

    function handleSortBy(name: string) {
        changeSortBy(name);
    }

    function handleFilterClick(type: string) {
        setFilterType(type);
    }

    const { columns, columnsToShow, updateColumns } = useDynamicTableColumns(FILE_LIST_DEFAULT_COLUMNS, "file_details");

    const table = loading ? (
        <div className="flex justify-center _text-center">
            <Loader rawLoader />
        </div>
    ) : (
        <div className="b-table__wrapper">
            <table className="b-table b-files-list-table">
                <thead className="b-table__head">
                    <tr className="b-table__row _head">
                        <th className="b-table__cell">Information</th>
                        <th className="b-table__cell __withSwitch" />
                        {columnsToShow.map(({ label, sortable, name }) => (
                            <th className="b-table__cell" key={label}>
                                {sortable ? (
                                    <SortButton text={label} onClick={() => handleSortBy(name)} sorted={getSortOrderForField(name)} />
                                ) : (
                                    label
                                )}
                            </th>
                        ))}
                    </tr>
                </thead>
                <tbody className="b-table__body">
                    {sortedFiles.map((filesGroup, index) => (
                        <FileListItem
                            addFilesTo={addFilesTo}
                            columnsToShow={columnsToShow}
                            fileID={filesGroup[0].id}
                            fileTypes={filteredFileTypes}
                            files={filesGroup}
                            filesChangeCallback={filesChangeCallback}
                            filesDeleteCallback={filesDeleteCallback}
                            fromSearch={fromSearch}
                            key={index}
                            organizationID={organizationID}
                            parentID={objectID}
                            readOnly={readOnly}
                            variant={variant}
                            eventRefetch={eventRefetch}
                        />
                    ))}
                </tbody>
            </table>
        </div>
    );

    return (
        <>
            <div className="b-files-list-table__wrapper">
                {variant !== "contract" && (
                    <div className="flex justify-between align-center">
                        {variant === "newContract" ? (
                            <p className="_text-grey h6 _font-bold _text-uppercase">Add files to the contract</p>
                        ) : (
                            <h3>{label || "Files"}</h3>
                        )}

                        {!readOnly && (
                            <UserRoleCheck
                                availableForRoles={[
                                    "owner_administrator",
                                    "manufacturer_administrator",
                                    "manufacturer_brand_manager",
                                    "manufacturer_object_manager",
                                    "property_manager_administrator",
                                    "property_manager_building_manager",
                                    "service_provider_administrator",
                                ]}
                            >
                                <PlusButton onClick={open} />
                            </UserRoleCheck>
                        )}
                        <div className="flex b-files-list__filters">
                            {!hideFilter && (
                                <FilterDropdown
                                    text="Filter by type"
                                    className="b-files-list__filter"
                                    items={filteredFileTypes}
                                    onItemClick={handleFilterClick}
                                />
                            )}
                            <CheckboxDropdown className="b-files-list__column-select" checkboxes={columns} handleChange={updateColumns} />
                        </div>
                    </div>
                )}
                {table}
            </div>
            {!loading && (!files || files.length === 0) && <p className="b-files-list__no-files-message">No files added.</p>}
            <AddFileModal
                hide={hide}
                isOpen={isOpen}
                addFilesTo={addFilesTo}
                onUpload={filesChangeCallback}
                parentID={objectID}
                organizationID={organizationID}
                fileTypes={filteredFileTypes}
            />
        </>
    );
};

export const FilesList = withRouter(FilesListBase);
