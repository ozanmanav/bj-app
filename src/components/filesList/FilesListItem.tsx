import React, { FunctionComponent, useState } from "react";
import { Link } from "react-router-dom";
import get from "lodash.get";

import { BASE_URL } from "../../config";
import { TFile, TFileDetails } from "../../config/types";

import { FilePreviewModal } from "../modals/filePreviewModal/FilePreviewModal";
import { DownloadFilesModal } from "./downloadFilesModal";
import { UserRoleCheck } from "../userRoleCheck";
import { AddFileModal } from "./addFileModal";
import { FileIcon } from "./fileIcon";

import { downloadFiles, getFormattedFileSize } from "../../utils";
import { getObjectLink } from "../../utils";
import { getSource } from "../forms/dynamicInfoRows/utils";
import { URL_REGEX } from "../../config";
import { shouldPreviewFile, getFileDetails } from "./utils";

import { EditButton, CalendarAddButton, DownloadButton, useModal, ISelectOption } from "../ui/";
import { EventCheckboxSwitch } from "../events";

type TFilesListVariants = "object" | "newContract" | "contract" | "organization";

interface IFilesListItemProps {
    files: TFile[] | any[]; // TODO: update type after every files list will be implemented
    variant?: TFilesListVariants;
    columnsToShow: any[];
    fileTypes: ISelectOption[];
    addFilesTo?: string;
    organizationID?: string;
    filesChangeCallback?: Function;
    filesDeleteCallback?: Function;
    parentID?: string;
    readOnly?: boolean;
    fromSearch?: boolean;
    fileID: string;
    eventRefetch?: Function;
}

const FileListItem: FunctionComponent<IFilesListItemProps> = ({
    variant,
    organizationID,
    columnsToShow,
    files,
    fileTypes,
    addFilesTo,
    filesChangeCallback,
    filesDeleteCallback,
    parentID,
    readOnly,
    fromSearch,
    fileID,
    eventRefetch,
}) => {
    const { isOpen: isDownloadModalOpen, hide: downloadModalHide, open: downloadModalOpen } = useModal();
    const { isOpen: isEditModalOpen, hide: editModalHide, open: editModalOpen } = useModal();
    const { isOpen: isPreviewModalOpen, hide: previewModalHide, open: previewModalOpen } = useModal();

    const [previewUrl, setPreviewUrl] = useState<string>("");

    const shouldShowModal = files.length > 1;
    const objectLink = getObjectLink(get(files, "[0].type") || "", get(files, "[0].type.id", ""));

    function onDownloadClick() {
        shouldShowModal ? downloadModalOpen() : download();
    }

    function download() {
        const extension = files[0].extension;

        if (shouldPreviewFile(extension)) {
            setPreviewUrl(BASE_URL + files[0].url);
            previewModalOpen();

            return;
        }

        downloadSelf();
    }

    function downloadSelf() {
        downloadFiles(files);
        previewModalHide();
    }

    const rowClassname = `b-table__row ${readOnly ? "_no-edit" : ""}`;
    const fileDetails = getFileDetails(files);
    const events = get(files, "[0].events") || [];
    const noOp = () => {};
    const eventsRefetch = eventRefetch || noOp;

    const renderSourceCell = (fileDetails: TFileDetails, name: string) => {
        const source = getSource(fileDetails[name]);

        return typeof source === "object" && source ? (
            <td className="b-table__cell" key={source.name}>
                <a href={source.href} target="_blank" rel="noopener noreferrer">
                    {source.name}
                </a>
            </td>
        ) : (
            renderDefaultCell(fileDetails, name)
        );
    };

    const renderSourceUrlCell = (fileDetails: TFileDetails, name: string) => (
        <td className="b-table__cell" key={name}>
            {URL_REGEX.test(fileDetails[name]) ? (
                <a href={fileDetails[name]} target="_blank" rel="noopener noreferrer">
                    {fileDetails[name]}
                </a>
            ) : (
                fileDetails[name]
            )}
        </td>
    );

    const renderDefaultCell = (fileDetails: TFileDetails, name: string) => (
        <td className="b-table__cell" key={name}>
            {fileDetails[name]}
        </td>
    );

    const renderColumnCell = (name: string) => {
        switch (name) {
            case "source":
                return renderSourceCell(fileDetails, name);

            case "source_url":
                return renderSourceUrlCell(fileDetails, name);

            default:
                return renderDefaultCell(fileDetails, name);
        }
    };

    return (
        <tr className={rowClassname}>
            <td className="b-table__cell">
                <FileIcon extension={files[0].extension} />
                {fromSearch ? (
                    <Link to={objectLink}>
                        <p>{files[0].name}</p>
                        <p className="_text-grey h6">
                            {files[0].filename} | {getFormattedFileSize(files[0].size)} | Last edited {files[0].updated_at}
                        </p>
                    </Link>
                ) : (
                    <>
                        <p>{files[0].name}</p>
                        <p className="_text-grey h6">
                            {files[0].filename} | {getFormattedFileSize(files[0].size)} | Last edited {files[0].updated_at}
                        </p>{" "}
                    </>
                )}
            </td>

            {!readOnly && (
                <td className={`b-table__cell __withSwitch`}>
                    <div className="b-files-list-table__actions flex align-center">
                        <EventCheckboxSwitch events={events} refetch={eventsRefetch} />
                        <DownloadButton onClick={onDownloadClick} />
                        {variant !== "contract" && variant !== "newContract" && (
                            <UserRoleCheck
                                availableForRoles={[
                                    "owner_administrator",
                                    "manufacturer_administrator",
                                    "manufacturer_brand_manager",
                                    "manufacturer_object_manager",
                                    "property_manager_administrator",
                                    "property_manager_building_manager",
                                    "service_provider_administrator",
                                    "service_provider_service_manager",
                                ]}
                            >
                                <EditButton onClick={editModalOpen} />
                                <Link
                                    to={{
                                        pathname: "/app/event/add",
                                        search: `?${variant === "object" ? `obj=${parentID}` : `org=${organizationID}`}&fileID=${fileID}`,
                                    }}
                                >
                                    <CalendarAddButton />
                                </Link>
                            </UserRoleCheck>
                        )}
                    </div>
                </td>
            )}
            {columnsToShow.map(({ label, name }) => renderColumnCell(name))}
            {shouldShowModal && <DownloadFilesModal files={files} hide={downloadModalHide} isOpen={isDownloadModalOpen} />}
            {isEditModalOpen && (
                <AddFileModal
                    fileTypes={fileTypes}
                    hide={editModalHide}
                    isOpen={isEditModalOpen}
                    editFiles={files}
                    addFilesTo={addFilesTo}
                    onUpload={filesChangeCallback}
                    onDelete={filesDeleteCallback}
                    parentID={parentID}
                    organizationID={organizationID}
                    fileID={fileID}
                />
            )}
            <FilePreviewModal download={downloadSelf} url={previewUrl} hide={previewModalHide} isOpen={isPreviewModalOpen} />
        </tr>
    );
};

export default FileListItem;
