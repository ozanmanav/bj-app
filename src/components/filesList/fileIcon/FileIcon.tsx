import React, { FunctionComponent } from "react";
import "./FileIcon.scss";

interface IFileIconProps {
    extension?: string;
}

export const FileIcon: FunctionComponent<IFileIconProps> = ({ extension }) => {
    return <span className="file-icon flex align-center justify-center _font-bold">{extension ? `.${extension}` : ""}</span>;
};
