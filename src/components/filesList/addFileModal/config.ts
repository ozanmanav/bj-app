export const FILE_DESCRIPTION_FIELD_NAME = "Description";

export interface IUploadFiles {
    type_id: string;
    name: string;
    file: File;

    parent_ids: string[];
}
