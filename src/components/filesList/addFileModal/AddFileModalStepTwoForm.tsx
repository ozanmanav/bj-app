import React, { FunctionComponent, MouseEvent } from "react";
import { FormikProps } from "formik";
import { Icon } from "../../ui/icons";
import { FormCaption } from "../../forms/formsUI";
import { DynamicInfoRows, IDynamicInfoRowsState } from "../../forms/dynamicInfoRows";
import { ModalFooter } from "../../ui/modal";
import { Button } from "../../ui/buttons";

export interface IStepTwoState extends IDynamicInfoRowsState {}

interface IAddFileModalStepTwoFormProps extends FormikProps<IStepTwoState> {
    goToStepOne: (e: MouseEvent) => void;
    onRemove: (id?: string) => void;
    type: string;
    currentCategory?: string;
    fileTitle?: string;
    variantID?: string;
}

export const AddFileModalStepTwoForm: FunctionComponent<IAddFileModalStepTwoFormProps> = ({
    goToStepOne,
    variantID,
    currentCategory = "file",
    fileTitle,
    onRemove,
    ...formikProps
}) => {
    function onSelfSubmit() {
        formikProps.handleSubmit();
    }

    return (
        <>
            <button className="b-add-file-modal__back h6 _font-bold _text-uppercase flex align-center" onClick={goToStepOne}>
                <Icon icon="collapse" className="b-add-file-modal__back-icon" />
                Back
            </button>
            <FormCaption className="b-add-file-modal__step-2-caption">
                Add detail to the {fileTitle} ({currentCategory})
            </FormCaption>
            <DynamicInfoRows {...formikProps} variant={"object"} variantID={variantID} removeSingleRow={onRemove} fetchPolicy="no-cache" />
            <ModalFooter className="b-add-file-modal__footer">
                <Button primary text="Save file" icon="check" className="b-add-file-modal__footer-button" onClick={onSelfSubmit} />
            </ModalFooter>
        </>
    );
};
