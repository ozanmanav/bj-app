import React, { FunctionComponent, DragEvent, useState, ChangeEvent } from "react";
import "./FileInput.scss";
import { Icon, UploadFilesButton, showErrorToast } from "../../../ui";
import classnames from "classnames";
import { getFormattedFileSize } from "../../../../utils";
import { FileIcon } from "../../fileIcon";
import { UserRoleCheck } from "../../../userRoleCheck";
import get from "lodash.get";

interface IFileInputProps {
    files: File[];
    addFile: (file: File) => void;
    removeFile: (file: File) => void;
    error?: string;
}

export const FileInput: FunctionComponent<IFileInputProps> = ({ files, addFile, removeFile, error }) => {
    const [isDragOver, setIsDragOver] = useState<boolean>(false);

    const wrapperClassName = classnames("f-file-input__wrapper", { "_drag-over": isDragOver }, { _error: error });

    function handleFileInput(e: ChangeEvent<HTMLInputElement>) {
        const files = get(e, "target.files");

        if (files) {
            for (let i = 0; i < files.length; i++) {
                const file = files[i];
                const fileSize = file.size / 1024 / 1024; // in MB

                if (fileSize > 2) {
                    showErrorToast("File size exceeds 2 MB");
                } else {
                    addFile(files[i]);
                }
            }
        }
    }

    function handleDragOver(e: DragEvent) {
        e.preventDefault();

        setIsDragOver(true);
    }

    function handleDragLeave() {
        setIsDragOver(false);
    }

    function handleDrop(e: DragEvent) {
        e.preventDefault();

        setIsDragOver(false);

        if (!e.dataTransfer) {
            return;
        }

        if (e.dataTransfer.items) {
            for (let i = 0; i < e.dataTransfer.items.length; i++) {
                if (e.dataTransfer.items[i].kind === "file") {
                    const file = e.dataTransfer.items[i].getAsFile();

                    if (file) {
                        addFile(file);
                    }
                }
            }
        } else {
            // ie 11
            for (let i = 0; i < e.dataTransfer.files.length; i++) {
                const file = e.dataTransfer.files[i];
                addFile(file);
            }
        }
    }

    return (
        <>
            <div className={wrapperClassName} onDragOver={handleDragOver} onDragLeave={handleDragLeave} onDrop={handleDrop}>
                <p className="h3 _text-grey f-file-input__title">Drop files here</p>
                <UploadFilesButton className="f-file-input__button" />
                <input type="file" className="f-file-input__input" onChange={handleFileInput} multiple />
                {error && <p className="f-file-input__error">{error}</p>}
            </div>
            {files.length > 0 && <FilesList files={files} onRemove={removeFile} />}
        </>
    );
};

interface IFilesListProps {
    files: File[];
    onRemove: (file: File) => void;
}

const FilesList: FunctionComponent<IFilesListProps> = ({ files, onRemove }) => {
    return (
        <ul className="f-file-input__list">
            {files.map((file) => (
                <FileListItem file={file} key={`${file.name}-${file.lastModified}`} onRemove={onRemove} />
            ))}
        </ul>
    );
};

const FileListItem: FunctionComponent<{ file: File; onRemove: (file: File) => void }> = ({ file, onRemove }) => {
    const { name, size } = file;

    const extension = getExtensionByName(name);

    const formattedSize = getFormattedFileSize(size);

    function removeSelf() {
        onRemove(file);
    }

    return (
        <li className="f-file-input__list-item flex align-center justify-between">
            <p className="f-file-input__list-info flex align-center">
                <FileIcon extension={extension} />
                <span className="_text-grey h6">
                    {name} | {formattedSize}
                </span>
            </p>
            <UserRoleCheck
                availableForRoles={[
                    "owner_administrator",
                    "manufacturer_administrator",
                    "manufacturer_brand_manager",
                    "manufacturer_object_manager",
                    "property_manager_administrator",
                    "property_manager_building_manager",
                    "service_provider_administrator",
                ]}
            >
                <button className="f-file-input__list-remove" onClick={removeSelf}>
                    <Icon icon="remove" className="f-file-input__list-remove-icon" />
                </button>
            </UserRoleCheck>
        </li>
    );
};

function getExtensionByName(name: string) {
    const splitByDotLength = name.split(".").length;

    if (splitByDotLength < 2) {
        return "";
    }

    return name.split(".")[splitByDotLength - 1];
}
