import { useReducer } from "react";

export interface IFileInputReducerState {
    files: File[];
}

export interface IFileInputMethods {
    addFile: (file: File) => void;
    removeFile: (file: File) => void;
}

type TFileInputReducerAction = { type: "ADD_FILE"; file: File } | { type: "REMOVE_FILE"; file: File };

function fileInputReducer(state: IFileInputReducerState, { type, file }: TFileInputReducerAction): IFileInputReducerState {
    switch (type) {
        case "ADD_FILE":
            return {
                ...state,
                files: [...state.files, file],
            };
        case "REMOVE_FILE":
            return {
                ...state,
                files: state.files.filter((f) => f !== file),
            };
        default:
            return state;
    }
}

export function useFileInput(defaultFiles: File[] = []) {
    const [state, dispatch] = useReducer(fileInputReducer, { files: defaultFiles });

    function addFile(file: File) {
        // TODO: prevent uploading same file twice?
        dispatch({
            type: "ADD_FILE",
            file,
        });
    }

    function removeFile(file: File) {
        dispatch({
            type: "REMOVE_FILE",
            file,
        });
    }

    return {
        files: state.files,
        addFile,
        removeFile,
    };
}
