import { TFile } from "../../../config/types";
import { FILE_DESCRIPTION_FIELD_NAME } from "./config";
import { ISelectOption } from "../../ui/inputs";
import get from "lodash.get";

export function prepareEditStepOne(files: TFile[]) {
    const filesDetails = files[0].details;
    const descriptionField = filesDetails && filesDetails.find(({ name }) => name === FILE_DESCRIPTION_FIELD_NAME);
    const descriptionValue = descriptionField ? descriptionField.value : "";

    return {
        category: get(files, "[0].type.id"),
        name: get(files, "[0].name"),
        description: descriptionValue,
        files: files.map(
            (file) =>
                (({
                    name: file.filename,
                    size: file.size,
                    id: file.id,
                } as unknown) as File)
        ),
    };
}

export function prepareEditStepTwo(details: any) {
    return {
        rows: details || [],
    };
}

export function getCurrentCategoryLabel(categoryValue: string, categories: ISelectOption[]) {
    const currentCategory = categories.find(({ value }) => value === categoryValue);

    if (currentCategory) {
        return currentCategory.label;
    }

    return "";
}

export const prepareDetailDataForCreateMutation = (restDetail: object, fileID?: string, isCustomField?: boolean, model?: string) => {
    const { verification, source, source_url, source_date, verification_by } = get(restDetail, "meta");

    const details = {
        details: [
            {
                name: get(restDetail, "name"),
                type: get(restDetail, "type"),
                value: get(restDetail, "value"),
                context: get(restDetail, "context"),
                highlighted: get(restDetail, "highlighted") || false,
                verification,
                source,
                source_url,
                source_date,
                verification_by,
                is_custom_field: isCustomField,
            },
        ],
        model: model || "OBJECT",
        model_id: fileID,
    };

    return details;
};
export const prepareDetailDataForUpdateMutation = (restDetail: object, fileID?: string, isCustomField?: boolean, model?: string) => {
    const { verification, source, source_url, source_date, verification_by } = get(restDetail, "meta");

    const details = {
        id: get(restDetail, "id"),
        name: get(restDetail, "name"),
        type: get(restDetail, "type"),
        value: get(restDetail, "value"),
        context: get(restDetail, "context"),
        highlighted: get(restDetail, "highlighted") || false,
        verification,
        source,
        source_url,
        source_date,
        verification_by,
        is_custom_field: isCustomField,
        model: model || "OBJECT",
        model_id: fileID,
    };

    return details;
};
