import { FileInput, useFileInput } from "./fileInput";
import { ISelectOption, Select } from "../../ui/inputs/select";
import React, { FunctionComponent, useEffect, useRef, useState } from "react";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui/toasts";

import { Button } from "../../ui/buttons";
import { FormCaption } from "../../forms/formsUI";
import { FormikProps } from "formik";
import { Input } from "../../ui/inputs/input";
import { ModalFooter } from "../../ui/modal";
import { TFile } from "../../../config/types";
import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";
import { getArrayIDs } from "../../../views/app/event/addEvent/utils";
import { restoreScroll } from "../../../hooks";

export interface IStepOneState {
    category: string;
    name: string;
    files: File[];
}

const DELETE_FILE = gql`
    mutation DeleteFile($id: String!) {
        deleteObject(id: $id)
    }
`;

interface IAddFileModalStepOneFormProps extends FormikProps<IStepOneState> {
    addFilesTo?: string;
    fileTypes: ISelectOption[];
    onUpload?: Function;
    onDelete: Function;
    defaultFiles?: File[];
}

export const AddFileModalStepOneForm: FunctionComponent<IAddFileModalStepOneFormProps> = ({
    addFilesTo,
    fileTypes,
    onUpload,
    defaultFiles,
    onDelete,
    ...formikProps
}) => {
    const { files, ...fileMethods } = useFileInput(defaultFiles);

    const isInitialized = useRef<boolean>(false);
    const [fileToDelete, setFileToDelete] = useState<TFile | null>(null);

    const { values, errors, touched, handleSubmit, handleChange, handleBlur, setFieldValue, setFieldTouched } = formikProps;

    const fileError = touched && touched.files && errors && errors.files && errors.files.toString();

    useEffect(() => {
        if (isInitialized.current) {
            setFieldValue("files", files);
            setFieldTouched("files", true);
        }

        isInitialized.current = true;
    }, [files]);

    function addFile(file: File) {
        fileMethods.addFile(file);
    }

    function removeFile(file: File | TFile) {
        if ((file as TFile).id) {
            showDeleteConfirmation(file);
        } else {
            fileMethods.removeFile(file as File);
        }
    }

    function onSelfSubmit() {
        handleSubmit();
    }

    const [deleteFileMutation] = useMutation(DELETE_FILE);

    async function deleteFile(file: TFile) {
        try {
            const { data, errors } = await deleteFileMutation({
                variables: {
                    id: file.id,
                },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                const isDeleted = data.deleteObject;

                if (isDeleted) {
                    const deletedFileID = file.id;

                    const updatedValues = values.files.filter((f: any) => f !== file);

                    setFieldValue("files", updatedValues);
                    hideDeleteConfirmation();
                    restoreScroll();

                    if (onUpload) {
                        const newValuesIDs = getArrayIDs(updatedValues);

                        await onUpload(newValuesIDs, deletedFileID);

                        showSuccessToast("File is successfully deleted.");
                    }

                    if (!updatedValues.find((f: any) => f.filename === file.filename)) {
                        onDelete(deletedFileID);
                    }
                }
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    function hideDeleteConfirmation() {
        setFileToDelete(null);
    }

    function showDeleteConfirmation(file: File | TFile) {
        setFileToDelete(file as TFile);
    }

    return (
        <>
            {!fileToDelete ? (
                <>
                    <FormCaption>Add file to:</FormCaption>
                    <h2 className="h1 b-add-file-modal__target">{addFilesTo}</h2>
                    <FileInput files={values.files} addFile={addFile} removeFile={removeFile} error={fileError} />
                    <Input
                        placeholder="File Title"
                        name="name"
                        value={values.name}
                        error={errors && errors.name}
                        touched={touched && touched.name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                    <Select
                        options={fileTypes}
                        placeholder="Category"
                        position="top"
                        name="category"
                        value={values.category}
                        error={errors && errors.category}
                        touched={touched && touched.category}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                    <ModalFooter className="b-add-file-modal__footer">
                        <Button text="Details" primary icon="nextArrow" onClick={onSelfSubmit} />
                    </ModalFooter>
                </>
            ) : (
                <ConfirmDelete file={fileToDelete} confirm={deleteFile} cancel={hideDeleteConfirmation} />
            )}
        </>
    );
};

interface IConfirmDeleteProps {
    file: TFile | null;
    confirm: (file: TFile) => void;
    cancel: () => void;
}

const ConfirmDelete: FunctionComponent<IConfirmDeleteProps> = ({ file, confirm, cancel }) => {
    function onConfirm() {
        if (file) {
            confirm(file);
        }
    }

    return (
        <>
            <p className="b-add-file-modal__confirm-title h4 _font-bold">Delete file {file && file.name}?</p>
            <div className="b-add-file-modal__confirm-actions flex align-center">
                <Button text="Cancel" onClick={cancel} className="b-add-file-modal__confirm-button" />
                <Button text="Yes" primary onClick={onConfirm} className="b-add-file-modal__confirm-button" />
            </div>
        </>
    );
};
