import "./AddFileModal.scss";

import * as Yup from "yup";

import { AddFileModalStepOneForm, IStepOneState } from "./AddFileModalStepOneForm";
import { AddFileModalStepTwoForm, IStepTwoState } from "./AddFileModalStepTwoForm";
import { IModalProps, ISelectOption, Modal, showErrorToast, showSuccessToast } from "../../ui";
import React, { FunctionComponent, useState } from "react";
import { TFile, VALIDATION_ERRORS } from "../../../config";
import {
    getCurrentCategoryLabel,
    prepareEditStepOne,
    prepareEditStepTwo,
    prepareDetailDataForCreateMutation,
    prepareDetailDataForUpdateMutation,
} from "./utils";

import { DynamicsInfoRowSchema } from "../../forms/dynamicInfoRows";
import { Formik } from "formik";
import classnames from "classnames";
import get from "lodash.get";
import gql from "graphql-tag";
import { normalizeDetails, isNillOrEmpty } from "../../../utils";
import { useApolloClient, useQuery } from "@apollo/react-hooks";
import { UPDATE_DETAIL, CREATE_DETAIL, DELETE_DETAIL } from "../../object/objectDetailsTable/queries";

interface IAddFileModalProps extends IModalProps {
    addFilesTo?: string;
    onUpload?: Function;
    onDelete?: Function;
    parentID?: string;
    organizationID?: string;
    fileTypes: ISelectOption[];
    editFiles?: TFile[];
    fileID?: string;
}

const StepOneValidationSchema = Yup.object().shape({
    files: Yup.array()
        .of(Yup.mixed().required(VALIDATION_ERRORS.required))
        .min(1, "Should be at least one file"),
    name: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    category: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
});

const stepOneDefaultData = {
    category: "",
    name: "",
    files: [],
};

const StepTwoValidationSchema = Yup.object().shape({
    rows: DynamicsInfoRowSchema,
});

const stepTwoDefaultState = {
    rows: [],
};

const GET_FILE_DETAILS = gql`
    query GetFileDetails($id: String!) {
        details(model: OBJECT, model_id: $id) {
            data {
                id
                type
                name
                value
                context
                meta {
                    source
                    source_url
                    source_date
                    verification
                    verification_by
                    verifications {
                        verifier_id
                        verifier {
                            name
                        }
                    }
                }
            }
        }
    }
`;

const UPLOAD_FILE = gql`
    mutation UploadFile($type_id: String!, $name: String!, $parent_ids: [String], $organization_id: String, $file: Upload) {
        uploadFile(type_id: $type_id, name: $name, parent_ids: $parent_ids, organization_id: $organization_id, file: $file) {
            id
        }
    }
`;

const UPDATE_FILE = gql`
    mutation UpdateFile($id: String!, $type_id: String, $name: String) {
        updateObject(id: $id, type_id: $type_id, name: $name) {
            id
        }
    }
`;

export const AddFileModal: FunctionComponent<IAddFileModalProps> = ({
    hide,
    addFilesTo,
    onUpload,
    onDelete,
    parentID,
    organizationID,
    fileTypes,
    editFiles,
    fileID,
    ...props
}) => {
    const { data, refetch: refetchFileDetails } = useQuery(GET_FILE_DETAILS, {
        variables: {
            id: fileID,
        },
        fetchPolicy: "no-cache",
    });

    const detailsData = get(data, "details.data");

    const stepOneInitialState = editFiles ? prepareEditStepOne(editFiles) : stepOneDefaultData;
    const stepTwoInitialState = editFiles && fileID ? prepareEditStepTwo(detailsData) : stepTwoDefaultState;

    const [currentStep, setCurrentStep] = useState<1 | 2>(1);
    const [stepOneData, setStepOneData] = useState<IStepOneState>(stepOneInitialState);

    const currentCategory = getCurrentCategoryLabel(stepOneData.category, fileTypes);

    const apolloClient = useApolloClient();

    function goToStepTwo(stepOneData: IStepOneState) {
        setStepOneData(stepOneData);
        setCurrentStep(2);
    }

    function goToStepOne() {
        setCurrentStep(1);
    }

    function selfHide() {
        hide();
        setStepOneData(stepOneInitialState);
        setCurrentStep(1);
    }

    const onRemoveDetail = async (id?: any) => {
        const deleteResponse = await apolloClient.mutate({
            mutation: DELETE_DETAIL,
            variables: {
                model: "OBJECT",
                model_id: fileID,
                id,
            },
        });

        const isDeleted = get(deleteResponse, "data.deleteDetail");

        if (isDeleted) {
            await refetchFileDetails();

            showSuccessToast("Detail is successfully deleted.");

            selfHide();
        }
    };

    function prepareFilesToUpload() {
        return stepOneData.files
            .filter((file) => file instanceof File)
            .map((file) => ({
                type_id: stepOneData.category,
                name: stepOneData.name,
                file,
                parent_ids: parentID ? [parentID] : [],
                ...(organizationID && { organization_id: organizationID }),
            }));
    }

    function prepareFilesToUpdate() {
        return stepOneData.files
            .filter((file) => !(file instanceof File))
            .map((file) => ({
                id: ((file as unknown) as TFile).id,
                type_id: stepOneData.category,
                name: stepOneData.name,
            }));
    }

    async function uploadFiles() {
        const files = prepareFilesToUpload();

        try {
            // TODO: show uploading toast
            const response = await Promise.all([
                ...files.map((variables) => {
                    return apolloClient.mutate({
                        mutation: UPLOAD_FILE,
                        variables,
                    });
                }),
            ]);

            const uploadedIDs = response.map((res) => {
                return get(res, "data.uploadFile.id");
            });

            showSuccessToast("Files are successfully uploaded.");

            selfHide();

            if (onUpload) {
                onUpload(uploadedIDs);
            }

            return response;
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    async function updateFiles() {
        try {
            const filesToUpload = prepareFilesToUpload() || [];
            const filesToUpdate = prepareFilesToUpdate() || [];

            const response = await Promise.all([
                ...filesToUpload.map((variables) =>
                    apolloClient.mutate({
                        mutation: UPLOAD_FILE,
                        variables,
                    })
                ),
                ...filesToUpdate.map((variables) =>
                    apolloClient.mutate({
                        mutation: UPDATE_FILE,
                        variables,
                    })
                ),
            ]);

            const updatedIDs: string[] = response.map((res) => {
                const uploadFileId = get(res, "data.uploadFile.id");
                const updateFileId = get(res, "data.updateObject.id");

                return uploadFileId ? uploadFileId : updateFileId;
            });

            showSuccessToast("Files are successfully updated.");

            selfHide();

            if (onUpload) {
                onUpload(updatedIDs);
            }

            return response;
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    const groupDetails = (stepTwoData: IStepTwoState) => {
        const details = normalizeDetails(stepTwoInitialState.rows as []) || [];
        const detailsNames = details.map((item) => item.name) || [];

        const stateDetails = normalizeDetails(stepTwoData.rows as []) || [];

        const newDetails = details.length === 0 ? stateDetails : stateDetails.filter((e) => !detailsNames.includes(e.name));
        const updateDetails = stateDetails.filter((item) => !isNillOrEmpty(get(item, "id")));
        return { newDetails, updateDetails };
    };

    const updateCreateDetails = async (stepTwoData: IStepTwoState, stateFileID: string) => {
        try {
            const { updateDetails, newDetails } = groupDetails(stepTwoData);

            const response = await Promise.all([
                ...updateDetails.map((detail) => {
                    const { id } = detail;

                    if (id) {
                        return apolloClient.mutate({
                            mutation: UPDATE_DETAIL,
                            variables: prepareDetailDataForUpdateMutation(detail, stateFileID),
                        });
                    }

                    return null;
                }),
                ...newDetails.map(({ id, isCustomField, ...restDetail }) => {
                    if (!id) {
                        return apolloClient.mutate({
                            mutation: CREATE_DETAIL,
                            variables: prepareDetailDataForCreateMutation(restDetail, stateFileID, isCustomField),
                        });
                    }

                    return null;
                }),
            ]);

            return response;
        } catch (e) {
            showErrorToast(e.message);
        }
    };

    async function submit(stepTwoData: IStepTwoState) {
        const response = editFiles ? await updateFiles() : await uploadFiles();

        const stateFileID = editFiles ? fileID : get(response, "[0].data.uploadFile.id");
        // Detail operations
        await updateCreateDetails(stepTwoData, stateFileID);
    }

    const onDeleteCallback = (deletedID: string) => {
        if (onDelete) {
            onDelete(deletedID);
        }

        selfHide();
    };

    const modalClassname = classnames(["b-add-file-modal", { "_step-two": currentStep === 2 }]);

    return (
        <Modal hide={selfHide} {...props} className={modalClassname}>
            {currentStep === 1 && (
                <Formik
                    onSubmit={goToStepTwo}
                    initialValues={stepOneData}
                    validationSchema={StepOneValidationSchema}
                    render={(formikProps) => (
                        <AddFileModalStepOneForm
                            {...formikProps}
                            addFilesTo={addFilesTo}
                            fileTypes={fileTypes}
                            onUpload={onUpload}
                            onDelete={onDeleteCallback}
                            defaultFiles={stepOneData.files}
                        />
                    )}
                />
            )}

            {currentStep === 2 && (
                <Formik
                    onSubmit={submit}
                    initialValues={stepTwoInitialState}
                    validationSchema={StepTwoValidationSchema}
                    render={(formikProps) => (
                        <AddFileModalStepTwoForm
                            goToStepOne={goToStepOne}
                            type={stepOneData.category}
                            {...formikProps}
                            currentCategory={currentCategory}
                            fileTitle={stepOneData.name}
                            onRemove={onRemoveDetail}
                            variantID={fileID}
                        />
                    )}
                />
            )}
        </Modal>
    );
};
