import React, { FunctionComponent, useState, MouseEvent } from "react";
import "./DownloadFilesModal.scss";
import { Button, DownloadButton, IModalProps, Modal, useModal } from "../../ui";
import { FileIcon } from "../fileIcon";
import { downloadFiles, getFormattedFileSize } from "../../../utils";
import { TFile } from "../../../config/types";
import classnames from "classnames";
import { FilePreviewModal } from "../../modals/filePreviewModal/FilePreviewModal";
import { shouldPreviewFile } from "../utils";
import { BASE_URL } from "../../../config";

interface IDownloadFilesModalProps extends IModalProps {
    files: TFile[];
}

export const DownloadFilesModal: FunctionComponent<IDownloadFilesModalProps> = ({ files, ...props }) => {
    const [selectedIDs, setSelectedIDs] = useState<string[]>([]);

    function addSelected(id: string) {
        setSelectedIDs([...selectedIDs, id]);
    }

    function removeSelected(id: string) {
        setSelectedIDs(selectedIDs.filter((selectedID) => selectedID !== id));
    }

    function downloadSelected() {
        downloadFiles(files.filter(({ id }) => selectedIDs.includes(id)));
    }

    return (
        <Modal {...props}>
            <h3>{files[0].name}</h3>
            <div className="b-files-list__list">
                {files.map((file) => (
                    <DownloadFile
                        {...file}
                        key={file.id}
                        addSelected={addSelected}
                        removeSelected={removeSelected}
                        isSelected={selectedIDs.includes(file.id)}
                    />
                ))}
            </div>
            {selectedIDs.length > 0 && (
                <Button
                    text={`Download ${selectedIDs.length} file${selectedIDs.length > 1 ? "s" : ""}`}
                    primary
                    onClick={downloadSelected}
                    className="b-download-files-modal__btn"
                />
            )}
        </Modal>
    );
};

interface IDownloadFileProps {
    addSelected: (id: string) => void;
    removeSelected: (id: string) => void;
    isSelected: boolean;
}

const DownloadFile: FunctionComponent<TFile & IDownloadFileProps> = ({ addSelected, removeSelected, isSelected, ...file }) => {
    const { isOpen, hide, open } = useModal();
    const [previewUrl, setPreviewUrl] = useState<string>("");

    const { id, extension, filename, size, updated_at } = file;

    const wrapperClassname = classnames("b-files-list__item flex align-center _downloadable", { _selected: isSelected });

    function download(e: MouseEvent) {
        e.stopPropagation();

        const extension = file.extension;

        if (shouldPreviewFile(extension)) {
            setPreviewUrl(BASE_URL + file.url);
            open();

            return;
        }

        downloadSelf();
    }

    function downloadSelf() {
        downloadFiles([file]);
    }

    function onSelfClick() {
        isSelected ? removeSelected(id) : addSelected(id);
    }

    return (
        <>
            <div className={wrapperClassname} onClick={onSelfClick}>
                <FileIcon extension={extension} />
                <div className="b-files-list__item-info">
                    <p>{filename}</p>
                    <p className="_text-grey h6">
                        {getFormattedFileSize(size)} | Last edited {updated_at}
                    </p>
                </div>
                <div className="b-files-list__actions flex align-center">
                    <DownloadButton onClick={download} />
                </div>
            </div>
            <FilePreviewModal url={previewUrl} download={downloadSelf} hide={hide} isOpen={isOpen} />
        </>
    );
};
