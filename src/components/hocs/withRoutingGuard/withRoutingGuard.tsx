import React from "react";
import { Redirect } from "react-router-dom";
import { useRoleCheck } from "../../../hooks";
import { TRole, TUserType } from "../../../config/types";

interface IWithRoutingGuardProps {
    redirectUrl?: string;
    availableForRoles?: TRole[];
    availableForAdminRoles?: TUserType[];
}

export const DEFAULT_ROUTING_GUARD_REDIRECT_URL = "/app/cockpit";

export const withRoutingGuard = (WrappedComponent: any, props: IWithRoutingGuardProps) => {
    return function(childProps: any) {
        const hasAccess = useRoleCheck(props.availableForRoles, props.availableForAdminRoles);

        if (hasAccess) {
            return <WrappedComponent {...childProps} />;
        }

        return <Redirect to={props.redirectUrl || DEFAULT_ROUTING_GUARD_REDIRECT_URL} />;
    };
};
