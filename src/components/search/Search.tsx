import React, { FunctionComponent, useReducer, useMemo, useState, useEffect, useCallback, useContext } from "react";
import "./Search.scss";
import { SearchQuery } from "./searchQuery";
import { SearchResults } from "./searchResults";
import { useRemoveBodyScroll, useChangeLocationCallback } from "../../hooks";
import { SearchQueryContextProvider } from "./searchQuery/searchQueryContext";
import { getCurrentBuildStep } from "./searchQuery/utils";
import { searchQueryReducer, searchQueryReducerInitialState, resetSearchState } from "./searchQuery/searchQueryReducer";
import { useDebounce } from "use-debounce";
import { performSearch } from "./utils";
import { useApolloClient } from "@apollo/react-hooks";
import { showErrorToast } from "../ui/toasts";
import { RouteComponentProps, withRouter } from "react-router";
import { AccountStateContext } from "../../contexts/accountStateContext";
import { SEARCH_QUERY_DEBOUNCE_TIME } from "./config";

interface SearchProps extends RouteComponentProps {
    isOpened: boolean;
    hide: () => void;
}

const SearchBase: FunctionComponent<SearchProps> = ({ isOpened, hide, history, location }) => {
    useRemoveBodyScroll(isOpened);
    const apolloClient = useApolloClient();

    const { user } = useContext(AccountStateContext);

    const isAdmin = user.is_admin;

    const [queryState, queryDispatch] = useReducer(searchQueryReducer, searchQueryReducerInitialState);

    const providerValue = useMemo(
        () => ({
            queryState,
            queryDispatch,
            queryBuildStep: getCurrentBuildStep(queryState),
        }),
        [queryState, queryDispatch]
    );
    const [debouncedQueryState] = useDebounce(queryState, SEARCH_QUERY_DEBOUNCE_TIME);

    const [results, setResults] = useState<any[]>([]);
    // const [adminResults, setAdminResults] = useState<any[]>([]);
    const [isLoading, setIsLoading] = useState<boolean>(false);

    const selfHide = useCallback(() => {
        queryDispatch(resetSearchState());
        setResults([]);
        hide();
    }, [queryDispatch, setResults, hide]);
    // const ref = useClickOutside<HTMLDivElement>(selfHide);
    useChangeLocationCallback(selfHide, location);

    useEffect(() => {
        async function fetchResults() {
            if (debouncedQueryState) {
                setIsLoading(true);

                const res = await performSearch(debouncedQueryState, apolloClient, isAdmin);

                setIsLoading(false);

                if (!isOpened) {
                    return;
                }

                // if (debouncedQueryState.action === "openObject" && res.user.length === 1) {
                //     selfHide();
                //     history.push(getObjectLink(res.user[0].type, res.user[0].id));
                //     return;
                // }

                setResults(res.user);
                // setAdminResults(res.admin);
            }
        }

        if (debouncedQueryState.action && isOpened) {
            try {
                fetchResults();
            } catch (e) {
                showErrorToast(e.message);
                setIsLoading(false);
            }
        }
    }, [debouncedQueryState]);

    if (!isOpened) {
        return null;
    }

    return (
        <SearchQueryContextProvider value={providerValue}>
            <div className="b-search">
                <SearchQuery />
                {/* {isAdmin && (
                    <Container>
                        <h2 className="b-search__group-title _first">Company results</h2>
                    </Container>
                )} */}
                <SearchResults results={results} action={queryState.action} showOnlyNumberOfObjects={false} isLoading={isLoading} />
                {/* {isAdmin && (
                    <Container>
                        <h2 className="b-search__group-title">Service results</h2>
                        <SearchResults
                            showOnlyNumberOfObjects={false}
                            isLoading={isLoading}
                            results={adminResults}
                        />
                    </Container>
                )} */}
            </div>
        </SearchQueryContextProvider>
    );
};

export const Search = withRouter(SearchBase);
