import gql from "graphql-tag";

export const SEARCH_QUERY_DEBOUNCE_TIME = 1000;

export const OBJECT_QUERY_ARGUMENTS_DECLARATION = `
    $field1: SearchObjectsFieldsEnum1, 
    $value1: String, 
    $comparison1: SearchObjectsComparisonEnum1, 
    $condition: SearchObjectsConditionEnum
    $field2: SearchObjectsFieldsEnum2, 
    $value2: String, 
    $comparison2: SearchObjectsComparisonEnum2, 
    $date1: String,
    $date_field1: SearchObjectsDateFieldsEnum1,
    $date_comparison1: SearchObjectsDateComparisonEnum1,
    $date2: String,
    $date_field2: SearchObjectsDateFieldsEnum2,
    $date_comparison2: SearchObjectsDateComparisonEnum2
    $relationship: SearchObjectsRelationship 
    $page: Int
`;

export const OBJECT_QUERY_ARGUMENTS = `
    field1: $field1, 
    value1: $value1, 
    comparison1: $comparison1,
    condition: $condition,
    field2: $field2, 
    value2: $value2, 
    comparison2: $comparison2,
    date1: $date1,
    date_field1: $date_field1,
    date_comparison1: $date_comparison1,
    date2: $date2,
    date_field2: $date_field2,
    date_comparison2: $date_comparison2,
    relationship: $relationship
    page: $page
`;

export const GET_OBJECT_QUERY = gql`
    query SearchObject(${OBJECT_QUERY_ARGUMENTS_DECLARATION}){
        search_objects(${OBJECT_QUERY_ARGUMENTS}) {
            data {
                id
                type {id, name}
                name
                address,
                meta {
                    author {
                        first_name,
                        last_name
                    }
                }
            }
            total
            current_page
            has_more_pages

        }
    }
`;

export const PEOPLE_QUERY_ARGUMENTS_DECLARATION = `
    $field1: SearchPeopleFieldsEnum1, 
    $value1: String, 
    $comparison1: SearchPeopleComparisonEnum1, 
    $condition: SearchPeopleConditionEnum
    $field2: SearchPeopleFieldsEnum2, 
    $value2: String, 
    $comparison2: SearchPeopleComparisonEnum2, 
    $organization_id: String,
    $date1: String,
    $date_field1: SearchPeopleDateFieldsEnum1,
    $date_comparison1: SearchPeopleDateComparisonEnum1,
    $date2: String,
    $date_field2: SearchPeopleDateFieldsEnum2,
    $date_comparison2: SearchPeopleDateComparisonEnum2
`;

export const PEOPLE_QUERY_ARGUMENTS = `
    field1: $field1, 
    value1: $value1, 
    comparison1: $comparison1,
    condition: $condition,
    field2: $field2, 
    value2: $value2, 
    comparison2: $comparison2,
    organization_id: $organization_id,
    date1: $date1,
    date_field1: $date_field1,
    date_comparison1: $date_comparison1,
    date2: $date2,
    date_field2: $date_field2,
    date_comparison2: $date_comparison2
`;

export const SEARCH_PEOPLE_QUERY = gql`
  query SearchPeople(${PEOPLE_QUERY_ARGUMENTS_DECLARATION}) {
          search_people(${PEOPLE_QUERY_ARGUMENTS}) {
              total
            current_page
            has_more_pages
            data {
                id
                name
                type,
                email,
                phone,
                mobile_phone,
                employers { 
                    id
                    organization {
                        name
                    },
                    job {
                        name
                    } 
                }
                
            }
        }
    }
`;

export const FILE_QUERY_ARGUMENTS_DECLARATION = `
    $field1: SearchFilesFieldsEnum1, 
    $value1: String, 
    $comparison1: SearchFilesComparisonEnum1, 
    $condition: SearchFilesConditionEnum
    $field2: SearchFilesFieldsEnum2, 
    $value2: String, 
    $comparison2: SearchFilesComparisonEnum2, 
    $date1: String,
    $date_field1: SearchFilesDateFieldsEnum1,
    $date_comparison1: SearchFilesDateComparisonEnum1,
    $date2: String,
    $date_field2: SearchFilesDateFieldsEnum2,
    $date_comparison2: SearchFilesDateComparisonEnum2
`;

export const FILE_QUERY_ARGUMENTS = `
    field1: $field1, 
    value1: $value1, 
    comparison1: $comparison1,
    condition: $condition,
    field2: $field2, 
    value2: $value2, 
    comparison2: $comparison2,
    date1: $date1,
    date_field1: $date_field1,
    date_comparison1: $date_comparison1,
    date2: $date2,
    date_field2: $date_field2,
    date_comparison2: $date_comparison2
`;

/// TODO: details deleted from here, should implement another query or in the files list.
export const FILE_PEOPLE_QUERY = gql`
  query SearchFiles(${FILE_QUERY_ARGUMENTS_DECLARATION}) {
        search_files(${FILE_QUERY_ARGUMENTS}) {
            total
            current_page
            has_more_pages
         data {
            id
            type {
                id
                type
                entity
                name
            }
            name
            url
            filename
            extension
            size
            updated_at
          }
        }
    }
`;

export const CONTRACT_QUERY_ARGUMENTS_DECLARATION = `
    $field1: SearchContractsFieldsEnum1, 
    $value1: String, 
    $comparison1: SearchContractsComparisonEnum1, 
    $condition: SearchContractsConditionEnum
    $field2: SearchContractsFieldsEnum2, 
    $value2: String, 
    $comparison2: SearchContractsComparisonEnum2, 
    $date1: String,
    $date_field1: SearchContractsDateFieldsEnum1,
    $date_comparison1: SearchContractsDateComparisonEnum1,
    $date2: String,
    $date_field2: SearchContractsDateFieldsEnum2,
    $date_comparison2: SearchContractsDateComparisonEnum2,
    $organization_id: String,
`;

export const CONTRACT_QUERY_ARGUMENTS = `
    field1: $field1, 
    value1: $value1, 
    comparison1: $comparison1,
    condition: $condition,
    field2: $field2, 
    value2: $value2, 
    comparison2: $comparison2,
    date1: $date1,
    date_field1: $date_field1,
    date_comparison1: $date_comparison1,
    date2: $date2,
    date_field2: $date_field2,
    date_comparison2: $date_comparison2,
    organization_id: $organization_id,
`;

export const CONTRACT_PEOPLE_QUERY = gql`
  query SearchContracts(${CONTRACT_QUERY_ARGUMENTS_DECLARATION}) {
          search_contracts(${CONTRACT_QUERY_ARGUMENTS}) {
              total
            current_page
            has_more_pages
            data {
                id
                organization {
                    id
                    name
                    category
                } 
                role{
                    id
                    name
                    type
                    entity
                    organizations
                }
                employees{
                    id
                    user{
                        id
                        first_name
                        last_name
                    }
                }
                objects{
                    name
                    id
                    address
                    type{
                        id
                        type
                        name
                    }
                }
                object_count
            }
        }
    }
`;

export const EVENT_QUERY_ARGUMENTS_DECLARATION = `
    $field1: SearchEventsFieldsEnum1, 
    $value1: String, 
    $comparison1: SearchEventsComparisonEnum1, 
    $condition: SearchEventsConditionEnum
    $field2: SearchEventsFieldsEnum2, 
    $value2: String, 
    $comparison2: SearchEventsComparisonEnum2, 
    $date1: String,
    $date_field1: SearchEventsDateFieldsEnum1,
    $date_comparison1: SearchEventsDateComparisonEnum1,
    $date2: String,
    $date_field2: SearchEventsDateFieldsEnum2,
    $date_comparison2: SearchEventsDateComparisonEnum2
`;

export const EVENT_QUERY_ARGUMENTS = `
    field1: $field1, 
    value1: $value1, 
    comparison1: $comparison1,
    condition: $condition,
    field2: $field2, 
    value2: $value2, 
    comparison2: $comparison2,
    date1: $date1,
    date_field1: $date_field1,
    date_comparison1: $date_comparison1,
    date2: $date2,
    date_field2: $date_field2,
    date_comparison2: $date_comparison2
`;

// TODO: details deleted from here should implement another place.
export const EVENT_QUERY = gql`
  query SearchEvents(${EVENT_QUERY_ARGUMENTS_DECLARATION}) {
          search_events(${EVENT_QUERY_ARGUMENTS}) {
            total
            current_page
            has_more_pages
            data {
                 id
                 name
                 start_date
                 end_date
                 type {
                    name
                    id
                }
                object {
                    id
                    name
                    address
                }
            }
        }
    }
`;

export const ORGANIZATION_QUERY_ARGUMENTS_DECLARATION = `
    $field1: SearchOrganizationsFieldsEnum1, 
    $value1: String, 
    $comparison1: SearchOrganizationsComparisonEnum1, 
    $condition: SearchOrganizationsConditionEnum
    $field2: SearchOrganizationsFieldsEnum2, 
    $value2: String, 
    $comparison2: SearchOrganizationsComparisonEnum2, 
    $date1: String,
    $date_field1: SearchOrganizationsDateFieldsEnum1,
    $date_comparison1: SearchOrganizationsDateComparisonEnum1,
    $date2: String,
    $date_field2: SearchOrganizationsDateFieldsEnum2,
    $date_comparison2: SearchOrganizationsDateComparisonEnum2
    $relationship: SearchOrganizationsRelationship 
`;

export const ORGANIZATION_QUERY_ARGUMENTS = `
    field1: $field1, 
    value1: $value1, 
    comparison1: $comparison1,
    condition: $condition,
    field2: $field2, 
    value2: $value2, 
    comparison2: $comparison2,
    date1: $date1,
    date_field1: $date_field1,
    date_comparison1: $date_comparison1,
    date2: $date2,
    date_field2: $date_field2,
    date_comparison2: $date_comparison2,
    relationship: $relationship
`;

export const ORGANIZATION_QUERY = gql`
    query SearchOrganization(${ORGANIZATION_QUERY_ARGUMENTS_DECLARATION}){
        search_organizations(${ORGANIZATION_QUERY_ARGUMENTS}) {
            total
            current_page
            has_more_pages
            data {
                id
                type {id, name}
                name
                category
                address {
                    city
                    street
                    number
                    plz
                    country
                    line_1
                    line_2
                }
                email
                phone
                meta {
                    author {
                        first_name,
                        last_name
                    }
                }
            }
        }
    }
`;
