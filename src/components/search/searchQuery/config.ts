export type TActionQueries =
    | "search_people"
    | "search_objects"
    | "search_files"
    | "search_contracts"
    | "search_events"
    | "search_organizations";

interface IQueryBaseParams {
    label: string;
}

export interface IActionQueryParams extends IQueryBaseParams {
    query: TActionQueries;
}

export const ACTIONS: IActionQueryParams[] = [
    {
        label: "Object",
        query: "search_objects",
    },
    {
        label: "People",
        query: "search_people",
    },
    {
        label: "File",
        query: "search_files",
    },
    {
        label: "RELATIONSHIP/CONTRACT",
        query: "search_contracts",
    },
    {
        label: "Events",
        query: "search_events",
    },
    {
        label: "Organizations",
        query: "search_organizations",
    },
];

export type TSearchByQueries =
    | "object_name"
    | "object_type"
    | "object_info"
    | "object_relationship"
    | "object_date"
    | "object_meta"
    | "person_info"
    | "person_works_for"
    | "person_meta"
    | "person_by_role"
    | "file_info"
    | "file_type"
    | "file_meta"
    | "contract_by_role"
    | "contract_meta"
    | "contract_date"
    | "event_info"
    | "event_meta"
    | "organization_info"
    | "organization_type"
    | "organization_relationship"
    | string;

export interface ISearchByQueryParams extends IQueryBaseParams {
    query: TSearchByQueries;
}

const OBJECT_FIELD_NAMES: ISearchByQueryParams[] = [
    {
        label: "Object Info",
        query: "object_info",
    },
    {
        label: "Object Meta",
        query: "object_meta",
    },
    {
        label: "Object Date",
        query: "object_date",
    },
    {
        label: "Object Relationship",
        query: "object_relationship",
    },
];

const PEOPLE_FIELD_NAMES: ISearchByQueryParams[] = [
    {
        label: "Person Info",
        query: "person_info",
    },
    {
        label: "Person Works For",
        query: "person_works_for",
    },
    {
        label: "Person Meta",
        query: "person_meta",
    },

    {
        label: "Person By Role",
        query: "person_by_role",
    },
];

const FILE_FIELD_NAMES: ISearchByQueryParams[] = [
    {
        label: "File Info",
        query: "file_info",
    },
    {
        label: "File Type",
        query: "file_type",
    },
    {
        label: "File Meta",
        query: "file_meta",
    },
];

const CONTRACT_FIELD_NAMES: ISearchByQueryParams[] = [
    {
        label: "Contract by Role",
        query: "contract_by_role",
    },
    {
        label: "Contract Meta",
        query: "contract_meta",
    },
    {
        label: "Contract Date",
        query: "contract_date",
    },
];

const EVENT_FIELD_NAMES: ISearchByQueryParams[] = [
    {
        label: "Event Info",
        query: "event_info",
    },
    {
        label: "Event Meta",
        query: "event_meta",
    },
];

const ORGANIZATION_FIELD_NAMES: ISearchByQueryParams[] = [
    {
        label: "Organization Detail",
        query: "organization_info",
    },
    {
        label: "Organization Type",
        query: "organization_type",
    },
    {
        label: "Organization Structure",
        query: "organization_relationship",
    },
];

export const SEARCH_BY = {
    search_objects: OBJECT_FIELD_NAMES,
    search_people: PEOPLE_FIELD_NAMES,
    search_files: FILE_FIELD_NAMES,
    search_contracts: CONTRACT_FIELD_NAMES,
    search_events: EVENT_FIELD_NAMES,
    search_organizations: ORGANIZATION_FIELD_NAMES,
};

export type TByDatesQueries = "created_at" | "updated_at";

export interface IByDatesQueryParams extends IQueryBaseParams {
    query: TByDatesQueries;
}

export const BY_DATES: IByDatesQueryParams[] = [
    {
        label: "Created",
        query: "created_at",
    },
    {
        label: "Updated",
        query: "updated_at",
    },
];
