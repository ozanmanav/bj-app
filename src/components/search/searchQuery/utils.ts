import { ISearchQueryReducerState } from "./searchQueryReducer";
import { TActionQueries, ACTIONS, BY_DATES, TByDatesQueries, SEARCH_BY, TSearchByQueries } from "./config";
import contains from "ramda/es/contains";
import splitAt from "ramda/es/splitAt";

export function getCurrentBuildStep(queryState: ISearchQueryReducerState) {
    if (queryState.action === null) {
        return 1;
    }

    if (queryState.searchBy.length === 0) {
        return 2;
    }

    if (queryState.byDates.type === null) {
        return 3;
    }

    return 4;
}

export function getActionLabelByType(type: TActionQueries | null) {
    const action = ACTIONS.find(({ query }) => query === type);

    return action ? action.label : "";
}

export function getSearchByLabelByType(actionType: TActionQueries, type: TSearchByQueries | null) {
    let preparedType = findTypeFromMultipleQuery(type);

    const action = SEARCH_BY[actionType].find(({ query }) => query === preparedType);

    return action ? action.label : "";
}

export function getByDatesLabelByType(type: TByDatesQueries | null) {
    const action = BY_DATES.find(({ query }) => query === type);

    return action ? action.label : "";
}

function findTypeFromMultipleQuery(type: TSearchByQueries | null) {
    return type && contains("_2", type) ? splitAt(-2, type)[0] : type;
}
