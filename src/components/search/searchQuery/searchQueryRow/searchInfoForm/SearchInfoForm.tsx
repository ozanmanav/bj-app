import React, { FunctionComponent, useEffect } from "react";
import { Formik, FormikProps, FormikActions } from "formik";
import * as Yup from "yup";
import { VALIDATION_ERRORS } from "../../../../../config";
import { Input, ISelectOption, Select } from "../../../../ui/inputs";
import "./SearchInfoForm.scss";
import { SEARCH_STRING_COMPARISON_OPTIONS } from "../config";
import { isNillOrEmpty, getObjectTypesOptions } from "../../../../../utils";
import { useObjectTypes } from "../../../../../hooks";

const SearchInfoFormSchema = Yup.object().shape({
    field: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    comparison: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    value: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
});

export const SearchInfoInitialState = {
    field: "",
    comparison: "",
    value: "",
};

export interface ISearchInfo {
    field: string;
    comparison: string;
    value: string;
}

interface ISearchInfoFormBaseProps extends FormikProps<ISearchInfo> {
    fieldOptions: ISelectOption[];
}

const SearchInfoFormBase: FunctionComponent<ISearchInfoFormBaseProps> = ({
    values,
    errors,
    touched,
    submitForm,
    handleSubmit,
    handleChange,
    handleBlur,
    fieldOptions,
}) => {
    const objectTypes = useObjectTypes();
    const selectOptions = getObjectTypesOptions(objectTypes);
    useEffect(() => {
        if (touched.value) {
            setTimeout(submitForm, 0);
        }
    }, [values.field, values.comparison]);

    const onChangeLastInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        handleChange(event);
        setTimeout(submitForm, 0);
    };

    const onObjectTypeChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        handleChange(event);
        setTimeout(submitForm, 0);
    };

    return (
        <form onSubmit={handleSubmit} className="f-search-info">
            <div className="flex justify-between">
                <Select
                    name="field"
                    options={fieldOptions}
                    value={values.field}
                    className="f-search-info__field"
                    marginBottom="none"
                    placeholder="Field"
                    error={errors && errors.field}
                    touched={touched && touched.field}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />
                <Select
                    name="comparison"
                    options={SEARCH_STRING_COMPARISON_OPTIONS}
                    value={values.comparison}
                    disabled={isNillOrEmpty(values.field)}
                    className="f-search-info__comparison"
                    marginBottom="none"
                    placeholder="Comparison"
                    error={errors && errors.comparison}
                    touched={touched && touched.comparison}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />
                {values.field !== "type_id" && (
                    <Input
                        name="value"
                        placeholder="Value"
                        marginBottom="none"
                        value={values.value}
                        disabled={isNillOrEmpty(values.comparison)}
                        className="f-search-info__value"
                        error={errors && errors.value}
                        touched={touched && touched.value}
                        onChange={onChangeLastInput}
                        onBlur={handleBlur}
                    />
                )}
                {values.field === "type_id" && (
                    <Select
                        name="value"
                        value={values.value}
                        disabled={isNillOrEmpty(values.comparison)}
                        options={selectOptions}
                        onChange={onObjectTypeChange}
                        placeholder="Object Type"
                        marginBottom="none"
                        className="b-search-query-row__build-input"
                    />
                )}
            </div>
        </form>
    );
};

export const SearchInfoForm: FunctionComponent<{
    onSubmit: (values: ISearchInfo, formikActions: FormikActions<ISearchInfo>) => void;
    fieldOptions: ISelectOption[];
}> = ({ onSubmit, fieldOptions }) => {
    return (
        <>
            <Formik
                initialValues={SearchInfoInitialState}
                onSubmit={onSubmit}
                validationSchema={SearchInfoFormSchema}
                render={(formikProps) => <SearchInfoFormBase fieldOptions={fieldOptions} {...formikProps} />}
            />
        </>
    );
};
