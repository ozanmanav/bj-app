import React, { FunctionComponent, useEffect } from "react";
import { Formik, FormikProps, FormikActions } from "formik";
import * as Yup from "yup";
import { VALIDATION_ERRORS } from "../../../../../config";
import { AutocompleteInput, Select } from "../../../../ui/inputs";
import "./SearchOrganizationRelationshipForm.scss";
import get from "lodash.get";
import { isNillOrEmpty } from "../../../../../utils";
import { ORGANIZATION_RELATIONSHIP_FIELDS } from "../config";

const SearchOrganizationRelationshipFormSchema = Yup.object().shape({
    organizationRelationshipType: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    organizationID: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
});

export const SearchOrganizationRelationshipFormInitialState = {
    organizationRelationshipType: "",
    organizationID: "",
};

export interface ISearchOrganizationRelationship {
    organizationRelationshipType: string;
    organizationID: string;
}

interface ISearchOrganizationRelationshipFormBaseProps extends FormikProps<ISearchOrganizationRelationship> {}

const SearchOrganizationRelationshipFormBase: FunctionComponent<ISearchOrganizationRelationshipFormBaseProps> = ({
    values,
    errors,
    touched,
    submitForm,
    handleSubmit,
    handleBlur,
    setFieldValue,
}) => {
    useEffect(() => {
        if (!isNillOrEmpty(values.organizationID) && touched.organizationRelationshipType) {
            setTimeout(submitForm, 0);
        }
    }, [values.organizationID]);

    const onSelectOrganizationRelationshipType = (event: React.ChangeEvent<HTMLSelectElement>) => {
        setFieldValue("organizationRelationshipType", event.currentTarget.value);
    };

    const onSelectObject = (item: any) => {
        setFieldValue("organizationID", get(item, "id"));
        setTimeout(submitForm, 0);
    };

    return (
        <form onSubmit={handleSubmit} className="f-search-organization-relationship">
            <div className="flex justify-between">
                <Select
                    name="organizationRelationshipType"
                    options={ORGANIZATION_RELATIONSHIP_FIELDS}
                    value={values.organizationRelationshipType}
                    className="f-search-organization-relationship__relationship-type"
                    marginBottom="none"
                    placeholder="Relationship Type"
                    error={errors && errors.organizationRelationshipType}
                    touched={touched && touched.organizationRelationshipType}
                    onChange={onSelectOrganizationRelationshipType}
                    onBlur={handleBlur}
                />
                <AutocompleteInput
                    className="f-search-organization-relationship__object"
                    placeholder="Search Organizations"
                    searchFor={"allOrganizations"}
                    onChange={onSelectObject}
                    disabled={isNillOrEmpty(values.organizationRelationshipType)}
                    isSetValue={true}
                />
            </div>
        </form>
    );
};

export const SearchOrganizationRelationshipForm: FunctionComponent<{
    onSubmit: (values: ISearchOrganizationRelationship, formikActions: FormikActions<ISearchOrganizationRelationship>) => void;
}> = ({ onSubmit }) => {
    return (
        <>
            <Formik
                initialValues={SearchOrganizationRelationshipFormInitialState}
                onSubmit={onSubmit}
                validationSchema={SearchOrganizationRelationshipFormSchema}
                render={(formikProps) => <SearchOrganizationRelationshipFormBase {...formikProps} />}
            />
        </>
    );
};
