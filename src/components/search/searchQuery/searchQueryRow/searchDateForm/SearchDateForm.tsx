import React, { FunctionComponent, useEffect } from "react";
import { Formik, FormikProps, FormikActions } from "formik";
import * as Yup from "yup";
import { VALIDATION_ERRORS, DATE_FORMAT } from "../../../../../config";
import { Select } from "../../../../ui/inputs";
import "./SearchDateForm.scss";
import { BY_DATES_SELECT_OPTIONS, COMMON_DATE_FIELDS } from "../config";
import { DatepickerSimple } from "../../../../ui";
import { isNillOrEmpty } from "../../../../../utils";
import { formatDate } from "../../../../ui/datepicker/utils";

const SearchDateFormSchema = Yup.object().shape({
    date_field: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    date_comparison: Yup.string().trim(),
    date: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
});

export const SearchDateFormInitialState = {
    date_field: "",
    date_comparison: "",
    date: "",
};

export interface ISearchDate {
    date_field: string;
    date_comparison: string;
    date: string;
}

interface ISearchDateFormBaseProps extends FormikProps<ISearchDate> {}

const SearchDateFormBase: FunctionComponent<ISearchDateFormBaseProps> = ({
    values,
    errors,
    touched,
    submitForm,
    handleSubmit,
    handleChange,
    handleBlur,
    setFieldValue,
}) => {
    useEffect(() => {
        if (touched.date) {
            setTimeout(submitForm, 0);
        }
    }, [values.date_field]);

    const onChangeDatePicker = (date: Date) => {
        if (date) {
            setFieldValue("date", formatDate(date, DATE_FORMAT));
            setTimeout(submitForm, 0);
        }
    };

    return (
        <form onSubmit={handleSubmit} className="f-search-date-form ">
            <div className="flex justify-between">
                <Select
                    name="date_field"
                    options={COMMON_DATE_FIELDS}
                    value={values.date_field}
                    className="f-search-date-form__field"
                    marginBottom="none"
                    placeholder="Field"
                    error={errors && errors.date_field}
                    touched={touched && touched.date_field}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />
                <Select
                    name="date_comparison"
                    options={BY_DATES_SELECT_OPTIONS}
                    value={values.date_comparison}
                    disabled={isNillOrEmpty(values.date_field)}
                    className="f-search-date-form__comparison"
                    marginBottom="none"
                    placeholder="Comparison"
                    error={errors && errors.date_comparison}
                    touched={touched && touched.date_comparison}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />
                <DatepickerSimple className="f-search-date-form__value" onChange={onChangeDatePicker} />
            </div>
        </form>
    );
};

export const SearchDateForm: FunctionComponent<{
    onSubmit: (values: ISearchDate, formikActions: FormikActions<ISearchDate>) => void;
}> = ({ onSubmit }) => {
    return (
        <>
            <Formik
                initialValues={SearchDateFormInitialState}
                onSubmit={onSubmit}
                validationSchema={SearchDateFormSchema}
                render={(formikProps) => <SearchDateFormBase {...formikProps} />}
            />
        </>
    );
};
