import { ISelectOption } from "../../../ui";

export const BY_DATES_SELECT_OPTIONS = [
    {
        value: "less_than",
        label: "Before",
    },
    {
        value: "greater_than",
        label: "After",
    },
    {
        value: "between",
        label: "Between",
    },
    {
        value: "equal",
        label: "Equal",
    },
];

export const LOGICAL_OPERATORS_OPTIONS = [
    { value: "AND", label: "AND" },
    { value: "OR", label: "OR" },
];

export const PERSON_INFO_FIELDS: ISelectOption[] = [
    {
        value: "first_name",
        label: "First Name",
    },
    {
        value: "last_name",
        label: "Last Name",
    },
    {
        value: "email",
        label: "E-mail",
    },
    {
        value: "phone",
        label: "Phone",
    },
    {
        value: "language",
        label: "Language",
    },
];

export const SEARCH_STRING_COMPARISON_OPTIONS: ISelectOption[] = [
    {
        value: "is",
        label: "Is",
    },
    {
        value: "starts_with",
        label: "Starts With",
    },
    {
        value: "ends_with",
        label: "Ends With",
    },
    {
        value: "contains",
        label: "Contains",
    },
];

export const OBJECT_INFO_FIELDS: ISelectOption[] = [
    {
        value: "name",
        label: "Object Name",
    },
    {
        value: "type_id",
        label: "Object Type",
    },
    {
        value: "address",
        label: "Address",
    },
    {
        value: "country",
        label: "Country",
    },
    {
        value: "city",
        label: "City",
    },
    {
        value: "street",
        label: "Street",
    },
    {
        value: "details_name",
        label: "Details Name",
    },
    {
        value: "details_value",
        label: "Details Value",
    },
];

export const COMMON_META_FIELDS: ISelectOption[] = [
    {
        value: "author_id",
        label: "Author",
    },
    {
        value: "modified_by",
        label: "Modified By",
    },

    {
        value: "verification_by",
        label: "Verification By",
    },
    {
        value: "context",
        label: "Context",
    },
];

export const COMMON_DATE_FIELDS: ISelectOption[] = [
    {
        value: "source_date",
        label: "Source Date",
    },
    {
        value: "created_at",
        label: "Created At",
    },
    {
        value: "updated_at",
        label: "Updated At",
    },
];

export const FILE_INFO_FIELDS: ISelectOption[] = [
    {
        value: "name",
        label: "File Name",
    },
    {
        value: "type",
        label: "File Type",
    },
    {
        value: "mime",
        label: "Mime",
    },
    {
        value: "extension",
        label: "Extension",
    },
    {
        value: "details_name",
        label: "Details Name",
    },
    {
        value: "details_value",
        label: "Details Value",
    },
];

export const EVENT_INFO_FIELDS: ISelectOption[] = [
    {
        value: "name",
        label: "Event Name",
    },
    {
        value: "type_id",
        label: "Event Type",
    },
    {
        value: "details_name",
        label: "Details Name",
    },
    {
        value: "details_value",
        label: "Details Value",
    },
];

export const ORGANIZATION_INFO_FIELDS: ISelectOption[] = [
    {
        value: "name",
        label: "Name",
    },
    {
        value: "country",
        label: "Country",
    },
    {
        value: "city",
        label: "City",
    },
    {
        value: "street",
        label: "Street",
    },
    {
        value: "phone",
        label: "Phone",
    },
    {
        value: "email",
        label: "E-mail",
    },
    {
        value: "details_name",
        label: "Details Name",
    },
    {
        value: "details_value",
        label: "Details Value",
    },
];

export const ORGANIZATION_RELATIONSHIP_FIELDS: ISelectOption[] = [
    {
        value: "lower_id",
        label: "Parent",
    },
    {
        value: "higher_id",
        label: "Subsidiary",
    },
];
