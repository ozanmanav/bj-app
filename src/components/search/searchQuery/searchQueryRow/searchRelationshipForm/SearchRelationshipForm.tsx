import React, { FunctionComponent, useEffect } from "react";
import { Formik, FormikProps, FormikActions } from "formik";
import * as Yup from "yup";
import { VALIDATION_ERRORS } from "../../../../../config";
import { AutocompleteInput, Select } from "../../../../ui/inputs";
import "./SearchRelationshipForm.scss";
import get from "lodash.get";
import { useRelationTypes } from "../../../../../hooks/apollo";
import { getRelationTypesOptions, isNillOrEmpty } from "../../../../../utils";

const SearchRelationshipFormSchema = Yup.object().shape({
    relationshipType: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    objectID: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
});

export const SearchRelationshipFormInitialState = {
    relationshipType: "",
    objectID: "",
};

export interface ISearchRelationship {
    relationshipType: string;
    objectID: string;
}

interface ISearchRelationshipFormBaseProps extends FormikProps<ISearchRelationship> {}

const SearchRelationshipFormBase: FunctionComponent<ISearchRelationshipFormBaseProps> = ({
    values,
    errors,
    touched,
    submitForm,
    handleSubmit,
    handleBlur,
    setFieldValue,
}) => {
    const relationTypes = useRelationTypes();
    const relationTypesOptions = getRelationTypesOptions(relationTypes);

    useEffect(() => {
        if (!isNillOrEmpty(values.objectID) && touched.relationshipType) {
            setTimeout(submitForm, 0);
        }
    }, [values.objectID]);

    const onSelectRelationshipType = (event: React.ChangeEvent<HTMLSelectElement>) => {
        setFieldValue("relationshipType", event.currentTarget.value);
    };

    const onSelectObject = (item: any) => {
        setFieldValue("objectID", get(item, "id"));
        setTimeout(submitForm, 0);
    };

    return (
        <form onSubmit={handleSubmit} className="f-search-relationship">
            <div className="flex justify-between">
                <Select
                    name="relationshipType"
                    options={relationTypesOptions}
                    value={values.relationshipType}
                    className="f-search-relationship__relationship-type"
                    marginBottom="none"
                    placeholder="Relationship Type"
                    error={errors && errors.relationshipType}
                    touched={touched && touched.relationshipType}
                    onChange={onSelectRelationshipType}
                    onBlur={handleBlur}
                />
                <AutocompleteInput
                    className="f-search-relationship__object"
                    placeholder="Search Objects"
                    searchFor={"objects"}
                    onChange={onSelectObject}
                    disabled={isNillOrEmpty(values.relationshipType)}
                    isSetValue={true}
                />
            </div>
        </form>
    );
};

export const SearchRelationshipForm: FunctionComponent<{
    onSubmit: (values: ISearchRelationship, formikActions: FormikActions<ISearchRelationship>) => void;
}> = ({ onSubmit }) => {
    return (
        <>
            <Formik
                initialValues={SearchRelationshipFormInitialState}
                onSubmit={onSubmit}
                validationSchema={SearchRelationshipFormSchema}
                render={(formikProps) => <SearchRelationshipFormBase {...formikProps} />}
            />
        </>
    );
};
