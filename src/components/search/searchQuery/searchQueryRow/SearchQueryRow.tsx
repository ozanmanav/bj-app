import React, { ChangeEvent, FunctionComponent, InputHTMLAttributes, MouseEvent, SelectHTMLAttributes, useContext, useEffect } from "react";
import "./SearchQueryRow.scss";
import { AutocompleteInput, Button, Container, DatepickerSimple, Input, Select } from "../../../ui";
import { getActionLabelByType, getByDatesLabelByType, getSearchByLabelByType } from "../utils";
import { searchQueryContext } from "../searchQueryContext";
import { TSearchByQueries } from "../config";
import classNames from "classnames";
import * as actions from "../searchQueryReducer";
import { getObjectTypesOptions, getPersonRolesOptions, getOrganizationTypesOptions } from "../../../../utils";
import { useFileTypes, useObjectTypes, useUserRoles, useOrganizationTypes } from "../../../../hooks";
import { IOrganization } from "../../../../config";
import {
    BY_DATES_SELECT_OPTIONS,
    EVENT_INFO_FIELDS,
    FILE_INFO_FIELDS,
    LOGICAL_OPERATORS_OPTIONS,
    OBJECT_INFO_FIELDS,
    PERSON_INFO_FIELDS,
    ORGANIZATION_INFO_FIELDS,
} from "./config";
import get from "lodash.get";
import { ISearchContractByRole, SearchContractByRoleForm } from "./searchContractByRoleForm";
import { SearchMetaForm } from "./searchMetaForm";
import { ISearchInfo, SearchInfoForm } from "./searchInfoForm";
import { ISearchDate, SearchDateForm } from "./searchDateForm";
import { ISearchRelationship, SearchRelationshipForm } from "./searchRelationshipForm";
import { SearchOrganizationRelationshipForm, ISearchOrganizationRelationship } from "./searchOrganizationRelationshipForm";

export const SearchQueryRow: FunctionComponent = () => {
    const { queryBuildStep } = useContext(searchQueryContext);
    const isBuilding = queryBuildStep > 1;

    return (
        <div className={classNames("b-search-query-row", { _building: isBuilding })}>
            <Container className="flex align-center justify-between">
                {isBuilding ? <SearchQueryRowBuild /> : <SearchQueryRowPlaceholder />}
            </Container>
        </div>
    );
};

const SearchQueryRowPlaceholder: FunctionComponent = () => {
    return <p className="b-search-query-row__placeholder _text-grey h4">Search for specific objects, find totals and due dates...</p>;
};

const SearchQueryRowBuild: FunctionComponent = () => {
    const { queryState, queryBuildStep } = useContext(searchQueryContext);
    const { action } = queryState;

    const actionText = getActionLabelByType(action);

    return (
        <div className="b-search-query-row__build">
            <p className="_text-grey _font-bold h6 _text-uppercase b-search-query-row__build-title">I want to search...</p>
            <div className="flex flex-wrap align-center b-search-query-row__build-blocks">
                <Button text={actionText} primary className="b-search-query-row__build-button" />

                {queryBuildStep > 2 && <SearchQuerySearchByRow />}

                {queryBuildStep > 3 && <SearchQueryByDatesRow />}
            </div>
        </div>
    );
};

const SearchQuerySearchByRow: FunctionComponent = () => {
    const { queryState } = useContext(searchQueryContext);
    const { searchBy } = queryState;

    return (
        <>
            <span className="_text-grey _font-bold h6 _text-uppercase b-search-query-row__separator">that have</span>
            {searchBy.map((item, index) => (
                <SearchQueryRowBuildSearchByItem {...item} showLogicalOperator={index > 0} key={item.type} />
            ))}
        </>
    );
};

interface ISearchQuerySearchByRowItemProps {
    type: TSearchByQueries;
    query: actions.IQuery;
    locationType?: string;
    showLogicalOperator: boolean;
}

const SearchQueryRowBuildSearchByItem: FunctionComponent<ISearchQuerySearchByRowItemProps> = ({ type, query, showLogicalOperator }) => {
    const { queryDispatch, queryState } = useContext(searchQueryContext);

    const typeText = getSearchByLabelByType(queryState.action, type);

    const showPersonInfoForm = type === "person_info" || type === "person_info_2";
    const showPersonWorksForInput = type === "person_works_for";
    const showPersonMetaForm = type === "person_meta" || type === "person_meta_2";
    const showPersonByRoleSelect = type === "person_by_role";
    const showObjectNameInput = type === "object_name";
    const showObjectTypeSelect = type === "object_type";
    const showObjectInfoForm = type === "object_info" || type === "object_info_2";
    const showObjectMetaForm = type === "object_meta" || type === "object_meta_2";
    const showObjectDateForm = type === "object_date" || type === "object_date_2";
    const showObjectRelationshipForm = type === "object_relationship" || type === "object_relationship_2";
    const showFileTypeSelect = type === "file_type" || type === "file_type_2";
    const showFileInfoForm = type === "file_info" || type === "file_info_2";
    const showFileMetaForm = type === "file_meta" || type === "file_meta_2";
    const showContractByRoleForm = type === "contract_by_role";
    const showContractMetaForm = type === "contract_meta" || type === "contract_meta_2";
    const showContractDateForm = type === "contract_date" || type === "contract_date_2";
    const showEventInfoForm = type === "event_info" || type === "event_info_2";
    const showEventMetaForm = type === "event_meta" || type === "event_meta_2";
    const showOrganizationInfoForm = type === "organization_info" || type === "organization_info_2";
    const showOrganizationTypeSelect = type === "organization_type";
    const showOrganizationRelationshipForm = type === "organization_relationship" || type === "organization_relationship_2";

    function removeCondition(e: MouseEvent) {
        e.stopPropagation();

        queryDispatch(actions.removeSearchByQuery(type));
    }

    function onLogicOperatorChange(e: ChangeEvent<HTMLSelectElement>) {
        queryDispatch(actions.updateSearchByLogicOperator(e.target.value));
    }

    function onPersonInfoChange(values: ISearchInfo) {
        queryDispatch(actions.updateSearchByPersonInfo(type, values));
    }

    function onPersonWorksForChange(selectedOrganization: IOrganization) {
        queryDispatch(actions.updateSearchByPersonWorksFor(type, { organizationID: get(selectedOrganization, "id") }));
    }

    function onPersonMetaChange(values: ISearchInfo) {
        queryDispatch(actions.updateSearchByPersonMeta(type, values));
    }

    function onPersonRoleChange(e: React.ChangeEvent<HTMLSelectElement>) {
        queryDispatch(actions.updateSearchByPersonRole(type, e.target.value));
    }

    function onObjectNameChange(e: React.ChangeEvent<HTMLInputElement>) {
        queryDispatch(actions.updateSearchByObjectName(type, e.target.value));
    }

    function onObjectTypeChange(e: React.ChangeEvent<HTMLSelectElement>) {
        queryDispatch(actions.updateSearchByObjectType(type, e.target.value));
    }

    function onObjectInfoChange(values: ISearchInfo) {
        queryDispatch(actions.updateSearchByObjectInfo(type, values));
    }

    function onObjectMetaChange(values: ISearchInfo) {
        queryDispatch(actions.updateSearchByObjectMeta(type, values));
    }

    function onObjectDateChange(values: ISearchDate) {
        queryDispatch(actions.updateSearchByObjectDate(type, values));
    }

    function onObjectRelationshipChange(values: ISearchRelationship) {
        queryDispatch(actions.updateSearchByObjectRelationship(type, values));
    }

    function onFileTypeChange(e: React.ChangeEvent<HTMLSelectElement>) {
        queryDispatch(actions.updateSearchByFileType(type, e.target.value));
    }

    function onFileInfoChange(values: ISearchInfo) {
        queryDispatch(actions.updateSearchByFileInfo(type, values));
    }

    function onFileMetaChange(values: ISearchInfo) {
        queryDispatch(actions.updateSearchByFileMeta(type, values));
    }

    function onContractByRoleChange(values: ISearchContractByRole) {
        queryDispatch(actions.updateSearchByContractByRole(type, values));
    }

    function onContractMetaChange(values: ISearchInfo) {
        queryDispatch(actions.updateSearchByContractMeta(type, values));
    }

    function onContractDateChange(values: ISearchDate) {
        queryDispatch(actions.updateSearchByContractDate(type, values));
    }

    function onEventInfoChange(values: ISearchInfo) {
        queryDispatch(actions.updateSearchByEventInfo(type, values));
    }

    function onEventMetaChange(values: ISearchInfo) {
        queryDispatch(actions.updateSearchByEventMeta(type, values));
    }

    function onOrganizationInfoChange(values: ISearchInfo) {
        queryDispatch(actions.updateSearchByOrganizationInfo(type, values));
    }

    function onOrganizationTypeChange(e: React.ChangeEvent<HTMLSelectElement>) {
        queryDispatch(actions.updateSearchByOrganizationType(type, e.currentTarget.value));
    }

    function onOrganizationRelationshipChange(values: ISearchOrganizationRelationship) {
        queryDispatch(actions.updateSearchByOrganizationRelationship(type, values));
    }

    return (
        <>
            {showLogicalOperator && (
                <div className="b-search-query-row__build-logical-ops">
                    <Select
                        onChange={onLogicOperatorChange}
                        options={LOGICAL_OPERATORS_OPTIONS}
                        value={queryState.logicOperator}
                        marginBottom="none"
                        externalSelectClassName="b-search-query-row__build-logical-ops__select _text-grey _font-bold h6 _text-uppercase"
                    />
                </div>
            )}

            <Button text={typeText} primary className="b-search-query-row__build-button" onClick={removeCondition} />

            {showPersonInfoForm && <SearchInfoForm fieldOptions={PERSON_INFO_FIELDS} onSubmit={onPersonInfoChange} />}

            {showPersonWorksForInput && <SearchPersonWorksForAutoCompleteInput onChange={onPersonWorksForChange} />}

            {showPersonMetaForm && <SearchMetaForm onSubmit={onPersonMetaChange} />}

            {showPersonByRoleSelect && <SearchPersonByRoleSelect onChange={onPersonRoleChange} value={query} />}

            {showObjectNameInput && <SearchObjectNameInput onChange={onObjectNameChange} />}

            {showObjectTypeSelect && <SearchObjectTypeSelect onChange={onObjectTypeChange} value={query} />}

            {showObjectRelationshipForm && <SearchRelationshipForm onSubmit={onObjectRelationshipChange} />}

            {showObjectInfoForm && <SearchInfoForm fieldOptions={OBJECT_INFO_FIELDS} onSubmit={onObjectInfoChange} />}

            {showObjectMetaForm && <SearchMetaForm onSubmit={onObjectMetaChange} />}

            {showObjectDateForm && <SearchDateForm onSubmit={onObjectDateChange} />}

            {showFileTypeSelect && <SearchFileTypeSelect onChange={onFileTypeChange} value={query} />}

            {showFileInfoForm && <SearchInfoForm fieldOptions={FILE_INFO_FIELDS} onSubmit={onFileInfoChange} />}

            {showFileMetaForm && <SearchMetaForm onSubmit={onFileMetaChange} />}

            {showContractByRoleForm && <SearchContractByRoleForm onSubmit={onContractByRoleChange} />}

            {showContractMetaForm && <SearchMetaForm onSubmit={onContractMetaChange} />}

            {showContractDateForm && <SearchDateForm onSubmit={onContractDateChange} />}

            {showEventInfoForm && <SearchInfoForm fieldOptions={EVENT_INFO_FIELDS} onSubmit={onEventInfoChange} />}

            {showEventMetaForm && <SearchMetaForm onSubmit={onEventMetaChange} />}

            {showOrganizationInfoForm && <SearchInfoForm fieldOptions={ORGANIZATION_INFO_FIELDS} onSubmit={onOrganizationInfoChange} />}

            {showOrganizationTypeSelect && <SearchOrganizationTypeSelect onChange={onOrganizationTypeChange} value={query} />}

            {showOrganizationRelationshipForm && <SearchOrganizationRelationshipForm onSubmit={onOrganizationRelationshipChange} />}
        </>
    );
};

export const SearchPersonWorksForAutoCompleteInput: FunctionComponent<{ onChange: (item: any) => void }> = ({ onChange }) => {
    return (
        <AutocompleteInput
            className="b-search-query-row__build-autocomplete"
            placeholder="Search organizations"
            searchFor={"allOrganizations"}
            resultsContainerSize="small"
            onChange={onChange}
            isSetValue={true}
        />
    );
};

const SearchPersonByRoleSelect: FunctionComponent<SelectHTMLAttributes<HTMLSelectElement>> = ({ onChange, value }) => {
    const userRoles = useUserRoles({});

    const selectOptions = getPersonRolesOptions(userRoles);

    return (
        <Select
            value={value}
            options={selectOptions}
            onChange={onChange}
            placeholder="Role"
            marginBottom="none"
            className="b-search-query-row__build-input"
        />
    );
};

const SearchObjectNameInput: FunctionComponent<InputHTMLAttributes<HTMLInputElement>> = ({ onChange, value }) => {
    return (
        <Input
            placeholder="Object Name"
            marginBottom="none"
            className="b-search-query-row__build-input"
            value={value}
            onChange={onChange}
        />
    );
};

const SearchObjectTypeSelect: FunctionComponent<SelectHTMLAttributes<HTMLSelectElement>> = ({ onChange, value }) => {
    const objectTypes = useObjectTypes();
    const selectOptions = getObjectTypesOptions(objectTypes);

    return (
        <Select
            value={value}
            options={selectOptions}
            onChange={onChange}
            placeholder="Object Type"
            marginBottom="none"
            className="b-search-query-row__build-input"
        />
    );
};

const SearchOrganizationTypeSelect: FunctionComponent<SelectHTMLAttributes<HTMLSelectElement>> = ({ onChange, value }) => {
    const organizationTypes = useOrganizationTypes();
    const selectOptions = getOrganizationTypesOptions(organizationTypes);

    return (
        <Select
            value={value}
            options={selectOptions}
            onChange={onChange}
            placeholder="Organization Type"
            marginBottom="none"
            className="b-search-query-row__build-input"
        />
    );
};

const SearchFileTypeSelect: FunctionComponent<SelectHTMLAttributes<HTMLSelectElement>> = ({ onChange, value }) => {
    const fileTypes = useFileTypes();
    const selectOptions = getObjectTypesOptions(fileTypes);

    return (
        <Select
            value={value}
            options={selectOptions}
            onChange={onChange}
            placeholder="File Type"
            marginBottom="none"
            className="b-search-query-row__build-input"
        />
    );
};

const SearchQueryByDatesRow: FunctionComponent = () => {
    const { queryState, queryDispatch } = useContext(searchQueryContext);
    const { byDates } = queryState;
    const { type, firstDate, secondDate, comparison } = byDates;

    const typeText = getByDatesLabelByType(type);

    const shouldShowSecondDatepicker = comparison === "between";

    const secondDatepickerDisabledDates = firstDate && { before: firstDate };

    useEffect(() => {
        queryDispatch(actions.updateDate(new Date(), "secondDate"));
    }, [shouldShowSecondDatepicker]);

    function handleSelectChange(e: ChangeEvent<HTMLSelectElement>) {
        queryDispatch(actions.setByDatesPeriod(e.target.value));
    }

    function removeByDates(e: MouseEvent) {
        e.stopPropagation();

        queryDispatch(actions.removeSearchByDates());
    }

    function onFirstDateChange(date: Date) {
        queryDispatch(actions.updateDate(date, "firstDate"));
    }

    function onSecondDateChange(date: Date) {
        queryDispatch(actions.updateDate(date, "secondDate"));
    }

    return (
        <>
            <span className="_text-grey _font-bold h6 _text-uppercase b-search-query-row__separator">and was</span>
            <Button text={typeText} primary className="b-search-query-row__build-button" onClick={removeByDates} />
            <Select
                options={BY_DATES_SELECT_OPTIONS}
                value={comparison}
                onChange={handleSelectChange}
                placeholder="Before"
                marginBottom="none"
            />
            <DatepickerSimple onChange={onFirstDateChange} value={firstDate} icon="calendarGrey" dateFormat="simple" />
            {shouldShowSecondDatepicker && (
                <>
                    <span className="_text-grey _font-bold h6 _text-uppercase b-search-query-row__separator">and</span>
                    <DatepickerSimple
                        onChange={onSecondDateChange}
                        defaultValue={secondDate}
                        disabledDays={secondDatepickerDisabledDates}
                        icon="calendarGrey"
                        dateFormat="simple"
                    />
                </>
            )}
        </>
    );
};
