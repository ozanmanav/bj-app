import React, { FunctionComponent, useEffect } from "react";
import { Formik, FormikProps, FormikActions } from "formik";
import * as Yup from "yup";
import { VALIDATION_ERRORS } from "../../../../..//config";
import { AutocompleteInput, Input, Select } from "../../../../ui/inputs";
import "./SearchMetaForm.scss";
import { COMMON_META_FIELDS } from "../config";
import get from "lodash.get";

const SearchMetaFormSchema = Yup.object().shape({
    field: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    comparison: Yup.string().trim(),
    value: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
});

export const SearchMetaInitialState = {
    field: "",
    comparison: "is",
    value: "",
};

export interface ISearchMeta {
    field: string;
    comparison: string;
    value: string;
}

interface ISearchMetaFormBaseProps extends FormikProps<ISearchMeta> {}

const SearchMetaFormBase: FunctionComponent<ISearchMetaFormBaseProps> = ({
    values,
    errors,
    touched,
    submitForm,
    handleSubmit,
    handleChange,
    handleBlur,
    setFieldValue,
}) => {
    useEffect(() => {
        if (touched.value) {
            setTimeout(submitForm, 0);
        }
    }, [values.field]);

    const onChangeAutoCompleteInput = (item: any) => {
        const userID = get(item, "id");

        if (userID) {
            setFieldValue("value", userID);
            setTimeout(submitForm, 0);
        }
    };
    const onChangeTextInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = get(event, "currentTarget.value");
        if (value) {
            setFieldValue("value", value);
            setTimeout(submitForm, 0);
        }
    };

    const onChangeSelect = (e: React.ChangeEvent<HTMLSelectElement>) => {
        if (e.target.value === "context") setFieldValue("value", "");
        handleChange(e);
    };

    return (
        <form onSubmit={handleSubmit} className="f-search-contract-info">
            <div className="flex justify-between">
                <Select
                    name="field"
                    options={COMMON_META_FIELDS}
                    value={values.field}
                    className="f-search-contract-info__field"
                    marginBottom="none"
                    placeholder="Field"
                    error={errors && errors.field}
                    touched={touched && touched.field}
                    onChange={onChangeSelect}
                    onBlur={handleBlur}
                />
                {(() => {
                    switch (values.field) {
                        case "modified_by":
                        case "verification_by":
                        case "author_id":
                            return (
                                <AutocompleteInput
                                    placeholder="Search by person"
                                    className="f-search-contract-info__input"
                                    searchFor="users"
                                    onChange={onChangeAutoCompleteInput}
                                    value={values.value}
                                    isSetValue={true}
                                />
                            );
                        case "context":
                            return (
                                <Input
                                    placeholder="Value"
                                    className="f-search-contract-info__input"
                                    onChange={onChangeTextInput}
                                    value={values.value}
                                    marginBottom={"none"}
                                />
                            );
                        default:
                            return null;
                    }
                })()}
            </div>
        </form>
    );
};

export const SearchMetaForm: FunctionComponent<{
    onSubmit: (values: ISearchMeta, formikActions: FormikActions<ISearchMeta>) => void;
}> = ({ onSubmit }) => {
    return (
        <>
            <Formik
                initialValues={SearchMetaInitialState}
                onSubmit={onSubmit}
                validationSchema={SearchMetaFormSchema}
                render={(formikProps) => <SearchMetaFormBase {...formikProps} />}
            />
        </>
    );
};
