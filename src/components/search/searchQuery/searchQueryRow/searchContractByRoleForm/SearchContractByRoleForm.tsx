import React, { FunctionComponent, useEffect } from "react";
import { Formik, FormikProps, FormikActions } from "formik";
import * as Yup from "yup";
import { VALIDATION_ERRORS } from "../../../../../config";
import { Select, AutocompleteInput } from "../../../../ui/inputs";
import "./SearchContractByRoleForm.scss";
import get from "lodash.get";
import { useContractRoles } from "../../../../../hooks";
import { getContractRolesOptions, isNillOrEmpty } from "../../../../../utils";

const SearchContractByRoleFormSchema = Yup.object().shape({
    role: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    organizationID: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
});

export const SearchContractByRoleInitialState = {
    role: "",
    organizationID: "",
};

export interface ISearchContractByRole {
    role: string;
    organizationID: string;
}

interface ISearchContractByRoleFormBaseProps extends FormikProps<ISearchContractByRole> {}

const SearchContractByRoleFormBase: FunctionComponent<ISearchContractByRoleFormBaseProps> = ({
    values,
    errors,
    touched,
    submitForm,
    handleSubmit,
    handleBlur,
    setFieldValue,
}) => {
    const contractRoles = useContractRoles();
    const selectOptions = getContractRolesOptions(contractRoles);

    useEffect(() => {
        if (touched.organizationID) {
            setTimeout(submitForm, 0);
        }
    }, [values.role]);

    const onSelectRole = (event: React.ChangeEvent<HTMLSelectElement>) => {
        setFieldValue("role", event.currentTarget.value);
    };

    const onSelectOrganization = (item: any) => {
        setFieldValue("organizationID", get(item, "id"));
        setTimeout(submitForm, 0);
    };

    return (
        <form onSubmit={handleSubmit} className="f-search-contract-by-role">
            <div className="flex justify-between">
                <Select
                    name="role"
                    options={selectOptions}
                    value={values.role}
                    className="f-search-contract-by-role__field"
                    marginBottom="none"
                    placeholder="Role"
                    error={errors && errors.role}
                    touched={touched && touched.role}
                    onChange={onSelectRole}
                    onBlur={handleBlur}
                />
                <AutocompleteInput
                    className="f-search-contract-by-role__organization"
                    placeholder="Search organizations"
                    searchFor={"allOrganizations"}
                    onChange={onSelectOrganization}
                    disabled={isNillOrEmpty(values.role)}
                    isSetValue={true}
                />
            </div>
        </form>
    );
};

export const SearchContractByRoleForm: FunctionComponent<{
    onSubmit: (values: ISearchContractByRole, formikActions: FormikActions<ISearchContractByRole>) => void;
}> = ({ onSubmit }) => {
    return (
        <>
            <Formik
                initialValues={SearchContractByRoleInitialState}
                onSubmit={onSubmit}
                validationSchema={SearchContractByRoleFormSchema}
                render={(formikProps) => <SearchContractByRoleFormBase {...formikProps} />}
            />
        </>
    );
};
