import React, { FunctionComponent } from "react";
import "./SearchQuery.scss";
import { SearchQueryOptions } from "./searchQueryOptions";
import { SearchQueryRow } from "./searchQueryRow";

export const SearchQuery: FunctionComponent = () => {
    return (
        <div className="b-search-query">
            <SearchQueryRow />
            <SearchQueryOptions />
        </div>
    );
};
