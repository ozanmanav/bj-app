import { TSearchQueryReducerAction, searchQueryReducerInitialState, ISearchQueryReducerState } from "../searchQueryReducer";
import { createContext, Dispatch } from "react";

interface ISearchQueryContextProps {
    queryState: ISearchQueryReducerState;
    queryDispatch: Dispatch<TSearchQueryReducerAction>;
    queryBuildStep: number;
}

export const searchQueryContext = createContext<ISearchQueryContextProps>({
    queryBuildStep: 1,
    queryState: searchQueryReducerInitialState,
    queryDispatch: () => {},
});
export const SearchQueryContextProvider = searchQueryContext.Provider;
