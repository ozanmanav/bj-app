import { TSearchQueryReducerAction } from "./searchQueryReducerActions";
import { TActionQueries, TSearchByQueries, TByDatesQueries } from "../config";
import { ISearchInfo } from "../searchQueryRow/searchInfoForm";

const SEARCH_BY_MAX_OPTIONS = 2;

export type IQuery = ISearchInfo | string | undefined | any;

export interface ISearchBy {
    type: TSearchByQueries;
    query: IQuery;
}

export interface IByDates {
    type: TByDatesQueries | null;
    comparison: string;
    firstDate?: Date;
    secondDate?: Date;
}

export interface ISearchQueryReducerState {
    action: TActionQueries;
    logicOperator: string;
    searchBy: ISearchBy[];
    byDates: IByDates;
}

export const searchQueryReducerInitialState: ISearchQueryReducerState = {
    action: "search_people",
    searchBy: [],
    logicOperator: "AND",
    byDates: {
        type: null,
        comparison: "less_than",
    },
};

export function searchQueryReducer(state: ISearchQueryReducerState, action: TSearchQueryReducerAction) {
    switch (action.type) {
        case "SET_ACTION":
            return {
                ...searchQueryReducerInitialState,
                action: action.query,
            };
        case "SET_SEARCH_BY":
            const currentSearchBy = state.searchBy;
            if (currentSearchBy.length > 1) {
                return state;
            }

            const filteredSearchByType = currentSearchBy.filter(({ type }) => type === action.query);
            if (filteredSearchByType.length === 2) {
                return state;
            }

            return {
                ...state,
                searchBy: [
                    ...currentSearchBy,
                    {
                        type: filteredSearchByType.length > 0 ? `${action.query}_2` : action.query,
                        query: "",
                    },
                ].slice(-SEARCH_BY_MAX_OPTIONS),
            };
        case "UPDATE_SEARCH_BY_PERSON_INFO": {
            const searchByCopy = [...state.searchBy];

            const personInfoFirst = action.queryType === "person_info";

            const { field, comparison, value } = action.personInfoSearch;

            const updatedSearchBy = searchByCopy.map(({ type, query, ...item }) => ({
                ...item,
                type,
                query:
                    type === action.queryType
                        ? personInfoFirst
                            ? { field1: field, comparison1: comparison, value1: value }
                            : { field2: field, comparison2: comparison, value2: value }
                        : query,
            }));

            return {
                ...state,
                searchBy: updatedSearchBy,
            };
        }
        case "UPDATE_SEARCH_BY_PERSON_WORKS_FOR": {
            const searchByCopy = [...state.searchBy];
            const updatedSearchBy = searchByCopy.map(({ type, query, ...item }) => ({
                ...item,
                type,
                query: type === action.queryType ? action.personWorksFor : query,
            }));

            return {
                ...state,
                searchBy: updatedSearchBy,
            };
        }
        case "UPDATE_SEARCH_BY_PERSON_META": {
            const searchByCopy = [...state.searchBy];

            const { field, comparison, value } = action.personMetaSearch;

            let personMetaQuery = {};

            const personMetaFirst = action.queryType === "person_meta";
            if (personMetaFirst) {
                personMetaQuery = { field1: field, comparison1: comparison, value1: value };
            }

            const personMetaSecond = action.queryType === "person_meta_2";
            if (personMetaSecond) {
                personMetaQuery = { ...personMetaQuery, ...{ field2: field, comparison2: comparison, value2: value } };
            }

            const updatedSearchBy = searchByCopy.map(({ type, query, ...item }) => ({
                ...item,
                type,
                query: type === action.queryType ? personMetaQuery : query,
            }));

            return {
                ...state,
                searchBy: updatedSearchBy,
            };
        }
        case "UPDATE_SEARCH_BY_PERSON_ROLE": {
            const searchByCopy = [...state.searchBy];
            const updatedSearchBy = searchByCopy.map(({ type, query, ...item }) => ({
                ...item,
                type,
                query: type === action.queryType ? action.personSearchRoleID : query,
            }));

            return {
                ...state,
                searchBy: updatedSearchBy,
            };
        }
        case "UPDATE_SEARCH_BY_OBJECT_NAME": {
            const searchByCopy = [...state.searchBy];
            const updatedSearchBy = searchByCopy.map(({ type, query, ...item }) => ({
                ...item,
                type,
                query: type === action.queryType ? action.objectName : query,
            }));

            return {
                ...state,
                searchBy: updatedSearchBy,
            };
        }
        case "UPDATE_SEARCH_BY_OBJECT_TYPE": {
            const searchByCopy = [...state.searchBy];
            const updatedSearchBy = searchByCopy.map(({ type, query, ...item }) => ({
                ...item,
                type,
                query: type === action.queryType ? action.objectType : query,
            }));

            return {
                ...state,
                searchBy: updatedSearchBy,
            };
        }
        case "UPDATE_SEARCH_BY_OBJECT_INFO": {
            const searchByCopy = [...state.searchBy];

            const { field, comparison, value } = action.objectInfoSearch;

            let objectInfoQuery = {};

            const objectInfoFirst = action.queryType === "object_info";
            if (objectInfoFirst) {
                objectInfoQuery = { field1: field, comparison1: comparison, value1: value };
            }

            const objectInfoSecond = action.queryType === "object_info_2";
            if (objectInfoSecond) {
                objectInfoQuery = { ...objectInfoQuery, ...{ field2: field, comparison2: comparison, value2: value } };
            }

            const updatedSearchBy = searchByCopy.map(({ type, query, ...item }) => ({
                ...item,
                type,
                query: type === action.queryType ? objectInfoQuery : query,
            }));

            return {
                ...state,
                searchBy: updatedSearchBy,
            };
        }
        case "UPDATE_SEARCH_BY_OBJECT_META": {
            const searchByCopy = [...state.searchBy];

            const { field, comparison, value } = action.objectMetaSearch;

            let objectMetaQuery = {};

            const objectInfoFirst = action.queryType === "object_meta";
            if (objectInfoFirst) {
                objectMetaQuery = { field1: field, comparison1: comparison, value1: value };
            }

            const objectInfoSecond = action.queryType === "object_meta_2";
            if (objectInfoSecond) {
                objectMetaQuery = { ...objectMetaQuery, ...{ field2: field, comparison2: comparison, value2: value } };
            }

            const updatedSearchBy = searchByCopy.map(({ type, query, ...item }) => ({
                ...item,
                type,
                query: type === action.queryType ? objectMetaQuery : query,
            }));

            return {
                ...state,
                searchBy: updatedSearchBy,
            };
        }
        case "UPDATE_SEARCH_BY_OBJECT_DATE": {
            const searchByCopy = [...state.searchBy];

            const { date_field, date_comparison, date } = action.objectDateSearch;

            let objectDateQuery = {};

            const objectDateFirst = action.queryType === "object_date";
            if (objectDateFirst) {
                objectDateQuery = { date_field1: date_field, date_comparison1: date_comparison, date1: date };
            }

            const objectDateSecond = action.queryType === "object_date_2";
            if (objectDateSecond) {
                objectDateQuery = {
                    ...objectDateQuery,
                    ...{ date_field2: date_field, date_comparison2: date_comparison, date2: date },
                };
            }

            const updatedSearchBy = searchByCopy.map(({ type, query, ...item }) => ({
                ...item,
                type,
                query: type === action.queryType ? objectDateQuery : query,
            }));

            return {
                ...state,
                searchBy: updatedSearchBy,
            };
        }
        case "UPDATE_SEARCH_BY_OBJECT_RELATIONSHIP": {
            const searchByCopy = [...state.searchBy];
            const updatedSearchBy = searchByCopy.map(({ type, query, ...item }) => ({
                ...item,
                type,
                query: type === action.queryType ? action.objectRelationshipSearch : query,
            }));

            return {
                ...state,
                searchBy: updatedSearchBy,
            };
        }
        case "UPDATE_SEARCH_BY_FILE_TYPE": {
            const searchByCopy = [...state.searchBy];
            const updatedSearchBy = searchByCopy.map(({ type, query, ...item }) => ({
                ...item,
                type,
                query: type === action.queryType ? action.fileType : query,
            }));

            return {
                ...state,
                searchBy: updatedSearchBy,
            };
        }
        case "UPDATE_SEARCH_BY_FILE_INFO": {
            const searchByCopy = [...state.searchBy];

            const { field, comparison, value } = action.fileInfoSearch;

            let fileInfoQuery = {};

            const objectInfoFirst = action.queryType === "file_info";
            if (objectInfoFirst) {
                fileInfoQuery = { field1: field, comparison1: comparison, value1: value };
            }

            const objectInfoSecond = action.queryType === "file_info_2";
            if (objectInfoSecond) {
                fileInfoQuery = { ...fileInfoQuery, ...{ field2: field, comparison2: comparison, value2: value } };
            }

            const updatedSearchBy = searchByCopy.map(({ type, query, ...item }) => ({
                ...item,
                type,
                query: type === action.queryType ? fileInfoQuery : query,
            }));

            return {
                ...state,
                searchBy: updatedSearchBy,
            };
        }
        case "UPDATE_SEARCH_BY_FILE_META": {
            const searchByCopy = [...state.searchBy];

            const { field, comparison, value } = action.fileMetaSearch;

            let fileMetaQuery = {};

            const fileMetaFirst = action.queryType === "file_meta";
            if (fileMetaFirst) {
                fileMetaQuery = { field1: field, comparison1: comparison, value1: value };
            }

            const fileMetaSecond = action.queryType === "file_meta_2";
            if (fileMetaSecond) {
                fileMetaQuery = { ...fileMetaQuery, ...{ field2: field, comparison2: comparison, value2: value } };
            }

            const updatedSearchBy = searchByCopy.map(({ type, query, ...item }) => ({
                ...item,
                type,
                query: type === action.queryType ? fileMetaQuery : query,
            }));

            return {
                ...state,
                searchBy: updatedSearchBy,
            };
        }
        case "UPDATE_SEARCH_BY_CONTRACT_BY_ROLE": {
            const searchByCopy = [...state.searchBy];

            const { role, organizationID } = action.contractByRoleSearch;

            let contractByRoleQuery = {};

            const contractByRoleFirst = action.queryType === "contract_by_role";
            if (contractByRoleFirst) {
                contractByRoleQuery = { field1: "role_id", comparison1: "is", value1: role };
            }

            const contractByRoleSecond = action.queryType === "contract_by_role_2";
            if (contractByRoleSecond) {
                contractByRoleQuery = {
                    ...contractByRoleQuery,
                    ...{ field2: "role_id", comparison2: "is", value2: role },
                };
            }

            if (organizationID) {
                contractByRoleQuery = { ...contractByRoleQuery, organization_id: organizationID };
            }

            const updatedSearchBy = searchByCopy.map(({ type, query, ...item }) => ({
                ...item,
                type,
                query: type === action.queryType ? contractByRoleQuery : query,
            }));

            return {
                ...state,
                searchBy: updatedSearchBy,
            };
        }
        case "UPDATE_SEARCH_BY_CONTRACT_META": {
            const searchByCopy = [...state.searchBy];

            const { field, comparison, value } = action.contractMetaSearch;

            let contractMetaQuery = {};

            const contractMetaFirst = action.queryType === "contract_meta";
            if (contractMetaFirst) {
                contractMetaQuery = { field1: field, comparison1: comparison, value1: value };
            }

            const contractMetaSecond = action.queryType === "contract_meta_2";
            if (contractMetaSecond) {
                contractMetaQuery = {
                    ...contractMetaQuery,
                    ...{ field2: field, comparison2: comparison, value2: value },
                };
            }

            const updatedSearchBy = searchByCopy.map(({ type, query, ...item }) => ({
                ...item,
                type,
                query: type === action.queryType ? contractMetaQuery : query,
            }));

            return {
                ...state,
                searchBy: updatedSearchBy,
            };
        }
        case "UPDATE_SEARCH_BY_CONTRACT_DATE": {
            const searchByCopy = [...state.searchBy];

            const { date_field, date_comparison, date } = action.contractDateSearch;

            let contractDateQuery = {};

            const contractDateFirst = action.queryType === "contract_date";
            if (contractDateFirst) {
                contractDateQuery = { date_field1: date_field, date_comparison1: date_comparison, date1: date };
            }

            const contractDateSecond = action.queryType === "contract_date_2";
            if (contractDateSecond) {
                contractDateQuery = {
                    ...contractDateQuery,
                    ...{ date_field2: date_field, date_comparison2: date_comparison, date2: date },
                };
            }

            const updatedSearchBy = searchByCopy.map(({ type, query, ...item }) => ({
                ...item,
                type,
                query: type === action.queryType ? contractDateQuery : query,
            }));

            return {
                ...state,
                searchBy: updatedSearchBy,
            };
        }
        case "UPDATE_SEARCH_BY_EVENT_INFO": {
            const searchByCopy = [...state.searchBy];

            const { field, comparison, value } = action.eventInfoSearch;

            let eventInfoQuery = {};

            const eventInfoFirst = action.queryType === "event_info";
            if (eventInfoFirst) {
                eventInfoQuery = { field1: field, comparison1: comparison, value1: value };
            }

            const eventInfoSecond = action.queryType === "event_info_2";
            if (eventInfoSecond) {
                eventInfoQuery = { ...eventInfoQuery, ...{ field2: field, comparison2: comparison, value2: value } };
            }

            const updatedSearchBy = searchByCopy.map(({ type, query, ...item }) => ({
                ...item,
                type,
                query: type === action.queryType ? eventInfoQuery : query,
            }));

            return {
                ...state,
                searchBy: updatedSearchBy,
            };
        }
        case "UPDATE_SEARCH_BY_ORGANIZATION_INFO": {
            const searchByCopy = [...state.searchBy];

            const { field, comparison, value } = action.organizationInfoSearch;

            let organizationInfoQuery = {};

            const organizationInfoFirst = action.queryType === "organization_info";
            if (organizationInfoFirst) {
                organizationInfoQuery = { field1: field, comparison1: comparison, value1: value };
            }

            const organizationInfoSecond = action.queryType === "organization_info_2";
            if (organizationInfoSecond) {
                organizationInfoQuery = { ...organizationInfoQuery, ...{ field2: field, comparison2: comparison, value2: value } };
            }

            const updatedSearchBy = searchByCopy.map(({ type, query, ...item }) => ({
                ...item,
                type,
                query: type === action.queryType ? organizationInfoQuery : query,
            }));

            return {
                ...state,
                searchBy: updatedSearchBy,
            };
        }
        case "UPDATE_SEARCH_BY_ORGANIZATION_TYPE": {
            const searchByCopy = [...state.searchBy];
            const updatedSearchBy = searchByCopy.map(({ type, query, ...item }) => ({
                ...item,
                type,
                query: type === action.queryType ? action.organizationType : query,
            }));

            return {
                ...state,
                searchBy: updatedSearchBy,
            };
        }
        case "UPDATE_SEARCH_BY_ORGANIZATION_RELATIONSHIP": {
            const searchByCopy = [...state.searchBy];
            const updatedSearchBy = searchByCopy.map(({ type, query, ...item }) => ({
                ...item,
                type,
                query: type === action.queryType ? action.organizationRelationshipSearch : query,
            }));

            return {
                ...state,
                searchBy: updatedSearchBy,
            };
        }
        case "UPDATE_LOGIC_OPERATOR": {
            return {
                ...state,
                logicOperator: action.logicOperator,
            };
        }
        case "SET_BY_DATES":
            return {
                ...state,
                byDates: {
                    ...state.byDates,
                    type: action.query,
                },
            };
        case "SET_BY_DATES_PERIOD":
            return {
                ...state,
                byDates: {
                    ...state.byDates,
                    comparison: action.period,
                },
            };
        case "SET_BY_DATES_DATE":
            return {
                ...state,
                byDates: {
                    ...state.byDates,
                    [action.dateField]: action.date,
                },
            };
        case "REMOVE_SEARCH_BY_QUERY":
            return {
                ...state,
                searchBy: state.searchBy.filter(({ type }) => type !== action.queryType),
            };
        case "REMOVE_SEARCH_BY_DATES":
            return {
                ...state,
                byDates: searchQueryReducerInitialState.byDates,
            };
        case "RESET_SEARCH_STATE":
            return searchQueryReducerInitialState;
        default:
            return state;
    }
}
