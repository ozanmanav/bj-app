import { TActionQueries, TSearchByQueries, TByDatesQueries } from "../config";
import { ISearchContractByRole } from "../searchQueryRow/searchContractByRoleForm";
import { ISearchInfo } from "../searchQueryRow/searchInfoForm";
import { ISearchDate } from "../searchQueryRow/searchDateForm";
import { ISearchRelationship } from "../searchQueryRow/searchRelationshipForm";
import { ISearchOrganizationRelationship } from "../searchQueryRow/searchOrganizationRelationshipForm";

export interface ISearchPersonWorksFor {
    organizationID: string;
}

export type TSearchQueryReducerAction =
    | { type: "SET_ACTION"; query: TActionQueries }
    | { type: "SET_SEARCH_BY"; query: TSearchByQueries }
    | { type: "UPDATE_LOGIC_OPERATOR"; logicOperator: string }
    | { type: "UPDATE_SEARCH_BY_PERSON_INFO"; queryType: TSearchByQueries; personInfoSearch: ISearchInfo }
    | { type: "UPDATE_SEARCH_BY_PERSON_WORKS_FOR"; queryType: TSearchByQueries; personWorksFor: ISearchPersonWorksFor }
    | { type: "UPDATE_SEARCH_BY_PERSON_META"; queryType: TSearchByQueries; personMetaSearch: ISearchInfo }
    | { type: "UPDATE_SEARCH_BY_PERSON_ROLE"; queryType: TSearchByQueries; personSearchRoleID: string }
    | { type: "UPDATE_SEARCH_BY_OBJECT_NAME"; queryType: TSearchByQueries; objectName: string }
    | { type: "UPDATE_SEARCH_BY_OBJECT_TYPE"; queryType: TSearchByQueries; objectType: string }
    | { type: "UPDATE_SEARCH_BY_OBJECT_INFO"; queryType: TSearchByQueries; objectInfoSearch: ISearchInfo }
    | { type: "UPDATE_SEARCH_BY_OBJECT_META"; queryType: TSearchByQueries; objectMetaSearch: ISearchInfo }
    | { type: "UPDATE_SEARCH_BY_OBJECT_DATE"; queryType: TSearchByQueries; objectDateSearch: ISearchDate }
    | { type: "UPDATE_SEARCH_BY_OBJECT_RELATIONSHIP"; queryType: TSearchByQueries; objectRelationshipSearch: ISearchRelationship }
    | { type: "UPDATE_SEARCH_BY_FILE_TYPE"; queryType: TSearchByQueries; fileType: string }
    | { type: "UPDATE_SEARCH_BY_FILE_INFO"; queryType: TSearchByQueries; fileInfoSearch: ISearchInfo }
    | { type: "UPDATE_SEARCH_BY_FILE_META"; queryType: TSearchByQueries; fileMetaSearch: ISearchInfo }
    | { type: "UPDATE_SEARCH_BY_CONTRACT_BY_ROLE"; queryType: TSearchByQueries; contractByRoleSearch: ISearchContractByRole }
    | { type: "UPDATE_SEARCH_BY_CONTRACT_META"; queryType: TSearchByQueries; contractMetaSearch: ISearchInfo }
    | { type: "UPDATE_SEARCH_BY_CONTRACT_DATE"; queryType: TSearchByQueries; contractDateSearch: ISearchDate }
    | { type: "UPDATE_SEARCH_BY_EVENT_INFO"; queryType: TSearchByQueries; eventInfoSearch: ISearchInfo }
    | { type: "UPDATE_SEARCH_BY_EVENT_META"; queryType: TSearchByQueries; eventMetaSearch: ISearchInfo }
    | { type: "UPDATE_SEARCH_BY_ORGANIZATION_INFO"; queryType: TSearchByQueries; organizationInfoSearch: ISearchInfo }
    | { type: "UPDATE_SEARCH_BY_ORGANIZATION_TYPE"; queryType: TSearchByQueries; organizationType: string }
    | {
          type: "UPDATE_SEARCH_BY_ORGANIZATION_RELATIONSHIP";
          queryType: TSearchByQueries;
          organizationRelationshipSearch: ISearchOrganizationRelationship;
      }
    | { type: "REMOVE_SEARCH_BY_QUERY"; queryType: TSearchByQueries }
    | { type: "SET_BY_DATES"; query: TByDatesQueries }
    | { type: "SET_BY_DATES_PERIOD"; period: string }
    | { type: "SET_BY_DATES_DATE"; date: Date; dateField: "firstDate" | "secondDate" }
    | { type: "REMOVE_SEARCH_BY_DATES" }
    | { type: "RESET_SEARCH_STATE" };

export const setAction = (query: TActionQueries): TSearchQueryReducerAction => ({
    type: "SET_ACTION",
    query,
});

export const setSearchBy = (query: TSearchByQueries): TSearchQueryReducerAction => {
    return {
        type: "SET_SEARCH_BY",
        query,
    };
};

export const updateSearchByPersonInfo = (queryType: TSearchByQueries, personInfoSearch: ISearchInfo): TSearchQueryReducerAction => ({
    type: "UPDATE_SEARCH_BY_PERSON_INFO",
    queryType,
    personInfoSearch,
});

export const updateSearchByPersonWorksFor = (
    queryType: TSearchByQueries,
    personWorksFor: ISearchPersonWorksFor
): TSearchQueryReducerAction => ({
    type: "UPDATE_SEARCH_BY_PERSON_WORKS_FOR",
    queryType,
    personWorksFor,
});

export const updateSearchByPersonMeta = (queryType: TSearchByQueries, personMetaSearch: ISearchInfo): TSearchQueryReducerAction => ({
    type: "UPDATE_SEARCH_BY_PERSON_META",
    queryType,
    personMetaSearch,
});

export const updateSearchByPersonRole = (queryType: TSearchByQueries, personSearchRoleID: string): TSearchQueryReducerAction => ({
    type: "UPDATE_SEARCH_BY_PERSON_ROLE",
    queryType,
    personSearchRoleID,
});

export const updateSearchByObjectName = (queryType: TSearchByQueries, objectName: string): TSearchQueryReducerAction => ({
    type: "UPDATE_SEARCH_BY_OBJECT_NAME",
    queryType,
    objectName,
});

export const updateSearchByObjectType = (queryType: TSearchByQueries, objectType: string): TSearchQueryReducerAction => ({
    type: "UPDATE_SEARCH_BY_OBJECT_TYPE",
    queryType,
    objectType,
});

export const updateSearchByObjectInfo = (queryType: TSearchByQueries, objectInfoSearch: ISearchInfo): TSearchQueryReducerAction => ({
    type: "UPDATE_SEARCH_BY_OBJECT_INFO",
    queryType,
    objectInfoSearch,
});

export const updateSearchByObjectMeta = (queryType: TSearchByQueries, objectMetaSearch: ISearchInfo): TSearchQueryReducerAction => ({
    type: "UPDATE_SEARCH_BY_OBJECT_META",
    queryType,
    objectMetaSearch,
});

export const updateSearchByObjectDate = (queryType: TSearchByQueries, objectDateSearch: ISearchDate): TSearchQueryReducerAction => ({
    type: "UPDATE_SEARCH_BY_OBJECT_DATE",
    queryType,
    objectDateSearch,
});

export const updateSearchByObjectRelationship = (
    queryType: TSearchByQueries,
    objectRelationshipSearch: ISearchRelationship
): TSearchQueryReducerAction => ({
    type: "UPDATE_SEARCH_BY_OBJECT_RELATIONSHIP",
    queryType,
    objectRelationshipSearch,
});

export const updateSearchByFileType = (queryType: TSearchByQueries, fileType: string): TSearchQueryReducerAction => ({
    type: "UPDATE_SEARCH_BY_FILE_TYPE",
    queryType,
    fileType,
});

export const updateSearchByFileInfo = (queryType: TSearchByQueries, fileInfoSearch: ISearchInfo): TSearchQueryReducerAction => ({
    type: "UPDATE_SEARCH_BY_FILE_INFO",
    queryType,
    fileInfoSearch,
});

export const updateSearchByFileMeta = (queryType: TSearchByQueries, fileMetaSearch: ISearchInfo): TSearchQueryReducerAction => ({
    type: "UPDATE_SEARCH_BY_FILE_META",
    queryType,
    fileMetaSearch,
});

export const updateSearchByContractByRole = (
    queryType: TSearchByQueries,
    contractByRoleSearch: ISearchContractByRole
): TSearchQueryReducerAction => ({
    type: "UPDATE_SEARCH_BY_CONTRACT_BY_ROLE",
    queryType,
    contractByRoleSearch,
});

export const updateSearchByContractMeta = (queryType: TSearchByQueries, contractMetaSearch: ISearchInfo): TSearchQueryReducerAction => ({
    type: "UPDATE_SEARCH_BY_CONTRACT_META",
    queryType,
    contractMetaSearch,
});

export const updateSearchByContractDate = (queryType: TSearchByQueries, contractDateSearch: ISearchDate): TSearchQueryReducerAction => ({
    type: "UPDATE_SEARCH_BY_CONTRACT_DATE",
    queryType,
    contractDateSearch,
});

export const updateSearchByEventInfo = (queryType: TSearchByQueries, eventInfoSearch: ISearchInfo): TSearchQueryReducerAction => ({
    type: "UPDATE_SEARCH_BY_EVENT_INFO",
    queryType,
    eventInfoSearch,
});

export const updateSearchByEventMeta = (queryType: TSearchByQueries, eventMetaSearch: ISearchInfo): TSearchQueryReducerAction => ({
    type: "UPDATE_SEARCH_BY_EVENT_META",
    queryType,
    eventMetaSearch,
});

export const updateSearchByOrganizationInfo = (
    queryType: TSearchByQueries,
    organizationInfoSearch: ISearchInfo
): TSearchQueryReducerAction => ({
    type: "UPDATE_SEARCH_BY_ORGANIZATION_INFO",
    queryType,
    organizationInfoSearch,
});

export const updateSearchByOrganizationType = (queryType: TSearchByQueries, organizationType: string): TSearchQueryReducerAction => ({
    type: "UPDATE_SEARCH_BY_ORGANIZATION_TYPE",
    queryType,
    organizationType,
});

export const updateSearchByOrganizationRelationship = (
    queryType: TSearchByQueries,
    organizationRelationshipSearch: ISearchOrganizationRelationship
): TSearchQueryReducerAction => ({
    type: "UPDATE_SEARCH_BY_ORGANIZATION_RELATIONSHIP",
    queryType,
    organizationRelationshipSearch,
});

export const updateSearchByLogicOperator = (logicOperator: string): TSearchQueryReducerAction => ({
    type: "UPDATE_LOGIC_OPERATOR",
    logicOperator,
});

export const setByDates = (query: TByDatesQueries): TSearchQueryReducerAction => ({
    type: "SET_BY_DATES",
    query,
});

export const setByDatesPeriod = (period: string): TSearchQueryReducerAction => ({
    type: "SET_BY_DATES_PERIOD",
    period,
});

export const updateDate = (date: Date, dateField: "firstDate" | "secondDate"): TSearchQueryReducerAction => ({
    type: "SET_BY_DATES_DATE",
    date,
    dateField,
});

export const removeSearchByQuery = (queryType: TSearchByQueries): TSearchQueryReducerAction => ({
    type: "REMOVE_SEARCH_BY_QUERY",
    queryType,
});

export const removeSearchByDates = (): TSearchQueryReducerAction => ({
    type: "REMOVE_SEARCH_BY_DATES",
});

export const resetSearchState = (): TSearchQueryReducerAction => ({
    type: "RESET_SEARCH_STATE",
});
