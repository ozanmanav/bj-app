import React, { ButtonHTMLAttributes, FunctionComponent, useContext } from "react";
import "./SearchQueryOptions.scss";
import { Container, Button } from "../../../ui";
import { TActionQueries, ACTIONS, TByDatesQueries, BY_DATES, SEARCH_BY, TSearchByQueries } from "../config";
import { TSearchQueryReducerAction, setAction, setSearchBy, setByDates } from "../searchQueryReducer";
import { searchQueryContext } from "../searchQueryContext";
import { splitAndCapitalizeFirstLetter } from "../../../../utils/stringHelpers";
import get from "lodash.get";
import { isNillOrEmpty } from "../../../../utils";

export const SearchQueryOptions: FunctionComponent = () => {
    const { queryBuildStep, queryState } = useContext(searchQueryContext);

    const firstQueryValue = get(queryState, "searchBy[0].query");

    return (
        <div className="b-search-options">
            <Container className="flex">
                <div className="b-search-options__group _search-options">
                    <h3 className="b-search-options__group-title">I want to search...</h3>
                    <div className="flex flex-wrap">
                        {ACTIONS.map((action) => (
                            <QueryButton {...action} key={action.query} actionCreator={setAction} />
                        ))}
                    </div>
                </div>
                <div className="b-search-options__group _search-by">
                    <h3 className="b-search-options__group-title">{`For the ${splitAndCapitalizeFirstLetter(queryState.action)}...`}</h3>
                    <div className="flex flex-wrap">
                        {SEARCH_BY[queryState.action].map((searchBy) => (
                            <QueryButton {...searchBy} key={searchBy.query} actionCreator={setSearchBy} disabled={queryBuildStep < 2} />
                        ))}
                    </div>
                </div>
                <div className="b-search-options__group">
                    <h3 className="b-search-options__group-title">Around the dates...</h3>
                    <div className="flex flex-wrap">
                        {BY_DATES.map((date) => (
                            <QueryButton
                                {...date}
                                key={date.query}
                                actionCreator={setByDates}
                                disabled={isNillOrEmpty(firstQueryValue) || queryBuildStep < 3}
                            />
                        ))}
                    </div>
                </div>
            </Container>
        </div>
    );
};

type TQueries = TActionQueries | TSearchByQueries | TByDatesQueries;

interface IQueryButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    query: TQueries;
    label: string;
    actionCreator: (query: any) => TSearchQueryReducerAction;
}

const QueryButton: FunctionComponent<IQueryButtonProps> = ({ label, query, actionCreator, ...props }) => {
    const { queryDispatch } = useContext(searchQueryContext);

    function onClick() {
        queryDispatch(actionCreator(query));
    }

    return <Button text={label} className="b-search-options__option" onClick={onClick} {...props} />;
};
