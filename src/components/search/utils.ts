import { IByDates, ISearchPersonWorksFor, ISearchQueryReducerState } from "./searchQuery/searchQueryReducer";
import { handleGraphQLErrors, showErrorToast } from "../ui/toasts";
import ApolloClient from "apollo-client";
import { IGraphQLError } from "../../config/types";
import { flattenDeep, isNillOrEmpty } from "../../utils";
import get from "lodash.get";
import { formatDate } from "../ui/datepicker/utils";
import { isEmpty, isNil } from "ramda";
import { GET_OBJECT_QUERY, SEARCH_PEOPLE_QUERY, FILE_PEOPLE_QUERY, CONTRACT_PEOPLE_QUERY, EVENT_QUERY, ORGANIZATION_QUERY } from "./config";
import { ISearchContractByRole } from "./searchQuery/searchQueryRow/searchContractByRoleForm";
import { ISearchInfo } from "./searchQuery/searchQueryRow/searchInfoForm";
import { DATE_FORMAT } from "../../config";

const GRAPHQL_SEARCH_QUERIES = {
    search_objects: GET_OBJECT_QUERY,
    search_people: SEARCH_PEOPLE_QUERY,
    search_files: FILE_PEOPLE_QUERY,
    search_contracts: CONTRACT_PEOPLE_QUERY,
    search_events: EVENT_QUERY,
    search_organizations: ORGANIZATION_QUERY,
};

export async function performSearch(
    queryState: ISearchQueryReducerState,
    apolloClient: ApolloClient<any>,
    isAdmin: boolean
): Promise<{ user: any[]; admin: any[] }> {
    const query = getSearchGraphQLQuery(queryState);
    const variables = getSearchGraphQLQueryVariables(queryState);

    if (isNillOrEmpty(variables) || isNillOrEmpty(queryState.searchBy[0].query)) {
        return {
            user: [],
            admin: [],
        };
    }

    const requests = [
        apolloClient.query({
            query,
            variables,
        }),
    ];

    // if (isAdmin) {
    //     requests.push(
    //         apolloClient.query({
    //             query,
    //             variables: {
    //                 ...variables,
    //                 admin: true, // serves no purpose, just allowing apollo to differentiate with the previous one
    //             },
    //             ...GRAPHQL_ADMIN_CONTEXT,
    //         })
    //     );
    // }

    try {
        const responses = await Promise.all(requests);

        const errors = responses.map(({ errors }) => errors).filter(Boolean);

        if (errors.length > 0) {
            handleGraphQLErrors(flattenDeep(errors) as IGraphQLError[]);
            return {
                user: [],
                admin: [],
            };
        }

        return {
            user: get(responses, `[0].data.${queryState.action}.data`) || [],
            admin: [],
        };
    } catch (e) {
        showErrorToast(e.message);
        return {
            user: [],
            admin: [],
        };
    }
}

function getSearchGraphQLQuery(queryState: ISearchQueryReducerState) {
    const { action } = queryState;

    if (!action || !GRAPHQL_SEARCH_QUERIES[action]) {
        const NO_ACTION_ERROR_MESSAGE = "No such action";
        showErrorToast(NO_ACTION_ERROR_MESSAGE);
        console.error(NO_ACTION_ERROR_MESSAGE);
        return;
    }

    return GRAPHQL_SEARCH_QUERIES[action];
}

function getSearchGraphQLQueryVariables(queryState: ISearchQueryReducerState): object | undefined {
    const searchBy = get(queryState, "searchBy");

    if (isEmpty(searchBy) || isNil(searchBy)) {
        return {};
    }

    const dates = queryState.byDates;
    const datesQuery = getDatesSearchQuery(dates);

    if (queryState.action === "search_people") {
        const personMetaSearchBy = searchBy.some(({ type }) => type === "person_meta");
        if (personMetaSearchBy) {
            const firstPersonMetaSearchBy = searchBy.find(({ type }) => type === "person_meta");
            const firstPersonMetaSearchByQuery = get(firstPersonMetaSearchBy, "query") as ISearchInfo;

            const secondPersonMetaSearchBy = searchBy.find(({ type }) => type === "person_meta_2");
            const secondPersonMetaSearchByQuery = get(secondPersonMetaSearchBy, "query") as ISearchInfo;

            return {
                ...datesQuery,
                ...firstPersonMetaSearchByQuery,
                ...secondPersonMetaSearchByQuery,
                ...(secondPersonMetaSearchByQuery && { condition: queryState.logicOperator }),
            };
        }

        const personByRoleSearchBy = searchBy.find(({ type }) => type === "person_by_role");
        const personByRoleSearchByQuery = get(personByRoleSearchBy, "query") as string;

        const firstPersonInfoSearchBy = searchBy.find(({ type }) => type === "person_info");
        const firstPersonInfoSearchByQuery = get(firstPersonInfoSearchBy, "query") as ISearchInfo;

        const secondPersonInfoSearchBy = searchBy.find(({ type }) => type === "person_info_2");
        const secondPersonInfoSearchByQuery = get(secondPersonInfoSearchBy, "query") as ISearchInfo;

        const personWorksForSearchBy = searchBy.find(({ type }) => type === "person_works_for");
        const personWorksForSearchByQuery = get(personWorksForSearchBy, "query") as ISearchPersonWorksFor;

        return {
            ...datesQuery,
            ...firstPersonInfoSearchByQuery,
            ...secondPersonInfoSearchByQuery,
            ...((secondPersonInfoSearchByQuery || personWorksForSearchByQuery) && {
                condition: queryState.logicOperator,
            }),
            ...(personWorksForSearchByQuery && { organization_id: personWorksForSearchByQuery.organizationID }),
            ...(personByRoleSearchByQuery && { role_id: personByRoleSearchByQuery }),
        };
    }

    if (queryState.action === "search_objects") {
        const objectNameSearchBy = searchBy.find(({ type }) => type === "object_name");

        if (objectNameSearchBy) {
            const objectNameSearchByQuery = get(objectNameSearchBy, "query") as string;

            return { field1: "name", comparison1: "is", value1: objectNameSearchByQuery };
        }

        const objectRelationshipSearchBy = searchBy.find(({ type }) => type === "object_relationship");
        const relationshipType = get(objectRelationshipSearchBy, "query.relationshipType");
        const objectID = get(objectRelationshipSearchBy, "query.objectID");

        if (objectRelationshipSearchBy && !isNillOrEmpty(relationshipType)) {
            const relationTypeSplit = objectRelationshipSearchBy.query.relationshipType.split("/_(.*)/");

            const parentType = `${relationTypeSplit[0]}_id`;
            const typeID = relationTypeSplit[1];

            return { relationship: { type_id: typeID, level: parentType, object_id: objectID } };
        }

        const objectInfoSearchBy = searchBy.some(({ type }) => type === "object_info");
        if (objectInfoSearchBy) {
            const firstObjectInfoSearchBy = searchBy.find(({ type }) => type === "object_info");
            const firstObjectInfoSearchByQuery = get(firstObjectInfoSearchBy, "query") as ISearchInfo;

            const secondObjectInfoSearchBy = searchBy.find(({ type }) => type === "object_info_2");
            const secondObjectInfoSearchByQuery = get(secondObjectInfoSearchBy, "query") as ISearchInfo;

            return {
                ...datesQuery,
                ...firstObjectInfoSearchByQuery,
                ...secondObjectInfoSearchByQuery,
                ...(secondObjectInfoSearchByQuery && { condition: queryState.logicOperator }),
            };
        }

        const objectMetaSearchBy = searchBy.some(({ type }) => type === "object_meta");
        if (objectMetaSearchBy) {
            const firstObjectMetaSearchBy = searchBy.find(({ type }) => type === "object_meta");
            const firstObjectMetaSearchByQuery = get(firstObjectMetaSearchBy, "query") as ISearchInfo;

            const secondObjectMetaSearchBy = searchBy.find(({ type }) => type === "object_meta_2");
            const secondObjectMetaSearchByQuery = get(secondObjectMetaSearchBy, "query") as ISearchInfo;

            return {
                ...datesQuery,
                ...firstObjectMetaSearchByQuery,
                ...secondObjectMetaSearchByQuery,
                ...(secondObjectMetaSearchByQuery && { condition: queryState.logicOperator }),
            };
        }

        const objectDateSearchBy = searchBy.some(({ type }) => type === "object_date");
        if (objectDateSearchBy) {
            const firstObjectDateSearchBy = searchBy.find(({ type }) => type === "object_date");
            const firstObjectDateSearchByQuery = get(firstObjectDateSearchBy, "query") as ISearchInfo;

            const secondObjectDateSearchBy = searchBy.find(({ type }) => type === "object_date_2");
            const secondObjectDateSearchByQuery = get(secondObjectDateSearchBy, "query") as ISearchInfo;

            return {
                ...datesQuery,
                ...firstObjectDateSearchByQuery,
                ...secondObjectDateSearchByQuery,
                ...(secondObjectDateSearchByQuery && { condition: queryState.logicOperator }),
            };
        }
    }

    if (queryState.action === "search_files") {
        const fileInfoSearchBy = searchBy.some(({ type }) => type === "file_info");
        if (fileInfoSearchBy) {
            let firstFileInfoSearchByQuery, secondFileInfoSearchByQuery;
            if (fileInfoSearchBy) {
                const firstFileInfoSearchBy = searchBy.find(({ type }) => type === "file_info");
                firstFileInfoSearchByQuery = get(firstFileInfoSearchBy, "query") as ISearchInfo;

                const secondFileInfoSearchBy = searchBy.find(({ type }) => type === "file_info_2");
                secondFileInfoSearchByQuery = get(secondFileInfoSearchBy, "query") as ISearchInfo;
            }

            const fileTypeSearchBy = searchBy.some(({ type }) => type === "file_type");
            let firstFileTypeSearchByQuery, secondFileTypeSearchByQuery, firstFileType, secondFileType;
            if (fileTypeSearchBy) {
                const firstFileTypeSearchBy = searchBy.find(({ type }) => type === "file_type");
                firstFileType = get(firstFileTypeSearchBy, "query") as string;
                firstFileTypeSearchByQuery = { field1: "type", comparison1: "is", value1: firstFileType };

                const secondFileTypeSearchBy = searchBy.find(({ type }) => type === "file_type_2");
                secondFileType = get(secondFileTypeSearchBy, "query") as string;
                secondFileTypeSearchByQuery = { field2: "type", comparison2: "is", value2: secondFileType };
            }

            return {
                ...datesQuery,
                ...firstFileInfoSearchByQuery,
                ...secondFileInfoSearchByQuery,
                ...(firstFileType && firstFileTypeSearchByQuery),
                ...(secondFileType && secondFileTypeSearchByQuery),
                ...(queryState.searchBy.length > 1 && { condition: queryState.logicOperator }),
            };
        }

        const fileTypeSearchBy = searchBy.some(({ type }) => type === "file_type");
        let firstFileTypeSearchByQuery, secondFileTypeSearchByQuery, firstFileType, secondFileType;
        if (fileTypeSearchBy) {
            const firstFileTypeSearchBy = searchBy.find(({ type }) => type === "file_type");
            firstFileType = get(firstFileTypeSearchBy, "query") as string;
            firstFileTypeSearchByQuery = { field1: "type", comparison1: "is", value1: firstFileType };

            const secondFileTypeSearchBy = searchBy.find(({ type }) => type === "file_type_2");
            secondFileType = get(secondFileTypeSearchBy, "query") as string;
            secondFileTypeSearchByQuery = { field2: "type", comparison2: "is", value2: secondFileType };

            return {
                ...datesQuery,
                ...(firstFileType && firstFileTypeSearchByQuery),
                ...(secondFileType && secondFileTypeSearchByQuery),
                ...(queryState.searchBy.length > 1 && { condition: queryState.logicOperator }),
            };
        }

        const fileMetaSearchBy = searchBy.some(({ type }) => type === "file_meta");
        if (fileMetaSearchBy) {
            const firstFileMetaSearchBy = searchBy.find(({ type }) => type === "file_meta");
            const firstFileMetaSearchByQuery = get(firstFileMetaSearchBy, "query") as ISearchInfo;

            const secondFileMetaSearchBy = searchBy.find(({ type }) => type === "file_meta_2");
            const secondFileMetaSearchByQuery = get(secondFileMetaSearchBy, "query") as ISearchInfo;

            return {
                ...datesQuery,
                ...firstFileMetaSearchByQuery,
                ...secondFileMetaSearchByQuery,
                ...(secondFileMetaSearchByQuery && { condition: queryState.logicOperator }),
            };
        }
    }

    if (queryState.action === "search_contracts") {
        const contractByRoleSearchBy = searchBy.some(({ type }) => type === "contract_by_role");
        if (contractByRoleSearchBy) {
            const firstContractByRoleSearchBy = searchBy.find(({ type }) => type === "contract_by_role");
            const firstContractByRoleSearchByQuery = get(firstContractByRoleSearchBy, "query") as ISearchContractByRole;

            const secondContractByRoleSearchBy = searchBy.find(({ type }) => type === "contract_by_role_2");
            const secondContractByRoleSearchByQuery = get(secondContractByRoleSearchBy, "query") as ISearchContractByRole;

            return {
                ...datesQuery,
                ...firstContractByRoleSearchByQuery,
                ...secondContractByRoleSearchByQuery,
                ...(secondContractByRoleSearchByQuery && { condition: queryState.logicOperator }),
            };
        }

        const contractMetaSearchBy = searchBy.some(({ type }) => type === "contract_meta");
        if (contractMetaSearchBy) {
            const firstContractMetaSearchBy = searchBy.find(({ type }) => type === "contract_meta");
            const firstContractMetaSearchByQuery = get(firstContractMetaSearchBy, "query") as ISearchInfo;

            const secondContractMetaSearchBy = searchBy.find(({ type }) => type === "contract_meta_2");
            const secondContractMetaSearchByQuery = get(secondContractMetaSearchBy, "query") as ISearchInfo;

            return {
                ...datesQuery,
                ...firstContractMetaSearchByQuery,
                ...secondContractMetaSearchByQuery,
                ...(secondContractMetaSearchByQuery && { condition: queryState.logicOperator }),
            };
        }

        const contractDateSearchBy = searchBy.some(({ type }) => type === "contract_date");
        if (contractDateSearchBy) {
            const firstContractDateSearchBy = searchBy.find(({ type }) => type === "contract_date");
            const firstContractDateSearchByQuery = get(firstContractDateSearchBy, "query") as ISearchInfo;

            const secondContractDateSearchBy = searchBy.find(({ type }) => type === "contract_date_2");
            const secondContractDateSearchByQuery = get(secondContractDateSearchBy, "query") as ISearchInfo;

            return {
                ...datesQuery,
                ...firstContractDateSearchByQuery,
                ...secondContractDateSearchByQuery,
                ...(secondContractDateSearchByQuery && { condition: queryState.logicOperator }),
            };
        }
    }

    if (queryState.action === "search_events") {
        const eventInfoSearchBy = searchBy.some(({ type }) => type === "event_info");
        if (eventInfoSearchBy) {
            const firstEventInfoSearchBy = searchBy.find(({ type }) => type === "event_info");
            const firstEventInfoSearchByQuery = get(firstEventInfoSearchBy, "query") as ISearchInfo;

            const secondEventInfoSearchBy = searchBy.find(({ type }) => type === "event_info_2");
            const secondEventInfoSearchByQuery = get(secondEventInfoSearchBy, "query") as ISearchInfo;

            return {
                ...datesQuery,
                ...firstEventInfoSearchByQuery,
                ...secondEventInfoSearchByQuery,
                ...(secondEventInfoSearchByQuery && { condition: queryState.logicOperator }),
            };
        }

        const eventMetaSearchBy = searchBy.some(({ type }) => type === "event_meta");
        if (eventMetaSearchBy) {
            const firstEventMetaSearchBy = searchBy.find(({ type }) => type === "event_meta");
            const firstEventMetaSearchByQuery = get(firstEventMetaSearchBy, "query") as ISearchInfo;

            const secondEventMetaSearchBy = searchBy.find(({ type }) => type === "event_meta_2");
            const secondEventMetaSearchByQuery = get(secondEventMetaSearchBy, "query") as ISearchInfo;

            return {
                ...datesQuery,
                ...firstEventMetaSearchByQuery,
                ...secondEventMetaSearchByQuery,
                ...(secondEventMetaSearchByQuery && { condition: queryState.logicOperator }),
            };
        }
    }

    if (queryState.action === "search_organizations") {
        const organizationInfoSearchBy = searchBy.some(({ type }) => type === "organization_info");
        if (organizationInfoSearchBy) {
            const firstOrganizationInfoSearchBy = searchBy.find(({ type }) => type === "organization_info");
            const firstOrganizationInfoSearchByQuery = get(firstOrganizationInfoSearchBy, "query") as ISearchInfo;

            const secondOrganizationInfoSearchBy = searchBy.find(({ type }) => type === "organization_info_2");
            const secondOrganizationInfoSearchByQuery = get(secondOrganizationInfoSearchBy, "query") as ISearchInfo;

            return {
                ...datesQuery,
                ...firstOrganizationInfoSearchByQuery,
                ...secondOrganizationInfoSearchByQuery,
                ...(secondOrganizationInfoSearchByQuery && { condition: queryState.logicOperator }),
            };
        }

        const organizationTypeSearchBy = searchBy.some(({ type }) => type === "organization_type");
        let firstOrganizationTypeSearchByQuery, secondOrganizationTypeSearchByQuery, firstOrganizationType, secondOrganizationType;
        if (organizationTypeSearchBy) {
            const firstFileTypeSearchBy = searchBy.find(({ type }) => type === "organization_type");
            firstOrganizationType = get(firstFileTypeSearchBy, "query") as string;
            firstOrganizationTypeSearchByQuery = { field1: "type_id", comparison1: "is", value1: firstOrganizationType };

            const secondFileTypeSearchBy = searchBy.find(({ type }) => type === "organization_type_2");
            secondOrganizationType = get(secondFileTypeSearchBy, "query") as string;
            secondOrganizationTypeSearchByQuery = { field2: "type_id", comparison2: "is", value2: secondOrganizationType };

            return {
                ...datesQuery,
                ...(firstOrganizationType && firstOrganizationTypeSearchByQuery),
                ...(secondOrganizationType && secondOrganizationTypeSearchByQuery),
                ...(queryState.searchBy.length > 1 && { condition: queryState.logicOperator }),
            };
        }

        const organizationRelationshipSearchBy = searchBy.find(({ type }) => type === "organization_relationship");
        const organizationRelationshipType = get(organizationRelationshipSearchBy, "query.organizationRelationshipType");
        const organizationID = get(organizationRelationshipSearchBy, "query.organizationID");

        if (organizationRelationshipSearchBy && !isNillOrEmpty(organizationRelationshipType)) {
            return {
                relationship: {
                    level: get(organizationRelationshipSearchBy, "query.organizationRelationshipType"),
                    organization_id: organizationID,
                },
            };
        }
    }
}

function getDatesSearchQuery(dates: IByDates) {
    let formattedFirstDate;

    const { firstDate, comparison, type } = dates;
    if (comparison === "between" && firstDate) {
        const { secondDate } = dates;
        formattedFirstDate = formatDate(firstDate, DATE_FORMAT);
        if (secondDate) {
            const formattedSecondDate = formatDate(secondDate, DATE_FORMAT);

            return {
                ...{ date1: formattedFirstDate, date_field1: type, date_comparison1: "greater_than" },
                ...{ date2: formattedSecondDate, date_field2: type, date_comparison2: "less_than" },
            };
        }
    }

    if (firstDate) {
        formattedFirstDate = formatDate(firstDate, DATE_FORMAT);

        return {
            ...(formattedFirstDate && { date1: formattedFirstDate, date_field1: type, date_comparison1: comparison }),
        };
    }
}

// function getDatesObjectByDatePeriod(dates: IByDates) {
//     const { firstDate, secondDate, comparison } = dates;

//     // adding 12h because dates from day picker have noon time
//     const formattedFirstDate = firstDate ? addHours(firstDate, -12).toISOString() : null;

//     if (comparison === "equal") {
//         return {
//             firstDate: formattedFirstDate,
//             // add one day to search from date one start to date one end
//             secondDate: addDays(parseDate(formattedFirstDate || ""), 1),
//         };
//     }

//     if (comparison === "before") {
//         return {
//             firstDate: null,
//             secondDate: formattedFirstDate,
//         };
//     }
//     console.log({
//         firstDate: formattedFirstDate,
//         // removing 12h because dates from day picker have noon time
//         secondDate: secondDate && comparison === "between" ? addHours(secondDate, 12).toISOString() : null,
//     });
//     // after or between date period
//     return {
//         firstDate: formattedFirstDate,
//         // removing 12h because dates from day picker have noon time
//         secondDate: secondDate && comparison === "between" ? addHours(secondDate, 12).toISOString() : null,
//     };
// }

export const isStringType = (value: any) => {
    return typeof value === "string";
};
