import React, { FunctionComponent, useContext } from "react";
import "./SearchResults.scss";
import { Container, ExportButton, SortButton, ObjectName, ObjectNameSimple } from "../../ui";
import get from "lodash.get";
import { useSortingTable } from "../../../hooks";
import orderBy from "lodash.orderby";
import { splitAndCapitalizeFirstLetter } from "../../../utils/stringHelpers";
import { TActionQueries } from "../searchQuery/config";
import { Loader } from "../../ui/loader";
import { searchQueryContext } from "../searchQuery/searchQueryContext";
import { searchQueryReducerInitialState } from "../searchQuery/searchQueryReducer";
import { PersonName } from "../../personName/PersonName";
import { EventName } from "../../eventName";
import { FileName } from "../../fileName";
import { ContractName } from "../../contractName/ContractName";
import { IOrganization, IAddress } from "../../../config";
import { FilesList } from "../../filesList";
import { isAfter, format } from "date-fns";
import { OrganizationName } from "../../organizationName";
import { getSearchOrganizationAddressString } from "./utils";

interface ISearchResultsTableProps {
    results: any[];
    actionName: string;
}

interface ISearchResultsProps {
    results: any[];
    action: TActionQueries;
    showOnlyNumberOfObjects: boolean;
    isLoading: boolean;
}

const SORT_STRINGS = {
    NAME: "name",
};

export const SearchResults: FunctionComponent<ISearchResultsProps> = ({ action, results, showOnlyNumberOfObjects, isLoading }) => {
    const { queryState } = useContext(searchQueryContext);

    const actionName = splitAndCapitalizeFirstLetter(action);
    return (
        <Container className="b-search-results">
            {isLoading && <Loader />}
            {results.length > 0 ? (
                <>
                    <div className="flex align-center justify-between b-search-results__header">
                        <h3 className="flex align-center">
                            {actionName} matching the search
                            <span className="_text-grey h5 flex justify-center align-center b-search-results__header-amount">
                                {results.length}
                            </span>
                        </h3>
                        {!showOnlyNumberOfObjects && <ExportButton text="Export results" primary={false} />}
                    </div>
                    {!showOnlyNumberOfObjects && <SearchResultsTable results={results} actionName={actionName} />}
                </>
            ) : (
                !isLoading && queryState !== searchQueryReducerInitialState && <h3>No {actionName} matching the search</h3>
            )}
        </Container>
    );
};

const SearchResultsTable: FunctionComponent<ISearchResultsTableProps> = ({ results, actionName }) => {
    const { sortBy, sortingOrder, getSortOrderForField, changeSortBy } = useSortingTable({ localStorageKey: "search" });
    const { queryState } = useContext(searchQueryContext);
    const sortedResults = orderBy(results, [sortBy], [sortingOrder]);

    function sortByName() {
        changeSortBy(SORT_STRINGS.NAME);
    }

    if (queryState.action === "search_files") {
        return <FilesList files={sortedResults} fromSearch={true} />;
    } else if (queryState.action === "search_objects") {
        return <SearchResultObjectTable results={results} actionName={actionName} />;
    } else if (queryState.action === "search_people") {
        return <SearchResultPeopleTable results={results} actionName={actionName} />;
    } else if (queryState.action === "search_events") {
        return <SearchResultEventTable results={results} actionName={actionName} />;
    } else if (queryState.action === "search_contracts") {
        return <SearchResultRelationshipTable results={results} actionName={actionName} />;
    } else if (queryState.action === "search_organizations") {
        return <SearchResultOrganizationTable results={results} actionName={actionName} />;
    }

    return (
        <div className="b-table__wrapper">
            <table className="b-table _first-cell-border">
                <thead className="b-table__head">
                    <tr className="b-table__row _head">
                        <th className="b-table__cell">No</th>
                        <th className="b-table__cell">
                            <SortButton text={`${actionName} name`} onClick={sortByName} sorted={getSortOrderForField(SORT_STRINGS.NAME)} />
                        </th>
                    </tr>
                </thead>
                <tbody className="b-table__body">
                    {sortedResults.map((item, index) => (
                        <SearchResultsTableRow key={item.id} {...item} index={index} />
                    ))}
                </tbody>
            </table>
        </div>
    );
};

interface ISearchResultsTableRowProps {
    name: string;
    type: string;
    id: string;
    index: number;
    address: string;
    organization?: IOrganization;
    meta: {
        author: {
            first_name: string;
            last_name: string;
        };
    };
}

const SearchResultsTableRow: FunctionComponent<ISearchResultsTableRowProps> = ({ name, type, id, index, organization }) => {
    const { queryState } = useContext(searchQueryContext);

    const typeLabel = generateObjectTypeLabel(type);

    if (queryState.action === "search_contracts" && !organization) {
        return null;
    }

    return (
        <tr className="b-table__row">
            <td className="b-table__cell">{index + 1}</td>
            <td className="b-table__cell">
                {(function() {
                    switch (queryState.action) {
                        case "search_objects":
                            return <ObjectName name={name} type={type} typeLabel={typeLabel} id={id} />;
                        case "search_people":
                            return <PersonName name={name} type={type} typeLabel={typeLabel} id={id} />;
                        case "search_files":
                            return <FileName name={name} type={type} typeLabel={typeLabel} id={id} />;
                        case "search_contracts":
                            return <ContractName name={name} type={type} typeLabel={typeLabel} id={id} organization={organization} />;
                        case "search_events":
                            return <EventName name={name} type={type} typeLabel={typeLabel} id={id} />;
                    }
                })()}
            </td>
        </tr>
    );
};

function generateObjectTypeLabel(type: string) {
    return type ? type : "";
}

function generateEventDate(startDate: string, endDate: string) {
    if (!endDate && !startDate) {
        return "";
    }
    //determine if start date is after present day
    const eventStarted = isAfter(new Date(), new Date(startDate));
    if (!eventStarted) {
        return format(new Date(startDate), "yyyy/MM/d");
    }
    //determine if event is completed
    const eventCompleted = isAfter(new Date(endDate), new Date());
    if (eventCompleted) {
        return format(new Date(endDate), "yyyy/MM/d");
    }
    //find next occurence
    return format(new Date(endDate), "yyyy/MM/d");
}

const SearchResultObjectTable: FunctionComponent<ISearchResultsTableProps> = ({ results, actionName }) => {
    const { sortBy, sortingOrder, getSortOrderForField, changeSortBy } = useSortingTable({ localStorageKey: "search" });
    const sortedResults = orderBy(results, [sortBy], [sortingOrder]);

    function sortByName() {
        changeSortBy(SORT_STRINGS.NAME);
    }

    return (
        <div className="b-table__wrapper">
            <table className="b-table _first-cell-border">
                <thead className="b-table__head">
                    <tr className="b-table__row _head">
                        <th className="b-table__cell">No</th>
                        <th className="b-table__cell">
                            <SortButton text={`${actionName} name`} onClick={sortByName} sorted={getSortOrderForField(SORT_STRINGS.NAME)} />
                        </th>
                        <th className="b-table__cell">Address</th>
                        <th className="b-table__cell">Owner</th>
                    </tr>
                </thead>
                <tbody className="b-table__body">
                    {sortedResults.map((item, index) => (
                        <SearchResultObjectTableRow key={item.id} {...item} index={index} />
                    ))}
                </tbody>
            </table>
        </div>
    );
};

interface ISearchResultsObjectTableRowProps {
    name: string;
    type: {
        id: string;
        name: string;
    };
    id: string;
    index: number;
    address: string;
    organization?: IOrganization;
    meta: {
        author: {
            first_name: string;
            last_name: string;
        };
    };
}

const SearchResultObjectTableRow: FunctionComponent<ISearchResultsObjectTableRowProps> = ({ name, type, id, index, address, meta }) => {
    const owner = get(meta, "author", "");

    return (
        <tr className="b-table__row">
            <td className="b-table__cell">{index + 1}</td>
            <td className="b-table__cell">
                <ObjectName name={name} type={type ? type.name : ""} id={id} />
            </td>
            <td className="b-table__cell">{address}</td>
            <td className="b-table__cell"> {owner ? `${owner.first_name} ${owner.last_name}` : ""} </td>
        </tr>
    );
};

const SearchResultPeopleTable: FunctionComponent<ISearchResultsTableProps> = ({ results, actionName }) => {
    const { sortBy, sortingOrder, getSortOrderForField, changeSortBy } = useSortingTable({ localStorageKey: "search" });
    const sortedResults = orderBy(results, [sortBy], [sortingOrder]);

    function sortByName() {
        changeSortBy(SORT_STRINGS.NAME);
    }

    return (
        <div className="b-table__wrapper">
            <table className="b-table _first-cell-border">
                <thead className="b-table__head">
                    <tr className="b-table__row _head">
                        <th className="b-table__cell">No</th>
                        <th className="b-table__cell">
                            <SortButton text={`${actionName} name`} onClick={sortByName} sorted={getSortOrderForField(SORT_STRINGS.NAME)} />
                        </th>
                        <th className="b-table__cell">Email</th>
                        <th className="b-table__cell">Phone Number</th>
                        <th className="b-table__cell">Organization</th>
                        <th className="b-table__cell">Position</th>
                    </tr>
                </thead>
                <tbody className="b-table__body">
                    {sortedResults.map((item, index) => (
                        <SearchResultPeopleTableRow key={item.id} {...item} index={index} />
                    ))}
                </tbody>
            </table>
        </div>
    );
};

interface ISearchResultsPeopleTableRowProps extends ISearchResultsTableRowProps {
    email: string;
    phone: string;
    mobile_phone: string;
}

const SearchResultPeopleTableRow: FunctionComponent<ISearchResultsPeopleTableRowProps> = ({
    name,
    type,
    id,
    index,
    email,
    phone,
    mobile_phone,
}) => {
    const typeLabel = generateObjectTypeLabel(type);

    return (
        <tr className="b-table__row">
            <td className="b-table__cell">{index + 1}</td>
            <td className="b-table__cell">
                <PersonName name={name} type={type} typeLabel={typeLabel} id={id} />
            </td>
            <td className="b-table__cell">{email}</td>
            <td className="b-table__cell"> {mobile_phone ? mobile_phone : phone} </td>
            <td className="b-table__cell"> </td>
            <td className="b-table__cell"> </td>
        </tr>
    );
};

const SearchResultEventTable: FunctionComponent<ISearchResultsTableProps> = ({ results, actionName }) => {
    const { sortBy, sortingOrder, getSortOrderForField, changeSortBy } = useSortingTable({ localStorageKey: "search" });
    const sortedResults = orderBy(results, [sortBy], [sortingOrder]);

    function sortByName() {
        changeSortBy(SORT_STRINGS.NAME);
    }

    return (
        <div className="b-table__wrapper">
            <table className="b-table _first-cell-border">
                <thead className="b-table__head">
                    <tr className="b-table__row _head">
                        <th className="b-table__cell">No</th>
                        <th className="b-table__cell">
                            <SortButton text={`${actionName} name`} onClick={sortByName} sorted={getSortOrderForField(SORT_STRINGS.NAME)} />
                        </th>
                        <th className="b-table__cell">Date</th>
                        <th className="b-table__cell">Building Address</th>
                        <th className="b-table__cell">Object</th>
                    </tr>
                </thead>
                <tbody className="b-table__body">
                    {sortedResults.map((item, index) => (
                        <SearchResultEventTableRow key={item.id} {...item} index={index} />
                    ))}
                </tbody>
            </table>
        </div>
    );
};

interface ISearchResultsEventTableRowProps {
    name: string;
    type: {
        name: string;
        id: string;
    };
    id: string;
    index: number;
    address: string;
    start_date: string;
    end_date: string;
    organization?: IOrganization;
    meta: {
        author: {
            first_name: string;
            last_name: string;
        };
    };
    details: any;
    created_at: string;
    object: {
        name: string;
        address: string;
    };
}

const SearchResultEventTableRow: FunctionComponent<ISearchResultsEventTableRowProps> = ({
    name,
    type,
    id,
    index,
    object,
    start_date,
    end_date,
}) => {
    const typeName = get(type, "name", "");
    const objectName = get(object, "name", "");
    const objectAddress = get(object, "address", "");

    const typeLabel = generateObjectTypeLabel(typeName);
    const date = generateEventDate(start_date, end_date);

    return (
        <tr className="b-table__row">
            <td className="b-table__cell">{index + 1}</td>
            <td className="b-table__cell">
                <EventName name={name} type={typeName} typeLabel={typeLabel} id={id} />
            </td>
            <td className="b-table__cell">{date}</td>
            <td className="b-table__cell">{objectAddress} </td>
            <td className="b-table__cell">{objectName} </td>
        </tr>
    );
};

const SearchResultRelationshipTable: FunctionComponent<ISearchResultsTableProps> = ({ results }) => {
    const { sortBy, sortingOrder, getSortOrderForField, changeSortBy } = useSortingTable({ localStorageKey: "search" });
    const sortedResults = orderBy(results, [sortBy], [sortingOrder]);

    function sortByName() {
        changeSortBy(SORT_STRINGS.NAME);
    }

    return (
        <div className="b-table__wrapper">
            <table className="b-table _first-cell-border">
                <thead className="b-table__head">
                    <tr className="b-table__row _head">
                        <th className="b-table__cell">No</th>
                        <th className="b-table__cell">
                            <SortButton text={"Organization"} onClick={sortByName} sorted={getSortOrderForField(SORT_STRINGS.NAME)} />
                        </th>
                        <th className="b-table__cell">Object</th>
                        <th className="b-table__cell">Person</th>
                        <th className="b-table__cell">Relationship Type</th>
                    </tr>
                </thead>
                <tbody className="b-table__body">
                    {sortedResults.map((item, index) => (
                        <SearchResultRelationshipTableRow key={item.id} {...item} index={index} />
                    ))}
                </tbody>
            </table>
        </div>
    );
};

interface ISearchResultRelationshipTableRowProps extends ISearchResultsTableRowProps {
    employees: [
        {
            id: string;
            user: {
                id: string;
                first_name: string;
                last_name: string;
            };
        }
    ];
    objects: {
        name: string;
        id: string;
        address: string;
    };
    role: {
        name: string;
    };
}

const SearchResultRelationshipTableRow: FunctionComponent<ISearchResultRelationshipTableRowProps> = ({
    name,
    type,
    id,
    index,
    organization,
    employees,
    objects,
    role,
}) => {
    const objectName = get(objects, "[0].name", "");
    const objectID = get(objects, "[0].id", "") || "";
    const objectType = get(objects, "[0].type", "");
    const objectRelationship = get(role, "name", "");
    const authors =
        employees.map((i) => {
            const user = get(i, "user", "");
            if (user) return user.first_name + " " + user.last_name;

            return null;
        }) || [];
    const typeLabel = generateObjectTypeLabel(type);

    return (
        <tr className="b-table__row">
            <td className="b-table__cell">{index + 1}</td>
            <td className="b-table__cell">
                <ContractName name={name} type={type} typeLabel={typeLabel} id={id} organization={organization} />
            </td>
            <td className="b-table__cell">
                <ObjectNameSimple name={objectName} type={objectType} id={objectID} postfix="organizations" />
            </td>
            <td className="b-table__cell">{authors ? authors.join(", ") : ""} </td>
            <td className="b-table__cell">{objectRelationship} </td>
        </tr>
    );
};

const SearchResultOrganizationTable: FunctionComponent<ISearchResultsTableProps> = ({ results, actionName }) => {
    const { sortBy, sortingOrder, getSortOrderForField, changeSortBy } = useSortingTable({ localStorageKey: "search" });
    const sortedResults = orderBy(results, [sortBy], [sortingOrder]);

    function sortByName() {
        changeSortBy(SORT_STRINGS.NAME);
    }

    return (
        <div className="b-table__wrapper">
            <table className="b-table _first-cell-border">
                <thead className="b-table__head">
                    <tr className="b-table__row _head">
                        <th className="b-table__cell">No</th>
                        <th className="b-table__cell">
                            <SortButton text={`${actionName} name`} onClick={sortByName} sorted={getSortOrderForField(SORT_STRINGS.NAME)} />
                        </th>
                        <th className="b-table__cell">Type</th>
                        <th className="b-table__cell">Address</th>
                        <th className="b-table__cell">E-Mail</th>
                        <th className="b-table__cell">Phone</th>
                    </tr>
                </thead>
                <tbody className="b-table__body">
                    {sortedResults.map((item, index) => (
                        <SearchResultOrganizationTableRow key={item.id} {...item} index={index} />
                    ))}
                </tbody>
            </table>
        </div>
    );
};

interface ISearchResultsOrganizationTableRowProps {
    name: string;
    type: {
        id: string;
        name: string;
    };
    category: string;
    email: string;
    phone: string;
    id: string;
    index: number;
    address: IAddress;
    organization?: IOrganization;
    meta: {
        author: {
            first_name: string;
            last_name: string;
        };
    };
}

const SearchResultOrganizationTableRow: FunctionComponent<ISearchResultsOrganizationTableRowProps> = ({
    name,
    email,
    phone,
    type,
    id,
    index,
    address,
    category,
}) => {
    const isClientOrganization = category === "CLIENT";

    const addressString = getSearchOrganizationAddressString(address);

    return (
        <tr className="b-table__row">
            <td className="b-table__cell">{index + 1}</td>
            <td className="b-table__cell">{<OrganizationName name={name} isClientOrganization={isClientOrganization} id={id} />}</td>
            <td className="b-table__cell">{type ? type.name : ""}</td>
            <td className="b-table__cell">{addressString}</td>
            <td className="b-table__cell"> {email} </td>
            <td className="b-table__cell"> {phone} </td>
        </tr>
    );
};
