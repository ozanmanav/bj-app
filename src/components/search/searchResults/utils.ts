import { IAddress } from "../../../config";

export const getSearchOrganizationAddressString = (address: IAddress) => {
    return address
        ? `${address.line_1 || ""} ${address.line_2 || ""}
        ${address.street && (address.line_1 || address.line_2) ? "," : ""}
        ${address.street || ""} ${address.number || ""}
        ${address.city && (address.street || address.number) ? "," : ""}
        ${address.plz || ""} ${address.city || ""}`
        : "";
};
