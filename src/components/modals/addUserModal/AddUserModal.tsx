import React, { FunctionComponent } from "react";
import { IModalProps, Modal } from "../../ui/modal";
import { AddUserForm } from "../../forms/addUserForm";
import { IUser, IJob } from "../../forms/addUserForm/definitions";

interface IAddUserModalProps extends IModalProps {
    refetch?: () => void;
    onUserAdded?: (user: IUser, employeeID?: string, job?: IJob) => void;
    organizationID: string;
    companyCountry?: string;
    companyType?: string;
    editedUser?: IUser;
    isAdminPanel?: boolean;
    variant?: "withCompaniesList";
    adminID?: string;
    isClientOrganization?: boolean;
    title?: string;
}

export const AddUserModal: FunctionComponent<IAddUserModalProps> = ({
    refetch,
    editedUser,
    organizationID,
    companyType,
    isAdminPanel,
    variant,
    adminID,
    companyCountry,
    title,
    isClientOrganization,
    onUserAdded,
    ...modalProps
}) => {
    const callback = (user?: IUser, employeeID?: string, job?: IJob) => {
        refetch && refetch();
        onUserAdded && user && onUserAdded(user, employeeID, job);
        modalProps.hide();
    };

    return (
        <Modal {...modalProps}>
            <AddUserForm
                editedUser={editedUser}
                organizationID={organizationID}
                companyCountry={companyCountry}
                companyType={companyType}
                callback={callback}
                refetch={refetch}
                isAdminPanel={isAdminPanel}
                variant={variant}
                adminID={adminID}
                title={title}
                isClientOrganization={isClientOrganization}
            />
        </Modal>
    );
};
