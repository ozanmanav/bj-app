import React, { FunctionComponent, useEffect, useState, useCallback } from "react";
import "./AddNonClientOrganizationModal.scss";
import { Modal, ModalFooter, Button, Input, Select, handleGraphQLErrors, showErrorToast, showSuccessToast, Loading } from "../../ui";
import { Formik, FormikProps } from "formik";
import { phoneCodesByCountry } from "../../../config";
import { useMutation, useApolloClient } from "@apollo/react-hooks";
import { getCountriesOptions, getOrganizationTypesOptions } from "../../../utils";
import { useOrganizationTypes } from "../../../hooks";
import { COUNTRIES } from "../../../config/countries";
import get from "lodash.get";
import { useCountryPhoneCode } from "../../../hooks/useCountryPhoneCode";
import {
    CREATE_NON_CLIENT_ORGANIZATION,
    GET_NON_CLIENT_ORGANIZATION,
    INonClientOrganizationModal,
    NonClientOrganizationDefaultState,
    NonClientOrganizationFormSchema,
    INonClientOrganizationFormState,
} from "./definitions";

export const AddNonClientOrganizationModal: FunctionComponent<INonClientOrganizationModal> = ({ organizationID, callback, ...props }) => {
    const [initialValues, setInitialValues] = useState<INonClientOrganizationFormState>(NonClientOrganizationDefaultState);
    const [loading, setLoading] = useState(false);
    const [addNonClientOrganization] = useMutation(CREATE_NON_CLIENT_ORGANIZATION);
    const { phone_code, country } = useCountryPhoneCode();
    const apolloClient = useApolloClient();

    const getNonClientOrg = useCallback(
        async function() {
            try {
                const { data, loading } = await apolloClient.query({
                    query: GET_NON_CLIENT_ORGANIZATION,
                    variables: {
                        id: organizationID,
                    },
                });

                setLoading(loading);
                setInitialValues(get(data, "organizations.data[0]"));
            } catch (e) {
                showErrorToast(e.message);
            }
        },
        [organizationID, apolloClient]
    );

    useEffect(() => {
        setInitialValues(
            (prevInitialValues: INonClientOrganizationFormState): INonClientOrganizationFormState => ({
                ...prevInitialValues,
                phone_code,
                country,
            })
        );

        if (organizationID) {
            getNonClientOrg();
        }
    }, [organizationID, phone_code, country]);

    async function onNewNonClientOrganizationSubmit(state: INonClientOrganizationFormState) {
        try {
            const { country, city, street, number, plz, phone_code, phone, line_1, line_2 } = state;

            const googleMapsAddress = `${country}, ${city}, ${street}, ${number}`;
            const googleMapsLink = encodeURI(`https://maps.google.com/maps/search/?api=1&query=${googleMapsAddress}`);

            const res = await addNonClientOrganization({
                variables: {
                    ...state,
                    category: "NON_CLIENT",
                    phone: `${phone_code} ${phone}`,
                    address: {
                        country,
                        city,
                        street,
                        number,
                        plz,
                        maps_link: googleMapsLink,
                        line_1,
                        line_2,
                    },
                },
            });

            const { errors } = res;

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                callback && callback(get(res, "data.organization"));
                props.hide();
                showSuccessToast(`${get(res, "data.organization.name")} is successfully ${organizationID ? "updated" : "added"}`);
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    return (
        <Modal {...props}>
            {organizationID && loading ? (
                <Loading />
            ) : (
                <Formik
                    onSubmit={onNewNonClientOrganizationSubmit}
                    initialValues={initialValues}
                    enableReinitialize
                    render={(formikProps) => <NonClientOrganizationForm {...formikProps} isEditing={!!organizationID} />}
                    validationSchema={NonClientOrganizationFormSchema}
                />
            )}
        </Modal>
    );
};

interface IOrganizationFormProps extends FormikProps<INonClientOrganizationFormState> {
    isEditing?: boolean;
}

const NonClientOrganizationForm: FunctionComponent<IOrganizationFormProps> = ({
    isEditing,
    values,
    errors,
    touched,
    handleSubmit,
    handleChange,
    handleBlur,
}) => {
    const organizationTypes = useOrganizationTypes();

    return (
        <form onSubmit={handleSubmit}>
            <h2 className="h1 b-new-non-client-organization-modal__title">
                {isEditing ? "Edit organization (Non-Client)" : "Add new organization (Non-Client)"}
            </h2>
            <Input
                autoComplete="no"
                placeholder="Organization name"
                name="name"
                value={values.name}
                error={errors.name}
                touched={touched.name}
                onChange={handleChange}
                onBlur={handleBlur}
            />
            <Select
                options={getOrganizationTypesOptions(organizationTypes)}
                placeholder="Organization Type"
                name="type_id"
                value={values.type_id}
                error={errors.type_id}
                touched={touched.type_id}
                onChange={handleChange}
                onBlur={handleBlur}
            />
            <Input
                autoComplete="no"
                type="email"
                placeholder="E-Mail"
                name="email"
                value={values.email}
                error={errors.email}
                touched={touched.email}
                onChange={handleChange}
                onBlur={handleBlur}
            />
            <div className="flex">
                <Select
                    options={phoneCodesByCountry}
                    placeholder="Phone code"
                    name="phone_code"
                    value={values.phone_code}
                    error={errors.phone_code}
                    touched={touched.phone_code}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    marginBottom="none"
                    position="top"
                    className="b-new-non-client-organization-modal__phone-code"
                />
                <div className="b-new-non-client-organization-modal__phone">
                    <Input
                        autoComplete="no"
                        type="phone"
                        placeholder="Phone number"
                        name="phone"
                        value={values.phone}
                        error={errors.phone}
                        touched={touched.phone}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        marginBottom="normal"
                    />
                </div>
            </div>
            <Input
                autoComplete="no"
                type="line_1"
                placeholder="Address Line 1"
                name="line_1"
                value={values.line_1}
                error={errors.line_1}
                touched={touched.line_1}
                onChange={handleChange}
                onBlur={handleBlur}
            />
            <div className="flex">
                <div className="b-new-non-client-organization-modal__street">
                    <Input
                        autoComplete="no"
                        placeholder="Street"
                        name="street"
                        value={values.street}
                        error={errors.street}
                        touched={touched.street}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                </div>
                <Input
                    autoComplete="no"
                    placeholder="Number"
                    name="number"
                    value={values.number}
                    error={errors.number}
                    touched={touched.number}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className="b-new-non-client-organization-modal__number"
                />
            </div>
            <Input
                autoComplete="no"
                type="line_2"
                placeholder="Address Line 2"
                name="line_2"
                value={values.line_2}
                error={errors.line_2}
                touched={touched.line_2}
                onChange={handleChange}
                onBlur={handleBlur}
            />

            <div className="flex">
                <Input
                    autoComplete="no"
                    placeholder="PLZ"
                    className="b-new-non-client-organization-modal__plz"
                    name="plz"
                    value={values.plz}
                    error={errors.plz}
                    touched={touched.plz}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />
                <div className="b-new-non-client-organization-modal__city">
                    <Input
                        autoComplete="no"
                        placeholder="Ort/City"
                        name="city"
                        value={values.city}
                        error={errors.city}
                        touched={touched.city}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                </div>
            </div>
            <Select
                options={getCountriesOptions(COUNTRIES)}
                placeholder="Country"
                name="country"
                position="top"
                value={values.country}
                error={errors.country}
                touched={touched.country}
                onChange={handleChange}
                onBlur={handleBlur}
            />

            <ModalFooter>
                <Button text={isEditing ? "Update organization" : "Save organization"} primary icon="check" type="submit" />
            </ModalFooter>
        </form>
    );
};
