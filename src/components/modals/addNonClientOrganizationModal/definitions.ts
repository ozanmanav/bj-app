import * as Yup from "yup";
import { VALIDATION_ERRORS } from "../../../config";
import gql from "graphql-tag";
import { IModalProps } from "../../ui";

// TODO: add phone validation
// zip code validation according to this https://en.wikipedia.org/wiki/Postal_codes_in_Switzerland_and_Liechtenstein
export const NonClientOrganizationFormSchema = Yup.object().shape({
    name: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    number: Yup.number().typeError(VALIDATION_ERRORS.number),
    type_id: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    email: Yup.string().email(VALIDATION_ERRORS.email),
    phone: Yup.number().typeError(VALIDATION_ERRORS.number),
    plz: Yup.number()
        .min(1000, VALIDATION_ERRORS.zip)
        .max(9999, VALIDATION_ERRORS.zip)
        .typeError(VALIDATION_ERRORS.zip),
});

export const NonClientOrganizationDefaultState = {
    name: "",
    street: "",
    number: "",
    country: "",
    plz: "",
    city: "",
    email: "",
    phone: "",
    phone_code: "",
    type_id: "",
    line_1: "",
    line_2: "",
};

export interface INonClientOrganizationFormState {
    name: string;
    street: string;
    number: string;
    country: string;
    email: string;
    phone: string;
    phone_code: string;
    type_id: string;
    city: string;
    plz: string;
    line_1: string;
    line_2: string;
}

export const CREATE_NON_CLIENT_ORGANIZATION = gql`
    mutation CreateOrganization(
        $id: String
        $name: String
        $address: OrganizationAddressParameters
        $phone: String
        $email: String
        $type_id: String
        $category: CategoryEnum
    ) {
        organization(id: $id, name: $name, address: $address, phone: $phone, email: $email, type_id: $type_id, category: $category) {
            id
            name
        }
    }
`;

export const GET_NON_CLIENT_ORGANIZATION = gql`
    query GetOrganization($id: String!) {
        organizations(id: $id) {
            data {
                id
                name
                address {
                    plz
                    city
                }
                phone
                email
                type_id
            }
        }
    }
`;

export interface INonClientOrganizationModal extends IModalProps {
    organizationID?: string;
    callback?: (organization: { id: string; name: string; relationId: string }) => void;
}
