import React, { FunctionComponent } from "react";
import { IModalProps, Modal } from "../../ui/modal";
import { IEventRelatedObjectsFormState } from "../../../views/app/event/addEvent/eventRelatedObjectsForm/definitions";
import { IRelatedObjectsTableObject } from "../../tables";
import { TaskRelatedObjectsForm } from "../../../views/app/task/addTask/taskRelatedObjectsForm";
import { ITaskRelatedObjectsFormState } from "../../../views/app/task/addTask/taskRelatedObjectsForm/definitions";

interface IAddRelatedObjectToTaskModalProps extends IModalProps {
    relatedObjects: IRelatedObjectsTableObject[];
    refetch?: (values: ITaskRelatedObjectsFormState) => void;
}

export const AddRelatedObjectToTaskModal: FunctionComponent<IAddRelatedObjectToTaskModalProps> = ({
    refetch,
    relatedObjects,
    ...modalProps
}) => {
    const callback = (values: IEventRelatedObjectsFormState) => {
        refetch && refetch(values);
        modalProps.hide();
    };

    return (
        <Modal {...modalProps}>
            <h3>Add Related Objects to Task</h3>
            <TaskRelatedObjectsForm initialValues={{ objects: relatedObjects }} onSubmit={callback} />
        </Modal>
    );
};
