import React, { FunctionComponent } from "react";
import { IModalProps, Modal } from "../../ui/modal";
import { EventRelatedObjectsForm } from "../../../views/app/event/addEvent/eventRelatedObjectsForm";
import { IEventRelatedObjectsFormState } from "../../../views/app/event/addEvent/eventRelatedObjectsForm/definitions";
import { IRelatedObjectsTableObject } from "../../tables";

interface IAddRelatedObjectToEventModalProps extends IModalProps {
    relatedObjects: IRelatedObjectsTableObject[];
    refetch?: (values: IEventRelatedObjectsFormState) => void;
}

export const AddRelatedObjectToEventModal: FunctionComponent<IAddRelatedObjectToEventModalProps> = ({
    refetch,
    relatedObjects,
    ...modalProps
}) => {
    const callback = (values: IEventRelatedObjectsFormState) => {
        refetch && refetch(values);
        modalProps.hide();
    };

    return (
        <Modal {...modalProps}>
            <h3>Add Related Objects to Event</h3>
            <EventRelatedObjectsForm initialValues={{ objects: relatedObjects }} onSubmit={callback} />
        </Modal>
    );
};
