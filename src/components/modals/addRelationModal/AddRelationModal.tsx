import React, { FunctionComponent } from "react";
import { IModalProps, Modal } from "../../ui/modal";
import { AddRelationForm, IAddRelationFormState } from "../../forms/addRelationForm";
import { useApolloClient } from "@apollo/react-hooks";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui/toasts";
import { CREATE_RELATIONSHIP_MUTATION, UPDATE_RELATIONSHIP_MUTATION } from "./config";
import { TRelationRoles } from "../../../config/types";
import get from "lodash.get";

interface IAddRelationModalProps extends IModalProps {
    onSubmit: () => void;
    objectID: string;
    objectName: string;
    addObjectLink?: string;
    relationRole?: TRelationRoles;
    editedRelation?: IAddRelationFormState;
    objectCity?: string;
    defaultRole?: string;
}

export const AddRelationModal: FunctionComponent<IAddRelationModalProps> = ({
    onSubmit,
    objectID,
    addObjectLink,
    relationRole,
    editedRelation,
    objectName,
    objectCity,
    defaultRole,
    ...props
}) => {
    const apolloClient = useApolloClient();

    async function addRelation(relation: IAddRelationFormState) {
        const splittedRole = relation.role.split(/_(.*)/);
        const relationType = splittedRole[0];
        const relationID = splittedRole[1];

        const isHigher = relationType === "higher";

        const lowerID = isHigher ? relation.objectID : objectID;
        const higherID = isHigher ? objectID : relation.objectID;

        try {
            const { errors } = await apolloClient.mutate({
                mutation: editedRelation ? UPDATE_RELATIONSHIP_MUTATION : CREATE_RELATIONSHIP_MUTATION,
                variables: {
                    ...(editedRelation && { id: get(editedRelation, "relationID") }),
                    type_id: relationID,
                    higher_id: higherID,
                    lower_id: lowerID,
                },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                await onSubmit();
                showSuccessToast("Relation successfully added.");
                props.hide();
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    return (
        <Modal {...props}>
            <AddRelationForm
                onSubmit={addRelation}
                addObjectLink={addObjectLink}
                editedRelation={editedRelation}
                relationRole={relationRole}
                objectID={objectID}
                objectName={objectName}
                objectCity={objectCity}
                defaultRole={defaultRole}
            />
        </Modal>
    );
};
