import gql from "graphql-tag";

export const CREATE_RELATIONSHIP_MUTATION = gql`
    mutation CreateRelationship($type_id: String!, $higher_id: String!, $lower_id: String!) {
        createRelationship(type_id: $type_id, higher_id: $higher_id, lower_id: $lower_id) {
            id
        }
    }
`;

export const UPDATE_RELATIONSHIP_MUTATION = gql`
    mutation UpdateRelationship($id: String!, $type_id: String!, $higher_id: String!, $lower_id: String!) {
        updateRelationship(id: $id, type_id: $type_id, higher_id: $higher_id, lower_id: $lower_id) {
            id
        }
    }
`;
