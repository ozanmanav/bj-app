import React, { FunctionComponent } from "react";
import "./ForgotPasswordModal.scss";
import { IModalProps, Modal, ModalFooter } from "../../ui/modal";
import { Formik, FormikProps } from "formik";
import * as Yup from "yup";
import { Input } from "../../ui/inputs";
import { Button } from "../../ui/buttons";
import { VALIDATION_ERRORS } from "../../../config";
import { showErrorToast, showSuccessToast } from "../../ui/toasts";
import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";

const RESET_PASSWORD = gql`
    mutation ResetPassword($email: String!) {
        resetUserPassword(email: $email) {
            result
        }
    }
`;

interface IForgotPasswordFormState {
    email: string;
}

const forgotPasswordFormInitialValues = {
    email: "",
};

const ForgotPasswordFormSchema = Yup.object().shape({
    email: Yup.string()
        .email(VALIDATION_ERRORS.email)
        .required(VALIDATION_ERRORS.required),
});

interface IForgotPasswordModalProps extends IModalProps {}

export const ForgotPasswordModal: FunctionComponent<IForgotPasswordModalProps> = ({ ...props }) => {
    const [resetPassword] = useMutation(RESET_PASSWORD);

    async function onSubmit(values: IForgotPasswordFormState) {
        try {
            await resetPassword({
                variables: {
                    email: values.email,
                },
            });

            props.hide();
            showSuccessToast("Restoration email has been sent.");
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    return (
        <Modal {...props}>
            <Formik
                onSubmit={onSubmit}
                initialValues={forgotPasswordFormInitialValues}
                validationSchema={ForgotPasswordFormSchema}
                component={ForgotPasswordForm}
            />
        </Modal>
    );
};

interface IForgotPasswordFormProps extends FormikProps<IForgotPasswordFormState> {}

const ForgotPasswordForm: FunctionComponent<IForgotPasswordFormProps> = ({
    values,
    errors,
    touched,
    handleSubmit,
    handleChange,
    handleBlur,
}) => {
    return (
        <form onSubmit={handleSubmit}>
            <h2 className="h1 b-forgot-password__title">Forgot password?</h2>
            <Input
                name="email"
                value={values.email}
                error={errors && errors.email}
                touched={touched && touched.email}
                onChange={handleChange}
                onBlur={handleBlur}
                marginBottom="none"
                placeholder="E-Mail"
            />
            <ModalFooter>
                <Button text="Restore" icon="check" primary type="submit" />
            </ModalFooter>
        </form>
    );
};
