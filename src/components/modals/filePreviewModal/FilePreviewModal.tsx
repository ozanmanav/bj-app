import React, { FunctionComponent } from "react";
import "./FilePreviewModal.scss";
import { IModalProps, Modal } from "../../ui/modal";
import { Button } from "../../ui/buttons";

interface IIFrameModalProps extends IModalProps {
    url: string;
    download: () => void;
}

const IMAGE_URL_REGEX = /([a-z\-_0-9/:.]*\.(jpg|jpeg|png|svg|gif|bmp))/i;

export const FilePreviewModal: FunctionComponent<IIFrameModalProps> = ({ url, download, ...props }) => {
    const isImage = IMAGE_URL_REGEX.test(url);

    return (
        <Modal {...props} className="b-file-preview-modal">
            {isImage ? (
                <img src={url} alt="" className="b-file-preview-modal__preview" />
            ) : (
                <object data={url} className="b-file-preview-modal__preview">
                    <a href={url} className="b-file-preview-modal__error ">
                        Unable to preview the file, click to download.
                    </a>
                </object>
            )}
            <Button text="Download file" onClick={download} />
        </Modal>
    );
};
