import React, { FunctionComponent } from "react";
import { IModalProps, Modal } from "../../ui/modal";
import { AdminSuspendOrganizationForm } from "../../forms/adminSuspendOrganizationForm";
import { IAdminSuspendOrganizationFormContract } from "../../forms/adminSuspendOrganizationForm/AdminSuspendOrganizationForm";

interface IAdminSuspendOrganizationModalProps extends IModalProps {
    organizationId: string;
    refetch: Function;
    contracts: IAdminSuspendOrganizationFormContract[];
}

export const AdminSuspendOrganizationModal: FunctionComponent<IAdminSuspendOrganizationModalProps> = ({
    organizationId,
    contracts,
    refetch,
    ...props
}) => {
    return (
        <Modal {...props}>
            <AdminSuspendOrganizationForm refetch={refetch} contracts={contracts} hideModal={props.hide} organizationId={organizationId} />
        </Modal>
    );
};
