import gql from "graphql-tag";

export const UPDATE_LOGO_MUTATIONS = gql`
    mutation UpdateOrganizationLogo($id: String!, $logo_id: String!) {
        organization(id: $id, logo_id: $logo_id) {
            id
        }
    }
`;
