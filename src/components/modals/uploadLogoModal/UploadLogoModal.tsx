import React, { FunctionComponent } from "react";
import { IModalProps, Modal } from "../../ui/modal";
import { UploadImageForm } from "../../forms/uploadLogoForm";
import { useMutation } from "@apollo/react-hooks";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui/toasts";
import { UPDATE_LOGO_MUTATIONS } from "./config";

export type TImageUploadModalType = "organization";

interface IImageUploadModalProps extends IModalProps {
    entityId: string;
    refetch: Function;
}

export const UploadLogoModal: FunctionComponent<IImageUploadModalProps> = ({ refetch, entityId, ...modalProps }) => {
    const [updateLogoMutation] = useMutation(UPDATE_LOGO_MUTATIONS);

    async function updateCompany(logo_id: string) {
        try {
            const { errors } = await updateLogoMutation({
                variables: {
                    id: entityId,
                    logo_id,
                },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                showSuccessToast("Logo is successfully updated");
                refetch();
                modalProps.hide();
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    return (
        <Modal {...modalProps}>
            <UploadImageForm callback={updateCompany} title="Update logo" />
        </Modal>
    );
};
