import React, { FunctionComponent } from "react";
import { IModalProps, Modal } from "../../ui/modal";
import { SettingsDetailsForm } from "../../forms";
import { IAddress } from "../../../config";
import "./EditCompanyDetailsModal.scss";

interface IEditCompanyDetailsModalProps extends IModalProps {
    organizationId: string;
    address: IAddress;
    invoiceAddresses: IAddress[];
}

export const EditCompanyDetailsModal: FunctionComponent<IEditCompanyDetailsModalProps> = ({
    organizationId,
    address,
    invoiceAddresses,
    ...props
}) => {
    function onEditCallback() {
        props.hide();
    }

    return (
        <Modal {...props} className="b-edit-company-address-modal">
            <h3 className="b-edit-company-address-modal__title">Edit Company Details</h3>
            <SettingsDetailsForm organizationId={organizationId} invoiceAddresses={invoiceAddresses} callback={onEditCallback} />
        </Modal>
    );
};
