import gql from "graphql-tag";

export const CREATE_USER_WITH_COMPANY = gql`
    mutation CreateUser(
        $email: String!
        $password: String
        $first_name: String!
        $last_name: String!
        $is_client: Boolean!
        $is_admin: Boolean
    ) {
        createUser(
            email: $email
            password: $password
            first_name: $first_name
            last_name: $last_name
            is_client: $is_client
            is_admin: $is_admin
        ) {
            id
        }
    }
`;

export const CREATE_CLIENT_ORGANIZATION = gql`
    mutation CreateClientOrganization(
        $id: String
        $name: String
        $address: OrganizationAddressParameters
        $phone: String
        $email: String
        $type_id: String
        $category: String
    ) {
        organization(id: $id, name: $name, address: $address, phone: $phone, email: $email, type_id: $type_id, category: $category) {
            id
            name
            email
        }
    }
`;

export const GET_USER_BY_EMAIL = gql`
    query GetUserByEmail($email: String!) {
        users(email: $email) {
            data {
                id
            }
        }
    }
`;
