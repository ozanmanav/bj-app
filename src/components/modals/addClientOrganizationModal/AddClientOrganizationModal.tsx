import React, { FunctionComponent, useState, useEffect } from "react";
import { IModalProps, Modal } from "../../ui/modal";
import { AddClientOrganizationForm } from "../../forms/clientOrganizationPrefilledForm";
import { AddAdminUserForm } from "../../forms/addAdminUserForm";
import { useMutation, useApolloClient } from "@apollo/react-hooks";
import { CREATE_USER_WITH_COMPANY, GET_USER_BY_EMAIL, CREATE_CLIENT_ORGANIZATION } from "./definitions";
import { FormikActions } from "formik";
import get from "lodash.get";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui";
import { IAddAdminUserFormBaseState } from "../../forms/addAdminUserForm/definitions";
import {
    IClientOrganizationPrefilledFormState,
    clientOrganizationPrefilledFormState,
} from "../../forms/clientOrganizationPrefilledForm/definitions";
import { useCountryPhoneCode } from "../../../hooks/useCountryPhoneCode";
import "./AddClientOrganizationModal.scss";

interface IAddClientOrganizationModalProps extends IModalProps {
    callback?: (currentOrganization: { id: string; name: string; relationId: string }) => void;
}

export const AddClientOrganizationModal: FunctionComponent<IAddClientOrganizationModalProps> = ({ callback, ...props }) => {
    const [currentOrganization, setClientOrganization] = useState<IClientOrganizationPrefilledFormState>(
        clientOrganizationPrefilledFormState
    );

    const { phone_code, country } = useCountryPhoneCode();
    useEffect(() => {
        setClientOrganization(
            (prevClientOrganization: IClientOrganizationPrefilledFormState): IClientOrganizationPrefilledFormState => ({
                ...prevClientOrganization,
                address: {
                    ...prevClientOrganization.address,
                    country,
                },
                phone_code,
            })
        );
    }, [phone_code, country]);

    const apolloClient = useApolloClient();
    const [currentStep, setCurrentStep] = useState<1 | 2>(1);

    function onAddCallback(values: IClientOrganizationPrefilledFormState) {
        setClientOrganization(values);
        setCurrentStep(2);
    }

    function goToStepOne() {
        setCurrentStep(1);
    }

    const [createUserMutation] = useMutation(CREATE_USER_WITH_COMPANY);
    const [createClientOrganizationMutation] = useMutation(CREATE_CLIENT_ORGANIZATION);

    async function createAdminUser(values: IAddAdminUserFormBaseState, form: FormikActions<IAddAdminUserFormBaseState>) {
        const { email, first_name, last_name } = values;

        try {
            const { data: existingUserData } = await apolloClient.query({
                query: GET_USER_BY_EMAIL,
                variables: {
                    email,
                },
            });

            let adminUserID = get(existingUserData, "users.data[0].id");

            if (!adminUserID) {
                const res = await createUserMutation({
                    variables: {
                        email,
                        first_name,
                        last_name,
                        is_client: true,
                        is_admin: true,
                    },
                });

                const { data, errors } = res;

                if (errors) {
                    handleGraphQLErrors(errors);
                } else {
                    adminUserID = data.createUser.id;
                }
            }

            createClientOrganization(adminUserID);
        } catch (e) {
            e.graphQLErrors.forEach((error: any) => {
                if (error.validation) {
                    error.validation.email && form.setFieldError("email", error.validation.email.join(" "));
                } else {
                    showErrorToast(error.message);
                }
            });
        }
    }

    async function createClientOrganization(admin_user_id: string) {
        try {
            const { data, errors } = await createClientOrganizationMutation({
                variables: {
                    admin_user_id,
                    name: currentOrganization.name,
                    type_id: currentOrganization.type_id,
                    role_id: currentOrganization.role_id,
                    site_url: currentOrganization.site_url,
                    email: currentOrganization.email,
                    address: currentOrganization.address,
                    phone: currentOrganization.phone ? `${currentOrganization.phone_code} ${currentOrganization.phone}` : null,
                },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                showSuccessToast("Client organization is successfully created.");
                setClientOrganization(clientOrganizationPrefilledFormState);
                callback && callback(data.createCompany);
                setCurrentStep(1);
                props.hide();
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    return (
        <Modal {...props}>
            {currentStep === 1 && <AddClientOrganizationForm callback={onAddCallback} currentOrganization={currentOrganization} />}
            {currentStep === 2 && <AddAdminUserForm callback={createAdminUser} goToStepOne={goToStepOne} />}
        </Modal>
    );
};
