import React, { FunctionComponent } from "react";
import { IModalProps, Modal } from "../../ui/modal";
import { AdminMetaForm } from "../../forms/adminMetaForm";
import "./AdminMetaModal.scss";
import { TAdminEntityTypes } from "../../tables/adminTypesTable/config";

interface IAdminMetaModalProps extends IModalProps {
    typeId?: string;
    refetch?: Function;
    entityType: TAdminEntityTypes;
}

export const AdminMetaModal: FunctionComponent<IAdminMetaModalProps> = ({ typeId, refetch, entityType, ...props }) => {
    function onSubmitSuccess() {
        refetch && refetch();
        props.hide();
    }

    return (
        <Modal {...props} className="b-admin-meta-modal">
            <AdminMetaForm callback={onSubmitSuccess} typeId={typeId} entityType={entityType} />
        </Modal>
    );
};
