import React, { FunctionComponent } from "react";
import { IModalProps, Modal } from "../../ui/modal";
import { ConvertToClientOrganizationForm } from "../../forms/convertToClientOrganizationForm/ConvertToClientOrganizationForm";
import "./ConvertToClientOrganizationModal.scss";
interface IConvertToClientOrganizationModalProps extends IModalProps {
    organizationId: string;
    callback: (organization_id: string) => void;
}

export const ConvertToClientOrganizationModal: FunctionComponent<IConvertToClientOrganizationModalProps> = ({
    organizationId,
    callback,
    ...props
}) => {
    return (
        <Modal {...props} className="b-convert-to-client-org-modal">
            <ConvertToClientOrganizationForm hide={props.hide} organizationId={organizationId} callback={callback} />
        </Modal>
    );
};
