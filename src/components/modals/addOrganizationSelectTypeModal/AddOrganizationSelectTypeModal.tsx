import React, { FunctionComponent } from "react";
import { IModalProps, Modal, useModal } from "../../ui/modal";
import { Button } from "../../ui/buttons";
import { AddClientOrganizationModal } from "../addClientOrganizationModal";
import { AddNonClientOrganizationModal } from "../addNonClientOrganizationModal";
import "./AddOrganizationSelectTypeModal.scss";

interface IAddOrganizationSelectTypeModalProps extends IModalProps {
    onAddClientOrganizationCallback?: (currentOrganization?: { id: string; name: string }) => void;
    onAddNonClientOrganizationCallback?: (nonClientOrganization?: { id: string; name: string }) => void;
}

export const AddOrganizationSelectTypeModal: FunctionComponent<IAddOrganizationSelectTypeModalProps> = ({
    onAddClientOrganizationCallback,
    onAddNonClientOrganizationCallback,
    ...props
}) => {
    const {
        hide: hideAddClientOrganizationsModal,
        open: openAddClientOrganizationsModal,
        isOpen: isOpenAddClientOrganizationsModal,
    } = useModal();
    const {
        hide: hideAddNonClientOrganizationsModal,
        open: openAddNonClientOrganizationsModal,
        isOpen: isOpenAddNonClientOrganizationsModal,
    } = useModal();

    function openClientOrganizationModal() {
        props.hide();
        openAddClientOrganizationsModal();
    }

    function openNonClientOrganizationModal() {
        props.hide();
        openAddNonClientOrganizationsModal();
    }

    return (
        <>
            <Modal {...props}>
                <h2 className="h1 b-add-organization-select-type-modal__title">Select organization type</h2>
                <Button text="Client" onClick={openClientOrganizationModal} className="b-add-organization-select-type-modal__btn" />
                <Button text="Non-Client" onClick={openNonClientOrganizationModal} />
            </Modal>
            <AddClientOrganizationModal
                hide={hideAddClientOrganizationsModal}
                isOpen={isOpenAddClientOrganizationsModal}
                callback={onAddClientOrganizationCallback}
            />
            <AddNonClientOrganizationModal
                hide={hideAddNonClientOrganizationsModal}
                isOpen={isOpenAddNonClientOrganizationsModal}
                callback={onAddNonClientOrganizationCallback}
            />
        </>
    );
};
