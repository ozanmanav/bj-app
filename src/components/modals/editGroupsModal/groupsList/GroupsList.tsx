import React, { FunctionComponent } from "react";
import "./GroupsList.scss";
import { IGroup } from "../../../../config/types";
import { GroupListItem } from "./GroupListItem";
import { SortButton } from "../../../ui/buttons";
import { useSortingTable } from "../../../../hooks";
import orderBy from "lodash.orderby";

interface IGroupsListProps {
    groups: IGroup[];
    callback: () => void;
}

export const GroupsList: FunctionComponent<IGroupsListProps> = ({ groups, callback }) => {
    const { changeSortBy, sortBy, sortingOrder, getSortOrderForField } = useSortingTable({ localStorageKey: "admin_groups_list" });

    const sortedGroups = orderBy(groups, [sortBy], [sortingOrder]);

    function sortByName() {
        changeSortBy("name");
    }

    return (
        <>
            <h3 className="b-groups-list__title">Groups</h3>
            {groups.length > 0 ? (
                <div className="b-table__wrapper">
                    <table className="b-table">
                        <thead className="b-table__head">
                            <tr className="b-table__row _head">
                                <th className="b-table__cell">
                                    <SortButton text="Group name" onClick={sortByName} sorted={getSortOrderForField("name")} />
                                </th>
                            </tr>
                        </thead>
                        <tbody className="b-table__body">
                            {sortedGroups.map((group) => (
                                <GroupListItem key={group.id} {...group} callback={callback} />
                            ))}
                        </tbody>
                    </table>
                </div>
            ) : (
                <p>No groups added.</p>
            )}
        </>
    );
};
