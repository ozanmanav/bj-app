import React, { FunctionComponent, useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import { useModal } from "../../../ui/modal";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../../ui/toasts";
import { Formik } from "formik";
import { Input } from "../../../ui/inputs/input";
import { Icon } from "../../../ui/icons";
import { EditButton, RemoveIconButton } from "../../../ui/buttons";
import { ConfirmModal } from "../../confirmModal";
import { IGroup } from "../../../../config/types";
import gql from "graphql-tag";
import * as Yup from "yup";
import { VALIDATION_ERRORS } from "../../../../config";

const EditGroupSchema = Yup.object().shape({
    name: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
});

const DELETE_GROUP = gql`
    mutation DeleteGroup($id: String!) {
        deleteGroup(id: $id)
    }
`;

const UPDATE_GROUP = gql`
    mutation UpdateGroup($id: String!, $name: String!, $entity: GroupsEntitiesEnum!) {
        updateGroup(id: $id, name: $name, entity: $entity) {
            id
            name
        }
    }
`;

interface IGroupListItemProps extends IGroup {
    callback: () => void;
}

export const GroupListItem: FunctionComponent<IGroupListItemProps> = ({ id, name, entity, callback }) => {
    const [isEditing, setIsEditing] = useState<boolean>(false);

    const [deleteGroupMutation] = useMutation(DELETE_GROUP, {
        variables: {
            id,
        },
    });

    const [updateGroupMutation] = useMutation(UPDATE_GROUP);

    const { isOpen, hide, open } = useModal();

    function openEditForm() {
        setIsEditing(true);
    }

    function hideEditForm() {
        setIsEditing(false);
    }

    async function deleteGroup() {
        try {
            const { errors } = await deleteGroupMutation();

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                hide();
                callback();
                showSuccessToast("Group is successfully deleted.");
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    async function updateGroupName(values: { name: string }) {
        try {
            const { errors } = await updateGroupMutation({
                variables: {
                    id,
                    entity,
                    name: values.name,
                },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                showSuccessToast("Group is successfully updated.");
                hideEditForm();
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    if (isEditing) {
        return (
            <Formik
                onSubmit={updateGroupName}
                initialValues={{ name }}
                validationSchema={EditGroupSchema}
                render={({ values, errors, touched, handleSubmit, handleChange, handleBlur }) => (
                    <tr className="b-table__row">
                        <td className="b-table__cell b-groups-list__edit">
                            <form onSubmit={handleSubmit} className="flex align-center justify-between">
                                <Input
                                    name="name"
                                    placeholder="Group name"
                                    value={values.name}
                                    error={errors && errors.name}
                                    touched={touched && touched.name}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    marginBottom="none"
                                />
                                <div className="b-groups-list__item-actions flex align-center justify-end">
                                    <button type="submit" className="simple-button">
                                        <Icon icon="checkBlue" />
                                    </button>
                                    <RemoveIconButton onClick={hideEditForm} />
                                </div>
                            </form>
                        </td>
                    </tr>
                )}
            />
        );
    }

    return (
        <tr className="b-table__row">
            <td className="b-table__cell">
                <div className="flex align-center justify-between">
                    <span className="b-groups-list__item-name">{name}</span>
                    <div className="b-groups-list__item-actions flex align-center justify-end">
                        <EditButton onClick={openEditForm} />
                        <RemoveIconButton onClick={open} />
                    </div>
                    <ConfirmModal title="Are you sure?" onConfirm={deleteGroup} hide={hide} isOpen={isOpen} />
                </div>
            </td>
        </tr>
    );
};
