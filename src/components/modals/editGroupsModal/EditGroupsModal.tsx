import React, { FunctionComponent } from "react";
import "./EditGroupsModal.scss";
import { IModalProps, Modal, ModalFooter } from "../../ui/modal";
import { TAdminEntityTypes } from "../../tables/adminTypesTable/config";
import { AddTypeGroupForm } from "../../forms/addTypeGroupForm";
import { Button } from "../../ui/buttons";
import { Loading } from "../../ui/loading";
import { GroupsList } from "./groupsList";
import { IGroup } from "../../../config/types";

interface IEditGroupsModalProps extends IModalProps {
    entityType: TAdminEntityTypes;
    entityTypeName: string;
    groups: IGroup[];
    loading: boolean;
    refetch: () => void;
}

export const EditGroupsModal: FunctionComponent<IEditGroupsModalProps> = ({
    entityType,
    entityTypeName,
    loading,
    groups,
    refetch,
    ...props
}) => {
    return (
        <Modal {...props}>
            <h2 className="h1 b-edit-groups-modal__title">Edit {entityTypeName} groups</h2>
            <AddTypeGroupForm entityType={entityType} callback={refetch} />
            <div>{loading ? <Loading /> : <GroupsList groups={groups} callback={refetch} />}</div>
            <ModalFooter>
                <Button text="Done" icon="check" primary onClick={props.hide} />
            </ModalFooter>
        </Modal>
    );
};
