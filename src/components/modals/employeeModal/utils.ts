import { IEmployee } from "../../../config";
import { IEmployeeDefaultState, employeeDefaultState } from "../../forms/employeeForm/definitions";

export function prepareContactInitialValues(employee?: IEmployee): IEmployeeDefaultState | undefined {
    if (!employee || !employee.user) {
        return;
    }

    const splittedPhone = employee.user.phone && employee.user.phone.split(" ");
    const splittedMobilePhone = employee.user.mobile_phone && employee.user.mobile_phone.split(" ");

    return {
        ...employeeDefaultState,
        ...employee.user,
        job_title: (employee.job && employee.job.name) || "",
        phone: splittedPhone ? splittedPhone[splittedPhone.length - 1] : "",
        phone_code: splittedPhone ? splittedPhone.slice(0, splittedPhone.length - 1).join(" ") : "",
        mobile_phone: splittedMobilePhone ? splittedMobilePhone[splittedMobilePhone.length - 1] : "",
        mobile_phone_code: splittedMobilePhone ? splittedMobilePhone.slice(0, splittedMobilePhone.length - 1).join(" ") : "",
    };
}
