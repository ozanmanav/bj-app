import React, { FunctionComponent, useCallback, useState, useEffect } from "react";
import "./EmployeeModal.scss";
import { Modal, IModalProps, handleGraphQLErrors, showErrorToast, showSuccessToast, Loading } from "../../ui";
import { EmployeeForm } from "../../forms/employeeForm";
import { useMutation, useApolloClient } from "@apollo/react-hooks";
import { IEmployee } from "../../../config/types";
import { prepareContactInitialValues } from "./utils";
import get from "lodash.get";
import { IEmployeeDefaultState } from "../../forms/employeeForm/definitions";
import { EMPLOYEE, GET_EMPLOYEE } from "./queries";

interface IEmployeeModal extends IModalProps {
    organizationID: string;
    employee?: IEmployee;
    refetch?: Function;
    onSubmitSuccess?: (employee: IEmployee) => void;
    title?: string;
}

export const EmployeeModal: FunctionComponent<IEmployeeModal> = ({
    organizationID,
    employee,
    onSubmitSuccess,
    refetch,
    title,
    ...props
}) => {
    const apolloClient = useApolloClient();
    const [loading, setLoading] = useState<Boolean>(false);
    const [contactData, setContactData] = useState<IEmployee>();

    const getEmployee = useCallback(
        async function() {
            try {
                setLoading(true);
                const { data } = await apolloClient.query({
                    query: GET_EMPLOYEE,
                    variables: {
                        id: employee && employee.id,
                    },
                    fetchPolicy: "no-cache",
                });

                setContactData(get(data, "employee.data[0]"));
                setLoading(false);
            } catch (e) {
                showErrorToast(e.message);
            }
        },
        [employee, apolloClient]
    );

    useEffect(() => {
        if (employee && employee.id && props.isOpen) {
            getEmployee();
        }
    }, [employee, props.isOpen]);

    const [createEmployee] = useMutation(EMPLOYEE);

    const onSubmit = async (state: IEmployeeDefaultState) => {
        try {
            const options = {
                variables: {
                    ...state,
                    id: employee ? employee.id : null,
                    phone: state.phone ? state.phone_code + " " + state.phone : null,
                    mobile_phone: state.mobile_phone ? state.mobile_phone_code + " " + state.mobile_phone : null,
                    organization_id: organizationID,
                },
            };

            const res = await createEmployee(options);
            const { errors } = res;

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                if (onSubmitSuccess) {
                    onSubmitSuccess(res.data.employee);
                }

                refetch && refetch();

                props.hide();

                showSuccessToast(
                    `${res.data.employee.first_name} ${res.data.employee.last_name} is successfully ${employee ? "updated" : "added"}`
                );
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    };

    return (
        <Modal {...props}>
            {loading ? (
                <Loading />
            ) : (
                <EmployeeForm
                    onSubmit={onSubmit}
                    initialValues={prepareContactInitialValues(contactData)}
                    title={title}
                    organizationID={organizationID}
                />
            )}
        </Modal>
    );
};
