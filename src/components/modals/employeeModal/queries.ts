import gql from "graphql-tag";

export const EMPLOYEE = gql`
    mutation Employee(
        $organization_id: String!
        $id: String
        $first_name: String
        $last_name: String
        $email: String
        $job_title: String
        $phone: String
        $mobile_phone: String
        $language: String
        $note: String
    ) {
        employee(
            organization_id: $organization_id
            id: $id
            first_name: $first_name
            last_name: $last_name
            email: $email
            job_title: $job_title
            phone: $phone
            mobile_phone: $mobile_phone
            language: $language
            note: $note
        ) {
            id
            job {
                id
                type
                name
            }
            user {
                first_name
                last_name
                phone
                email
            }
        }
    }
`;

export const GET_EMPLOYEE = gql`
    query GetEmployee($id: String!) {
        employee(id: $id) {
            data {
                id
                job {
                    id
                    name
                    type
                }
                user {
                    id
                    name
                    first_name
                    last_name
                    email
                    phone
                    active
                }
            }
        }
    }
`;
