import React, { FunctionComponent } from "react";
import { IModalProps, Modal } from "../../ui/modal";
import { AdminUserForm } from "../../forms/adminUserForm";
import { IAdminUser } from "../../tables/adminRelatedUsersTable/config";

interface IAdminUserModal extends IModalProps {
    refetch?: Function;
    user?: IAdminUser;
}

export const AdminUserModal: FunctionComponent<IAdminUserModal> = ({ refetch, user, ...props }) => {
    return (
        <Modal {...props}>
            <AdminUserForm callback={props.hide} refetch={refetch} user={user} />
        </Modal>
    );
};
