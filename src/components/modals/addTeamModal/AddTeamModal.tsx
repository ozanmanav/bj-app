import React, { FunctionComponent } from "react";
import { IModalProps, Modal } from "../../ui/modal";
import { AddTeamForm, IAddTeamFormState } from "../../forms/addTeamForm";
import { ILocalAutocompleteOption } from "../../ui/inputs";

interface IAddTeamModalProps extends IModalProps {
    refetch: () => void;
    users: ILocalAutocompleteOption[];
    organizationID: string;
    editedTeam?: IAddTeamFormState;
}

export const AddTeamModal: FunctionComponent<IAddTeamModalProps> = ({ refetch, users, organizationID, editedTeam, ...modalProps }) => {
    const callback = () => {
        refetch();
        modalProps.hide();
    };

    return (
        <Modal {...modalProps}>
            <AddTeamForm users={users} editedTeam={editedTeam} organizationID={organizationID} callback={callback} />
        </Modal>
    );
};
