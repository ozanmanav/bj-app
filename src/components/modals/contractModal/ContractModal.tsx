import React, { FunctionComponent } from "react";
import "./ContractModal.scss";
import { IModalProps, Modal } from "../../ui/modal";
import { IOrganizationFormState, ORGANIZATION_FORM_DEFAULT_STATE, OrganizationForm } from "../../forms/organizationForm";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { Loading } from "../../ui/loading";
import get from "lodash.get";
import { getInitialValues } from "./utils";
import { useSendContract, useUpdateContract } from "../../../hooks/apollo";

// TODO: Details deleted from this query should implement another place
const GET_CONTRACT = gql`
    query GetContract($id: String!) {
        contracts(id: $id) {
            data {
                id
                role_id
                type_id
                organization {
                    id
                    name
                    category
                }
                objects {
                    id
                    name
                    type {
                        id
                    }
                }
                employees {
                    id
                    user {
                        first_name
                        last_name
                        phone
                        email
                    }
                    job {
                        name
                        id
                    }
                }

                files {
                    id
                    type {
                        id
                        type
                        name
                    }
                    name
                    url
                    filename
                    extension
                    size
                    events {
                        id
                        timeline
                    }
                    updated_at
                    created_at
                    details {
                        context
                        type
                        value
                        name
                        meta {
                            last_modification {
                                modifier {
                                    name
                                }
                            }
                            author {
                                name
                            }
                            context
                            verification
                            verification_by
                            source_url
                            source_date
                            source
                        }
                    }
                    mime_type
                }
            }
        }
    }
`;

const GET_CONTRACT_DETAILS = gql`
    query GetContractDetails($model_id: String!, $model: DetailsModelEnum!) {
        details(model_id: $model_id, model: $model) {
            data {
                id
                name
                type
                value
                context
                meta {
                    source
                    source_url
                    source_date
                    verification
                    verification_by
                }
            }
        }
    }
`;

const GET_OBJECT_NAME = gql`
    query GetObjectName($id: String!) {
        objects(id: $id) {
            data {
                name
            }
        }
    }
`;

interface IContractModalProps extends IModalProps {
    objectID: string;
    refetch: () => void;
    contractID?: string;
}

export const ContractModal: FunctionComponent<IContractModalProps> = ({ contractID, objectID, refetch, ...modalProps }) => {
    const addContract = useSendContract(contractSendCallback);
    const updateContract = useUpdateContract(contractSendCallback);

    const { data: contractData, loading } = useQuery(GET_CONTRACT, {
        variables: {
            id: contractID,
        },
        fetchPolicy: "no-cache",
    });

    const { data: objectData } = useQuery(GET_OBJECT_NAME, {
        variables: {
            id: objectID,
        },
    });

    const { data: contractDetails } = useQuery(GET_CONTRACT_DETAILS, {
        variables: {
            model_id: contractID,
            model: "CONTRACT",
        },
        fetchPolicy: "no-cache",
    });

    const objectName = get(objectData, "objects.data[0].name");
    const contract = { ...get(contractData, "contracts.data[0]"), details: get(contractDetails, "details.data") };

    const initialValues = contractID && contract ? getInitialValues(contract, objectID) : ORGANIZATION_FORM_DEFAULT_STATE;

    const organizationName = get(contractData, "contracts.data[0].organization.name");
    const organizationCategory = get(contractData, "contracts.data[0].organization.category");
    const isClientOrganization = organizationCategory === "CLIENT";

    function sendContract(contract: IOrganizationFormState) {
        if (organizationName) {
            updateContract({
                contractID,
                contractState: contract,
                objectID,
                initialValues,
            });
        } else {
            addContract({
                contractState: contract,
                objectID,
            });
        }
    }

    function contractSendCallback() {
        refetch();
        modalProps.hide();
    }

    return (
        <Modal {...modalProps} className="b-contract-modal">
            {loading && contractID ? (
                <Loading />
            ) : (
                <OrganizationForm
                    onSubmit={sendContract}
                    initialValues={initialValues}
                    organizationName={organizationName}
                    enableReinitialize
                    objectName={objectName}
                    isClientOrganization={isClientOrganization}
                />
            )}
        </Modal>
    );
};
