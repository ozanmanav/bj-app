import { IContract } from "../../../config/types";
import { IOrganizationFormState } from "../../forms/organizationForm";
import get from "lodash.get";
import { IInfoRowParams } from "../../forms";

export function getInitialValues(contract: IContract, objectID: string): IOrganizationFormState {
    const defaultMetaState = {
        source: "",
        source_url: "",
        source_date: "",
        verification: false,
        verification_by: "",
    };
    const contractDetails: IInfoRowParams[] = (get(contract, "details") || []).map((i) => ({
        ...i,
        meta: i.meta === null ? defaultMetaState : i.meta,
    }));
    // const contractDetailsMeta = get(contract, "details.meta") === null ? defaultMetaState : get(contract, "details.meta");

    return {
        relationships: [
            {
                organization_id: get(contract, "organization.id") || "",
                role: contract.role_id,
                objects: contract.objects ? contract.objects.filter(({ id }: any) => id !== objectID) : [],
                files: contract.files || [],
                contacts: contract.employees,
            },
        ],
        files: contract.files || [],
        rows: {
            contractInformation: contractDetails || [],
        },
    };
}
