import React, { FunctionComponent } from "react";
import "./AddSubsidiaryModalForm.scss";
import { ModalFooter } from "../../ui/modal";
import { Button, RemoveObjectButton } from "../../ui/buttons";
import { FormikProps } from "formik";
import { AutocompleteInput } from "../../ui/inputs/autocomplete";
import { ISubsidiary } from "../../../config/types";
import { showErrorToast } from "../../ui";
import get from "lodash.get";

export interface IAddSubsidiaryModalFormState {
    organizations: ISubsidiary[];
}

interface IAddSubsidiaryModalFormProps extends FormikProps<IAddSubsidiaryModalFormState> {
    title: string;
    organizationID: string;
    isClient?: boolean;
    isParent?: boolean;
}

export const AddSubsidiaryModalForm: FunctionComponent<IAddSubsidiaryModalFormProps> = ({
    values,
    errors,
    setFieldValue,
    handleSubmit,
    title,
    organizationID,
    isClient,
    isParent,
}) => {
    const addSubsidiary = (subsidiary: ISubsidiary) => {
        const prevValues = get(values, "organizations");

        if (prevValues) {
            if (organizationID === subsidiary.id) {
                return showErrorToast("You can not add subsidiary with same organization");
            }

            if (!prevValues.find((v) => v.id === subsidiary.id)) {
                setFieldValue("organizations", [...prevValues, subsidiary]);
            }
        }
    };

    const removeSubsidiary = (subsidiary: ISubsidiary) => {
        const prevValues = get(values, "organizations");

        if (prevValues) {
            setFieldValue(
                "organizations",
                prevValues.filter((v) => v.id !== subsidiary.id)
            );
        }
    };

    return (
        <>
            <form onSubmit={handleSubmit}>
                <h2 className="h1 f-add-subsidiary__title">{title}</h2>
                <div className="f-add-subsidiary__group">
                    <AutocompleteInput
                        className="f-add-subsidiary__autocomplete"
                        placeholder="Search organizations"
                        searchFor={
                            isParent
                                ? isClient
                                    ? "allOrganizations"
                                    : "nonClientOrganizations"
                                : isClient
                                ? "clientOrganizations"
                                : "allOrganizations"
                        }
                        position="top"
                        resultsContainerSize="small"
                        onChange={addSubsidiary}
                        isSetValue={false}
                    />

                    {values.organizations.length > 0 && (
                        <div className="f-add-subsidiary__current flex align-center flex-wrap">
                            {values.organizations.map((subsidiary) => (
                                <RemoveSubsidiaryButton
                                    key={subsidiary.id}
                                    subsidiary={subsidiary}
                                    remove={removeSubsidiary}
                                />
                            ))}
                        </div>
                    )}
                </div>
                <ModalFooter>
                    <Button text="Save" primary type="submit" />
                </ModalFooter>
            </form>
        </>
    );
};

interface IRemoveSubsidiaryButtonProps {
    subsidiary: ISubsidiary;
    remove: (subsidiary: ISubsidiary) => void;
}

const RemoveSubsidiaryButton: FunctionComponent<IRemoveSubsidiaryButtonProps> = ({ subsidiary, remove }) => {
    function removeSelf() {
        remove(subsidiary);
    }

    return <RemoveObjectButton text={subsidiary.name} onClick={removeSelf} />;
};
