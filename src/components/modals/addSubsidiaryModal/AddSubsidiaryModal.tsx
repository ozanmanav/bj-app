import React, { FunctionComponent } from "react";
import { IModalProps, Modal } from "../../ui/modal";
import { AddSubsidiaryModalForm, IAddSubsidiaryModalFormState } from "./AddSubsidiaryModalForm";
import { Formik } from "formik";
import { ISubsidiary } from "../../../config/types";

interface IAddSubsidiaryModalProps extends IModalProps {
    subsidiaryOrganizations: ISubsidiary[];
    title: string;
    organizationID: string;
    isClient?: boolean;
    isParent?: boolean;
    onSubsidiariesSubmit: (values: IAddSubsidiaryModalFormState) => void;
}

export const AddSubsidiaryModal: FunctionComponent<IAddSubsidiaryModalProps> = ({
    subsidiaryOrganizations,
    onSubsidiariesSubmit,
    title,
    organizationID,
    isClient,
    isParent,
    ...props
}) => {
    function updateSubsidiaries(values: IAddSubsidiaryModalFormState) {
        onSubsidiariesSubmit(values);
    }

    return (
        <Modal {...props}>
            <Formik
                onSubmit={updateSubsidiaries}
                initialValues={{
                    organizations: subsidiaryOrganizations,
                }}
                render={(formikProps) => (
                    <AddSubsidiaryModalForm
                        isClient={isClient}
                        title={title}
                        isParent={isParent}
                        organizationID={organizationID}
                        {...formikProps}
                    />
                )}
            />
        </Modal>
    );
};
