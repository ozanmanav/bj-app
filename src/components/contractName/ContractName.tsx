import React, { FunctionComponent } from "react";
import { Icon } from "../ui/icons";
import "./ContractName.scss";
import { IOrganization } from "../../config";
import get from "lodash.get";
import { Link } from "react-router-dom";

interface IContractNameProps {
    name: string;
    type: string;
    id?: string;
    typeLabel?: string;
    organization?: IOrganization;
}

export const ContractName: FunctionComponent<IContractNameProps> = ({ name, type, id, organization }) => {
    const organizationName = get(organization, "name");
    const organizationID = get(organization, "id");
    const isClientOrganization = get(organization, "category") === "CLIENT";
    const redirectUrl = isClientOrganization
        ? `/app/organizations/client/${organizationID}/relationships`
        : `/app/organizations/${organizationID}/relationships`;
    const nameElClassName = `_font-bold ${type === "company" ? "_text-primary" : "_text-black"} b-contract-name__name`;

    return (
        <Link className="flex" to={redirectUrl}>
            <Icon icon={type} className="b-contract-name__type-icon _small" />
            <div className="flex flex-column justify-center">
                <p className={nameElClassName} title={organizationName}>
                    {organizationName}
                </p>
            </div>
        </Link>
    );
};
