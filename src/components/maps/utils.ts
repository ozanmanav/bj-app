import {
    SWISS_MAP_API_URL,
    SWISS_MAP_BUILDING_DETAILS_LAYER_NAME,
    GOOGLE_MAPS_RESTICTION_WIDTH_RATIO,
    INVALID_BUILDING_ADDRESS_ERROR,
    MARKER_FEATURE_NAME,
    PDF_FILES_BASE_URL,
} from "./config";
import get from "lodash.get";

export async function searchByAddress(address: string, limit: number = 1) {
    return fetch(
        `${SWISS_MAP_API_URL}SearchServer?type=featuresearch&features=${SWISS_MAP_BUILDING_DETAILS_LAYER_NAME}&searchText=${encodeURI(
            address
        )}&limit=${limit}`
    ).then((res) => res.json());
}

export async function getBuildingDataByFeatureID(featureID: string, cantonAbbr: string) {
    const data = await Promise.all([
        fetch(`${SWISS_MAP_API_URL}/MapServer/${SWISS_MAP_BUILDING_DETAILS_LAYER_NAME}/${featureID}`).then((res) => res.json()),
        fetch(`${SWISS_MAP_API_URL}/SearchServer?type=locations&origins=kantone&searchText=${cantonAbbr}`).then((res) => res.json()),
    ]);

    if (!data[0].feature) {
        throw new Error(INVALID_BUILDING_ADDRESS_ERROR);
    }

    const attributes = get(data[0], "feature.attributes");
    // removing html tags from result with regex (<b>Bern</b> -> Bern)
    const canton = get(data[1], "results[0].attrs.label").replace(/(<([^>]+)>)/gi, "");

    const egid = attributes.egid;
    const strasse = attributes.strname_de;
    const nr = attributes.deinr;
    const plz = attributes.plz4;
    const bfsNummer = attributes.gdenr;
    const gemeinde = attributes.gdename;
    const ort = attributes.plzname;
    const buildingDetailsLink = PDF_FILES_BASE_URL + egid;

    return {
        egid,
        strasse,
        nr,
        plz,
        canton,
        bfsNummer,
        gemeinde,
        ort,
        buildingDetailsLink,
    };
}

export function getSwissMapIcon(coordinates: any, swissMarkerStyle: any) {
    // create icon
    // @ts-ignore
    const iconFeature = new ol.Feature({
        // @ts-ignore
        geometry: new ol.geom.Point(coordinates),
        name: MARKER_FEATURE_NAME,
    });

    // apply icon style
    iconFeature.setStyle(swissMarkerStyle);

    // create vector
    // @ts-ignore
    return new ol.source.Vector({
        features: [iconFeature],
    });
}

export function loadSwissgeoResources(cb: Function) {
    if (!document.querySelector('link[href="https://public.geo.admin.ch/resources/api/4.4.2/ga.css"]')) {
        document.head.insertAdjacentHTML(
            "beforeend",
            '<link rel="stylesheet" type="text/css" href="https://public.geo.admin.ch/resources/api/4.4.2/ga.css" />'
        );
    }

    const scriptURLs = [
        "//cdnjs.cloudflare.com/ajax/libs/proj4js/2.2.1/proj4.js",
        "https://public.geo.admin.ch/resources/api/4.4.2/EPSG21781.js",
        "https://public.geo.admin.ch/resources/api/4.4.2/EPSG2056.js",
        "https://public.geo.admin.ch/resources/api/4.4.2/ga.js",
    ];
    let loadedScripts = 0;

    scriptURLs.forEach((url) => {
        if (document.querySelector(`script[src="${url}"]`)) {
            loadedScripts++;

            if (loadedScripts === scriptURLs.length) {
                cb();
            }

            return;
        }

        const script = document.createElement("script");
        script.src = url;
        script.async = false;

        script.addEventListener("load", () => {
            loadedScripts++;

            if (loadedScripts === scriptURLs.length) {
                cb();
            }
        });

        document.body.appendChild(script);
    });
}

export function calcSeparatorX(isDragging: boolean, currentTranslate: number, deltaX: number, wrapperWidth: number) {
    return restrictTranslate(
        (wrapperWidth * (1 - GOOGLE_MAPS_RESTICTION_WIDTH_RATIO * 2)) / 2,
        isDragging ? currentTranslate + deltaX : currentTranslate
    );
}

function restrictTranslate(restrictValue: number, value: number) {
    return Math.abs(value) > restrictValue ? restrictValue * Math.sign(value) : value;
}

export function restrictGoogleMapWidth(width: number) {
    const max = (1 - GOOGLE_MAPS_RESTICTION_WIDTH_RATIO) * 100;
    const min = GOOGLE_MAPS_RESTICTION_WIDTH_RATIO * 100;

    if (width > max) {
        return max;
    } else if (width < min) {
        return min;
    }

    return width;
}

export function formatAddress(address: string) {
    const addressWords = address.split(" ");

    const buildingNumberIndex = addressWords.findIndex((word) => /\d/.test(word));

    if (buildingNumberIndex === -1) {
        return address;
    }

    return addressWords.slice(0, buildingNumberIndex + 1).join(" ") + ", " + addressWords.slice(buildingNumberIndex + 1).join(" ");
}
