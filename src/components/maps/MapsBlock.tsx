import React, { FunctionComponent, useEffect, useRef, useState } from "react";
import "./MapsBlock.scss";
import { ISwissMapFeatureInfo, useLoadScript } from "../../hooks";
import {
    DEFAULT_GOOGLE_MAP_WIDTH,
    GOOGLE_MAPS_MARKER_ZOOM,
    GOOGLE_MAPS_OPTIONS,
    MARKER_OFFSET_X,
    MARKER_OFFSET_Y,
    SWISS_MAP_MARKER_ZOOM,
    SWISS_MAP_MARKERS_LAYER_NAME,
    SWISS_MAP_OPTIONS,
    SWISS_MAP_PROJECTION,
    MARKER_FEATURE_NAME,
    TOOLTIP_OFFSET_X,
    TOOLTIP_OFFSET_Y,
    SWISS_MAPS_API_DEBOUNCE,
    SWISS_MAPS_DETAILS_DEFAULT_META,
} from "./config";
import markerIcon from "./marker.svg";
import {
    calcSeparatorX,
    getBuildingDataByFeatureID,
    getSwissMapIcon,
    loadSwissgeoResources,
    restrictGoogleMapWidth,
    searchByAddress,
} from "./utils";
import { useGesture } from "react-with-gesture";
import get from "lodash.get";
import { showErrorToast } from "../ui/toasts";

interface IMapsBlockProps {
    defaultAddress?: string;
    feature?: ISwissMapFeatureInfo;
    setFieldValue?: Function;
    setIsAddressValid?: Function;
}

const GOOGLE_MAPS_API_JS_URL = `https://maps.googleapis.com/maps/api/js?key=${process.env.REACT_APP_GOOGLE_MAPS_API_KEY}&libraries=places`;
const SWISS_GEO_MAPS_JS = "https://api3.geo.admin.ch/loader.js?lang=en&version=4.4.2";

export const MapsBlock: FunctionComponent<IMapsBlockProps> = ({ defaultAddress, feature, setFieldValue, setIsAddressValid }) => {
    // maps logic
    useLoadScript(GOOGLE_MAPS_API_JS_URL, initGoogleMaps);
    useLoadScript(SWISS_GEO_MAPS_JS, initSwissMap);

    const isGoogleMapsInitialized = useRef<boolean>(false);
    const isSwissMapsInitialized = useRef<boolean>(false);

    const googleMapWrapperRef = useRef<HTMLDivElement>(null);
    const googleMap = useRef<any>();
    const googleMapSearch = useRef<any>();
    const googleMapInfoWindow = useRef<any>();
    const googleMapIcon = useRef<any>();
    const googleMapMarkers = useRef<any[]>([]);

    const swissMap = useRef<any>();
    const swissMarkerStyle = useRef<any>();
    const swissMapMarkerPopup = useRef<HTMLDivElement>(null);

    function clearGoogleMapMarkers() {
        googleMapMarkers.current.forEach((marker) => marker.setMap(null));
    }

    function clearSwissMapMarkers() {
        swissMap.current.getLayers().forEach((layer: any) => {
            if (layer.get("name") === SWISS_MAP_MARKERS_LAYER_NAME) {
                layer.setSource(null);
            }
        });

        if (swissMapMarkerPopup.current) {
            swissMapMarkerPopup.current.innerHTML = "";
        }
    }

    function invalidateAddress() {
        setIsAddressValid && setIsAddressValid(false);
        setFieldValue && setFieldValue("addressDetails", []);
        clearGoogleMapMarkers();
        clearSwissMapMarkers();
    }

    function validateAddress() {
        setIsAddressValid && setIsAddressValid(true);
    }

    function initSwissMap() {
        if (!isSwissMapsInitialized.current) {
            loadSwissgeoResources(() => {
                // @ts-ignore
                if (!window.ga) {
                    return;
                }

                // @ts-ignore
                const layer = ga.layer.create("ch.swisstopo.pixelkarte-farbe");

                // @ts-ignore
                const markersLayer = new ol.layer.Vector({
                    source: null,
                    name: SWISS_MAP_MARKERS_LAYER_NAME,
                });

                // @ts-ignore
                swissMap.current = new ga.Map({
                    target: "swiss-map",
                    layers: [layer, markersLayer],
                    controls: [],
                    // @ts-ignore
                    view: new ol.View(SWISS_MAP_OPTIONS),
                });

                // @ts-ignore
                const popup = new ol.Overlay({
                    element: swissMapMarkerPopup.current,
                    offset: [TOOLTIP_OFFSET_X, TOOLTIP_OFFSET_Y],
                    positioning: "top-center",
                });

                swissMap.current.addOverlay(popup);

                swissMapMarkerPopup.current && swissMapMarkerPopup.current.classList.add("_initialized");

                swissMap.current.on("click", (e: any) => {
                    const markerFeature = swissMap.current.forEachFeatureAtPixel(e.pixel, function(feature: any) {
                        if (feature.getProperties().name === MARKER_FEATURE_NAME) {
                            return feature;
                        }

                        return null;
                    });

                    if (markerFeature) {
                        const coordinates = markerFeature.getGeometry().getCoordinates();

                        popup.setPosition(coordinates);
                    } else {
                        popup.setPosition(null);
                    }
                });

                // set icon style
                // @ts-ignore
                swissMarkerStyle.current = new ol.style.Style({
                    // @ts-ignore
                    image: new ol.style.Icon({
                        anchor: [MARKER_OFFSET_X, MARKER_OFFSET_Y],
                        anchorXUnits: "pixels",
                        anchorYUnits: "pixels",
                        opacity: 1,
                        src: markerIcon,
                    }),
                });

                isSwissMapsInitialized.current = true;
            });
        }
    }

    function initGoogleMaps() {
        if (!isGoogleMapsInitialized.current) {
            isGoogleMapsInitialized.current = true;
            const googleMapOptions = {
                ...GOOGLE_MAPS_OPTIONS,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                    position: google.maps.ControlPosition.LEFT_BOTTOM,
                },
                mapTypeId: google.maps.MapTypeId.HYBRID,
            };

            if (googleMapWrapperRef.current) {
                googleMap.current = new google.maps.Map(googleMapWrapperRef.current, googleMapOptions);
            }

            googleMapSearch.current = new google.maps.places.PlacesService(googleMap.current);
            googleMapInfoWindow.current = new google.maps.InfoWindow();
            googleMapIcon.current = {
                anchor: new google.maps.Point(MARKER_OFFSET_X, MARKER_OFFSET_Y),
                url: markerIcon,
            };
        }
    }

    async function updateMaps(feature: ISwissMapFeatureInfo) {
        try {
            // get the data
            const { lat, lon, detail, featureId } = feature;

            if (!featureId) {
                invalidateAddress();
                return;
            }

            const cantoneAbbr = get(detail.match(/_(.{2})_/), "[1]");

            // fetching required data from "ch.bfs.gebaeude_wohnungs_register" layer
            const { egid, nr, plz, strasse, canton, bfsNummer, gemeinde, ort, buildingDetailsLink } = await getBuildingDataByFeatureID(
                featureId,
                cantoneAbbr
            );

            if (setFieldValue) {
                setFieldValue(
                    "addressDetails",
                    [
                        egid && {
                            name: "EGID",
                            type: "string",
                            value: egid,
                            ...SWISS_MAPS_DETAILS_DEFAULT_META,
                        },
                        strasse && {
                            name: "strasse",
                            type: "string",
                            value: strasse,
                            ...SWISS_MAPS_DETAILS_DEFAULT_META,
                        },
                        nr && {
                            name: "nr",
                            type: "string",
                            value: nr,
                            ...SWISS_MAPS_DETAILS_DEFAULT_META,
                        },
                        plz && {
                            name: "PLZ",
                            type: "string",
                            value: plz,
                            ...SWISS_MAPS_DETAILS_DEFAULT_META,
                        },
                        canton && {
                            name: "Kanton",
                            type: "string",
                            value: canton,
                            ...SWISS_MAPS_DETAILS_DEFAULT_META,
                        },
                        ort && {
                            name: "Ort",
                            type: "string",
                            value: ort,
                            ...SWISS_MAPS_DETAILS_DEFAULT_META,
                        },
                        gemeinde && {
                            name: "Gemeinde",
                            type: "string",
                            value: gemeinde,
                            ...SWISS_MAPS_DETAILS_DEFAULT_META,
                        },
                        bfsNummer && {
                            name: "BFS-Nummer",
                            type: "string",
                            value: bfsNummer,
                            ...SWISS_MAPS_DETAILS_DEFAULT_META,
                        },
                        buildingDetailsLink && {
                            name: "Building details",
                            type: "string",
                            value: buildingDetailsLink,
                            ...SWISS_MAPS_DETAILS_DEFAULT_META,
                        },
                    ].filter(Boolean)
                );
            }

            validateAddress();

            const markerTooltipContent = `
                canton: ${canton},
                plz: ${plz}, 
                strasse: ${strasse}, 
                nr: ${nr}, 
                EGID: ${egid}, 
            `;

            if (swissMap.current) {
                // transform coordinates to the map projection
                // @ts-ignore
                const coordinates = ol.proj.fromLonLat([lon, lat], SWISS_MAP_PROJECTION);

                // generate icon
                const vectorSource = getSwissMapIcon(coordinates, swissMarkerStyle.current);

                swissMap.current.getLayers().forEach((layer: any) => {
                    // find a markers layer and replace his source with new icon
                    if (layer.get("name") === SWISS_MAP_MARKERS_LAYER_NAME) {
                        layer.setSource(vectorSource);
                    }
                });

                // centering map
                // @ts-ignore
                swissMap.current.getView().setCenter(coordinates);
                swissMap.current.getView().setZoom(SWISS_MAP_MARKER_ZOOM);

                if (swissMapMarkerPopup.current) {
                    swissMapMarkerPopup.current.innerHTML = markerTooltipContent;
                }
            }

            if (googleMap.current) {
                createMarker(lat, lon, markerTooltipContent);

                googleMap.current.setCenter({ lat, lng: lon });
                googleMap.current.setZoom(GOOGLE_MAPS_MARKER_ZOOM);
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    function createMarker(lat: number, lng: number, detail: string) {
        clearGoogleMapMarkers();

        // @ts-ignore
        const marker = new google.maps.Marker({
            map: googleMap.current,
            position: { lat, lng },
            icon: googleMapIcon.current,
        });

        googleMapMarkers.current.push(marker);

        // @ts-ignore
        google.maps.event.addListener(marker, "click", function() {
            googleMapInfoWindow.current.setContent(detail);
            // @ts-ignore
            googleMapInfoWindow.current.open(googleMap.current, this);
        });
    }

    useEffect(() => {
        function handleAddress() {
            if (!isSwissMapsInitialized.current) {
                setTimeout(() => handleAddress(), SWISS_MAPS_API_DEBOUNCE);

                return;
            }

            // searching for address
            searchByAddress(defaultAddress as string)
                .then(async (res) => {
                    if (!res.results || res.results.length === 0) {
                        invalidateAddress();
                        return;
                    }

                    const firstMatch = res.results[0];

                    updateMaps(firstMatch.attrs);
                })
                .catch(() => invalidateAddress());
        }

        if (defaultAddress) {
            handleAddress();
        }
    }, [defaultAddress]);

    useEffect(() => {
        if (feature) {
            updateMaps(feature);
        }
    }, [feature]);

    // drag logic
    const wrapperRef = useRef<HTMLDivElement>(null);
    const [isDragging, setIsDragging] = useState<boolean>(false);
    const [googleMapWidth, setGoogleMapWidth] = useState<number>(DEFAULT_GOOGLE_MAP_WIDTH);
    const [separatorTranslateX, setSeparatorTranslateX] = useState<number>(0);
    const [
        bind,
        {
            delta: [x],
            down,
        },
    ] = useGesture({
        onDown() {
            setIsDragging(true);
        },
    });

    const wrapperWidth = wrapperRef.current ? wrapperRef.current.clientWidth : 0;
    const resultSeparatorTranslateX = calcSeparatorX(isDragging, separatorTranslateX, x, wrapperWidth);

    // resize
    useEffect(() => {
        if (!isDragging || (down && isDragging)) {
            return;
        }

        // on drag end
        setSeparatorTranslateX(calcSeparatorX(isDragging, separatorTranslateX, x, wrapperWidth));

        if (wrapperRef.current) {
            const deltaPercent = (x / wrapperWidth) * 100;
            setGoogleMapWidth(restrictGoogleMapWidth(googleMapWidth + deltaPercent));
            setIsDragging(false);
        }
    }, [down, x, googleMapWidth, separatorTranslateX, isDragging, wrapperWidth]);

    // updating maps after resize
    useEffect(() => {
        if (swissMap.current) {
            swissMap.current.updateSize();
        }

        if (googleMap.current) {
            const center = googleMap.current.getCenter();
            google.maps.event.trigger(googleMap.current, "resize");
            googleMap.current.setCenter(center);
        }
    }, [googleMapWidth]);

    return (
        <div className="b-maps-block flex align-center" ref={wrapperRef}>
            <div className="b-maps-block__map-container" ref={googleMapWrapperRef} style={{ width: `${googleMapWidth}%` }} />
            <div className="b-maps-block__separator" {...bind()} style={{ transform: `translateX(${resultSeparatorTranslateX}px)` }} />
            <div className="b-maps-block__map-container" id="swiss-map" style={{ width: `${100 - googleMapWidth}%` }}>
                <div className="b-maps-block__popup" ref={swissMapMarkerPopup} />
            </div>
        </div>
    );
};
