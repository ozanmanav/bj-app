import React, { FunctionComponent, useState } from "react";
import "./SwissMapAutocomplete.scss";
import { ISwissMapFeatureInfo, useClickOutside } from "../../../hooks";
import cn from "classnames";

interface ISwissMapAutocompleteProps {
    results: ISwissMapFeatureInfo[];
    onChange: (feature: ISwissMapFeatureInfo) => void;
    className?: string;
}

export const SwissMapAutocomplete: FunctionComponent<ISwissMapAutocompleteProps> = ({ results, onChange, children, className }) => {
    const [showResults, setShowResults] = useState<boolean>(false);

    const wrapperRef = useClickOutside<HTMLDivElement>(closeResults);

    function openResults() {
        setShowResults(true);
    }

    function closeResults() {
        setShowResults(false);
    }

    function onItemClick(item: ISwissMapFeatureInfo) {
        closeResults();
        onChange(item);
    }

    const wrapperClassname = cn(["b-swiss-map-autocomplete__wrapper", { _opened: showResults && results.length > 0 }, className]);

    return (
        <div className={wrapperClassname} onFocusCapture={openResults} ref={wrapperRef}>
            {children}
            {showResults && results.length > 0 && (
                <div className="b-swiss-map-autocomplete__results">
                    {results.map((item) => (
                        <SwissMapAutocompleteItem item={item} onChange={onItemClick} key={item.featureId} />
                    ))}
                </div>
            )}
        </div>
    );
};

interface ISwissMapAutocompleteItemProps {
    item: ISwissMapFeatureInfo;
    onChange: (feature: ISwissMapFeatureInfo) => void;
}

const SwissMapAutocompleteItem: FunctionComponent<ISwissMapAutocompleteItemProps> = ({ item, onChange }) => {
    const { label } = item;

    function onSelfClick() {
        onChange(item);
    }

    return (
        <button className="b-swiss-map-autocomplete__results-item block" onClick={onSelfClick}>
            {label}
        </button>
    );
};
