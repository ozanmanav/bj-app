import React, { FunctionComponent, useState } from "react";
import { FormikProps } from "formik";
import { INVALID_BUILDING_ADDRESS_ERROR } from "./config";
import { SearchInput } from "../ui/inputs";
import { MapsBlock } from "./MapsBlock";
import { ISwissMapFeatureInfo, useSwissMapAutocomplete } from "../../hooks";
import { SwissMapAutocomplete } from "./swissMapAutocomplete";
import { formatAddress } from "./utils";

export interface IMapsBlockWithSearchState {
    address: string;
}

interface IMapsBlockWithSearchProps extends FormikProps<IMapsBlockWithSearchState> {
    addressChangeCallback: (address: string) => void;
}

export const MapsBlockWithSearch: FunctionComponent<IMapsBlockWithSearchProps> = ({
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    addressChangeCallback,
    ...formikProps
}) => {
    const [isAddressValid, setIsAddressValid] = useState<boolean>(true);
    const [selectedFeature, setSelectedFeature] = useState<ISwissMapFeatureInfo>();

    const inputError = [errors.address, !isAddressValid ? INVALID_BUILDING_ADDRESS_ERROR : ""].filter(Boolean).join(". ");

    const searchResult = useSwissMapAutocomplete(values.address);

    function setAddressValidation(isValid: boolean) {
        setIsAddressValid(isValid);
        formikProps.setFieldTouched("address", true, false);
    }

    function onAddressSelect(feature: ISwissMapFeatureInfo) {
        const formattedAddress = formatAddress(feature.label);

        formikProps.setFieldValue("address", formattedAddress);
        setSelectedFeature(feature);

        addressChangeCallback && addressChangeCallback(formattedAddress);
    }

    return (
        <div className="b-maps-block__wrapper">
            <SwissMapAutocomplete results={searchResult} onChange={onAddressSelect} className="b-maps-block__search">
                <SearchInput
                    value={values.address}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={inputError}
                    touched={touched && touched.address}
                    name="address"
                    placeholder="Example: Saustrasse 34, Zurich Switzerland..."
                    marginBottom="none"
                />
            </SwissMapAutocomplete>
            <MapsBlock setFieldValue={formikProps.setFieldValue} feature={selectedFeature} setIsAddressValid={setAddressValidation} />
        </div>
    );
};
