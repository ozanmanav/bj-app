import { format } from "date-fns";
const defaultLat = 46.812875;
const defaultLng = 8.443732;

export const MARKER_OFFSET_X = 30;
export const MARKER_OFFSET_Y = 30;

export const TOOLTIP_OFFSET_X = 0;
export const TOOLTIP_OFFSET_Y = -80;

export const DEFAULT_GOOGLE_MAP_WIDTH = 50;
export const GOOGLE_MAPS_RESTICTION_WIDTH_RATIO = 0.1;

export const GOOGLE_MAPS_OPTIONS = {
    zoom: 7.2,
    center: { lat: defaultLat, lng: defaultLng },
    disableDefaultUI: true,
};
export const GOOGLE_MAPS_MARKER_ZOOM = 17;

export const SWISS_MAP_OPTIONS = {};
export const SWISS_MAP_API_URL = "https://api3.geo.admin.ch/rest/services/api/";
export const SWISS_MAP_BUILDING_DETAILS_LAYER_NAME = "ch.bfs.gebaeude_wohnungs_register";
export const SWISS_MAP_PROJECTION = "EPSG:2056";
export const SWISS_MAP_MARKERS_LAYER_NAME = "markersLayer";
export const SWISS_MAP_MARKER_ZOOM = 10;

export const PDF_FILES_BASE_URL = "https://www.housing-stat.ch/regbl/resources/public/geb_public/";

export const INVALID_BUILDING_ADDRESS_ERROR = "Invalid building address";

export const MARKER_FEATURE_NAME = "marker-feature";

export const SWISS_MAPS_API_DEBOUNCE = 300;

export const SWISS_MAPS_DETAILS_DEFAULT_META = {
    source: "Eidgenössisches Gebäude- und Wohnungsregister (GWR)",
    source_url: "map.geo.admin.ch",
    source_date: format(new Date(), "yyyy-M-d H:mm:ss"),
    verification: true,
    verification_by: "Bundesamt für Statistik, Sektion Gebäude und Wohnungen",
};
