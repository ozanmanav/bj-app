import React, { FunctionComponent } from "react";
import "./RelationalLists.scss";
import logo from "./logo.jpeg";
import { CalendarAddButton, BranchesButton, Icon, Image, ContractsList, EditButton, RemoveIconButton, ObjectList } from "../ui";
import { IOrganization } from "../../config/types";
import { getYear } from "date-fns";
import { parseDate } from "../ui/datepicker/utils";

interface IRelationalListProps extends IOrganization {
    refetch: () => void;
    objectType: string;
    objectId: string;
}

export const RelationalList: FunctionComponent<IRelationalListProps> = ({ id, name, created_at, contracts, refetch, objectId, type }) => {
    return (
        <div className="b-relational-list">
            <div className="b-relational-list__wrapper">
                <header className="h6 _text-uppercase _text-grey _font-bold b-relational-list__header">{type && type.name}</header>
                <div className="b-relational-list__body flex align-center">
                    <Image src={logo} alt="logo" className="b-relational-list__logo" />
                    <div className="b-relational-list__info">
                        <p className="b-relational-list__name _font-bold">{name}</p>
                        <p className="b-relational-list__since h6 _text-grey">{created_at && `Since ${getYear(parseDate(created_at))}`}</p>
                    </div>
                    <div className="b-relational-list__actions flex align-center">
                        <BranchesButton />
                        <CalendarAddButton />
                        <RemoveIconButton to={`/app/object/${objectId}/organization/${id}/successor`} />
                    </div>
                </div>
            </div>
            <div className="b-relational-list__group-wrapper">
                <div className="b-relational-list__group">
                    <h5 className="b-relational-list__group-title">
                        Key contacts <Icon icon="datepickerArrow" />
                    </h5>
                    {/*<PersonsList contacts={employees || []} refetch={refetch} organizationID={id} objectId={objectId} />*/}
                </div>
                <div className="b-relational-list__group">
                    <h5 className="b-relational-list__group-title">
                        Contracts <Icon icon="datepickerArrow" />
                    </h5>
                    <ContractsList contracts={contracts || []} objectId={objectId} refetch={refetch} />
                </div>
            </div>
        </div>
    );
};

export const RelationalListInstallations: FunctionComponent = () => {
    return (
        <div className="b-relational-list _installations">
            <div className="b-relational-list__wrapper">
                <header className="h6 _text-uppercase _text-grey _font-bold b-relational-list__header">Buildings</header>
                <div className="b-relational-list__body flex align-center">
                    <Icon icon="street" className="b-relational-list__icon" />
                    <div className="b-relational-list__info">
                        <p className="b-relational-list__name _font-bold">Big building</p>
                        <p className="b-relational-list__since h6 _text-grey">233 St Street 284 Zurich Switzerland</p>
                    </div>
                    <div className="b-relational-list__actions flex align-center">
                        <EditButton />
                        <CalendarAddButton />
                        <RemoveIconButton />
                    </div>
                </div>
            </div>
            <div className="b-relational-list__group-wrapper">
                <div className="b-relational-list__group">
                    <h5 className="b-relational-list__group-title">
                        Objects <Icon icon="datepickerArrow" />
                    </h5>
                    <ObjectList />
                </div>
            </div>
        </div>
    );
};
