import React, { FunctionComponent, useContext } from "react";
import { FormAccordionContent, FormCaption, FormFooterSave } from "../formsUI";
import { Formik, FormikProps, FormikActions, FormikValues } from "formik";
import * as Yup from "yup";
import { PASSWORD_MIN_LENGTH, VALIDATION_ERRORS } from "../../../config";
import { Input } from "../../ui/inputs";
import { Column, Row } from "../../ui/grid";
import "./ChangePasswordForm.scss";
import { AccountStateContext } from "../../../contexts/accountStateContext";
import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui/toasts";
import { ForgotPasswordModal } from "../../modals/forgotPasswordModal";
import { useModal } from "../../ui/modal";

const UPDATE_PASSWORD = gql`
    mutation UpdatePassword($id: String!, $new_password: String!, $old_password: String!) {
        updateUserPassword(id: $id, new_password: $new_password, old_password: $old_password)
    }
`;

const ChangePasswordFormSchema = Yup.object().shape({
    currentPassword: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    newPassword: Yup.string()
        .trim()
        .min(PASSWORD_MIN_LENGTH, VALIDATION_ERRORS.passwordMinLength)
        .required(VALIDATION_ERRORS.required),
    confirmNewPassword: Yup.string()
        .trim()
        .oneOf([Yup.ref("newPassword")], `Passwords should match.`)
        .required(VALIDATION_ERRORS.required),
});

interface IChangePasswordFormState {
    currentPassword: string;
    newPassword: string;
    confirmNewPassword: string;
}

const changePasswordFormInitialValues = {
    currentPassword: "",
    newPassword: "",
    confirmNewPassword: "",
};

interface IChangePasswordFormBaseProps extends FormikProps<IChangePasswordFormState> {
    openForgotPasswordModal: () => void;
}

const ChangePasswordFormBase: FunctionComponent<IChangePasswordFormBaseProps> = ({
    values,
    errors,
    touched,
    handleSubmit,
    handleChange,
    handleBlur,
    openForgotPasswordModal,
}) => {
    return (
        <form onSubmit={handleSubmit}>
            <FormAccordionContent>
                <Row>
                    <Column>
                        <label>
                            <FormCaption>Current password</FormCaption>
                            <Input
                                type="password"
                                name="currentPassword"
                                value={values.currentPassword}
                                error={errors && errors.currentPassword}
                                touched={touched && touched.currentPassword}
                                onChange={handleChange}
                                onBlur={handleBlur}
                            />
                        </label>
                    </Column>
                    <Column>
                        <button
                            type="button"
                            className="_font-bold _text-primary f-change-password__description"
                            onClick={openForgotPasswordModal}
                        >
                            Forgot password?
                        </button>
                    </Column>
                </Row>
                <Row>
                    <Column>
                        <label>
                            <FormCaption>New password</FormCaption>
                            <Input
                                type="password"
                                name="newPassword"
                                value={values.newPassword}
                                error={errors && errors.newPassword}
                                touched={touched && touched.newPassword}
                                onChange={handleChange}
                                onBlur={handleBlur}
                            />
                        </label>
                    </Column>
                    <Column>
                        <p className="f-change-password__description">8-character minimum; case sensitive</p>
                    </Column>
                </Row>
                <Row>
                    <Column>
                        <label>
                            <FormCaption>Reenter new password</FormCaption>
                            <Input
                                type="password"
                                marginBottom="none"
                                name="confirmNewPassword"
                                value={values.confirmNewPassword}
                                error={errors && errors.confirmNewPassword}
                                touched={touched && touched.confirmNewPassword}
                                onChange={handleChange}
                                onBlur={handleBlur}
                            />
                        </label>
                    </Column>
                </Row>
            </FormAccordionContent>
            <FormFooterSave />
        </form>
    );
};

export const ChangePasswordForm: FunctionComponent = () => {
    const { user } = useContext(AccountStateContext);
    const [updatePassword] = useMutation(UPDATE_PASSWORD);

    const { hide, open, isOpen } = useModal();

    async function onSubmit(values: IChangePasswordFormState, actions: FormikActions<FormikValues>) {
        try {
            const { data, errors } = await updatePassword({
                variables: {
                    id: user.id,
                    new_password: values.newPassword,
                    old_password: values.currentPassword,
                },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else if (data.updateUserPassword) {
                actions.resetForm();
                showSuccessToast("Password is successfully updated.");
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    return (
        <>
            <Formik
                initialValues={changePasswordFormInitialValues}
                onSubmit={onSubmit}
                validationSchema={ChangePasswordFormSchema}
                render={(formikProps) => <ChangePasswordFormBase {...formikProps} openForgotPasswordModal={open} />}
            />
            <ForgotPasswordModal hide={hide} isOpen={isOpen} />
        </>
    );
};
