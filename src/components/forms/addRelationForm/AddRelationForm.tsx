import React, { ChangeEvent, FunctionComponent, useState } from "react";
import "./AddRelationForm.scss";
import { Formik, FormikProps } from "formik";
import { IRelation, TRelationRoles } from "../../../config/types";
import { FormCaption } from "../formsUI";
import { AutocompleteInput, Select } from "../../ui/inputs";
import { OBJECT_SEARCH_SCOPES, VALIDATION_ERRORS } from "../../../config";
import * as Yup from "yup";
import { ModalFooter } from "../../ui/modal";
import { Button, ButtonLink } from "../../ui/buttons";
import { useRelationTypes } from "../../../hooks/apollo";
import { getRelationTypesOptions } from "../../../utils";
import get from "lodash.get";

export interface IAddRelationFormState {
    objectID: string;
    role: string;
}

interface IAddRelationFormProps {
    onSubmit: (state: IAddRelationFormState) => void;
    editedRelation?: IAddRelationFormState;
    addObjectLink?: string;
    relationRole?: TRelationRoles;
    objectID: string;
    objectName: string;
    objectCity?: string;
    defaultRole?: string;
}

const DEFAULT_STATE = {
    objectID: "",
    role: "",
};

const AddRelationFormSchema = Yup.object().shape({
    objectID: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    role: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
});

export const AddRelationForm: FunctionComponent<IAddRelationFormProps> = ({
    onSubmit,
    addObjectLink,
    relationRole,
    objectID,
    editedRelation,
    objectName,
    objectCity,
    defaultRole,
}) => {
    let initialValues = defaultRole ? { ...DEFAULT_STATE, role: defaultRole } : DEFAULT_STATE;

    if (editedRelation) {
        initialValues = editedRelation;
    }

    return (
        <Formik
            onSubmit={onSubmit}
            initialValues={initialValues}
            validationSchema={AddRelationFormSchema}
            render={(formikProps) => (
                <AddRelationFormBase
                    {...formikProps}
                    addObjectLink={addObjectLink}
                    objectID={objectID}
                    objectName={objectName}
                    editedRelation={editedRelation}
                    objectCity={objectCity}
                />
            )}
        />
    );
};

interface IAddRelationFormBaseProps extends FormikProps<IAddRelationFormState> {
    addObjectLink?: string;
    objectID: string;
    objectName: string;
    editedRelation?: IAddRelationFormState;
    objectCity?: string;
}

const AddRelationFormBase: FunctionComponent<IAddRelationFormBaseProps> = ({
    values,
    errors,
    touched,
    handleSubmit,
    handleChange,
    handleBlur,
    setFieldValue,
    setFieldTouched,
    addObjectLink,
    objectID,
    editedRelation,
    objectName,
    objectCity,
}) => {
    const [scope, setScope] = useState<string>(OBJECT_SEARCH_SCOPES[0].value as string);

    const relationTypes = useRelationTypes();
    const relationOptions = getRelationTypesOptions(relationTypes);
    const searchInCity = relationOptions.filter((i) => i.label === "Child" || i.label === "Parent").find((i) => i.value === values.role);

    function onObjectSelect(object: IRelation) {
        setFieldValue("objectID", object.id);
        setFieldTouched("objectID", true);
    }

    function onScopeChange(e: ChangeEvent<HTMLSelectElement>) {
        setScope(e.target.value);
    }

    return (
        <form onSubmit={handleSubmit}>
            <h2 className="h1 f-add-relation__title"> {editedRelation ? "Edit Relation" : "Add Relation"} </h2>
            {addObjectLink && <ButtonLink to={addObjectLink} text="Create new object" className="f-add-relation__add-new-button" />}
            <div className="h1 f-add-relation__separator" />
            <FormCaption>{objectName} is</FormCaption>
            <Select
                options={relationOptions}
                placeholder={"Please select relationship  type"}
                name="role"
                value={values.role}
                error={errors && errors.role}
                touched={touched && touched.role}
                onChange={handleChange}
                onBlur={handleBlur}
            />
            <FormCaption>Of </FormCaption>
            <Select options={OBJECT_SEARCH_SCOPES} value={scope} onChange={onScopeChange} />
            <FormCaption>Object Name</FormCaption>
            <AutocompleteInput
                searchFor="objects"
                searchScope={scope}
                scopeObjectID={objectID}
                onChange={onObjectSelect}
                error={errors && errors.objectID}
                touched={touched && touched.objectID}
                marginBottom="normal"
                searchInCity={searchInCity && objectCity}
                defaultInputValue={get(values, "objectName") || ""}
            />

            <ModalFooter>
                <Button text={editedRelation ? "Edit Relation" : "Add Relation"} type="submit" primary />
            </ModalFooter>
        </form>
    );
};
