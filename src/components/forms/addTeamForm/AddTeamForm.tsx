import React, { FunctionComponent } from "react";
import "./AddTeamForm.scss";
import { Formik, FormikProps } from "formik";
import * as Yup from "yup";
import { VALIDATION_ERRORS } from "../../../config";
import { Column, Row } from "../../ui/grid";
import { AutocompleteInput, ILocalAutocompleteOption, Input, LocalAutocomplete } from "../../ui/inputs";
import { Button, RemoveObjectButton } from "../../ui/buttons";
import { ModalFooter } from "../../ui/modal";
import { FormTitle } from "../formTitle";
import gql from "graphql-tag";
import { FormCaption } from "../formsUI";
import { useMutation } from "@apollo/react-hooks";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui/toasts";
import { ITeamObject } from "../../tables/buildingTeamsTable";

const ADD_ORGANIZATION_TEAM = gql`
    mutation AddOrganizationTeam($organization_id: String!, $name: String, $users: [String], $objects: [String]) {
        addOrganizationTeam(organization_id: $organization_id, name: $name, users: $users, objects: $objects) {
            id
        }
    }
`;

const UPDATE_ORGANIZATION_TEAM = gql`
    mutation UpdateOrganizationTeam($organization_id: String!, $team_id: String!, $users: [String], $objects: [String]) {
        updateOrganizationTeam(organization_id: $organization_id, team_id: $team_id, users: $users, objects: $objects) {
            id
        }
    }
`;

const OrganizationFormSchema = Yup.object().shape({
    name: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
});

const addTeamFormState = {
    name: "",
    users: [],
    buildings: [],
};

export interface IAddTeamFormState {
    id?: string;
    name: string;
    users: ILocalAutocompleteOption[];
    buildings: ITeamObject[];
}

interface IAddTeamFormProps {
    callback: () => void;
    users: ILocalAutocompleteOption[];
    organizationID: string;
    editedTeam?: IAddTeamFormState;
}

interface IAddTeamFormBaseProps extends FormikProps<IAddTeamFormState> {
    users: ILocalAutocompleteOption[];
    title: string;
}

const AddTeamFormBase: FunctionComponent<IAddTeamFormBaseProps> = ({
    values,
    setFieldValue,
    touched,
    errors,
    handleSubmit,
    handleChange,
    handleBlur,
    users,
    title,
}) => {
    function addUser(user: ILocalAutocompleteOption) {
        if (!values.users.find((item: ILocalAutocompleteOption) => item.id === user.id)) {
            setFieldValue("users", [...values.users, user]);
        }
    }

    function removeUser(user: ILocalAutocompleteOption) {
        setFieldValue("users", values.users.filter((item: ILocalAutocompleteOption) => item.id !== user.id));
    }

    function addBuilding(object: ITeamObject) {
        if (!values.buildings.find((item: ITeamObject) => item.id === object.id)) {
            setFieldValue("buildings", [...values.buildings, object]);
        }
    }

    function removeBuilding(object: ITeamObject) {
        setFieldValue("buildings", values.buildings.filter((item: ITeamObject) => item.id !== object.id));
    }

    return (
        <form className="f-add-team" onSubmit={handleSubmit}>
            <FormTitle text={title} />
            <Row gutter="sm">
                <Column width={12}>
                    <Input
                        disabled={!!values.id}
                        placeholder="Team name"
                        name="name"
                        value={values.name}
                        error={errors && errors.name}
                        touched={touched && touched.name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                </Column>
            </Row>
            <Row gutter="sm">
                <Column width={12}>
                    <FormCaption>ADD USER</FormCaption>
                    <LocalAutocomplete
                        className="f-add-team__autocomplete"
                        placeholder="Start typing to find user..."
                        position="top"
                        resultsContainerSize="small"
                        onChange={addUser}
                        options={users}
                        name="users"
                    />
                    <ul className="f-add-team__results">
                        {values.users.map((user: ILocalAutocompleteOption) => (
                            <li className="f-add-team__results-item" key={user.id}>
                                <RemoveObjectButton type="button" text={user.label} onClick={() => removeUser(user)} />
                            </li>
                        ))}
                    </ul>
                </Column>
            </Row>
            <Row gutter="sm">
                <Column width={12}>
                    <FormCaption>ACCESS TO BUILDINGS</FormCaption>
                    <AutocompleteInput
                        className="f-add-team__autocomplete"
                        placeholder="Start typing to find building..."
                        searchFor="buildings"
                        position="top"
                        resultsContainerSize="small"
                        onChange={addBuilding}
                        isSetValue={false}
                        name="buildings"
                    />
                    <ul className="f-add-team__results">
                        {values.buildings.map((building: ITeamObject) => (
                            <li className="f-add-team__results-item" key={building.id}>
                                <RemoveObjectButton type="button" text={building.name} onClick={() => removeBuilding(building)} />
                            </li>
                        ))}
                    </ul>
                </Column>
            </Row>
            <ModalFooter>
                <Button text="Save team" primary icon="check" type="submit" />
            </ModalFooter>
        </form>
    );
};

export const AddTeamForm: FunctionComponent<IAddTeamFormProps> = ({ callback, editedTeam, users, organizationID }) => {
    const [addOrganizationTeamMutation] = useMutation(ADD_ORGANIZATION_TEAM);
    const [updateOrganizationTeamMutation] = useMutation(UPDATE_ORGANIZATION_TEAM);

    const formInitialValues = editedTeam || addTeamFormState;
    const title = editedTeam ? "Update team" : "Add new team";

    async function addOrganizationTeam(values: IAddTeamFormState) {
        try {
            const res = await addOrganizationTeamMutation({
                variables: {
                    organization_id: organizationID,
                    name: values.name,
                    users: values.users.map((user) => user.id),
                    objects: values.buildings.map((building: any) => building.id),
                },
            });
            const { errors } = res;

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                showSuccessToast("Team is successfully added");
                if (callback) {
                    callback();
                }
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    async function updateOrganizationTeam(values: IAddTeamFormState) {
        try {
            const res = await updateOrganizationTeamMutation({
                variables: {
                    team_id: editedTeam && editedTeam.id,
                    organization_id: organizationID,
                    users: values.users.map((user) => user.id),
                    objects: values.buildings.map((building: any) => building.id),
                },
            });
            const { errors } = res;

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                showSuccessToast("Team is successfully updated");
                if (callback) {
                    callback();
                }
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    return (
        <Formik
            onSubmit={editedTeam ? updateOrganizationTeam : addOrganizationTeam}
            initialValues={formInitialValues}
            validationSchema={OrganizationFormSchema}
            render={(formikProps) => <AddTeamFormBase {...formikProps} title={title} users={users} />}
        />
    );
};
