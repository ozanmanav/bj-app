import * as Yup from "yup";
import { VALIDATION_ERRORS } from "../../../config";

export interface IClientOrganizationPrefilledFormState {
    first_name: string;
    last_name: string;
    email: string;
    password: string;
    passwordConfirmation: string;
    name: string;
    site_url: string;
    type_id: string;
    role_id: string;
    address: {
        street: string;
        number: string;
        city: string;
        country: string;
        plz: string;
        line_1: string;
        line_2: string;
    };
    phone: string;
    phone_code: string;
}

export const clientOrganizationPrefilledFormState = {
    name: "",
    site_url: "",
    type_id: "",
    role_id: "",
    first_name: "",
    last_name: "",
    email: "",
    password: "",
    passwordConfirmation: "",
    address: {
        street: "",
        number: "",
        city: "",
        country: "",
        plz: "",
        line_1: "",
        line_2: "",
    },
    phone: "",
    phone_code: "",
};

export const clientOrganizationPrefilledFormSchemaShared = {
    name: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    role_id: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
};

export const AddClientOrganizationValidationSchema = Yup.object().shape({
    ...clientOrganizationPrefilledFormSchemaShared,
});
