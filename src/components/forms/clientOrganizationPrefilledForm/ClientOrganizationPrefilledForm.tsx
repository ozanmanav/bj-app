import { AddClientOrganizationValidationSchema, IClientOrganizationPrefilledFormState } from "./definitions";
import { Column, Row } from "../../ui/grid";
import { phoneCodesByCountry } from "../../../config";
import { Formik, FormikProps } from "formik";
import { Input, Select } from "../../ui/inputs";
import React, { FunctionComponent } from "react";
import { getOrganizationRolesOptions, getCountriesOptions, getOrganizationTypesOptions } from "../../../utils";
import { useOrganizationRoles, useOrganizationTypes } from "../../../hooks/apollo";
import { Button } from "../../ui/buttons";
import { COUNTRIES } from "../../../config/countries";
import classnames from "classnames";
import get from "lodash.get";

interface IClientOrganizationPrefilledFormBaseProps extends FormikProps<IClientOrganizationPrefilledFormState> {
    wrapped?: boolean;
    redirect?: () => void;
}

const ClientOrganizationPrefilledFormBase: FunctionComponent<IClientOrganizationPrefilledFormBaseProps> = ({
    wrapped,
    values,
    handleSubmit,
    errors,
    touched,
    handleChange,
    handleBlur,
}) => {
    const organizationTypes = useOrganizationTypes();
    const organizationRoles = useOrganizationRoles();

    const wrapperClassName = classnames(["f-prefilled", { _wrapped: wrapped }]);

    return (
        <form className={wrapperClassName} onSubmit={handleSubmit}>
            <div className="f-prefilled__body">
                <h2 className="f-prefilled__title h1">{wrapped ? "Add new organization (Client)" : "Welcome"}</h2>
                <Input
                    autoComplete="no"
                    placeholder="Organization name"
                    name="name"
                    value={values.name}
                    error={errors && errors.name}
                    touched={touched && touched.name}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />
                <Select
                    name="type_id"
                    options={getOrganizationTypesOptions(organizationTypes)}
                    value={values.type_id}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    placeholder="Organization Type"
                    error={errors && errors.type_id}
                    touched={touched && touched.type_id}
                />
                <Select
                    name="role_id"
                    options={getOrganizationRolesOptions(organizationRoles)}
                    value={values.role_id}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    placeholder="Organization Role"
                    error={errors && errors.role_id}
                    touched={touched && touched.role_id}
                />
                <Input
                    autoComplete="no"
                    placeholder="Site url"
                    name="site_url"
                    value={values.site_url}
                    error={errors && errors.site_url}
                    touched={touched && touched.site_url}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />{" "}
                <Input
                    autoComplete="no"
                    placeholder="E-Mail"
                    name="email"
                    value={values.email}
                    error={errors && errors.email}
                    touched={touched && touched.email}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />
                <div className="flex">
                    <Select
                        options={phoneCodesByCountry}
                        placeholder="Phone code"
                        name="phone_code"
                        value={values.phone_code}
                        error={errors.phone_code}
                        touched={touched.phone_code}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        marginBottom="normal"
                        position="top"
                        className="b-new-client-organization-modal__phone-code"
                    />
                    <div className="b-new-client-organization-modal__phone">
                        <Input
                            autoComplete="no"
                            type="phone"
                            placeholder="Phone number"
                            name="phone"
                            value={values.phone}
                            error={errors.phone}
                            touched={touched.phone}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            marginBottom="normal"
                        />
                    </div>
                </div>
                <Input
                    autoComplete="no"
                    type="line_1"
                    placeholder="Address Line 1"
                    name="address.line_1"
                    value={values.address.line_1}
                    error={get(errors, "address.line_1")}
                    touched={get(touched, "address.line_1")}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />
                {wrapped && (
                    <>
                        <Row gutter="sm">
                            <Column>
                                <Input
                                    autoComplete="no"
                                    placeholder="Street"
                                    name="address.street"
                                    value={values.address.street}
                                    error={get(errors, "address.street")}
                                    touched={get(touched, "address.street")}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                            </Column>
                            <Column>
                                <Input
                                    autoComplete="no"
                                    placeholder="Number"
                                    name="address.number"
                                    value={values.address.number}
                                    error={get(errors, "address.number")}
                                    touched={get(touched, "address.number")}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                            </Column>
                        </Row>
                        <Input
                            autoComplete="no"
                            type="line_2"
                            placeholder="Address Line 2"
                            name="address.line_2"
                            value={values.address.line_2}
                            error={get(errors, "address.line_2")}
                            touched={get(touched, "address.line_2")}
                            onChange={handleChange}
                            onBlur={handleBlur}
                        />
                        <Row gutter="sm">
                            <Column>
                                <Input
                                    autoComplete="no"
                                    placeholder="PLZ"
                                    name="address.plz"
                                    value={values.address.plz}
                                    error={get(errors, "address.plz")}
                                    touched={get(touched, "address.plz")}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                            </Column>
                            <Column>
                                <Input
                                    autoComplete="no"
                                    placeholder="City"
                                    name="address.city"
                                    value={values.address.city}
                                    error={get(errors, "address.city")}
                                    touched={get(touched, "address.city")}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                            </Column>
                        </Row>
                        <Select
                            options={getCountriesOptions(COUNTRIES)}
                            placeholder="Country"
                            name="address.country"
                            value={values.address.country}
                            error={get(errors, "address.country")}
                            touched={get(touched, "address.country")}
                            onChange={handleChange}
                            onBlur={handleBlur}
                        />
                    </>
                )}
                {!wrapped && (
                    <>
                        <Input
                            autoComplete="no"
                            type="password"
                            placeholder="Password"
                            name="password"
                            value={values.password}
                            error={errors && errors.password}
                            touched={touched && touched.password}
                            onChange={handleChange}
                            onBlur={handleBlur}
                        />
                        <Input
                            autoComplete="no"
                            type="password"
                            placeholder="Confirm password"
                            name="passwordConfirmation"
                            value={values.passwordConfirmation}
                            error={errors && errors.passwordConfirmation}
                            touched={touched && touched.passwordConfirmation}
                            onChange={handleChange}
                            onBlur={handleBlur}
                        />
                    </>
                )}
            </div>
            <div className="f-prefilled__footer">
                <Button className="f-prefilled__button" text="Confirm" primary icon="check" type="submit" />
            </div>
        </form>
    );
};

interface IAddClientOrganizationFormProps {
    currentOrganization: IClientOrganizationPrefilledFormState;
    callback: (state: IClientOrganizationPrefilledFormState) => void;
}

export const AddClientOrganizationForm: FunctionComponent<IAddClientOrganizationFormProps> = ({ currentOrganization, callback }) => {
    return (
        <Formik
            onSubmit={callback}
            initialValues={currentOrganization}
            validationSchema={AddClientOrganizationValidationSchema}
            render={(props) => <ClientOrganizationPrefilledFormBase {...props} wrapped />}
        />
    );
};
