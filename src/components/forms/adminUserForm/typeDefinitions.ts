export interface IAdminUserFormState {
    id: string;
    first_name: string;
    last_name: string;
    email: string;
    type: string;
    phone: string;
    phone_code: string;
}

export interface IAdminUserFormBaseProps {
    title: string;
    isUpdatingUser: boolean;
}
