export const ROLES = [
    {
        value: "admin",
        label: "Admin",
    },
    {
        value: "datamanager",
        label: "Datamanager",
    },
];

export const adminUserFormState = {
    id: "",
    first_name: "",
    last_name: "",
    email: "",
    type: "",
    phone_code: "",
    phone: "",
    companies: [],
};
