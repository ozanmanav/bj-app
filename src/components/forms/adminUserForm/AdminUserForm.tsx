import React, { FunctionComponent, useState } from "react";
import { Column, Row } from "../../ui/grid";
import { Input, Select } from "../../ui/inputs";
import { ModalFooter } from "../../ui/modal";
import { Button } from "../../ui/buttons";
import { Formik, FormikActions, FormikProps } from "formik";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui/toasts";
import { useMutation } from "@apollo/react-hooks";
import { CREATE_USER, SET_USER_SYSTEM_ROLE, UPDATE_USER } from "./queries";
import { IAdminUserFormBaseProps, IAdminUserFormState } from "./typeDefinitions";
import { AdminUserFormSchema } from "./validations";
import "./AdminUserForm.scss";
import { ROLES, adminUserFormState } from "./constants";
import { phoneCodesByCountry } from "../../../config";
import { useCountryPhoneCode } from "../../../hooks/useCountryPhoneCode";
import { IAdminUser } from "../../tables/adminRelatedUsersTable/config";

const AdminUserFormBase: FunctionComponent<IAdminUserFormBaseProps & FormikProps<IAdminUserFormState>> = ({
    values,
    errors,
    touched,
    handleSubmit,
    handleChange,
    handleBlur,
    isSubmitting,
    title,
    isUpdatingUser,
}) => {
    return (
        <form className="f-admin-user" onSubmit={handleSubmit}>
            <h2 className="f-admin-user__title h1">{title}</h2>
            <Row gutter="sm">
                <Column>
                    <Input
                        placeholder="First name"
                        name="first_name"
                        value={values.first_name}
                        error={errors && errors.first_name}
                        touched={touched && touched.first_name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                </Column>
                <Column>
                    <Input
                        placeholder="Last name"
                        name="last_name"
                        value={values.last_name}
                        error={errors && errors.last_name}
                        touched={touched && touched.last_name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                </Column>
            </Row>
            <Row>
                <Column width={12}>
                    <Select
                        options={ROLES}
                        placeholder="Role"
                        name="type"
                        value={values.type}
                        error={errors && errors.type}
                        touched={touched && touched.type}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                </Column>
            </Row>
            <Row>
                <Column width={12}>
                    <Input
                        placeholder="E-Mail"
                        name="email"
                        value={values.email}
                        error={errors && errors.email}
                        touched={touched && touched.email}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        marginBottom="none"
                        disabled={isUpdatingUser}
                    />
                </Column>
            </Row>
            <div className="f-admin-user__phone">
                <Select
                    options={phoneCodesByCountry}
                    placeholder="Phone code"
                    name="phone_code"
                    value={values.phone_code}
                    error={errors.phone_code}
                    touched={touched.phone_code}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    marginBottom="none"
                    position="top"
                    className="f-admin-user__phone-code"
                    disabled={isUpdatingUser}
                />
                <div className="f-admin-user__phone-number">
                    <Input
                        type="phone"
                        placeholder="Phone number"
                        name="phone"
                        value={values.phone}
                        error={errors.phone}
                        touched={touched.phone}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        marginBottom="normal"
                        disabled={isUpdatingUser}
                    />
                </div>
            </div>
            <ModalFooter>
                <Button text="Save person" primary icon="check" type="submit" disabled={isSubmitting} />
            </ModalFooter>
        </form>
    );
};

interface IAdminUserFormProps {
    user?: IAdminUser;
    callback: Function;
    refetch?: Function;
}

export const AdminUserForm: FunctionComponent<IAdminUserFormProps> = ({ callback, refetch, user }) => {
    const [userID, setUserID] = useState<string>("");
    const [createUserMutation] = useMutation(CREATE_USER);
    const [updateUserMutation] = useMutation(UPDATE_USER);
    const [setUserSystemRole] = useMutation(SET_USER_SYSTEM_ROLE);
    const { phone_code } = useCountryPhoneCode();
    const isUpdatingUser = user !== undefined;
    const title = isUpdatingUser ? "Update user" : "Add new user";

    let adminUserFormInitialValues = user || {
        ...adminUserFormState,
        phone_code,
    };

    if (user && !user.phone) {
        adminUserFormInitialValues.phone_code = phone_code;
    }

    async function createUpdateUser(values: IAdminUserFormState, formik: FormikActions<IAdminUserFormState>) {
        try {
            const createUpdateMutation: Promise<any> = isUpdatingUser
                ? updateUserMutation({
                      variables: {
                          id: values.id,
                          email: values.email,
                          first_name: values.first_name,
                          last_name: values.last_name,
                          phone: values.phone ? `${values.phone_code} ${values.phone}` : null,
                      },
                  })
                : createUserMutation({
                      variables: {
                          email: values.email,
                          first_name: values.first_name,
                          last_name: values.last_name,
                          phone: values.phone ? `${values.phone_code} ${values.phone}` : null,
                          is_admin: true,
                      },
                  });

            const { data, errors } = await createUpdateMutation;

            const userID = isUpdatingUser ? data.updateUser.id : data.createUser.id;

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                const setUserSystemRoleResponse = await setUserSystemRole({
                    variables: {
                        id: userID,
                        type: values.type,
                    },
                });

                if (setUserSystemRoleResponse.errors) {
                    handleGraphQLErrors(setUserSystemRoleResponse.errors);
                } else {
                    showSuccessToast(`User is successfully ${isUpdatingUser ? "updated" : "added"}`);

                    refetch && refetch();

                    setUserID(userID);
                }
            }
        } catch (e) {
            e.graphQLErrors.forEach((error: any) => {
                if (error.validation) {
                    error.validation.email && formik.setFieldError("email", error.validation.email.join(" "));
                } else {
                    showErrorToast(error.message);
                }
            });
        }

        formik.setSubmitting(false);
    }

    function onConfirm() {
        callback();
    }

    return userID ? (
        <>
            <h2 className="h1 f-admin-user__title">Organizations</h2>
            <ModalFooter>
                <Button text="Confirm" primary onClick={onConfirm} />
            </ModalFooter>
        </>
    ) : (
        <Formik
            onSubmit={createUpdateUser}
            initialValues={adminUserFormInitialValues}
            component={(formikProps) => <AdminUserFormBase {...formikProps} title={title} isUpdatingUser={isUpdatingUser} />}
            validationSchema={AdminUserFormSchema}
        />
    );
};
