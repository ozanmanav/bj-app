import gql from "graphql-tag";

export const CREATE_USER = gql`
    mutation CreateUser($email: String!, $first_name: String!, $last_name: String!, $phone: String, $is_admin: Boolean) {
        createUser(email: $email, first_name: $first_name, last_name: $last_name, phone: $phone, is_admin: $is_admin) {
            id
        }
    }
`;

export const UPDATE_USER = gql`
    mutation UpdateUser($id: String!, $email: String!, $first_name: String!, $last_name: String!, $phone: String) {
        updateUser(id: $id, email: $email, first_name: $first_name, last_name: $last_name, phone: $phone) {
            id
        }
    }
`;

export const SET_USER_SYSTEM_ROLE = gql`
    mutation SetSystemRole($id: String!, $type: SystemRoleEnum) {
        setSystemRole(id: $id, type: $type) {
            id
        }
    }
`;
