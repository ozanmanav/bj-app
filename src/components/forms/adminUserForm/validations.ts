import * as Yup from "yup";
import { VALIDATION_ERRORS } from "../../../config";

export const AdminUserFormSchema = Yup.object().shape({
    first_name: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    last_name: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    email: Yup.string()
        .email(VALIDATION_ERRORS.email)
        .required(VALIDATION_ERRORS.required),
    type: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    phone: Yup.number()
        .typeError(VALIDATION_ERRORS.number)
        .required(VALIDATION_ERRORS.required),
    phone_code: Yup.number().required(VALIDATION_ERRORS.required),
});
