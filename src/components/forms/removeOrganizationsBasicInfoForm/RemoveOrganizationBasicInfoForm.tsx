import React, { FunctionComponent } from "react";
import "./RemoveOrganizationBasicInfoForm.scss";
import { FormCaption, FormAccordionContent, FormFooterNext } from "../formsUI";
import { Checkbox, DatepickerSimple } from "../../ui";
import { Formik } from "formik";

export interface IRemoveOrganizationBasicInfoFormState {
    startDate: string;
    endDate: string;
    withoutCreating: boolean;
}

interface IRemoveOrganizationBasicInfoFormParams {
    onSubmit: (state: IRemoveOrganizationBasicInfoFormState) => void;
    initialValues: IRemoveOrganizationBasicInfoFormState;
}

export const RemoveOrganizationBasicInfoForm: FunctionComponent<IRemoveOrganizationBasicInfoFormParams> = ({ onSubmit, initialValues }) => {
    return (
        <Formik
            onSubmit={onSubmit}
            initialValues={initialValues}
            render={({ values, handleSubmit, handleChange }) => (
                <form onSubmit={handleSubmit}>
                    <FormAccordionContent>
                        <div className="flex align-center">
                            <div className="f-remove-organization-basic-info__datepicker">
                                <FormCaption>End date</FormCaption>
                                <DatepickerSimple
                                    onChange={(e) => {
                                        console.log(e);
                                    }}
                                />
                            </div>
                            <div className="f-remove-organization-basic-info__datepicker">
                                <FormCaption>Start date</FormCaption>
                                <DatepickerSimple
                                    onChange={(e) => {
                                        console.log(e);
                                    }}
                                />
                            </div>
                            <div className="f-remove-organization-basic-info__checkbox">
                                <Checkbox
                                    label="End the relationship without creating a new one"
                                    marginBottom="none"
                                    checked={values.withoutCreating}
                                    name="withoutCreating"
                                    onChange={handleChange}
                                />
                            </div>
                        </div>
                    </FormAccordionContent>
                    <FormFooterNext />
                </form>
            )}
        />
    );
};
