import React, { forwardRef, Ref, useEffect, useState } from "react";
import "./OrganizationForm.scss";
import { FormAccordionContent, FormCaption, FormFooterNext, IFormFooterNextProps } from "../formsUI";
import { Column, Row, useModal } from "../../ui";
import { NewRelationship, KeyContacts, AboutObject } from "../";
import { FilesList } from "../../filesList";
import { Formik, FormikProps } from "formik";
import { IEmployee, IFormWithFormik, IRelationship, TFile } from "../../../config/types";
import * as Yup from "yup";
import { DynamicInfoRows, IInfoRowParams, DynamicsInfoRowSchema } from "../dynamicInfoRows";
import { VALIDATION_ERRORS } from "../../../config";
import { AddNonClientOrganizationModal } from "../../modals/addNonClientOrganizationModal";
import get from "lodash.get";
import { filesChangeCallback } from "../../filesList/utils";
import { useApolloClient } from "@apollo/react-hooks";
import { AddUserModal } from "../../modals/addUserModal";
import gql from "graphql-tag";
import { getArrayIDs } from "../../../views/app/event/addEvent/utils";
import { uniq } from "ramda";
import { IUser, IJob } from "../addUserForm/definitions";
import { GET_DETAILS } from "../../object/objectDetailsTable/queries";

const OrganizationFormSchema = Yup.object().shape({
    rows: Yup.object().shape({
        contractInformation: DynamicsInfoRowSchema,
    }),
    relationships: Yup.array().of(
        Yup.object().shape({
            organization_id: Yup.string()
                .trim()
                .required(VALIDATION_ERRORS.required),
            role: Yup.string()
                .trim()
                .required(VALIDATION_ERRORS.required),
        })
    ),
});

export const ORGANIZATION_FORM_DEFAULT_STATE = {
    relationships: [
        {
            organization_id: "",
            role: "",
            objects: [],
            files: [],
            contacts: [],
        },
    ],
    rows: {
        contractInformation: [],
    },
    files: [],
};

export interface IOrganizationFormState {
    relationships: IRelationship[];
    rows: {
        contractInformation: IInfoRowParams[];
    };
    files: TFile[];
}

interface IOrganizationFormBaseProps extends FormikProps<IOrganizationFormState>, IFormFooterNextProps {
    objectID?: string;
    objectName?: string;
    objectType?: string;
    organizationName?: string;
    isClientOrganization: boolean;
    title?: string;
}

export const GET_ORGANIZATION_CATEGORY = gql`
    query GetOrganizationCategory($organization_id: String!) {
        organizations(id: $organization_id) {
            data {
                id
                name
                category
            }
        }
    }
`;

const OrganizationFormBase = forwardRef((props: IOrganizationFormBaseProps, ref: Ref<HTMLDivElement>) => {
    const {
        objectID,
        objectType,
        goPrevAccordionItem,
        organizationName,
        skipButtonText,
        title,
        objectName,
        isClientOrganization,
        ...formikProps
    } = props;
    const [isClientOrgState, setIsClientOrganization] = useState<boolean>(isClientOrganization);
    const { values, handleSubmit, setValues, setFieldValue, setFieldTouched } = formikProps;

    const organizationID = get(values, "relationships[0].organization_id");

    const [currentRelationOrganizationName, setCurrentRelationOrganizationName] = useState<string>(organizationName || "");
    const [currentRelationOrganizationID, setCurrentRelationOrganizationID] = useState<string>("");
    const [organizationCountryName, setOrganizationCountryName] = useState<string>("");

    const [contactListRefetch, setContactListRefetch] = useState(false);
    const [organizationListRefetch, setOrganizationListRefetch] = useState(false);

    const { open: openNewRelationshipModal, hide: hideNewRelationshipModal, isOpen: isOpenNewRelationshipModal } = useModal();
    const { open: openNewEmployeeModal, hide: hideNewEmployeeModal, isOpen: isOpenNewEmployeeModal } = useModal();

    const companyRoleId = get(values, "relationships[0].role");

    const addNewContactToContactsList = (contact: IEmployee) => {
        const stateCopy = { ...values };

        stateCopy.relationships[0].contacts.push(contact);
        setValues(stateCopy);
        setContactListRefetch(true);
    };

    function onAddNewUser(user: IUser, employeeID?: string, job?: IJob) {
        if (user && employeeID) {
            // should convert to employee
            const { id = "", first_name, last_name, email, phone, phone_code, language } = user;

            const contact: IEmployee = {
                id: employeeID,
                user: {
                    id,
                    first_name,
                    last_name,
                    email,
                    phone: `${phone_code} ${phone}`,
                    language,
                },
                job,
            };

            addNewContactToContactsList(contact);
        }
    }

    const onAddNewOrganization = (organization: { id: string; name: string }) => {
        setCurrentRelationOrganizationName(organization.name);
        setCurrentRelationOrganizationID(organization.id);
        setFieldValue("relationships[0].organization_name", organization.name);
        setFieldValue("relationships[0].organization_id", organization.id);
        setFieldTouched("relationships[0].organization_id", true);
        setOrganizationListRefetch(true);
    };

    const apolloClient = useApolloClient();

    useEffect(() => {
        const getOrganizationCategory = async () => {
            const { data: organizationData } = await apolloClient.query({
                query: GET_ORGANIZATION_CATEGORY,
                variables: {
                    organization_id: organizationID,
                },
            });

            const organization = get(organizationData, "organizations.data[0]");
            const category = get(organization, "category");

            if (category === "CLIENT") {
                setIsClientOrganization(true);
            } else {
                setIsClientOrganization(false);
            }
        };

        const getOrganizationDetail = async () => {
            const { data: detailData } = await apolloClient.query({
                query: GET_DETAILS,
                variables: {
                    model_id: organizationID,
                    model: "ORGANIZATION",
                },
            });

            const organizationDetail = get(detailData, "details.data");

            const organizationCountries = organizationDetail.filter((detail: any) => detail.name === "country");

            const organizationCountry = get(organizationCountries, "[0].value");

            setOrganizationCountryName(organizationCountry);
        };

        if (organizationID) {
            getOrganizationCategory();
            getOrganizationDetail();
        }
    }, [organizationID]);

    const onSelfFilesChange = (updatedIDs: string[] = [], deletedID: string) => {
        const currentFileIDs = getArrayIDs(values.files);

        if (deletedID) {
            return filesChangeCallback(
                currentFileIDs.filter((currentID: string) => currentID !== deletedID),
                formikProps.setFieldValue,
                apolloClient
            );
        }

        return filesChangeCallback(uniq([...updatedIDs, ...currentFileIDs]), formikProps.setFieldValue, apolloClient);
    };

    return (
        <>
            <form className="f-organization" onSubmit={handleSubmit} autoComplete="off">
                <FormAccordionContent>
                    <h3 className="f-organization__title" ref={ref}>
                        {title || (organizationName ? "Edit Relationship" : "New Relationship")}
                    </h3>
                    <Row gutter="md">
                        <Column>
                            <AboutObject objectName={objectName} />
                        </Column>
                        <Column>
                            <NewRelationship
                                {...formikProps}
                                defaultValue={organizationID}
                                defaultInputValue={currentRelationOrganizationName}
                                setDefaultInputValue={setCurrentRelationOrganizationName}
                                organizationListRefetch={organizationListRefetch}
                                setOrganizationListRefetch={setOrganizationListRefetch}
                                organizationID={currentRelationOrganizationID}
                                openModal={openNewRelationshipModal}
                            />
                            {organizationID && (
                                <KeyContacts
                                    {...formikProps}
                                    showEditButton={!isClientOrgState}
                                    isClientOrganization={isClientOrgState}
                                    contactListRefetch={contactListRefetch}
                                    setContactListRefetch={setContactListRefetch}
                                    openModal={openNewEmployeeModal}
                                />
                            )}
                        </Column>
                    </Row>
                    {companyRoleId && (
                        <>
                            <FormCaption className="f-organization__contact-info-title">Add relationship information</FormCaption>
                            <DynamicInfoRows
                                {...formikProps}
                                variant="organization"
                                suffix=".contractInformation"
                                entityId={companyRoleId}
                            />
                        </>
                    )}
                    <FilesList
                        label="Files about relationship"
                        hideFilter={true}
                        files={values.files}
                        className="f-building-details__files"
                        addFilesTo="Add files to the contract"
                        filesChangeCallback={onSelfFilesChange}
                    />
                </FormAccordionContent>
                <FormFooterNext goPrevAccordionItem={goPrevAccordionItem} skip={props.skip} skipButtonText={skipButtonText} />
            </form>
            <AddNonClientOrganizationModal
                hide={hideNewRelationshipModal}
                isOpen={isOpenNewRelationshipModal}
                callback={onAddNewOrganization}
            />
            <AddUserModal
                hide={hideNewEmployeeModal}
                isOpen={isOpenNewEmployeeModal}
                organizationID={organizationID}
                isClientOrganization={isClientOrganization}
                onUserAdded={onAddNewUser}
                companyCountry={organizationCountryName}
            />
        </>
    );
});

interface IOrganizationFormProps extends IFormWithFormik<IOrganizationFormState>, IFormFooterNextProps {
    objectID?: string;
    objectType?: string;
    organizationName?: string;
    title?: string;
    enableReinitialize?: boolean;
    objectName?: string;
    isClientOrganization: boolean;
}

export const OrganizationForm = React.forwardRef((props: IOrganizationFormProps, ref: Ref<HTMLDivElement>) => {
    const {
        onSubmit,
        initialValues,
        objectID,
        objectType,
        goPrevAccordionItem,
        organizationName,
        skip,
        skipButtonText,
        title,
        enableReinitialize,
        objectName,
        isClientOrganization,
    } = props;

    return (
        <Formik
            onSubmit={onSubmit}
            enableReinitialize={enableReinitialize}
            initialValues={initialValues}
            validationSchema={OrganizationFormSchema}
            render={(props) => (
                <OrganizationFormBase
                    {...props}
                    isClientOrganization={isClientOrganization}
                    goPrevAccordionItem={goPrevAccordionItem}
                    objectID={objectID}
                    objectType={objectType}
                    ref={ref}
                    organizationName={organizationName}
                    skip={skip}
                    skipButtonText={skipButtonText}
                    title={title}
                    objectName={objectName}
                />
            )}
        />
    );
});
