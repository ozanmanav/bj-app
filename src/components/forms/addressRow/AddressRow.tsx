import React, { FunctionComponent } from "react";
import "./AddressRow.scss";
import { ADDRESS_DEFAULT_VALUE, FormCaption, ISettingsDetailsFormState } from "../";
import { Column, Row } from "../../ui/grid";
import { Input, Select } from "../../ui/inputs";
import { COUNTRIES } from "../../../config/countries";
import { getCountriesOptions } from "../../../utils";
import { ArrayHelpers, FormikHandlers, FormikProps } from "formik";
import * as Yup from "yup";
import { VALIDATION_ERRORS } from "../../../config";
import { PlusButton, RemoveIconButton } from "../../ui/buttons";
import get from "lodash.get";

export const AddressSchema = Yup.object().shape({
    city: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    country: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
});

interface IAddressRowProps extends FormikHandlers, FormikProps<ISettingsDetailsFormState> {
    type?: "main_address";
    suffix: string;
    index?: number;
    arrayLength?: number;
    arrayHelpers?: ArrayHelpers;
}

export const AddressRow: FunctionComponent<IAddressRowProps> = ({
    index = 0,
    suffix,
    arrayLength,
    arrayHelpers,
    type,
    values,
    touched,
    errors,
    handleBlur,
    handleChange,
}) => {
    const addressValues = get(values, suffix);
    const addressErrors = get(errors, suffix);
    const addressTouched = get(touched, suffix);

    function addRow() {
        arrayHelpers && arrayHelpers.push(ADDRESS_DEFAULT_VALUE);
    }

    function removeRow() {
        if (!Number.isInteger(index)) {
            return;
        }

        arrayHelpers && arrayHelpers.remove(index);
    }

    return (
        <Column className="b-address-row">
            <FormCaption className="b-address-row__caption">
                {type === "main_address" ? (
                    "Main address"
                ) : (
                    <>
                        Invoice Details {index + 1}
                        <PlusButton onClick={addRow} />
                        {arrayLength && <RemoveIconButton onClick={removeRow} />}
                    </>
                )}
            </FormCaption>
            <Input
                placeholder="Address Line 1"
                value={addressValues.line_1}
                onBlur={handleBlur}
                onChange={handleChange}
                name={`${suffix}.line_1`}
                error={addressErrors && addressErrors.line_1}
                touched={addressTouched && addressTouched.line_1}
            />
            <Input
                placeholder="Address Line 2"
                value={addressValues.line_2}
                onBlur={handleBlur}
                onChange={handleChange}
                name={`${suffix}.line_2`}
                error={addressErrors && addressErrors.line_2}
                touched={addressTouched && addressTouched.line_2}
            />
            <Row gutter="sm">
                <Column>
                    <Input
                        placeholder="Street"
                        value={addressValues.street}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        name={`${suffix}.street`}
                        error={addressErrors && addressErrors.street}
                        touched={addressTouched && addressTouched.street}
                    />
                </Column>
                <Column>
                    <Input
                        placeholder="Number"
                        value={addressValues.number}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        name={`${suffix}.number`}
                        error={addressErrors && addressErrors.number}
                        touched={addressTouched && addressTouched.number}
                    />
                </Column>
            </Row>
            <Row gutter="sm">
                <Column>
                    <Input
                        placeholder="Postal code"
                        value={addressValues.plz}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        name={`${suffix}.plz`}
                        error={addressErrors && addressErrors.plz}
                        touched={addressTouched && addressTouched.plz}
                    />
                </Column>
                <Column>
                    <Input
                        placeholder="City"
                        value={addressValues.city}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        name={`${suffix}.city`}
                        error={addressErrors && addressErrors.city}
                        touched={addressTouched && addressTouched.city}
                    />
                </Column>
            </Row>
            <Select
                options={getCountriesOptions(COUNTRIES)}
                placeholder="Country"
                value={addressValues.country}
                onBlur={handleBlur}
                onChange={handleChange}
                name={`${suffix}.country`}
                error={addressErrors && addressErrors.country}
                touched={addressTouched && addressTouched.country}
            />
        </Column>
    );
};

interface IEmptyAddressRowProps extends ArrayHelpers {}

export const EmptyAddressRow: FunctionComponent<IEmptyAddressRowProps> = ({ push }) => {
    function addRow() {
        push(ADDRESS_DEFAULT_VALUE);
    }

    return (
        <Column className="b-address-row">
            <FormCaption className="flex align-center f-settings-details__caption">
                Add invoice address <PlusButton onClick={addRow} />
            </FormCaption>
        </Column>
    );
};
