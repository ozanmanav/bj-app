import * as Yup from "yup";
import { VALIDATION_ERRORS, IEmployee } from "../../../config";
import { FormikProps } from "formik";

export const ConvertToClientOrganizationFormSchema = Yup.object().shape({
    organization_role_id: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    users: Yup.array().of(
        Yup.object().shape({
            role_id: Yup.string().required("Required"),
        })
    ),
});

export const convertToCompanyFormInitialValues = {
    organization_role_id: "",
    users: [],
};

export interface IInvitedUser {
    user_id?: string;
    job_id?: string;
    role_id?: string;
    invited?: boolean;
}

export interface IConvertToClientOrganizationFormState {
    organization_role_id: string;
    users: IInvitedUser[];
}

export interface IOrganizationRole {
    id: string;
    name: string;
}

export interface IUserRole {
    id: string;
    name: string;
    company_type?: string;
}

export interface IConvertToClientOrganizationFormBaseProps extends FormikProps<IConvertToClientOrganizationFormState> {
    organizationEmployees?: IEmployee[];
    organizationRoles?: IOrganizationRole[];
    userRoles: IUserRole[];
    getUserRoles: (company_type: string) => void;
    openAddUserModal: () => void;
    loading?: boolean;
}

export interface IConvertToClientOrganizationFormProps {
    hide: Function;
    organizationId: string;
    callback: (organization_id: string) => void;
}
