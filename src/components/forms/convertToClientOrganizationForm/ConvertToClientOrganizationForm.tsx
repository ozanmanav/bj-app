import React, { FunctionComponent, useState } from "react";
import { ModalFooter, useModal } from "../../ui/modal";
import { Button } from "../../ui/buttons";
import { Select } from "../../ui/inputs";
import { useMutation, useQuery, useApolloClient } from "@apollo/react-hooks";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui/toasts";
import { Formik } from "formik";
import { getOrganizationRolesOptions, getUserRolesOptions, isNillOrEmpty } from "../../../utils";
import { useOrganizationRoles } from "../../../hooks/apollo";
import get from "lodash.get";
import {
    convertToCompanyFormInitialValues,
    ConvertToClientOrganizationFormSchema,
    IConvertToClientOrganizationFormState,
    IConvertToClientOrganizationFormBaseProps,
    IConvertToClientOrganizationFormProps,
    IInvitedUser,
    IUserRole,
} from "./definitions";
import { GET_ORGANIZATION_EMPLOYEES, CONVERT_NON_CLIENT_TO_CLIENT, GET_USER_ROLES } from "./queries";
import { IEmployee } from "../../../config";
import { SelectToInviteUsersTable } from "../../tables/selectToInviteUsersTable/SelectToInviteUsersTable";
import "./ConvertToClientOrganizationForm.scss";
import { AddUserModal } from "../../modals";

const ConvertToClientOrganizationFormBase: FunctionComponent<IConvertToClientOrganizationFormBaseProps> = ({
    values,
    errors,
    touched,
    handleBlur,
    handleChange,
    handleSubmit,
    setFieldValue,
    organizationRoles,
    organizationEmployees,
    userRoles,
    getUserRoles,
    openAddUserModal,
}) => {
    function addOrUpdateUser(newInvitedUser?: IInvitedUser) {
        if (newInvitedUser) {
            const isNewUser = !values.users.find((invitedUser: IInvitedUser) => invitedUser.user_id === newInvitedUser.user_id);

            if (isNewUser) {
                setFieldValue("users", [...values.users, newInvitedUser]);
            } else {
                setFieldValue("users", [
                    ...values.users.filter((invitedUser: IInvitedUser) => invitedUser.user_id !== newInvitedUser.user_id),
                    newInvitedUser,
                ]);
            }
        }
    }

    function removeUser(userID?: string) {
        if (userID) {
            setFieldValue(
                "users",
                values.users.filter((invitedUser: IInvitedUser) => invitedUser.user_id !== userID)
            );
        }
    }

    const onChangeOrganizationRole = (event: React.ChangeEvent<HTMLSelectElement>) => {
        handleChange(event);

        const {
            currentTarget: { value },
        } = event;

        if (organizationRoles) {
            const selectedRole = organizationRoles.find((item) => item.id === value);

            if (selectedRole) {
                getUserRoles(selectedRole.name.toLowerCase());
            }
        }
    };

    return (
        <form className="f-convert" onSubmit={handleSubmit}>
            <h2 className="h1 f-convert__title">Invite to Join BobJ</h2>
            <Select
                options={getOrganizationRolesOptions(organizationRoles)}
                placeholder="Organization Role"
                name="organization_role_id"
                value={values.organization_role_id}
                error={errors && errors.organization_role_id}
                touched={touched && touched.organization_role_id}
                onChange={onChangeOrganizationRole}
                onBlur={handleBlur}
            />

            {organizationEmployees && organizationEmployees.length > 0 ? (
                <SelectToInviteUsersTable
                    addOrUpdateUser={addOrUpdateUser}
                    removeUser={removeUser}
                    employees={organizationEmployees}
                    userRoleOptions={getUserRolesOptions(userRoles)}
                    disabled={!values.organization_role_id}
                />
            ) : (
                <div className="f-convert__no-user-message">
                    This organization doesn't have any user. You can click{" "}
                    <b className="f-convert__no-user-message-new-user-click" onClick={openAddUserModal}>
                        here
                    </b>
                    to add a new user. <br />
                    <br /> Note: Organization should have at least one user for conversion.
                </div>
            )}

            <ModalFooter>
                <Button type="submit" text="Convert and Invite" icon="check" primary disabled={isNillOrEmpty(values.users)} />
            </ModalFooter>
        </form>
    );
};

export const ConvertToClientOrganizationForm: FunctionComponent<IConvertToClientOrganizationFormProps> = ({
    hide,
    organizationId,
    callback,
}) => {
    const { open: openUserModal, hide: hideAddUserModal, isOpen: isOpenOpenAddUserModal } = useModal();
    const [userRoles, setUserRoles] = useState<IUserRole[]>([]);
    const organizationRoles = useOrganizationRoles();
    const [convertNonClientToClientMutation] = useMutation(CONVERT_NON_CLIENT_TO_CLIENT);
    const apolloClient = useApolloClient();

    const openAddUserModal = () => {
        openUserModal();
    };

    const { data: organizationEmployeesData, loading: loadingEmployees, refetch: refetchEmployees } = useQuery(GET_ORGANIZATION_EMPLOYEES, {
        variables: {
            id: organizationId,
        },
        fetchPolicy: "network-only",
    });
    const organizationEmployees = (get(organizationEmployeesData, `organizations.data[0].employees`) as IEmployee[]) || [];

    const convertNonClientToClient = async (values: IConvertToClientOrganizationFormState) => {
        const administratorID = get(
            userRoles.find((i) => i.name === "Administrator"),
            "id"
        );
        const administrators = (get(values, "users") || []).filter((i) => i.role_id === administratorID);

        if (administrators.length < 1) {
            showErrorToast("One of the users should be assigned Administrator role");
        } else {
            try {
                const { data: convertNonClientToClientResponse, errors } = await convertNonClientToClientMutation({
                    variables: {
                        organization_id: organizationId,
                        ...values,
                    },
                });

                if (errors) {
                    handleGraphQLErrors(errors);
                } else {
                    callback(get(convertNonClientToClientResponse, `convertNonClientToClient.id`));
                    hide();
                    showSuccessToast("Non-Client organization successfully converted to Client Organization");
                }
            } catch (e) {
                e.graphQLErrors.forEach((error: any) => {
                    showErrorToast(error.message);
                });
            }
        }
    };

    const getUserRoles = async (organization_role_type: string) => {
        const normalizedOrganizationRoleType = organization_role_type && organization_role_type.replace(/ /g, "_");
        const { data: userRolesData } = await apolloClient.query({
            query: GET_USER_ROLES,
            variables: {
                organization_role_type: normalizedOrganizationRoleType,
            },
            fetchPolicy: "network-only",
        });

        setUserRoles(get(userRolesData, `user_roles_list`));
    };

    return (
        <>
            <Formik
                initialValues={convertToCompanyFormInitialValues}
                onSubmit={convertNonClientToClient}
                validationSchema={ConvertToClientOrganizationFormSchema}
                render={(props) => (
                    <ConvertToClientOrganizationFormBase
                        loading={loadingEmployees}
                        organizationRoles={organizationRoles}
                        organizationEmployees={organizationEmployees}
                        getUserRoles={getUserRoles}
                        userRoles={userRoles}
                        openAddUserModal={openAddUserModal}
                        {...props}
                    />
                )}
            />{" "}
            <AddUserModal
                refetch={refetchEmployees}
                hide={hideAddUserModal}
                isOpen={isOpenOpenAddUserModal}
                organizationID={organizationId}
                isClientOrganization={false}
            />
        </>
    );
};
