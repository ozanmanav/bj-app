import gql from "graphql-tag";

export const CONVERT_NON_CLIENT_TO_CLIENT = gql`
    mutation ConvertNonClientToClient($organization_id: String!, $organization_role_id: String!, $users: [NonClientToClientUsers]!) {
        convertNonClientToClient(organization_id: $organization_id, organization_role_id: $organization_role_id, users: $users) {
            id
        }
    }
`;

export const GET_ORGANIZATION_EMPLOYEES = gql`
    query GetOrganizationEmployees($id: String) {
        organizations(id: $id) {
            data {
                id
                name
                teams {
                    id
                    name
                    users
                    users_data {
                        id
                        name
                        first_name
                        email
                        phone
                    }
                }
                employees {
                    id
                    job_id
                    invited
                    user_role {
                        id
                        type
                        name
                    }
                    job {
                        id
                        type
                        name
                    }
                    user {
                        id
                        first_name
                        last_name
                        email
                        phone
                        language
                    }
                }
            }
        }
    }
`;

export const GET_USER_ROLES = gql`
    query UserRolesList($organization_role_type: String) {
        user_roles_list(organization_role_type: $organization_role_type) {
            id
            name
        }
    }
`;
