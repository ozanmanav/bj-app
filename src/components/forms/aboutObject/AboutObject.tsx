import React, { FunctionComponent } from "react";
import "./AboutObject.scss";
import { Input } from "../../ui";
import { FormCaption } from "../formsUI";

interface INewRelationshipProps {
    objectName?: string;
}

const DEFAULT_OBJECT_NAME = "This object";

export const AboutObject: FunctionComponent<INewRelationshipProps> = ({ objectName }) => {
    return (
        <div className="b-about-object">
            <FormCaption className="b-about-object__caption">About object</FormCaption>
            <Input placeholder={objectName || DEFAULT_OBJECT_NAME} name="thisObject" disabled={true} />
        </div>
    );
};
