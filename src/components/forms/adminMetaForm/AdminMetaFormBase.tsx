import React, { FunctionComponent } from "react";
import { FieldArray, FormikProps } from "formik";
import { AdminMetaFormRow, DEFAULT_METAFIELD_VALUE, IAdminMetaFormState } from "../adminMetaFormRow/AdminMetaFormRow";
import { TAdminEntityTypes } from "../../tables/adminTypesTable/config";
import { AddButton, Button } from "../../ui/buttons";
import { FormCaption } from "../index";
import { Input } from "../../ui/inputs/input";
import { IMetatype } from "../../../config/types";
import { ModalFooter } from "../../ui/modal";
import get from "lodash.get";

interface IAdminMetaFormBaseProps extends FormikProps<IAdminMetaFormState> {
    typeId?: string;
    entityType: TAdminEntityTypes;
}

const AddRowButton: FunctionComponent<{ push: Function }> = ({ push }) => {
    function addRow() {
        push(DEFAULT_METAFIELD_VALUE);
    }

    return <AddButton text="Add more" onClick={addRow} />;
};

export const AdminMetaFormBase: FunctionComponent<IAdminMetaFormBaseProps> = ({ typeId, entityType, ...formikProps }) => {
    const { values, errors, touched, handleChange, handleBlur, handleSubmit } = formikProps;

    const rowsName = "meta";

    const rows = get(values, rowsName) || [];

    return (
        <form className="f-admin-meta" onSubmit={handleSubmit}>
            <h2 className="h1 f-admin-meta__title">{typeId ? "Edit type" : "Add new type"}</h2>
            <div className="f-admin-meta__body">
                {entityType === "relationship" ? (
                    <>
                        <div className="flex align-center f-admin-meta__relationships-name">
                            <span className="f-admin-meta__separator-first h4 _text-grey">Object 1 - </span>
                            <Input
                                placeholder="field"
                                name="parent_name"
                                value={values.parent_name}
                                error={errors && errors.parent_name}
                                touched={touched && touched.parent_name}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                marginBottom="none"
                            />
                            <span className="f-admin-meta__separator h4 _text-grey"> - Object 2</span>
                        </div>
                        <div className="flex align-center f-admin-meta__relationships-name">
                            <span className="f-admin-meta__separator-first h4 _text-grey">Object 2 - </span>
                            <Input
                                placeholder="field"
                                name="child_name"
                                value={values.child_name}
                                error={errors && errors.child_name}
                                touched={touched && touched.child_name}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                marginBottom="none"
                            />
                            <span className="f-admin-meta__separator h4 _text-grey"> - Object 1</span>
                        </div>
                    </>
                ) : (
                    <Input
                        placeholder="Type Name"
                        name="name"
                        value={values.name || ""}
                        error={errors && errors.name}
                        touched={touched && touched.name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                )}
                <FormCaption>Meta fields:</FormCaption>
                <FieldArray
                    name={rowsName}
                    render={(arrayHelpers) => (
                        <>
                            {rows.map((field: IMetatype, index: number) => (
                                <AdminMetaFormRow {...formikProps} {...arrayHelpers} index={index} key={index} />
                            ))}
                            {values.meta.length === 0 && <AddRowButton push={arrayHelpers.push} />}
                        </>
                    )}
                />
            </div>
            <ModalFooter>
                <Button type="submit" text="Save" icon="check" primary />
            </ModalFooter>
        </form>
    );
};
