export function getInitialRelationNames(name: string) {
    const splittedName = name.split("-");

    if (splittedName.length < 2) {
        return {
            parentName: "",
            childName: "",
        };
    }

    return {
        parentName: splittedName[0],
        childName: splittedName[1],
    };
}
