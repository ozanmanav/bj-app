import React, { FunctionComponent } from "react";
import "./AdminMetaForm.scss";
import { VALIDATION_ERRORS, IMetatype } from "../../../config";
import { Formik } from "formik";
import * as Yup from "yup";
import get from "lodash.get";
import differenceBy from "lodash.differenceby";
import { IAdminMetaFormState } from "../adminMetaFormRow/AdminMetaFormRow";
import gql from "graphql-tag";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui/toasts";
import { useMutation, useQuery } from "@apollo/react-hooks";
import { TAdminEntityTypes, TYPE_QUERIES } from "../../tables/adminTypesTable/config";
import { Loading } from "../../ui/loading";
import { normalizeGraphQLData } from "../../../utils/";
import { AdminMetaFormBase } from "./AdminMetaFormBase";
import { getInitialRelationNames } from "./utils";

const UPDATE_TYPE = gql`
    mutation UpdateEntityType($id: String!, $name: String!) {
        updateEntityType(id: $id, name: $name) {
            id
        }
    }
`;
const UPDATE_TYPE_META = gql`
    mutation UpdateTypeMeta($id: String!, $model_id: String!, $type: UpdateTypeMetaTypeEnum, $name: String, $context: String) {
        updateTypeMeta(id: $id, model_id: $model_id, type: $type, name: $name, context: $context) {
            id
        }
    }
`;

const DELETE_TYPE_META = gql`
    mutation DeleteTypeMeta($model_id: String!, $ids: [String]) {
        deleteTypeMeta(model_id: $model_id, ids: $ids)
    }
`;

const GET_ENTITY_TYPE_BY_ID = gql`
    query EntityType($id: String!) {
        entity_types(id: $id) {
            data {
                id
                name
                type
            }
        }
    }
`;

const GET_ENTITY_TYPE_META_BY_ID = gql`
    query TypeMeta($model_id: String!) {
        type_meta(model_id: $model_id) {
            data {
                id
                name
                type
                context
            }
        }
    }
`;

const AdminMetaFormSchemaBase = {
    meta: Yup.array().of(
        Yup.object().shape({
            name: Yup.string()
                .trim()
                .required(VALIDATION_ERRORS.required),
            type: Yup.string()
                .trim()
                .required(VALIDATION_ERRORS.required),
        })
    ),
};

const AdminMetaFormSchema = Yup.object().shape({
    name: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    ...AdminMetaFormSchemaBase,
});

const AdminMetaFormRelationshipSchema = Yup.object().shape({
    parent_name: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    child_name: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    ...AdminMetaFormSchemaBase,
});

const adminMetaFormInitialValues = {
    name: "",
    parent_name: "",
    child_name: "",
    meta: [],
};

interface IAdminMetaFormProps {
    typeId?: string;
    callback?: Function;
    entityType: TAdminEntityTypes;
}

export const AdminMetaForm: FunctionComponent<IAdminMetaFormProps> = ({ typeId, callback, entityType }) => {
    const isRelationshipType = entityType === "relationship";

    const { data, loading } = useQuery(GET_ENTITY_TYPE_BY_ID, {
        variables: {
            id: typeId,
        },
        fetchPolicy: "network-only",
    });

    const { data: metaData, loading: metaLoading } = useQuery(GET_ENTITY_TYPE_META_BY_ID, {
        variables: {
            model_id: typeId,
        },
        fetchPolicy: "network-only",
    });

    const [createNewTypeMutation] = useMutation(TYPE_QUERIES[entityType].createMutation);
    const [createNewTypeMetaMutation] = useMutation(TYPE_QUERIES[entityType].createTypeMetaMutation);
    const [updateTypeMutation] = useMutation(UPDATE_TYPE);
    const [updateTypeMetaMutation] = useMutation(UPDATE_TYPE_META);
    const [deleteTypeMetaMutation] = useMutation(DELETE_TYPE_META);

    const fetchedValues = get(data, "entity_types.data[0]") || {};
    const fetchedMetaValues: IMetatype[] = get(metaData, "type_meta.data") || [];

    async function onSubmit(values: IAdminMetaFormState) {
        if (typeId) {
            await updateType(values, typeId);
        } else {
            await createNewType(values);
        }
    }

    async function createNewType(values: IAdminMetaFormState) {
        try {
            const { data: entityData, errors: newTypeErrors } = await createNewTypeMutation({
                variables: {
                    name: isRelationshipType ? `${values.parent_name}-${values.child_name}` : values.name,
                    type: values.name
                        .split(" ")
                        .join("_")
                        .toLowerCase(),
                },
            });

            const createdTypeId = get(entityData, "createEntityType.id");

            const { errors: newTypeMetaErrors } = await createNewTypeMetaMutation({
                variables: {
                    model_id: createdTypeId,
                    metas: values.meta,
                },
            });

            const errors = newTypeErrors || newTypeMetaErrors;

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                showSuccessToast("Type is successfully created");
                callback && callback();
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    async function updateType(values: IAdminMetaFormState, id: string) {
        const changedMetaValues: IMetatype[] = values.meta.map((metafield) => normalizeGraphQLData(metafield));
        const changedMetaValuesWithID: IMetatype[] = changedMetaValues.filter((changedMetaValue: IMetatype) => changedMetaValue.id);

        const deletedMetaValues = differenceBy(fetchedMetaValues, changedMetaValuesWithID, "id");

        try {
            const { errors } = await updateTypeMutation({
                variables: {
                    id,
                    name: isRelationshipType ? `${values.parent_name}-${values.child_name}` : values.name,
                },
            });

            await Promise.all([
                ...changedMetaValues.map(async ({ id: meta_id, name, type, context }) =>
                    meta_id
                        ? await updateTypeMetaMutation({
                              variables: {
                                  id: meta_id,
                                  model_id: id,
                                  type,
                                  name,
                                  context, // TODO: not updating context as empty string ""
                              },
                          })
                        : await createNewTypeMetaMutation({
                              variables: {
                                  model_id: id,
                                  metas: [
                                      {
                                          name,
                                          type,
                                          context,
                                      },
                                  ],
                              },
                          })
                ),
            ]);

            if ((deletedMetaValues && deletedMetaValues.length) || !changedMetaValuesWithID.length) {
                const deletedMetaValuesIDs: string[] = [];

                if (deletedMetaValues.length) {
                    deletedMetaValues.forEach((deletedMetaValue) =>
                        deletedMetaValue.id ? deletedMetaValuesIDs.push(deletedMetaValue.id) : null
                    );
                } else {
                    fetchedMetaValues.forEach((fetchedMetaValue) =>
                        fetchedMetaValue.id ? deletedMetaValuesIDs.push(fetchedMetaValue.id) : null
                    );
                }

                await deleteTypeMetaMutation({
                    variables: { model_id: id, ids: deletedMetaValuesIDs },
                });
            }

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                showSuccessToast("Type is successfully updated");
                callback && callback();
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    if (metaLoading && loading && typeId) {
        return <Loading />;
    }

    const { parentName, childName } = getInitialRelationNames(fetchedValues.name || "");
    const initialValues = typeId
        ? {
              ...fetchedValues,
              meta: fetchedMetaValues,
              parent_name: isRelationshipType ? parentName : "",
              child_name: isRelationshipType ? childName : "",
          }
        : adminMetaFormInitialValues;

    return (
        <Formik
            onSubmit={onSubmit}
            initialValues={initialValues}
            enableReinitialize={true}
            validationSchema={isRelationshipType ? AdminMetaFormRelationshipSchema : AdminMetaFormSchema}
            render={(props) => <AdminMetaFormBase typeId={typeId} entityType={entityType} {...props} />}
        />
    );
};
