import React, { FunctionComponent, HTMLAttributes } from "react";
import "./FormsUI.scss";
import { AssignButton, NextButton, SaveButton, SaveAndContinueButton, ButtonLink, Button } from "../../ui";
import classNames from "classnames";

interface IHTMLElementProps extends HTMLAttributes<HTMLElement> {}

export interface IFormFooterNextProps {
    goPrevAccordionItem?: () => void;
    skip?: () => void;
    skipButtonText?: string;
    submitButtonText?: string;
}

export const FormFooterNext: FunctionComponent<IFormFooterNextProps> = ({
    submitButtonText,
    goPrevAccordionItem,
    skip,
    skipButtonText = "Skip",
}) => {
    return (
        <footer className="f-accordion__footer">
            <div className="f-accordion__footer-list">
                <div className="f-accordion__footer-item" data-testid="FormFooterNextButton">
                    <NextButton type="submit" text={submitButtonText} />
                </div>
                {goPrevAccordionItem && (
                    <div className="f-accordion__footer-item" data-testid="FormFooterPrevButton">
                        <Button text="Prev" onClick={goPrevAccordionItem} />
                    </div>
                )}
                {skip && (
                    <div className="f-accordion__footer-item _skip">
                        <Button text={skipButtonText} onClick={skip} data-testid="FormFooterSkipButton" />
                    </div>
                )}
            </div>
        </footer>
    );
};

export interface IFormFooterWithSaveProps {
    saveAndContinue?: (state: any) => void;
    goPrevAccordionItem?: () => void;
}

function touchAllErrors(errorsObj: any): any {
    return Object.keys(errorsObj).reduce(
        (acc, item) => ({
            ...acc,
            [item]: Array.isArray(errorsObj[item])
                ? // replacing undefined with empty objects
                  errorsObj[item].map((i: any) => (i ? touchAllErrors(i) : {}))
                : typeof errorsObj[item] === "object"
                ? touchAllErrors(errorsObj[item])
                : true,
        }),
        {}
    );
}

export const FormFooterWithSave: FunctionComponent<IFormFooterWithSaveProps & { variant?: "no-formik" } & any> = ({
    saveAndContinue,
    goPrevAccordionItem,
    variant,
    validateForm,
    values,
    setErrors,
    setTouched,
}) => {
    async function onSelfSaveAndContinue() {
        if (!saveAndContinue) {
            return;
        }

        if (variant === "no-formik") {
            saveAndContinue();

            return;
        }

        const errors = await validateForm(values);

        if (Object.entries(errors).length !== 0) {
            setErrors(errors);
            setTouched(touchAllErrors(errors));

            return;
        }

        saveAndContinue(values);
    }

    return (
        <footer className="f-accordion__footer flex justify-between">
            <div className="f-accordion__footer-list">
                <div className="f-accordion__footer-item">
                    <NextButton type="submit" />
                </div>
                {goPrevAccordionItem && (
                    <div className="f-accordion__footer-item">
                        <Button text="Prev" onClick={goPrevAccordionItem} />
                    </div>
                )}
            </div>
            {saveAndContinue && <SaveAndContinueButton onClick={onSelfSaveAndContinue} />}
        </footer>
    );
};

export const FormFooterAssign: FunctionComponent = () => {
    return (
        <footer className="f-accordion__footer">
            <AssignButton type="submit" />
        </footer>
    );
};

interface IFormFooterWithSkipProps {
    to: string;
}

export const FormFooterWithSkip: FunctionComponent<IFormFooterWithSkipProps> = ({ to }) => {
    return (
        <footer className="f-accordion__footer flex justify-between">
            <NextButton type="submit" />
            <ButtonLink text="Skip" to={to} />
        </footer>
    );
};

export const FormFooterSave: FunctionComponent = () => {
    return (
        <footer className="f-accordion__footer">
            <SaveButton type="submit" />
        </footer>
    );
};

export const FormAccordionContent: FunctionComponent<IHTMLElementProps> = ({ className, children }) => {
    return <div className={classNames("f-accordion", className)}>{children}</div>;
};

export const FormCaption: FunctionComponent<IHTMLElementProps> = ({ className, children }) => {
    return <p className={classNames("f-caption", className)}>{children}</p>;
};

export const FormActions: FunctionComponent<IHTMLElementProps> = ({ className, children }) => {
    return <div className={classNames("flex", "f-actions", className)}>{children}</div>;
};
