import React, { FunctionComponent } from "react";
import "./AddTypeGroupForm.scss";
import { Input } from "../../ui/inputs";
import { Button } from "../../ui/buttons";
import { Formik, FormikActions } from "formik";
import { TAdminEntityTypes, TYPE_QUERIES } from "../../tables/adminTypesTable/config";
import * as Yup from "yup";
import { VALIDATION_ERRORS } from "../../../config";
import { useMutation } from "@apollo/react-hooks";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui/toasts";

interface IAddTypeGroupFormState {
    name: string;
}

const AddTypeGroupFormSchema = Yup.object().shape({
    name: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
});

const ADD_TYPE_GROUP_DEFAULT_STATE = {
    name: "",
};

interface IAddTypeGroupFormProps {
    entityType: TAdminEntityTypes;
    callback: () => void;
}

export const AddTypeGroupForm: FunctionComponent<IAddTypeGroupFormProps> = ({ entityType, callback }) => {
    const [createGroup] = useMutation(TYPE_QUERIES[entityType].createGroupMutation);

    async function addGroup(values: IAddTypeGroupFormState, formActions: FormikActions<IAddTypeGroupFormState>) {
        try {
            const { errors } = await createGroup({
                variables: {
                    name: values.name,
                },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                await callback();
                formActions.resetForm();
                showSuccessToast("Group successfully added.");
            }
        } catch (e) {
            showErrorToast(e.message);
        } finally {
            formActions.setSubmitting(false);
        }
    }

    return (
        <Formik
            onSubmit={addGroup}
            initialValues={ADD_TYPE_GROUP_DEFAULT_STATE}
            validationSchema={AddTypeGroupFormSchema}
            render={({ values, errors, touched, handleChange, handleBlur, handleSubmit }) => (
                <form className="flex align-center f-add-type-group" onSubmit={handleSubmit}>
                    <Input
                        name="name"
                        placeholder="Group name"
                        value={values.name}
                        error={errors && errors.name}
                        touched={touched && touched.name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        marginBottom="none"
                        className="f-add-type-group__input"
                    />
                    <Button text="Add" type="submit" />
                </form>
            )}
        />
    );
};
