import React, { FunctionComponent } from "react";
import "./KeyContacts.scss";
import { FormCaption } from "../formsUI";
import { PlusButton } from "../../ui/buttons";
import { KeyContact } from "../keyContact";
import { AutocompleteInput } from "../../ui/inputs";
import { FormikProps } from "formik";
import { IOrganizationFormState } from "../organizationForm";
import { IEmployee } from "../../../config/types";

interface IKeyContacts extends FormikProps<IOrganizationFormState> {
    openModal: () => void;
    contactListRefetch: boolean;
    isClientOrganization: boolean;
    setContactListRefetch: (state: boolean) => void;
    showEditButton?: boolean;
}

export const KeyContacts: FunctionComponent<IKeyContacts> = ({
    openModal,
    contactListRefetch,
    setContactListRefetch,
    isClientOrganization,
    showEditButton,
    ...formikProps
}) => {
    const { values, setValues } = formikProps;
    const organizationID = values.relationships[0].organization_id;

    const onSelect = (contact: IEmployee) => {
        if (values.relationships[0].contacts.find(({ id }) => id === contact.id)) {
            return;
        }
        const stateCopy = { ...values };

        stateCopy.relationships[0].contacts.push(contact);
        setValues(stateCopy);
    };

    const onRemove = (contact: IEmployee) => {
        const stateCopy = { ...values };

        stateCopy.relationships[0].contacts = stateCopy.relationships[0].contacts.filter(({ id }) => contact.id !== id);
        setValues(stateCopy);
    };

    return (
        <div className="b-key-contacts">
            <FormCaption className="b-key-contacts__caption">
                Key contacts
                {!isClientOrganization && <PlusButton onClick={openModal} />}
            </FormCaption>
            <ul className="b-key-contacts__list">
                {values.relationships[0].contacts.map((employee) => (
                    <li className="b-key-contacts__item" key={employee.id}>
                        <KeyContact
                            employee={employee}
                            onRemove={onRemove}
                            organizationID={organizationID}
                            isClientOrganization={isClientOrganization}
                            showEditButton={showEditButton}
                        />
                    </li>
                ))}
            </ul>
            <div className="b-key-contacts__autocomplete">
                <AutocompleteInput
                    placeholder="Search by persons name or E-Mail in existing contacts..."
                    className="b-new-relationship__search"
                    searchFor="employee"
                    onChange={onSelect}
                    isRefetch={contactListRefetch}
                    setRefetch={setContactListRefetch}
                    organizationID={organizationID}
                    isSetValue={false}
                />
            </div>
        </div>
    );
};
