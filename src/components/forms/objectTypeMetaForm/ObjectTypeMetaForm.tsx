import React, { FunctionComponent, useContext } from "react";
import { Formik, FormikProps } from "formik";
import { Column, Row } from "../../ui/grid";
import { Input, Select } from "../../ui/inputs";
import * as Yup from "yup";
import { defaultValueTypes, VALIDATION_ERRORS } from "../../../config";
import "./ObjectTypeMetaForm.scss";
import { FormActions } from "../formsUI";
import { AddButton, RemoveButton, SaveButton } from "../../ui/buttons";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui/toasts";
import { AccountStateContext } from "../../../contexts/accountStateContext";

const ADD_ENTITY_TYPE_META = gql`
    mutation AddEntityTypeMetaField(
        $object_type_id: String
        $is_company_field: Boolean
        $name: String
        $type: String
        $companies: [String]
    ) {
        addEntityTypeMetaField(
            object_type_id: $object_type_id
            is_company_field: $is_company_field
            name: $name
            type: $type
            companies: $companies
        ) {
            id
        }
    }
`;

const UPDATE_ENTITY_TYPE_META = gql`
    mutation UpdateEntityTypeMetaField($id: String, $object_type_id: String, $name: String) {
        updateEntityTypeMetaField(id: $id, object_type_id: $object_type_id, name: $name) {
            id
        }
    }
`;

const REMOVE_ENTITY_TYPE_META = gql`
    mutation RemoveEntityTypeMetaField($meta_id: String, $object_type_id: String) {
        removeEntityTypeMetaField(meta_id: $meta_id, object_type_id: $object_type_id) {
            id
        }
    }
`;

export type TObjectsType = "buildings" | "files" | "relationships" | "events";

const ObjectTypeMetaFormSchema = Yup.object().shape({
    name: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    type: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
});

export const objectTypeMetaFormEmptyRow = {
    name: "",
    type: "",
};

export interface IObjectTypeMetaFormState {
    id?: string;
    name: string;
    type: string;
}

interface IObjectTypeMetaFormBaseProps extends FormikProps<IObjectTypeMetaFormState> {
    type?: string;
    objectTypeId: string;
    refetch: Function;
    removeMetaField?: Function;
    addMetaField?: () => void;
}

const ObjectTypeMetaFormBase: FunctionComponent<IObjectTypeMetaFormBaseProps> = ({
    addMetaField,
    removeMetaField,
    refetch,
    type,
    objectTypeId,
    ...formikProps
}) => {
    const [removeEntityTypeMetaMutation] = useMutation(REMOVE_ENTITY_TYPE_META);
    const { values, initialValues, errors, touched, handleChange, handleBlur } = formikProps;
    const isChanged = initialValues.name !== values.name || initialValues.type !== values.type;

    const removeEntityTypeMeta = async (meta_id: string, object_type_id: string) => {
        try {
            const res = await removeEntityTypeMetaMutation({
                variables: {
                    meta_id,
                    object_type_id,
                },
            });
            const { errors } = res;

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                refetch();
                showSuccessToast("Removed.");
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    };

    function onMetaTypeRemove() {
        if (values.id) {
            removeEntityTypeMeta(values.id, objectTypeId);
        } else if (removeMetaField) {
            removeMetaField();
        }
    }

    return (
        <form className="f-object-type-meta" onSubmit={formikProps.handleSubmit}>
            <Row gutter="sm">
                <Column>
                    <Input
                        placeholder="Custom"
                        value={values.name}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        name="name"
                        error={errors && errors.name}
                        touched={touched && touched.name}
                    />
                </Column>
                <Column>
                    <Select
                        options={defaultValueTypes}
                        placeholder="Type"
                        value={values.type}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        name="type"
                        error={errors && errors.type}
                        touched={touched && touched.type}
                    />
                </Column>
            </Row>
            <Row gutter="sm">
                <Column width={12}>
                    <FormActions>
                        <SaveButton className="f-object-type-meta__btn" disabled={!isChanged} type="submit" />
                        <RemoveButton className="f-object-type-meta__btn" onClick={onMetaTypeRemove} />
                        {addMetaField && (
                            <AddButton className="f-object-type-meta__btn" text="Add more" type="button" onClick={addMetaField} />
                        )}
                    </FormActions>
                </Column>
            </Row>
        </form>
    );
};

interface IObjectTypeMetaFormProps {
    type?: TObjectsType;
    objectTypeId: string;
    row: IObjectTypeMetaFormState;
    refetch: Function;
    removeMetaField?: Function;
    addMetaField?: () => void;
}

export const ObjectTypeMetaForm: FunctionComponent<IObjectTypeMetaFormProps> = ({
    addMetaField,
    removeMetaField,
    objectTypeId,
    type,
    row,
    refetch,
}) => {
    const [addEntityTypeMetaMutation] = useMutation(ADD_ENTITY_TYPE_META);
    const [updateEntityTypeMetaMutation] = useMutation(UPDATE_ENTITY_TYPE_META);
    const accountStateContext = useContext(AccountStateContext);

    const addEntityTypeMeta = async (values: IObjectTypeMetaFormState) => {
        try {
            const res = await addEntityTypeMetaMutation({
                variables: {
                    name: values.name,
                    type: values.type,
                    is_company_field: true,
                    object_type_id: objectTypeId,
                    companies: [accountStateContext.currentOrganization.id],
                },
            });
            const { errors } = res;

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                refetch();
                showSuccessToast("Saved.");
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    };

    const updateEntityTypeMeta = async (values: IObjectTypeMetaFormState) => {
        try {
            const res = await updateEntityTypeMetaMutation({
                variables: {
                    id: values.id,
                    name: values.name,
                    object_type_id: objectTypeId,
                },
            });
            const { errors } = res;

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                refetch();
                showSuccessToast("Updated.");
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    };

    function onSubmit(values: IObjectTypeMetaFormState) {
        if (values.id) {
            return updateEntityTypeMeta(values);
        }
        return addEntityTypeMeta(values);
    }

    return (
        <Formik
            onSubmit={onSubmit}
            initialValues={row}
            enableReinitialize={true}
            validationSchema={ObjectTypeMetaFormSchema}
            render={(formikProps) => (
                <ObjectTypeMetaFormBase
                    {...formikProps}
                    addMetaField={addMetaField}
                    removeMetaField={removeMetaField}
                    objectTypeId={objectTypeId}
                    refetch={refetch}
                    type={type}
                />
            )}
        />
    );
};
