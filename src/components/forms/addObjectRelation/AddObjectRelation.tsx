import React, { ChangeEvent, FunctionComponent, useState } from "react";
import "./AddObjectRelation.scss";
import { AutocompleteInput, Select, AddButton, Row, Column } from "../../ui";
import { IRelation } from "../../../config";
import { useRelationTypes } from "../../../hooks/apollo";
import { getRelationTypesOptions } from "../../../utils";

interface IAddObjectRelationProps {
    addRelation?: (state: IRelation) => void; // TODO: remove optionality
    objectStreet?: string;
    objectCity?: string;
}

const addObjectRelationDefaultState = {
    id: "",
    name: "",
    type: {},
    relationType: "",
};

export const AddObjectRelation: FunctionComponent<IAddObjectRelationProps> = ({ addRelation, objectStreet, objectCity }) => {
    const [state, setState] = useState<IRelation>(addObjectRelationDefaultState);

    const relationTypes = useRelationTypes();
    const relationTypesOptions = getRelationTypesOptions(relationTypes);

    function handleTypeChange(e: ChangeEvent<HTMLSelectElement>) {
        setState({
            ...state,
            relationType: e.target.value,
        });
    }

    function handleObjectChange(item: any) {
        setState({
            ...state,
            ...item,
        });
    }

    const searchInCity = relationTypesOptions
        .filter((i) => i.label === "Child" || i.label === "Parent")
        .find((i) => i.value === state.relationType);

    function selfSubmit() {
        // TODO: remove addRelation check
        if (state.name.trim().length > 0 && state.relationType && addRelation) {
            addRelation(state);
            setState(addObjectRelationDefaultState);
        }
    }

    return (
        <Row gutter="sm" className="b-add-object-relation__wrapper">
            <Column>
                <AutocompleteInput
                    searchFor="objects"
                    placeholder="Search an existing objects..."
                    onChange={handleObjectChange}
                    defaultInputValue={state.name}
                    marginBottom="none"
                    searchInCity={searchInCity && objectCity}
                />
            </Column>
            <Column className="flex align-center">
                <Select
                    options={relationTypesOptions}
                    value={state.relationType}
                    onChange={handleTypeChange}
                    placeholder="Relationship Type"
                    className="b-add-object-relation__select"
                    marginBottom="none"
                />
                <AddButton className="b-add-object-relation__add" text="Add Related Object" onClick={selfSubmit} />
            </Column>
        </Row>
    );
};
