import React, { FunctionComponent, useEffect, useState } from "react";
import { Formik, FormikProps } from "formik";
import { Input } from "../../ui/inputs";
import "./CompanyNameForm.scss";
import { EditButton, RemoveIconButton, SaveIconButton } from "../../ui/buttons";
import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui/toasts";
import { UserRoleCheck } from "../../userRoleCheck";
import * as Yup from "yup";
import { VALIDATION_ERRORS } from "../../../config";

const EditNameSchema = Yup.object().shape({
    name: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
});

const UPDATE_COMPANY = gql`
    mutation UpdateCompanyName($id: String!, $name: String!) {
        updateCompany(id: $id, name: $name) {
            id
        }
    }
`;

interface ICompanyNameViewProps {
    toggleEdit: () => void;
    name: string;
}

const CompanyNameView: FunctionComponent<ICompanyNameViewProps> = ({ toggleEdit, name }) => {
    return (
        <div className="f-company-name flex align-center">
            <div className="f-company-name__view flex align-end">
                <h2 className="f-company-name__name h1">{name}</h2>
                <UserRoleCheck
                    availableForRoles={[
                        "owner_administrator",
                        "manufacturer_administrator",
                        "manufacturer_brand_manager",
                        "manufacturer_object_manager",
                        "property_manager_administrator",
                        "property_manager_building_manager",
                        "service_provider_administrator",
                        "service_provider_service_manager",
                    ]}
                >
                    <div className="f-company-name__actions">
                        <div className="f-company-name__actions-item">
                            <EditButton onClick={toggleEdit} />
                        </div>
                    </div>
                </UserRoleCheck>
            </div>
        </div>
    );
};

interface ICompanyNameEditFormProps extends FormikProps<{ name: string }> {
    toggleEdit: () => void;
}

const CompanyNameEditForm: FunctionComponent<ICompanyNameEditFormProps> = ({
    toggleEdit,
    values,
    errors,
    touched,
    setValues,
    initialValues,
    handleChange,
    handleBlur,
    handleSubmit,
}) => {
    function hideEditForm() {
        toggleEdit();
        setValues(initialValues);
    }

    return (
        <form className="f-company-name flex align-center" onSubmit={handleSubmit}>
            <div className="f-company-name__edit flex align-center">
                <Input
                    className="f-company-name__input"
                    placeholder="Organization name"
                    name="name"
                    value={values.name}
                    error={errors && errors.name}
                    touched={touched && touched.name}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    marginBottom="none"
                />
                <div className="f-company-name__actions flex">
                    {initialValues.name !== values.name && values.name && (
                        <div className="f-company-name__actions-item">
                            <SaveIconButton type="submit" />
                        </div>
                    )}
                    <div className="f-company-name__actions-item">
                        <RemoveIconButton onClick={hideEditForm} />
                    </div>
                </div>
            </div>
        </form>
    );
};

interface ICompanyNameFormProps {
    name: string;
    companyId: string;
    refetch: () => void;
}

export const CompanyNameForm: FunctionComponent<ICompanyNameFormProps> = ({ name, companyId, refetch }) => {
    const initialValue = { name };

    const [isOpen, setIsOpen] = useState(false);

    const [updateCompanyMutation] = useMutation(UPDATE_COMPANY);

    useEffect(() => {
        setIsOpen(false);
    }, [name]);

    function toggleEdit() {
        setIsOpen(!isOpen);
    }

    async function updateCompany(values: { name: string }) {
        try {
            const res = await updateCompanyMutation({
                variables: {
                    id: companyId,
                    name: values.name,
                },
            });
            const { errors } = res;

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                showSuccessToast("Company name is successfully updated");
                refetch();
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    if (!isOpen) {
        return <CompanyNameView name={name} toggleEdit={toggleEdit} />;
    }

    return (
        <Formik
            onSubmit={updateCompany}
            enableReinitialize
            validationSchema={EditNameSchema}
            initialValues={initialValue}
            render={(props) => <CompanyNameEditForm toggleEdit={toggleEdit} {...props} />}
        />
    );
};
