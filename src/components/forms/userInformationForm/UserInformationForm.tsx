import React, { FunctionComponent, useContext } from "react";
import "./UserInformationForm.scss";
import { FormCaption, FormAccordionContent, FormFooterSave } from "../formsUI";
import { Row, Column, Input, showErrorToast, showSuccessToast, handleGraphQLErrors, Select } from "../../ui";
import { Formik } from "formik";
import { AccountStateContext } from "../../../contexts/accountStateContext";
import { phoneCodesByCountry } from "../../../config";
import { getPhoneInitialValues, getLanguagesOptions } from "../../../utils";
import { useUserLanguages } from "../../../hooks";
import { useMutation } from "@apollo/react-hooks";
import { UPDATE_USER } from "../adminUserForm/queries";
import { UserInformationFormSchema } from "./definitions";

interface IUserInformationFormProps {}

interface IUserInformationState {
    id: string;
    first_name: string;
    last_name: string;
    email: string;
    language: string;
    phone_code: string;
    phone: string;
}

export const UserInformationForm: FunctionComponent<IUserInformationFormProps> = () => {
    const { user } = useContext(AccountStateContext);

    const [updateUserMutation] = useMutation(UPDATE_USER);

    const employeeLanguages = useUserLanguages();

    const UserInformationFormInitialValues: IUserInformationState = {
        id: user.id,
        first_name: user.first_name,
        last_name: user.last_name,
        email: user.email,
        language: user.language,
        phone: getPhoneInitialValues(user.phone).phone,
        phone_code: getPhoneInitialValues(user.phone).phone_code,
    };

    async function onSubmit(values: IUserInformationState) {
        try {
            const { data, errors } = await updateUserMutation({
                variables: {
                    id: values.id,
                    first_name: values.first_name,
                    last_name: values.last_name,
                    email: values.email,
                    phone: values.phone ? `${values.phone_code} ${values.phone}` : null,
                    language: user.language,
                },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else if (data.updateUser) {
                showSuccessToast("User information is successfully updated.");
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    return (
        <Formik
            onSubmit={onSubmit}
            initialValues={UserInformationFormInitialValues}
            validationSchema={UserInformationFormSchema}
            render={({ values, handleSubmit, handleChange, errors, touched, handleBlur }) => (
                <form onSubmit={handleSubmit}>
                    <FormAccordionContent>
                        <Row>
                            <Column>
                                <label>
                                    <FormCaption>First Name</FormCaption>
                                    <Input
                                        type="text"
                                        marginBottom="normal"
                                        name="first_name"
                                        value={values.first_name}
                                        error={errors && errors.first_name}
                                        touched={touched && touched.first_name}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                    />
                                </label>
                            </Column>
                            <Column>
                                <label>
                                    <FormCaption>Last Name</FormCaption>
                                    <Input
                                        type="text"
                                        marginBottom="normal"
                                        name="last_name"
                                        value={values.last_name}
                                        error={errors && errors.last_name}
                                        touched={touched && touched.last_name}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                    />
                                </label>
                            </Column>
                        </Row>
                        <Row>
                            <Column>
                                <label>
                                    <FormCaption>Email</FormCaption>
                                    <Input
                                        type="email"
                                        marginBottom="normal"
                                        name="email"
                                        value={values.email}
                                        error={errors && errors.email}
                                        touched={touched && touched.email}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                    />
                                </label>
                            </Column>
                        </Row>
                        <Row>
                            <Column>
                                <label>
                                    <FormCaption>Phone</FormCaption>
                                    <div className="flex">
                                        <Select
                                            options={phoneCodesByCountry}
                                            placeholder="Phone code"
                                            name="phone_code"
                                            value={values.phone_code}
                                            error={errors.phone_code}
                                            touched={touched.phone_code}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            marginBottom="normal"
                                            className="f-add-user-form__phone-code"
                                        />
                                        <div className="f-add-user-form__phone">
                                            <Input
                                                type="phone"
                                                placeholder="Phone number"
                                                name="phone"
                                                value={values.phone}
                                                error={errors.phone}
                                                touched={touched.phone}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                marginBottom="normal"
                                            />
                                        </div>
                                    </div>
                                </label>
                            </Column>
                        </Row>
                        <Row>
                            <Column>
                                <label>
                                    <FormCaption>Language</FormCaption>
                                    <Select
                                        options={getLanguagesOptions(employeeLanguages)}
                                        placeholder="Language"
                                        name="language"
                                        value={values.language}
                                        error={errors.language}
                                        touched={touched.language}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        marginBottom="normal"
                                    />
                                </label>
                            </Column>
                        </Row>
                    </FormAccordionContent>
                    <FormFooterSave />
                </form>
            )}
        />
    );
};
