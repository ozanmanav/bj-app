import React, { FunctionComponent } from "react";
import "./FormTitle.scss";

interface IFormTitle {
    text: string;
}

export const FormTitle: FunctionComponent<IFormTitle> = ({ text }) => {
    return <h2 className="form-title h1">{text}</h2>;
};
