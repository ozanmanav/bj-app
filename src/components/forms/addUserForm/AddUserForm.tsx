import React, { FunctionComponent, useEffect, useState } from "react";
import { Formik } from "formik";
import { phoneCodesByCountry } from "../../../config";
import { Column, Row } from "../../ui/grid";
import { Input, Select, Checkbox } from "../../ui/inputs";
import { Button } from "../../ui/buttons";
import { ModalFooter } from "../../ui/modal";
import { useMutation } from "@apollo/react-hooks";
import { getOrganizationRolesOptions, getJobTitlesOptions, getLanguagesOptions, isNillOrEmpty } from "../../../utils";
import { FormTitle } from "../formTitle";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui/toasts";
import { AdminPanelCompaniesList } from "./AdminCompaniesList";
import { useUserLanguages, useJobTitles } from "../../../hooks/apollo";
import get from "lodash.get";
import { useCountryPhoneCode } from "../../../hooks/useCountryPhoneCode";
import "./AddUserForm.scss";
import { IAddUserFormProps, addUserFormState, IUser, OrganizationUserFormSchema } from "./definitions";
import { CREATE_USER, UPDATE_USER, UPDATE_ORGANIZATION_USER, INVITE_USER } from "./queries";
import { Loader } from "../../ui/loader";
import { useUserRolesWithoutType } from "../../../hooks/apollo/useUserRoles";

export const AddUserForm: FunctionComponent<IAddUserFormProps> = ({
    editedUser,
    organizationID,
    companyType,
    callback,
    isAdminPanel,
    variant,
    adminID,
    companyCountry,
    title,
    isClientOrganization,
    refetch,
}) => {
    const [loading, setLoading] = useState<boolean>(false);
    const [invitationSent, setInvitationSent] = useState<boolean>(false);
    const { phone_code } = useCountryPhoneCode(companyCountry);

    const organizationRolesList = useUserRolesWithoutType();

    const formInitialValues = {
        ...addUserFormState,
        isClientOrganization,
        phone_code,
        ...editedUser,
    };

    const formTitle = title ? title : editedUser ? "Update Person" : isClientOrganization ? "Add New Person" : "Create New Person";

    const [createUserMutation] = useMutation(CREATE_USER);
    const [updateUserMutation] = useMutation(UPDATE_USER);
    const [updateOrganizationUserMutation] = useMutation(UPDATE_ORGANIZATION_USER);
    const [inviteUserMutation] = useMutation(INVITE_USER);
    const jobTitles = useJobTitles();
    const employeeLanguages = useUserLanguages();

    useEffect(() => {
        if (editedUser && organizationRolesList.length) {
            formInitialValues.role = editedUser.role;
        }
    }, [editedUser, organizationRolesList, formInitialValues.role]);

    async function createUser(user: IUser) {
        try {
            setLoading(true);
            const { data, errors } = await createUserMutation({
                variables: {
                    first_name: user.first_name,
                    last_name: user.last_name,
                    email: user.email,
                    phone: user.phone ? `${user.phone_code} ${user.phone}` : null,
                    language: user.language,
                    is_client: isClientOrganization,
                    organization_id: organizationID,
                    role_id: user.role,
                    job_id: user.job_title,
                    is_admin: false,
                    invited: user.invited,
                },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                const responseCreateUser = get(data, "createUser");
                if (responseCreateUser) {
                    showSuccessToast("User is successfully added");

                    if (callback) {
                        const employeeID = get(responseCreateUser, "employers[0].id");
                        const job = get(responseCreateUser, "employers[0].job");

                        callback(user, employeeID, job);
                    }
                }
            }
            setLoading(false);
        } catch (error) {
            showErrorToast(error.message);
        }
    }

    async function updateUser(user: IUser) {
        try {
            setLoading(true);
            const { errors: updateUserErrors } = await updateUserMutation({
                variables: {
                    id: user.id,
                    first_name: user.first_name,
                    email: user.email,
                    last_name: user.last_name,
                    phone: user.phone ? `${user.phone_code} ${user.phone}` : null,
                    language: user.language,
                    is_client: isClientOrganization,
                },
            });

            if (updateUserErrors) {
                handleGraphQLErrors(updateUserErrors);
            } else {
                updateUserOrganization(user);
            }
            setLoading(false);
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    async function updateUserOrganization(user: IUser) {
        try {
            setLoading(true);
            const { data: updateUserData, errors: updateOrganizationUserErrors } = await updateOrganizationUserMutation({
                variables: {
                    organization_id: organizationID,
                    user_id: user.id,
                    user_role_id: user.role || "none",
                    job_id: user.job_title,
                    is_client: isClientOrganization,
                },
            });

            if (updateOrganizationUserErrors) {
                handleGraphQLErrors(updateOrganizationUserErrors);
            } else {
                const responseUpdateUser = get(updateUserData, "updateUser");

                showSuccessToast("User is successfully updated");

                if (callback) {
                    if (callback) {
                        const employeeID = get(responseUpdateUser, "employers[0].id");

                        callback(user, employeeID);
                    }
                }
            }
            setLoading(false);
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    const withCompanyList = variant === "withCompaniesList";

    return (
        <Formik
            onSubmit={editedUser ? updateUser : createUser}
            initialValues={formInitialValues}
            validationSchema={OrganizationUserFormSchema}
            render={({ values, handleSubmit, errors, touched, handleChange, handleBlur, setFieldValue }) => {
                const sendInviteMail = async () => {
                    if (editedUser) {
                        const { id } = editedUser;

                        try {
                            const { data: inviteUserData, errors: inviteUserErrors } = await inviteUserMutation({
                                variables: {
                                    user_id: id,
                                    organization_id: organizationID,
                                },
                            });

                            if (inviteUserErrors) {
                                handleGraphQLErrors(inviteUserErrors);
                            } else {
                                const responseInviteUser = get(inviteUserData, "inviteUser");

                                if (responseInviteUser) {
                                    setInvitationSent(true);
                                    showSuccessToast("E-Mail is successfully sent.");
                                    setFieldValue("status", "invited");
                                    refetch && (await refetch());
                                }
                            }
                        } catch (e) {
                            showErrorToast(e.message);
                        }
                    }
                };

                return (
                    <form onSubmit={handleSubmit}>
                        <FormTitle text={formTitle} />
                        <Row gutter="sm">
                            <Column>
                                <Input
                                    placeholder="First name"
                                    name="first_name"
                                    value={values.first_name}
                                    error={errors && errors.first_name}
                                    touched={touched && touched.first_name}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                            </Column>
                            <Column>
                                <Input
                                    placeholder="Last name"
                                    name="last_name"
                                    value={values.last_name}
                                    error={errors && errors.last_name}
                                    touched={touched && touched.last_name}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                            </Column>
                        </Row>
                        <Input
                            placeholder="E-Mail"
                            type="email"
                            name="email"
                            value={values.email}
                            error={errors.email}
                            touched={touched.email}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            disabled={!!editedUser && !isNillOrEmpty(values.email) && isClientOrganization}
                        />
                        <div className="flex">
                            <Select
                                options={phoneCodesByCountry}
                                placeholder="Phone code"
                                name="phone_code"
                                value={values.phone_code}
                                error={errors.phone_code}
                                touched={touched.phone_code}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                marginBottom="none"
                                position="top"
                                className="f-add-user-form__phone-code"
                            />
                            <div className="f-add-user-form__phone">
                                <Input
                                    type="phone"
                                    placeholder="Phone number"
                                    name="phone"
                                    value={values.phone}
                                    error={errors.phone}
                                    touched={touched.phone}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    marginBottom="normal"
                                />
                            </div>
                        </div>
                        {values.isClientOrganization && (
                            <Select
                                name="role"
                                options={getOrganizationRolesOptions(organizationRolesList)}
                                value={values.role}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                placeholder="Role"
                                error={errors.role}
                                touched={touched.role}
                                disabled={editedUser && adminID === editedUser.id}
                            />
                        )}
                        <Select
                            name="job_title"
                            options={getJobTitlesOptions(jobTitles)}
                            value={values.job_title}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            placeholder="Job title"
                            error={errors.job_title}
                            touched={touched.job_title}
                            marginBottom={"normal"}
                            disabled={editedUser && adminID === editedUser.id}
                            position="top"
                        />
                        <Select
                            options={getLanguagesOptions(employeeLanguages)}
                            placeholder="Language"
                            name="language"
                            position="top"
                            value={values.language}
                            error={errors.language}
                            touched={touched.language}
                            onChange={handleChange}
                            onBlur={handleBlur}
                        />
                        {!isNillOrEmpty(editedUser) && values.status && isClientOrganization && (
                            <div className="f-add-user-form__status">
                                <div className="f-add-user-form__status-text"> Invitation Status:</div>
                                <div className="f-add-user-form__status-status">{values.status}</div>{" "}
                                {values.status && values.status.toLowerCase() !== "registered" && (
                                    <div className="f-add-user-form__status-button">
                                        <Button
                                            disabled={invitationSent}
                                            text={values.status === "invited" ? "Resend Invite" : "Send Invite"}
                                            onClick={sendInviteMail}
                                        />
                                    </div>
                                )}
                            </div>
                        )}

                        {isNillOrEmpty(editedUser) && isClientOrganization && (
                            <Checkbox
                                label="Send Invite E-Mail"
                                marginBottom="none"
                                checked={values.invited}
                                name="invited"
                                onChange={handleChange}
                            />
                        )}

                        {withCompanyList && editedUser && editedUser.id && <AdminPanelCompaniesList userID={editedUser.id} />}
                        <ModalFooter>
                            {loading ? (
                                <div className={"flex justify-center"}>
                                    <Loader rawLoader />{" "}
                                </div>
                            ) : (
                                <Button text="Save user" primary icon="check" type="submit" />
                            )}
                        </ModalFooter>
                    </form>
                );
            }}
        />
    );
};
