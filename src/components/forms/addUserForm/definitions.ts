import { VALIDATION_ERRORS } from "../../../config";
import * as Yup from "yup";

export const OrganizationUserFormSchema = Yup.object().shape({
    isClientOrganization: Yup.boolean(),
    first_name: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    last_name: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    role: Yup.string()
        .trim()
        .when("isClientOrganization", {
            is: true,
            then: Yup.string().required(VALIDATION_ERRORS.required),
        }),
    phone: Yup.number().typeError(VALIDATION_ERRORS.number),
    phone_code: Yup.number(),
    email: Yup.string()
        .email(VALIDATION_ERRORS.email)
        .required(VALIDATION_ERRORS.required),
});

export const addUserFormState = {
    first_name: "",
    last_name: "",
    email: "",
    role: "",
    job_title: "",
    phone_code: "",
    phone: "",
    language: "",
    isClientOrganization: false,
    invited: false,
};

export interface IUser {
    id?: string;
    first_name: string;
    last_name: string;
    role: string;
    email: string;
    job_title: string;
    phone: string;
    phone_code: string;
    language?: string;
    isClientOrganization?: boolean;
    invited?: boolean;
    status?: IStatusUser;
}

export interface IJob {
    id: string;
    name: string;
    type: string;
}

export type TAddUserFormVariants = "withCompaniesList";

export type IStatusUser = "not invited" | "invited" | "registered";

export interface IAddUserFormProps {
    title?: string;
    editedUser?: IUser & { isClientOrganization?: boolean };
    organizationID: string;
    companyType?: string;
    companyCountry?: string;
    callback?: (user?: IUser, employeeId?: string, job?: IJob) => void;
    refetch?: () => void;
    isAdminPanel?: boolean;
    variant?: TAddUserFormVariants;
    adminID?: string;
    isClientOrganization?: boolean;
    invited?: boolean;
}
