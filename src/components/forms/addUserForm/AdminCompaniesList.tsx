import React, { FunctionComponent, useCallback, useEffect, useState } from "react";
import { useApolloClient, useMutation } from "@apollo/react-hooks";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui/toasts";
import { FormCaption } from "../formsUI";
import { AutocompleteInput, Select } from "../../ui/inputs";
import { Button, RemoveObjectButton } from "../../ui/buttons";
import gql from "graphql-tag";
import { ConfirmModal } from "../../modals/confirmModal";
import { IModalProps, Modal, ModalFooter, useModal } from "../../ui/modal";
import { Formik, FormikProps } from "formik";
import * as Yup from "yup";
import { VALIDATION_ERRORS } from "../../../config";
import { getOrganizationRolesOptions } from "../../../utils";
import "./AdminCompaniesList.scss";
import { useUserRoles } from "../../../hooks/apollo";
import get from "lodash.get";

const GET_ORGANIZATIONS_BY_ADMIN_USER_ID = gql`
    query GetOrganizationsByAdminUserID($admin_user_id: String!) {
        organizations(admin_user_id: $admin_user_id) {
            data {
                id
                name
            }
        }
    }
`;

const ADD_USER_TO_COMPANY = gql`
    mutation AddCompanyUser($company_id: String!, $user_id: String!, $user_role_id: String!) {
        addCompanyUser(company_id: $company_id, user_id: $user_id, user_role_id: $user_role_id) {
            user {
                id
            }
        }
    }
`;

const REMOVE_USER = gql`
    mutation RemoveCompanyUser($company_id: String!, $user_id: String!) {
        removeCompanyUser(company_id: $company_id, user_id: $user_id)
    }
`;

interface IAdminPanelCompaniesListProps {
    userID: string;
}

interface IAdminPanelCompany {
    id: string;
    name: string;
    type: string;
}

export const AdminPanelCompaniesList: FunctionComponent<IAdminPanelCompaniesListProps> = ({ userID }) => {
    const apolloClient = useApolloClient();
    const [userOrganizations, setUserOrganizations] = useState<IAdminPanelCompany[]>([]);

    const { open, isOpen, hide } = useModal();
    const [newCompany, setNewCompany] = useState<IAdminPanelCompany>({ id: "", type: "", name: "" });

    const getCompanies = useCallback(
        async function() {
            try {
                const { data } = await apolloClient.query({
                    query: GET_ORGANIZATIONS_BY_ADMIN_USER_ID,
                    variables: {
                        admin_user_id: userID,
                    },
                    fetchPolicy: "no-cache",
                });

                setUserOrganizations(get(data, "organizations.data"));
            } catch (e) {
                showErrorToast(e.message);
            }
        },
        [apolloClient, userID]
    );

    useEffect(() => {
        if (userID && getCompanies) {
            getCompanies();
        }
    }, [userID, getCompanies]);

    function onAddCompany(company: { id: string; name: string; type: { type: string } }) {
        setNewCompany({
            ...company,
            type: company.type.type,
        });
        open();
    }

    function onCompanyListChange() {
        getCompanies();
    }

    return (
        <>
            <FormCaption>Belong to</FormCaption>
            <AutocompleteInput searchFor="clientOrganizations" onChange={onAddCompany} />
            <div className="f-admin-user__companies">
                {userOrganizations.map((organization) => (
                    <AdminPanelCompaniesListRemoveBlock
                        {...organization}
                        key={organization.id}
                        userID={userID}
                        callback={onCompanyListChange}
                    />
                ))}
            </div>
            <AddCompanyToUserModal hide={hide} isOpen={isOpen} company={newCompany} userID={userID} callback={onCompanyListChange} />
        </>
    );
};

interface IAdminPanelCompaniesListRemoveBlockProps extends IAdminPanelCompany {
    userID: string;
    callback: () => void;
}

const AdminPanelCompaniesListRemoveBlock: FunctionComponent<IAdminPanelCompaniesListRemoveBlockProps> = ({
    userID,
    callback,
    id,
    name,
}) => {
    const { open, hide, isOpen } = useModal();
    const [removeUser] = useMutation(REMOVE_USER, {
        variables: {
            company_id: id,
            user_id: userID,
        },
    });

    async function removeUserFromCompany() {
        try {
            const { errors } = await removeUser();

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                await callback();
                hide();
                showSuccessToast("User is successfully removed");
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    return (
        <>
            <RemoveObjectButton text={name} onClick={open} />
            <ConfirmModal title="Are you sure?" onConfirm={removeUserFromCompany} hide={hide} isOpen={isOpen} />
        </>
    );
};

interface IAddCompanyToUserModalProps extends IModalProps {
    company: IAdminPanelCompany;
    callback: () => void;
    userID: string;
}

interface IAddCompanyToUserModalFormState {
    role: string;
}

const AddCompanyToUserSchema = Yup.object().shape({
    role: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
});

const addCompanyToUserInitialValues = { role: "" };

const AddCompanyToUserModal: FunctionComponent<IAddCompanyToUserModalProps> = ({ company, userID, callback, ...props }) => {
    const [addCompanyToUser] = useMutation(ADD_USER_TO_COMPANY);

    async function onAdd(values: IAddCompanyToUserModalFormState) {
        try {
            const { errors } = await addCompanyToUser({
                variables: {
                    company_id: company.id,
                    user_id: userID,
                    user_role_id: values.role,
                },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                await callback();
                props.hide();
                showSuccessToast("User is successfully added.");
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    return (
        <Modal {...props}>
            <Formik
                onSubmit={onAdd}
                initialValues={addCompanyToUserInitialValues}
                validationSchema={AddCompanyToUserSchema}
                render={(formikProps) => <AddCompanyToUserModalForm {...formikProps} company={company} />}
            />
        </Modal>
    );
};

interface IAddCompanyToUserModalFormProps extends FormikProps<IAddCompanyToUserModalFormState> {
    company: IAdminPanelCompany;
}

const AddCompanyToUserModalForm: FunctionComponent<IAddCompanyToUserModalFormProps> = ({
    handleBlur,
    handleChange,
    handleSubmit,
    values,
    errors,
    touched,
    company,
}) => {
    const organizationRoles = useUserRoles({ organization_role_type: company.type });

    return (
        <>
            <h2 className="h1 b-admin-companies-list__title">Select role</h2>
            <Select
                options={getOrganizationRolesOptions(organizationRoles)}
                placeholder="Role"
                name="role"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.role}
                error={errors && errors.role}
                touched={touched && touched.role}
            />
            <ModalFooter>
                <Button text="Add" icon="check" primary onClick={() => handleSubmit()} />
            </ModalFooter>
        </>
    );
};
