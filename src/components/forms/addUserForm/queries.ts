import gql from "graphql-tag";

export const CREATE_USER = gql`
    mutation CreateUser(
        $first_name: String!
        $last_name: String!
        $email: String
        $phone: String
        $language: String
        $is_client: Boolean!
        $organization_id: String!
        $role_id: String
        $job_id: String
        $is_admin: Boolean
        $invited: Boolean
    ) {
        createUser(
            first_name: $first_name
            last_name: $last_name
            email: $email
            phone: $phone
            language: $language
            is_client: $is_client
            organization_id: $organization_id
            role_id: $role_id
            job_id: $job_id
            is_admin: $is_admin
            invited: $invited
        ) {
            id
            type
            employers {
                id
                job {
                    id
                    name
                    type
                }
            }
        }
    }
`;

export const UPDATE_USER = gql`
    mutation UpdateUser(
        $id: String!
        $email: String!
        $password: String
        $first_name: String!
        $last_name: String!
        $phone: String
        $language: String
        $is_client: Boolean!
    ) {
        updateUser(
            id: $id
            email: $email
            password: $password
            first_name: $first_name
            last_name: $last_name
            phone: $phone
            language: $language
            is_client: $is_client
        ) {
            id
            type
        }
    }
`;

export const UPDATE_ORGANIZATION_USER = gql`
    mutation UpdateOrganizationUser($organization_id: String!, $user_id: String!, $user_role_id: String!, $job_id: String) {
        updateOrganizationUser(organization_id: $organization_id, user_id: $user_id, user_role_id: $user_role_id, job_id: $job_id) {
            id
        }
    }
`;

export const INVITE_USER = gql`
    mutation InviteUser($user_id: String!, $organization_id: String!) {
        inviteUser(user_id: $user_id, organization_id: $organization_id) {
            id
        }
    }
`;
