import React, { FunctionComponent } from "react";
import "./BatchAddObjectForm.scss";
import { FormCaption, FormAccordionContent, IFormFooterWithSaveProps, FormFooterWithSkip } from "../";
import { Select, Input, Row, Column, ObjectNameSimple, EditButton, AutocompleteInput } from "../../ui";
import { Formik, FormikProps } from "formik";
import * as Yup from "yup";
import { IFormWithFormik, IRelation, VALIDATION_ERRORS } from "../../../config";
import { getObjectTypesOptions, getRelationTypesOptions } from "../../../utils";
import { useObjectTypes, useRelationTypes } from "../../../hooks";
import get from "lodash.get";

const BatchAddObjectSchema = Yup.object().shape({
    type: Yup.string().required(VALIDATION_ERRORS.required),
    amount: Yup.number()
        .positive(VALIDATION_ERRORS.positive)
        .required(VALIDATION_ERRORS.required),
    role: Yup.string().required(VALIDATION_ERRORS.required),
});

export interface IParentBuilding {
    id: string;
    name: string;
    type?: object;
    address?: string;
    streetName?: string;
}

export interface IBatchAddObjectFormState {
    parentBuilding: IParentBuilding;
    type?: any;
    role: string;
    amount?: number;
    parentObject: IRelation;
}

interface IBatchAddObjectFormBaseProps extends FormikProps<IBatchAddObjectFormState>, IFormFooterWithSaveProps {
    parentID: string;
}

const BatchAddObjectFormBase: FunctionComponent<IBatchAddObjectFormBaseProps> = ({
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    handleSubmit,
    setFieldValue,
    parentID,
}) => {
    const types = useObjectTypes(true);

    function handleParentIDChange(object: IRelation) {
        setFieldValue("parentObject", object);
    }
    const relationTypes = useRelationTypes();
    const relationOptions = getRelationTypesOptions(relationTypes);

    const typeID = get(values, "type") || "";

    return (
        <form onSubmit={handleSubmit}>
            <FormAccordionContent>
                <Select
                    options={getObjectTypesOptions(types)}
                    placeholder="Object type"
                    name="type"
                    textCapitalize={false}
                    value={typeID}
                    error={errors && errors.type}
                    touched={touched && touched.type}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />
                <Row gutter="sm">
                    <Column>
                        <FormCaption>Add as </FormCaption>
                        <Select
                            options={relationOptions}
                            placeholder={"Please select relationship  type"}
                            name="role"
                            value={values.role}
                            error={errors && errors.role}
                            touched={touched && touched.role}
                            onChange={handleChange}
                            onBlur={handleBlur}
                        />
                    </Column>
                    <Column>
                        <FormCaption> of</FormCaption>
                        <AutocompleteInput
                            placeholder="Search an existing object to add as parent"
                            searchFor="objects"
                            onChange={handleParentIDChange}
                            searchOnStreet={values.parentBuilding.streetName}
                            defaultInputValue={values.parentObject.name}
                        />
                    </Column>
                    <Column>
                        <FormCaption>How many objects to add?</FormCaption>
                        <Input
                            placeholder="How many objects to add"
                            name="amount"
                            type="number"
                            value={values.amount || ""}
                            error={errors && errors.amount}
                            touched={touched && touched.amount}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className="f-batch-add-object__number"
                        />
                    </Column>
                </Row>
                {values.parentBuilding.id && (
                    <>
                        <FormCaption>Belonging to the building</FormCaption>
                        <BatchAddObjectBelongingTable {...values.parentBuilding} />
                    </>
                )}
            </FormAccordionContent>
            <FormFooterWithSkip to={`/app/building/${parentID}`} />
        </form>
    );
};

interface IBatchAddObjectFormProps extends IFormFooterWithSaveProps, IFormWithFormik<IBatchAddObjectFormState> {
    parentID: string;
}

export const BatchAddObjectForm: FunctionComponent<IBatchAddObjectFormProps> = ({ onSubmit, initialValues, saveAndContinue, parentID }) => {
    return (
        <Formik
            onSubmit={onSubmit}
            initialValues={initialValues}
            enableReinitialize
            validationSchema={BatchAddObjectSchema}
            render={(props) => <BatchAddObjectFormBase {...props} saveAndContinue={saveAndContinue} parentID={parentID} />}
        />
    );
};

const BatchAddObjectBelongingTable: FunctionComponent<IParentBuilding> = ({ id, name, address }) => {
    return (
        <div className="b-table__wrapper b-belonging-table">
            <table className="b-table">
                <thead className="b-table__head">
                    <tr className="b-table__row _head">
                        <th className="b-table__cell">Object title</th>
                        <th className="b-table__cell">Address</th>
                    </tr>
                </thead>
                <tbody className="b-table__body">
                    <tr className="b-table__row _medium">
                        <td className="b-table__cell">
                            <ObjectNameSimple name={name} type="building" id={id} />
                        </td>
                        <td className="b-table__cell">
                            <div className="flex align-center">
                                <p>{address}</p>
                                <div className="b-belonging-table__actions flex align-center">
                                    <EditButton />
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
};
