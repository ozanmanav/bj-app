import React, { FunctionComponent } from "react";
import { AutocompleteInput, RadioInput } from "../../ui/inputs";
import { Button } from "../../ui/buttons";
import "./AdminSuspendOrganizationForm.scss";
import { ModalFooter } from "../../ui/modal";
import { Formik, FormikActions, FormikProps } from "formik";
import { useMutation } from "@apollo/react-hooks";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui/toasts";
import gql from "graphql-tag";
import { VALIDATION_ERRORS } from "../../../config";

const DELETE_CONTRACT = gql`
    mutation DeleteOrganization($id: String!, $organization_id: String!) {
        deleteContract(id: $id, organization_id: $organization_id)
    }
`;

const UPDATE_CONTRACT = gql`
    mutation UpdateContract($id: String!, $organization_id: String!) {
        updateContract(id: $id, organization_id: $organization_id) {
            id
        }
    }
`;

const adminSuspendOrganizationFormInitialValues = {
    action: "reassign",
    selectedOrganizationId: "",
};

export interface IAdminSuspendOrganizationFormContract {
    id: string;
}

interface IAdminSuspendOrganizationFormState {
    action: string;
    selectedOrganizationId: string;
}

interface IAdminSuspendOrganizationFormBaseProps extends FormikProps<IAdminSuspendOrganizationFormState> {}

const AdminSuspendOrganizationFormBase: FunctionComponent<IAdminSuspendOrganizationFormBaseProps> = ({
    values,
    handleSubmit,
    handleChange,
    setFieldValue,
    errors,
    touched,
}) => {
    function selectOrganization(organization: any) {
        setFieldValue("selectedOrganizationId", organization.id);
    }

    return (
        <form className="f-admin-suspend-organization" onSubmit={handleSubmit}>
            <h2 className="f-admin-suspend-organization__title h1">Remove organization</h2>
            <div className="f-admin-suspend-organization__radio-group">
                <RadioInput
                    value="delete"
                    name="action"
                    label="Delete all contracts associated with this organizations"
                    checked={values.action === "delete"}
                    onChange={handleChange}
                />
                <RadioInput
                    value="reassign"
                    name="action"
                    label="Assign contracts to another organization"
                    checked={values.action === "reassign"}
                    onChange={handleChange}
                />
            </div>
            <AutocompleteInput
                placeholder="Start typing to find organization..."
                disabled={values.action === "delete"}
                searchFor="nonClientOrganizations"
                onChange={selectOrganization}
                name="selectedOrganizationId"
                error={errors && errors.selectedOrganizationId}
                touched={touched && touched.selectedOrganizationId}
            />
            <ModalFooter>
                <Button type="submit" text="Remove" primary icon="check" />
            </ModalFooter>
        </form>
    );
};

interface IAdminSuspendOrganizationFormProps {
    organizationId: string;
    contracts: IAdminSuspendOrganizationFormContract[];
    hideModal: Function;
    refetch: Function;
}

export const AdminSuspendOrganizationForm: FunctionComponent<IAdminSuspendOrganizationFormProps> = ({
    organizationId,
    contracts,
    refetch,
    hideModal,
}) => {
    const [removeContractMutation] = useMutation(DELETE_CONTRACT);
    const [updateContractMutation] = useMutation(UPDATE_CONTRACT);

    async function removeContract(id: string, organization_id: string) {
        try {
            const { errors } = await removeContractMutation({
                variables: {
                    id,
                    organization_id,
                },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    async function removeAllContracts(contracts: IAdminSuspendOrganizationFormContract[], organizationId: string) {
        await Promise.all(contracts.map(({ id }: IAdminSuspendOrganizationFormContract) => removeContract(id, organizationId)));
        showSuccessToast("Removed");
    }

    async function updateContract(id: string, organization_id: string) {
        try {
            const { errors } = await updateContractMutation({
                variables: {
                    id,
                    organization_id,
                },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    async function updateAllContracts(contracts: IAdminSuspendOrganizationFormContract[], organizationId: string) {
        await Promise.all(contracts.map(({ id }: IAdminSuspendOrganizationFormContract) => updateContract(id, organizationId)));
        showSuccessToast("Updated");
    }

    async function onSubmit(values: IAdminSuspendOrganizationFormState, form: FormikActions<IAdminSuspendOrganizationFormState>) {
        if (values.action === "delete") {
            await removeAllContracts(contracts, organizationId);
            refetch();
            hideModal();
        } else {
            if (values.selectedOrganizationId) {
                await updateAllContracts(contracts, values.selectedOrganizationId);
                refetch();
                hideModal();
            } else {
                form.setFieldError("selectedOrganizationId", VALIDATION_ERRORS.required);
            }
        }
    }

    return (
        <Formik
            onSubmit={onSubmit}
            initialValues={adminSuspendOrganizationFormInitialValues}
            render={(props) => <AdminSuspendOrganizationFormBase {...props} />}
        />
    );
};
