import React, { FunctionComponent } from "react";
import { FormFooterWithSave, FormAccordionContent, IFormFooterWithSaveProps } from "../formsUI";
import { Input, Select, Row, Column } from "../../ui";
import { Formik, FormikProps } from "formik";
import { IFormWithFormik } from "../../../config/types";
import * as Yup from "yup";
import { VALIDATION_ERRORS } from "../../../config";
import { getObjectTypesOptions } from "../../../utils";
import { useObjectTypes } from "../../../hooks";

const ObjectBasicInfoFormSchema = Yup.object().shape({
    type: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    name: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
});

export interface IObjectBasicInfoFormState {
    type: string;
    name: string;
}

interface IObjectBasicInfoFormBaseProps extends FormikProps<IObjectBasicInfoFormState>, IFormFooterWithSaveProps {}

const ObjectBasicInfoFormBase: FunctionComponent<IObjectBasicInfoFormBaseProps> = ({ saveAndContinue, ...formikProps }) => {
    const { handleSubmit, handleChange, handleBlur, values, errors, touched } = formikProps;

    const types = useObjectTypes(true);

    return (
        <form onSubmit={handleSubmit}>
            <FormAccordionContent>
                <Row gutter="sm">
                    <Column>
                        <Select
                            options={getObjectTypesOptions(types)}
                            placeholder="Apartment"
                            name="type"
                            error={errors && errors.type}
                            touched={touched && touched.type}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.type}
                            marginBottom="none"
                            textCapitalize={false}
                        />
                    </Column>
                    <Column>
                        <Input
                            placeholder="Name"
                            marginBottom="none"
                            name="name"
                            error={errors && errors.name}
                            touched={touched && touched.name}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.name}
                        />
                    </Column>
                </Row>
            </FormAccordionContent>
            <FormFooterWithSave saveAndContinue={saveAndContinue} {...formikProps} />
        </form>
    );
};

interface IObjectBasicInfoFormProps extends IFormWithFormik<IObjectBasicInfoFormState>, IFormFooterWithSaveProps {}

export const ObjectBasicInfoForm: FunctionComponent<IObjectBasicInfoFormProps> = ({ onSubmit, initialValues, saveAndContinue }) => {
    return (
        <Formik
            onSubmit={onSubmit}
            initialValues={initialValues}
            enableReinitialize={true}
            validationSchema={ObjectBasicInfoFormSchema}
            render={(props) => <ObjectBasicInfoFormBase {...props} saveAndContinue={saveAndContinue} />}
        />
    );
};
