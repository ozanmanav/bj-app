import * as Yup from "yup";
import { VALIDATION_ERRORS } from "../../../config";
import { FormikProps } from "formik";

export const EmployeeValidationSchema = Yup.object().shape({
    first_name: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    last_name: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    phone: Yup.number()
        .typeError(VALIDATION_ERRORS.number)
        .required(VALIDATION_ERRORS.required),
    phone_code: Yup.number().required(VALIDATION_ERRORS.required),
    mobile_phone: Yup.number().typeError(VALIDATION_ERRORS.number),
    email: Yup.string()
        .email(VALIDATION_ERRORS.email)
        .required(VALIDATION_ERRORS.required),
});

export const employeeDefaultState = {
    first_name: "",
    last_name: "",
    email: "",
    job_title: "",
    phone: "",
    phone_code: "",
    mobile_phone: "",
    mobile_phone_code: "",
    language: "",
    note: "",
};

export interface IEmployeeDefaultState {
    first_name: string;
    last_name: string;
    email: string;
    job_title: string;
    phone: string;
    phone_code: string;
    mobile_phone: string;
    mobile_phone_code: string;
    language: string;
    note?: string;
}

export interface IEmployeeFormProps {
    onSubmit: (state: IEmployeeDefaultState) => void;
    initialValues?: IEmployeeDefaultState;
    title?: string;
    organizationID: string;
}

export interface IEmployeeFormBaseProps extends FormikProps<IEmployeeDefaultState> {
    isEditing: boolean;
    title?: string;
    organizationID: string;
}
