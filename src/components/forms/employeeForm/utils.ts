import { phoneCodesByCountry } from "../../../config";

export function getOrganizationCountryCode(countryName: string) {
    const countryCodeObject = phoneCodesByCountry.find(({ label }) => label.toLocaleLowerCase().includes(countryName.toLocaleLowerCase()));

    return countryCodeObject ? countryCodeObject.value : "";
}
