import gql from "graphql-tag";

export const GET_ORGANIZATION_COUNTRY = gql`
    query OrganizationCountry($id: String!) {
        organizations(id: $id) {
            data {
                id
                address {
                    country
                }
            }
        }
    }
`;
