import React, { FunctionComponent, useEffect } from "react";
import "./EmployeeForm.scss";
import { Formik } from "formik";
import { Column, Row } from "../../ui/grid";
import { Input, Select, TextArea } from "../../ui/inputs";
import { ModalFooter } from "../../ui/modal";
import { Button } from "../../ui/buttons";
import { useQuery } from "@apollo/react-hooks";
import { getJobTitlesOptions, getLanguagesOptions } from "../../../utils";
import get from "lodash.get";
import {
    IEmployeeFormProps,
    employeeDefaultState,
    EmployeeValidationSchema,
    IEmployeeFormBaseProps,
} from "./definitions";
import { GET_ORGANIZATION_COUNTRY } from "./queries";
import { getOrganizationCountryCode } from "./utils";
import { phoneCodesByCountry } from "../../../config";
import { useUserLanguages, useJobTitles } from "../../../hooks";

const EmployeeFormBase: FunctionComponent<IEmployeeFormBaseProps> = ({
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    handleSubmit,
    isEditing,
    title,
    setFieldValue,
    organizationID,
}) => {
    const { data: organizationCountryData } = useQuery(GET_ORGANIZATION_COUNTRY, {
        variables: {
            id: organizationID,
        },
        fetchPolicy: "network-only",
    });

    const employeeLanguages = useUserLanguages();
    const jobTitles = useJobTitles();
    const organizationCountry = get(organizationCountryData, "organizations.data[0].address.country");

    useEffect(() => {
        if (values.phone_code.trim().length === 0 && organizationCountry) {
            setFieldValue("phone_code", getOrganizationCountryCode(organizationCountry));
        }
        if (values.mobile_phone_code.trim().length === 0 && organizationCountry) {
            setFieldValue("mobile_phone_code", getOrganizationCountryCode(organizationCountry));
        }
    }, [values, organizationCountry]);

    function onSelfSubmit() {
        handleSubmit();
    }

    return (
        <div className="f-contact">
            <h2 className="f-contact__title h1">{title ? title : isEditing ? "Update person" : "Create New Person"}</h2>
            <Row gutter="sm">
                <Column>
                    <Input
                        placeholder="First name"
                        name="first_name"
                        value={values.first_name}
                        error={errors.first_name}
                        touched={touched.first_name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                </Column>
                <Column>
                    <Input
                        placeholder="Last name"
                        name="last_name"
                        value={values.last_name}
                        error={errors.last_name}
                        touched={touched.last_name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                </Column>
            </Row>
            <Row>
                <Column width={12}>
                    <Select
                        options={getJobTitlesOptions(jobTitles)}
                        placeholder="Job Title"
                        name="job_title"
                        value={values.job_title}
                        error={errors.job_title}
                        touched={touched.job_title}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                </Column>
            </Row>
            <Row>
                <Column width={12}>
                    <Input
                        placeholder="E-Mail"
                        name="email"
                        value={values.email}
                        error={errors.email}
                        touched={touched.email}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                </Column>
            </Row>
            <Row>
                <Column width={12} className="flex align-center">
                    <Select
                        options={phoneCodesByCountry}
                        placeholder="Phone code"
                        name="phone_code"
                        value={values.phone_code}
                        error={errors.phone_code}
                        touched={touched.phone_code}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className="f-contact__phone-code"
                    />
                    <Input
                        placeholder="Phone number"
                        name="phone"
                        value={values.phone}
                        error={errors.phone}
                        touched={touched.phone}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                </Column>
            </Row>
            <Row>
                <Column width={12} className="flex align-center">
                    <Select
                        options={phoneCodesByCountry}
                        placeholder="Phone code"
                        name="mobile_phone_code"
                        value={values.mobile_phone_code}
                        error={errors.mobile_phone_code}
                        touched={touched.mobile_phone_code}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className="f-contact__phone-code"
                    />
                    <Input
                        placeholder="Mobile number"
                        name="mobile_phone"
                        value={values.mobile_phone}
                        error={errors.mobile_phone}
                        touched={touched.mobile_phone}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                </Column>
            </Row>
            <Row>
                <Column width={12}>
                    <Select
                        options={getLanguagesOptions(employeeLanguages)}
                        placeholder="Language"
                        name="language"
                        value={values.language}
                        error={errors.language}
                        touched={touched.language}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                </Column>
            </Row>
            <Row>
                <Column width={12}>
                    <TextArea
                        placeholder="Note"
                        name="note"
                        rows={6}
                        marginBottom="none"
                        value={values.note}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                </Column>
            </Row>
            <ModalFooter>
                <Button text="Save" primary icon="check" onClick={onSelfSubmit} />
            </ModalFooter>
        </div>
    );
};

export const EmployeeForm: FunctionComponent<IEmployeeFormProps> = ({
    onSubmit,
    initialValues,
    title,
    organizationID,
}) => {
    return (
        <Formik
            onSubmit={onSubmit}
            initialValues={initialValues || employeeDefaultState}
            validationSchema={EmployeeValidationSchema}
            render={(formikProps) => (
                <EmployeeFormBase
                    {...formikProps}
                    title={title}
                    isEditing={!!initialValues}
                    organizationID={organizationID}
                />
            )}
        />
    );
};
