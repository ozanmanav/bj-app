import { CUSTOM_FIELD_NAME, IMetafield, VALIDATION_ERRORS } from "../../../config";
import gql from "graphql-tag";
import * as Yup from "yup";
import { getYupType } from "../../../utils";
import { ILocalAutocompleteOption } from "../../ui/inputs/autocomplete";

export const DEFAULT_FIELDS = [
    {
        name: CUSTOM_FIELD_NAME,
        type: "string",
    },
];

export const DYNAMIC_INFO_ROW_DEFAULT_VALUE = {
    name: CUSTOM_FIELD_NAME,
    type: "string",
    value: "",
    customName: "",
    context: "",
    meta: {
        source: "",
        source_url: "",
        source_date: "",
        verification: false,
        verification_by: "",
    },

    highlighted: false,
};

export interface IInfoRowParams extends IMetafield {
    customName?: string;
}

export interface IDynamicInfoRowsState {
    rows: IInfoRowParams[] | { [key: string]: IInfoRowParams[] };
}

export type TFormTypes = "single";

export const GET_ENTITY_META = gql`
    query TypeMeta($model_id: String!) {
        type_meta(model_id: $model_id) {
            data {
                name
                type
            }
        }
    }
`;

export const DynamicsInfoRowSchema = Yup.array().of(
    Yup.object().shape({
        name: Yup.string()
            .trim()
            .required(VALIDATION_ERRORS.required),
        type: Yup.string()
            .trim()
            .required(VALIDATION_ERRORS.required),
        value: Yup.mixed()
            .when("type", (type: string) => getYupType(type))
            .required(VALIDATION_ERRORS.required),
        customName: Yup.mixed().when("name", (field: string) =>
            field === CUSTOM_FIELD_NAME
                ? Yup.string()
                      .trim()
                      .required(VALIDATION_ERRORS.required)
                : Yup.string()
        ),
    })
);

export interface IDynamicInfoRowSourceResults extends ILocalAutocompleteOption {
    type: "organization_client" | "organization" | "user";
}
