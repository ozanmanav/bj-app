import React, { ChangeEvent, FunctionComponent, useEffect, useState } from "react";
import { CUSTOM_FIELD_NAME, defaultValueTypes, IMetatype, IOrganization, DATE_FORMAT, DATE_FORMAT_WITH_TIME } from "../../../config";
import { formatDate, parseDateForDatePicker } from "../../ui/datepicker/utils";
import classNames from "classnames";
import { Column, Row } from "../../ui/grid";
import { Select } from "../../ui/inputs/select";
import { getMetafieldNames } from "../../../utils";
import { Input, InputCheckbox } from "../../ui/inputs/input";
import { DatepickerSimple } from "../../ui/datepicker";
import { TextArea } from "../../ui/inputs/textarea";
import { LocalAutocomplete } from "../../ui/inputs/autocomplete";
import { FormActions } from "../formsUI";
import { Button, RemoveButton, SaveButton } from "../../ui/buttons";
import { UserRoleCheck } from "../../userRoleCheck";
import { ArrayHelpers, FormikActions, FormikErrors, FormikHandlers, FormikTouched } from "formik";
import { AddRowButton } from "./AddRowButton";
import { DEFAULT_FIELDS, IDynamicInfoRowSourceResults, IDynamicInfoRowsState, IInfoRowParams, TFormTypes } from "./config";
import { useDebounce } from "use-debounce";
import { AUTOCOMPLETE_DEBOUNCE_TIME_MS, AUTOCOMPLETE_SEARCH_QUERIES } from "../../ui/inputs/";
import { useApolloClient } from "@apollo/react-hooks";
import { createSourceOrganizationLink, getSource } from "./utils";
import get from "lodash.get";
import { COUNTRIES } from "../../../config/countries";

interface IInfoRowProps extends FormikHandlers, FormikActions<IDynamicInfoRowsState>, IInfoRowParams, ArrayHelpers {
    isLast: boolean;
    row: number;
    errors?: FormikErrors<IInfoRowParams>;
    touched?: FormikTouched<IInfoRowParams>;
    fields?: IMetatype[];
    removeCustomField?: boolean;
    formType?: TFormTypes;
    removeSingleRow: (id?: string) => void;
    suffix: string;
    saveButtonType: "submit" | "reset" | "button";
    cancel?: () => void;
}

export const DynamicInfoRow: FunctionComponent<IInfoRowProps> = ({
    id,
    row,
    errors,
    touched,
    isLast,
    name,
    type,
    customName,
    value,
    context,
    meta: { source, source_url, source_date, verification, verification_by },
    handleChange,
    handleBlur,
    setFieldValue,
    setFieldTouched,
    remove,
    removeSingleRow,
    push,
    formType,
    fields = [],
    suffix,
    saveButtonType,
    removeCustomField,
    cancel,
    created_at,
}) => {
    const parsedSourceDate = source_date ? parseDateForDatePicker(source_date, DATE_FORMAT_WITH_TIME) : undefined;
    const apolloClient = useApolloClient();

    const [isExpanded, setIsExpanded] = useState<boolean>(false);
    const [sourceSearch, setSourceSearch] = useState<string>("");
    const [debouncedSourceSearch] = useDebounce(sourceSearch, AUTOCOMPLETE_DEBOUNCE_TIME_MS);
    const [sourcesSearchResults, setSourcesSearchResults] = useState<IDynamicInfoRowSourceResults[]>([]);
    const countries = COUNTRIES.map((country) => ({ value: country.name }));

    useEffect(() => {
        async function getSourceSearchResults() {
            const response = await Promise.all([
                apolloClient.query({
                    query: AUTOCOMPLETE_SEARCH_QUERIES.allOrganizations,
                    variables: {
                        searchBy: debouncedSourceSearch,
                    },
                }),
                apolloClient.query({
                    query: AUTOCOMPLETE_SEARCH_QUERIES.users,
                    variables: {
                        searchBy: debouncedSourceSearch,
                    },
                }),
            ]);

            const organizations = (get(response[0], "data.organizations.data") || []).map((organization: IOrganization) => ({
                id: organization.id,
                value: organization.name,
                label: organization.name,
                type: `organization${organization.category === "CLIENT" ? "_client" : ""}`,
            }));
            const users = (get(response[1], "data.users.data") || []).map((user: { id: string; name: string }) => ({
                id: user.id,
                value: user.name,
                label: user.name,
                type: "user",
            }));

            setSourcesSearchResults([...organizations, ...users]);
        }

        if (debouncedSourceSearch && apolloClient) {
            getSourceSearchResults();
        }
    }, [debouncedSourceSearch, apolloClient]);

    const valueTypeNormalized = type ? type.toLowerCase() : "";
    const fieldNames = removeCustomField ? fields : [...DEFAULT_FIELDS, ...fields];
    const isCustom = customName !== undefined;

    const rowName = `rows${suffix}`;
    const rowValuePrefix = `${rowName}[${row}]`;

    function onSelfChange(e: ChangeEvent<HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement>) {
        if (e.target.name.includes("name")) {
            setFieldValue(`${rowValuePrefix}.customName`, e.target.value === CUSTOM_FIELD_NAME ? "" : undefined);

            const selectedField = fieldNames.find(({ name }) => name === e.target.value);

            if (selectedField) {
                setFieldValue(`${rowValuePrefix}.type`, selectedField.type);
            }
        }
        if (e.target.name === `${rowValuePrefix}.type`) {
            setFieldValue(`${rowValuePrefix}.value`, "");
        }

        if (e.target.name === `${rowValuePrefix}.country`) {
            const selectedField = countries.find(({ value }) => value === e.target.value);

            if (selectedField) setFieldValue(`${rowValuePrefix}.value`, selectedField.value);
        }

        if (e.target.name === `${rowValuePrefix}.verification` && !(e.target as HTMLInputElement).checked) {
            setFieldValue(`${rowValuePrefix}.verification_by`, "");
        }

        handleChange(e);
    }

    function onValueDateChange(e: Date) {
        setFieldValue(`${rowValuePrefix}.value`, formatDate(e));
    }

    function onValueDateBlur() {
        setFieldTouched(`${rowValuePrefix}.value`, true);
    }

    function onSourceDateChange(e: Date) {
        setFieldValue(`${rowValuePrefix}.meta.source_date`, formatDate(e, DATE_FORMAT));
    }

    function onSourceDateBlur() {
        setFieldTouched(`${rowValuePrefix}.meta.source_date`, true);
    }

    function onSelfRemove() {
        id && removeSingleRow ? removeSingleRow(id) : remove(row);
    }

    function onSourceChange(value: string) {
        setFieldValue(`${rowValuePrefix}.meta.source`, value);
        setFieldTouched(`${rowValuePrefix}.meta.source`, true);
        setSourceSearch(value);
    }

    function onSourceSelect(source: IDynamicInfoRowSourceResults) {
        setFieldValue(`${rowValuePrefix}.meta.source`, source.type !== "user" ? createSourceOrganizationLink(source) : source.value);
    }

    function selfToggle() {
        setIsExpanded((prevState) => !prevState);
    }

    const showMoreButtonClassName = classNames(["b-dynamic-info-row__show-more-btn", { _expanded: isExpanded }]);

    const formattedSource = getSource(source || "");
    const sourceToShow = typeof formattedSource === "object" ? formattedSource.name : formattedSource;
    return (
        <>
            <Row gutter="sm">
                <Column width={3}>
                    <Select
                        options={getMetafieldNames(fieldNames)}
                        placeholder="Please Select"
                        name={`${rowValuePrefix}.name`}
                        value={name}
                        onChange={onSelfChange}
                        onBlur={handleBlur}
                        error={errors && errors.name}
                        touched={touched && touched.name}
                    />
                </Column>
                <Column width={3}>
                    <Select
                        options={defaultValueTypes}
                        disabled={!isCustom}
                        placeholder="Number"
                        name={`${rowValuePrefix}.type`}
                        value={type}
                        onChange={onSelfChange}
                        onBlur={handleBlur}
                        error={errors && errors.type}
                        touched={touched && touched.type}
                    />
                </Column>
                {isCustom && (
                    <Column width={3}>
                        <Input
                            placeholder="Custom"
                            onChange={onSelfChange}
                            onBlur={handleBlur}
                            name={`${rowValuePrefix}.customName`}
                            value={customName}
                            error={errors && errors.customName}
                            touched={touched && touched.customName}
                        />
                    </Column>
                )}
                <Column width={isCustom ? 3 : 6}>
                    {valueTypeNormalized === "date" ? (
                        <DatepickerSimple
                            defaultValue={value ? parseDateForDatePicker(value) : undefined}
                            onBlur={onValueDateBlur}
                            onChange={onValueDateChange}
                            touched={touched && touched.value}
                            error={errors && errors.value}
                            fullWidth
                        />
                    ) : (
                        (() => {
                            if (name === "country") {
                                return (
                                    <Select
                                        options={countries}
                                        name={`${rowValuePrefix}.country`}
                                        value={value}
                                        onChange={onSelfChange}
                                        onBlur={handleBlur}
                                        error={errors && errors.type}
                                        touched={touched && touched.type}
                                    />
                                );
                            } else {
                                return (
                                    <Input
                                        placeholder="Value"
                                        onChange={onSelfChange}
                                        onBlur={handleBlur}
                                        name={`${rowValuePrefix}.value`}
                                        value={value}
                                        error={errors && errors.value}
                                        touched={touched && touched.value}
                                    />
                                );
                            }
                        })()
                    )}
                </Column>
                <Column width={12}>
                    <TextArea
                        placeholder="Remarks"
                        onChange={onSelfChange}
                        onBlur={handleBlur}
                        name={`${rowValuePrefix}.context`}
                        value={context || ""}
                        error={errors && errors.context}
                        touched={touched && touched.context}
                        rows={3}
                    />
                </Column>
                {isExpanded && (
                    <>
                        <Column width={4}>
                            <LocalAutocomplete
                                options={sourcesSearchResults}
                                onInputChange={onSourceChange}
                                placeholder="Source"
                                onChange={onSourceSelect}
                                onBlur={handleBlur}
                                name={`${rowValuePrefix}.meta.source`}
                                value={source}
                                defaultInputValue={sourceToShow}
                                error={errors && errors.meta?.source}
                                touched={touched && touched.meta?.source}
                                clearInputAfterSelect={false}
                            />
                        </Column>
                        <Column width={4}>
                            <Input
                                placeholder="Source url"
                                onChange={onSelfChange}
                                onBlur={handleBlur}
                                name={`${rowValuePrefix}.meta.source_url`}
                                value={source_url}
                                error={errors && errors.meta?.source_url}
                                touched={touched && touched.meta?.source_url}
                            />
                        </Column>
                        <Column width={4}>
                            <DatepickerSimple
                                placeholder="Source Date"
                                defaultValue={parsedSourceDate}
                                onBlur={onSourceDateBlur}
                                onChange={onSourceDateChange}
                                touched={touched && touched.meta?.source_date}
                                error={errors && errors.meta?.source_date}
                                fullWidth
                            />
                        </Column>
                        <Column width={6}>
                            <InputCheckbox
                                placeholder="Verification"
                                onChange={onSelfChange}
                                onBlur={handleBlur}
                                name={`${rowValuePrefix}.meta.verification`}
                                checked={verification}
                                error={errors && errors.meta?.verification}
                                touched={touched && touched.meta?.verification}
                            />
                        </Column>
                        <Column width={6}>
                            <Input
                                placeholder="Verification by"
                                onChange={onSelfChange}
                                onBlur={handleBlur}
                                name={`${rowValuePrefix}.meta.verification_by`}
                                value={verification_by}
                                error={errors && errors.meta?.verification_by}
                                touched={touched && touched.meta?.verification_by}
                                disabled={!verification}
                            />
                        </Column>
                    </>
                )}
            </Row>
            <FormActions className="b-dynamic-info-row__actions">
                {saveButtonType === "submit" && <SaveButton type={saveButtonType} />}
                <Button
                    text={isExpanded ? "Hide meta fields" : "Show meta fields"}
                    icon="nextArrowGrey"
                    className={showMoreButtonClassName}
                    onClick={selfToggle}
                />
                <UserRoleCheck
                    skipCheck={formType !== "single"}
                    availableForRoles={[
                        "owner_administrator",
                        "manufacturer_administrator",
                        "manufacturer_brand_manager",
                        "manufacturer_object_manager",
                        "property_manager_administrator",
                        "property_manager_building_manager",
                        "service_provider_administrator",
                    ]}
                >
                    {created_at && (
                        <RemoveButton className={classNames({ "f-building-details__entity-remove": !isLast })} onClick={onSelfRemove} />
                    )}
                </UserRoleCheck>
                {formType === "single" && cancel && <Button text="Cancel" onClick={cancel} />}
                {formType !== "single" && isLast && <AddRowButton push={push} />}
            </FormActions>
        </>
    );
};
