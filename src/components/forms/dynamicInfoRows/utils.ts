import { IDynamicInfoRowSourceResults } from "./config";

const MARKDOWN_LINK_REGEX = /(?:__|[*#])|\[(.*?)\]\((.+?)\)/;

export function createSourceOrganizationLink(source: IDynamicInfoRowSourceResults): string {
    return `[${source.value}](/app/organizations/${source.type === "organization_client" ? "client/" : ""}${source.id})`;
}

export function getSource(source: string): string | { href: string; name: string } {
    const regexRes = MARKDOWN_LINK_REGEX.exec(source);

    if (regexRes) {
        return {
            href: regexRes[2],
            name: regexRes[1],
        };
    }

    return source;
}
