import React, { FunctionComponent } from "react";
import { AddButton } from "../../ui/buttons";
import { DYNAMIC_INFO_ROW_DEFAULT_VALUE } from "./config";

export const AddRowButton: FunctionComponent<{ push: Function }> = ({ push }) => {
    function addRow() {
        push(DYNAMIC_INFO_ROW_DEFAULT_VALUE);
    }

    return <AddButton text="Add more" onClick={addRow} />;
};
