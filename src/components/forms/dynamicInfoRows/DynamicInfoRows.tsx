import React, { FunctionComponent } from "react";
import "./DynamicInfoRows.scss";
import { FieldArray, FormikProps } from "formik";
import { useQuery } from "@apollo/react-hooks";
import get from "lodash.get";
import { detailsSortFunction } from "../../object/objectDetailsTable/utils";
import { AddRowButton } from "./AddRowButton";
import { GET_ENTITY_META, IDynamicInfoRowsState, TFormTypes } from "./config";
import { DynamicInfoRow } from "./DynamicInfoRow";
import { Loader } from "../../ui/loader";

interface IDynamicInfoRowProps {
    variant: "organization" | "object";
    variantID?: string;
    variantTypeID?: string;
    suffix?: string;
    formType?: TFormTypes;
    removeCustomField?: boolean;
    saveButtonType?: "submit" | "reset" | "button";
    removeSingleRow?: () => void;
    remove?: (item: any) => void;
    fetchPolicy?: "no-cache";
    entityId?: string;
    cancel?: () => void;
}

export const DynamicInfoRows: FunctionComponent<FormikProps<IDynamicInfoRowsState> & IDynamicInfoRowProps> = ({
    values,
    handleSubmit,
    errors,
    touched,
    suffix = "",
    formType,
    removeSingleRow,
    remove,
    saveButtonType = "button",
    fetchPolicy,
    entityId,
    cancel,
    variant,
    variantID,
    variantTypeID,
    removeCustomField,
    ...formikProps
}) => {
    const { data, loading: loadingMeta } = useQuery(GET_ENTITY_META, {
        variables: {
            model_id: variantTypeID,
        },
        fetchPolicy: "no-cache",
    });

    const fields = get(data, "type_meta.data") || [];
    const sortedFields = formType === "single" ? fields.sort(detailsSortFunction) : fields;
    const rowsName = `rows${suffix}`;

    const rowsValues = get(values, rowsName);
    const rowsErrors = get(errors, rowsName);
    const rowsTouched = get(touched, rowsName);

    if (loadingMeta) return <Loader rawLoader text="Loading..." />;

    return (
        <FieldArray
            name={rowsName}
            render={(arrayHelpers) => (
                <>
                    {rowsValues &&
                        rowsValues.map((row: any, index: number) => (
                            <DynamicInfoRow
                                removeCustomField={removeCustomField}
                                isLast={rowsValues.length === index + 1}
                                key={index}
                                row={index}
                                handleSubmit={handleSubmit}
                                errors={rowsErrors && rowsErrors[index]}
                                touched={rowsTouched && rowsTouched[index]}
                                fields={sortedFields}
                                formType={formType}
                                removeSingleRow={removeSingleRow}
                                remove={remove}
                                suffix={suffix}
                                saveButtonType={saveButtonType}
                                cancel={cancel}
                                {...formikProps}
                                {...arrayHelpers}
                                {...row}
                            />
                        ))}
                    {formType !== "single" && rowsValues && rowsValues.length === 0 && <AddRowButton push={arrayHelpers.push} />}
                </>
            )}
        />
    );
};
