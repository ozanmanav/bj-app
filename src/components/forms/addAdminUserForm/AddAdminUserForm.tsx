import React, { FunctionComponent } from "react";
import { Formik } from "formik";
import { Column, Row } from "../../ui/grid";
import { Input } from "../../ui/inputs";
import { Button } from "../../ui/buttons";
import { ModalFooter } from "../../ui/modal";
import { FormTitle } from "../formTitle";
import "./AddAdminUserForm.scss";
import { IAddAdminUserFormBaseProps, addAdminUserFormState, AddAdminUserFormSchema, IAddAdminUserFormProps } from "./definitions";
import { Icon } from "../../ui";

const AddAdminUserFormBase: FunctionComponent<IAddAdminUserFormBaseProps> = ({ title, goToStepOne, ...formikProps }) => {
    const { values, errors, touched, handleChange, handleBlur, handleSubmit } = formikProps;

    return (
        <form onSubmit={handleSubmit}>
            <FormTitle text={title} />
            <button className="b-add-file-modal__back h6 _font-bold _text-uppercase flex align-center" onClick={goToStepOne}>
                <Icon icon="collapse" className="b-add-file-modal__back-icon" />
                Back
            </button>
            <Row gutter="sm">
                <Column>
                    <Input
                        autoComplete="no"
                        placeholder="First name"
                        name="first_name"
                        value={values.first_name}
                        error={errors && errors.first_name}
                        touched={touched && touched.first_name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                </Column>
                <Column>
                    <Input
                        autoComplete="no"
                        placeholder="Last name"
                        name="last_name"
                        value={values.last_name}
                        error={errors && errors.last_name}
                        touched={touched && touched.last_name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                </Column>
            </Row>
            <Input
                autoComplete="no"
                placeholder="E-Mail"
                type="email"
                name="email"
                value={values.email}
                error={errors.email}
                touched={touched.email}
                onChange={handleChange}
                onBlur={handleBlur}
            />
            <ModalFooter>
                <Button text="Save user" primary icon="check" type="submit" />
            </ModalFooter>
        </form>
    );
};

export const AddAdminUserForm: FunctionComponent<IAddAdminUserFormProps> = ({ callback, goToStepOne }) => {
    const title = "Add Admin User to Organization (Client)";

    return (
        <Formik
            onSubmit={callback}
            initialValues={addAdminUserFormState}
            validationSchema={AddAdminUserFormSchema}
            render={(props) => <AddAdminUserFormBase title={title} goToStepOne={goToStepOne} {...props} />}
        />
    );
};
