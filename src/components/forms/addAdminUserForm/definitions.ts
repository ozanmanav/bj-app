import * as Yup from "yup";
import { VALIDATION_ERRORS } from "../../../config";
import { FormikProps, FormikActions } from "formik";

export const addAdminUserFormState = {
    first_name: "",
    last_name: "",
    email: "",
};

export interface IAddAdminUserFormBaseState {
    first_name: string;
    last_name: string;
    email: string;
}

export interface IAddAdminUserFormBaseProps extends FormikProps<IAddAdminUserFormBaseState> {
    goToStepOne?: () => void;
    title: string;
}

export interface IAddAdminUserFormProps {
    goToStepOne?: () => void;
    callback: (state: IAddAdminUserFormBaseState, form: FormikActions<IAddAdminUserFormBaseState>) => void;
}

export const AddAdminUserFormSchema = Yup.object().shape({
    first_name: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    last_name: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    email: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
});
