import { ArrayHelpers, FormikProps } from "formik";
import React, { FunctionComponent } from "react";
import { Column, Row } from "../../ui/grid";
import { Input, Select, TextArea } from "../../ui/inputs";
import { defaultValueTypes, IMetatype } from "../../../config";
import { AddButton, RemoveButton } from "../../ui/buttons";
import "./AdminMetaFormRow.scss";
import get from "lodash.get";

export const DEFAULT_METAFIELD_VALUE = {
    name: "",
    type: "",
    context: "",
};

export interface IAdminMetaFormState {
    name: string;
    parent_name: string;
    child_name: string;
    meta: IMetatype[];
}

interface IAdminMetaFormRowProps extends FormikProps<IAdminMetaFormState>, ArrayHelpers {
    index: number;
}

export const AdminMetaFormRow: FunctionComponent<IAdminMetaFormRowProps> = ({
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    index,
    remove,
    push,
}) => {
    const rowValues = get(values, `meta[${index}]`);
    const rowErrors = get(errors, `meta[${index}]`);
    const rowTouched = get(touched, `meta[${index}]`);

    function addRow() {
        push(DEFAULT_METAFIELD_VALUE);
    }

    function onSelfRemove() {
        remove(index);
    }

    return (
        <Row gutter="sm" className="f-admin-meta__row">
            <Column>
                <Input
                    placeholder="Name"
                    name={`meta[${index}].name`}
                    value={rowValues.name}
                    error={rowErrors && rowErrors.name}
                    touched={rowTouched && rowTouched.name}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />
            </Column>
            <Column>
                <Select
                    options={defaultValueTypes}
                    placeholder="Type"
                    name={`meta[${index}].type`}
                    value={rowValues.type || ""}
                    error={rowErrors && rowErrors.type}
                    touched={rowTouched && rowTouched.type}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />
            </Column>
            <Column width={12}>
                <TextArea
                    rows={3}
                    placeholder="Context"
                    name={`meta[${index}].context`}
                    value={rowValues.context}
                    error={rowErrors && rowErrors.context}
                    touched={rowTouched && rowTouched.context}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />
            </Column>
            <Column width={12} className="f-admin-meta__actions">
                <RemoveButton onClick={onSelfRemove} />
                {index === values.meta.length - 1 && <AddButton text="Add more" onClick={addRow} />}
            </Column>
        </Row>
    );
};
