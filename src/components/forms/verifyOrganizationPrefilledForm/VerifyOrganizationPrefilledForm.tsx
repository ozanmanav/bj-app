import { ACTIVATE_USER, GET_USER_BY_CODE } from "./queries";
import {
    IVerifyOrganizationPrefilledFormState,
    verifyOrganizationPrefilledFormState,
    verifyOrganizationPrefilledFormSchema,
} from "./definitions";
import { phoneCodesByCountry } from "../../../config";
import { Formik, FormikProps } from "formik";
import { ILoginState, LOGIN } from "../../../views/login/Login";
import { Input, Select } from "../../ui/inputs";
import React, { FunctionComponent, useContext, useEffect, useState } from "react";
import { getPhoneInitialValues, getJobTitlesOptions, getLanguagesOptions, getUserRolesOptions } from "../../../utils";
import { handleGraphQLErrors, showErrorToast } from "../../ui/toasts";
import { useMutation, useQuery } from "@apollo/react-hooks";
import { useJobTitles, useUserLanguages, useUserRoles } from "../../../hooks/apollo";
import { AccountStateContext } from "../../../contexts/accountStateContext";
import { Button } from "../../ui/buttons";
import { RouteComponentProps } from "react-router";
import get from "lodash.get";
import { getLocationSearchParam } from "../../../utils/locationSearchHelpers";
import { Loader } from "../../ui/loader";

interface IVerifyOrganizationPrefilledFormBaseProps extends FormikProps<IVerifyOrganizationPrefilledFormState> {
    goToLogin?: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const VerifyOrganizationPrefilledFormBase: FunctionComponent<IVerifyOrganizationPrefilledFormBaseProps> = ({
    values,
    handleSubmit,
    errors,
    touched,
    handleChange,
    handleBlur,
    goToLogin,
}) => {
    const userRoles = useUserRoles({ organization_role_type: values.organization_role_type });
    const userLanguages = useUserLanguages();
    const jobTitles = useJobTitles();

    return (
        <form className="f-prefilled" onSubmit={handleSubmit}>
            <div className="f-prefilled__body">
                <h2 className="f-prefilled__title h1">Welcome</h2>
                <Input
                    placeholder="Organization name"
                    name="name"
                    disabled={true}
                    value={values.name}
                    error={errors && errors.name}
                    touched={touched && touched.name}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />
                <Input
                    placeholder="First name"
                    name="first_name"
                    value={values.first_name}
                    error={errors && errors.first_name}
                    touched={touched && touched.first_name}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />
                <Input
                    placeholder="Last name"
                    name="last_name"
                    value={values.last_name}
                    error={errors && errors.last_name}
                    touched={touched && touched.last_name}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />
                <Input
                    disabled={true}
                    placeholder="E-Mail"
                    name="email"
                    value={values.email}
                    error={errors && errors.email}
                    touched={touched && touched.email}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />

                <Select
                    options={getJobTitlesOptions(jobTitles)}
                    placeholder="Job Title"
                    name="job_title"
                    disabled={values.userEnabled}
                    value={values.job_title}
                    error={errors.job_title}
                    touched={touched.job_title}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />
                {values.userEnabled && (
                    <Select
                        options={getUserRolesOptions(userRoles)}
                        placeholder="Please Select a Role"
                        value={values.role}
                        disabled={values.userEnabled}
                        error={errors.role}
                        touched={touched.role}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                )}

                {!values.userEnabled && (
                    <>
                        <div className="flex">
                            <Select
                                options={phoneCodesByCountry}
                                placeholder="Phone code"
                                name="phone_code"
                                value={values.phone_code}
                                error={errors.phone_code}
                                touched={touched.phone_code}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                marginBottom="normal"
                                position="top"
                                className="b-new-client-organization-modal__phone-code"
                            />
                            <div className="b-new-client-organization-modal__phone">
                                <Input
                                    type="phone"
                                    placeholder="Phone number"
                                    name="phone"
                                    value={values.phone}
                                    error={errors.phone}
                                    touched={touched.phone}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    marginBottom="normal"
                                />
                            </div>
                        </div>
                        <Select
                            options={getLanguagesOptions(userLanguages)}
                            placeholder="Language"
                            name="language"
                            value={values.language}
                            error={errors.language}
                            touched={touched.language}
                            onChange={handleChange}
                            onBlur={handleBlur}
                        />
                        <Input
                            type="password"
                            placeholder="Password"
                            name="password"
                            value={values.password}
                            error={errors && errors.password}
                            touched={touched && touched.password}
                            onChange={handleChange}
                            onBlur={handleBlur}
                        />
                        <Input
                            type="password"
                            placeholder="Confirm password"
                            name="passwordConfirmation"
                            value={values.passwordConfirmation}
                            error={errors && errors.passwordConfirmation}
                            touched={touched && touched.passwordConfirmation}
                            onChange={handleChange}
                            onBlur={handleBlur}
                        />
                    </>
                )}
            </div>
            <div className="f-prefilled__footer">
                {values.userEnabled ? (
                    <Button className="f-prefilled__button" text="Go to Login" primary onClick={goToLogin} />
                ) : (
                    <Button className="f-prefilled__button" text="Confirm" primary icon="check" type="submit" />
                )}
            </div>
        </form>
    );
};

export const VerifyOrganizationPrefilledForm: FunctionComponent<RouteComponentProps> = ({ history, location }) => {
    const [prefilledInitialState, setPrefilledState] = useState<IVerifyOrganizationPrefilledFormState>(
        verifyOrganizationPrefilledFormState
    );

    const [formLoaded, setFormLoaded] = useState(false);
    const accountStateContext = useContext(AccountStateContext);
    const [activateUserMutation] = useMutation(ACTIVATE_USER);
    const { refetch: loginRefetch } = useQuery(LOGIN, { skip: true });
    const { refetch: getUserRefetch, loading } = useQuery(GET_USER_BY_CODE, { skip: true });

    const verificationCode = getLocationSearchParam(location.search, "verify");
    const organizationID = getLocationSearchParam(location.search, "org");

    useEffect(() => {
        const fillForm = async () => {
            try {
                setFormLoaded(false);

                const { data, errors } = await getUserRefetch({
                    email_verification_code: verificationCode,
                    ...(organizationID && { organization_id: organizationID }),
                });

                if (errors) {
                    handleGraphQLErrors(errors);
                    history.push("/");
                } else {
                    const organization = get(data, "get_user_by_code.organization");
                    const user = get(data, "get_user_by_code.user");
                    const job_id = get(data, "get_user_by_code.job_id");
                    const role_id = get(data, "get_user_by_code.user_role_id");
                    const userEnabled = get(user, "enabled") || false;
                    const splittedPhone = getPhoneInitialValues(user.phone);

                    setPrefilledState({
                        ...verifyOrganizationPrefilledFormState,
                        organization_role_type: get(organization, "role.type"),
                        name: get(organization, "name"),
                        first_name: get(user, "first_name"),
                        last_name: get(user, "last_name"),
                        phone_code: splittedPhone.phone_code,
                        phone: splittedPhone.phone,
                        email: get(user, "email"),
                        job_title: job_id,
                        role: role_id,
                        userEnabled,
                    });

                    setFormLoaded(true);
                }
            } catch (e) {
                console.log(e);
                showErrorToast(e.message);
                history.push("/");
            }
        };

        if (verificationCode) {
            fillForm();
        } else {
            showErrorToast(`Verification Error: ${verificationCode} - ${organizationID}`);
            console.log(`Verification Error: ${verificationCode} - ${organizationID}`);
            history.push("/");
        }
    }, [verificationCode, organizationID]);

    async function activateUser(email_verification_code: string, values: IVerifyOrganizationPrefilledFormState) {
        const { data, errors } = await activateUserMutation({
            variables: {
                email_verification_code,
                organization_id: organizationID,
                first_name: values.first_name,
                last_name: values.last_name,
                phone: values.phone ? `${values.phone_code} ${values.phone}` : null,
                password: values.password,
                password_confirmation: values.passwordConfirmation,
                ...(values.job_title !== "" && { job_id: values.job_title }),
                ...(values.language !== "" && { language: values.language }),
            },
        });

        if (errors) {
            handleGraphQLErrors(errors);
        } else {
            return get(data, "activateUser");
        }
    }

    async function login(values: ILoginState) {
        const { data, errors } = await loginRefetch({
            email: values.email,
            password: values.password,
        });

        if (errors) {
            handleGraphQLErrors(errors);
        } else {
            return get(data, "login");
        }
    }

    async function confirmUser(values: IVerifyOrganizationPrefilledFormState) {
        try {
            setFormLoaded(false);

            const activateUserResponse = await activateUser(verificationCode, values);

            if (!activateUserResponse) {
                throw new Error("Activate User error");
            }

            const user = await login(values);

            if (!user) {
                throw new Error("User does not exist");
            }

            accountStateContext.saveUser(user);
            history.push("/app/cockpit");
        } catch (error) {
            console.log(error);
            showErrorToast(error.message);
            history.push("/");
        }
    }

    const goToLogin = () => {
        accountStateContext.clearAccountData();
        history.push("/login");
    };

    if (!formLoaded || loading) {
        return <Loader />;
    }

    return (
        <Formik
            onSubmit={confirmUser}
            initialValues={prefilledInitialState}
            validationSchema={verifyOrganizationPrefilledFormSchema}
            render={(props) => <VerifyOrganizationPrefilledFormBase goToLogin={goToLogin} {...props} />}
        />
    );
};
