import * as Yup from "yup";
import { VALIDATION_ERRORS, PASSWORD_MIN_LENGTH } from "../../../config";

export interface IVerifyOrganizationPrefilledFormState {
    name: string;
    first_name: string;
    last_name: string;
    email: string;
    job_title: string;
    role: string;
    phone: string;
    phone_code: string;
    language: string;
    password: string;
    passwordConfirmation: string;
    organization_role_type: string;
    userEnabled: boolean;
}

export const verifyOrganizationPrefilledFormState = {
    name: "",
    first_name: "",
    last_name: "",
    email: "",
    job_title: "",
    role: "",
    phone: "",
    phone_code: "",
    language: "",
    password: "",
    passwordConfirmation: "",
    organization_role_type: "",
    userEnabled: false,
};

export const verifyOrganizationPrefilledFormSchema = Yup.object().shape({
    userEnabled: Yup.boolean(),
    first_name: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    last_name: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    email: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    password: Yup.string()
        .trim()
        .when("userEnabled", {
            is: false,
            then: Yup.string()
                .min(PASSWORD_MIN_LENGTH, VALIDATION_ERRORS.passwordMinLength)
                .required(VALIDATION_ERRORS.password),
        }),
    passwordConfirmation: Yup.string()
        .trim()
        .when("isUserEnabled", {
            is: false,
            then: Yup.string()
                .oneOf([Yup.ref("password")], VALIDATION_ERRORS.passwordConfirmation)
                .required(VALIDATION_ERRORS.password),
        }),
});
