import gql from "graphql-tag";

export const GET_CURRENT_CLIENT = gql`
    query CurrentClient {
        current_client {
            id
        }
    }
`;

export const ACTIVATE_USER = gql`
    mutation ActivateUser(
        $email_verification_code: String!
        $first_name: String!
        $last_name: String!
        $phone: String!
        $language: String
        $password: String!
        $password_confirmation: String!
        $job_id: String
        $organization_id: String!
    ) {
        activateUser(
            email_verification_code: $email_verification_code
            first_name: $first_name
            last_name: $last_name
            phone: $phone
            language: $language
            password: $password
            password_confirmation: $password_confirmation
            job_id: $job_id
            organization_id: $organization_id
        ) {
            id
        }
    }
`;

export const GET_USER_BY_CODE = gql`
    query GetUserByCode($email_verification_code: String!, $organization_id: String) {
        get_user_by_code(email_verification_code: $email_verification_code, organization_id: $organization_id) {
            id
            organization {
                id
                name
                role {
                    type
                }
            }
            user {
                id
                name
                first_name
                last_name
                phone
                email
                enabled
            }
            job_id
            user_role_id
        }
    }
`;

export const GET_COMPANY = gql`
    query Company($admin_user_id: String) {
        organizations(admin_user_id: $admin_user_id) {
            data {
                site_url
                name
                type_id
            }
        }
    }
`;

export const UPDATE_ORGANIZATION = gql`
    mutation UpdateCompany($id: String!, $site_url: String) {
        organization(id: $id, site_url: $site_url) {
            id
            name
            type {
                id
                type
                name
            }
            role {
                id
                type
                name
            }
            address {
                city
                street
                number
                plz
                country
                line_1
                line_2
            }
        }
    }
`;
