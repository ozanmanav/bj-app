import React, { FunctionComponent, useEffect } from "react";
import "./NewRelationship.scss";
import { Select, AutocompleteInput, PlusButton, RemoveObjectButton } from "../../ui";
import { FormCaption } from "../formsUI";
import { FormikProps } from "formik";
import { IOrganizationFormState } from "../organizationForm";
import get from "lodash.get";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { getRelationshipRolesOptions } from "../../../utils";

const GET_RELATIONSHIP_TYPES = gql`
    query GetContractTypes {
        entity_types(entity: "CONTRACT") {
            data {
                id
                name
                groups {
                    name
                }
            }
        }
    }
`;

interface INewRelationshipProps extends FormikProps<IOrganizationFormState> {
    openModal: () => void;
    organizationListRefetch: boolean;
    setOrganizationListRefetch: (state: boolean) => void;
    organizationID?: string;
    defaultValue?: string;
    defaultInputValue?: string;
    setDefaultInputValue?: (value: string) => void;
}

export const NewRelationship: FunctionComponent<INewRelationshipProps> = ({
    openModal,
    organizationListRefetch,
    setOrganizationListRefetch,
    defaultValue,
    defaultInputValue,
    organizationID,
    setDefaultInputValue,
    ...formikProps
}) => {
    const { values, errors, touched, handleBlur, handleChange, setFieldTouched, setFieldValue, resetForm } = formikProps;

    const { data } = useQuery(GET_RELATIONSHIP_TYPES);

    const rowValues = get(values, "relationships[0]");
    const rowErrors = get(errors, "relationships[0]");
    const rowTouched = get(touched, "relationships[0]");

    useEffect(() => {
        if (organizationID) {
            setFieldValue("relationships[0].organization_id", organizationID);
            setFieldValue("relationships[0].contacts", []);
            setFieldTouched("relationships[0].organization_id", true);
        }
    }, [organizationID]);

    const roles = get(data, "entity_types.data") || [];

    const clearSelectedKeyContacts = () => {
        setFieldValue("relationships[0].contacts", []);
    };

    const clearSelectedOrganization = () => {
        resetForm();
        setDefaultInputValue && setDefaultInputValue("");
    };

    function onOrganizationChange(item: { id: string; name: string }) {
        if (rowValues && rowValues.organization_id !== item.id) {
            clearSelectedKeyContacts();
        }

        setDefaultInputValue && setDefaultInputValue(item.name);
        setFieldValue("relationships[0].organization_name", item.name);
        setFieldValue("relationships[0].organization_id", item.id);
        setFieldTouched("relationships[0].organization_id", true);
    }

    return (
        <div className="b-new-relationship">
            <FormCaption className="b-new-relationship__caption">
                Where organization
                <PlusButton onClick={openModal} />
            </FormCaption>
            <Select
                options={getRelationshipRolesOptions(roles)}
                placeholder="Select Role"
                className="b-new-relationship__select"
                name="relationships[0].role"
                value={rowValues.role}
                error={rowErrors && rowErrors.role}
                touched={rowTouched && rowTouched.role}
                onChange={handleChange}
                onBlur={handleBlur}
            />
            <AutocompleteInput
                placeholder="Search for an existing organization..."
                className="b-new-relationship__search"
                searchFor="allOrganizations"
                disabled={values.relationships[0].organization_name !== undefined}
                name="relationships[0].organization_id"
                error={rowErrors && rowErrors.organization_id}
                touched={rowTouched && rowTouched.organization_id}
                onBlur={handleBlur}
                isRefetch={organizationListRefetch}
                setRefetch={setOrganizationListRefetch}
                defaultValue={defaultValue}
                defaultInputValue={defaultInputValue}
                onChange={onOrganizationChange}
            />
            <div className="b-new-relationship__selection-box">
                {values.relationships[0].organization_name && (
                    <RemoveObjectButton
                        text={values.relationships[0].organization_name}
                        type="button"
                        onClick={clearSelectedOrganization}
                    />
                )}
            </div>
        </div>
    );
};
