import React, { FunctionComponent } from "react";
import "./SettingsDetailsForm.scss";
import { AddressRow, FormAccordionContent, FormFooterSave, EmptyAddressRow } from "../";
import { AddressSchema } from "../addressRow";
import { FieldArray, Formik, FormikProps } from "formik";
import * as Yup from "yup";
import get from "lodash.get";
import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui/toasts";
import { IAddress } from "../../../config/types";
import { normalizeGraphQLData } from "../../../utils";
import { useCountryByIP } from "../../../hooks";
import { Row } from "../../ui/grid";

export const ADDRESS_DEFAULT_VALUE = {
    line_1: "",
    line_2: "",
    city: "",
    street: "",
    number: "",
    country: "",
    plz: "",
};

const UPDATE_COMPANY_DETAILS = gql`
    mutation UpdateCompanyDetails($id: String!, $invoice_addresses: [OrganizationInvoiceAddresses]) {
        organization(id: $id, invoice_addresses: $invoice_addresses) {
            id
            invoice_addresses {
                city
                street
                number
                country
                plz
                line_1
                line_2
            }
        }
    }
`;

const SettingsDetailsFormSchema = Yup.object().shape({
    invoice_addresses: Yup.array().of(AddressSchema),
});

export interface ISettingsDetailsFormState {
    invoice_addresses: IAddress[];
}

interface ISettingsDetailsFormBaseProps extends FormikProps<ISettingsDetailsFormState> {}

const SettingsDetailsFormBase: FunctionComponent<ISettingsDetailsFormBaseProps> = ({ ...formikProps }) => {
    const rowsName = "invoice_addresses";
    const rowsValues = get(formikProps.values, rowsName);

    return (
        <form className="f-settings-details" onSubmit={formikProps.handleSubmit}>
            <FormAccordionContent className="f-settings-details__content">
                <Row>
                    <FieldArray
                        name={rowsName}
                        render={(arrayHelpers) => (
                            <>
                                {rowsValues.map((row: IAddress, index: number) => (
                                    <AddressRow
                                        {...formikProps}
                                        key={`${rowsName}[${index}]`}
                                        suffix={`${rowsName}[${index}]`}
                                        index={index}
                                        arrayHelpers={arrayHelpers}
                                        arrayLength={rowsValues.length}
                                    />
                                ))}
                                {rowsValues.length === 0 && <EmptyAddressRow {...arrayHelpers} />}
                            </>
                        )}
                    />
                </Row>
            </FormAccordionContent>
            <FormFooterSave />
        </form>
    );
};

interface ISettingsDetailsFormProps {
    organizationId: string;
    invoiceAddresses?: IAddress[];
    callback?: () => void;
}

export const SettingsDetailsForm: FunctionComponent<ISettingsDetailsFormProps> = ({ organizationId, invoiceAddresses, callback }) => {
    const [updateCompanyMutation] = useMutation(UPDATE_COMPANY_DETAILS);

    const country = useCountryByIP();
    const countryName = country ? country.name : "";

    const addressDefaultValue = {
        ...ADDRESS_DEFAULT_VALUE,
        country: countryName,
    };

    const initialValues = {
        invoice_addresses: invoiceAddresses || [addressDefaultValue],
    };

    async function updateCompanyDetails(values: ISettingsDetailsFormState) {
        try {
            const res = await updateCompanyMutation({
                variables: {
                    id: organizationId,
                    invoice_addresses: values.invoice_addresses.map((address) => normalizeGraphQLData(address)),
                },
            });
            const { errors } = res;

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                showSuccessToast("Company details are successfully updated");
                callback && callback();
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    return (
        <Formik
            initialValues={initialValues}
            onSubmit={updateCompanyDetails}
            validationSchema={SettingsDetailsFormSchema}
            enableReinitialize
            render={(props) => <SettingsDetailsFormBase {...props} />}
        />
    );
};
