import React, { FunctionComponent, useEffect, useState } from "react";
import { Formik, FormikProps } from "formik";
import { Input, Select } from "../../ui/inputs";
import "./CompanyContactsForm.scss";
import { EditButton, RemoveIconButton, SaveIconButton } from "../../ui/buttons";
import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui/toasts";
import * as Yup from "yup";
import { phoneCodesByCountry, VALIDATION_ERRORS } from "../../../config";
import { UserRoleCheck } from "../../userRoleCheck";
import { getPhoneInitialValues } from "../../../utils";

const UPDATE_COMPANY = gql`
    mutation UpdateCompanyName($id: String!, $email: String, $phone: String, $site_url: String) {
        updateCompany(id: $id, email: $email, phone: $phone, site_url: $site_url) {
            id
        }
    }
`;

const CompanyContactsFormSchema = Yup.object().shape({
    email: Yup.string().email(VALIDATION_ERRORS.email),
    phone: Yup.number().typeError(VALIDATION_ERRORS.number),
});

interface ICompany {
    email: string;
    phone: string;
    site_url: string;
}

interface ICompanyContactsEditFormState extends ICompany {
    phone_code: string;
}

interface ICompanyContactsEditFormProps extends FormikProps<ICompanyContactsEditFormState> {
    toggleEdit: () => void;
}

const CompanyContactsEditForm: FunctionComponent<ICompanyContactsEditFormProps> = ({
    values,
    initialValues,
    setValues,
    errors,
    touched,
    handleChange,
    handleBlur,
    toggleEdit,
}) => {
    const { email, phone, phone_code, site_url } = values;
    const isChanged = !(email === initialValues.email && phone === initialValues.phone && site_url === initialValues.site_url);

    function hideEditForm() {
        toggleEdit();
        setValues(initialValues);
    }

    return (
        <div className="f-company-contacts__edit flex align-center">
            <div className="f-company-contacts__fields">
                <div className="f-company-contacts__fields-item">
                    <Input
                        className="f-company-contacts__input"
                        placeholder="Site url"
                        name="site_url"
                        value={site_url}
                        onChange={handleChange}
                        marginBottom="none"
                    />
                </div>
                <div className="f-company-contacts__fields-item">
                    <Input
                        className="f-company-contacts__input"
                        placeholder="E-Mail"
                        name="email"
                        value={email}
                        error={errors && errors.email}
                        touched={touched && touched.email}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        marginBottom="none"
                    />
                </div>
                <div className="f-company-contacts__fields-item">
                    <Select
                        options={phoneCodesByCountry}
                        name="phone_code"
                        placeholder="Phone code"
                        value={phone_code}
                        error={errors && errors.phone_code}
                        touched={touched && touched.phone_code}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        marginBottom="none"
                        className="f-company-contacts__select"
                    />
                </div>
                <div className="f-company-contacts__fields-item">
                    <Input
                        className="f-company-contacts__input"
                        placeholder="Phone"
                        name="phone"
                        value={phone}
                        error={errors && errors.phone}
                        touched={touched && touched.phone}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        marginBottom="none"
                    />
                </div>
            </div>
            <div className="f-company-contacts__actions flex">
                {isChanged && (
                    <div className="f-company-contacts__actions-item">
                        <SaveIconButton type="submit" />
                    </div>
                )}
                <div className="f-company-contacts__actions-item">
                    <RemoveIconButton onClick={hideEditForm} />
                </div>
            </div>
        </div>
    );
};

interface ICompanyContactsViewProps {
    email: string;
    phone: string;
    site_url: string;
    toggleEdit: () => void;
}

const CompanyContactsView: FunctionComponent<ICompanyContactsViewProps> = ({ email, site_url, phone, toggleEdit }) => {
    return (
        <div className="f-company-contacts__view flex align-center">
            <div className="f-company-contacts__list">
                <div className="f-company-contacts__item">
                    {site_url ? (
                        <a href={site_url} className="_text-primary _font-bold" target="_blank" rel="noopener noreferrer">
                            {site_url}
                        </a>
                    ) : (
                        <span className="_text-grey _font-bold">Site url</span>
                    )}
                </div>
                <div className="f-company-contacts__item">
                    {email ? (
                        <a href={`mailto:${email}`} className="_text-primary _font-bold">
                            {email}
                        </a>
                    ) : (
                        <span className="_text-grey _font-bold">E-Mail</span>
                    )}
                </div>
                <div className="f-company-contacts__item">
                    {phone ? (
                        <a href={`tel:${phone}`} className="_text-black">
                            {phone}
                        </a>
                    ) : (
                        <span className="_text-grey _font-bold">Phone</span>
                    )}
                </div>
            </div>
            <UserRoleCheck
                availableForRoles={[
                    "owner_administrator",
                    "manufacturer_administrator",
                    "manufacturer_brand_manager",
                    "manufacturer_object_manager",
                    "property_manager_administrator",
                    "property_manager_building_manager",
                    "service_provider_administrator",
                    "service_provider_service_manager",
                ]}
            >
                <div className="f-company-contacts__actions">
                    <div className="f-company-contacts__actions-item">
                        <EditButton onClick={toggleEdit} />
                    </div>
                </div>
            </UserRoleCheck>
        </div>
    );
};

interface ICompanyContactsFormProps extends ICompany {
    companyId: string;
    companyCountry: string;
    refetch: () => void;
}

export const CompanyContactsForm: FunctionComponent<ICompanyContactsFormProps> = ({
    email,
    phone,
    site_url,
    companyCountry,
    companyId,
    refetch,
}) => {
    const initialValue = {
        email: email || "",
        site_url: site_url || "",
        ...getPhoneInitialValues(phone, companyCountry),
    };

    const [isOpen, setIsOpen] = useState(false);

    const [updateCompanyMutation] = useMutation(UPDATE_COMPANY);

    useEffect(() => {
        setIsOpen(false);
    }, [email, phone, site_url]);

    function toggleEdit() {
        setIsOpen(!isOpen);
    }

    async function updateCompany({ email, phone, phone_code, site_url }: ICompanyContactsEditFormState) {
        try {
            const res = await updateCompanyMutation({
                variables: {
                    id: companyId,
                    email,
                    phone: `${phone_code} ${phone}`,
                    site_url,
                },
            });
            const { errors } = res;

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                showSuccessToast("Company contacts are successfully updated");
                refetch();
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    return (
        <Formik
            onSubmit={updateCompany}
            enableReinitialize={true}
            validationSchema={CompanyContactsFormSchema}
            initialValues={initialValue}
            render={(props) => (
                <form className="f-company-contacts flex align-center" onSubmit={props.handleSubmit}>
                    {isOpen ? (
                        <CompanyContactsEditForm toggleEdit={toggleEdit} {...props} />
                    ) : (
                        <CompanyContactsView toggleEdit={toggleEdit} email={email} phone={phone} site_url={site_url} />
                    )}
                </form>
            )}
        />
    );
};
