import React, { FunctionComponent, DragEvent, useState, ChangeEvent } from "react";
import "./ImageInput.scss";
import { Button } from "../../../ui";
import classNames from "classnames";

interface IFileInputProps {
    error?: string;
    addFile: (file: File) => void;
}

const availableImageTypes = ["image/jpeg", "image/png"];

export const ImageInput: FunctionComponent<IFileInputProps> = ({ addFile, error }) => {
    const [isDragOver, setIsDragOver] = useState<boolean>(false);

    function handleFileInput(e: ChangeEvent<HTMLInputElement>) {
        if (e.target.files && e.target.files.length) {
            addFile(e.target.files[0]);
        }
    }

    function handleDragOver(e: DragEvent) {
        e.preventDefault();

        setIsDragOver(true);
    }

    function handleDragLeave() {
        setIsDragOver(false);
    }

    function handleDrop(e: DragEvent) {
        e.preventDefault();

        setIsDragOver(false);

        if (!e.dataTransfer) {
            return;
        }
        if (e.dataTransfer.items && e.dataTransfer.items.length) {
            if (e.dataTransfer.items[0].kind === "file") {
                const file = e.dataTransfer.items[0].getAsFile();

                if (file && availableImageTypes.includes(file.type)) {
                    addFile(file);
                }
            }
        } else {
            // ie 11
            const file = e.dataTransfer.files[0];

            if (file && availableImageTypes.includes(file.type)) {
                addFile(file);
            }
        }
    }

    return (
        <>
            <div
                className={classNames("f-file-input__wrapper", { "_drag-over": isDragOver }, { _error: error })}
                onDragOver={handleDragOver}
                onDragLeave={handleDragLeave}
                onDrop={handleDrop}
            >
                <p className="h3 _text-grey f-file-input__title">Drop image here</p>
                <Button text="Upload image" icon="upload" className="f-image-input__button" primary />
                <input className="f-file-input__input" type="file" accept="image/x-png,image/jpeg" onChange={handleFileInput} />
                {error && <p className="f-file-input__error">{error}</p>}
            </div>
        </>
    );
};
