import React, { FunctionComponent, useEffect } from "react";
import { Formik, FormikProps } from "formik";
import * as Yup from "yup";
import { VALIDATION_ERRORS } from "../../../config";
import ReactCrop from "react-image-crop";
import "react-image-crop/lib/ReactCrop.scss";
import { Button } from "../../ui/buttons";
import { ModalFooter } from "../../ui/modal";
import { ImageInput } from "./imageInput";
import { FormCaption } from "../formsUI";
import "./UploadLogoForm.scss";
import gql from "graphql-tag";
import { useApolloClient } from "@apollo/react-hooks";
import { handleGraphQLErrors, showErrorToast } from "../../ui/toasts";
import { getCroppedImg } from "../../../utils";
import { Loader } from "../../ui/loader";

const UPLOAD_FILE = gql`
    mutation UploadFile($type_id: String!, $name: String!, $file: Upload, $parent_ids: [String]) {
        uploadFile(type_id: $type_id, name: $name, file: $file, parent_ids: $parent_ids) {
            id
            url
        }
    }
`;

const StepOneValidationSchema = Yup.object().shape({
    imageCropped: Yup.boolean().oneOf([true], VALIDATION_ERRORS.required),
});

const uploadLogoFormInitialState = {
    crop: { aspect: 1, x: 0, y: 0, width: 120, height: 120 },
    initialImageUrl: "",
    imageRef: null,
    fileName: "",
    imageCropped: false,
};

interface IUploadLogoFormState {
    crop: ReactCrop.Crop;
    initialImageUrl: string;
    imageRef: HTMLImageElement | null;
    fileName: string;
    imageCropped: boolean;
}

interface IUploadLogoFormProps {
    objectID?: string;
    callback: (logo_id: string) => void;
    loading?: boolean;
    title: string;
}

interface IUploadLogoFormBaseProps extends FormikProps<IUploadLogoFormState> {
    title: string;
    loading?: boolean;
}

const UploadImageFormBase: FunctionComponent<IUploadLogoFormBaseProps> = ({
    values,
    touched,
    errors,
    setFieldTouched,
    setValues,
    setFieldValue,
    handleSubmit,
    title,
    loading,
}) => {
    const imageError = (touched && touched.imageCropped && errors && errors.imageCropped && errors.imageCropped.toString()) || "";

    useEffect(() => {
        if (values.initialImageUrl) {
            setFieldValue("imageCropped", true);
            setFieldTouched("imageCropped", true);
        }
    }, [values.crop, values.initialImageUrl]);

    function getFile(file: File) {
        const objectURL = URL.createObjectURL(file);
        setValues({ ...values, initialImageUrl: objectURL, fileName: file.name });
    }

    function setImageRef(image: HTMLImageElement) {
        setFieldValue("imageRef", image);
    }

    function setCrop(newCrop: ReactCrop.Crop) {
        setFieldValue("crop", newCrop);
    }

    function cancelCrop() {
        setFieldValue("initialImageUrl", null);
        setFieldValue("imageCropped", false);
    }

    return (
        <form className="f-upload-image" onSubmit={handleSubmit}>
            <FormCaption className="f-upload-image__title">{title}</FormCaption>
            {values.initialImageUrl ? (
                <>
                    <div className="f-upload-image__crop-outer">
                        <ReactCrop src={values.initialImageUrl} crop={values.crop} onImageLoaded={setImageRef} onChange={setCrop} />
                    </div>
                    <Button text="Cancel" className="f-upload-image__crop-cancel" onClick={cancelCrop} />
                </>
            ) : (
                <ImageInput addFile={getFile} error={imageError} />
            )}
            <ModalFooter>
                {loading ? (
                    <div className="f-upload-image__loader">
                        <Loader rawLoader />
                    </div>
                ) : (
                    <Button text="Save" primary icon="check" type="submit" />
                )}
            </ModalFooter>
        </form>
    );
};

export const UploadImageForm: FunctionComponent<IUploadLogoFormProps> = ({ callback, title, objectID, loading }) => {
    const apolloClient = useApolloClient();

    async function uploadFile(file: File) {
        try {
            const { data, errors } = await apolloClient.mutate({
                mutation: UPLOAD_FILE,
                variables: {
                    name: file.name,
                    type_id: "bild",
                    parent_ids: objectID && [objectID],
                    file,
                },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                return data.uploadFile.id;
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    function onSubmit(values: IUploadLogoFormState) {
        values.imageRef &&
            getCroppedImg(values.imageRef, values.crop, values.fileName)
                .then((file: any) => uploadFile(file))
                .then((imageId: string) => callback(imageId));
    }

    return (
        <Formik
            onSubmit={onSubmit}
            enableReinitialize={true}
            initialValues={uploadLogoFormInitialState}
            validationSchema={StepOneValidationSchema}
            render={(props) => <UploadImageFormBase {...props} loading={loading} title={title} />}
        />
    );
};
