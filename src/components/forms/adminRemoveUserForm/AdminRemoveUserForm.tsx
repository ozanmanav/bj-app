import React, { FunctionComponent } from "react";
import { AutocompleteInput, RadioInput } from "../../ui/inputs";
import { Button, RemoveObjectButton } from "../../ui/buttons";
import "./AdminRemoveUserForm.scss";
import { ModalFooter } from "../../ui/modal";

export const AdminRemoveUserForm: FunctionComponent = () => {
    return (
        <form className="f-admin-remove-user">
            <h2 className="f-admin-remove-user__title h1">Remove user</h2>
            <div className="f-admin-remove-user__radio-group">
                <RadioInput value="delete" name="user-action" label="Delete all info added by this user" />
                <RadioInput value="reassign" name="user-action" label="Assign ownership to another user" />
            </div>
            {/* TODO: check searchFor attribute */}
            <AutocompleteInput placeholder="Start typing to find user..." searchFor="users" onChange={() => {}} />
            <div className="f-admin-remove-user__users">
                <RemoveObjectButton text="Sam Mendes" />
            </div>
            <ModalFooter>
                <Button text="Remove" primary icon="check" />
            </ModalFooter>
        </form>
    );
};
