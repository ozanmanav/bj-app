import React, { FocusEvent, FunctionComponent, useContext, useEffect, useState } from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import { FAIL_REGISTER_REDIRECT, PASSWORD_MIN_LENGTH, VALIDATION_ERRORS } from "../../../config";
import { Column, Row } from "../../ui/grid";
import { Input } from "../../ui/inputs";
import gql from "graphql-tag";
import { useApolloClient, useMutation, useQuery } from "@apollo/react-hooks";
import { Button } from "../../ui/buttons";
import { getLocationSearchParam } from "../../../utils/locationSearchHelpers";
import { RouteComponentProps } from "react-router";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui/toasts";
import { Loading } from "../../ui/loading";
import { LOGIN } from "../../../views/login/Login";
import { AccountStateContext } from "../../../contexts/accountStateContext";
import get from "lodash.get";

const GET_USER_BY_CODE = gql`
    query GetUserByCode($email_verification_code: String!) {
        get_user_by_code(email_verification_code: $email_verification_code) {
            id
            user {
                email
                first_name
                last_name
            }
        }
    }
`;

const ACTIVATE_USER = gql`
    mutation ActivateUser(
        $email_verification_code: String!
        $password: String!
        $password_confirmation: String!
        $first_name: String!
        $last_name: String!
        $organization_id: String!
    ) {
        activateUser(
            email_verification_code: $email_verification_code
            password: $password
            password_confirmation: $password_confirmation
            first_name: $first_name
            last_name: $last_name
            organization_id: $organization_id
        ) {
            id
        }
    }
`;

const UserPrefilledFormSchema = Yup.object().shape({
    firstName: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    lastName: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    // TODO uncomment when api update
    // jobTitle: Yup.string().trim().required(VALIDATION_ERRORS.required),
    password: Yup.string()
        .trim()
        .min(PASSWORD_MIN_LENGTH, VALIDATION_ERRORS.passwordMinLength)
        .required(VALIDATION_ERRORS.password),
    passwordConfirmation: Yup.string()
        .trim()
        .oneOf([Yup.ref("password")], VALIDATION_ERRORS.passwordConfirmation)
        .required(VALIDATION_ERRORS.password),
});

interface IUserPrefilledForm {
    firstName: string;
    lastName: string;
    jobTitle: string;
    email: string;
    passwordConfirmation: string;
    password: string;
}

const userPrefilledFormState = {
    firstName: "",
    lastName: "",
    jobTitle: "",
    email: "",
    passwordConfirmation: "",
    password: "",
};

export const UserPrefilledForm: FunctionComponent<RouteComponentProps> = ({ location, history }) => {
    const apolloClient = useApolloClient();

    const { saveUser } = useContext(AccountStateContext);

    const [userName, setUserName] = useState(userPrefilledFormState.firstName);

    const [activateUserMutation] = useMutation(ACTIVATE_USER);

    const verificationCode = getLocationSearchParam(location.search, "verify");
    const organizationID = getLocationSearchParam(location.search, "org");

    const { data, loading } = useQuery(GET_USER_BY_CODE, {
        variables: {
            email_verification_code: verificationCode,
        },
    });

    const user = get(data, "get_user_by_code.user");

    if (user) {
        userPrefilledFormState.firstName = user.first_name;
        userPrefilledFormState.lastName = user.last_name;
        userPrefilledFormState.email = user.email;
    }

    useEffect(() => {
        setUserName(userPrefilledFormState.firstName);
    }, []);

    function onFirstNameFieldBlur(e: FocusEvent<HTMLInputElement>) {
        setUserName(e.target.value);
    }

    const activateUser = async (values: IUserPrefilledForm) => {
        try {
            const res = await activateUserMutation({
                variables: {
                    email_verification_code: verificationCode,
                    password: values.password,
                    password_confirmation: values.passwordConfirmation,
                    first_name: values.firstName,
                    last_name: values.lastName,
                    ...(organizationID && { organization_id: organizationID }),
                },
            });
            const { errors } = res;

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                const { data } = await apolloClient.query({
                    query: LOGIN,
                    variables: {
                        email: values.email,
                        password: values.password,
                    },
                });

                saveUser(data.login);

                showSuccessToast("Account is successfully activated.");
                history.push("/app/cockpit");
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    };

    function failRedirect() {
        history.push(FAIL_REGISTER_REDIRECT);
    }

    if (!verificationCode || (!loading && !user)) {
        failRedirect();
    }

    if (loading) {
        return <Loading />;
    }

    return (
        <Formik
            onSubmit={activateUser}
            initialValues={userPrefilledFormState}
            validationSchema={UserPrefilledFormSchema}
            render={({ values, handleSubmit, errors, touched, handleChange, handleBlur }) => (
                <form className="f-prefilled" onSubmit={handleSubmit}>
                    <div className="f-prefilled__body">
                        <h2 className="f-prefilled__title h1">Welcome {userName}</h2>
                        <Row gutter="sm">
                            <Column>
                                <Input
                                    placeholder="First name"
                                    name="firstName"
                                    value={values.firstName}
                                    error={errors && errors.firstName}
                                    touched={touched && touched.firstName}
                                    onChange={handleChange}
                                    onBlur={onFirstNameFieldBlur}
                                />
                            </Column>
                            <Column>
                                <Input
                                    placeholder="Last name"
                                    name="lastName"
                                    value={values.lastName}
                                    error={errors && errors.lastName}
                                    touched={touched && touched.lastName}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                            </Column>
                        </Row>
                        {/*TODO uncomment when api update*/}
                        {/*<Row gutter="sm">*/}
                        {/*    <Column width={12}>*/}
                        {/*        <Input*/}
                        {/*            placeholder="Job position"*/}
                        {/*            name="jobTitle"*/}
                        {/*            value={values.jobTitle}*/}
                        {/*            error={errors && errors.jobTitle}*/}
                        {/*            touched={touched && touched.jobTitle}*/}
                        {/*            onChange={handleChange}*/}
                        {/*            onBlur={handleBlur}*/}
                        {/*        />*/}
                        {/*    </Column>*/}
                        {/*</Row>*/}
                        <Row gutter="sm">
                            <Column width={12}>
                                <Input
                                    placeholder="E-Mail"
                                    name="email"
                                    value={values.email}
                                    disabled={true}
                                    error={errors && errors.email}
                                    touched={touched && touched.email}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                            </Column>
                        </Row>
                        <Row gutter="sm">
                            <Column width={12}>
                                <Input
                                    type="password"
                                    placeholder="Password"
                                    name="password"
                                    value={values.password}
                                    error={errors && errors.password}
                                    touched={touched && touched.password}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                            </Column>
                        </Row>
                        <Row gutter="sm">
                            <Column width={12}>
                                <Input
                                    type="password"
                                    placeholder="Confirm password"
                                    name="passwordConfirmation"
                                    value={values.passwordConfirmation}
                                    error={errors && errors.passwordConfirmation}
                                    touched={touched && touched.passwordConfirmation}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                            </Column>
                        </Row>
                    </div>
                    <div className="f-prefilled__footer">
                        <Button className="f-prefilled__button" text="Confirm" primary icon="check" type="submit" />
                    </div>
                </form>
            )}
        />
    );
};
