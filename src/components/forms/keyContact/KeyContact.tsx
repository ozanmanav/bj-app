import React, { FunctionComponent } from "react";
import "./KeyContact.scss";
import { Icon, CalendarAddButton, RemoveIconButton, EditButton, useModal } from "../../ui";
import { IEmployee } from "../../../config/types";
import { AddUserModal } from "../../modals";
import { getPhoneInitialValues } from "../../../utils";

interface IKeyContactProps {
    employee: IEmployee;
    organizationID: string;
    onRemove: (employee: IEmployee) => void;
    isClientOrganization: boolean;
    showEditButton?: boolean;
}

export const KeyContact: FunctionComponent<IKeyContactProps> = ({
    showEditButton = true,
    employee,
    organizationID,
    onRemove,
    isClientOrganization,
}) => {
    const { hide, open, isOpen } = useModal();

    const removeContact = () => onRemove(employee);

    const { user, job, user_role } = employee;

    const editedUser = {
        id: (user && user.id) || "",
        email: (user && user.email) || "",
        first_name: (user && user.first_name) || "",
        last_name: (user && user.last_name) || "",
        role: (user_role && user_role.id) || "",
        job_title: (job && job.id) || "",
        phone: (user && getPhoneInitialValues(user.phone))?.phone || "",
        phone_code: (user && getPhoneInitialValues(user.phone))?.phone_code || "",
        language: (user && user.language) || "",
    };

    return (
        <div className="b-key-contact">
            <div className="b-key-contact__info">
                <Icon icon="person" className="b-key-contact__info-img" />
                {user && (
                    <p className="b-key-contact__info-name _font-bold">
                        {user.first_name} {user.last_name} {job && `(${job.name})`}
                    </p>
                )}
            </div>
            <div className="b-key-contact__actions flex align-center">
                {showEditButton && <EditButton onClick={open} />}

                <CalendarAddButton />
                <RemoveIconButton type="button" onClick={removeContact} />
            </div>
            <AddUserModal
                organizationID={organizationID}
                hide={hide}
                isOpen={isOpen}
                editedUser={editedUser}
                isClientOrganization={isClientOrganization}
            />
        </div>
    );
};
