import React, { FunctionComponent, useEffect } from "react";
import "./BatchBasicDetailsForm.scss";
import { FormCaption, FormFooterNext, FormAccordionContent } from "../";
import { Input, Loading } from "../../ui";
import { IFormWithFormik, IMetafield, IMetatype } from "../../../config/types";
import { Formik, FormikProps, FieldArray } from "formik";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import * as Yup from "yup";
import { VALIDATION_ERRORS } from "../../../config";
import get from "lodash.get";
import { MAX_BASIC_METAFIELDS_AMOUNT } from "./config";
import { useRelationTypes } from "../../../hooks";
import { getRelationTypesOptions } from "../../../utils";

const BatchBasicDetailsSchema = Yup.object().shape({
    objects: Yup.array().of(
        Yup.object().shape({
            name: Yup.string()
                .trim()
                .required(VALIDATION_ERRORS.required),
        })
    ),
});

interface IBatchBasicDetailsFormSharedProps {
    type: object;
    parentObjectName?: string;
    parentBuildingName?: string;
    numberOfObjects?: number;
    role: string;
}

export interface IBatchBasicDetailsFormState {
    objects: {
        name: string;
        meta: IMetafield[];
    }[];
}

interface BatchBasicDetailsFormBaseProps extends FormikProps<IBatchBasicDetailsFormState>, IBatchBasicDetailsFormSharedProps {}

const OBJECT_TYPE = gql`
    query ObjectType($type: String!) {
        entity_types(id: $type) {
            data {
                name
            }
        }
    }
`;

const OBJECT_TYPE_META = gql`
    query TypeMeta($model_id: String!) {
        type_meta(model_id: $model_id) {
            data {
                name
                type
            }
        }
    }
`;

const BatchBasicDetailsFormBase: FunctionComponent<BatchBasicDetailsFormBaseProps> = ({
    handleSubmit,
    numberOfObjects,
    type,
    parentObjectName,
    parentBuildingName,
    role,
    ...formikProps
}) => {
    const { data, loading } = useQuery(OBJECT_TYPE, {
        variables: {
            type,
        },
    });

    const { data: metaData, loading: metaLoading } = useQuery(OBJECT_TYPE_META, {
        variables: {
            model_id: type,
        },
    });

    const relationTypes = useRelationTypes();
    const relationOptions = getRelationTypesOptions(relationTypes);

    const objectType = get(data, "entity_types.data[0]") || {};
    const meta = get(metaData, "type_meta.data") || [];

    const { name } = objectType;
    const objectsArray = new Array(numberOfObjects).fill({
        name,
        type,
        meta: meta.map((item: IMetatype) => ({ value: "", ...item })).slice(0, MAX_BASIC_METAFIELDS_AMOUNT), // TODO: update if necessary
    });
    const relationship = relationOptions.find((i) => i.value === role);
    const relationshipType = get(relationship, "label") || "";

    if (loading && metaLoading) {
        return (
            <FormAccordionContent>
                <Loading />
            </FormAccordionContent>
        );
    }

    return (
        <form onSubmit={handleSubmit}>
            <FormAccordionContent>
                {!numberOfObjects ? (
                    <h3>You are not adding any objects.</h3>
                ) : (
                    <h3 className="_font-regular f-batch-basic-details__title">
                        You are adding{" "}
                        <span className="_text-primary">
                            {numberOfObjects} {name}
                        </span>{" "}
                        {parentObjectName ? `as ${relationshipType} of` : ""} <span className="_text-primary"> {parentObjectName}</span>
                        {parentBuildingName ? " in " : ""}
                        {parentBuildingName && <span className="_text-primary">{parentBuildingName}.</span>}
                    </h3>
                )}
                {numberOfObjects && !loading && (
                    <FieldArray
                        name="objects"
                        render={({ remove }) =>
                            objectsArray.map((item, index) => (
                                <BatchBasicDetailsFormRow
                                    {...item}
                                    remove={remove}
                                    numberOfObjects={numberOfObjects}
                                    key={index}
                                    index={index}
                                    {...formikProps}
                                />
                            ))
                        }
                    />
                )}
            </FormAccordionContent>
            <FormFooterNext />
        </form>
    );
};

interface IBatchBasicDetailsFormProps extends IFormWithFormik<IBatchBasicDetailsFormState>, IBatchBasicDetailsFormSharedProps {}

export const BatchBasicDetailsForm: FunctionComponent<IBatchBasicDetailsFormProps> = ({ onSubmit, initialValues, ...restProps }) => {
    return (
        <Formik
            onSubmit={onSubmit}
            initialValues={initialValues}
            validationSchema={BatchBasicDetailsSchema}
            render={(props) => <BatchBasicDetailsFormBase {...props} {...restProps} />}
        />
    );
};

interface IBatchBasicDetailsFormRowProps extends FormikProps<IBatchBasicDetailsFormState> {
    name: string;
    type: object;
    meta: IMetafield[];
    index: number;
    remove: Function;
}

const BatchBasicDetailsFormRow: FunctionComponent<IBatchBasicDetailsFormRowProps> = ({
    name,
    type,
    index,
    meta,
    remove,
    ...formikProps
}) => {
    const { values, errors, touched, setFieldValue, handleChange, handleBlur } = formikProps;

    // setting the initial values for row and reset errors and touched
    useEffect(() => {
        setFieldValue(`objects[${index}]`, {
            name: "",
            type,
            meta,
        });

        return () => {
            remove(index);
        };
    }, [name]);

    if (!values.objects[index]) {
        return null;
    }

    const namePath = `objects[${index}].name`;

    const nameError = get(errors, namePath);
    const nameTouched = get(touched, namePath);
    const nameValue = get(values, namePath);

    return (
        <div className="f-batch-basic-details__row flex align-end">
            <p className="f-batch-basic-details__row-index flex align-center justify-center">{index + 1}</p>
            <label className="f-batch-basic-details__row-label">
                <FormCaption className="f-batch-basic-details__row-caption">
                    <span className="_text-capitalize">{name}</span> name
                </FormCaption>
                <Input
                    marginBottom="none"
                    name={`objects[${index}].name`}
                    value={nameValue}
                    error={nameError}
                    touched={nameTouched}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />
            </label>
            {meta.map((item, metaIndex) => (
                <BatchBasicDetailsFormRowField item={item} metaIndex={metaIndex} index={index} {...formikProps} key={item.name} />
            ))}
        </div>
    );
};

interface IBatchBasicDetailsFormRowFieldProps extends FormikProps<IBatchBasicDetailsFormState> {
    metaIndex: number;
    index: number;
    item: IMetafield;
}

const BatchBasicDetailsFormRowField: FunctionComponent<IBatchBasicDetailsFormRowFieldProps> = ({
    item,
    values,
    errors,
    touched,
    metaIndex,
    index,
    handleChange,
    handleBlur,
}) => {
    const fieldPath = `objects[${index}].meta[${metaIndex}].value`;

    const fieldError = get(errors, fieldPath);
    const fieldTouched = get(touched, fieldPath);
    const value = get(values, fieldPath);

    return (
        <label className="f-batch-basic-details__row-label">
            <FormCaption>{item.name}</FormCaption>
            <Input
                marginBottom="none"
                name={`objects[${index}].meta[${metaIndex}].value`}
                value={value}
                error={fieldError}
                touched={fieldTouched}
                onChange={handleChange}
                onBlur={handleBlur}
            />
        </label>
    );
};
