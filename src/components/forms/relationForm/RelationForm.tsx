import React, { FormEvent, FunctionComponent, useEffect, useState } from "react";
import "./RealationForm.scss";
import { FormAccordionContent, FormFooterWithSave, AddObjectRelation, IFormFooterWithSaveProps } from "../";
import { ObjectRelationTable } from "../../tables";
import { IRelation } from "../../../config/types";

export type TRelationFormState = IRelation[];

interface IRelationFormProps extends IFormFooterWithSaveProps {
    onSubmit: (state: TRelationFormState) => void;
    goPrevAccordionItem?: () => void;
    relations: TRelationFormState;
    objectStreet?: string;
    objectCity?: string;
}

export const RELATION_FORM_DEFAULT_STATE = [];

export const RelationForm: FunctionComponent<IRelationFormProps> = ({
    onSubmit,
    goPrevAccordionItem,
    relations,
    objectStreet,
    objectCity,
}) => {
    const [state, setState] = useState<TRelationFormState>(RELATION_FORM_DEFAULT_STATE);

    useEffect(() => {
        setState(relations);
    }, [relations]);

    function onSelfSubmit(e: FormEvent) {
        e.preventDefault();

        onSubmit(state);
    }

    function addRelation(relation: IRelation) {
        setState([...state, relation]);
    }

    function removeRelation(id: string) {
        setState((prevState) => prevState.filter((relation) => relation.id !== id));
    }

    return (
        <form onSubmit={onSelfSubmit}>
            <FormAccordionContent>
                <ObjectRelationTable removeRelation={removeRelation} items={state} title="Related Objects" />
                <AddObjectRelation addRelation={addRelation} objectStreet={objectStreet} objectCity={objectCity} />
            </FormAccordionContent>
            <FormFooterWithSave goPrevAccordionItem={goPrevAccordionItem} variant="no-formik" />
        </form>
    );
};
