import React, { FunctionComponent } from "react";
import "./BuildingSearch.scss";
import { Input } from "../../ui";
import { MapsBlockWithSearch, IMapsBlockWithSearchState } from "../../maps";
import { FormAccordionContent, FormFooterSave } from "../formsUI";
import { Formik, FormikProps } from "formik";
import * as Yup from "yup";
import { IFormWithFormik, VALIDATION_ERRORS } from "../../../config";
import { IInfoRowParams } from "../dynamicInfoRows";

const BuildingSearchSchema = Yup.object().shape({
    name: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    address: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
});

export interface IBuildingSearchState extends IMapsBlockWithSearchState {
    name: string;
    addressDetails: IInfoRowParams[];
}

interface IBuildingSearchBaseProps extends FormikProps<IBuildingSearchState> {}

const BuildingSearchBase: FunctionComponent<IBuildingSearchBaseProps> = ({
    values,
    errors,
    touched,
    handleSubmit,
    handleChange,
    handleBlur,
    ...formikProps
}) => {
    function onAddressChange(address: string) {
        if (!(touched && touched.name)) {
            formikProps.setFieldValue("name", address);
        }
    }

    return (
        <form onSubmit={handleSubmit}>
            <MapsBlockWithSearch
                values={values}
                errors={errors}
                touched={touched}
                handleChange={handleChange}
                handleBlur={handleBlur}
                handleSubmit={handleSubmit}
                addressChangeCallback={onAddressChange}
                {...formikProps}
            />
            <FormAccordionContent>
                <Input
                    placeholder="Select Building name"
                    name="name"
                    marginBottom="none"
                    value={values.name}
                    error={errors && errors.name}
                    touched={touched && touched.name}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />
            </FormAccordionContent>
            <FormFooterSave />
        </form>
    );
};

interface IBuildingSearchProps extends IFormWithFormik<IBuildingSearchState> {}

export const BuildingSearch: FunctionComponent<IBuildingSearchProps> = ({ initialValues, onSubmit }) => {
    return (
        <Formik
            initialValues={initialValues}
            enableReinitialize={true}
            onSubmit={onSubmit}
            validationSchema={BuildingSearchSchema}
            component={BuildingSearchBase}
        />
    );
};
