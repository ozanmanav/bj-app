import get from "lodash.get";

export function normalizeLogs(old_value: any, new_value: any): any {
    if (typeof old_value !== "object" && typeof new_value !== "object") {
        return {
            old_value,
            new_value,
        };
    } else {
        return Object.keys(new_value).map((i) => normalizeLogs(get(old_value, `${i}`), get(new_value, `${i}`)));
    }
}
