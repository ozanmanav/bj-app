import React, { FunctionComponent } from "react";
import { IUser } from "../../forms/addUserForm/definitions";
import ObjectHistoryTableDetail from "./ObjectHistoryTableDetail";
import { format } from "date-fns";
import get from "lodash.get";

interface ObjectHistoryTableRowPros {
    id: string;
    model: string;
    model_id: string;
    new_fields: string;
    action: "create" | "update" | "delete" | "restore";
    old_value: string;
    old_fields: string;
    user: IUser;
    user_id: string;
    created_at: string;
    updated_at: string;
    variant: string;
}

const ObjectHistoryTableRow: FunctionComponent<ObjectHistoryTableRowPros> = ({
    action,
    created_at,
    updated_at,
    user,
    new_fields,
    old_value,
    old_fields,
    variant,
    id,
    model,
}) => {
    const actionType = {
        create: "created",
        update: "changed",
        delete: "deleted",
        restore: "restored",
    };

    const userDetails = {
        name: `${get(user, "first_name") || ""} ${get(user, "last_name") || ""}`,
        email: get(user, "email") || "",
    };

    let newValues: object = JSON.parse(new_fields);
    let oldValues: object = JSON.parse(old_fields);
    let logAction = actionType[action];
    let valuesKeys: string[];

    // god save me...
    const isDelete = () => Array.isArray(newValues) && !Array.isArray(oldValues);
    const isCreate = () => !Array.isArray(newValues) && Array.isArray(oldValues);
    const isUpdate = () => !Array.isArray(newValues) && !Array.isArray(oldValues);

    if (isDelete()) {
        valuesKeys = Object.keys(oldValues);
        logAction = "deleted";
    } else if (isCreate()) {
        valuesKeys = Object.keys(newValues);
        logAction = "created";
    } else if (isUpdate()) {
        valuesKeys = Object.keys(oldValues);
    } else {
        valuesKeys = Object.keys(newValues);
    }

    const details = (
        <tr className="b-table__row" key={id}>
            <td className="b-table__cell">
                <span className="b-table__details">
                    <h5 className="b-object-history-table__change-type _text-uppercaseFirst">{`${model.slice(0, -1)} ${
                        actionType[action]
                    }`}</h5>
                    <p className="h6 _text-grey">{`by ${userDetails.name} (${userDetails.email})`}</p>

                    {valuesKeys.map((key) => (
                        <ObjectHistoryTableDetail
                            oldValue={(get(oldValues, key) || "N/A").toString()}
                            newValue={(get(newValues, key) || "N/A").toString()}
                            type={key}
                            action={logAction}
                            key={key}
                        />
                    ))}
                    <p className="h6 _text-grey _text_margin_up">
                        on{" "}
                        {action === "update"
                            ? format(new Date(updated_at), "d MMM yyyy, hh:mm")
                            : format(new Date(created_at), "d MMM yyyy, hh:mm")}
                    </p>
                </span>
            </td>
        </tr>
    );

    return valuesKeys.length > 0 ? details : null;
};

export default ObjectHistoryTableRow;
