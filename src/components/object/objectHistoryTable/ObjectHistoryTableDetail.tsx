import React, { FunctionComponent } from "react";

interface ObjectHistoryTableDetailProps {
    oldValue: object;
    newValue: object;
    type: string;
    action: string;
}

const ObjectHistoryTableDetail: FunctionComponent<ObjectHistoryTableDetailProps> = ({ oldValue, newValue, type, action }) => {
    let details: any;

    switch (action) {
        case "created":
            details = (
                <>
                    <span className="_text-black _font-bold"> {newValue} </span>
                    <span>{action}</span>
                </>
            );
            break;

        case "deleted":
            details = (
                <>
                    <span className="_text-black _font-bold"> {oldValue} </span>
                    <span>{action}</span>
                </>
            );
            break;

        default:
            details = (
                <>
                    <span>{action}</span>
                    <br />
                    <span>{oldValue}</span>
                    <span className="_text-black _font-bold"> to </span>
                    <span>{newValue} </span>
                </>
            );
            break;
    }

    return (
        <p className="b-object-history-table__change _text-grey" key={type}>
            <span className="_text-black _font-bold _text-capitalize">{type} </span>
            {details}
        </p>
    );
};

export default ObjectHistoryTableDetail;
