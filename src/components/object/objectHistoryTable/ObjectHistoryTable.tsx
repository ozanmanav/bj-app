import React, { FunctionComponent } from "react";
import "./ObjectHistoryTable.scss";
import gql from "graphql-tag";
import get from "lodash.get";
import { useQuery } from "@apollo/react-hooks";
import { Loader } from "../../ui/loader";
import Pagination from "../../ui/pagination";
import { IObjectHistoryProps } from "../../../views/app/object/objectHistory/ObjectHistory";
import ObjectHistoryTableRow from "./ObjectHistoryTableRow";

const GET_LOG = gql`
    query GetLog($model_id: String!, $model: LogsModelEnum!, $limit: Int!, $page: Int!) {
        logs(model: $model, model_id: $model_id, limit: $limit, page: $page) {
            data {
                id
                model
                model_id
                action
                new_fields
                old_value
                old_fields
                user {
                    id
                    first_name
                    last_name
                    email
                }
                user_id
                created_at
                updated_at
            }
            has_more_pages
            current_page
            last_page
        }
    }
`;

export const ObjectHistoryTable: FunctionComponent<IObjectHistoryProps> = ({ variant, variantID }) => {
    const limit: number = 10;

    const { data, loading, refetch, error } = useQuery(GET_LOG, {
        variables: {
            model: variant,
            model_id: variantID,
            limit,
            page: 1,
        },
        fetchPolicy: "cache-and-network",
    });

    const logs = get(data, "logs.data") || [];
    const lastPaginationPage = (get(data, "logs.last_page") as number) || 1;
    const showPagination = lastPaginationPage > 1;

    const handlePageClick = async (data: any) => {
        await refetch({ page: data.selected + 1, model: variant, model_id: variantID, limit });
    };

    const loader = loading ? (
        <div className={"flex justify-center"}>
            <Loader withText text="change logs" />
        </div>
    ) : null;

    const pagination = (
        <Pagination marginPagesDisplayed={2} pageRangeDisplayed={5} onPageChange={handlePageClick} pageCount={lastPaginationPage} />
    );

    const table = (
        <div className="b-table__wrapper b-object-history-table__wrapper">
            <table className="b-table">
                <thead className="b-table__head">
                    <tr className="b-table__row _head">
                        <th className="b-table__cell">Changes</th>
                    </tr>
                </thead>
                <tbody className="b-table__body">
                    {logs.map((item: any) => (
                        <ObjectHistoryTableRow key={item.id} {...item} variant={variant} />
                    ))}
                </tbody>
            </table>
        </div>
    );

    const toReturn = (
        <>
            <h3>Change log</h3>
            {!loader && !error && logs.length > 0 ? table : loader}
            {logs.length > 0 && showPagination && pagination}
            {error && <p className={"flex justify-center"}>Sorry. We are currently experiencing technical difficulties.</p>}
            {!error && logs.length === 0 && <p>No logs available.</p>}
        </>
    );

    return toReturn;
};
