import { BUILDING_DETAILS_NAME_ORDER, DATES_FIELDS, IObjectDetailsItem } from "./config";
import get from "lodash.get";
import { formatDate } from "../../ui/datepicker/utils";
import { TPageType } from "../../../config/types/Page";

export function normalizeDetailsUsers(details: any[] = []) {
    return details.map(({ __typename, ...item }) => ({
        ...item,
        author_data: getName(item.author_data),
        updater_data: getName(item.updater_data),
    }));
}

export function normalizeAddressDetailName(name: string) {
    switch (name) {
        case "line_1":
            return "Address Line 1";
        case "line_2":
            return "Address Line 2";
        case "plz":
            return "PLZ";
        default:
            return name;
    }
}

function getName(userData: { first_name: string; last_name: string }) {
    if (!userData) {
        return "";
    }

    return `${userData.first_name || ""} ${userData.last_name || ""}`.trim();
}

export function detailsSortFunction(a: { name: string }, b: { name: string }): number {
    const aIndex = getOrderIndex(a);
    const bIndex = getOrderIndex(b);

    return aIndex - bIndex;
}

function getOrderIndex(detail: { name: string }) {
    if (!BUILDING_DETAILS_NAME_ORDER.includes(detail.name)) {
        return BUILDING_DETAILS_NAME_ORDER.length;
    }

    return BUILDING_DETAILS_NAME_ORDER.findIndex((type) => detail.name === type);
}

export function getObjectDetailsFieldValue(item: IObjectDetailsItem, name: string) {
    const fieldValue = get(item, name);

    if (DATES_FIELDS.includes(name) || (name === "value" && item.type === "date")) {
        return fieldValue ? formatDate(new Date(fieldValue)) : fieldValue;
    }

    if (typeof fieldValue === "boolean") {
        return fieldValue ? "true" : "false";
    }

    return fieldValue;
}

export function getOrganizationsDetailsFieldValue(item: any, name: string) {
    const fieldValue = get(item, name);

    if (DATES_FIELDS.includes(name)) {
        return fieldValue ? formatDate(new Date(fieldValue)) : fieldValue;
    }

    if (typeof fieldValue === "boolean") {
        return fieldValue ? "true" : "false";
    }

    return fieldValue;
}

export function getTypePath(type: TPageType) {
    switch (type) {
        case "object":
            return "objects.data[0].type";
        case "organization":
            return "organizations.data[0].type";
        default:
            return "";
    }
}

export function getNamePath(type: TPageType) {
    switch (type) {
        case "object":
            return "objects.data[0].name";
        case "organization":
            return "organizations.data[0].name";
        default:
            return "";
    }
}
export function getEmailPath(type: TPageType) {
    switch (type) {
        case "organization":
            return "organizations.data[0].email";
        default:
            return "";
    }
}
export function getPhonePath(type: TPageType) {
    switch (type) {
        case "organization":
            return "organizations.data[0].phone";
        default:
            return "";
    }
}

export function getDetailsPath(type: TPageType) {
    switch (type) {
        case "object":
            return "details.data";
        case "organization":
            return "details.data";
        default:
            return "";
    }
}

export function getAddressPath(type: TPageType) {
    switch (type) {
        case "object":
            return "objects.data[0].address";
        case "organization":
            return "organizations.data[0].address";
        default:
            return "";
    }
}

export function getRolePath(type: TPageType) {
    switch (type) {
        case "organization":
            return "organizations.data[0].role";
        default:
            return "";
    }
}

export function getCategoryPath(type: TPageType) {
    switch (type) {
        case "organization":
            return "organizations.data[0].category";
        default:
            return "";
    }
}

export function getMetaPath(type: TPageType) {
    switch (type) {
        case "organization":
            return "organizations.data[0].meta";
        case "object":
            return "objects.data[0].meta";
        default:
            return "";
    }
}

export function getCreatedAtPath(type: TPageType) {
    switch (type) {
        case "organization":
            return "organizations.data[0].created_at";
        case "object":
            return "objects.data[0].created_at";
        default:
            return "";
    }
}
