import React, { FunctionComponent, useState, ChangeEvent, useEffect } from "react";
import classnames from "classnames";
import { UserRoleCheck } from "../../userRoleCheck";
import { CheckboxSwitch } from "../../ui/inputs/checkbox";
import { EditButton, CalendarAddButton, RemoveIconButton, SaveIconButton } from "../../ui/buttons";
import { EDIT_AVAILABLE_FOR_ROLES } from "./config";
import { Input, ICheckboxDropdownItemState, Select, ISelectOption } from "../../ui";
import { normalizeAddressDetailName, getOrganizationsDetailsFieldValue } from "./utils";
import { phoneCodesByCountry, URL_REGEX } from "../../../config";
import { getPhoneInitialValues } from "../../../utils";
import get from "lodash.get";
import { getSource } from "../../forms/dynamicInfoRows/utils";
import { Link } from "react-router-dom";
import { formatDate } from "../../ui/datepicker/utils";

interface IObjectDetailsCustomRowProps {
    value: string;
    selectValue?: string;
    name: string;
    meta?: object;
    inlineInputType?: "input" | "select" | "phone";
    inlineSelectOptions?: ISelectOption[];
    inlineSelectPlaceholder?: string;
    columnsToShow: ICheckboxDropdownItemState[];
    onSubmitSave: (updatedFieldValue: string, updatedFieldName: string) => void;
    isEditAvailable?: boolean;
    variant?: "object" | "organization";
    variantID?: string;
    setInlineEditModeFromChild?: (objectType: string, initRun: boolean) => void;
}

export const ObjectDetailsCustomRow: FunctionComponent<IObjectDetailsCustomRowProps> = ({
    value,
    selectValue = "",
    name,
    inlineInputType = "input",
    inlineSelectOptions = [],
    inlineSelectPlaceholder = "",
    isEditAvailable,
    onSubmitSave,
    columnsToShow,
    meta,
    variant,
    variantID,
    setInlineEditModeFromChild,
}) => {
    const [isChanged, setIsChanged] = useState<boolean>(false);
    const [inlineEditMode, setInlineEditMode] = useState<boolean>(false);
    const [updateFieldValue, setUpdatedFieldValue] = useState<string>(selectValue);

    const initialPhoneValues = getPhoneInitialValues(value);
    const [splittedPhone, setSplittedPhone] = useState(initialPhoneValues);

    useEffect(() => {
        if (setInlineEditModeFromChild) setInlineEditModeFromChild(name, true);
    }, []);

    useEffect(() => {
        if (inlineInputType === "phone") {
            setUpdatedFieldValue(`${splittedPhone.phone_code} ${splittedPhone.phone}`);
        }
    }, [splittedPhone]);

    const toggleInlineEditMode = () => {
        setInlineEditMode((prevState) => !prevState);
        if (setInlineEditModeFromChild) setInlineEditModeFromChild(name, false);
    };

    const onChangeEditInput = (e: ChangeEvent<HTMLInputElement>) => {
        const updatedValue = e.currentTarget.value;

        if (updatedValue !== value) {
            setUpdatedFieldValue(updatedValue);
            setIsChanged(true);
        } else {
            setIsChanged(false);
        }
    };

    const onChangeEditSelect = (e: React.ChangeEvent<HTMLSelectElement>) => {
        const updatedValue = e.currentTarget.value;

        if (updatedValue !== value) {
            setUpdatedFieldValue(updatedValue);
            setIsChanged(true);
        } else {
            setIsChanged(false);
        }
    };

    const onClickSaveButton = () => {
        toggleInlineEditMode();
        onSubmitSave(updateFieldValue, name);
    };

    const onChangeEditPhoneCode = (e: React.ChangeEvent<HTMLSelectElement>) => {
        const updatedPhoneCode = e.currentTarget.value;

        if (updatedPhoneCode !== initialPhoneValues.phone_code) {
            setSplittedPhone({
                ...splittedPhone,
                phone_code: updatedPhoneCode,
            });
            setIsChanged(true);
        } else {
            setSplittedPhone({
                ...splittedPhone,
                phone_code: initialPhoneValues.phone_code,
            });
            setIsChanged(false);
        }
    };

    const onChangeEditPhone = (e: React.ChangeEvent<HTMLInputElement>) => {
        const updatedPhone = e.currentTarget.value;

        if (updatedPhone !== initialPhoneValues.phone) {
            setSplittedPhone({
                ...splittedPhone,
                phone: updatedPhone,
            });
            setIsChanged(true);
        } else {
            setSplittedPhone({
                ...splittedPhone,
                phone: initialPhoneValues.phone,
            });
            setIsChanged(false);
        }
    };

    const rowClassname = classnames(["b-table__row _medium"]);

    return (
        <tr className={rowClassname}>
            <td
                className="b-table__cell _font-bold"
                style={(inlineInputType === "select" || inlineInputType === "phone") && inlineEditMode ? { height: "80px" } : {}}
            >
                <div className="flex align-center">
                    <span className="b-object-details-table__detail-label _text-capitalize" title={name}>
                        {normalizeAddressDetailName(name)}
                    </span>
                </div>
            </td>

            <td
                className="b-table__cell"
                style={(inlineInputType === "select" || inlineInputType === "phone") && inlineEditMode ? { height: "80px" } : {}}
            >
                <UserRoleCheck
                    availableForRoles={[
                        "owner_administrator",
                        "owner_portfolio_manager",
                        "manufacturer_administrator",
                        "manufacturer_brand_manager",
                        "manufacturer_object_manager",
                        "property_manager_administrator",
                        "property_manager_building_manager",
                        "service_provider_administrator",
                        "service_provider_service_manager",
                    ]}
                >
                    <div className="b-object-details-table__actions flex align-center">
                        <CheckboxSwitch />
                        <UserRoleCheck availableForRoles={EDIT_AVAILABLE_FOR_ROLES}>
                            {inlineEditMode ? (
                                isChanged ? (
                                    <SaveIconButton onClick={onClickSaveButton} />
                                ) : (
                                    <RemoveIconButton onClick={toggleInlineEditMode} />
                                )
                            ) : (
                                <EditButton onClick={toggleInlineEditMode} disabled={!isEditAvailable} />
                            )}
                            <Link to={`/app/event/add?${variant === "object" ? "obj=" : "org="}${variantID}`}>
                                <CalendarAddButton />
                            </Link>
                        </UserRoleCheck>
                    </div>
                </UserRoleCheck>
            </td>

            <td className="b-table__cell" key={name}>
                {inlineEditMode && inlineInputType === "input" && (
                    <Input className="b-table__cell__inline-input" defaultValue={value} onChange={onChangeEditInput} marginBottom="none" />
                )}

                {inlineEditMode && inlineInputType === "select" && (
                    <Select
                        options={inlineSelectOptions}
                        onChange={onChangeEditSelect}
                        value={updateFieldValue}
                        placeholder={inlineSelectPlaceholder}
                    />
                )}

                {inlineEditMode && inlineInputType === "phone" && (
                    <div className="b-table__cell__inline-phone">
                        <div className="b-table__cell__inline-phone-code">
                            <Select
                                options={phoneCodesByCountry}
                                placeholder="Phone code"
                                value={splittedPhone.phone_code}
                                onChange={onChangeEditPhoneCode}
                            />
                        </div>
                        <div className="b-table__cell__inline-phone-number">
                            <Input type="number" placeholder="Mobile number" value={splittedPhone.phone} onChange={onChangeEditPhone} />
                        </div>
                    </div>
                )}
                {!inlineEditMode && name === "Phone" && <>{value ? value.split(" ").join("") : ""}</>}
                {!inlineEditMode && name !== "Phone" && <>{value}</>}
            </td>
            {columnsToShow
                .filter((i) => i.name !== "value")
                .map(({ name }) => {
                    if (name === "updated_at") {
                        const lastModification = get(meta, "last_modification");

                        if (lastModification) {
                            const modifiedDate = formatDate(new Date(get(lastModification, "modified_date")));

                            return (
                                <td className="b-table__cell" key={name}>
                                    {modifiedDate}
                                </td>
                            );
                        }
                    }

                    if (name === "updater_data") {
                        const lastModification = get(meta, "last_modification");

                        if (lastModification) {
                            const modifier = get(lastModification, "modifier");
                            return (
                                <td className="b-table__cell" key={name}>
                                    {modifier.name}
                                </td>
                            );
                        }
                    }

                    if (name === "author_data") {
                        const author = get(meta, "author");

                        if (author) {
                            const authorName = get(author, "name");

                            return (
                                <td className="b-table__cell" key={name}>
                                    {authorName}
                                </td>
                            );
                        }
                    }

                    const value = getOrganizationsDetailsFieldValue(meta, name);

                    if (name === "source") {
                        const source = getSource(value);

                        if (typeof source === "object" && source) {
                            return (
                                <td className="b-table__cell" key={name}>
                                    <a href={source.href} target="_blank" rel="noopener noreferrer">
                                        {source.name}
                                    </a>
                                </td>
                            );
                        }
                    }

                    return (
                        <td className="b-table__cell" key={name}>
                            {URL_REGEX.test(value) || name === "source_url" ? (
                                <a href={value} target="_blank" rel="noopener noreferrer">
                                    {value}
                                </a>
                            ) : (
                                value
                            )}
                        </td>
                    );
                })}
        </tr>
    );
};
