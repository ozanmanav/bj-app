import React, { FunctionComponent, useState } from "react";
import "./HighlightFieldButton.scss";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../../ui/toasts";
import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";

import star from "./star.svg";
import starActive from "./star-active.svg";
import starDisabled from "./star-disabled.svg";
import { GET_CONTENT_HEADER_OBJECT } from "../../../contentHeaders/objectContentHeader";
import { TPageType } from "../../../../config/types/Page";
import { ORGANIZATION_HEADER_QUERY } from "../../../contentHeaders/organizationContentHeader/config";

const ICONS = {
    normal: star,
    active: starActive,
    disabled: starDisabled,
};

const UPDATE_HIGHLIGHTED_FIELD = gql`
    mutation UpdateHighlightedField($id: String!, $highlighted: Boolean!, $model: UpdateDetailsModelEnum!, $model_id: String!) {
        updateDetail(id: $id, highlighted: $highlighted, model: $model, model_id: $model_id) {
            id
            name
            highlighted
        }
    }
`;

interface IHighlightFieldButtonProps {
    isHighlighted?: boolean;
    canHighlightMore?: boolean;
    objectID: string;
    type: string;
    refetch: () => void;
    variant: TPageType;
    detailID: string;
}

export const HighlightFieldButton: FunctionComponent<IHighlightFieldButtonProps> = ({
    isHighlighted,
    objectID,
    type,
    canHighlightMore,
    refetch,
    variant,
    detailID,
}) => {
    const [loading, setLoading] = useState<boolean>(false);

    const [updateField] = useMutation(UPDATE_HIGHLIGHTED_FIELD);

    const isDisabled = loading || (!isHighlighted && !canHighlightMore);
    const title = isDisabled ? "You can highlight only 2 items" : "";

    async function onToggle() {
        setLoading(true);

        try {
            const { errors } = await updateField({
                variables: {
                    id: detailID,
                    highlighted: !isHighlighted,
                    model: variant === "object" ? "OBJECT" : "ORGANIZATION",
                    model_id: objectID,
                },
                refetchQueries: [
                    {
                        query: variant === "object" ? GET_CONTENT_HEADER_OBJECT : ORGANIZATION_HEADER_QUERY,
                        variables: {
                            id: objectID,
                        },
                    },
                ],
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                showSuccessToast(isHighlighted ? "Field is no longer highlighted." : "Field is highlighted.");
                refetch();
            }
        } catch (e) {
            showErrorToast(e.message);
        }

        setLoading(false);
    }

    const iconSrc = getIconSrc(isDisabled, isHighlighted);

    return (
        <button onClick={onToggle} disabled={isDisabled} title={title} className="b-highlight-field-button">
            <img src={iconSrc} alt="icon" />
        </button>
    );
};

function getIconSrc(disabled: boolean, highlighted?: boolean) {
    if (disabled) {
        return ICONS.disabled;
    }

    if (highlighted) {
        return ICONS.active;
    }

    return ICONS.normal;
}
