import gql from "graphql-tag";

export const GET_VARIANT_INFOS = {
    object: gql`
        query GetObjectVariantInfo($id: String!) {
            objects(id: $id) {
                data {
                    id
                    name
                    type {
                        id
                        type
                        name
                    }
                    created_at
                    meta {
                        author {
                            id
                            name
                        }
                        source
                        source_url
                        source_date
                        verification
                        verification_by
                        context
                        last_modification {
                            modified_date
                            modifier {
                                id
                                name
                            }
                        }
                    }
                }
            }
        }
    `,
    organization: gql`
        query GetOrganizationVariantInfo($id: String!) {
            organizations(id: $id) {
                data {
                    id
                    name
                    email
                    phone
                    category
                    type {
                        id
                        type
                        name
                    }
                    created_at
                    meta {
                        author {
                            id
                            name
                        }
                        source
                        source_url
                        source_date
                        verification
                        verification_by
                        context
                        last_modification {
                            modified_date
                            modifier {
                                id
                                name
                            }
                        }
                    }
                    role {
                        id
                        type
                        name
                    }
                    address {
                        city
                        street
                        number
                        plz
                        country
                        line_1
                        line_2
                    }
                }
            }
        }
    `,
};

export const UPDATE_VARIANT = {
    object: gql`
        mutation UpdateObject($id: String!, $type_id: String!) {
            updateObject(id: $id, type_id: $type_id) {
                id
                name
                type {
                    id
                }
            }
        }
    `,
    organization: gql`
        mutation UpdateOrganization(
            $id: String!
            $name: String
            $address: OrganizationAddressParameters
            $type_id: String
            $role_id: String
            $email: String
            $phone: String
        ) {
            organization(id: $id, name: $name, address: $address, type_id: $type_id, role_id: $role_id, phone: $phone, email: $email) {
                id
                name
                phone
                email
                type {
                    id
                    type
                    name
                }
                role {
                    id
                    type
                    name
                }
                address {
                    city
                    street
                    number
                    plz
                    country
                    line_1
                    line_2
                }
            }
        }
    `,
};

export const GET_DETAILS = gql`
    query GetDetails($model: DetailsModelEnum!, $model_id: String!) {
        details(model: $model, model_id: $model_id) {
            data {
                id
                type
                name
                value
                context
                events {
                    id
                    timeline
                }
                meta {
                    author_id
                    author {
                        id
                        name
                    }
                    verification
                    verification_by
                    source
                    source_url
                    source_date
                    verifications {
                        verifier_id
                        verifier {
                            name
                        }
                        verified_date
                    }
                    modifications {
                        modifier_id
                        modifier {
                            name
                        }
                        modified_date
                    }
                }
                created_at
                updated_at
                highlighted
            }
        }
    }
`;

export const UPDATE_DETAIL = gql`
    mutation UpdateDetail(
        $id: String!
        $model: UpdateDetailsModelEnum!
        $model_id: String!
        $type: String!
        $name: String!
        $value: String
        $context: String
        $highlighted: Boolean
        $source: String
        $source_date: String
        $source_url: String
        $verification: Boolean
        $verification_by: String
    ) {
        updateDetail(
            id: $id
            model: $model
            model_id: $model_id
            type: $type
            name: $name
            value: $value
            context: $context
            highlighted: $highlighted
            source: $source
            source_date: $source_date
            source_url: $source_url
            verification: $verification
            verification_by: $verification_by
        ) {
            id
            name
            value
            is_primary
            is_organization_field
            context
            highlighted
            meta {
                author_id
                author {
                    id
                    name
                }
                verification
                verification_by
                source
                source_url
                source_date
                verifications {
                    verifier_id
                    verifier {
                        name
                    }
                    verified_date
                }
                modifications {
                    modifier_id
                    modifier {
                        name
                    }
                    modified_date
                }
            }
        }
    }
`;

export const CREATE_DETAIL = gql`
    mutation CreateDetail($model: CreateDetailsModelEnum!, $model_id: String!, $details: [DetailsField]) {
        createDetail(model: $model, model_id: $model_id, details: $details) {
            id
            name
            value
            is_primary
            is_organization_field
            context
            highlighted
            meta {
                author_id
                author {
                    id
                    name
                }
                verification
                verification_by
                source
                source_url
                source_date
                verifications {
                    verifier_id
                    verifier {
                        name
                    }
                    verified_date
                }
                modifications {
                    modifier_id
                    modifier {
                        name
                    }
                    modified_date
                }
            }
        }
    }
`;

export const DELETE_DETAIL = gql`
    mutation DeleteDetail($id: String, $ids: [String], $model: DeleteDetailsModelEnum!, $model_id: String!) {
        deleteDetail(id: $id, model: $model, model_id: $model_id, ids: $ids)
    }
`;
