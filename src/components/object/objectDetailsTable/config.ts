import { ICheckboxDropdownItemState } from "../../ui/checkboxDropdown";
import { TRole } from "../../../config/types";
import { IInfoRowParams } from "../../forms/dynamicInfoRows";

export const DATES_FIELDS = ["source_date", "created_at", "updated_at"];

export const MAX_AMOUNT_OF_FIELDS_HIGHLIGHTED = 4;
export const MAX_AMOUNT_OF_FIELDS_HIGHLIGHTED_ORG = 2;

export const BUILDING_DETAILS_NAME_ORDER = ["PLZ", "Gemeinde", "strasse", "nr", "EGID", "Kanton", "Ort", "BFS-Nummer", "Building details"];

export const OBJECT_DETAILS_TABLE_DEFAULT_COLUMNS: ICheckboxDropdownItemState[] = [
    {
        name: "value",
        label: "Value",
        sortable: false,
        value: true,
    },
    {
        name: "context",
        label: "Context",
        sortable: false,
        value: false,
    },
    {
        name: "source",
        label: "Source",
        sortable: false,
        value: true,
    },
    {
        name: "source_url",
        label: "Source url",
        sortable: false,
        value: false,
    },
    {
        name: "source_date",
        label: "Source Date",
        sortable: false,
        value: false,
    },
    {
        name: "verification",
        label: "Verification",
        sortable: false,
        value: true,
    },
    {
        name: "verification_by",
        label: "Verified by",
        sortable: false,
        value: false,
    },
    {
        name: "created_at",
        label: "Date Created",
        sortable: false,
        value: false,
    },
    {
        name: "author_data",
        label: "Author",
        sortable: false,
        value: false,
    },
    {
        name: "updated_at",
        label: "Date Modified",
        sortable: false,
        value: false,
    },
    {
        name: "updater_data",
        label: "Modified by",
        sortable: false,
        value: false,
    },
];

export const EDIT_AVAILABLE_FOR_ROLES: TRole[] = [
    "owner_administrator",
    "manufacturer_administrator",
    "manufacturer_brand_manager",
    "manufacturer_object_manager",
    "property_manager_administrator",
    "property_manager_building_manager",
    "service_provider_administrator",
    "service_provider_service_manager",
];

export interface IObjectDetailsRows {
    rows: IInfoRowParams[];
}

export interface IObjectDetailsItem extends IInfoRowParams {
    highlighted: boolean;
    events: { id: string; timeline: boolean }[];
}
