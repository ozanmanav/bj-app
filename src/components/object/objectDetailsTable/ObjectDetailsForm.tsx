import React, { FunctionComponent } from "react";
import { Formik } from "formik";
import { FormAccordionContent, FormCaption } from "../../forms/formsUI";
import { DynamicInfoRows, DynamicsInfoRowSchema, IInfoRowParams } from "../../forms/dynamicInfoRows";
import * as Yup from "yup";
import { IObjectDetailsRows } from "./config";
import { TPageType } from "../../../config/types/Page";

const ObjectDetailsSchema = Yup.object().shape({
    rows: DynamicsInfoRowSchema,
});

interface IObjectDetailsFormProps {
    model: IInfoRowParams;
    variant: TPageType;
    variantID: string;
    variantTypeID: string;
    onSubmit: (data: IObjectDetailsRows) => void;
    onRemove: () => void;
    isClientOrganization: boolean;
    cancel: () => void;
}

export const ObjectDetailsForm: FunctionComponent<IObjectDetailsFormProps> = ({
    model,
    variant,
    variantID,
    variantTypeID,
    onSubmit,
    onRemove,
    cancel,
    isClientOrganization,
}) => {
    return (
        <Formik
            initialValues={{ rows: [model] }}
            enableReinitialize={true}
            onSubmit={onSubmit}
            validationSchema={ObjectDetailsSchema}
            render={(props) => (
                <form onSubmit={props.handleSubmit} className="b-object-details-table__add-detail-form">
                    <FormAccordionContent>
                        <FormCaption>Add meta data</FormCaption>
                        <DynamicInfoRows
                            {...props}
                            variant={variant}
                            variantID={variantID}
                            variantTypeID={variantTypeID}
                            removeCustomField={variant === "organization" && !isClientOrganization}
                            removeSingleRow={onRemove}
                            formType="single"
                            saveButtonType="submit"
                            cancel={cancel}
                        />
                    </FormAccordionContent>
                </form>
            )}
        />
    );
};
