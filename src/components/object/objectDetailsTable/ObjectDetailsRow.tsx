import React, { FunctionComponent } from "react";
import classnames from "classnames";
import { HighlightFieldButton } from "./highlightFieldButton";
import { UserRoleCheck } from "../../userRoleCheck";
import { EditButton, CalendarAddButton } from "../../ui/buttons";
import { EDIT_AVAILABLE_FOR_ROLES, IObjectDetailsItem } from "./config";
import { URL_REGEX } from "../../../config";
import { IInfoRowParams } from "../../forms/dynamicInfoRows";
import { ICheckboxDropdownItemState } from "../../ui/checkboxDropdown";
import { getObjectDetailsFieldValue } from "./utils";
import { getSource } from "../../forms/dynamicInfoRows/utils";
import get from "lodash.get";
import { Link } from "react-router-dom";
import { EventCheckboxSwitch } from "../../events";
import { TPageType } from "../../../config/types/Page";

interface IObjectDetailsTableRowProps {
    item: IObjectDetailsItem;
    objectID: string;
    refetch: () => void;
    openForm: (item: IInfoRowParams) => void;
    columnsToShow: ICheckboxDropdownItemState[];
    isAnyOrganization: boolean;
    isEditAvailable?: boolean;
    canHighlightMore?: boolean;
    eventRefetch: Function;
    variant: TPageType;
    hasValueColumn: boolean;
}

export const ObjectDetailsTableRow: FunctionComponent<IObjectDetailsTableRowProps> = ({
    openForm,
    item,
    columnsToShow,
    isEditAvailable,
    objectID,
    refetch,
    canHighlightMore,
    isAnyOrganization,
    eventRefetch,
    variant,
    hasValueColumn,
}) => {
    function showForm() {
        openForm(item);
    }

    const rowClassname = classnames(["b-table__row _medium", { "_no-edit": !isEditAvailable }]);
    const events = get(item, "events") || [];
    return (
        <tr className={rowClassname}>
            <td className="b-table__cell _font-bold">
                <div className="flex align-center">
                    <HighlightFieldButton
                        isHighlighted={item.highlighted}
                        objectID={objectID}
                        refetch={refetch}
                        type={item.name}
                        canHighlightMore={canHighlightMore}
                        variant={variant}
                        detailID={item.id || ""}
                    />

                    <span className="b-object-details-table__detail-label _text-capitalize" title={item.name}>
                        {item.name.replace("_", " ")}
                    </span>
                </div>
            </td>
            <td className="b-table__cell">
                <UserRoleCheck
                    availableForRoles={[
                        "owner_administrator",
                        "owner_portfolio_manager",
                        "manufacturer_administrator",
                        "manufacturer_brand_manager",
                        "manufacturer_object_manager",
                        "property_manager_administrator",
                        "property_manager_building_manager",
                        "service_provider_administrator",
                        "service_provider_service_manager",
                    ]}
                >
                    <div className="b-object-details-table__actions flex align-center">
                        <EventCheckboxSwitch events={events} refetch={eventRefetch} />
                        <UserRoleCheck availableForRoles={EDIT_AVAILABLE_FOR_ROLES}>
                            <EditButton onClick={showForm} />
                            <Link
                                to={{
                                    pathname: `/app/event/add`,
                                    search: `?${isAnyOrganization ? "org" : "obj"}=${objectID}&detailID=${item.id}`,
                                }}
                            >
                                <CalendarAddButton />
                            </Link>
                        </UserRoleCheck>
                    </div>
                </UserRoleCheck>
            </td>
            {(!columnsToShow.length || !hasValueColumn) && <td className="b-table__cell"></td>}
            {columnsToShow.map(({ name }) => {
                //spreading meta to object
                const spreadItem = { ...item, ...get(item, "meta") };

                if (name === "author_data") {
                    const author = get(spreadItem, "author");

                    if (author) {
                        const authorName = get(author, "name");

                        return (
                            <td className="b-table__cell" key={name}>
                                {authorName}
                            </td>
                        );
                    }
                }

                const value = getObjectDetailsFieldValue(spreadItem, name);

                if (name === "source") {
                    const source = getSource(value);

                    if (typeof source === "object" && source) {
                        return (
                            <td className="b-table__cell" key={name}>
                                <a href={source.href} target="_blank" rel="noopener noreferrer">
                                    {source.name}
                                </a>
                            </td>
                        );
                    }
                }

                return (
                    <td className="b-table__cell" key={name}>
                        {URL_REGEX.test(value) || name === "source_url" ? (
                            <a href={value} target="_blank" rel="noopener noreferrer">
                                {value}
                            </a>
                        ) : (
                            value
                        )}
                    </td>
                );
            })}
        </tr>
    );
};
