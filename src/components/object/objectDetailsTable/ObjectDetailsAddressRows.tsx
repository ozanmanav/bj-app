import React, { FunctionComponent } from "react";
import { ObjectDetailsCustomRow } from "./ObjectDetailsCustomRow";
import { ICheckboxDropdownItemState } from "../../ui";
import get from "lodash.get";

interface IObjectDetailsAddressRowsProps {
    address: string;
    isEditAvailable?: boolean;
    columnsToShow: ICheckboxDropdownItemState[];
    addressSubmit: (updatedFieldValue: string, updatedFieldName: string) => void;
}

export const ObjectDetailsAddressRows: FunctionComponent<IObjectDetailsAddressRowsProps> = ({
    address,
    isEditAvailable,
    columnsToShow,
    addressSubmit,
}) => {
    return (
        <>
            <ObjectDetailsCustomRow
                key="street"
                name="street"
                columnsToShow={columnsToShow}
                value={get(address, "street")}
                isEditAvailable={isEditAvailable}
                onSubmitSave={addressSubmit}
            />
            <ObjectDetailsCustomRow
                key="number"
                name="number"
                columnsToShow={columnsToShow}
                value={get(address, "number")}
                isEditAvailable={isEditAvailable}
                onSubmitSave={addressSubmit}
            />
            <ObjectDetailsCustomRow
                key="plz"
                name="plz"
                columnsToShow={columnsToShow}
                value={get(address, "plz")}
                isEditAvailable={isEditAvailable}
                onSubmitSave={addressSubmit}
            />
            <ObjectDetailsCustomRow
                key="city"
                name="city"
                columnsToShow={columnsToShow}
                value={get(address, "city")}
                isEditAvailable={isEditAvailable}
                onSubmitSave={addressSubmit}
            />
            <ObjectDetailsCustomRow
                key="country"
                name="country"
                columnsToShow={columnsToShow}
                value={get(address, "country")}
                isEditAvailable={isEditAvailable}
                onSubmitSave={addressSubmit}
            />
        </>
    );
};
