import React, { FunctionComponent, useState } from "react";
import "./ObjectDetailsTable.scss";
import { PlusButton, showErrorToast, handleGraphQLErrors, CheckboxDropdown, showSuccessToast, useModal } from "../../ui";
import { getOrganizationTypesOptions, getOrganizationRolesOptions, getObjectTypesOptions } from "../../../utils";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { DYNAMIC_INFO_ROW_DEFAULT_VALUE, IInfoRowParams, GET_ENTITY_META } from "../../forms/dynamicInfoRows";
import { GRAPHQL_ADMIN_CONTEXT, IAddress } from "../../../config";
import get from "lodash.get";
import { UserRoleCheck } from "../../userRoleCheck";
import {
    EDIT_AVAILABLE_FOR_ROLES,
    IObjectDetailsRows,
    MAX_AMOUNT_OF_FIELDS_HIGHLIGHTED,
    OBJECT_DETAILS_TABLE_DEFAULT_COLUMNS,
    MAX_AMOUNT_OF_FIELDS_HIGHLIGHTED_ORG,
} from "./config";
import {
    detailsSortFunction,
    getDetailsPath,
    getNamePath,
    getTypePath,
    normalizeDetailsUsers,
    getAddressPath,
    getRolePath,
    getCategoryPath,
    getEmailPath,
    getPhonePath,
    getMetaPath,
    getCreatedAtPath,
} from "./utils";
import { useRoleCheck, useOrganizationTypes, useOrganizationRoles, useObjectTypes } from "../../../hooks";
import classnames from "classnames";
import { ObjectDetailsTableRow } from "./ObjectDetailsRow";
import { ObjectDetailsForm } from "./ObjectDetailsForm";
import { useDynamicTableColumns } from "../../../hooks/useDynamicTableColumns";
import { TPageType } from "../../../config/types/Page";
import { GET_VARIANT_INFOS, UPDATE_VARIANT, GET_DETAILS, UPDATE_DETAIL, CREATE_DETAIL, DELETE_DETAIL } from "./queries";
import { ObjectDetailsCustomRow } from "./ObjectDetailsCustomRow";
import { getObjectWithoutTypename } from "../../../utils/getArrayWithoutTypename";
import { Loader } from "../../ui/loader";
import { ConfirmModal } from "../../modals";
import { GET_CONTENT_HEADER_OBJECT } from "../../contentHeaders/objectContentHeader/queries";

interface IObjectDetailsTableProps {
    variantID: string;
    variant: TPageType;
    eventRefetch: Function;
}

export const ObjectDetailsTable: FunctionComponent<IObjectDetailsTableProps> = ({ variantID, variant, eventRefetch }) => {
    const [isFormOpen, setFormOpen] = useState<boolean>(false);
    const [typeID, setTypeID] = useState<string>("");
    const [model, setModel] = useState<IInfoRowParams>(DYNAMIC_INFO_ROW_DEFAULT_VALUE);
    const [inlineEditMode, setInlineEditMode] = useState<Array<{ name: string; isEditing: boolean }>>([]);
    const uppercasedVariant = variant && variant.toUpperCase();
    const { isOpen: isConfirmModalOpen, open: openConfirmModal, hide: hideConfirmModal } = useModal();

    const { data: variantData, refetch: refetchVariantInfos, loading: loadingVariantInfos } = useQuery(GET_VARIANT_INFOS[variant], {
        variables: {
            id: variantID,
        },
        fetchPolicy: "no-cache",
        ...GRAPHQL_ADMIN_CONTEXT,
    });

    const [updateVariantMutation] = useMutation(UPDATE_VARIANT[variant]);

    const { data: detailsData, refetch: refetchDetails, loading: loadingDetails } = useQuery(GET_DETAILS, {
        variables: {
            model_id: variantID,
            model: uppercasedVariant,
        },
        fetchPolicy: "no-cache",
        ...GRAPHQL_ADMIN_CONTEXT,
    });

    const [updateDetailMutation] = useMutation(UPDATE_DETAIL);
    const [createDetailMutation] = useMutation(CREATE_DETAIL);
    const [deleteDetailMutation] = useMutation(DELETE_DETAIL);

    const objectTypes = useObjectTypes();
    const objectName = get(variantData, getNamePath(variant));
    const objectEmail = get(variantData, getEmailPath(variant));
    const objectPhone = get(variantData, getPhonePath(variant));
    const objectRole = get(variantData, getRolePath(variant)) || "";

    const variantType = get(variantData, getTypePath(variant)) || "";

    const refetchMetaFields = {
        query: GET_ENTITY_META,
        variables: {
            model_id: get(variantType, "id"),
        },
    };

    const refetchContentHeaderObject = {
        query: GET_CONTENT_HEADER_OBJECT,
        variables: {
            id: variantID,
        },
    };

    const isObject = variant === "object";
    const isAnyOrganization = variant === "organization";
    const organizationTypes = useOrganizationTypes();
    const organizationRoles = useOrganizationRoles();
    const organizationCategory = get(variantData, getCategoryPath(variant));
    const isClientOrganization = organizationCategory === "CLIENT";
    const meta = get(variantData, getMetaPath(variant));
    const created_at = get(variantData, getCreatedAtPath(variant));

    const rawDetails = get(detailsData, getDetailsPath(variant)) || [];
    const details = normalizeDetailsUsers(rawDetails.sort(detailsSortFunction));

    const rawAddress = get(variantData, getAddressPath(variant)) || {};

    const address = getObjectWithoutTypename(rawAddress);

    const isEditAvailable = useRoleCheck(EDIT_AVAILABLE_FOR_ROLES);

    const canHighlightMore =
        details.filter(({ highlighted }) => highlighted).length <
        (variant === "object" ? MAX_AMOUNT_OF_FIELDS_HIGHLIGHTED : MAX_AMOUNT_OF_FIELDS_HIGHLIGHTED_ORG);

    const { columns, columnsToShow, updateColumns } = useDynamicTableColumns(OBJECT_DETAILS_TABLE_DEFAULT_COLUMNS, "object_details");

    function setInlineEditModeFromChild(objectType: string, initRun: boolean): void {
        if (initRun) {
            setInlineEditMode((prevState) => [...prevState, { name: objectType, isEditing: false }]);
        } else {
            let rawInlineEditMode: Array<{ name: string; isEditing: boolean }> = inlineEditMode;
            let changeIndex: number = rawInlineEditMode.findIndex((object) => object.name === objectType);
            rawInlineEditMode[changeIndex] = { name: objectType, isEditing: !rawInlineEditMode[changeIndex].isEditing };
            setInlineEditMode([...rawInlineEditMode]);
        }
    }

    function showEditForm(data: IInfoRowParams) {
        setModel(data);
        showForm();
    }

    function showAddForm() {
        setModel(DYNAMIC_INFO_ROW_DEFAULT_VALUE);
        showForm();
    }

    function showForm() {
        setFormOpen(true);
    }

    function hideForm() {
        setFormOpen(false);
    }

    function onSubmitDetails({ rows }: IObjectDetailsRows) {
        const {
            id,
            type,
            name,
            value,
            context,
            highlighted,
            customName,
            meta: { source = "", source_url = "", source_date = "", verification_by = "", verification = false },
        } = rows[0];

        const isCustomField = name === "Custom";

        const detail = id
            ? {
                  ...(id && { id }),
                  model: uppercasedVariant,
                  model_id: variantID,
                  type,
                  name: isCustomField ? customName : name,
                  is_custom_field: isCustomField,
                  value,
                  context,
                  highlighted,
                  verification,
                  source,
                  source_url,
                  source_date,
                  verification_by,
              }
            : {
                  model: uppercasedVariant,
                  model_id: variantID,
                  details: [
                      {
                          type,
                          name: isCustomField ? customName : name,
                          is_custom_field: isCustomField,
                          value,
                          context,
                          highlighted,
                          verification,
                          source,
                          source_url,
                          source_date,
                          verification_by,
                      },
                  ],
              };

        const operationMutation = id ? updateDetailMutation : createDetailMutation;
        const operationMessagePostfix = id ? "updated" : "created";

        operationMutation({
            variables: detail,
            refetchQueries: [refetchMetaFields],
        })
            .then(({ errors }) => {
                if (errors) {
                    handleGraphQLErrors(errors);
                } else {
                    showSuccessToast(`Detail successfully ${operationMessagePostfix}`);
                    refetchDetails();
                    hideForm();
                }
            })
            .catch((e) => showErrorToast(e.message));
    }

    interface IUpdatedObjectFields {
        details: IInfoRowParams[];
        address: IAddress;
        name?: string;
        type_id?: string;
        role_id?: string;
        email?: string;
        phone?: string;
    }
    function updateVariant(updatedObjectFields: IUpdatedObjectFields) {
        const { address, name, type_id, role_id, email, phone } = updatedObjectFields;

        const options = {
            variables: {
                id: variantID,
                type: variantType ? (isAnyOrganization ? variantType.type : variantType) : "",
                ...(address && { address }),
                ...(name ? { name } : { name: objectName }),
                ...(type_id && { type_id }),
                // ...(objectType && { type_id: objectType.id }),
                ...(role_id && { role_id }),
                ...(email && { email }),
                ...(phone && { phone }),
            },
            refetchQueries: [refetchMetaFields],
        };

        updateVariantMutation(options)
            .then(({ errors }) => {
                if (errors) {
                    handleGraphQLErrors(errors);
                } else {
                    showSuccessToast("Successfully Updated");
                    refetchVariantInfos();
                    hideForm();
                }
            })
            .catch((e) => showErrorToast(e.message));
    }

    function onRemoveDetail() {
        const detailID = get(model, "id");

        if (detailID) {
            deleteDetailMutation({
                variables: { id: detailID, model: uppercasedVariant, model_id: variantID },
                refetchQueries: [refetchMetaFields, refetchContentHeaderObject],
            })
                .then(({ errors }) => {
                    if (errors) {
                        handleGraphQLErrors(errors);
                    } else {
                        showSuccessToast("Successfully Deleted");
                        refetchDetails();
                        hideForm();
                    }
                })
                .catch((e) => showErrorToast(e.message));
        }
    }

    // function addressSubmit(updateFieldValue: string, updatedFieldKey: string) {
    //     const updatedAddress: IAddress = { ...address, [updatedFieldKey]: updateFieldValue };

    //     updateVariant({ address: updatedAddress } as IUpdatedObjectFields);
    // }
    function handleTypeSubmit(updatedTypeId: string) {
        if (updatedTypeId !== variantType.id) {
            setTypeID(updatedTypeId);
            openConfirmModal();
        }
    }

    function handleConfirm() {
        updateVariant({ type_id: typeID } as IUpdatedObjectFields);

        hideConfirmModal();
    }

    function nameSubmit(updatedName: string) {
        updateVariant({ name: updatedName } as IUpdatedObjectFields);
    }

    function typeSubmit(updatedTypeId: string) {
        updateVariant({ type_id: updatedTypeId } as IUpdatedObjectFields);
    }

    function roleSubmit(updatedRoleId: string) {
        updateVariant({ role_id: updatedRoleId } as IUpdatedObjectFields);
    }

    function emailSubmit(updatedEmail: string) {
        updateVariant({ email: updatedEmail } as IUpdatedObjectFields);
    }

    function phoneSubmit(updatedPhone: string) {
        updateVariant({ phone: updatedPhone } as IUpdatedObjectFields);
    }

    const rowClassname = classnames(["b-table__row _head", { "_no-edit": !isEditAvailable }]);
    const showData = details.length > 0 || address;
    const hasValueColumn: boolean = !!columnsToShow.filter((columnToShow) => columnToShow.name === "value").length;

    return (
        <>
            <div className="b-object-details-table__wrapper">
                <h3 className="b-table__title flex align-center">
                    Details
                    <UserRoleCheck
                        availableForRoles={[
                            "owner_administrator",
                            "manufacturer_administrator",
                            "manufacturer_brand_manager",
                            "manufacturer_object_manager",
                            "property_manager_administrator",
                            "property_manager_building_manager",
                            "service_provider_administrator",
                        ]}
                    >
                        <PlusButton onClick={showAddForm} />
                    </UserRoleCheck>
                    <CheckboxDropdown className="b-object-details-table__column-select" checkboxes={columns} handleChange={updateColumns} />
                </h3>
                {loadingDetails || loadingVariantInfos ? (
                    <Loader withText text="Details" />
                ) : (
                    <>
                        {" "}
                        {showData && (
                            <div
                                className="b-table__wrapper"
                                style={
                                    inlineEditMode.filter((object) => object.isEditing === true).length
                                        ? { overflowX: "unset", position: "relative" }
                                        : { overflowX: "scroll" }
                                }
                            >
                                <table className="b-table b-object-details-table">
                                    <thead className="b-table__head">
                                        <tr className={rowClassname}>
                                            <th className="b-table__cell">Information</th>
                                            <th className="b-table__cell" />
                                            {!hasValueColumn && <th className="b-table__cell"></th>}
                                            {columnsToShow.map(({ label }) => (
                                                <th className="b-table__cell" key={label}>
                                                    {label}
                                                </th>
                                            ))}
                                        </tr>
                                    </thead>
                                    <tbody className="b-table__body">
                                        {isAnyOrganization && objectName && (
                                            <ObjectDetailsCustomRow
                                                key={"Name"}
                                                name={"Name"}
                                                columnsToShow={columnsToShow}
                                                value={objectName}
                                                isEditAvailable={isEditAvailable}
                                                onSubmitSave={nameSubmit}
                                                meta={{ ...meta, created_at }}
                                                variant={variant}
                                                variantID={variantID}
                                                setInlineEditModeFromChild={setInlineEditModeFromChild}
                                            />
                                        )}
                                        {isObject && (
                                            <ObjectDetailsCustomRow
                                                key={"Type"}
                                                inlineInputType="select"
                                                inlineSelectOptions={getObjectTypesOptions(objectTypes)}
                                                inlineSelectPlaceholder="Please select a type"
                                                name={"Type"}
                                                columnsToShow={columnsToShow}
                                                value={variantType && variantType.name}
                                                selectValue={variantType && variantType.id}
                                                isEditAvailable={isEditAvailable}
                                                onSubmitSave={handleTypeSubmit}
                                                meta={{ ...meta, created_at }}
                                                variant={variant}
                                                variantID={variantID}
                                                setInlineEditModeFromChild={setInlineEditModeFromChild}
                                            />
                                        )}
                                        {isAnyOrganization && (
                                            <ObjectDetailsCustomRow
                                                key={"Type"}
                                                inlineInputType="select"
                                                inlineSelectOptions={getOrganizationTypesOptions(organizationTypes)}
                                                inlineSelectPlaceholder="Please select a type"
                                                name={"Type"}
                                                columnsToShow={columnsToShow}
                                                value={variantType && variantType.name}
                                                selectValue={variantType && variantType.id}
                                                isEditAvailable={isEditAvailable}
                                                onSubmitSave={typeSubmit}
                                                meta={{ ...meta, created_at }}
                                                variant={variant}
                                                variantID={variantID}
                                                setInlineEditModeFromChild={setInlineEditModeFromChild}
                                            />
                                        )}
                                        {isAnyOrganization && objectName ? (
                                            <ObjectDetailsCustomRow
                                                key={"E-Mail"}
                                                name={"E-Mail"}
                                                columnsToShow={columnsToShow}
                                                value={objectEmail}
                                                isEditAvailable={isEditAvailable}
                                                onSubmitSave={emailSubmit}
                                                meta={{ ...meta, created_at }}
                                                variant={variant}
                                                variantID={variantID}
                                                setInlineEditModeFromChild={setInlineEditModeFromChild}
                                            />
                                        ) : null}
                                        {isAnyOrganization && objectName && (
                                            <ObjectDetailsCustomRow
                                                key={"Phone"}
                                                name={"Phone"}
                                                inlineInputType="phone"
                                                columnsToShow={columnsToShow}
                                                value={objectPhone}
                                                isEditAvailable={isEditAvailable}
                                                onSubmitSave={phoneSubmit}
                                                meta={{ ...meta, created_at }}
                                                variant={variant}
                                                variantID={variantID}
                                                setInlineEditModeFromChild={setInlineEditModeFromChild}
                                            />
                                        )}
                                        {isClientOrganization && (
                                            <ObjectDetailsCustomRow
                                                key={"Role"}
                                                inlineInputType="select"
                                                inlineSelectOptions={getOrganizationRolesOptions(organizationRoles)}
                                                inlineSelectPlaceholder="Please select a role"
                                                name={"Role"}
                                                columnsToShow={columnsToShow}
                                                value={objectRole && objectRole.name}
                                                selectValue={objectRole && objectRole.id}
                                                isEditAvailable={isEditAvailable}
                                                onSubmitSave={roleSubmit}
                                                meta={{ ...meta, created_at }}
                                                variant={variant}
                                                variantID={variantID}
                                                setInlineEditModeFromChild={setInlineEditModeFromChild}
                                            />
                                        )}
                                        {/* {isAnyOrganization && address && (
                                            <ObjectDetailsAddressRows
                                                columnsToShow={columnsToShow}
                                                address={address}
                                                isEditAvailable={isEditAvailable}
                                                addressSubmit={addressSubmit}
                                            />
                                        )} */}
                                        {details.length > 0 &&
                                            details.map((item, i) => {
                                                return (
                                                    <ObjectDetailsTableRow
                                                        openForm={showEditForm}
                                                        item={item}
                                                        key={i}
                                                        columnsToShow={columnsToShow}
                                                        isEditAvailable={isEditAvailable}
                                                        objectID={variantID}
                                                        refetch={refetchDetails}
                                                        canHighlightMore={canHighlightMore}
                                                        isAnyOrganization={isAnyOrganization}
                                                        eventRefetch={eventRefetch}
                                                        variant={variant}
                                                        hasValueColumn={hasValueColumn}
                                                    />
                                                );
                                            })}
                                    </tbody>
                                </table>
                            </div>
                        )}{" "}
                    </>
                )}
                {isFormOpen && (
                    <ObjectDetailsForm
                        variant={variant}
                        variantID={variantID}
                        variantTypeID={get(variantType, "id")}
                        isClientOrganization={isClientOrganization}
                        model={model}
                        onRemove={onRemoveDetail}
                        onSubmit={onSubmitDetails}
                        cancel={hideForm}
                    />
                )}
                {isObject && (
                    <ConfirmModal
                        title="Are you sure?"
                        description="By changing the object type the current object details will be deleted"
                        onConfirm={handleConfirm}
                        hide={hideConfirmModal}
                        isOpen={isConfirmModalOpen}
                    />
                )}
            </div>
        </>
    );
};
