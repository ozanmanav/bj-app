import React, { FunctionComponent, useState } from "react";
import "./RelatedObject.scss";
import { PlusButton, useModal, ButtonLink } from "../../ui";
import { TRelatedObjectVariants } from "./variants";
import { RelatedObjectTable, IRelatedObjectTableItem } from "./relatedObjectTable";
import { UserRoleCheck } from "../../userRoleCheck";
import { TRelationRoles, TRole } from "../../../config/types";
import { AddRelationModal } from "../../modals/addRelationModal";
import { IAddRelationFormState } from "../../forms";

interface IRelatedObjectProps {
    data: IRelatedObjectTableItem[];
    title: string;
    variant?: TRelatedObjectVariants;
    updateCallback: Function;
    addObjectLink?: string;
    bulkAddObjectsLink?: string;
    relationRole?: TRelationRoles;
    objectID: string;
    objectName: string;
    objectCity?: string;
    addMultipleChildObjects?: boolean;
    defaultRole?: string;
    eventRefetch: Function;
}

export const ROLES_FOR_ADDING_OBJECTS: TRole[] = [
    "owner_administrator",
    "manufacturer_administrator",
    "manufacturer_brand_manager",
    "manufacturer_object_manager",
    "property_manager_administrator",
    "property_manager_building_manager",
    "service_provider_administrator",
    "service_provider_service_manager",
];

export const RelatedObject: FunctionComponent<IRelatedObjectProps> = ({
    data,
    title,
    variant,
    updateCallback,
    addObjectLink,
    objectID,
    relationRole,
    addMultipleChildObjects,
    objectName,
    objectCity,
    defaultRole,
    eventRefetch,
}) => {
    const [editedRelation, setEditedRelation] = useState<any>(undefined);
    const { open, hide, isOpen } = useModal();

    function onRelationSubmit() {
        updateCallback();
        hide();
    }

    async function onAddClick() {
        await setEditedRelation(undefined);
        open();
    }

    async function onEditClick(relation: IAddRelationFormState) {
        await setEditedRelation(relation);
        open();
    }

    return (
        <div className="b-related-object">
            <div className="flex align-center justify-between">
                <h3 className="b-related-object__h3">
                    {title}
                    {addObjectLink && (
                        <UserRoleCheck availableForRoles={ROLES_FOR_ADDING_OBJECTS}>
                            <PlusButton onClick={onAddClick} />

                            {addMultipleChildObjects && (
                                <ButtonLink
                                    text="Create Multiple Child Objects"
                                    to={`/app/object/add/simple?parentID=${objectID}`}
                                    className="b-related-object__action"
                                />
                            )}
                        </UserRoleCheck>
                    )}
                </h3>
            </div>
            <RelatedObjectTable
                relationRole={relationRole}
                data={data}
                variant={variant}
                updateCallback={updateCallback}
                onEditClick={onEditClick}
                eventRefetch={eventRefetch}
            />
            <AddRelationModal
                onSubmit={onRelationSubmit}
                objectID={objectID}
                objectName={objectName}
                editedRelation={editedRelation}
                hide={hide}
                isOpen={isOpen}
                addObjectLink={addObjectLink}
                relationRole={relationRole}
                objectCity={objectCity}
                defaultRole={defaultRole}
            />
        </div>
    );
};
