import React, { FunctionComponent, useState } from "react";
import { TableCollapseButton } from "../../../tables/tableCollapseButton";
import { ObjectNameSimple } from "../../../objectName";
import { CalendarAddButton, RemoveIconButton, SortButton, EditButton } from "../../../ui/buttons";
import { TRelatedObjectVariants } from "../variants";
import { useSortingTable } from "../../../../hooks";
import { generateGroupName, groupRelatedObjects } from "../../../../utils";
import { ConfirmModal } from "../../../modals/confirmModal";
import { useModal } from "../../../ui/modal";
import { useApolloClient } from "@apollo/react-hooks";
import { showErrorToast, showSuccessToast } from "../../../ui/toasts";
import gql from "graphql-tag";
import { UserRoleCheck } from "../../../userRoleCheck";
import { Icon } from "../../../ui/icons";
import { IObjectMinimal, TRelationRoles } from "../../../../config/types";
import classnames from "classnames";
import orderBy from "lodash.orderby";
import get from "lodash.get";
import { IAddRelationFormState } from "../../../forms";
import { Link } from "react-router-dom";
import { EventCheckboxSwitch } from "../../../events";

const ITEMS_TO_SHOW = 2;

const SORT_STRINGS = {
    NAME: "name",
    TYPE: "typeLabel",
    RELATION_TYPE: "relationType",
};

const DELETE_RELATIONSHIP = gql`
    mutation DeleteRelationship($id: String!) {
        deleteRelationship(id: $id)
    }
`;

export interface IRelatedObjectTableItem extends IObjectMinimal {
    relationID: string;
    relationType?: string;
}

interface IRelatedObjectTableProps {
    data: IRelatedObjectTableItem[];
    relationRole?: TRelationRoles;
    variant?: TRelatedObjectVariants;
    updateCallback: Function;
    onEditClick: (relation: IAddRelationFormState) => void;
    eventRefetch: Function;
}

export const RelatedObjectTable: FunctionComponent<IRelatedObjectTableProps> = ({
    onEditClick,
    data,
    variant,
    updateCallback,
    relationRole,
    eventRefetch,
}) => {
    const [isCollapsed, setIsCollapsed] = useState<boolean>(true);
    const { isOpen, hide, open } = useModal();
    const [objectsToDelete, setObjectsToDelete] = useState<IRelatedObjectTableItem[]>([]);
    const apolloClient = useApolloClient();

    const { sortBy, sortingOrder, getSortOrderForField, changeSortBy } = useSortingTable({
        localStorageKey: "related_object",
    });

    const dataWithTypeLabels = data.map((item) => {
        return {
            ...item,
            typeLabel: get(item, "type.name") || "",
        };
    });

    const groups = groupRelatedObjects(orderBy(dataWithTypeLabels, [sortBy], [sortingOrder]));
    const groupsSorted = sortBy === "name" ? orderBy(groups, [(group) => get(group, "[0]")], [sortingOrder]) : groups;
    const shouldAllowCollapse = groupsSorted.length > ITEMS_TO_SHOW;
    const dataToShow = isCollapsed && shouldAllowCollapse ? groupsSorted.slice(0, ITEMS_TO_SHOW) : groupsSorted;
    const rowsAmount = groupsSorted.length;
    const collapsedItems = groupsSorted.length - ITEMS_TO_SHOW;

    let tableWrapperClassName = "b-table__wrapper b-related-object-table";

    if (variant) {
        tableWrapperClassName += ` _${variant}`;
    }

    function toggle() {
        setIsCollapsed(!isCollapsed);
    }

    function onDeleteModalOpen(ids: string[]) {
        setObjectsToDelete(data.filter(({ id }) => ids.includes(id)));
        open();
    }

    function onDeleteModalHide() {
        setObjectsToDelete([]);
        hide();
    }

    async function onObjectsDelete() {
        try {
            hide();

            await Promise.all(
                objectsToDelete.map(({ relationID }) =>
                    apolloClient.mutate({
                        mutation: DELETE_RELATIONSHIP,
                        variables: {
                            id: relationID,
                        },
                    })
                )
            );

            showSuccessToast(`${objectsToDelete.map(({ name }) => `"${name}"`)} are successfully removed from relations.`);
            setObjectsToDelete([]);

            updateCallback();
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    if (dataToShow.length === 0) {
        return null;
    }

    function sortByName() {
        changeSortBy(SORT_STRINGS.NAME);
    }

    function sortByType() {
        changeSortBy(SORT_STRINGS.TYPE);
    }

    function sortByRelationType() {
        changeSortBy(SORT_STRINGS.RELATION_TYPE);
    }

    const confirmationMessage = `Are you sure you want to delete relationship with ${objectsToDelete
        .map(({ name }) => `"${name}"`)
        .join(", ")}?`;

    return (
        <div className={tableWrapperClassName}>
            <table className="b-table">
                <thead className="b-table__head">
                    <tr className="b-table__row _head">
                        {variant === "combined" && (
                            <th className="b-table__cell">
                                <SortButton
                                    text="Relationship"
                                    onClick={sortByRelationType}
                                    sorted={getSortOrderForField(SORT_STRINGS.RELATION_TYPE)}
                                />
                            </th>
                        )}
                        <th className="b-table__cell">
                            <div className="flex align-center">
                                <SortButton text="Related Object" onClick={sortByName} sorted={getSortOrderForField(SORT_STRINGS.NAME)} />
                                {variant !== "simple" && <BuildingRelatedObjectTableRowsAmount amount={rowsAmount} />}
                            </div>
                        </th>

                        <th className="b-table__cell">
                            <SortButton
                                text="Type of Related Object"
                                onClick={sortByType}
                                sorted={getSortOrderForField(SORT_STRINGS.TYPE)}
                            />
                        </th>
                    </tr>
                </thead>
                <tbody className="b-table__body">
                    {dataToShow.map(([groupName, groupItems]: any, index) => (
                        <BuildingRelatedObjectTableRowGroup
                            name={groupName}
                            items={groupItems}
                            key={index}
                            onDelete={onDeleteModalOpen}
                            onEditClick={onEditClick}
                            variant={variant}
                            relationRole={relationRole}
                            eventRefetch={eventRefetch}
                        />
                    ))}
                </tbody>
            </table>
            {shouldAllowCollapse && <TableCollapseButton isCollapsed={isCollapsed} collapsedItems={collapsedItems} onClick={toggle} />}
            <ConfirmModal title={confirmationMessage} onConfirm={onObjectsDelete} hide={onDeleteModalHide} isOpen={isOpen} />
        </div>
    );
};

interface IBuildingRelatedObjectTableRowProps {
    id: string;
    name: string;
    type: string;
    typeLabel: string;
    relationType?: string;
    relationTypeID: string;
    onDelete: (ids: string[]) => void;
    onEditClick: (relation: IAddRelationFormState) => void;
    expanded?: boolean;
    variant?: TRelatedObjectVariants;
    relationID?: string;
    relationRole?: TRelationRoles;
    events: { id: string; timeline: boolean }[];
    eventRefetch: Function;
}

interface IEditRelationFormState extends IAddRelationFormState {
    objectName?: string;
    relationID: string;
}

const BuildingRelatedObjectTableRowExpanded: FunctionComponent<IBuildingRelatedObjectTableRowProps> = ({
    id,
    name,
    type,
    typeLabel,
    onDelete,
    expanded,
    relationType,
    variant,
    onEditClick,
    relationID,
    relationTypeID,
    relationRole,
    events,
    eventRefetch,
}) => {
    const firstCellClassName = classnames(["b-table__cell", { "b-related-object-table__expanded": expanded }]);

    function onSelfDelete() {
        onDelete([id]);
    }

    function onSelfEdit() {
        const relation: IEditRelationFormState = {
            objectID: id,
            role: relationTypeID || "",
            objectName: name,
            relationID: relationID || "",
        };
        onEditClick(relation);
    }

    return (
        <tr className="b-table__row _medium">
            {variant === "combined" && <td className="b-table__cell _text-capitalize">{relationType}</td>}
            <td className={firstCellClassName}>
                <ObjectNameSimple type={type} name={name} id={id} />
            </td>

            <td className="b-table__cell b-related-object-table__actions-cell">
                <div className="flex justify-between align-center">
                    <p>{typeLabel}</p>
                    <div className="b-related-object-table-row__actions flex align-center">
                        <EventCheckboxSwitch events={events} refetch={eventRefetch} />
                        <UserRoleCheck
                            availableForRoles={[
                                "owner_administrator",
                                "manufacturer_administrator",
                                "manufacturer_brand_manager",
                                "manufacturer_object_manager",
                                "property_manager_administrator",
                                "property_manager_building_manager",
                                "service_provider_administrator",
                                "service_provider_service_manager",
                            ]}
                        >
                            <Link to={{ pathname: `/app/event/add`, search: `?obj=${id}&relationshipID=${relationID}` }}>
                                <CalendarAddButton />
                            </Link>
                            <EditButton onClick={onSelfEdit} />
                            <RemoveIconButton onClick={onSelfDelete} />
                        </UserRoleCheck>
                    </div>
                </div>
            </td>
        </tr>
    );
};

interface IBuildingRelatedObjectTableRowGroupProps {
    name: string;
    items: any[];
    onDelete: (ids: string[]) => void;
    onEditClick: (relation: IAddRelationFormState) => void;
    variant?: TRelatedObjectVariants;
    relationRole?: TRelationRoles;
    eventRefetch: Function;
}

const BuildingRelatedObjectTableRowGroup: FunctionComponent<IBuildingRelatedObjectTableRowGroupProps> = ({
    name,
    items,
    onDelete,
    variant,
    onEditClick,
    relationRole,
    eventRefetch,
}) => {
    const [isExpanded, setIsExpanded] = useState<boolean>();
    if (items.length === 1) {
        return (
            <BuildingRelatedObjectTableRowExpanded
                {...items[0]}
                relationRole={relationRole}
                onEditClick={onEditClick}
                onDelete={onGroupDelete}
                variant={variant}
                eventRefetch={eventRefetch}
            />
        );
    }

    function toggle() {
        setIsExpanded(!isExpanded);
    }

    const groupName = generateGroupName(name);
    const expandRowElClassName = `b-table__row b-related-object-table-row-group ${isExpanded ? "_expanded" : ""}`;

    function onGroupDelete() {
        onDelete(items.map(({ id }) => id));
    }

    return (
        <>
            <tr className={expandRowElClassName}>
                <td className="b-table__cell">
                    <div className="flex align-center _cursor-pointer" onClick={toggle}>
                        <Icon icon={name} className="b-related-object-table-row-group__icon" />
                        <span className="_font-bold">{groupName}</span>
                        <BuildingRelatedObjectTableRowsAmount amount={items.length} variant="white" />
                        {!isExpanded && <div className="b-related-object-table-row-group__collapse" />}
                    </div>
                </td>
                <td className="b-table__cell b-related-object-table__actions-cell" colSpan={variant === "combined" ? 2 : 1}>
                    <div className="flex justify-between align-center">
                        <div className="b-related-object-table-row__actions flex align-center">
                            {/* <CheckboxSwitch /> */}
                            <UserRoleCheck
                                availableForRoles={[
                                    "owner_administrator",
                                    "manufacturer_administrator",
                                    // "manufacturer_brand_manager",
                                    // "manufacturer_object_manager",
                                    "property_manager_administrator",
                                    // "property_manager_building_manager",
                                    "service_provider_administrator",
                                    // "service_provider_service_manager",
                                ]}
                            >
                                {/* <RemoveIconButton onClick={onGroupDelete} /> */}
                            </UserRoleCheck>
                        </div>
                    </div>
                </td>
            </tr>
            {isExpanded &&
                items.map((item) => (
                    <BuildingRelatedObjectTableRowExpanded
                        {...item}
                        key={item.id}
                        onDelete={onDelete}
                        onEditClick={onEditClick}
                        expanded
                        variant={variant}
                        relationRole={relationRole}
                    />
                ))}
        </>
    );
};

const BuildingRelatedObjectTableRowsAmount: FunctionComponent<{
    amount: number;
    variant?: "white";
}> = ({ amount, variant }) => {
    let className = "b-related-object-table__amount h5 _text-grey flex align-center justify-center";

    if (variant === "white") {
        className += " _white";
    }

    return <span className={className}>{amount}</span>;
};
