import React, { FunctionComponent } from "react";
import { Icon } from "../ui/icons";
import "./PersonName.scss";

interface IPersonNameProps {
    name: string;
    type: string;
    id?: string;
    typeLabel?: string;
}

export const PersonName: FunctionComponent<IPersonNameProps> = ({ name, type, id }) => {
    const nameElClassName = `_font-bold ${type === "company" ? "_text-primary" : "_text-black"} b-person-name__name`;

    return (
        <div className="flex">
            <Icon icon={type} className="b-person-name__type-icon _small" />
            <div className="flex flex-column justify-center">
                <p className={nameElClassName} title={name}>
                    {name}
                </p>
            </div>
        </div>
    );
};
