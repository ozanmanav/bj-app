import React, { FunctionComponent } from "react";
import { Icon } from "../../ui/icons";

export interface EventContentHeaderProps {
    title?: string;
}

export const TaskContentHeader: FunctionComponent<EventContentHeaderProps> = ({ title }) => {
    return (
        <div className="flex align-center b-content-header _not-last-group">
            <Icon icon="house" className="b-content-header__icon" />
            <div className="">
                <p className="h4 _text-grey _text-uppercase _font-bold">Add Task</p>
            </div>
        </div>
    );
};
