import React, { FunctionComponent } from "react";
import { Image } from "../../ui/images";
import { getImageURL } from "../../../utils";
import { useModal } from "../../ui/modal";
import { UploadLogoModal } from "../../modals";
import { Icon } from "../../ui/icons";

interface IContentHeaderLogoOuterProps {
    logoUrl: string;
    refetch: Function;
    entityId: string;
}

export const ContentHeaderLogoOuter: FunctionComponent<IContentHeaderLogoOuterProps> = ({ logoUrl, refetch, entityId }) => {
    const { isOpen: imageUploadModalIsOpen, hide: imageUploadModalHide, open: imageUploadModalOpen } = useModal();

    return (
        <div className="b-content-header__logo-outer">
            <Image src={getImageURL(logoUrl)} alt="" className="b-content-header__logo" />
            <button className="b-content-header__update simple-button" type="button" onClick={imageUploadModalOpen}>
                <Icon icon="arrowTop" className="b-content-header__update-img" />
            </button>
            <UploadLogoModal refetch={refetch} entityId={entityId} hide={imageUploadModalHide} isOpen={imageUploadModalIsOpen} />
        </div>
    );
};
