import gql from "graphql-tag";

export const UPDATE_NAME = {
    object: gql`
        mutation UpdateObjectName($id: String!, $name: String) {
            updateObject(id: $id, name: $name) {
                id
            }
        }
    `,
    organization: gql`
        mutation UpdateOrganizationName($id: String!, $name: String) {
            organization(id: $id, name: $name) {
                id
                name
            }
        }
    `,
};
