import React, { FunctionComponent } from "react";
import "./ContentHeaderNameModal.scss";
import { IModalProps, Modal, ModalFooter } from "../../ui/modal";
import { Formik } from "formik";
import { Button } from "../../ui/buttons";
import { Input } from "../../ui/inputs/input";
import * as Yup from "yup";
import { VALIDATION_ERRORS } from "../../../config";
import { useMutation } from "@apollo/react-hooks";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui/toasts";
import { UPDATE_NAME } from "./config";
import { TPageType } from "../../../config/types/Page";

const ContentHeaderNameModalSchema = Yup.object().shape({
    name: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
});

interface IContentHeaderNameModalState {
    name: string;
}

interface IContentHeaderNameModalProps extends IModalProps {
    objectID: string;
    type: TPageType;
    name: string;
    refetch?: () => void;
}

export const ContentHeaderNameModal: FunctionComponent<IContentHeaderNameModalProps> = ({ refetch, objectID, name, type, ...props }) => {
    const [updateObjectName] = useMutation(UPDATE_NAME[type]);

    async function updateName(values: IContentHeaderNameModalState) {
        try {
            const { errors } = await updateObjectName({
                variables: {
                    id: objectID,
                    name: values.name,
                },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                await (refetch && refetch());
                props.hide();
                showSuccessToast("Name is successfully updated.");
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    return (
        <Modal {...props}>
            <Formik
                onSubmit={updateName}
                initialValues={{ name }}
                validationSchema={ContentHeaderNameModalSchema}
                render={({ values, errors, touched, handleChange, handleBlur, handleSubmit }) => (
                    <form onSubmit={handleSubmit}>
                        <h2 className="h1 b-content-header-name-modal__title">Edit name</h2>
                        <Input
                            name="name"
                            placeholder="Object name"
                            value={values.name}
                            error={errors && errors.name}
                            touched={touched && touched.name}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            marginBottom="none"
                        />
                        <ModalFooter>
                            <Button text="Save" primary type="submit" />
                        </ModalFooter>
                    </form>
                )}
            />
        </Modal>
    );
};
