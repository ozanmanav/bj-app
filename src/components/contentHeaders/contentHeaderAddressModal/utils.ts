import { ApolloClient } from "apollo-client";
import { IMetafield } from "../../../config/types";
import gql from "graphql-tag";
import get from "lodash.get";

const GET_OBJECT_DETAILS = gql`
    query GetObjectDetails($id: String!) {
        details(model: OBJECT, model_id: $id) {
            data {
                type
                name
                value
                context
                meta {
                    author_id
                    author {
                        id
                        name
                    }
                    source
                    source_url
                    source_date
                    verification
                    verification_by
                    verifications {
                        verifier_id
                        verifier {
                            name
                        }
                        verified_date
                    }
                    modifications {
                        modifier_id
                        modifier {
                            name
                        }
                        modified_date
                    }
                }
                created_at
                updated_at
                highlighted
            }
        }
    }
`;

export async function mergeBuildingDetails(
    apolloClient: ApolloClient<any>,
    objectID: string,
    addressDetails: IMetafield[]
): Promise<IMetafield[]> {
    const { data } = await apolloClient.query({
        query: GET_OBJECT_DETAILS,
        variables: {
            id: objectID,
        },
    });

    const details = get(data, "data.details.data") || [];

    return addressDetails.reduce((acc: IMetafield[], detail) => {
        const currentDetailIndex = acc.findIndex(({ name }) => name === detail.name);

        if (currentDetailIndex !== -1) {
            acc[currentDetailIndex] = detail;
        } else {
            acc.push(detail);
        }

        return acc;
    }, details);
}
