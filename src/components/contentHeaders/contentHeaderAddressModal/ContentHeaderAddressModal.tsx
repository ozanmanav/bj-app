import React, { FunctionComponent, useState } from "react";
import "./ContentHeaderAddressModal.scss";
import { Modal, IModalProps, Icon, Button, Input, showErrorToast, handleGraphQLErrors } from "../../ui";
import { MapsBlock } from "../../maps";
import { Formik, FormikActions, FormikProps } from "formik";
import { INVALID_BUILDING_ADDRESS_ERROR } from "../../maps/config";
import { IInfoRowParams } from "../../forms/dynamicInfoRows";
import * as Yup from "yup";
import { VALIDATION_ERRORS } from "../../../config";
import classnames from "classnames";
import gql from "graphql-tag";
import { /* useApolloClient */ useMutation } from "@apollo/react-hooks";
import { normalizeAddressParameters /* normalizeDetails */ } from "../../../utils/";
import { SwissMapAutocomplete } from "../../maps/swissMapAutocomplete";
import { ISwissMapFeatureInfo, useSwissMapAutocomplete } from "../../../hooks";
import { formatAddress } from "../../maps/utils";
// import { mergeBuildingDetails } from "./utils";
// import { UPDATE_DETAIL } from "../../object/objectDetailsTable/queries";

const ContentHeaderAddressModalSchema = Yup.object().shape({
    name: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    address: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
});

interface IContentHeaderAddressModalSharedProps {
    name: string;
    address: string;
    isBuilding?: boolean;
}

interface IContentHeaderAddressModalProps extends IModalProps, IContentHeaderAddressModalSharedProps {
    objectID: string;
    objectType: string;
    refetch: Function;
}

// TODO - Details update deleted here waiting BE
const UPDATE_BUILDING = gql`
    mutation UpdateBuilding($id: String!, $name: String!, $address: String!, $address_parameters: UpdateObjectAddressParameters) {
        updateObject(id: $id, name: $name, address: $address, address_parameters: $address_parameters) {
            id
            name
            address
        }
    }
`;

export const ContentHeaderAddressModal: FunctionComponent<IContentHeaderAddressModalProps> = ({
    name,
    address,
    objectID,
    objectType,
    refetch,
    isBuilding,
    ...props
}) => {
    const [isEditing, setIsEditing] = useState<boolean>(false);

    // const apolloClient = useApolloClient();

    const [updateBuilding] = useMutation(UPDATE_BUILDING);

    async function onEdit(
        values: IContentHeaderAddressModalSharedProps & { addressDetails?: IInfoRowParams[] },
        formActions: FormikActions<IContentHeaderAddressModalSharedProps>
    ) {
        try {
            // TODO: Waiting BE changes details
            // const mergedDetails = await mergeBuildingDetails(apolloClient, objectID, values.addressDetails || []);

            const { errors } = await updateBuilding({
                variables: {
                    id: objectID,
                    name: values.name,
                    address: values.address,
                    address_parameters: normalizeAddressParameters(values.addressDetails),
                    // details: normalizeDetails(mergedDetails),
                    // type: objectType,
                },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                await refetch();
                formActions.resetForm();
                setIsEditing(false);
            }
        } catch (e) {
            showErrorToast(e.message);
        } finally {
            formActions.setSubmitting(false);
        }
    }

    return (
        <Modal {...props} className="b-content-header-address-modal">
            <Formik
                onSubmit={onEdit}
                initialValues={{ name, address }}
                validationSchema={ContentHeaderAddressModalSchema}
                render={(formikProps) => (
                    <ContentHeaderAddressModalContent
                        name={name}
                        address={address}
                        isEditing={isEditing}
                        setIsEditing={setIsEditing}
                        isBuilding={isBuilding}
                        {...formikProps}
                    />
                )}
            />
        </Modal>
    );
};

interface IContentHeaderAddressModalContent
    extends FormikProps<IContentHeaderAddressModalSharedProps>,
        IContentHeaderAddressModalSharedProps {
    isEditing: boolean;
    setIsEditing: (isEditing: boolean) => void;
}

const ContentHeaderAddressModalContent: FunctionComponent<IContentHeaderAddressModalContent> = ({
    handleSubmit,
    handleChange,
    handleBlur,
    values,
    errors,
    touched,
    resetForm,
    setFieldValue,
    isSubmitting,
    address,
    name,
    isEditing,
    setIsEditing,
    isBuilding,
}) => {
    const [isAddressValid, setIsAddressValid] = useState<boolean>(true);
    const [selectedFeature, setSelectedFeature] = useState<ISwissMapFeatureInfo>();

    const searchResult = useSwissMapAutocomplete(values.address);

    function showEditForm() {
        setIsEditing(true);
    }

    function hideEditForm() {
        setIsEditing(false);
    }

    function onEditCancel() {
        hideEditForm();
        resetForm();
    }

    function onAddressChange(feature: ISwissMapFeatureInfo) {
        setFieldValue("address", formatAddress(feature.label));
        setSelectedFeature(feature);
    }

    const googleMapsLink = encodeURI(`https://maps.google.com/maps/search/?api=1&query=${address}`);
    const swissMapsLink = encodeURI(`https://map.geo.admin.ch/?swisssearch=${address}`);

    const addressError = [errors.address, !isAddressValid ? INVALID_BUILDING_ADDRESS_ERROR : ""].filter(Boolean).join(". ");

    const wrapperClassname = classnames([
        "flex align-center justify-between b-content-header-address-modal__info",
        { _editing: isEditing },
    ]);

    return (
        <>
            <div className={wrapperClassname}>
                {isEditing ? (
                    <form onSubmit={handleSubmit} className="b-content-header-address-modal__form flex align-center justify-between">
                        <div className="b-content-header-address-modal__form-inputs">
                            <Input
                                placeholder="Name"
                                name="name"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                value={values.name}
                                error={errors.name}
                                touched={touched.name}
                                className="b-content-header-address-modal__input"
                            />
                            <SwissMapAutocomplete
                                results={searchResult}
                                onChange={onAddressChange}
                                className="b-content-header-address-modal__autocomplete"
                            >
                                <Input
                                    placeholder="Address"
                                    name="address"
                                    onBlur={handleBlur}
                                    onChange={handleChange}
                                    value={values.address}
                                    error={addressError}
                                    touched={touched.address}
                                    marginBottom="none"
                                    className="b-content-header-address-modal__input"
                                />
                            </SwissMapAutocomplete>
                        </div>
                        <div className="b-content-header-address-modal__form-actions">
                            <Button text="Cancel" onClick={onEditCancel} disabled={isSubmitting} />
                            <Button text="Save" type="submit" primary disabled={isSubmitting} />
                        </div>
                    </form>
                ) : (
                    <>
                        <div>
                            <h2 className="h1 b-content-header-address-modal__object-name">{name}</h2>
                            <p className="">
                                <Icon icon="pinGrey" className="b-content-header-address-modal__pin" />
                                {address}
                            </p>
                        </div>
                        {isBuilding && (
                            <Button icon="editGrey" text="Edit" className="b-content-header-address-modal__edit" onClick={showEditForm} />
                        )}
                    </>
                )}
            </div>
            <MapsBlock
                defaultAddress={address}
                setFieldValue={setFieldValue}
                setIsAddressValid={setIsAddressValid}
                feature={selectedFeature}
            />
            <div className="flex justify-around b-content-header-address-modal__links">
                <a href={googleMapsLink} target="_blank" rel="noopener noreferrer" className="_font-bold _text-primary">
                    Open on Google Maps
                </a>
                <a href={swissMapsLink} target="_blank" rel="noopener noreferrer" className="_font-bold _text-primary">
                    Open on SWISS Geo
                </a>
            </div>
        </>
    );
};
