export const VERTICAL_HIGHLIGHTS_BREAKPOINT = 1;

export interface BaseContentHeaderProps {
    objectName: string;
}
