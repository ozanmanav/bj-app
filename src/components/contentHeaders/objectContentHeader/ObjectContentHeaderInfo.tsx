import React, { FunctionComponent, useState, useEffect } from "react";
import { useModal } from "../../ui/modal";
import { Container } from "../../ui/grid";
import { Icon } from "../../ui/icons";
import { Link } from "react-router-dom";
import { getImageURL, getObjectLink } from "../../../utils";
import { Image } from "../../ui/images";
import { IMetafield } from "../../../config/types";
import { ContentHeaderAddressModal } from "../contentHeaderAddressModal";
import { ContentHeaderImageModal } from "../contentHeaderImageModal";
import get from "lodash.get";
import { EditButton } from "../../ui/buttons";
import { ContentHeaderNameModal } from "../contentHeaderNameModal";
import classNames from "classnames";
import { VERTICAL_HIGHLIGHTS_BREAKPOINT } from "../config";

interface IParentObject {
    id: string;
    type: string;
    name: string;
    address: string;
    is_building: boolean;
}

interface ISubHeaderObjectInfo {
    address: string;
    isBuilding: boolean;
    redirectLink: string;
    name: string;
    openAddressModal: () => void;
}

const SubHeaderObjectInfo: FunctionComponent<ISubHeaderObjectInfo> = ({ redirectLink, openAddressModal, address, name, isBuilding }) => {
    return (
        <div className="b-content-header__building-icon-container">
            <Link to={redirectLink} className="_text-primary _font-bold">
                <Icon icon="building" className="b-content-header__building-icon" width={24} height={24} />
            </Link>

            {address === name ? (
                <>
                    {isBuilding && address && (
                        <button className="h5 _text-primary _font-bold b-content-header__address-button" onClick={openAddressModal}>
                            <Icon icon="pin" className="b-content-header__address-icon" />
                        </button>
                    )}
                    <div className="b-content-header__address-text">{name}</div>
                </>
            ) : (
                <>
                    <Link to={redirectLink} className="_text-primary _font-bold">
                        {name}
                    </Link>
                    {isBuilding && address && (
                        <div className="b-content-header__address-span">
                            (
                            <button className="h5 _text-primary _font-bold b-content-header__address-button" onClick={openAddressModal}>
                                <Icon icon="pin" className="b-content-header__address-icon" />
                                {address}
                            </button>
                            )
                        </div>
                    )}
                </>
            )}
        </div>
    );
};

export const ObjectContentHeaderInfo: FunctionComponent<{ objectData: any; refetch: () => void }> = ({ objectData, refetch }) => {
    const [modalAddress, setModalAddress] = useState<string>("");
    const { isOpen: isAddressModalOpen, open: openAddressModal, hide: hideAddressModal } = useModal();
    const { isOpen: isImageModalOpen, open: openImageModal, hide: hideImageModal } = useModal();
    const { isOpen: isNameModalOpen, open: openNameModal, hide: hideNameModal } = useModal();

    const name = get(objectData, "objects.data[0].name");
    const objectID = get(objectData, "objects.data[0].id");
    const isBuilding = get(objectData, "objects.data[0].is_building");
    const primaryDetails = (get(objectData, "details.data") || []).filter(({ highlighted }: IMetafield) => highlighted);
    const type = get(objectData, "objects.data[0].type");
    const objectImage = get(objectData, "objects.data[0].logo.url");

    const address = get(objectData, "objects.data[0].address");
    const addressReferences = get(objectData, "objects.data[0].address_references") || [];

    const highlightsClassName = classNames([
        "b-content-header__highlights flex align-center justify-center",
        { _vertical: primaryDetails.length > VERTICAL_HIGHLIGHTS_BREAKPOINT },
    ]);

    useEffect(() => {
        if (address) {
            setModalAddress(address);
        }
        return () => {
            setModalAddress("");
        };
    }, [address]);

    const onClickOpenAddressModal = async (address: string) => {
        await setModalAddress(address);

        openAddressModal();
    };

    return (
        <>
            <Container className="flex align-center">
                {/* TODO: update icon */}
                <Icon icon="building" className="b-content-header__icon" />
                <div className="flex flex-column align-start">
                    <h2 className="b-content-header__name h1 flex align-center" title={name}>
                        <span className="b-content-header__name-wrapper">{name}</span>
                        <EditButton onClick={openNameModal} className="b-content-header__name-edit" />
                    </h2>
                    {isBuilding ? (
                        address && (
                            <button
                                className="h5 _text-primary _font-bold b-content-header__address-button"
                                onClick={() => onClickOpenAddressModal(address)}
                            >
                                <Icon icon="pin" className="b-content-header__address-icon" />
                                {address}
                            </button>
                        )
                    ) : (
                        <div className="b-content-header__parent-links">
                            {addressReferences.map((parentObject: IParentObject) => {
                                const { id, name, address, is_building } = parentObject;

                                return (
                                    <SubHeaderObjectInfo
                                        key={id}
                                        name={name}
                                        address={address}
                                        isBuilding={is_building}
                                        openAddressModal={() => onClickOpenAddressModal(address)}
                                        redirectLink={getObjectLink(type, id)}
                                    />
                                );
                            })}
                        </div>
                    )}
                </div>
                {objectImage ? (
                    <Image
                        src={getImageURL(objectImage)}
                        alt="Generalagentur"
                        className="b-content-header__image"
                        onClick={openImageModal}
                    />
                ) : (
                    <button className="b-content-header__image _button" onClick={openImageModal}>
                        Upload image
                    </button>
                )}
                <div className={highlightsClassName}>
                    {primaryDetails && primaryDetails.length > 0 ? (
                        primaryDetails.map(({ name, value, highlighted }: IMetafield) => {
                            if (highlighted) {
                                return (
                                    <p className="b-content-header__highlight" title={value} key={name + value}>
                                        <span className="_font-bold _text-capitalize">{name}:</span> {value}
                                    </p>
                                );
                            } else {
                                return null;
                            }
                        })
                    ) : (
                        <p className="b-content-header__highlight">No highlighted fields. You can select them on the details screen.</p>
                    )}
                </div>
            </Container>
            <ContentHeaderAddressModal
                isOpen={isAddressModalOpen}
                hide={hideAddressModal}
                name={name}
                address={modalAddress}
                objectID={objectID}
                objectType={type}
                refetch={refetch}
                isBuilding={isBuilding}
            />
            <ContentHeaderImageModal objectID={objectID} refetch={refetch} hide={hideImageModal} isOpen={isImageModalOpen} type="object" />
            <ContentHeaderNameModal
                refetch={refetch}
                objectID={objectID}
                name={name}
                hide={hideNameModal}
                isOpen={isNameModalOpen}
                type="object"
            />
        </>
    );
};
