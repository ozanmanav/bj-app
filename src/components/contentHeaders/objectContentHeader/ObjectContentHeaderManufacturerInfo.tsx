import React, { FunctionComponent } from "react";
import { Container } from "../../ui/grid";
import { Image } from "../../ui/images";
import manufacturerImage from "./manufacturer.png";

export const ObjectContentHeaderManufacturerInfo: FunctionComponent = () => {
    return (
        <Container className="flex align-center">
            <Image src={manufacturerImage} alt="manufacturer logo" className="b-content-header__manufacturer-logo" />
            <div className="flex flex-column align-start">
                <h2 className="b-content-header__name h1">Novartis</h2>
                <p>
                    <a href="mailto:example@company.com" className="_text-primary _font-bold">
                        example@company.com
                    </a>
                    <span className="b-content-header__contacts-separator _text-grey">|</span>
                    <a href="tel:+41791234567" className="_text-black">
                        +41 79 123 45 67
                    </a>
                </p>
            </div>
        </Container>
    );
};
