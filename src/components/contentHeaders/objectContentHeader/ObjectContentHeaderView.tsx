import React, { FunctionComponent } from "react";
import { Container } from "../../ui/grid";
import { CustomNavLink } from "../../ui/links";
import { DetailsIcon, FinancialsIcon, InstallationsIcon, OrganizationsIcon, RelatedIcon, SettingsIcon } from "../ContentHeaderIcons";
import { UserRoleCheck } from "../../userRoleCheck";
import { RouteComponentProps, Redirect } from "react-router";
import { BaseContentHeaderProps } from "../config";
import { ObjectContentHeaderManufacturerInfo } from "./ObjectContentHeaderManufacturerInfo";
import { ObjectContentHeaderInfo } from "./ObjectContentHeaderInfo";
import { useQuery } from "@apollo/react-hooks";
import { GET_CONTENT_HEADER_OBJECT } from "./queries";
import { GRAPHQL_ADMIN_CONTEXT } from "../../../config";
import get from "lodash.get";
import { handleGraphQLErrors } from "../../ui";

interface ObjectContentHeaderProps extends BaseContentHeaderProps, RouteComponentProps<{ id: string }> {}

export const ObjectContentHeaderView: FunctionComponent<ObjectContentHeaderProps> = ({ match, location }) => {
    const objectID = match.params.id;

    const { data: contentHeaderObjectData, refetch: objectRefetch, error: objectError } = useQuery(GET_CONTENT_HEADER_OBJECT, {
        variables: {
            id: objectID,
        },
        ...GRAPHQL_ADMIN_CONTEXT,
    });

    if (objectError) {
        handleGraphQLErrors(objectError.graphQLErrors);
        return <Redirect to="/app/cockpit" />;
    }

    const isBuilding = get(contentHeaderObjectData, "objects.data[0].is_building");

    if (isBuilding && location.pathname.includes("object")) {
        return <Redirect to={`/app/building/${objectID}`} />;
    }

    const linkPrefix = `/app/${isBuilding ? "building" : "object"}`;
    const shouldShowManufacturerHeader = location.pathname.includes("installations") && location.search.includes("manufacturer");

    return (
        <div className="b-content-header _building">
            <div className="b-content-header__building-info-row">
                {shouldShowManufacturerHeader ? (
                    <ObjectContentHeaderManufacturerInfo />
                ) : (
                    <ObjectContentHeaderInfo objectData={contentHeaderObjectData} refetch={objectRefetch} />
                )}
            </div>
            <nav className="b-content-header__nav">
                <Container className="flex justify-between align-center">
                    <CustomNavLink to={`${linkPrefix}/${objectID}`} exact navLink className="b-content-header__nav-link">
                        <DetailsIcon />
                        Details
                    </CustomNavLink>
                    <UserRoleCheck
                        availableForRoles={["manufacturer_administrator", "manufacturer_brand_manager", "manufacturer_object_manager"]}
                    >
                        <CustomNavLink to={`${linkPrefix}/${objectID}/installations`} exact navLink className="b-content-header__nav-link">
                            <InstallationsIcon />
                            Installations
                        </CustomNavLink>
                    </UserRoleCheck>
                    <CustomNavLink to={`${linkPrefix}/${objectID}/related`} navLink className="b-content-header__nav-link">
                        <RelatedIcon />
                        Related objects
                    </CustomNavLink>
                    <CustomNavLink to={`${linkPrefix}/${objectID}/organizations`} navLink className="b-content-header__nav-link">
                        <OrganizationsIcon />
                        Organizations
                    </CustomNavLink>
                    <CustomNavLink to={`${linkPrefix}/${objectID}/financials`} navLink className="b-content-header__nav-link" disabled>
                        <FinancialsIcon />
                        Financials
                    </CustomNavLink>
                    <UserRoleCheck
                        availableForRoles={[
                            "owner_administrator",
                            "manufacturer_administrator",
                            "manufacturer_brand_manager",
                            "manufacturer_object_manager",
                            "property_manager_administrator",
                            "property_manager_building_manager",
                            "service_provider_administrator",
                        ]}
                    >
                        <CustomNavLink to={`${linkPrefix}/${objectID}/admin`} navLink className="b-content-header__nav-link">
                            <SettingsIcon />
                            Admin
                        </CustomNavLink>
                    </UserRoleCheck>
                </Container>
            </nav>
        </div>
    );
};
