import gql from "graphql-tag";

export const GET_CONTENT_HEADER_OBJECT = gql`
    query GetContentHeaderObject($id: String!) {
        objects(id: $id, details: { highlighted: true }) {
            data {
                id
                name
                address
                address_references {
                    id
                    name
                    is_building
                    type {
                        id
                    }
                    address
                }
                is_building
                type {
                    id
                }
                logo {
                    url
                }
                higherRelations {
                    id
                    type {
                        id
                        name
                    }
                    higher {
                        id
                        name
                        is_building
                        type {
                            id
                        }
                        address
                    }
                }
            }
        }
        details(model: OBJECT, model_id: $id) {
            data {
                value
                name
                highlighted
            }
        }
    }
`;
