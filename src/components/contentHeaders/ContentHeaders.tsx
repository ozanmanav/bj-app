import React, { FunctionComponent } from "react";
import "./ContentHeaders.scss";
import { Switch, Route } from "react-router-dom";
import { NewObjectContentHeader } from "./newObjectContentHeader";
import { ObjectContentHeaderView } from "./objectContentHeader";
import { OrganizationContentHeader } from "./organizationContentHeader";

export const ContentHeader: FunctionComponent = () => {
    return (
        <Switch>
            <Route path={["/app/building/add", "/app/object/add"]} component={NewObjectContentHeader} />
            {/* hiding other headers on this route */}
            <Route path="/app/object/:objectID/organization/:organizationID/successor" />
            <Route path={["/app/building/:id/", "/app/object/:id/"]} component={ObjectContentHeaderView} />
            <Route
                path={[
                    "/app/organizations/client/:id/",
                    "/app/admin/organizations/client/:id/",
                    "/app/admin/organizations/:id/",
                    "/app/organizations/:id/",
                ]}
                component={OrganizationContentHeader}
            />
        </Switch>
    );
};
