import React, { FunctionComponent } from "react";
import { Icon } from "../../ui/icons";

export interface EventContentHeaderProps {
    title?: string;
    subtitle?: string;
    headerRightInfo?: string;
    isTask?: boolean;
}

export const EventContentHeader: FunctionComponent<EventContentHeaderProps> = ({ title, subtitle, headerRightInfo, isTask }) => {
    return (
        <div className="flex align-center b-content-header _not-last-group">
            <Icon icon="house" className="b-content-header__icon" />
            {title ? (
                <div className="">
                    <p className="h6 _text-grey _text-uppercase _font-bold">Add event to:</p>
                    <h3 className="">{title}</h3>
                    <p className="h6 _text-grey _text-uppercase _font-bold">{subtitle}</p>
                </div>
            ) : (
                <div className="">
                    <p className="h4 _text-grey _text-uppercase _font-bold">Add {`${isTask ? "task" : "event"}`}</p>
                </div>
            )}
            <p className="h4 _text-grey _text-uppercase _font-bold b-content-header__header-right"> {headerRightInfo}</p>
        </div>
    );
};
