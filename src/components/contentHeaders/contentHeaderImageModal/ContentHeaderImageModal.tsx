import React, { FunctionComponent, useState } from "react";
import { IModalProps, Modal } from "../../ui/modal";
import { UploadImageForm } from "../../forms/uploadLogoForm";
import { useMutation } from "@apollo/react-hooks";
import { handleGraphQLErrors, showErrorToast } from "../../ui/toasts";
import { GET_OBJECT_FILES } from "../../../views/app/object/objectDetails/ObjectDetails";
import { UPDATE_LOGO } from "./config";
import { TPageType } from "../../../config/types/Page";
import { GET_DETAILS } from "../../object/objectDetailsTable/queries";

interface IContentHeaderImageModalProps extends IModalProps {
    objectID: string;
    refetch?: () => void;
    type: TPageType;
}

export const ContentHeaderImageModal: FunctionComponent<IContentHeaderImageModalProps> = ({ objectID, refetch, type, ...props }) => {
    const [updateLogo] = useMutation(UPDATE_LOGO[type]);
    const [loading, setLoading] = useState(false);

    async function onUpload(imageID: string) {
        try {
            setLoading(true);

            const { errors } = await updateLogo({
                variables: {
                    id: objectID,
                    logo_id: imageID,
                },
                refetchQueries:
                    type === "object"
                        ? [
                              {
                                  query: GET_OBJECT_FILES,
                                  variables: {
                                      id: objectID,
                                  },
                              },
                              {
                                  query: GET_DETAILS,
                                  variables: {
                                      model: "OBJECT",
                                      model_id: objectID,
                                  },
                              },
                          ]
                        : [],
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                await (refetch && refetch());
                props.hide();
            }
        } catch (e) {
            showErrorToast(e.message);
        } finally {
            setLoading(false);
        }
    }

    return (
        <Modal {...props}>
            <UploadImageForm
                loading={loading}
                callback={onUpload}
                title="Update image"
                objectID={type === "object" ? objectID : undefined}
            />
        </Modal>
    );
};
