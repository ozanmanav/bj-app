import gql from "graphql-tag";

export const UPDATE_LOGO = {
    object: gql`
        mutation UpdateObjectLogo($id: String!, $logo_id: String!) {
            updateObject(id: $id, logo_id: $logo_id) {
                id
            }
        }
    `,
    organization: gql`
        mutation UpdateOrganizationLogo($id: String!, $logo_id: String!) {
            organization(id: $id, logo_id: $logo_id) {
                id
                logo {
                    url
                }
            }
        }
    `,
};
