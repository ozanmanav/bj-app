import React, { FunctionComponent } from "react";
import { RouteComponentProps } from "react-router";
import { isBuildingRoute } from "../../../utils/isBuildingRoute";
import { Container } from "../../ui/grid";
import { Icon } from "../../ui/icons";
import { BaseContentHeaderProps } from "../config";

export const NewObjectContentHeader: FunctionComponent<BaseContentHeaderProps & RouteComponentProps> = ({
    objectName = null,
    location,
}) => {
    const isBuilding = isBuildingRoute(location);
    const renderedObjectName = objectName || `New ${isBuilding ? "building" : "object"}`;

    return (
        <Container className="flex align-center b-content-header _new-object">
            <Icon icon="house" className="b-content-header__icon" />
            <div className="">
                <h2 className="h1">{renderedObjectName}</h2>
            </div>
        </Container>
    );
};
