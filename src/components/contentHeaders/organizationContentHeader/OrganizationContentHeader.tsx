import React, { FunctionComponent } from "react";
import { RouteComponentProps } from "react-router-dom";
import { Container } from "../../ui/grid";
import { CustomNavLink } from "../../ui/links";
import {
    DetailsIcon,
    FinancialsIcon,
    InstallationsIcon,
    OrganizationsIcon,
    RelatedIcon,
    SettingsIcon,
    StructureIcon,
} from "../ContentHeaderIcons";
import { OrganizationContentHeaderInfo } from "./OrganizationContentHeaderInfo";
import { useQuery, useApolloClient } from "@apollo/react-hooks";
import { ORGANIZATION_HEADER_QUERY } from "./config";
import get from "lodash.get";
import { IMetafield } from "../../../config/types";
import { GRAPHQL_ADMIN_CONTEXT } from "../../../config";
import { GET_ORGANIZATION_FILES } from "../../../views/app/adminPanel/adminOrganization/adminOrganizationDetails/queries";
import { isClientOrganizationRoute } from "../../../utils/isCompanyRoute";

interface IOrganizationContentHeaderProps extends RouteComponentProps<{ id: string }> {
    refetchFiles: () => void;
}

export const OrganizationContentHeader: FunctionComponent<IOrganizationContentHeaderProps> = ({ location, match }) => {
    const organizationID = match.params.id;
    const isClientOrganization = isClientOrganizationRoute(location);
    const apolloClient = useApolloClient();
    const linkPrefix = `/app/${location.pathname.includes("/admin/") ? "admin/" : ""}organizations${isClientOrganization ? "/client" : ""}`;

    const { data, refetch, loading } = useQuery(ORGANIZATION_HEADER_QUERY, {
        variables: {
            id: organizationID,
        },
    });

    const onCallbackHeaderInfo = () => {
        apolloClient
            .query({
                query: GET_ORGANIZATION_FILES,
                variables: {
                    id: organizationID,
                },
                ...GRAPHQL_ADMIN_CONTEXT,
                fetchPolicy: "network-only",
            })
            .then(() => refetch());
    };

    const dataPrefix = "organizations";

    const name = get(data, `${dataPrefix}.data[0].name`);
    const isSuspended = get(data, `${dataPrefix}.data[0].suspend`);
    const logo = get(data, `${dataPrefix}.data[0].logo.url`);
    const organizationType = get(data, `${dataPrefix}.data[0].type.type`);

    // TODO: update
    const address = "";
    const primaryDetails: IMetafield[] = (get(data, "details.data") || []).filter(({ highlighted }: IMetafield) => highlighted);

    return (
        <div className="b-content-header _building">
            <div className="b-content-header__building-info-row">
                <OrganizationContentHeaderInfo
                    isClientOrganization={isClientOrganization}
                    organizationID={organizationID}
                    name={name}
                    logo={logo}
                    address={address}
                    primaryDetails={primaryDetails}
                    refetch={onCallbackHeaderInfo}
                    isSuspended={isSuspended}
                    loading={loading}
                    type={"organization"}
                />
            </div>
            <nav className="b-content-header__nav">
                <Container className="flex justify-between align-center">
                    <CustomNavLink to={`${linkPrefix}/${organizationID}`} exact navLink className="b-content-header__nav-link">
                        <DetailsIcon />
                        Details
                    </CustomNavLink>
                    {organizationType === "manufacturer" && (
                        <CustomNavLink
                            to={`${linkPrefix}/${organizationID}/installations`}
                            exact
                            navLink
                            className="b-content-header__nav-link"
                        >
                            <InstallationsIcon />
                            Installations
                        </CustomNavLink>
                    )}
                    <CustomNavLink to={`${linkPrefix}/${organizationID}/related`} navLink className="b-content-header__nav-link">
                        <RelatedIcon />
                        Related objects
                    </CustomNavLink>
                    <CustomNavLink to={`${linkPrefix}/${organizationID}/people`} navLink className="b-content-header__nav-link">
                        <OrganizationsIcon />
                        People
                    </CustomNavLink>
                    <CustomNavLink to={`${linkPrefix}/${organizationID}/relationships`} navLink className="b-content-header__nav-link">
                        <StructureIcon />
                        Structure
                    </CustomNavLink>
                    {isClientOrganization && (
                        <CustomNavLink to={`${linkPrefix}/${organizationID}/financials`} navLink className="b-content-header__nav-link">
                            <FinancialsIcon />
                            Financials
                        </CustomNavLink>
                    )}

                    <CustomNavLink to={`${linkPrefix}/${organizationID}/admin`} navLink className="b-content-header__nav-link">
                        <SettingsIcon />
                        Admin
                    </CustomNavLink>
                </Container>
            </nav>
        </div>
    );
};
