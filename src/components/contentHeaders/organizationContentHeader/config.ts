import gql from "graphql-tag";

export const ORGANIZATION_HEADER_QUERY = gql`
    query GetOrganizationHeader($id: String!) {
        organizations(id: $id) {
            data {
                name
                suspend
                type {
                    id
                    type
                }
                logo {
                    id
                    url
                }
            }
        }
        details(model: ORGANIZATION, model_id: $id) {
            data {
                value
                name
                highlighted
            }
        }
    }
`;
