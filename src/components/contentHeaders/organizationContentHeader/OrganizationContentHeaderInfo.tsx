import React, { FunctionComponent } from "react";
import { Container } from "../../ui/grid";
import { IMetafield } from "../../../config/types";
import { ContentHeaderLogoOuter } from "../contentHeaderLogoOuter/ContentHeaderLogoOuter";
import { TImageUploadModalType } from "../../modals/uploadLogoModal/UploadLogoModal";
import { withRouter, RouteComponentProps } from "react-router";
import classNames from "classnames";
import { VERTICAL_HIGHLIGHTS_BREAKPOINT } from "../config";

interface IOrganizationContentHeaderInfoProps {
    name: string;
    address: string;
    primaryDetails: IMetafield[];
    logo: string;
    organizationID: string;
    isClientOrganization: boolean;
    refetch: () => void;
    type: TImageUploadModalType;
    isSuspended?: boolean;
    loading?: boolean;
}

const OrganizationContentHeaderInfoBase: FunctionComponent<IOrganizationContentHeaderInfoProps & RouteComponentProps> = ({
    name,
    logo,
    organizationID,
    refetch,
    primaryDetails,
}) => {
    const highlightsClassName = classNames([
        "b-content-header__highlights flex align-center justify-center",
        { _vertical: primaryDetails.length > VERTICAL_HIGHLIGHTS_BREAKPOINT },
    ]);
    return (
        <>
            <Container className="flex align-center">
                {/* TODO: update icon */}
                <ContentHeaderLogoOuter logoUrl={logo} refetch={refetch} entityId={organizationID} />
                <div className="flex flex-column align-start">
                    <h2 className="b-content-header__name h1 flex align-center" title={name}>
                        <span className="b-content-header__name-wrapper">{name}</span>
                    </h2>
                </div>
                <div className={highlightsClassName}>
                    {primaryDetails && primaryDetails.length > 0 ? (
                        primaryDetails.map(({ name, value, highlighted }: IMetafield) => {
                            if (highlighted) {
                                return (
                                    <p className="b-content-header__highlight" title={value} key={name + value}>
                                        <span className="_font-bold _text-capitalize">{name}:</span> {value}
                                    </p>
                                );
                            } else {
                                return null;
                            }
                        })
                    ) : (
                        <p className="b-content-header__highlight">No highlighted fields. You can select them on the details screen.</p>
                    )}
                </div>
            </Container>
        </>
    );
};

export const OrganizationContentHeaderInfo = withRouter(OrganizationContentHeaderInfoBase);
