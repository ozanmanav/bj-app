import React, { FunctionComponent } from "react";
import { Icon } from "../../ui/icons";
import "./EventBlock.scss";

interface IEventBlockProps {
    name: string;
    type: string;
}

export const EventBlock: FunctionComponent<IEventBlockProps> = ({ name, type }) => {
    return (
        <div className="flex">
            <Icon icon={type} className="b-object-name__type-icon" />
            <div className="flex flex-column justify-center">
                <p className="_text-black b-object-name__name">{name}</p>
                <p className="_text-grey h6 b-object-name__type _text-capitalize">{type}</p>
            </div>
        </div>
    );
};
