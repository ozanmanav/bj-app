import React, { FunctionComponent } from "react";
import "./EventsTable.scss";
import { Filter, ObjectName } from "../../ui";
import { EventBlock } from "../EventBlock";

const data = [
    {
        object: {
            name: "Building Zenex",
            type: "building",
        },
        event: {
            name: "Doors open now",
            type: "commissioning",
        },
        id: 1,
        date: "14.14.2019",
    },
    {
        object: {
            name: "Building Zenex",
            type: "building",
        },
        event: {
            name: "Doors open now",
            type: "commissioning",
        },
        id: 2,
        date: "14.14.2019",
    },
    {
        object: {
            name: "Building Zenex",
            type: "building",
        },
        event: {
            name: "Doors open now",
            type: "commissioning",
        },
        id: 3,
        date: "14.14.2019",
    },
    {
        object: {
            name: "Building Zenex",
            type: "building",
        },
        event: {
            name: "Doors open now",
            type: "commissioning",
        },
        id: 4,
        date: "14.14.2019",
    },
];

export const EventsTable: FunctionComponent = () => {
    return (
        <div className="_not-last-group">
            <header className="b-events__header b-group-header flex justify-between">
                <h3 className="flex align-center b-group-header__title">Events </h3>
                <Filter onApply={() => {}} dateStart={new Date().toDateString()} typeDropdown dateDropdown />
            </header>
            <div className="b-table__wrapper">
                {/* TODO: add sorting */}
                <table className="b-table">
                    <thead className="b-table__head">
                        <tr className="b-table__row _head">
                            <th className="b-table__cell">Object name</th>
                            <th className="b-table__cell">Event</th>
                            <th className="b-table__cell">Date</th>
                        </tr>
                    </thead>
                    <tbody className="b-table__body">
                        {data.map(({ object, event, id, date }) => (
                            <tr className="b-table__row" key={id}>
                                <td className="b-table__cell">
                                    <ObjectName {...object} />
                                </td>
                                <td className="b-table__cell">
                                    <EventBlock {...event} />
                                </td>
                                {/* TODO: move it to the left */}
                                <td className="b-table__cell _text-black">{date}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
};
