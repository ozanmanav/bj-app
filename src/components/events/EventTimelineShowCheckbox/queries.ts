import gql from "graphql-tag";

export const UPDATE_EVENT_TIMELINE_STATUS = gql`
    mutation UpdateEventTimelineStatus($id: String!, $timeline: Boolean!) {
        updateEvent(id: $id, timeline: $timeline) {
            id
            name
            timeline
        }
    }
`;
