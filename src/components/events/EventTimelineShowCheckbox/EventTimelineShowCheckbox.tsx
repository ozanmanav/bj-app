import React, { FunctionComponent, useState } from "react";
import "./EventTimelineShowCheckbox.scss";
import { CheckboxSwitch, handleGraphQLErrors, showErrorToast } from "../../ui";
import get from "lodash.get";
import { useMutation } from "@apollo/react-hooks";
import { UPDATE_EVENT_TIMELINE_STATUS } from "./queries";

interface EventTimelineShowCheckboxProps {
    eventID: string;
}

export const EventTimelineShowCheckbox: FunctionComponent<EventTimelineShowCheckboxProps> = ({ eventID }) => {
    const [loading, setLoading] = useState(false);
    const [updateEventTimelineStatusMutation] = useMutation(UPDATE_EVENT_TIMELINE_STATUS);

    const onChangeTextInput = async (event: React.ChangeEvent<HTMLInputElement>) => {
        try {
            setLoading(true);

            const value = get(event, "currentTarget.value");

            const { errors } = await updateEventTimelineStatusMutation({
                variables: {
                    id: eventID,
                    timeline: value,
                },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            }
        } catch (e) {
            showErrorToast(e.message);
        } finally {
            setLoading(false);
        }
    };
    return <CheckboxSwitch onChange={onChangeTextInput} disabled={loading} />;
};
