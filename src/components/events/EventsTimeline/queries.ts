import gql from "graphql-tag";

export const GET_EVENTS = gql`
    query GetEvents($object_id: String, $organization_id: String, $strict: Boolean) {
        events(object_id: $object_id, organization_id: $organization_id, strict: $strict, timeline: true) {
            data {
                id
                name
                type {
                    id
                    name
                }
                frequency {
                    type
                    days
                    months
                    interval
                    count
                }
                related_objects {
                    id
                    name
                }
                related_organizations {
                    id
                    name
                }
                organization {
                    id
                    name
                }
                object {
                    id
                    name
                }
                files {
                    id
                }
                ends_on
                start_date
                end_date
            }
        }
    }
`;
