export type IEventVariant = "object" | "organization" | "contract" | "relationship" | "building" | undefined;

export interface EventsTimelineProps {
    showAddButton?: boolean;
    initialState?: object;
    showStrictSwitcher?: boolean;
    variant?: IEventVariant;
    variantID?: string;
    events: object;
    loading: boolean;
    refetch: Function;
}

export const DATE_FORMAT_STRING = "yyyy/MM/dd";
export const DATE_FORMAT_STRING_TO_EXPORT = "dd/MM/yyyy";
export const DATE_FORMAT_STRING_TO_EXPORT_REVERSE = "yyyy/MM/dd";
