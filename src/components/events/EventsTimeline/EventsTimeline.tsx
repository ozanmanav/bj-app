import React, { FunctionComponent, useReducer, useMemo, useEffect } from "react";
import XlsExport from "xlsexport/xls-export.js";
import get from "lodash.get";
import orderBy from "lodash.orderby";
import { PlusButton, Select, CheckboxSwitch, ExportButton } from "../../ui";
import { IEvent, IEventTimelineExport } from "../../../views/app/event/eventDetail/definitions";
import {
    getAddEventRoute,
    TimeLineTypes,
    isSingleEvent,
    isRepeatableEventWithinInterval,
    getNextOccuranceDate,
    getUpdatedEvent,
} from "./utils";
import { EventsTimelineProps, DATE_FORMAT_STRING, DATE_FORMAT_STRING_TO_EXPORT, DATE_FORMAT_STRING_TO_EXPORT_REVERSE } from "./definitions";
import { TimelineFilter } from "./TimelineFilter/TimelineFilter";
import { Timeline } from "./Timeline";
import { timelineReducer, timelineReducerInitialState, setTimelineType, setStrictSwitch } from "./TimelineReducer";
import { TimelineContextProvider } from "./TimelineContext";
import "./EventsTimeline.scss";

import { format, isBefore, isWithinInterval } from "date-fns";

export const EventsTimeline: FunctionComponent<EventsTimelineProps> = ({
    showAddButton = true,
    variant,
    variantID,
    showStrictSwitcher,
    events: eventsData,
    refetch,
    loading,
    initialState = {},
}) => {
    const [timelineState, timelineDispatch] = useReducer(timelineReducer, { ...timelineReducerInitialState, ...initialState });

    const timelineProviderValue = useMemo(
        () => ({
            timelineState,
            timelineDispatch,
        }),
        [timelineState, timelineDispatch]
    );

    useEffect(() => {
        if (showStrictSwitcher) {
            refetch({
                ...(variant === "object" && { object_id: variantID }),
                ...(variant === "organization" && { organization_id: variantID }),
                strict: timelineState.showStrictEvents,
            });
        }
    }, [timelineState.showStrictEvents]);

    const events = (get(eventsData, "events.data") as IEvent[]) || [];

    const onChangeTimelineType = (event: React.ChangeEvent<HTMLSelectElement>) => {
        timelineDispatch(setTimelineType(event.currentTarget.value));
    };

    const onChangeStrictSwitcher = (event: React.ChangeEvent<HTMLInputElement>) => {
        timelineDispatch(setStrictSwitch(event.currentTarget.checked));
    };

    const exportData = () => {
        if (events.length === 0) return;
        let eventsToExport: IEventTimelineExport[] = [];
        const timelineFilterDates = get(timelineState, "timelineFilterDates");

        // reseting time to 00:00:00
        const timelineInterval = {
            start: new Date(format(timelineFilterDates.startDate, DATE_FORMAT_STRING)),
            end: new Date(format(timelineFilterDates.endDate, DATE_FORMAT_STRING)),
        };

        events.forEach((event) => {
            if (event.frequency && event.frequency.type !== null) {
                let tempEvent = { ...event };
                let { start_date, frequency } = tempEvent;

                // reseting time to 00:00:00
                start_date = format(new Date(start_date), DATE_FORMAT_STRING);
                if (tempEvent.end_date !== null) {
                    tempEvent.end_date = format(new Date(tempEvent.end_date), DATE_FORMAT_STRING);
                } else {
                    tempEvent.end_date = format(new Date(), DATE_FORMAT_STRING);
                }

                if (isSingleEvent(tempEvent)) {
                    // is single occurence and within the chosen interval
                    if (isWithinInterval(new Date(start_date), timelineInterval)) {
                        eventsToExport.push(getUpdatedEvent(tempEvent, start_date, DATE_FORMAT_STRING_TO_EXPORT_REVERSE, variant));
                    }
                } else {
                    // is repeatable event and falls within the chosen interval
                    if (isRepeatableEventWithinInterval(tempEvent, timelineInterval)) {
                        for (
                            let occurenceDate = new Date(start_date), occurrence = 1;
                            (occurenceDate <= new Date(tempEvent.end_date) ||
                                (tempEvent.end_date === null && frequency.count === null) ||
                                (occurrence <= frequency.count && frequency.count > 0)) &&
                            occurenceDate <= timelineInterval.end;
                            occurrence++
                        ) {
                            let tempEventOccurence = { ...event };

                            if (!isBefore(occurenceDate, timelineInterval.start)) {
                                eventsToExport.push(
                                    getUpdatedEvent(tempEventOccurence, occurenceDate, DATE_FORMAT_STRING_TO_EXPORT_REVERSE, variant)
                                );
                            }

                            occurenceDate = getNextOccuranceDate(occurenceDate, event.frequency.type);
                        }
                    }
                }
            }
        });
        eventsToExport = orderBy(eventsToExport, ["start_date", "asc"]);
        eventsToExport.map((e: any) => {
            e.start_date = format(new Date(e.start_date), DATE_FORMAT_STRING_TO_EXPORT);
            return e;
        });

        const instance = new XlsExport(eventsToExport);
        instance.exportToCSV("Events timeline export.csv");
    };

    return (
        <TimelineContextProvider value={timelineProviderValue}>
            <div className="b-events-timeline _not-last-group _no-border">
                <header className="b-events__header b-group-header flex justify-between">
                    <h3 className="flex align-center b-group-header__title">
                        Events
                        {showAddButton && variant && <PlusButton to={getAddEventRoute(variant, variantID)} />}
                    </h3>

                    <div className="flex b-events-timeline__filter align-center">
                        {showStrictSwitcher && (
                            <div className="b-events-timeline__filter-switcher">
                                All Events{" "}
                                <div className="b-events-timeline__filter-switcher-switch">
                                    <CheckboxSwitch checked={timelineState.showStrictEvents} onChange={onChangeStrictSwitcher} />
                                </div>{" "}
                                Object Related Events
                            </div>
                        )}

                        <Select
                            options={TimeLineTypes}
                            value={timelineState.timelineType}
                            onChange={onChangeTimelineType}
                            button
                            placeholder="Timeline Type"
                            className="b-filter__element"
                            marginBottom="none"
                        />
                        <TimelineFilter />
                        {events.length > 0 && <ExportButton text="Export" primary className="_event_export" onClick={exportData} />}
                    </div>
                </header>

                <Timeline events={events} loading={loading} />
            </div>
        </TimelineContextProvider>
    );
};
