import { ITimelineFilterState, TimelineType, ITimelinePickerState } from "./timelineReducer";

export type ITimelineReducerAction =
    | { type: "SET_PICKER_STATE"; timelinePickerState: ITimelinePickerState }
    | { type: "SET_FILTER_DATES"; timelineFilterDates: ITimelineFilterState }
    | { type: "SET_TIMELINE_TYPE"; timelineType: TimelineType }
    | { type: "SET_STRICT_SWITCH"; showStrictEvents: boolean };

export const setPickerState = (timelinePickerState: ITimelinePickerState): ITimelineReducerAction => ({
    type: "SET_PICKER_STATE",
    timelinePickerState,
});

export const setFilterDates = (timelineFilterDates: ITimelineFilterState): ITimelineReducerAction => ({
    type: "SET_FILTER_DATES",
    timelineFilterDates,
});

export const setTimelineType = (timelineType: TimelineType): ITimelineReducerAction => ({
    type: "SET_TIMELINE_TYPE",
    timelineType,
});

export const setStrictSwitch = (showStrictEvents: boolean): ITimelineReducerAction => ({
    type: "SET_STRICT_SWITCH",
    showStrictEvents,
});
