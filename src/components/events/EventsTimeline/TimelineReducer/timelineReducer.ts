import { ITimelineReducerAction } from "./timelineReducerActions";
import { getFilterDatesFromPickerState, getPickerStateFromFilterDates } from "../utils";
import { addMonths, subMonths, addYears } from "date-fns";

export type TimelineType = "oneMonth" | "monthly" | "yearly" | string;

export interface ITimelinePickerState {
    startMonth?: number;
    startYear?: string;
    endMonth?: number;
    endYear?: string;
}

export interface ITimelineFilterState {
    startDate: Date;
    endDate: Date;
}

export interface ITimelineReducerState {
    timelinePickerState: ITimelinePickerState;
    timelineFilterDates: ITimelineFilterState;
    timelineType: TimelineType;
    showStrictEvents: boolean;
}

const initialTimelinePickerState: ITimelinePickerState = {
    startMonth: new Date().getMonth() + 1,
    startYear: new Date().getFullYear().toString(),
    endMonth: new Date().getMonth() + 1,
    endYear: new Date().getFullYear().toString(),
};
const date = new Date();
const initialTimelineFilterDates: ITimelineFilterState = {
    startDate: new Date(date.getFullYear(), date.getMonth(), 1),
    endDate: new Date(date.getFullYear(), date.getMonth() + 1, 0),
};

export const timelineReducerInitialState: ITimelineReducerState = {
    timelinePickerState: initialTimelinePickerState,
    timelineFilterDates: initialTimelineFilterDates,
    timelineType: "oneMonth",
    showStrictEvents: false,
};

export function timelineReducer(state: ITimelineReducerState = timelineReducerInitialState, action: ITimelineReducerAction) {
    switch (action.type) {
        case "SET_PICKER_STATE":
            return {
                ...state,
                timelinePickerState: action.timelinePickerState,
                timelineFilterDates: getFilterDatesFromPickerState(action.timelinePickerState),
            };
        case "SET_FILTER_DATES":
            return {
                ...state,
                timelineFilterDates: action.timelineFilterDates,
                timelinePickerState: getPickerStateFromFilterDates(action.timelineFilterDates),
            };
        case "SET_TIMELINE_TYPE":
            let adjustedTimelineFilterDates: ITimelineFilterState = initialTimelineFilterDates;

            if (action.timelineType === "oneMonth") {
                adjustedTimelineFilterDates = {
                    startDate: new Date(date.getFullYear(), date.getMonth(), 1),
                    endDate: new Date(date.getFullYear(), date.getMonth() + 1, 0),
                };
            }

            if (action.timelineType === "monthly") {
                adjustedTimelineFilterDates = {
                    startDate: subMonths(new Date(date.getFullYear(), date.getMonth(), 1), 4),
                    endDate: addMonths(new Date(date.getFullYear(), date.getMonth() + 1, 0), 1),
                };
            }

            if (action.timelineType === "yearly") {
                adjustedTimelineFilterDates = {
                    startDate: new Date(date.getFullYear(), 1, 1),
                    endDate: addYears(new Date(date.getFullYear(), 0, 1), 1),
                };
            }

            return {
                ...state,
                timelineType: action.timelineType,
                timelineFilterDates: adjustedTimelineFilterDates,
                timelinePickerState: getPickerStateFromFilterDates(adjustedTimelineFilterDates),
            };

        case "SET_STRICT_SWITCH":
            return {
                ...state,
                showStrictEvents: action.showStrictEvents,
            };
        default:
            return state;
    }
}
