import React, { FunctionComponent, useContext } from "react";
import { Button } from "../../../ui";
import { OneMonthPicker, MonthlyPicker, YearlyPicker } from "./TimelinePickers";
import { timelineContext } from "../TimelineContext";
import { setPickerState, ITimelinePickerState } from "../TimelineReducer";

export const TimelineFilter: FunctionComponent = () => {
    const {
        timelineState: { timelineType, timelinePickerState },
        timelineDispatch,
    } = useContext(timelineContext);

    const onPressApply = () => {
        // onApply(getDatesFromMonthsAndYears(selectedDate));
    };

    const onPickerChange = (state: ITimelinePickerState) => {
        timelineDispatch(setPickerState(state));
    };

    return (
        <div className="flex align-center b-filter">
            <div className="flex align-center b-filter__dropdowns-wrapper">
                {(() => {
                    switch (timelineType) {
                        case "oneMonth":
                            return <OneMonthPicker onSubmit={onPickerChange} initialValues={timelinePickerState} />;
                        case "monthly":
                            return <MonthlyPicker onSubmit={onPickerChange} initialValues={timelinePickerState} />;
                        case "yearly":
                            return <YearlyPicker onSubmit={onPickerChange} initialValues={timelinePickerState} />;
                        default:
                            return null;
                    }
                })()}
            </div>

            <Button text="Apply" onClick={onPressApply} primary />
        </div>
    );
};
