import React, { FunctionComponent } from "react";
import styled from "styled-components";
import { Formik, FormikProps } from "formik";
import { YEARS, MONTHS } from "../utils";
import useUpdateEffect from "../../../../hooks/useUpdateEffect";
import { Select } from "../../../ui";
import { ITimelinePickerState } from "../TimelineReducer";

const MonthsContainer = styled.div`
    width: 160px;
`;

const YearsContainer = styled.div`
    margin-left: 10px;
    margin-right: 10px;
    width: 120px;
`;

const Separator = styled.div`
    width: 25px;
`;

const OneMonthPickerBase: FunctionComponent<FormikProps<ITimelinePickerState>> = ({ values, setFieldValue, handleSubmit }) => {
    useUpdateEffect(() => {
        handleSubmit();
    }, [values.startMonth, values.startYear]);

    const onChangeMonth = (event: React.ChangeEvent<HTMLSelectElement>) => {
        const { name, value } = event.currentTarget;
        const month = parseInt(value);

        setFieldValue(name, month);
        setFieldValue("endMonth", month);
    };

    const onChangeYear = (event: React.ChangeEvent<HTMLSelectElement>) => {
        const { name, value } = event.currentTarget;

        setFieldValue(name, value);
        setFieldValue("endYear", value);
    };

    return (
        <div className="flex align-center">
            <MonthsContainer>
                <Select options={MONTHS} marginBottom="none" name="startMonth" onChange={onChangeMonth} value={values.startMonth} />
            </MonthsContainer>

            <YearsContainer>
                <Select options={YEARS} marginBottom="none" name="startYear" onChange={onChangeYear} value={values.startYear} />
            </YearsContainer>
        </div>
    );
};

export interface IOneMonthPickerProps {
    initialValues: ITimelinePickerState;
    onSubmit: (state: ITimelinePickerState) => void;
}

export const OneMonthPicker: FunctionComponent<IOneMonthPickerProps> = ({ onSubmit, initialValues }) => {
    return (
        <Formik onSubmit={onSubmit} initialValues={initialValues} component={(formikProps) => <OneMonthPickerBase {...formikProps} />} />
    );
};

const MonthlyPickerBase: FunctionComponent<FormikProps<ITimelinePickerState>> = ({ values, setFieldValue, handleSubmit }) => {
    useUpdateEffect(() => {
        handleSubmit();
    }, [values]);

    const onChangeMonth = (event: React.ChangeEvent<HTMLSelectElement>) => {
        const { name, value } = event.currentTarget;

        const month = parseInt(value);

        setFieldValue(name, month);
    };

    const onChangeYear = (event: React.ChangeEvent<HTMLSelectElement>) => {
        const { name, value } = event.currentTarget;

        setFieldValue(name, value);
    };

    return (
        <div className="flex align-center">
            <MonthsContainer>
                <Select options={MONTHS} marginBottom="none" name="startMonth" onChange={onChangeMonth} value={values.startMonth} />
            </MonthsContainer>

            <YearsContainer>
                <Select options={YEARS} marginBottom="none" name="startYear" onChange={onChangeYear} value={values.startYear} />
            </YearsContainer>
            <Separator>to</Separator>
            <MonthsContainer>
                <Select options={MONTHS} marginBottom="none" name="endMonth" onChange={onChangeMonth} value={values.endMonth} />
            </MonthsContainer>

            <YearsContainer>
                <Select options={YEARS} marginBottom="none" name="endYear" onChange={onChangeYear} value={values.endYear} />
            </YearsContainer>
        </div>
    );
};

export interface IMonthlyPickerProps {
    initialValues: ITimelinePickerState;
    onSubmit: (state: ITimelinePickerState) => void;
}

export const MonthlyPicker: FunctionComponent<IMonthlyPickerProps> = ({ onSubmit, initialValues }) => {
    return <Formik onSubmit={onSubmit} initialValues={initialValues} component={(formikProps) => <MonthlyPickerBase {...formikProps} />} />;
};

const YearlyPickerBase: FunctionComponent<FormikProps<ITimelinePickerState>> = ({ values, setFieldValue, handleSubmit }) => {
    useUpdateEffect(() => {
        handleSubmit();
    }, [values]);

    const onChangeYear = (event: React.ChangeEvent<HTMLSelectElement>) => {
        const { name, value } = event.currentTarget;

        setFieldValue(name, value);
    };

    return (
        <div className="flex align-center">
            <YearsContainer>
                <Select options={YEARS} marginBottom="none" name="startYear" onChange={onChangeYear} value={values.startYear} />
            </YearsContainer>

            <Separator>to</Separator>

            <YearsContainer>
                <Select options={YEARS} marginBottom="none" name="endYear" onChange={onChangeYear} value={values.endYear} />
            </YearsContainer>
        </div>
    );
};

export interface IYearlyPickerProps {
    initialValues: ITimelinePickerState;
    onSubmit: (state: ITimelinePickerState) => void;
}

export const YearlyPicker: FunctionComponent<IYearlyPickerProps> = ({ onSubmit, initialValues }) => {
    return <Formik onSubmit={onSubmit} initialValues={initialValues} component={(formikProps) => <YearlyPickerBase {...formikProps} />} />;
};
