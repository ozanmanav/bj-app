import { createContext, Dispatch } from "react";
import { ITimelineReducerState, ITimelineReducerAction, timelineReducerInitialState } from "../TimelineReducer";

interface ITimelineContextProps {
    timelineState: ITimelineReducerState;
    timelineDispatch: Dispatch<ITimelineReducerAction>;
}

export const timelineContext = createContext<ITimelineContextProps>({
    timelineState: timelineReducerInitialState,
    timelineDispatch: () => {},
});
export const TimelineContextProvider = timelineContext.Provider;
