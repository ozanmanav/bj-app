import { IEventVariant } from "./definitions";
import { ISelectOption } from "../../ui";
import { IEvent } from "../../../views/app/event/eventDetail/definitions";
import {
    getMonth,
    getYear,
    isSameDay,
    isBefore,
    isWithinInterval,
    areIntervalsOverlapping,
    isAfter,
    addDays,
    addWeeks,
    addMonths,
    format,
    addYears,
} from "date-fns";
import get from "lodash.get";
import { ITimelineFilterState, ITimelinePickerState } from "./TimelineReducer";

const getStripEvent = (event: IEvent, variant: IEventVariant) => ({
    start_date: event["start_date"],
    name: event["name"],
    related_objects: get(event, "related_objects")
        ? get(event, "related_objects")
              .map((o) => o.name)
              .join(", ")
        : "N/A",
    url: window.location.origin + getAddEventRoute(variant, event["id"]),
});

export const getAddEventRoute = (variant: IEventVariant, variantID?: string) => {
    switch (variant) {
        case "object":
            return `/app/event/add?obj=${variantID}`;
        case "organization":
            return `/app/event/add?org=${variantID}`;
    }
};

export const getUpdatedEvent = (event: IEvent, date: Date | string, dateFormat: string, variant: IEventVariant) => {
    event.start_date = format(new Date(date), dateFormat);
    return getStripEvent(event, variant);
};

export const isSingleEvent = (event: IEvent) => {
    return (
        event.frequency.type === "none" ||
        isSameDay(new Date(event.start_date), new Date(event.end_date)) ||
        (event.end_date !== null && isBefore(new Date(event.end_date), new Date(event.start_date)))
    );
};

export const isRepeatableEventWithinInterval = (event: IEvent, timelineInterval: any) => {
    return (
        (event.ends_on === "date" &&
            (isWithinInterval(new Date(event.start_date), timelineInterval) ||
                isWithinInterval(new Date(event.end_date), timelineInterval) ||
                areIntervalsOverlapping({ start: new Date(event.start_date), end: new Date(event.end_date) }, timelineInterval))) ||
        ((event.ends_on === "continuous" || event.ends_on === "occurrences") && !isAfter(new Date(event.start_date), timelineInterval.end))
    );
};

export const getNextOccuranceDate = (date: Date, type: string) => {
    switch (type) {
        case "daily":
            return addDays(date, 1);
        case "weekly":
            return addWeeks(date, 1);
        case "monthly":
            return addMonths(date, 1);
        case "yearly":
            return addYears(date, 1);
        default:
            return new Date();
    }
};

export const TimeLineTypes = [
    { value: "oneMonth", label: "One Month" },
    { value: "monthly", label: "Monthly" },
    { value: "yearly", label: "Yearly" },
];

const range = (start: number, stop: number, step = 1) => {
    var a: ISelectOption[] = [],
        b = start;
    while (b < stop) {
        const stepValue = (b += step || 1);
        a.push({
            value: `${stepValue}`,
            label: `${stepValue}`,
        });
    }
    return a;
};

export const YEARS = range(1960, 2022, 1);

export const MONTHS = [
    { value: 1, label: "January" },
    { value: 2, label: "February" },
    { value: 3, label: "March" },
    { value: 4, label: "April" },
    { value: 5, label: "May" },
    { value: 6, label: "June" },
    { value: 7, label: "July" },
    { value: 8, label: "August" },
    { value: 9, label: "September" },
    { value: 10, label: "October" },
    { value: 11, label: "November" },
    { value: 12, label: "December" },
];

export const getPickerStateFromFilterDates = (dates: ITimelineFilterState): ITimelinePickerState => {
    const { startDate, endDate } = dates;

    return {
        startMonth: getMonth(startDate) + 1,
        startYear: getYear(startDate).toString(),
        endMonth: getMonth(endDate) + 1,
        endYear: getYear(endDate).toString(),
    };
};
export const getFilterDatesFromPickerState = (pickerState: ITimelinePickerState): ITimelineFilterState => {
    let startDate = new Date();
    let endDate = new Date();
    if (pickerState) {
        const { startMonth, startYear, endMonth, endYear } = pickerState;

        if (startYear && startMonth) {
            startDate = new Date(parseInt(startYear), startMonth - 1, 1);
        }

        if (endYear && endMonth) {
            endDate = new Date(parseInt(endYear), endMonth, 1);
        }
    }

    return {
        startDate,
        endDate,
    };
};
