import classNames from "classnames";
import { getYear } from "date-fns";
import get from "lodash.get";
import groupBy from "ramda/es/groupBy";
import React, { FunctionComponent, useEffect, useRef, useState, useContext } from "react";
import { useClickOutside } from "../../../../hooks";
import { isNillOrEmpty } from "../../../../utils";
import { IEvent } from "../../../../views/app/event/eventDetail/definitions";
import { timelineContext } from "../TimelineContext";
import "./Timeline.scss";
import {
    cleanAndLowerCase,
    generateDayCaptionsFromDate,
    generateWeekCaptionsFromDate,
    generateYearCaptionsFromDate,
    getMonthTextAndYear,
    parseDateAndGetMonthYear,
    parseDateAndGetShortMonthYear,
    parseDateAndGetWeekOfMonth,
    normalizedEventsForMonthlyType,
    parseDateAndGetDay,
} from "./utils";
import { EventIcon, TimelineTooltip, Button } from "../../../ui";
import { Loader } from "../../../ui/loader";

interface TimelineIconProps {
    event: IEvent;
    sameDay?: boolean;
    index: number;
    isGroupOpen?: boolean;
    isDay?: boolean;
    wrapperStyle?: object;
}

const TimelineIcon: FunctionComponent<TimelineIconProps> = ({ event, sameDay, index, wrapperStyle, isDay }) => {
    const typeName = get(event, "type.name");
    const cleanTypeName = cleanAndLowerCase(typeName);

    const wrapperRef = useClickOutside<HTMLDivElement>(hideTooltip);

    const [isTooltipOpen, setIsTooltipOpen] = useState<boolean>(false);
    const currentIsTooltipOpen = useRef(isTooltipOpen);

    useEffect(() => {
        currentIsTooltipOpen.current = isTooltipOpen;
    }, [isTooltipOpen]);

    function openTooltip() {
        setIsTooltipOpen(true);
    }

    function hideTooltip() {
        if (currentIsTooltipOpen.current) {
            setIsTooltipOpen(false);
        }
    }

    function toggleTooltip() {
        isTooltipOpen ? hideTooltip() : openTooltip();
    }

    const wrapperClassStyle = {
        ...(isTooltipOpen && { zIndex: `50` }),
        ...(isDay && { marginLeft: -6 }),
        ...wrapperStyle,
    };

    return (
        <div className="b-timeline__inner-icon" style={wrapperClassStyle} ref={wrapperRef}>
            <button onClick={toggleTooltip}>
                <EventIcon icon={cleanTypeName} />
            </button>
            <TimelineTooltip event={event} isOpen={isTooltipOpen} />
        </div>
    );
};

interface TimelineGroupProps {
    events: IEvent[];
    wrapperStyle?: object;
    isDay?: boolean;
}

const TimelineGroup: FunctionComponent<TimelineGroupProps> = ({ events = [], wrapperStyle, isDay }) => {
    const groupCount = events.length;

    const [isGroupOpen, setIsGroupOpen] = useState<boolean>(false);
    const currentIsGroupOpen = useRef(isGroupOpen);

    useEffect(() => {
        currentIsGroupOpen.current = isGroupOpen;
    }, [isGroupOpen]);

    const openGroup = () => {
        setIsGroupOpen(true);
    };

    const hideGroup = () => {
        if (currentIsGroupOpen.current) {
            setIsGroupOpen(false);
        }
    };

    const wrapperRef = useClickOutside<HTMLDivElement>(hideGroup);

    const toggleGroup = () => {
        isGroupOpen ? hideGroup() : openGroup();
    };

    const wrapperClassStyle = {
        ...(isDay && { marginLeft: -4 }),
        ...wrapperStyle,
    };

    return (
        <div className="b-timeline__inner-icon" style={wrapperClassStyle} ref={wrapperRef}>
            {isGroupOpen && (
                <div className="b-timeline__inner-icon-label">
                    {events.map((event, index) => (
                        <TimelineIcon key={index} event={event} sameDay={index > 0} index={index} isGroupOpen={isGroupOpen} />
                    ))}
                </div>
            )}
            <EventIcon groupCount={groupCount} onClick={toggleGroup} />
        </div>
    );
};

interface ITimelineProps {
    events: IEvent[];
    loading?: boolean;
}

export const Timeline: FunctionComponent<ITimelineProps> = ({ events = [], loading }) => {
    const {
        timelineState: { timelineType },
    } = useContext(timelineContext);

    if (loading) {
        return (
            <div className="b-timeline__loading">
                <Loader rawLoader />
            </div>
        );
    }

    return (() => {
        switch (timelineType) {
            case "oneMonth":
                return <OneMonthTimeline events={events} />;
            case "monthly":
                return <MonthlyTimeline events={events} />;
            case "yearly":
                return <YearlyTimeline events={events} />;
            default:
                return null;
        }
    })();
};

export const OneMonthTimeline: FunctionComponent<ITimelineProps> = ({ events = [] }) => {
    const {
        timelineState: { timelineFilterDates },
    } = useContext(timelineContext);

    const monthTextAndYear: string = getMonthTextAndYear(timelineFilterDates.startDate);

    const dayCaptions: number[] = generateDayCaptionsFromDate(timelineFilterDates.startDate);
    const normalizedEventsForTheOneMonthType = normalizedEventsForMonthlyType(events, timelineFilterDates);
    const groupByDay = groupBy((event) => {
        return parseDateAndGetDay(event.start_date).toString();
    }, normalizedEventsForTheOneMonthType);

    return (
        <div className="b-timeline">
            <div className="b-timeline__inner"></div>
            <div className="b-timeline__captions">
                {dayCaptions.map((dayCaptionKey) => {
                    const normalizedEventsOfDay = groupByDay[dayCaptionKey + 1] || [];
                    const firstEvent = normalizedEventsOfDay[0];

                    return (
                        <div key={dayCaptionKey}>
                            {!isNillOrEmpty(normalizedEventsOfDay) && (
                                <>
                                    {normalizedEventsOfDay.length === 1 && (
                                        <TimelineIcon event={firstEvent} index={1} key={dayCaptionKey} isDay />
                                    )}
                                    {normalizedEventsOfDay.length > 1 && (
                                        <TimelineGroup events={normalizedEventsOfDay} key={dayCaptionKey} isDay />
                                    )}
                                </>
                            )}

                            <p className="b-timeline__captions-caption oneMonth">{dayCaptionKey + 1}</p>
                        </div>
                    );
                })}
            </div>
            <div className="b-timeline__footer">
                <p className="b-timeline__footer-text h6 _text-uppercase _text-primary">{monthTextAndYear}</p>
                <p className="b-timeline__footer-text h6 _text-uppercase _text-primary">{monthTextAndYear}</p>
            </div>
        </div>
    );
};

export const MonthlyTimeline: FunctionComponent<ITimelineProps> = ({ events = [] }) => {
    const {
        timelineState: { timelineFilterDates },
    } = useContext(timelineContext);

    const weekCaptions = generateWeekCaptionsFromDate(timelineFilterDates.startDate, timelineFilterDates.endDate);

    const monthCountInRow = 4;
    const [monthCount, setMonthCount] = useState<number>(monthCountInRow);

    useEffect(() => {
        setMonthCount(monthCountInRow);
    }, [Object.keys(weekCaptions || {}).length]);

    const newEvents = normalizedEventsForMonthlyType(events, timelineFilterDates);
    const groupByWeek = groupBy((event) => {
        return parseDateAndGetMonthYear(event.start_date).toString();
    }, newEvents);

    const showMoreMonths = (): void => {
        if (weekCaptions) {
            if (Object.keys(weekCaptions).length >= monthCount + monthCountInRow) {
                setMonthCount((prevCount) => prevCount + monthCountInRow);
            } else {
                setMonthCount((prevCount) => prevCount + (Object.keys(weekCaptions).length % monthCountInRow));
            }
        }
    };
    return (
        <div className="b-timeline">
            <div className="b-timeline__captions b-timeline__captions--month">
                {weekCaptions &&
                    Object.keys(weekCaptions).map((key, monthIndex) => {
                        // eslint-disable-next-line no-lone-blocks
                        {
                            const eventsOfWeeks = groupBy((event) => {
                                return parseDateAndGetWeekOfMonth(event.start_date).toString();
                            }, groupByWeek[key] || []);
                            if (monthIndex < monthCount) {
                                return weekCaptions[key].length > 3 ? (
                                    <div key={key} style={{ width: `${100 / monthCountInRow}%` }}>
                                        <div className="b-timeline__inner"></div>
                                        <div className="b-timeline__captions-monthly">
                                            {weekCaptions[key].map((item, index) => {
                                                const normalizedEventsOfWeek = eventsOfWeeks[index + 1] || [];
                                                const firstEvent = normalizedEventsOfWeek[0];

                                                return (
                                                    <div key={index} className="b-timeline__captions-monthly__week">
                                                        {!isNillOrEmpty(normalizedEventsOfWeek) && (
                                                            <>
                                                                {normalizedEventsOfWeek.length === 1 && (
                                                                    <TimelineIcon event={firstEvent} index={1} key={firstEvent.id} />
                                                                )}
                                                                {normalizedEventsOfWeek.length > 1 && (
                                                                    <TimelineGroup events={normalizedEventsOfWeek} key={key} />
                                                                )}
                                                            </>
                                                        )}

                                                        <p
                                                            className={classNames("b-timeline__captions-caption monthly", {
                                                                blue: index === 0,
                                                            })}
                                                            key={item.toString()}
                                                        >
                                                            {`W${index + 1}`}
                                                        </p>
                                                    </div>
                                                );
                                            })}
                                        </div>

                                        <div className="b-timeline__captions-monthly__footer">{key}</div>
                                    </div>
                                ) : null;
                            } else {
                                return null;
                            }
                        }
                    })}
            </div>
            {Object.keys(weekCaptions ? weekCaptions : {}).length > monthCount && (
                <div className="b-timeline__showbutton">
                    <Button text="Show More" onClick={showMoreMonths} primary />
                </div>
            )}
        </div>
    );
};

export const YearlyTimeline: FunctionComponent<ITimelineProps> = ({ events = [] }) => {
    const {
        timelineState: { timelineFilterDates },
    } = useContext(timelineContext);

    const startYear = getYear(timelineFilterDates.startDate);
    const endYear = getYear(timelineFilterDates.endDate);
    const yearCountDefault = 2;
    const [yearCount, setYearCount] = useState<number>(yearCountDefault);

    const yearCaptions = generateYearCaptionsFromDate(timelineFilterDates.startDate, timelineFilterDates.endDate);
    const normalizedEvents = normalizedEventsForMonthlyType(events, timelineFilterDates);

    const groupByMonth = groupBy((item) => parseDateAndGetShortMonthYear(item.start_date).toString(), normalizedEvents);

    useEffect(() => {
        setYearCount(yearCountDefault);
    }, [Object.keys(yearCaptions).length]);

    const showMoreYears = (): void => {
        setYearCount((prevCount) => prevCount + yearCountDefault);
    };

    return (
        <div className="b-timeline">
            {(() => {
                let elements: Array<JSX.Element> = [];

                for (let outerIndex = 0; outerIndex < endYear - startYear; outerIndex++) {
                    if (outerIndex < yearCount) {
                        elements.push(
                            <div key={outerIndex}>
                                <div className="b-timeline__inner"></div>
                                <div className="b-timeline__captions-yearly">
                                    {Object.keys(yearCaptions).map((key, index) => {
                                        const normalizedEventsOfMonth = groupByMonth[key] || [];
                                        const firstEvent = normalizedEventsOfMonth[0];

                                        const monthName = key.split("-")[0];
                                        if (index >= 12 * outerIndex && index < 12 * (outerIndex + 1)) {
                                            return (
                                                <div key={index} className="b-timeline__captions-yearly__month">
                                                    {!isNillOrEmpty(normalizedEventsOfMonth) && (
                                                        <>
                                                            {normalizedEventsOfMonth.length === 1 && (
                                                                <TimelineIcon event={firstEvent} index={1} key={firstEvent.id} />
                                                            )}
                                                            {normalizedEventsOfMonth.length > 1 && (
                                                                <TimelineGroup events={normalizedEventsOfMonth} key={key} />
                                                            )}
                                                        </>
                                                    )}

                                                    <p className={"b-timeline__captions-caption"}>{monthName}</p>
                                                </div>
                                            );
                                        } else {
                                            return null;
                                        }
                                    })}
                                </div>
                                <div className="b-timeline__footer-year">
                                    {startYear + outerIndex} - {startYear + outerIndex + 1}
                                </div>
                            </div>
                        );
                    }
                }
                return elements;
            })()}
            {yearCount < endYear - startYear && (
                <div className="b-timeline__showbutton">
                    <Button text="Show More" onClick={showMoreYears} primary />
                </div>
            )}
        </div>
    );
};
