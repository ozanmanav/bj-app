import {
    format,
    getYear,
    getDaysInMonth,
    getDate,
    eachWeekOfInterval,
    getWeek,
    getWeekOfMonth,
    getMonth,
    isBefore,
    isAfter,
    addDays,
    addYears,
    addMonths,
    differenceInCalendarDays,
    differenceInCalendarWeeks,
    addWeeks,
    differenceInCalendarMonths,
    differenceInCalendarYears,
    subDays,
} from "date-fns";
import get from "lodash.get";
import groupBy from "ramda/es/groupBy";
import { parseDate, formatDate } from "../../../ui/datepicker/utils";
import { IEvent } from "../../../../views/app/event/eventDetail/definitions";
import { ITimelineFilterState } from "../TimelineReducer";
import { isNillOrEmpty } from "../../../../utils";
import { DATE_FORMAT_WITH_TIME } from "../../../../config";
import { mergeDeepWith, concat, flatten } from "ramda";

export const getMonthTextAndYear = (date: Date = new Date()) => {
    return `${format(date, "MMMM")} ${getYear(date)}`;
};

export const cleanAndLowerCase = (text: string) => {
    return text ? text.replace(/\s/g, "").toLowerCase() : text;
};

export const parseDateForTimeline = (dateString: string): Date => {
    const parsedDate = parseDate(dateString, DATE_FORMAT_WITH_TIME) || new Date();

    return parsedDate;
};

export const parseDateAndGetDay = (dateString: string) => {
    const parsedDate = parseDate(dateString, DATE_FORMAT_WITH_TIME) || new Date();
    const eventDay = getDate(parsedDate);

    return eventDay;
};

export const parseDateAndGetMonth = (dateString: string) => {
    const parsedDate = parseDate(dateString, DATE_FORMAT_WITH_TIME) || new Date();
    const eventDay = getMonth(parsedDate);

    return eventDay;
};

export const parseDateAndGetYear = (dateString: string) => {
    const parsedDate = parseDate(dateString, DATE_FORMAT_WITH_TIME) || new Date();
    const eventDay = getYear(parsedDate);

    return eventDay;
};

export const parseDateAndGetWeek = (dateString: string) => {
    const parsedDate = parseDate(dateString, DATE_FORMAT_WITH_TIME) || new Date();
    const eventDay = getWeek(parsedDate);

    return eventDay;
};

export const parseDateAndGetMonthYear = (dateString: string) => {
    const parsedDate = parseDate(dateString, DATE_FORMAT_WITH_TIME) || new Date();

    return formatDate(parsedDate, "MMMM yyyy").toString();
};

export const parseDateAndGetShortMonthYear = (dateString: string) => {
    const parsedDate = parseDate(dateString, DATE_FORMAT_WITH_TIME) || new Date();

    return formatDate(parsedDate, "MMM-yyyy").toString();
};

export const parseDateAndGetWeekOfMonth = (dateString: string) => {
    const parsedDate = parseDate(dateString, DATE_FORMAT_WITH_TIME) || new Date();

    const weekOfMonth = getWeekOfMonth(parsedDate);

    return weekOfMonth;
};

export const generateDayCaptionsFromDate = (date: Date = new Date()): number[] => {
    const daysInTheMonth = getDaysInMonth(date);

    return Array.from(Array(daysInTheMonth).keys());
};

export const generateWeekCaptionsFromDate = (startDate: Date = new Date(), endDate: Date = new Date()) => {
    try {
        var weekDays = eachWeekOfInterval(
            {
                start: startDate,
                end: endDate,
            },
            { weekStartsOn: 1 }
        );

        var groupByWeekDays = groupBy((weekDay) => formatDate(weekDay, "MMMM yyyy").toString(), weekDays);
        var filteredGroupByWeekDays: {
            [index: string]: Date[];
        } = {};
        // eslint-disable-next-line
        Object.keys(groupByWeekDays).map((month) => {
            // Filter months which have less weeks from 3 due to monthly tasks component.
            if (groupByWeekDays[month].length > 2) {
                filteredGroupByWeekDays[`${month}`] = groupByWeekDays[month];
            }
        });

        return filteredGroupByWeekDays;
    } catch (error) {
        console.log(error);
        return null;
    }
};

export const generateYearCaptionsFromDate = (startDate: Date = new Date(), endDate: Date = new Date()) => {
    try {
        var weekDays = eachWeekOfInterval(
            {
                start: startDate,
                end: endDate,
            },
            { weekStartsOn: 1 }
        );

        return groupBy((weekDay) => formatDate(weekDay, "MMM-yyyy").toString(), weekDays);
    } catch (error) {
        console.log(error);
        return [];
    }
};

export const isEqualWithFormat = (firstDate: Date, secondDate: Date, formatString: string = "yyyy-MM") => {
    try {
        return format(firstDate, "yyyy-MM") === format(secondDate, "yyyy-MM");
    } catch (error) {
        console.log(error);
    }
};

const isEqualWithMonthFormat = (firstDate: Date, secondDate: Date, formatString: string = "yyyy-MM") => {
    try {
        return format(firstDate, "MM") === format(secondDate, "MM");
    } catch (error) {
        console.log(error);
    }
};

export const normalizeEventsForTheOneMonthType = (
    events: IEvent[] = [],
    timelineFilterDates: ITimelineFilterState,
    dayCaptions: number[] = []
) => {
    const selectedMonthEvents: IEvent[] = events.filter((event) =>
        isEqualWithFormat(parseDateForTimeline(event.start_date), timelineFilterDates.startDate)
    );
    const filterMonth = getMonth(timelineFilterDates.startDate);
    const filterYear = getYear(timelineFilterDates.startDate);

    const previousYearlyEvents = events.filter((event) => {
        return (
            event.ends_on === "date" &&
            get(event, "frequency.type") === "yearly" &&
            isBefore(parseDateForTimeline(event.start_date), new Date(filterYear, filterMonth, 1)) &&
            isAfter(parseDateForTimeline(event.end_date), new Date(filterYear, filterMonth, 1)) &&
            isAfter(parseDateForTimeline(event.end_date), new Date(filterYear, filterMonth, parseDateAndGetDay(event.start_date))) &&
            isEqualWithMonthFormat(parseDateForTimeline(event.start_date), timelineFilterDates.startDate)
        );
    });

    const previousYearlyEventsOccurenceType = events.filter((event) => {
        return (
            event.ends_on === "occurrences" &&
            get(event, "frequency.type") === "yearly" &&
            isBefore(parseDateForTimeline(event.start_date), new Date(filterYear, filterMonth, 1)) &&
            isAfter(
                addYears(parseDateForTimeline(event.start_date), get(event, "frequency.count")),
                new Date(filterYear, filterMonth, 1)
            ) &&
            isEqualWithMonthFormat(parseDateForTimeline(event.start_date), timelineFilterDates.startDate)
        );
    });

    const previousMonthlyEvents = events.filter((event) => {
        return (
            event.ends_on === "date" &&
            get(event, "frequency.type") === "monthly" &&
            isBefore(parseDateForTimeline(event.start_date), new Date(filterYear, filterMonth, 1)) &&
            isAfter(parseDateForTimeline(event.end_date), new Date(filterYear, filterMonth, 1)) &&
            isAfter(parseDateForTimeline(event.end_date), new Date(filterYear, filterMonth, parseDateAndGetDay(event.start_date)))
        );
    });

    const previousMonthlyEventsOccurenceType = events.filter((event) => {
        return (
            event.ends_on === "occurrences" &&
            get(event, "frequency.type") === "monthly" &&
            isBefore(parseDateForTimeline(event.start_date), new Date(filterYear, filterMonth, 1)) &&
            isAfter(
                addMonths(parseDateForTimeline(event.start_date), get(event, "frequency.count") - 1),
                new Date(filterYear, filterMonth, 1)
            )
        );
    });

    const previousWeeklyEvents = events.filter((event) => {
        return (
            event.ends_on === "date" &&
            get(event, "frequency.type") === "weekly" &&
            isBefore(parseDateForTimeline(event.start_date), new Date(filterYear, filterMonth, 1)) &&
            isAfter(parseDateForTimeline(event.end_date), new Date(filterYear, filterMonth, 1)) &&
            isAfter(parseDateForTimeline(event.end_date), new Date(filterYear, filterMonth, parseDateAndGetDay(event.end_date)))
        );
    });
    const previousWeeklyEventsOccurenceType = events.filter((event) => {
        return (
            event.ends_on === "occurrences" &&
            get(event, "frequency.type") === "weekly" &&
            isBefore(parseDateForTimeline(event.start_date), new Date(filterYear, filterMonth, 1)) &&
            isAfter(addDays(parseDateForTimeline(event.start_date), get(event, "frequency.count") * 7), timelineFilterDates.startDate)
        );
    });

    const normalizedPreviousWeeklyEvents = {};
    dayCaptions.forEach((dayCaption) => {
        const toAddWeeklyEvents = [...previousWeeklyEvents, ...previousWeeklyEventsOccurenceType].filter((item) => {
            const eventStartDay = parseDateAndGetDay(item.start_date);
            const eventStartMonth = parseDateAndGetMonth(item.start_date);
            const eventStartYear = parseDateAndGetYear(item.start_date);
            const eventEndYear = parseDateAndGetYear(item.end_date);
            const eventEndDay = parseDateAndGetDay(item.end_date);
            const eventEndMonth = parseDateAndGetMonth(item.end_date);

            const occurrenceCount = get(item, "frequency.count");
            if (occurrenceCount) {
                return (
                    dayCaption % 7 === eventStartDay % 7 &&
                    isBefore(
                        new Date(filterYear, filterMonth, dayCaption),
                        addDays(new Date(eventStartYear, eventStartMonth, eventStartDay), occurrenceCount * 7)
                    )
                );
            }

            if (item.end_date) {
                return (
                    dayCaption % 7 === eventStartDay % 7 &&
                    isBefore(new Date(filterYear, filterMonth, dayCaption), new Date(eventEndYear, eventEndMonth, eventEndDay))
                );
            }
            return dayCaption % 7 === eventStartDay % 7;
        });

        if (!isNillOrEmpty(toAddWeeklyEvents)) {
            Object.assign(normalizedPreviousWeeklyEvents, { [dayCaption]: toAddWeeklyEvents });
        }
    });

    const neverEndEvents: IEvent[] = events.filter((event) => {
        return event.ends_on === "continuous" && isBefore(parseDateForTimeline(event.start_date), timelineFilterDates.startDate);
    });

    const dailyEvents = [
        ...neverEndEvents.filter((item: IEvent) => get(item, "frequency.type") === "daily"),
        ...selectedMonthEvents.filter((item: IEvent) => get(item, "frequency.type") === "daily"),
    ];

    const weeklyEvents = [
        ...neverEndEvents.filter((item: IEvent) => get(item, "frequency.type") === "weekly"),
        ...selectedMonthEvents.filter((item: IEvent) => get(item, "frequency.type") === "weekly"),
    ];

    const normalizedDailyEvents = {};
    dayCaptions.forEach((dayCaption) => {
        const toAddDailyEvents = dailyEvents.filter((item) => {
            const eventStartDay = parseDateAndGetDay(item.start_date);
            const eventStartMonth = parseDateAndGetMonth(item.start_date);
            const eventStartYear = parseDateAndGetYear(item.start_date);
            const eventEndDay = parseDateAndGetDay(item.end_date);

            const continuosEvent = get(item, "ends_on") === "continuous";
            if (continuosEvent) {
                if (filterMonth === eventStartMonth && filterYear === eventStartYear) {
                    return dayCaption >= eventStartDay;
                }

                return true;
            }

            const occurrenceCount = get(item, "frequency.count");
            if (occurrenceCount) {
                return dayCaption >= eventStartDay && dayCaption < eventStartDay + occurrenceCount;
            }

            if (item.end_date) {
                return dayCaption + 1 > eventStartDay && dayCaption - 1 < eventEndDay;
            }

            return dayCaption + 1 > eventStartDay;
        });

        if (!isNillOrEmpty(toAddDailyEvents)) {
            Object.assign(normalizedDailyEvents, { [dayCaption]: toAddDailyEvents });
        }
    });
    const normalizedWeeklyEvents = {};
    dayCaptions.forEach((dayCaption) => {
        const toAddWeeklyEvents = weeklyEvents.filter((item) => {
            const eventStartDay = parseDateAndGetDay(item.start_date);
            const eventStartMonth = parseDateAndGetMonth(item.start_date);
            const eventStartYear = parseDateAndGetYear(item.start_date);
            const eventEndYear = parseDateAndGetYear(item.end_date);
            const eventEndDay = parseDateAndGetDay(item.end_date);
            const eventEndMonth = parseDateAndGetMonth(item.end_date);
            const continuosEvent = get(item, "ends_on") === "continuous";
            if (continuosEvent) {
                if (filterMonth === eventStartMonth && filterYear === eventStartYear) {
                    return dayCaption % 7 === eventStartDay % 7;
                }

                return true;
            }
            const occurrenceCount = get(item, "frequency.count");
            if (occurrenceCount) {
                return (
                    dayCaption % 7 === eventStartDay % 7 &&
                    isBefore(
                        new Date(filterYear, filterMonth, dayCaption),
                        addDays(new Date(eventStartYear, eventStartMonth, eventStartDay), occurrenceCount * 7)
                    )
                );
            }

            if (item.end_date) {
                return (
                    dayCaption % 7 === eventStartDay % 7 &&
                    isBefore(new Date(filterYear, filterMonth, dayCaption), new Date(eventEndYear, eventEndMonth, eventEndDay))
                );
            }
            return dayCaption % 7 === eventStartDay % 7;
        });

        if (!isNillOrEmpty(toAddWeeklyEvents)) {
            Object.assign(normalizedWeeklyEvents, { [dayCaption]: toAddWeeklyEvents });
        }
    });

    const yearlyEvents = selectedMonthEvents.filter((item: IEvent) => get(item, "frequency.type") === "yearly");
    const monthlyEvents = selectedMonthEvents.filter((item: IEvent) => get(item, "frequency.type") === "monthly");

    const groupByDay = groupBy((item) => parseDateAndGetDay(item.start_date).toString(), [
        ...monthlyEvents,
        ...yearlyEvents,
        ...previousMonthlyEvents,
        ...previousMonthlyEventsOccurenceType,
        ...previousYearlyEvents,
        ...previousYearlyEventsOccurenceType,
    ]);

    const mergedEvents = mergeDeepWith(concat, normalizedDailyEvents, groupByDay);
    const mergedDeepEvents = mergeDeepWith(concat, mergedEvents, normalizedWeeklyEvents);
    const mergedGroupEvents = mergeDeepWith(concat, mergedDeepEvents, normalizedPreviousWeeklyEvents);

    return mergedGroupEvents;
};

export const normalizedEventsForMonthlyType = (events: IEvent[] = [], timelineFilterDates: ITimelineFilterState) => {
    const newEvents: any[] = [];
    events.forEach((event) => {
        switch (get(event, "frequency.type")) {
            case "none":
                if (
                    isBefore(subDays(timelineFilterDates.startDate, 1), parseDateForTimeline(get(event, "start_date"))) &&
                    isBefore(parseDateForTimeline(get(event, "start_date")), addDays(timelineFilterDates.endDate, 1))
                )
                    newEvents.push(event);
                break;
            case "daily":
                if (get(event, "ends_on") === "date") {
                    const numberOfDays = differenceInCalendarDays(
                        parseDateForTimeline(event.end_date),
                        parseDateForTimeline(event.start_date)
                    );
                    const interval = get(event, "frequency.interval") || 1;
                    newEvents.push(createDailyEvents(event, numberOfDays, interval, timelineFilterDates));
                    break;
                }
                if (get(event, "ends_on") === "occurrences") {
                    const numberOfDays = get(event, "frequency.count") || 0;
                    const interval = get(event, "frequency.interval") || 1;
                    newEvents.push(createDailyEvents(event, numberOfDays - 1, interval, timelineFilterDates));
                    break;
                }
                if (get(event, "ends_on") === "continuous") {
                    const numberOfDays = differenceInCalendarDays(timelineFilterDates.endDate, parseDateForTimeline(event.start_date));
                    const interval = get(event, "frequency.interval") || 1;
                    newEvents.push(createDailyEvents(event, numberOfDays, interval, timelineFilterDates));
                    break;
                }
                if (
                    isBefore(subDays(timelineFilterDates.startDate, 1), parseDateForTimeline(get(event, "start_date"))) &&
                    isBefore(parseDateForTimeline(get(event, "start_date")), addDays(timelineFilterDates.endDate, 1))
                )
                    newEvents.push(event);
                break;
            case "weekly":
                if (get(event, "ends_on") === "date") {
                    const numberOfDays = differenceInCalendarWeeks(
                        parseDateForTimeline(event.end_date),
                        parseDateForTimeline(event.start_date)
                    );
                    const interval = get(event, "frequency.interval") || 1;
                    newEvents.push(createWeeklyEvents(event, numberOfDays, interval, timelineFilterDates));
                    break;
                }
                if (get(event, "ends_on") === "occurrences") {
                    const numberOfDays = get(event, "frequency.count") || 0;
                    const interval = get(event, "frequency.interval") || 1;
                    newEvents.push(createWeeklyEvents(event, numberOfDays - 1, interval, timelineFilterDates));
                    break;
                }
                if (get(event, "ends_on") === "continuous") {
                    const numberOfDays = differenceInCalendarWeeks(timelineFilterDates.endDate, parseDateForTimeline(event.start_date));
                    const interval = get(event, "frequency.interval") || 1;
                    newEvents.push(createWeeklyEvents(event, numberOfDays, interval, timelineFilterDates));
                    break;
                }
                if (
                    isBefore(subDays(timelineFilterDates.startDate, 1), parseDateForTimeline(get(event, "start_date"))) &&
                    isBefore(parseDateForTimeline(get(event, "start_date")), addDays(timelineFilterDates.endDate, 1))
                )
                    newEvents.push(event);
                break;
            case "monthly":
                if (get(event, "ends_on") === "date") {
                    const numberOfDays = differenceInCalendarMonths(
                        parseDateForTimeline(event.end_date),
                        parseDateForTimeline(event.start_date)
                    );
                    const interval = get(event, "frequency.interval") || 1;
                    newEvents.push(createMonthlyEvents(event, numberOfDays, interval, timelineFilterDates));
                    break;
                }
                if (get(event, "ends_on") === "occurrences") {
                    const numberOfDays = get(event, "frequency.count") || 0;
                    const interval = get(event, "frequency.interval") || 1;
                    newEvents.push(createMonthlyEvents(event, numberOfDays - 1, interval, timelineFilterDates));
                    break;
                }
                if (get(event, "ends_on") === "continuous") {
                    const numberOfDays = differenceInCalendarMonths(timelineFilterDates.endDate, parseDateForTimeline(event.start_date));
                    const interval = get(event, "frequency.interval") || 1;
                    newEvents.push(createMonthlyEvents(event, numberOfDays, interval, timelineFilterDates));
                    break;
                }
                if (
                    isBefore(subDays(timelineFilterDates.startDate, 1), parseDateForTimeline(get(event, "start_date"))) &&
                    isBefore(parseDateForTimeline(get(event, "start_date")), addDays(timelineFilterDates.endDate, 1))
                )
                    newEvents.push(event);
                break;
            case "yearly":
                if (get(event, "ends_on") === "date") {
                    const numberOfDays = differenceInCalendarYears(
                        parseDateForTimeline(event.end_date),
                        parseDateForTimeline(event.start_date)
                    );
                    const interval = get(event, "frequency.interval") || 1;
                    newEvents.push(createYearlyEvents(event, numberOfDays, interval, timelineFilterDates));
                    break;
                }
                if (get(event, "ends_on") === "occurrences") {
                    const numberOfDays = get(event, "frequency.count") || 0;
                    const interval = get(event, "frequency.interval") || 1;
                    newEvents.push(createYearlyEvents(event, numberOfDays - 1, interval, timelineFilterDates));
                    break;
                }
                if (get(event, "ends_on") === "continuous") {
                    const numberOfDays = differenceInCalendarYears(timelineFilterDates.endDate, parseDateForTimeline(event.start_date));
                    const interval = get(event, "frequency.interval") || 1;
                    newEvents.push(createYearlyEvents(event, numberOfDays, interval, timelineFilterDates));
                    break;
                }
                if (
                    isBefore(subDays(timelineFilterDates.startDate, 1), parseDateForTimeline(get(event, "start_date"))) &&
                    isBefore(parseDateForTimeline(get(event, "start_date")), addDays(timelineFilterDates.endDate, 1))
                )
                    newEvents.push(event);
                break;
            default:
                //return event if it is within filtering range
                if (
                    isBefore(subDays(timelineFilterDates.startDate, 1), parseDateForTimeline(get(event, "start_date"))) &&
                    isBefore(parseDateForTimeline(get(event, "start_date")), addDays(timelineFilterDates.endDate, 1))
                )
                    newEvents.push(event);
        }
    });
    return flatten(newEvents).filter((newEvent) => newEvent !== null);
};

const createDailyEvents = (event: IEvent, days: number, interval: number = 1, timelineFilterDates: ITimelineFilterState) => {
    const newEvents = [];
    let i = 0;
    for (i = 0; i <= days; i += interval) {
        newEvents.push({ ...event, start_date: format(addDays(parseDateForTimeline(event.start_date), i), "yyyy-MM-d 00:00:00") });
    }
    return newEvents.filter(
        (event) =>
            isBefore(subDays(timelineFilterDates.startDate, 1), parseDateForTimeline(event.start_date)) &&
            isBefore(parseDateForTimeline(event.start_date), addDays(timelineFilterDates.endDate, 1))
    );
};

const createWeeklyEvents = (event: IEvent, days: number, interval: number = 1, timelineFilterDates: ITimelineFilterState) => {
    const newEvents = [];
    let i = 0;
    for (i = 0; i <= days; i += interval) {
        newEvents.push({ ...event, start_date: format(addWeeks(parseDateForTimeline(event.start_date), i), "yyyy-MM-d 00:00:00") });
    }
    return newEvents.filter(
        (event) =>
            isBefore(subDays(timelineFilterDates.startDate, 1), parseDateForTimeline(event.start_date)) &&
            isBefore(parseDateForTimeline(event.start_date), addDays(timelineFilterDates.endDate, 1))
    );
};

const createMonthlyEvents = (event: IEvent, days: number, interval: number = 1, timelineFilterDates: ITimelineFilterState) => {
    const newEvents = [];
    let i = 0;
    for (i = 0; i <= days; i += interval) {
        newEvents.push({ ...event, start_date: format(addMonths(parseDateForTimeline(event.start_date), i), "yyyy-MM-d 00:00:00") });
    }
    return newEvents.filter(
        (event) =>
            isBefore(subDays(timelineFilterDates.startDate, 1), parseDateForTimeline(event.start_date)) &&
            isBefore(parseDateForTimeline(event.start_date), addDays(timelineFilterDates.endDate, 1))
    );
};

const createYearlyEvents = (event: IEvent, days: number, interval: number = 1, timelineFilterDates: ITimelineFilterState) => {
    const newEvents = [];
    let i = 0;
    for (i = 0; i <= days; i++) {
        newEvents.push({ ...event, start_date: format(addYears(parseDateForTimeline(event.start_date), i), "yyyy-MM-d 00:00:00") });
    }
    return newEvents.filter(
        (event) =>
            isBefore(subDays(timelineFilterDates.startDate, 1), parseDateForTimeline(event.start_date)) &&
            isBefore(parseDateForTimeline(event.start_date), addDays(timelineFilterDates.endDate, 1))
    );
};
