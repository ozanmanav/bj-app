import gql from "graphql-tag";

export const UPDATE_EVENT_TIMELINE = gql`
    mutation UpdateEventTimeline($id: String!, $timeline: Boolean) {
        updateEvent(id: $id, timeline: $timeline) {
            id
        }
    }
`;
