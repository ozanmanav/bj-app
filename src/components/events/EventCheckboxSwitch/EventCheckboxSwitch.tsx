import React, { FunctionComponent } from "react";
import { CheckboxSwitch, showErrorToast, handleGraphQLErrors, showSuccessToast } from "../../ui";
import get from "lodash.get";
import { useMutation } from "@apollo/react-hooks";
import { UPDATE_EVENT_TIMELINE } from "./queries";

type TEvent = {
    id: string;
    timeline: boolean;
};
interface IEventCheckboxSwitchProps {
    events: TEvent[];
    refetch: Function;
}
export const EventCheckboxSwitch: FunctionComponent<IEventCheckboxSwitchProps> = ({ events, refetch }) => {
    const isCheckboxSwitchDisabled = events.length <= 0;
    const [checked, setChecked] = React.useState(isCheckboxSwitchDisabled ? false : get(events, "[0].timeline"));

    const [updateTimeline] = useMutation(UPDATE_EVENT_TIMELINE);
    const handleChange = async (event: React.ChangeEvent<HTMLInputElement>) => {
        setChecked(event.currentTarget.checked);
        try {
            const { errors } = await updateTimeline({
                variables: {
                    id: get(events, "[0].id"),
                    timeline: event.currentTarget.checked,
                },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                await refetch();
                showSuccessToast("Timeline successfully updated");
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    };
    return <CheckboxSwitch disabled={isCheckboxSwitchDisabled} onChange={handleChange} checked={checked} />;
};
