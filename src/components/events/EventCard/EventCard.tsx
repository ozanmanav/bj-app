import React, { FunctionComponent, useState } from "react";
import { parseAndFormatDate } from "../../../views/app/event/eventDetail/eventHeader/utils";
import { IEvent } from "../../../views/app/event/eventDetail/definitions";
import { Link } from "react-router-dom";
import get from "lodash.get";

interface IEventCardProps extends IEvent {
    hide: () => void;
}

export const EventCard: FunctionComponent<IEventCardProps> = ({ id, start_date, name, end_date, type, description, hide, frequency }) => {
    const [isExpanded, setIsExpanded] = useState<boolean>(false);
    const expand = () => {
        setIsExpanded(true);
    };
    return (
        <div className={"b-task"}>
            <div className="b-task__info-wrapper">
                <div className="flex align-center b-task__info">
                    <p className="h6 _text-grey _font-regular">
                        {parseAndFormatDate(start_date)} ● {get(frequency, "type")}
                    </p>

                    <p className="b-task__assigned h6">
                        <span className="_text-grey">Type: </span> <span className="_font-bold">{get(type, "name") || ""}</span>
                    </p>
                </div>
                <h2 className="_font-regular">
                    <Link to={`/app/event/${id}`} onClick={hide}>
                        {name}
                    </Link>
                </h2>
                <div className="b-task__content">
                    {!isExpanded && description && description.length > 150 ? (
                        <>
                            <p>{`${description.substring(0, 150)}...`}</p>
                            <button className="h5 _text-primary _font-bold b-task__more" onClick={expand}>
                                Read more
                            </button>
                        </>
                    ) : (
                        <p>{description}</p>
                    )}
                </div>
            </div>
        </div>
    );
};
