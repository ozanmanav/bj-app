import React, { FunctionComponent } from "react";
import { Link } from "react-router-dom";
import { Icon } from "../ui/icons";
import "./ObjectName.scss";
import { useObjectTypes } from "../../hooks";
import { getObjectLink, getTypeLabel } from "../../utils";

interface IObjectNameProps {
    name: string;
    type: string;
    id?: string;
    typeLabel?: string;
    postfix?: string;
}

export const ObjectName: FunctionComponent<IObjectNameProps> = ({ name, type, id, typeLabel }) => {
    const objectTypes = useObjectTypes();
    const objectTypeLabel = typeLabel || getTypeLabel(type, objectTypes);

    return (
        <Link to={getObjectLink(type, id)} className="flex">
            <Icon icon={type} className="b-object-name__type-icon" />
            <div className="flex flex-column justify-center">
                <p className="_font-bold _text-black b-object-name__name" title={name}>
                    {name}
                </p>
                {objectTypeLabel && <p className="_text-grey h6 b-object-name__type _text-capitalize">{objectTypeLabel}</p>}
            </div>
        </Link>
    );
};

export const ObjectNameSimple: FunctionComponent<IObjectNameProps> = ({ name, type, id, postfix }) => {
    const nameElClassName = `_font-bold ${type === "company" ? "_text-primary" : "_text-black"} b-object-name__name`;

    return (
        <Link to={getObjectLink(type, id, postfix ? postfix : "related")} className="flex">
            <Icon icon={type} className="b-object-name__type-icon _small" />
            <div className="flex flex-column justify-center">
                <p className={nameElClassName} title={name}>
                    {name}
                </p>
            </div>
        </Link>
    );
};
