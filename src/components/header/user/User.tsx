import React, { FunctionComponent, MouseEvent, useContext, useState } from "react";
import { RouteComponentProps, withRouter } from "react-router";
import { Button, ButtonLink, handleGraphQLErrors, Icon, showErrorToast } from "../../ui";
import { searchContext } from "../../../contexts/searchContext";
import gql from "graphql-tag";
import { useApolloClient, useQuery } from "@apollo/react-hooks";
import { AccountStateContext, ICurrentOrganizationState } from "../../../contexts/accountStateContext";
import "./User.scss";
import { useClickOutside } from "../../../hooks";
import classnames from "classnames";

const LOGOUT = gql`
    query Logout {
        logout
    }
`;

const SET_CURRENT_CLIENT = gql`
    query SetCurrentClient($id: String!) {
        setCurrentClient(id: $id) {
            id
        }
    }
`;

const AuthNav: FunctionComponent = () => {
    return (
        <nav className="b-header-user">
            <ButtonLink to="/login" text="Log in" className="b-header-user__button" />
        </nav>
    );
};

const UserBase: FunctionComponent<RouteComponentProps> = ({ history }) => {
    const { toggleVisibility } = useContext(searchContext);

    const { user, allUserOrganizations, currentOrganization, clearAccountData } = useContext(AccountStateContext);
    const allowCompanyChange = allUserOrganizations.length > 1;

    const [companiesListOpened, setCompaniesListOpened] = useState<boolean>(false);
    const [isCompanyChangeDisabled, setIsCompanyChangeDisabled] = useState<boolean>(false);

    const { refetch: logoutRefetch } = useQuery(LOGOUT, { skip: true });

    async function logout() {
        try {
            const { errors, data } = await logoutRefetch();

            if (errors) {
                handleGraphQLErrors(errors);
            } else if (data.logout) {
                clearAccountData();
                history.push("/login");
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    function toggle(e: MouseEvent) {
        e.stopPropagation();
        toggleVisibility();
    }

    function showCompaniesList() {
        if (!allowCompanyChange || isCompanyChangeDisabled) {
            return;
        }

        setCompaniesListOpened(true);
    }

    function hideCompaniesList() {
        if (isCompanyChangeDisabled) {
            return;
        }

        setCompaniesListOpened(false);
    }

    if (!user.id) {
        return <AuthNav />;
    }

    const buttonClassname = classnames(["flex align-center b-header-user__company-button", { _active: allowCompanyChange }]);

    return (
        <nav className="b-header-user flex">
            <button className="b-header__search flex justify-center align-center" onClick={toggle}>
                <Icon icon="search" className="b-header__search-icon" />
            </button>
            <div className="b-header-user__info-wrapper">
                <button className={buttonClassname} disabled={isCompanyChangeDisabled} onClick={showCompaniesList}>
                    <div className="b-header-user__company flex align-center justify-center _font-bold _text-primary">
                        {currentOrganization.name[0] && currentOrganization.name[0].toUpperCase()}
                    </div>
                    <div className="flex flex-column b-header-user__info">
                        <p className="_text-grey _text-left _font-bold">
                            Hello {user.first_name} {user.last_name}
                        </p>
                        <p className="_font-bold _text-left">{currentOrganization.role.name}</p>
                    </div>
                    {allowCompanyChange && <Icon icon="selectArrows" className="b-header-user__arrows" />}
                </button>
                {companiesListOpened && <UserCompaniesList hide={hideCompaniesList} setDisabled={setIsCompanyChangeDisabled} />}
            </div>
            <Button text="Logout" className="b-header-user__logout" onClick={logout} />
        </nav>
    );
};

export const User = withRouter(UserBase);

interface IUserCompaniesListProps {
    hide: () => void;
    setDisabled: (isDisabled: boolean) => void;
}

const UserCompaniesList: FunctionComponent<IUserCompaniesListProps> = ({ hide, setDisabled }) => {
    const { allUserOrganizations, currentOrganization } = useContext(AccountStateContext);
    const ref = useClickOutside<HTMLDivElement>(hide);

    const companiesToShow = allUserOrganizations.filter(({ id }) => id !== currentOrganization.id);

    return (
        <div className="b-header-user__companies" ref={ref}>
            {companiesToShow.map((userOrganization) => (
                <UserCompaniesListItem
                    userOrganization={userOrganization}
                    hide={hide}
                    setDisabled={setDisabled}
                    key={userOrganization.id}
                />
            ))}
        </div>
    );
};

interface IUserCompaniesListItemProps {
    userOrganization: ICurrentOrganizationState;
    hide: () => void;
    setDisabled: (isDisabled: boolean) => void;
}

const UserCompaniesListItemBase: FunctionComponent<IUserCompaniesListItemProps & RouteComponentProps> = ({
    history,
    userOrganization,
    hide,
    setDisabled,
}) => {
    const apolloClient = useApolloClient();
    const { currentOrganization: prevCompany, saveCurrentOrganization } = useContext(AccountStateContext);

    async function onSelfClick() {
        try {
            const { errors } = await apolloClient.query({
                query: SET_CURRENT_CLIENT,
                variables: {
                    id: userOrganization.id,
                },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                setDisabled(true);
                saveCurrentOrganization(userOrganization);
                hide();
                history.push("/app/cockpit");
            }
        } catch (e) {
            saveCurrentOrganization(prevCompany);
            showErrorToast(e.message);
        }

        setDisabled(false);
    }

    return (
        <button className="flex align-center b-header-user__company-button _active" onClick={onSelfClick}>
            <div className="b-header-user__company flex align-center justify-center _font-bold _text-primary">
                {userOrganization.name[0] && userOrganization.name[0].toUpperCase()}
            </div>
            <div className="flex flex-column b-header-user__info">
                <p className="_text-grey _text-left _font-bold b-header-user__info-company" title={userOrganization.name}>
                    {userOrganization.name}
                </p>
                <p className="_font-bold _text-left">{userOrganization.role.name}</p>
            </div>
        </button>
    );
};

export const UserCompaniesListItem = withRouter(UserCompaniesListItemBase);
