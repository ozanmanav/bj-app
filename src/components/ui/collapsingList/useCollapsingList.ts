import { useState } from "react";

export function useCollapsingList(defaultValue: boolean = true) {
    const [isCollapsed, setIsCollapsed] = useState<boolean>(defaultValue);

    function collapse() {
        setIsCollapsed(true);
    }

    function expand() {
        setIsCollapsed(false);
    }

    return {
        isCollapsed,
        collapse,
        expand,
    };
}
