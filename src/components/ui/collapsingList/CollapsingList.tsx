import React, { FunctionComponent } from "react";
import "./CollapsingList.scss";
import { appendClassName } from "../../../utils";
import { Button } from "../";

interface ICollapsingListProps {
    isCollapsed: boolean;
    collapse: () => void;
}

export const CollapsingList: FunctionComponent<ICollapsingListProps> = ({ isCollapsed, collapse, children }) => {
    const className = appendClassName("b-collapsing-list", isCollapsed ? "_collapsed" : "");

    return (
        <div className={className}>
            {children}
            {!isCollapsed && <Button text="Collapse" icon="collapse" onClick={collapse} className="b-collapsing-list__collapse-button" />}
        </div>
    );
};
