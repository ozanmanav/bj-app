import React, { FunctionComponent, ImgHTMLAttributes } from "react";
import "./Images.scss";
import classNames from "classnames";

interface IImageProps<T> extends ImgHTMLAttributes<T> {
    src: string;
    alt: string;
    contain?: boolean;
    className?: string;
    aspectRatio?: number;
}

export const Image: FunctionComponent<IImageProps<HTMLImageElement>> = ({ src, alt, contain, className, aspectRatio = 1.65, ...props }) => {
    return (
        <figure className={classNames("b-image__container flex justify-center align-center", className)} {...props}>
            <img src={src} alt={alt} className={classNames("b-image", { _contain: contain }, { _cover: !contain })} />
        </figure>
    );
};
