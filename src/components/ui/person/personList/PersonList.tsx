import React, { FunctionComponent } from "react";
import "./PersonList.scss";
import {
    PersonCollapsed,
    CollapsingList,
    useCollapsingList,
    PersonExpanded,
    TPersonVariants,
    showErrorToast,
    handleGraphQLErrors,
    showSuccessToast,
} from "../../";
import { IEmployee } from "../../../../config/types";
import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";
import { normalizeGraphQLData } from "../../../../utils";
import { getArrayIDs } from "../../../../views/app/event/addEvent/utils";

const REMOVE_CONTACT = gql`
    mutation RemoveContact($id: String!, $organization_id: String!, $employee_ids: [String]) {
        updateContract(id: $id, organization_id: $organization_id, employee_ids: $employee_ids) {
            id
        }
    }
`;

interface IPersonListProps {
    variant?: TPersonVariants;
    contacts: IEmployee[];
    refetch: () => void;
    organizationID: string;
    isClientOrganization: boolean;
    contractID: string;
}

// TODO: investigate flickering when "isCollapsed" changing
export const PersonsList: FunctionComponent<IPersonListProps> = ({
    variant,
    contacts,
    refetch,
    contractID,
    organizationID,
    isClientOrganization,
}) => {
    const { isCollapsed, collapse, expand } = useCollapsingList();
    const [removeContactMutation] = useMutation(REMOVE_CONTACT);

    async function deleteContact(contact: IEmployee) {
        const { user } = contact;
        try {
            const updatedEmployees = contacts.reduce((acc: IEmployee[], c) => {
                if (c.id !== contact.id) {
                    acc.push(normalizeGraphQLData(c));
                }

                return acc;
            }, []);

            const res = await removeContactMutation({
                variables: {
                    id: contractID,
                    organization_id: organizationID,
                    employee_ids: getArrayIDs(updatedEmployees),
                },
            });

            const { errors } = res;

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                refetch();
                if (user) {
                    showSuccessToast(`${user.first_name} ${user.last_name} is successfully deleted`);
                }
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    if (contacts.length === 1) {
        return (
            <PersonExpanded
                key={contacts[0].id}
                employee={contacts[0]}
                variant={variant}
                organizationID={organizationID}
                deleteContact={deleteContact}
                isClientOrganization={isClientOrganization}
                refetch={refetch}
            />
        );
    }

    return (
        <CollapsingList isCollapsed={isCollapsed} collapse={collapse}>
            {isCollapsed && contacts.length > 0 && (
                <PersonCollapsed
                    contactLength={contacts.length}
                    contact={contacts[0]}
                    expand={expand}
                    variant={variant}
                />
            )}
            {!isCollapsed &&
                contacts.map((employee) => (
                    <PersonExpanded
                        key={employee.id}
                        employee={employee}
                        variant={variant}
                        isClientOrganization={isClientOrganization}
                        organizationID={organizationID}
                        deleteContact={deleteContact}
                        refetch={refetch}
                    />
                ))}
        </CollapsingList>
    );
};
