import React, { FunctionComponent } from "react";
import "./Person.scss";
import { IEmployee } from "../../../config/types";
import classNames from "classnames";
import { UserRoleCheck } from "../../userRoleCheck";
import { PersonInfo } from "./PersonInfo";
import { TPersonVariants } from "./config";

interface IPersonCollapsedProps {
    variant?: TPersonVariants;
    contact: IEmployee;
    expand: () => void;
    contactLength: number;
}

export const PersonCollapsed: FunctionComponent<IPersonCollapsedProps> = ({ expand, variant, contactLength, contact }) => {
    return (
        <button className={classNames("b-person flex align-center", { _cockpit: variant === "cockpit" })} onClick={expand}>
            <PersonInfo {...contact} />
            <p className="b-person__amount">+{contactLength - 1}</p>
            <UserRoleCheck
                availableForRoles={[
                    "owner_administrator",
                    "manufacturer_administrator",
                    "manufacturer_brand_manager",
                    "manufacturer_object_manager",
                    "property_manager_administrator",
                    "property_manager_building_manager",
                    "service_provider_administrator",
                    "service_provider_service_manager",
                ]}
            ></UserRoleCheck>
        </button>
    );
};
