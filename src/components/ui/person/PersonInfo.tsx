import React, { FunctionComponent } from "react";
import { IEmployee } from "../../../config/types";
import { Icon } from "../icons";

export const PersonInfo: FunctionComponent<IEmployee> = ({ user, job }) => {
    return (
        <>
            <Icon icon="person" className="b-person__image" />
            {user && (
                <div className="b-person__info _text-left">
                    <p className="_font-bold b-person__name">
                        {user.first_name} {user.last_name} {job && `(${job.name})`}
                    </p>
                    {user.email && (
                        <p className="_text-grey b-person__contact">
                            <span className="_font-bold">E-Mail:</span> {user.email}
                        </p>
                    )}
                    {user.phone && (
                        <p className="_text-grey b-person__contact">
                            <span className="_font-bold">Phone:</span> {user.phone}
                        </p>
                    )}
                </div>
            )}
        </>
    );
};
