import React, { FunctionComponent } from "react";
import { useModal } from "../modal";
import classNames from "classnames";
import { UserRoleCheck } from "../../userRoleCheck";
import { EditButton, RemoveIconButton } from "../buttons";
import { ConfirmModal } from "../../modals/confirmModal";
import { IEmployee } from "../../../config/types";
import { TPersonVariants } from "./config";
import { PersonInfo } from "./PersonInfo";
import { AddUserModal } from "../../modals/addUserModal";
import { getPhoneInitialValues } from "../../../utils";

interface IPersonProps {
    variant?: TPersonVariants;
    employee: IEmployee;
    organizationID: string;
    isClientOrganization: boolean;
    refetch?: () => void;
    deleteContact: (employee: IEmployee) => void;
}

export const PersonExpanded: FunctionComponent<IPersonProps> = ({
    variant,
    employee,
    deleteContact,
    organizationID,
    isClientOrganization,
    refetch,
}) => {
    const { isOpen: confirmModalIsOpen, hide: confirmModalHide, open: confirmModalOpen } = useModal();
    const { isOpen: editModalIsOpen, hide: editModalHide, open: editModalOpen } = useModal();

    const onRemove = async () => {
        confirmModalHide();
        deleteContact(employee);
    };

    const wrapperClassName = classNames(["b-person", "flex", "_expanded", "align-center", { [`_${variant}`]: variant }]);

    const { user, job, user_role } = employee;
    const { phone, phone_code } = getPhoneInitialValues(user && user.phone);

    const editedUser = user && {
        id: user.id || "",
        email: user.email || "",
        first_name: user.first_name || "",
        last_name: user.last_name || "",
        role: (user_role && user_role.id) || "",
        job_title: (job && job.id) || "",
        language: user.language || "",
        phone: phone || "",
        phone_code: phone_code || "",
    };

    return (
        <div className={wrapperClassName}>
            <PersonInfo {...employee} />
            <UserRoleCheck
                availableForRoles={[
                    "owner_administrator",
                    "manufacturer_administrator",
                    "manufacturer_brand_manager",
                    "manufacturer_object_manager",
                    "property_manager_administrator",
                    "property_manager_building_manager",
                    "service_provider_administrator",
                    "service_provider_service_manager",
                ]}
            >
                <div className="b-person__actions flex align-center">
                    {!isClientOrganization && <EditButton onClick={editModalOpen} />}
                    <RemoveIconButton onClick={confirmModalOpen} />
                </div>
                <ConfirmModal title="Are you sure?" onConfirm={onRemove} hide={confirmModalHide} isOpen={confirmModalIsOpen} />
                <AddUserModal
                    title={editedUser ? "Update Contact" : "New Contact"}
                    organizationID={organizationID}
                    hide={editModalHide}
                    isOpen={editModalIsOpen}
                    editedUser={editedUser}
                    isClientOrganization={isClientOrganization}
                    refetch={refetch}
                />
            </UserRoleCheck>
        </div>
    );
};
