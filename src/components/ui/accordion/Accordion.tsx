import React, { FunctionComponent } from "react";
import "./Accordion.scss";
import { Icon, InfoTooltip } from "../";
import classNames from "classnames";

interface IAccordionProps {
    title: string;
    isOpen: boolean;
    disabled?: boolean;
    toggle?: (index: number) => void;
    index?: number;
    tooltip?: string;
    hideArrow?: boolean;
}

export const Accordion: FunctionComponent<IAccordionProps> = ({
    index,
    isOpen,
    title,
    tooltip,
    toggle,
    children,
    disabled,
    hideArrow = false,
}) => {
    function toggleSelf() {
        toggle && index && toggle(index);
    }

    return (
        <div className={classNames("b-accordion__wrapper", { _open: isOpen, _disabled: disabled })}>
            <button className="b-accordion__header flex justify-between align-center" onClick={toggleSelf}>
                <h3 className="b-accordion__title flex align-center">
                    {title}
                    {tooltip && <InfoTooltip text={tooltip} />}
                </h3>
                {!hideArrow && <Icon icon="accordionArrow" className="b-accordion__arrow" />}
            </button>
            <div className="b-accordion__content">{children}</div>
        </div>
    );
};
