import React, { FunctionComponent } from "react";
import "./ActionList.scss";

interface IActionList {
    children: React.ReactNodeArray | React.ReactNode;
}

export const ActionList: FunctionComponent<IActionList> = ({ children }) => {
    return (
        <div className="b-action-list flex align-center">
            {children && children instanceof Array ? (
                children.filter(Boolean).map((child: React.ReactNode, index: number) => (
                    <div className="b-action-list__item flex align-center" key={index}>
                        {child}
                    </div>
                ))
            ) : (
                <div className="b-action-list__item flex align-center">{children}</div>
            )}
        </div>
    );
};
