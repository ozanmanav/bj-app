import React, { FunctionComponent } from "react";
import ReactPaginate from "react-paginate";

import "./Pagination.scss";

export interface PaginationProps {
    pageCount: number;
    onPageChange: (selectedItem: { selected: number }) => void;
    marginPagesDisplayed: number;
    pageRangeDisplayed: number;
}

const Pagination: FunctionComponent<PaginationProps> = ({ pageCount, onPageChange, marginPagesDisplayed, pageRangeDisplayed }) => {
    return (
        <ReactPaginate
            previousLabel={"previous"}
            nextLabel={"next"}
            breakLabel={"..."}
            breakLinkClassName={"flex-inline justify-center align-center button Pagination-button"}
            pageCount={pageCount}
            marginPagesDisplayed={marginPagesDisplayed}
            pageRangeDisplayed={pageRangeDisplayed}
            onPageChange={onPageChange}
            containerClassName={"flex-inline justify-center align-center Pagination"}
            pageLinkClassName={"flex-inline justify-center align-center button Pagination-button"}
            previousLinkClassName={"flex-inline justify-center align-center button Pagination-item"}
            nextLinkClassName={"flex-inline justify-center align-center button Pagination-item"}
            disabledClassName={"Pagination-button__disabled"}
            activeLinkClassName={"Pagination-button__primary"}
        />
    );
};

export default Pagination;
