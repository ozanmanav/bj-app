import React, { ChangeEvent, FunctionComponent, HTMLAttributes, useState } from "react";
import { Button, Icon, SearchInput } from "../";
import { useClickOutside } from "../../../hooks";
import classnames from "classnames";

const MAX_ITEMS_DISPLAYED = 5;

export type TFilterDropDownOption = {
    value: string;
    label: string;
};

// TODO: remove optionality
interface FilterDropdownProps extends HTMLAttributes<HTMLDivElement> {
    text: string;
    small?: boolean;
    items?: TFilterDropDownOption[];
    onItemClick?: (value: string) => void;
}

export const FilterDropdown: FunctionComponent<FilterDropdownProps> = ({ text, small, className, items = [], onItemClick }) => {
    const [isOpened, setIsOpened] = useState<boolean>(false);
    const [search, setSearch] = useState<string>("");
    const [filteredBy, setFilteredBy] = useState<string | null>(null);
    const wrapperRef = useClickOutside<HTMLDivElement>(hideDropdown);

    const wrapperClassName = classnames(["b-filter-dropdown__wrapper", { _opened: isOpened }, { _small: small }, className]);

    const filteredItems = items
        .filter(({ label }) => label && label.toLowerCase().includes(search.toLowerCase()))
        .slice(0, MAX_ITEMS_DISPLAYED);

    const buttonText = filteredBy ? `Filtered by ${filteredBy}` : text;

    function toggleIsOpened() {
        setIsOpened(!isOpened);
    }

    function hideDropdown() {
        // avoiding unnecessary calls in useClickOutside hook
        if (isOpened) {
            setIsOpened(false);
        }
    }

    function handleItemClick(item: TFilterDropDownOption) {
        hideDropdown();

        setFilteredBy(item.label);

        if (onItemClick) {
            onItemClick(item.value);
        }
    }

    function handleSearchChange(e: ChangeEvent<HTMLInputElement>) {
        setSearch(e.target.value);
    }

    function resetFilter() {
        setFilteredBy("");

        if (onItemClick) {
            onItemClick("");
        }
    }

    return (
        <div className={wrapperClassName} ref={wrapperRef}>
            <Button text={buttonText} className="b-filter-dropdown__button" onClick={toggleIsOpened} type="button" />
            {isOpened && (
                <div className="b-filter-dropdown__content">
                    <SearchInput value={search} onChange={handleSearchChange} />
                    <div className="b-filter-dropdown__results">
                        {filteredBy && (
                            <button className="b-filter-dropdown__results-item _reset" type="button" onClick={resetFilter}>
                                Reset filter <Icon icon="remove" />
                            </button>
                        )}
                        {filteredItems.map((item) => (
                            <FilterDropdownItem key={item.value} item={item} onClick={handleItemClick} />
                        ))}
                    </div>
                </div>
            )}
        </div>
    );
};

// TODO: remove optionality
interface IFilterDropdownItemProps {
    item: TFilterDropDownOption;
    onClick: (value: TFilterDropDownOption) => void;
}

const FilterDropdownItem: FunctionComponent<IFilterDropdownItemProps> = ({ item, onClick }) => {
    function onSelfClick() {
        onClick(item);
    }

    return (
        <button className="b-filter-dropdown__results-item" type="button" onClick={onSelfClick}>
            {item.label}
        </button>
    );
};
