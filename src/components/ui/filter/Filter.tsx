import React, { FunctionComponent } from "react";
import "./Filter.scss";
import { Button, Select, DatepickerDouble, ClocksButton } from "../";
import { FilterDropdown } from "./FilterDropdown";

interface IFilterProps {
    onApply: () => void;
    dateStart?: string;
    dateEnd?: string;
    dateDropdown?: boolean;
    typeDropdown?: boolean;
    clocksButton?: boolean;
}

export const Filter: FunctionComponent<IFilterProps> = ({
    typeDropdown,
    dateDropdown,
    dateStart,
    dateEnd,
    clocksButton,
}) => {
    return (
        <div className="flex align-center b-filter">
            <div className="flex align-center b-filter__dropdowns-wrapper">
                {clocksButton && <ClocksButton className="b-filter__clocks" />}
                {typeDropdown && <FilterDropdown text="Filter by object type" />}
                {dateDropdown && (
                    <Select
                        options={[]}
                        button
                        placeholder="This week"
                        className="b-filter__element"
                        marginBottom="none"
                    />
                )}
            </div>
            {dateStart && (
                <>
                    <DatepickerDouble
                        className="b-filter__element"
                        onChange={(state) => {
                            return null;
                        }}
                    />
                </>
            )}
            <Button text="Apply" primary />
        </div>
    );
};
