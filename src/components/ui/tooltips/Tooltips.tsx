import React, { FunctionComponent } from "react";
import "./Tooltips.scss";
import { Icon, EVENT_ICONS } from "../";
import isEmpty from "ramda/es/isEmpty";
import { NavLink } from "react-router-dom";
import { format } from "date-fns";
import { parseDate } from "../datepicker/utils";
import { IEvent } from "../../../views/app/event/eventDetail/definitions";
import get from "lodash.get";
import { DATE_FORMAT_WITH_TIME, DATE_FORMAT } from "../../../config";
import { cleanAndLowerCase } from "../../events/EventsTimeline/Timeline/utils";

interface TooltipProps {
    isOpen: boolean;
}

interface TimelineTooltipProps extends TooltipProps {
    event: IEvent;
}

export const TimelineTooltip: FunctionComponent<TimelineTooltipProps> = ({ event, isOpen }) => {
    const { id, start_date, name, related_objects, related_organizations, organization, object, type, ends_on, files } = event;

    const formattedStartDate = format(parseDate(start_date, DATE_FORMAT_WITH_TIME), DATE_FORMAT);

    const variantName = get(organization, "name") || get(object, "name");
    const eventType = cleanAndLowerCase(get(type, "name"));

    if (!isOpen) {
        return null;
    }

    return (
        <div className="b-timeline-tooltip">
            <div className="b-timeline-tooltip__header">
                <span className="flex flex-row justify-center align-center">
                    {ends_on === "continuous" && (
                        <img className="b-timeline-tooltip__header-img" src={EVENT_ICONS["refresh"]} alt={"icon"} width="10" height="10" />
                    )}
                    <p className="h6 _text-grey">
                        {"  "}
                        {formattedStartDate}
                    </p>
                </span>

                <NavLink to={`/app/event/${id}`}>
                    <div className="_text-white">
                        <span className="flex flex-row justify-center align-center">
                            <img src={EVENT_ICONS[eventType] ? EVENT_ICONS[eventType] : EVENT_ICONS["abriss"]} alt={"icon"} />
                            <p>{name}</p> <br />
                            {files.length > 0 && (
                                <img
                                    className="b-timeline-tooltip__header-img"
                                    src={EVENT_ICONS["paperclip"]}
                                    alt={"icon"}
                                    width="15"
                                    height="15"
                                />
                            )}
                        </span>

                        <div className="b-timeline-tooltip__header-variant _font-bold">{variantName && `by ${variantName}`} </div>
                    </div>
                </NavLink>
            </div>
            <div className="b-timeline-tooltip__footer">
                {/* {user && <p className="_font-bold _text-white">by {user}</p>} */}

                {related_objects && !isEmpty(related_objects) && (
                    <div className="b-timeline-tooltip__related-container">
                        <p className="_font-bold _text-white">Related Objects</p>
                        <div className="flex">
                            {related_objects.map(({ name }, index) => (
                                <span className="b-timeline-tooltip__tag _text-white" key={index}>
                                    {name}
                                </span>
                            ))}
                        </div>{" "}
                    </div>
                )}

                {related_organizations && !isEmpty(related_organizations) && (
                    <div className="b-timeline-tooltip__related-container">
                        <p className="_font-bold _text-white">Related Organizations</p>
                        <div className="flex">
                            {related_organizations.map(({ name }, index) => (
                                <span className="b-timeline-tooltip__tag _text-white" key={index}>
                                    {name}
                                </span>
                            ))}
                        </div>
                    </div>
                )}
            </div>
        </div>
    );
};

interface InfoTooltipProps {
    text: string;
}

export const InfoTooltip: FunctionComponent<InfoTooltipProps> = ({ text }) => {
    return (
        <div className="b-info-tooltip">
            <Icon icon="info" />
            <div className="b-info-tooltip__content">{text}</div>
        </div>
    );
};
