import React, { FunctionComponent, ImgHTMLAttributes } from "react";
import "./Icons.scss";
import { appendClassName } from "../../../utils";

import abriss from "../../../icons/eventIcons/abriss.svg";
import ausfuhrung from "../../../icons/eventIcons/ausfuhrung.svg";
import erfassung from "../../../icons/eventIcons/erfassung.svg";
import ersatz from "../../../icons/eventIcons/ersatz.svg";
import inbetriebnahme from "../../../icons/eventIcons/inbetriebnahme.svg";
import mutation from "../../../icons/eventIcons/mutation.svg";
import renovation from "../../../icons/eventIcons/renovation.svg";
import reparatur from "../../../icons/eventIcons/reparatur.svg";
import sanierung from "../../../icons/eventIcons/sanierung.svg";
import service from "../../../icons/eventIcons/service.svg";
import zone from "../../../icons/eventIcons/zone.svg";
import planung from "../../../icons/eventIcons/planung.svg";
import vertrag from "../../../icons/eventIcons/vertrag.svg";
import pdf from "../../../icons/eventIcons/pdf.svg";
import doc from "../../../icons/eventIcons/doc.svg";

import { IIcons, ICONS } from "./Icons";

export const EVENT_ICONS: IIcons = {
    ...ICONS,
    abriss,
    ausführung: ausfuhrung,
    entwicklung: planung,
    ersatz,
    erfassung,
    inbetriebnahme,
    mutation,
    renovation,
    reparatur,
    sanierung,
    service,
    einzonung: zone,
    umzonung: zone,
    auszonung: zone,
    vorprojekt: planung,
    bauprojekt: planung,
    austausch: ersatz,
    zone,
    wartung: service,
    offerte: vertrag, // to be developed
    auftrag: vertrag, // to be developed
    rapport: pdf, // to be developed
    rechnung: zone, // to be developed
    inkrafttreten: zone, // to be developed
    eintrag: erfassung, // to be developed
    schätzung: vertrag,
    marktwertschätzung: pdf,
    vertragsabschluss: vertrag,
    vertragsbeginn: vertrag,
    kündigung: vertrag,
    vertragsende: vertrag,
    brief: doc,
    bericht: doc,
};

export interface IEventIconProps<T> extends ImgHTMLAttributes<T> {
    icon?: string;
    groupCount?: number;
}

export const EventIcon: FunctionComponent<IEventIconProps<HTMLElement>> = ({ icon, className, groupCount, ...props }) => {
    const figureClassName = appendClassName(`b-event-icon flex justify-center align-center icon_${icon}`, className);

    if (icon && !EVENT_ICONS[icon]) {
        console.log(`Event icon ${icon} does not exist`);
    }

    return (
        <figure className={figureClassName} {...props}>
            {icon ? (
                EVENT_ICONS["icon"] ? (
                    <img src={EVENT_ICONS["icon"]} alt={"icon"} />
                ) : null
            ) : (
                <div className="_text-white">{groupCount}</div>
            )}
        </figure>
    );
};
