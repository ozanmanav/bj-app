import React, { FunctionComponent, ImgHTMLAttributes } from "react";
import "./Icons.scss";
import { appendClassName } from "../../../utils";

import building from "../../../icons/building.svg";
import check from "../../../icons/check.svg";
import padlock from "../../../icons/padlock.svg";
import search from "../../../icons/search.svg";
import selectArrows from "../../../icons/selectArrows.svg";
import street from "../../../icons/street.svg";
import plus from "../../../icons/plus.svg";
import commissioning from "../../../icons/commissioning.svg";
import elevator from "../../../icons/elevator.svg";
import contract from "../../../icons/contract.svg";
import house from "../../../icons/house.svg";
import construction from "../../../icons/construction.svg";
import floor from "../../../icons/floor.svg";
import repair from "../../../icons/repair.svg";
import accordionArrow from "../../../icons/accordionArrow.svg";
import info from "../../../icons/info.svg";
import remove from "../../../icons/remove.svg";
import edit from "../../../icons/edit.svg";
import newFile from "../../../icons/newFile.svg";
import buttonPlus from "../../../icons/buttonPlus.svg";
import plusGrey from "../../../icons/plusGrey.svg";
import checkGrey from "../../../icons/checkGrey.svg";
import branches from "../../../icons/branches.svg";
import person from "../../../icons/person.svg";
import arrowDown from "../../../icons/arrowDown.svg";
import arrowTop from "../../../icons/arrowTop.svg";
import datepickerArrow from "../../../icons/datepickerArrow.svg";
import download from "../../../icons/download.svg";
import bell from "../../../icons/bell.svg";
import backArrow from "../../../icons/backArrow.svg";
import calendar from "../../../icons/calendar.svg";
import bellEmpty from "../../../icons/bellEmpty.svg";
import pin from "../../../icons/pin.svg";
import pinGrey from "../../../icons/pinGrey.svg";
import history from "../../../icons/history.svg";
import clocks from "../../../icons/clocks.svg";
import editGrey from "../../../icons/editGrey.svg";
import collapse from "../../../icons/collapse.svg";
import nextArrow from "../../../icons/nextArrow.svg";
import upload from "../../../icons/upload.svg";
import relation from "../../../icons/relation.svg";
import buildingTransparent from "../../../icons/buildingTransparent.svg";
import company from "../../../icons/company.svg";
import parking from "../../../icons/parking.svg";
import exportGrey from "../../../icons/exportGrey.svg";
import exportWhite from "../../../icons/exportWhite.svg";
import calendarGrey from "../../../icons/calendarGrey.svg";
import clocksGrey from "../../../icons/clocksGrey.svg";
import checkBlue from "../../../icons/checkBlue.svg";
import octagon from "../../../icons/octagon.svg";
import customField from "../../../icons/customField.svg";
import bigCheck from "../../../icons/bigCheck.svg";
import chf from "../../../icons/chf.svg";
import nextArrowGrey from "../../../icons/nextArrowGrey.svg";
import sortIcon from "../../../icons/sortIcon.svg";
import tick from "../../../icons/tick.svg";
import paperclip from "../../../icons/paperclip.svg";
import refresh from "../../../icons/refresh.svg";
import structure from "../../../icons/structure.svg";

// TODO: is string type necessary?
export type TIconType =
    | "building"
    | "check"
    | "padlock"
    | "search"
    | "street"
    | "selectArrows"
    | "plus"
    | "commissioning"
    | "elevator"
    | "contract"
    | "house"
    | "construction"
    | "floor"
    | "repair"
    | "accordionArrow"
    | "info"
    | "remove"
    | "edit"
    | "newFile"
    | "buttonPlus"
    | "plusGrey"
    | "checkGrey"
    | "branches"
    | "person"
    | "arrowDown"
    | "arrowTop"
    | "datepickerArrow"
    | "download"
    | "bell"
    | "backArrow"
    | "calendar"
    | "bellEmpty"
    | "pin"
    | "pinGrey"
    | "history"
    | "clocks"
    | "editGrey"
    | "collapse"
    | "nextArrow"
    | "upload"
    | "relation"
    | "buildingTransparent"
    | "company"
    | "parking"
    | "exportGrey"
    | "exportWhite"
    | "calendarGrey"
    | "clocksGrey"
    | "checkBlue"
    | "octagon"
    | "customField"
    | "bigCheck"
    | "chf"
    | "nextArrowGrey"
    | "sortIcon"
    | "tick"
    | "structure"
    | string;

export interface IIcons {
    [key: string]: string;
}

export const ICONS: IIcons = {
    building,
    check,
    padlock,
    search,
    street,
    paperclip,
    refresh,
    selectArrows,
    plus,
    commissioning,
    elevator,
    contract,
    house,
    construction,
    floor,
    repair,
    accordionArrow,
    info,
    remove,
    edit,
    newFile,
    buttonPlus,
    plusGrey,
    checkGrey,
    branches,
    person,
    arrowDown,
    arrowTop,
    datepickerArrow,
    download,
    bell,
    backArrow,
    calendar,
    bellEmpty,
    pin,
    history,
    clocks,
    pinGrey,
    editGrey,
    collapse,
    nextArrow,
    upload,
    relation,
    objectTransparent: buildingTransparent,
    company,
    parking,
    exportGrey,
    exportWhite,
    calendarGrey,
    clocksGrey,
    checkBlue,
    octagon,
    customField,
    bigCheck,
    chf,
    nextArrowGrey,
    sortIcon,
    tick,
    structure,
};

export interface IIconProps<T> extends ImgHTMLAttributes<T> {
    icon: TIconType;
}

export const Icon: FunctionComponent<IIconProps<HTMLImageElement>> = ({ icon, className, ...props }) => {
    if (!ICONS[icon]) {
        console.log(`Icon ${icon} does not exist`);

        return null;
    }

    const imgClassName = appendClassName(`icon_${icon}`, className);

    return <img src={ICONS[icon]} alt={icon} className={imgClassName} {...props} />;
};
