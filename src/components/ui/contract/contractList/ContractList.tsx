import React, { FunctionComponent } from "react";
import { CollapsingList, useCollapsingList, ContractCollapsed, ContractExpanded, TContractVariants } from "../../";
import { IContract, IObjectMinimal } from "../../../../config/types";

interface IContractListProps {
    variant?: TContractVariants;
    contracts: IContract[];
    objectId: string;
    objects?: IObjectMinimal[];
    refetch: () => void;
}

export const ContractsList: FunctionComponent<IContractListProps> = ({ variant, contracts, objectId, objects, refetch }) => {
    const { isCollapsed, collapse, expand } = useCollapsingList();
    return (
        <CollapsingList isCollapsed={isCollapsed} collapse={collapse}>
            {contracts.length &&
                (isCollapsed ? (
                    <ContractCollapsed expand={expand} variant={variant} countContract={contracts.length} objects={objects} />
                ) : (
                    contracts.map((contract: IContract) => (
                        <ContractExpanded
                            key={contract.id}
                            contract={contract}
                            variant={variant}
                            objectId={objectId}
                            refetch={refetch}
                            objects={objects}
                        />
                    ))
                ))}
        </CollapsingList>
    );
};
