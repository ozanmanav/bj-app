import React, { FunctionComponent } from "react";
import "./AffectedObjectsModal.scss";
import { IModalProps, Modal } from "../../modal";
import { ObjectName } from "../../../objectName";
import { IObjectMinimal } from "../../../../config/types";
import { useSortingTable } from "../../../../hooks";
import { SortButton } from "../../buttons";
import orderBy from "lodash.orderby";

const SORT_STRINGS = {
    NAME: "name",
};

interface IAffectedObjectsModalProps extends IModalProps {
    objects?: IObjectMinimal[];
}

export const AffectedObjectsModal: FunctionComponent<IAffectedObjectsModalProps> = ({ objects = [], ...props }) => {
    const { sortBy, sortingOrder, getSortOrderForField, changeSortBy } = useSortingTable({ localStorageKey: "contract_affected_objects" });

    const sortedObjects = orderBy(objects, [sortBy], [sortingOrder]);

    function sortByName() {
        changeSortBy(SORT_STRINGS.NAME);
    }

    return (
        <Modal {...props} className="b-affected-objects-modal">
            <h2 className="h1 b-affected-objects-modal__title">Affected object</h2>
            <div className="b-table__wrapper b-affected-objects-modal__content">
                <table className="b-table _first-cell-border">
                    <thead className="b-table__head">
                        <tr className="b-table__row _head">
                            <th className="b-table__cell">{objects.length}</th>
                            <th className="b-table__cell">
                                <SortButton text="Object name" onClick={sortByName} sorted={getSortOrderForField(SORT_STRINGS.NAME)} />
                            </th>
                        </tr>
                    </thead>
                    <tbody className="b-table__body">
                        {sortedObjects.map((object, index) => (
                            <tr className="b-table__row" key={object.id}>
                                <td className="b-table__cell">{index + 1}</td>
                                <td className="b-table__cell">
                                    <ObjectName {...object} />
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </Modal>
    );
};
