import React, { FunctionComponent } from "react";
import "./Contract.scss";
import {
    CheckboxSwitch,
    EditButton,
    handleGraphQLErrors,
    CalendarAddButton,
    RemoveIconButton,
    showErrorToast,
    showSuccessToast,
    useModal,
} from "../";
import { IContract, IMetafield, IObjectMinimal } from "../../../config/types";
import classNames from "classnames";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { ConfirmModal } from "../../modals/confirmModal";
import { ContractModal } from "../../modals/contractModal";
import { AffectedObjectsModal } from "./affectedObjectsModal";
import { FilesList } from "../../filesList";
import { UserRoleCheck } from "../../userRoleCheck";
import get from "lodash.get";

export type TContractVariants = "cockpit";

const DELETE_CONTRACT = gql`
    mutation DeleteContract($id: String!) {
        deleteContract(id: $id)
    }
`;

interface IContractProps {
    variant?: TContractVariants;
    contract: IContract;
    objectId: string;
    refetch: () => void;
    // TODO: remove possible undefined after "Relational list" update
    objects?: IObjectMinimal[];
}

interface IContractCollapsedProps {
    variant?: TContractVariants;
    expand: () => void;
    countContract: number;
    // TODO: remove possible undefined after "Relational list" update
    objects?: IObjectMinimal[];
}

export const ContractCollapsed: FunctionComponent<IContractCollapsedProps> = ({ expand, variant, objects, countContract }) => {
    const { hide, open, isOpen } = useModal();

    const moreContracts = countContract - 1;

    return (
        <button
            className={classNames("b-contract flex align-center justify-between", { _cockpit: variant === "cockpit" })}
            onClick={expand}
        >
            <p className="b-contract__info">
                affecting{" "}
                <button className="_text-primary _font-bold" onClick={open}>
                    {objects ? objects.length : 0} object
                </button>
                .
            </p>
            {moreContracts > 0 && <p className="b-contract__amount">+{moreContracts}</p>}
            {isOpen && <AffectedObjectsModal objects={objects} hide={hide} isOpen={isOpen} />}
        </button>
    );
};

export const ContractExpanded: FunctionComponent<IContractProps> = ({ variant, contract, objectId, objects, refetch }) => {
    const [removeContactMutation] = useMutation(DELETE_CONTRACT);
    const { isOpen: isConfirmModalOpen, hide: confirmModalHide, open: confirmModalOpen } = useModal();
    const { isOpen: isEditModalOpen, hide: editModalHide, open: editModalOpen } = useModal();
    const { isOpen: isAffectingObjectsModalOpen, hide: affectingObjectsModalHide, open: affectingObjectsModalOpen } = useModal();
    const contractFiles = get(contract, "files") || [];

    const onRemove = async () => {
        try {
            confirmModalHide();

            const res = await removeContactMutation({
                variables: {
                    id: contract.id,
                },
            });
            const { errors } = res;

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                refetch();
                showSuccessToast("Contract is successfully deleted");
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    };
    const contractDetails = get(contract, "details") || [];

    return (
        <div className={classNames("b-contract _expanded", { _cockpit: variant === "cockpit" })}>
            <div className="flex align-center justify-between">
                <div className="b-contract__info">
                    <p className="b-contract__info-affecting">
                        affecting{" "}
                        <button className="_text-primary _font-bold" onClick={affectingObjectsModalOpen}>
                            {objects ? objects.length : 0} object.
                        </button>
                    </p>
                    {contractDetails.map((field: IMetafield, index: number) => (
                        <p className="b-contract__info-field" key={index}>
                            <span className="b-contract__info-field-key">
                                {field.name}:{"  "}
                            </span>
                            {field.type === "date" ? (
                                <span className="b-contract__info-field-value">{field.value.slice(0, 10)}</span>
                            ) : (
                                <span className="b-contract__info-field-value">
                                    {"  "}
                                    {field.value}
                                </span>
                            )}
                        </p>
                    ))}
                </div>
                <div className="b-contract__actions flex align-center">
                    <CheckboxSwitch />
                    <UserRoleCheck
                        availableForRoles={[
                            "owner_administrator",
                            "manufacturer_administrator",
                            "manufacturer_brand_manager",
                            "manufacturer_object_manager",
                            "property_manager_administrator",
                            "property_manager_building_manager",
                            "service_provider_administrator",
                            "service_provider_service_manager",
                        ]}
                    >
                        <EditButton onClick={editModalOpen} />
                        <CalendarAddButton />
                        <RemoveIconButton onClick={confirmModalOpen} />
                    </UserRoleCheck>
                </div>
            </div>
            {contractFiles.length > 0 && <FilesList files={contract.files} variant="contract" />}

            {isEditModalOpen && (
                <ContractModal
                    objectID={objectId}
                    contractID={contract.id}
                    refetch={refetch}
                    hide={editModalHide}
                    isOpen={isEditModalOpen}
                />
            )}
            <ConfirmModal title="Are you sure?" onConfirm={onRemove} hide={confirmModalHide} isOpen={isConfirmModalOpen} />
            {isAffectingObjectsModalOpen && (
                <AffectedObjectsModal objects={objects} hide={affectingObjectsModalHide} isOpen={isAffectingObjectsModalOpen} />
            )}
        </div>
    );
};
