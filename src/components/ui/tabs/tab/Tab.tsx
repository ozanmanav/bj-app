import React, { FunctionComponent } from "react";
import "./Tab.scss";
import classNames from "classnames";

interface ITabProps {
    activeTab: string;
    label: string;
    onClick: (name: string) => void;
}

export const Tab: FunctionComponent<ITabProps> = ({ activeTab, label, onClick }) => {
    const onTabClick = () => onClick(label);

    return (
        <li className={classNames("b-tab", { _active: activeTab === label })} onClick={onTabClick}>
            <div className="b-tab__inner">{label}</div>
        </li>
    );
};
