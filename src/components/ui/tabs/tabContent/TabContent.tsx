import React, { FunctionComponent } from "react";
import "./TabContent.scss";

interface ITabContentProps {
    children: React.ReactNode;
    label: string;
}

export const TabContent: FunctionComponent<ITabContentProps> = ({ children }) => {
    return <div className="b-tab-content">{children}</div>;
};
