import React, { FunctionComponent, useState } from "react";
import "./Tabs.scss";
import { Tab } from "./tab";
import get from "lodash.get";

interface ITabsProps {
    children: React.ReactNodeArray;
}

export const Tabs: FunctionComponent<ITabsProps> = ({ children }) => {
    const [activeTab, setActiveTab] = useState<string>(get(children, "[0].props.label"));

    function onClickTab(name: string) {
        setActiveTab(name);
    }

    return (
        <div className="b-tabs">
            <ul className="b-tabs__nav">
                {children.map((child: any) => {
                    const { label } = child.props;

                    return <Tab activeTab={activeTab} label={label} key={label} onClick={onClickTab} />;
                })}
            </ul>
            <div className="b-tabs__content">
                {children.map((child: any) => {
                    if (child.props.label !== activeTab) return undefined;
                    return child.props.children;
                })}
            </div>
        </div>
    );
};
