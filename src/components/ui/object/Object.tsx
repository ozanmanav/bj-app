import React, { FunctionComponent } from "react";
import "./Object.scss";
import { Icon, CalendarAddButton, ArrowDownButton, EditButton, RemoveIconButton } from "../";

interface ObjectCollapsedProps {
    expand: () => void;
}

export const ObjectCollapsed: FunctionComponent<ObjectCollapsedProps> = ({ expand }) => {
    return (
        <div className="b-object flex align-center justify-between">
            <div className="b-object__info flex align-center">
                <Icon icon="elevator" className="b-object__icon" />
                <span className="b-object__name _font-bold">Elevators</span>
                <span className="b-object__amount _text-grey flex align-center justify-center _font-bold">5</span>
            </div>
            <div className="b-object__actions flex align-center">
                <ArrowDownButton onClick={expand} />
                <CalendarAddButton />
            </div>
        </div>
    );
};

export const ObjectExpanded: FunctionComponent = () => {
    return (
        <div className="b-object _expanded flex align-center justify-between">
            <div className="b-object__info flex align-center">
                <Icon icon="elevator" className="b-object__icon" />
                <span className="b-object__name _font-bold">Elevators</span>
            </div>
            <div className="b-object__actions flex align-center">
                <EditButton />
                <CalendarAddButton />
                <RemoveIconButton />
            </div>
        </div>
    );
};
