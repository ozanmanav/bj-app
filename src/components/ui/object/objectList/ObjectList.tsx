import React, { FunctionComponent } from "react";
import "./ObjectList.scss";
import { CollapsingList, useCollapsingList, ObjectCollapsed, ObjectExpanded } from "../../";

export const ObjectList: FunctionComponent = () => {
    const { isCollapsed, collapse, expand } = useCollapsingList();

    return (
        <CollapsingList isCollapsed={isCollapsed} collapse={collapse}>
            {isCollapsed ? (
                <ObjectCollapsed expand={expand} />
            ) : (
                <>
                    <ObjectExpanded />
                    <ObjectExpanded />
                    <ObjectExpanded />
                </>
            )}
        </CollapsingList>
    );
};
