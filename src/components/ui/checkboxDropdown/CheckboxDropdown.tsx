import React, { ChangeEvent, FunctionComponent, HTMLAttributes, useState } from "react";
import "./CheckboxDropdown.scss";
import classnames from "classnames";
import { Button } from "../buttons";
import { useClickOutside } from "../../../hooks";
import { CheckboxSwitch } from "../inputs";

export interface ICheckboxDropdownItemState {
    value: boolean;
    name: string;
    sortable: boolean;
    label: string;
}

interface ICheckboxDropdownProps extends HTMLAttributes<HTMLDivElement> {
    checkboxes: ICheckboxDropdownItemState[];
    handleChange: (name: string, value: boolean) => void;
}

export const CheckboxDropdown: FunctionComponent<ICheckboxDropdownProps> = ({ className, checkboxes, handleChange }) => {
    const [isOpened, setIsOpened] = useState<boolean>(false);

    const wrapperRef = useClickOutside<HTMLDivElement>(hideDropdown);

    function toggleIsOpened() {
        setIsOpened(!isOpened);
    }

    function hideDropdown() {
        // avoiding unnecessary calls in useClickOutside hook
        if (isOpened) {
            setIsOpened(false);
        }
    }

    const wrapperClassname = classnames(["b-checkbox-dropdown__wrapper", { _opened: isOpened }, className]);

    return (
        <div className={wrapperClassname} ref={wrapperRef}>
            <Button text="Select columns" className="b-checkbox-dropdown__button" onClick={toggleIsOpened} type="button" />
            {isOpened && (
                <div className="b-checkbox-dropdown__content">
                    <div className="b-checkbox-dropdown__results">
                        {checkboxes.map((checkbox) => (
                            <CheckboxDropdownItem handleChange={handleChange} key={checkbox.label} {...checkbox} />
                        ))}
                    </div>
                </div>
            )}
        </div>
    );
};

interface ICheckboxDropdownItemProps extends ICheckboxDropdownItemState {
    handleChange: (name: string, value: boolean) => void;
}

const CheckboxDropdownItem: FunctionComponent<ICheckboxDropdownItemProps> = ({ value, label, name, handleChange }) => {
    function onSelfChange(e: ChangeEvent<HTMLInputElement>) {
        handleChange(name, e.target.checked);
    }

    return (
        <label className="b-checkbox-dropdown__results-item flex align-center justify-between _text-uppercase _font-bold h6">
            <span className="b-checkbox-dropdown__results-item-label">{label}</span>
            <CheckboxSwitch name={name} checked={value} onChange={onSelfChange} />
        </label>
    );
};
