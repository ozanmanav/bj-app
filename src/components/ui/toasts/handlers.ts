import { toast } from "react-toastify";

export function handleGraphQLErrors(errors: any) {
    if (errors instanceof Array) {
        errors.forEach(({ message }) => {
            showErrorToast(message);
        });
    } else if (typeof errors === "string") {
        showErrorToast(errors);
    }
}

export function showErrorToast(message: string) {
    toast(message, {
        type: "error",
    });
}

export function showSuccessToast(message: string) {
    toast(message, {
        type: "success",
    });
}
