import gql from "graphql-tag";

// TODO: details deleted from here, i think it's not used, if used should be implement different query.
export const AUTOCOMPLETE_SEARCH_QUERIES = {
    allOrganizations: gql`
        query allOrganizations($searchBy: String!) {
            organizations(name: $searchBy) {
                data {
                    id
                    name
                    category
                }
            }
        }
    `,
    clientOrganizations: gql`
        query clientOrganizations($searchBy: String!) {
            organizations(name: $searchBy, category: CLIENT) {
                data {
                    id
                    name
                    type {
                        type
                    }
                }
            }
        }
    `,
    nonClientOrganizations: gql`
        query nonClientOrganizations($searchBy: String!) {
            organizations(name: $searchBy, category: NON_CLIENT) {
                data {
                    id
                    name
                    type {
                        type
                    }
                }
            }
        }
    `,
    objects: gql`
        query Objects($searchBy: String!, $parent_id: String, $address_search: AddressSearch, $scope: ScopeEnum, $object_id: String) {
            objects(name: $searchBy, parent_id: $parent_id, address_search: $address_search, scope: $scope, object_id: $object_id) {
                data {
                    id
                    name
                    type {
                        id
                        name
                    }
                    address_refs
                    address
                }
            }
        }
    `,
    buildings: gql`
        query Buildings($searchBy: String!) {
            buildings(name: $searchBy) {
                data {
                    id
                    name
                    type {
                        id
                        name
                    }
                }
            }
        }
    `,
    children: gql`
        query Children($searchBy: String!) {
            children(parent_id: $searchBy, is_file: false) {
                id
                name
                type
            }
        }
    `,
    employee: gql`
        query Employees($searchBy: String, $organization_id: String!) {
            employee(name: $searchBy, organization_id: $organization_id) {
                data {
                    id
                    job {
                        id
                        name
                        type
                    }
                    user {
                        id
                        name
                        first_name
                        last_name
                        email
                        phone
                        active
                        language
                    }
                }
            }
        }
    `,
    users: gql`
        query Users($searchBy: String!) {
            users(name: $searchBy) {
                data {
                    id
                    name
                    email
                    phone
                }
            }
        }
    `,
    events: gql`
        query Events($searchBy: String!, $organization_id: String, $object_id: String) {
            events(name: $searchBy, organization_id: $organization_id, object_id: $object_id) {
                data {
                    id
                    name
                }
            }
        }
    `,
    tasks: gql`
        query Tasks($searchBy: String!) {
            tasks(name: $searchBy) {
                data {
                    id
                    name
                }
            }
        }
    `,
};

export interface IAutoCompleteUser {
    name?: string;
    email?: string;
    id: string;
}
