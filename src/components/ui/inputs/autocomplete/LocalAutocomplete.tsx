import React, { ChangeEvent, FunctionComponent, useEffect, useState } from "react";
import "./AutocompleteInputs.scss";
import { IInputProps, MAX_AUTOCOMPLETE_RESULTS } from "../config";
import { useClickOutside } from "../../../../hooks";
import classNames from "classnames";
import { SearchInput } from "../input";

export interface ILocalAutocompleteOption {
    id: string;
    label: string;
    value: string;
}

interface ILocalAutocompleteResultsProps {
    options: ILocalAutocompleteOption[];
    onItemClick: (option: ILocalAutocompleteOption) => void;
}

const LocalAutocompleteResults: FunctionComponent<ILocalAutocompleteResultsProps> = ({ options, onItemClick }) => {
    const renderedData = options.slice(0, MAX_AUTOCOMPLETE_RESULTS);

    return (
        <div className="b-autocomplete">
            {renderedData.map((option: ILocalAutocompleteOption) => (
                <LocalAutocompleteResultsItem key={option.id} option={option} onClick={onItemClick} />
            ))}
            {renderedData.length === 0 && <p className="b-autocomplete__item _nothing">Nothing found</p>}
        </div>
    );
};

interface ILocalAutocompleteResultsItemProps {
    option: ILocalAutocompleteOption;
    onClick: (option: ILocalAutocompleteOption) => void;
}

const LocalAutocompleteResultsItem: FunctionComponent<ILocalAutocompleteResultsItemProps> = ({ option, onClick }) => {
    function onSelfClick() {
        onClick(option);
    }

    return (
        <button className="b-autocomplete__item" onClick={onSelfClick} type="button">
            {option.label}
        </button>
    );
};

interface ILocalAutocompleteProps extends IInputProps {
    onChange: (item: any) => void;
    onInputChange?: (inputValue: string) => void;
    options: ILocalAutocompleteOption[];
    position?: "top";
    resultsContainerSize?: "small";
    clearInputAfterSelect?: boolean;
    defaultInputValue?: string;
}

export const LocalAutocomplete: FunctionComponent<ILocalAutocompleteProps> = ({
    className,
    options,
    position,
    onInputChange,
    resultsContainerSize,
    defaultInputValue,
    clearInputAfterSelect = true,
    marginBottom,
    ...props
}) => {
    const [inputValue, setInputValue] = useState<string>(defaultInputValue || "");
    const [isFocused, setIsFocused] = useState<boolean>(false);
    const wrapperRef = useClickOutside<HTMLDivElement>(onBlur);
    const [autocompleteResults, setAutocompleteResults] = useState(options);

    useEffect(() => {
        const results = options.filter((option) => {
            return option.label.toLowerCase().indexOf(inputValue ? inputValue.toLowerCase() : "") !== -1;
        });

        setAutocompleteResults(results);
    }, [inputValue, options]);

    useEffect(() => {
        if (defaultInputValue) {
            setInputValue(defaultInputValue);
        }
    }, [defaultInputValue]);

    function onChange(e: ChangeEvent<HTMLInputElement>) {
        onInputChange && onInputChange(e.target.value);
        setInputValue(e.target.value);
    }

    function onBlur() {
        setIsFocused(false);
    }

    function onFocus() {
        setIsFocused(true);
    }

    function handleItemClick(option: ILocalAutocompleteOption) {
        const selectedOption = options.find(({ value }) => value === option.value);
        props.onChange(selectedOption);
        onBlur();
        clearInputAfterSelect && setInputValue("");
    }

    const wrapperClassName = classNames([
        "b-autocomplete__wrapper",
        className,
        { _opened: isFocused },
        { [`_margin-bottom-${marginBottom}`]: marginBottom },
        { [`_${position}`]: position },
        { [`_${resultsContainerSize}`]: resultsContainerSize },
    ]);

    return (
        <div className={wrapperClassName} ref={wrapperRef} onFocus={onFocus}>
            <SearchInput {...props} value={inputValue} onChange={onChange} marginBottom="none" />
            {isFocused && <LocalAutocompleteResults options={autocompleteResults} onItemClick={handleItemClick} />}
        </div>
    );
};
