import React, { ChangeEvent, FunctionComponent, useEffect, useState } from "react";
import "./AutocompleteInputs.scss";
import { useQuery } from "@apollo/react-hooks";
import { AUTOCOMPLETE_DEBOUNCE_TIME_MS, IInputProps, MAX_AUTOCOMPLETE_RESULTS } from "../config";
import { useDebounce } from "use-debounce";
import { useClickOutside } from "../../../../hooks";
import classNames from "classnames";
import { SearchInput } from "../input";
import get from "lodash.get";
import { AUTOCOMPLETE_SEARCH_QUERIES } from "./config";
import { isNillOrEmpty } from "../../../../utils";

export type TAutocompleteAvailableSearchQueries =
    | "allOrganizations"
    | "nonClientOrganizations"
    | "clientOrganizations"
    | "objects"
    | "buildings"
    | "users"
    | "employee"
    | "events"
    | "tasks";

interface IAutocompleteInputProps extends IInputProps {
    searchFor: TAutocompleteAvailableSearchQueries;
    searchScope?: string;
    scopeObjectID?: string;
    parentID?: string;
    searchOnStreet?: string;
    searchInCity?: string;
    organizationID?: string;
    onChange: (item: any) => void;
    isRefetch?: boolean;
    setRefetch?: (state: boolean) => void;
    isSetValue?: boolean;
    defaultValue?: string;
    defaultInputValue?: string;
    position?: "top";
    resultsContainerSize?: "small";
}

export const AutocompleteInput: FunctionComponent<IAutocompleteInputProps> = ({
    searchFor,
    searchScope,
    scopeObjectID,
    organizationID,
    parentID,
    className,
    isRefetch,
    setRefetch,
    defaultValue,
    defaultInputValue = "",
    isSetValue = true,
    searchOnStreet,
    searchInCity,
    marginBottom,
    position,
    resultsContainerSize,
    ...props
}) => {
    const [inputValue, setInputValue] = useState<string>(defaultInputValue);
    const [isFocused, setIsFocused] = useState<boolean>(false);
    const wrapperRef = useClickOutside<HTMLDivElement>(onBlur);
    const isOrganization =
        searchFor === "allOrganizations" || searchFor === "nonClientOrganizations" || searchFor === "clientOrganizations";
    const [searchBy] = useDebounce(inputValue, AUTOCOMPLETE_DEBOUNCE_TIME_MS);

    const { data, loading, refetch } = useQuery(AUTOCOMPLETE_SEARCH_QUERIES[searchFor], {
        skip: searchBy === "",
        variables: {
            searchBy,
            scope: searchScope,
            ...(!isNillOrEmpty(parentID) && { parent_id: parentID }),
            ...(!isNillOrEmpty(organizationID) && { organization_id: organizationID }),
            ...(!isNillOrEmpty(scopeObjectID) && { object_id: scopeObjectID }),
            ...(!isNillOrEmpty(searchOnStreet) && {
                address_search: {
                    street: searchOnStreet,
                },
            }),
            ...(!isNillOrEmpty(searchInCity) && {
                address_search: {
                    city: searchInCity,
                },
            }),
        },
        fetchPolicy: "network-only",
    });

    // refetching after changing list outside
    useEffect(() => {
        if (isRefetch && setRefetch) {
            refetch();
            setRefetch(false);
        }
    }, [isRefetch]);

    useEffect(() => {
        if (!loading && get(data, isOrganization ? "organizations" : searchFor) && defaultValue && inputValue === "") {
            const selectedItem = get(data, searchFor).find((field: any) => defaultValue === field.id);

            if (selectedItem) {
                setInputValue(selectedItem.name);
            }
        }
    }, [loading, defaultValue, data, searchFor, inputValue]);

    useEffect(() => {
        if (typeof defaultInputValue === "string") {
            setInputValue(defaultInputValue);
        }
    }, [defaultInputValue]);

    const dataToRender = isOrganization
        ? get(data, "organizations.data")
        : Array.isArray(get(data, searchFor))
        ? get(data, searchFor)
        : get(data, `${searchFor}.data`) || [];

    const showResults = !loading && dataToRender && isFocused;

    function onChange(e: ChangeEvent<HTMLInputElement>) {
        setInputValue(e.target.value);
    }

    function onFocus() {
        setIsFocused(true);
    }

    function onBlur() {
        setIsFocused(false);
    }

    function handleItemClick(id: string) {
        onBlur();

        const clickedItem = dataToRender.find((item: any) => item.id === id);

        if (isSetValue) {
            setInputValue(clickedItem.name);
        } else {
            setInputValue("");
        }

        props.onChange(clickedItem);
    }

    const wrapperClassname = classNames([
        "b-autocomplete__wrapper",
        className,
        { _opened: showResults },
        { [`_margin-bottom-${marginBottom}`]: marginBottom },
        { [`_${position}`]: position },
        { [`_${resultsContainerSize}`]: resultsContainerSize },
    ]);

    return (
        <div className={wrapperClassname} ref={wrapperRef} onFocus={onFocus}>
            <SearchInput {...props} value={inputValue} onChange={onChange} marginBottom="none" />
            {showResults && <AutocompleteResults data={dataToRender} searchFor={searchFor} onItemClick={handleItemClick} />}
        </div>
    );
};

interface IAutocompleteResultsProps {
    data: any;
    onItemClick: (id: string) => void;
    searchFor: TAutocompleteAvailableSearchQueries;
}

const AutocompleteResults: FunctionComponent<IAutocompleteResultsProps> = ({ data, searchFor, onItemClick }) => {
    const renderedData = data.slice(0, MAX_AUTOCOMPLETE_RESULTS);

    return (
        <div className="b-autocomplete">
            {renderedData.map((item: any) => {
                const name = item.name ? item.name : `${item.user.first_name} ${item.user.last_name}`;

                let dataInParenthesis = "";

                if (searchFor === "objects") {
                    const address = get(item, "address") || "";
                    const objectType = get(item, "type.name") || "";
                    dataInParenthesis = `${objectType}${objectType && address ? " - " : ""}${address}`;
                }

                return (
                    <AutocompleteResultsItem
                        key={item.id}
                        id={item.id}
                        name={name}
                        dataInParenthesis={dataInParenthesis}
                        onClick={onItemClick}
                    />
                );
            })}
            {renderedData.length === 0 && <p className="b-autocomplete__item _nothing">Nothing found</p>}
        </div>
    );
};

const AutocompleteResultsItem: FunctionComponent<{
    name: string;
    id: string;
    dataInParenthesis: string;
    onClick: (id: string) => void;
}> = ({ name, dataInParenthesis, id, onClick }) => {
    function onSelfClick() {
        onClick(id);
    }

    return (
        <button className="b-autocomplete__item" onClick={onSelfClick}>
            {name} {dataInParenthesis && `(${dataInParenthesis})`}
        </button>
    );
};
