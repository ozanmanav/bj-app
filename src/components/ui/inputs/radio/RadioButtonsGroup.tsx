import React, { FunctionComponent, SyntheticEvent, ReactText } from "react";
import "../Input.scss";
import { IRadioProps, IRadioMultipleProps } from "../config";
import classNames from "classnames";
import contains from "ramda/es/contains";

const RadioButton: FunctionComponent<IRadioProps> = ({ className, marginBottom = "normal", value, selected, ...props }) => {
    const inputClassName = classNames([
        "f-input _radio-btn",
        { [`_margin-bottom-${marginBottom}`]: marginBottom !== "none" },
        { _selected: selected },
        className,
    ]);

    return (
        <label className={inputClassName}>
            {value}
            <input type="radio" value={value} {...props} hidden />
        </label>
    );
};

const RadioButtonMultiple: FunctionComponent<IRadioMultipleProps> = ({
    className,
    marginBottom = "normal",
    label,
    value,
    selected,
    onChange,
    onClickButton,
    ...props
}) => {
    const inputClassName = classNames([
        "f-input _radio-btn",
        { [`_margin-bottom-${marginBottom}`]: marginBottom !== "none" },
        { _selected: selected },
        className,
    ]);

    const onClick = (event: React.MouseEvent<HTMLLabelElement, MouseEvent>) => {
        event.preventDefault();
        onClickButton(value);
    };

    return (
        <label className={inputClassName} onClick={onClick}>
            {label}
            <input {...props} type="radio" value={value} hidden />
        </label>
    );
};

interface IRadioButtonsGroupProps {
    inputs: IRadioProps[];
    name: string;
    selected: string | number;
    onChange: (e: SyntheticEvent) => void;
    wrapperClassName?: string;
}

export const RadioButtonsGroup: FunctionComponent<IRadioButtonsGroupProps> = ({ wrapperClassName, inputs, name, selected, onChange }) => {
    const wrapperElClassName = classNames(["flex", wrapperClassName]);

    return (
        <div className={wrapperElClassName}>
            {inputs.map((input) => (
                <RadioButton {...input} key={input.value} name={name} selected={input.value === selected} onChange={onChange} />
            ))}
        </div>
    );
};

interface IRadioButtonsGroupMultipleProps {
    inputs: IRadioProps[];
    name: string;
    selectedValues: string[] | number[];
    onClickButton: (value: ReactText) => void;
    wrapperClassName?: string;
}

export const RadioButtonsGroupMultiple: FunctionComponent<IRadioButtonsGroupMultipleProps> = ({
    wrapperClassName,
    inputs,
    name,
    selectedValues,
    onClickButton,
}) => {
    const wrapperElClassName = classNames(["flex", wrapperClassName]);

    return (
        <div className={wrapperElClassName}>
            {inputs.map((input) => {
                const isSelected = contains(input.value, selectedValues);

                return <RadioButtonMultiple {...input} key={input.value} name={name} selected={isSelected} onClickButton={onClickButton} />;
            })}
        </div>
    );
};
