import React, { FunctionComponent } from "react";
import { ISelectOption } from "./Select";
import classNames from "classnames";

interface IOptionItemProps extends ISelectOption {
    onChange: (value: string | number) => void;
    selected: boolean;
    textCapitalize?: boolean;
    grouped?: boolean;
    metaFields?: string[];
}

export const OptionItem: FunctionComponent<IOptionItemProps> = ({
    selected,
    label,
    value,
    onChange,
    grouped,
    textCapitalize = true,
    meta,
}) => {
    function onSelfChange() {
        onChange(value);
    }

    const buttonClassName = classNames([
        "block f-select__option-item _text-left",
        { "_text-capitalize": textCapitalize },
        { _selected: selected },
        { _grouped: grouped },
    ]);

    function renderMetaData() {
        if (meta) {
            return " (" + meta + ")";
        }
    }

    const displayText = label || value;

    // using "data-skip-click-outside-check" inside "useClickOutside", because during the check this element will be hidden
    return (
        <button
            onClick={onSelfChange}
            type="button"
            className={buttonClassName}
            title={displayText.toString() + renderMetaData()}
            data-skip-click-outside-check="true"
        >
            {displayText}
            {renderMetaData()}
        </button>
    );
};
