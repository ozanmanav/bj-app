import React, { ChangeEvent, FunctionComponent, SelectHTMLAttributes, useRef, useState, Fragment } from "react";
import "./Select.scss";
import { IInputBaseProps } from "../config";
import classNames from "classnames";
import { OptionItem } from "./OptionItem";
import { useClickOutside } from "../../../../hooks";
import { splitOptionsIntoGroups } from "./utils";
import { OPTIONS_TO_ENABLE_SELECT, REST_GROUP_NAME } from "./config";
import { SearchInput } from "../input";

export interface ISelectOption {
    value: string | number;
    label?: string;
    group?: string;
    meta?: [{ name: string }];
}

interface ISelectProps extends SelectHTMLAttributes<HTMLSelectElement>, IInputBaseProps {
    options: ISelectOption[];
    placeholder?: string;
    button?: boolean;
    textCapitalize?: boolean;
    position?: "top";
    externalSelectClassName?: string;
}

export const Select: FunctionComponent<ISelectProps> = ({
    value,
    onChange,
    options,
    className,
    externalSelectClassName,
    placeholder,
    button,
    error,
    touched,
    marginBottom = "normal",
    position,
    textCapitalize = true,
    ...props
}) => {
    const selectRef = useRef<HTMLSelectElement>(null);
    const selectWrapperRef = useClickOutside<HTMLDivElement>(hideOptions);

    const [shouldShowOptions, setShouldShowOptions] = useState<boolean>(false);
    const [search, setSearch] = useState<string>("");

    const shouldShowSearh = options.length > OPTIONS_TO_ENABLE_SELECT;

    const selectClassName = classNames([
        "f-select _text-left ",
        { "_text-capitalize": textCapitalize },
        { _selected: value && value !== "" },
        { _error: error && touched },
        { _opened: shouldShowOptions },
        externalSelectClassName,
    ]);
    const selectWrapperClassName = classNames([
        "f-select__wrapper",
        { [`_margin-bottom-${marginBottom}`]: marginBottom !== "none" },
        { _button: button },
        { [`_${position}`]: position },
        { "_with-search": shouldShowSearh },
        className,
    ]);

    function hideOptions() {
        setShouldShowOptions(false);
    }

    function toggleOptions() {
        setShouldShowOptions((prevState) => !prevState);
    }

    function onSearchChange(e: ChangeEvent<HTMLInputElement>) {
        setSearch(e.target.value);
    }

    // https://github.com/facebook/react/issues/11488#issuecomment-347775628
    // https://github.com/facebook/react/issues/11488#issuecomment-470623779
    function onSelfChange(value: string | number) {
        const select = selectRef.current;

        if (!select) {
            return;
        }

        const lastValue = select.value;
        select.value = value.toString();

        const event = new Event("change", { bubbles: true });

        // @ts-ignore
        const tracker = select._valueTracker;

        if (tracker) {
            tracker.setValue(lastValue);
        }

        select.dispatchEvent(event);

        setShouldShowOptions(false);
    }

    const selectedOption = options.find(({ value: v }) => v === value);
    const buttonText = selectedOption ? selectedOption.label || selectedOption.value : placeholder;

    const filteredOptions = options.filter(({ label, value }) =>
        label
            ? label.toLocaleLowerCase().includes(search.toLowerCase())
            : value
                  .toString()
                  .toLowerCase()
                  .includes(search.toLowerCase())
    );

    const groupedOptions = splitOptionsIntoGroups(filteredOptions);

    return (
        <div className={selectWrapperClassName} ref={selectWrapperRef}>
            <select className="_none" value={value} onChange={onChange} {...props} ref={selectRef}>
                <option value="" hidden>
                    {placeholder}
                </option>
                {options.map(({ value, label }) => (
                    <option value={value} key={value + (label || "")}>
                        {label || value}
                    </option>
                ))}
            </select>
            <button className={selectClassName} type="button" onClick={toggleOptions} disabled={props.disabled}>
                {buttonText}
            </button>
            {shouldShowOptions && (
                <>
                    {shouldShowSearh && (
                        <SearchInput
                            placeholder="Search for an option"
                            className="f-select__search"
                            marginBottom="none"
                            onChange={onSearchChange}
                            value={search}
                            autoFocus
                        />
                    )}
                    <div className="f-select__options-list">
                        {/* all groups */}
                        {Object.keys(groupedOptions).map(
                            (key) =>
                                key !== REST_GROUP_NAME && (
                                    <Fragment key={key}>
                                        <p className="f-select__group-title _font-bold">{key}</p>
                                        {groupedOptions[key].map((option) => (
                                            <OptionItem
                                                {...option}
                                                selected={option.value === value}
                                                key={option.value + (option.label || "")}
                                                onChange={onSelfChange}
                                                textCapitalize={false}
                                                grouped
                                            />
                                        ))}
                                    </Fragment>
                                )
                        )}
                        {/* rest */}
                        {groupedOptions[REST_GROUP_NAME] &&
                            groupedOptions[REST_GROUP_NAME].map((option) => (
                                <OptionItem
                                    {...option}
                                    selected={option.value === value}
                                    key={option.value + (option.label || "")}
                                    onChange={onSelfChange}
                                    textCapitalize={false}
                                />
                            ))}
                    </div>
                </>
            )}
            {error && touched && <div className="f-select__error">{error}</div>}
        </div>
    );
};
