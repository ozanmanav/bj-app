import { useState } from "react";
import useUpdateEffect from "../../../hooks/useUpdateEffect";

interface useSimplePickerProps {
    defaultValue: Date;
    onChange: (value: Date) => void;
}

export function useSimpleDatepicker({ defaultValue, onChange }: useSimplePickerProps) {
    const [value, setValue] = useState<Date>(defaultValue);

    useUpdateEffect(() => {
        if (onChange) {
            onChange(value);
        }
    }, [value]);

    function handleChangeValue(value: Date) {
        setValue(value);
    }

    return {
        value,
        handleChangeValue,
    };
}
