import React, { forwardRef, FunctionComponent, Ref } from "react";
import "react-day-picker/lib/style.css";
import "./Datepicker.scss";
import { DATE_FORMAT, DATEPICKER_DEFAULT_PLACEHOLDER } from "./config";
import { IDoubleDatepickerState, useDoubleDatepicker } from "./useDoubleDatepicker";
import classnames from "classnames";
import { DayCell, NavBar, WeekDay } from "./helpers";
import DayPickerInput from "react-day-picker/DayPickerInput";
import { formatDate, parseDateForDatePicker } from "./utils";
import { useYearMonthChangeCaption, YearMonthChangeCaption } from "./helpers/YearMonthChangeCaption";
import { DayPickerProps } from "react-day-picker";

const DatepickerInput = forwardRef(({ className, onDayChange, value, readOnly = false, onDayPickerHide, ...props }: any, ref: Ref<any>) => {
    return (
        <div className={className}>
            <DayPickerInput
                format={DATE_FORMAT}
                formatDate={formatDate}
                parseDate={parseDateForDatePicker}
                placeholder={DATEPICKER_DEFAULT_PLACEHOLDER}
                dayPickerProps={{
                    firstDayOfWeek: 1,
                    weekdayElement: <WeekDay />,
                    showOutsideDays: true,
                    renderDay: DayCell,
                    ...props,
                }}
                value={value}
                inputProps={{ readOnly }}
                onDayChange={onDayChange}
                onDayPickerHide={onDayPickerHide}
                ref={ref}
            />
        </div>
    );
});

interface IDatepickerDoubleProps extends DayPickerProps {
    onChange: (state: IDoubleDatepickerState) => void;
}

export const DatepickerDouble: FunctionComponent<IDatepickerDoubleProps> = ({ className, onChange }) => {
    const { to, from, toElRef, fromElRef, handleChangeTo, handleChangeFrom, handleDayPickerHideFrom } = useDoubleDatepicker({
        onChange,
        defaultFrom: new Date(),
        defaultTo: new Date(),
    });
    const { date, changeMonth } = useYearMonthChangeCaption(from || new Date());

    const wrapperClassName = classnames(["flex align-center b-datepicker _double", className]);

    const modifiers = { start: from, end: to };
    return (
        <div className={wrapperClassName}>
            <DatepickerInput
                month={date.month}
                className="_first"
                ref={fromElRef}
                numberOfMonths={2}
                value={from}
                selectedDays={[from, { from, to }]}
                disabledDays={{ after: to }}
                onDayChange={handleChangeFrom}
                onDayPickerHide={handleDayPickerHideFrom}
                modifiers={modifiers}
                navbarElement={<NavBar />}
                captionElement={({ date, localeUtils }: any) => (
                    <YearMonthChangeCaption date={date} localeUtils={localeUtils} onChange={changeMonth} />
                )}
            />
            <span className="_text-grey b-datepicker__separator">to</span>
            <DatepickerInput
                month={date.month}
                className="_second"
                ref={toElRef}
                numberOfMonths={2}
                value={to}
                selectedDays={[from, { from, to }]}
                onDayChange={handleChangeTo}
                disabledDays={{ before: from }}
                modifiers={modifiers}
                navbarElement={<NavBar />}
                captionElement={({ date, localeUtils }: any) => (
                    <YearMonthChangeCaption date={date} localeUtils={localeUtils} onChange={changeMonth} />
                )}
            />
        </div>
    );
};
