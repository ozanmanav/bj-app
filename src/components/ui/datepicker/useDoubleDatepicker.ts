import { useState, useRef, useEffect } from "react";
import { DayPickerInput } from "react-day-picker/types/DayPickerInput";

export interface IDoubleDatepickerState {
    from?: Date;
    to?: Date;
}

interface useDoublePickerProps {
    defaultFrom?: Date;
    defaultTo?: Date;
    onChange: (state: IDoubleDatepickerState) => void;
}

export function useDoubleDatepicker({ defaultFrom, defaultTo, onChange }: useDoublePickerProps) {
    const [state, setState] = useState<IDoubleDatepickerState>({ from: defaultFrom, to: defaultTo });
    const toElRef = useRef<DayPickerInput>(null);
    const fromElRef = useRef<DayPickerInput>(null);

    const { from, to } = state;

    useEffect(() => {
        if (onChange) {
            onChange(state);
        }
    }, [state]);

    function handleChangeFrom(from: Date) {
        setState({
            ...state,
            from,
        });
    }

    function handleChangeTo(to: Date) {
        setState({
            ...state,
            to,
        });
    }

    function handleDayPickerHideFrom() {
        if (toElRef.current && fromElRef.current) {
            toElRef.current.getInput().focus();
        }
    }

    return {
        from,
        to,
        handleChangeFrom,
        handleChangeTo,
        handleDayPickerHideFrom,
        toElRef,
        fromElRef,
    };
}
