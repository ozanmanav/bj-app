import dateFnsFormat from "date-fns/format";
import dateFnsParse from "date-fns/parse";
import { DATE_FORMAT, DATE_FORMAT_STRING } from "./config";
import { DateUtils } from "react-day-picker";

export function parseDate(dateString: string, formatString: string = DATE_FORMAT_STRING) {
    const backupDate = new Date(); // When parsing string 'January 1st' without a year, the values will be taken from 3rd argument backupDate which works as a context of parsing.
    return dateFnsParse(dateString, formatString, backupDate);
}

export function parseDateForDatePicker(dateString: string, formatString: string = DATE_FORMAT_STRING) {
    const parsedDate = parseDate(dateString, formatString);

    //Check day of month must be at least 2 characters 01, 05, 12, 31
    let dayOfMonth = dateString && typeof dateString === "string" && dateString.split(" ")[2];
    if (dayOfMonth && dayOfMonth.length < 2) {
        return undefined;
    }

    return DateUtils.isDate(parsedDate) ? parsedDate : undefined;
}

export function formatDate(date: Date, formatString: string = DATE_FORMAT) {
    return DateUtils.isDate(date) ? dateFnsFormat(date, formatString) : "Invalid Date";
}
