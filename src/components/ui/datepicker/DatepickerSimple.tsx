import React, { FunctionComponent } from "react";
import "react-day-picker/lib/style.css";
import "./Datepicker.scss";
import DayPickerInput from "react-day-picker/DayPickerInput";
import { DATE_FORMAT, DATEPICKER_DEFAULT_PLACEHOLDER, IDatepickerProps } from "./config";
import { formatDate, parseDateForDatePicker } from "./utils";
import classnames from "classnames";
import { Icon } from "../icons";
import { DayCell, NavBar, WeekDay } from "./helpers";
import { useYearMonthChangeCaption, YearMonthChangeCaption } from "./helpers/YearMonthChangeCaption";
import { useSimpleDatepicker } from "./useSimpleDatepicker";

interface IDatepickerSimpleProps extends IDatepickerProps {
    marginBottom?: "normal";
    icon?: "calendarGrey";
    dateFormat?: "simple";
    fullWidth?: boolean;
    readOnly?: boolean;
}

export const DatepickerSimple: FunctionComponent<IDatepickerSimpleProps> = ({
    onChange,
    onBlur,
    error,
    touched,
    className,
    placeholder,
    marginBottom,
    defaultValue = new Date(),
    icon,
    dateFormat,
    fullWidth,
    readOnly = false,
    ...props
}) => {
    const { value, handleChangeValue } = useSimpleDatepicker({ onChange, defaultValue });
    const { date, changeMonth } = useYearMonthChangeCaption(value || new Date());

    const wrapperClassName = classnames([
        "b-datepicker _single _left",
        { [`_margin-botton-${marginBottom}`]: marginBottom },
        { "_full-width": fullWidth },
        { _error: error && touched },
        { "_with-icon": icon },
        className,
    ]);

    return (
        <div className={wrapperClassName}>
            <DayPickerInput
                format={DATE_FORMAT}
                formatDate={formatDate}
                parseDate={parseDateForDatePicker}
                placeholder={placeholder || DATEPICKER_DEFAULT_PLACEHOLDER}
                value={value}
                dayPickerProps={{
                    firstDayOfWeek: 1,
                    month: date.month,
                    weekdayElement: <WeekDay />,
                    showOutsideDays: true,
                    renderDay: DayCell,
                    navbarElement: <NavBar />,
                    captionElement: ({ date, localeUtils }) => (
                        <YearMonthChangeCaption date={date} localeUtils={localeUtils} onChange={changeMonth} />
                    ),
                    ...props,
                }}
                inputProps={{ readOnly, onBlur }}
                onDayChange={handleChangeValue}
            />
            {icon && <Icon icon={icon} className="b-datepicker__icon" />}
            {error && touched && <div className="b-datepicker__error">{error}</div>}
        </div>
    );
};
