import React, { FunctionComponent } from "react";

export const WeekDay: FunctionComponent<any> = ({ weekday, className, localeUtils, locale }: any) => {
    const weekdayName = localeUtils.formatWeekdayLong(weekday, locale);

    return <div className={className}>{weekdayName.slice(0, 3)}</div>;
};
