import React, { FunctionComponent } from "react";
import arrow from "../../../../icons/datepickerArrow.svg";

function generateRecursion(cb: (callback?: () => void) => void, times: number): () => void {
    return times - 1 >= 0 ? () => cb(generateRecursion(cb, times - 1)) : cb;
}

export const NavBar: FunctionComponent<any> = ({ className, onNextClick, onPreviousClick, date, onChange, ...props }) => {
    function prevMonth() {
        onPreviousClick();
    }

    function nextMonth() {
        onNextClick();
    }

    function prevYear() {
        // TODO: is it bs?
        onPreviousClick(generateRecursion(onPreviousClick, 10));
    }

    function nextYear() {
        onNextClick(generateRecursion(onNextClick, 10));
    }

    return (
        <div className={className}>
            <button type="button" className="nav-bar-button _prev _year" onClick={prevYear}>
                <img src={arrow} alt="arrow" />
                <img src={arrow} alt="arrow" />
            </button>
            <button type="button" className="nav-bar-button _prev _month" onClick={prevMonth}>
                <img src={arrow} alt="arrow" />
            </button>
            <button type="button" className="nav-bar-button _next _month" onClick={nextMonth}>
                <img src={arrow} alt="arrow" />
            </button>
            <button type="button" className="nav-bar-button _next _year" onClick={nextYear}>
                <img src={arrow} alt="arrow" />
                <img src={arrow} alt="arrow" />
            </button>
        </div>
    );
};
