import React from "react";

export const DayCell = (day: Date) => {
    const date = day.getDate();

    return <span>{date}</span>;
};
