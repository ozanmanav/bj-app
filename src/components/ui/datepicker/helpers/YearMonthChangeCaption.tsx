import React, { FunctionComponent, useRef, useState } from "react";

const YEARS_TO_INCLUDE = 109;

const currentYear = new Date().getFullYear();
const fromMonth = new Date(currentYear - YEARS_TO_INCLUDE, 0);
const toMonth = new Date(currentYear + YEARS_TO_INCLUDE, 11);

function generateYears() {
    const years = [];

    for (let i = fromMonth.getFullYear(); i <= toMonth.getFullYear(); i += 1) {
        years.push(i);
    }

    return years;
}

export const YearMonthChangeCaption: FunctionComponent<any> = ({ date, localeUtils, onChange }) => {
    const yearSelectEl = useRef<HTMLSelectElement>(null);
    const monthSelectEl = useRef<HTMLSelectElement>(null);

    const months = localeUtils.getMonths();
    const years = generateYears();

    function handleChange() {
        if (!yearSelectEl.current || !monthSelectEl.current) {
            return;
        }

        const year = +yearSelectEl.current.value;
        const month = +monthSelectEl.current.value;

        onChange(new Date(year, month));
    }

    return (
        <div className="DayPicker-Caption flex align-center justify-center">
            <select name="month" onChange={handleChange} value={date.getMonth()} ref={monthSelectEl} className="b-datepicker__select">
                {months.map((month: string, i: number) => (
                    <option key={month} value={i}>
                        {month}
                    </option>
                ))}
            </select>
            <select name="year" onChange={handleChange} value={date.getFullYear()} ref={yearSelectEl} className="b-datepicker__select">
                {years.map((year) => (
                    <option key={year} value={year}>
                        {year}
                    </option>
                ))}
            </select>
        </div>
    );
};

export function useYearMonthChangeCaption(defaultMonth: Date) {
    const [date, setDate] = useState<any>({
        month: defaultMonth,
    });

    function changeMonth(month: Date) {
        setDate({ month });
    }

    return {
        date,
        changeMonth,
    };
}
