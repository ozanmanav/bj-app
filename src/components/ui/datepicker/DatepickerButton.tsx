import React, { FunctionComponent, useState } from "react";
import "react-day-picker/lib/style.css";
import "./Datepicker.scss";
import { IDatepickerProps } from "./config";
import { useClickOutside } from "../../../hooks";
import { CalendarButton } from "../buttons";
import DayPicker from "react-day-picker";
import classnames from "classnames";
import { DayCell, NavBar, WeekDay } from "./helpers";
import { useYearMonthChangeCaption, YearMonthChangeCaption } from "./helpers/YearMonthChangeCaption";

export const DatepickerButton: FunctionComponent<IDatepickerProps> = ({ onChange, defaultValue, className, ...props }) => {
    const { date, changeMonth } = useYearMonthChangeCaption(defaultValue ? new Date(defaultValue) : new Date());

    const [selectedDay, setSelectedDay] = useState<any>(defaultValue);
    const [isOpened, setIsOpened] = useState<boolean>(false);
    const dayPickerRef = useClickOutside<HTMLDivElement>(hide);

    const wrapperClassName = classnames(["b-datepicker-button", { _opened: isOpened }]);

    const dayPickerClassName = classnames(["b-datepicker-button__daypicker _single", className]);

    function handleDayChange(day: Date, { selected }: any) {
        if (selected) {
            setSelectedDay(null);
        } else {
            setSelectedDay(day);
            onChange(day);
            hide();
        }
    }

    function toggleIsOpened() {
        isOpened ? hide() : open();
    }

    function hide() {
        // avoiding unnecessary calls in useClickOutside hook
        if (isOpened) {
            setIsOpened(false);
        }
    }

    function open() {
        setIsOpened(true);
    }

    return (
        <div className={wrapperClassName} ref={dayPickerRef}>
            <CalendarButton className="b-datepicker-button__button" onClick={toggleIsOpened} />
            {isOpened && (
                <DayPicker
                    firstDayOfWeek={1}
                    month={date.month}
                    onDayClick={handleDayChange}
                    selectedDays={selectedDay}
                    className={dayPickerClassName}
                    renderDay={DayCell}
                    showOutsideDays
                    navbarElement={<NavBar />}
                    weekdayElement={<WeekDay />}
                    captionElement={({ date, localeUtils }) => (
                        <YearMonthChangeCaption date={date} localeUtils={localeUtils} onChange={changeMonth} />
                    )}
                />
            )}
        </div>
    );
};
