import { DayPickerProps } from "react-day-picker";
import { FormikErrors, FormikTouched } from "formik";

export const DATE_FORMAT = "yyyy MMM dd";
export const DATE_FORMAT_STRING = "yyyy/MM/dd";

export const DATEPICKER_DEFAULT_PLACEHOLDER = DATE_FORMAT;

export interface IDatepickerProps extends DayPickerProps {
    onChange: (date: Date) => void;
    defaultValue?: Date;
    value?: Date;
    error?: string | FormikErrors<any>;
    touched?: boolean | FormikTouched<any>;
    placeholder?: string;
}
