import React, { ButtonHTMLAttributes, FunctionComponent } from "react";
import "./Buttons.scss";
import { Icon, TIconType } from "../";
import { Link, LinkProps } from "react-router-dom";
import { getButtonClassName } from "./utils";
import { appendClassName } from "../../../utils";
import classNames from "classnames";
import { TSortingOrder } from "../../../hooks";

interface IButtonBaseProps {
    text: string;
    primary?: boolean;
    big?: boolean;
    icon?: TIconType;
    remove?: boolean;
}

interface IButtonAttributes extends ButtonHTMLAttributes<HTMLButtonElement> {}

export interface IButtonProps extends IButtonAttributes, IButtonBaseProps {}

interface IButtonLinkProps extends LinkProps, IButtonBaseProps {}

export const ButtonLink: FunctionComponent<IButtonLinkProps> = ({ text, className, icon, primary, big, ...props }) => {
    const buttonClassName = getButtonClassName({ primary, big, className });

    return (
        <Link {...props} className={buttonClassName}>
            {icon && <Icon icon={icon} className="button__icon" />}
            {text}
        </Link>
    );
};

export const Button: FunctionComponent<IButtonProps> = ({
    text,
    primary,
    className,
    big,
    icon,
    remove,
    type = "button",
    disabled,
    ...props
}) => {
    const buttonClassName = getButtonClassName({ primary, big, className, remove, disabled });

    return (
        <button {...props} disabled={disabled} type={type} className={buttonClassName}>
            {icon && <Icon icon={icon} className="button__icon" />}
            {text}
        </button>
    );
};

export const SortButton: FunctionComponent<IButtonAttributes & { text: string; sorted?: TSortingOrder | "none" }> = ({
    text,
    sorted,
    ...props
}) => {
    const isSorted = sorted && sorted !== "none";

    const iconClassName = classNames(["button__sort-icon", { [`_${sorted}`]: isSorted }]);

    return (
        <button {...props} type="button">
            {isSorted && <Icon icon="sortIcon" className={iconClassName} />}
            <span className="_font-bold _text-grey _text-uppercase">{text}</span>
        </button>
    );
};

export const PlusButton: FunctionComponent<IButtonAttributes & { to?: string }> = ({ to, ...props }) => {
    if (to) {
        return (
            <Link to={to} {...(props as LinkProps)} className="plus-button">
                <Icon icon="plus" />
            </Link>
        );
    }

    return (
        <button {...props} type="button" className="plus-button">
            <Icon icon="plus" />
        </button>
    );
};

export const BackButton: FunctionComponent<IButtonAttributes & { text?: string }> = ({ className, text = "Back", ...props }) => {
    const buttonClassName = appendClassName("back-button", className);

    return (
        <button className={buttonClassName} {...props}>
            <Icon icon="backArrow" className="back-button__icon" />
            <span className="back-button__text">{text}</span>
        </button>
    );
};

export const RemoveObjectButton: FunctionComponent<IButtonProps> = ({ className, ...props }) => {
    const buttonClassName = appendClassName("_remove-object", className);

    return <Button {...props} className={buttonClassName} />;
};

export const AddButton: FunctionComponent<IButtonAttributes & { text?: string }> = ({ className, text = "Add another", ...props }) => {
    const buttonClassName = appendClassName("_add", className);

    return <Button text={text} icon="plusGrey" className={buttonClassName} {...props} />;
};

export const AddAnotherOrganizationButton: FunctionComponent<IButtonAttributes> = ({ className, ...props }) => {
    const buttonClassName = appendClassName("_add-another-organization", className);

    return <Button text="Add another organization" icon="plusGrey" className={buttonClassName} {...props} />;
};

export const RemoveButton: FunctionComponent<IButtonAttributes> = ({ className, ...props }) => {
    const buttonClassName = appendClassName("_remove", className);

    return <Button text="Remove" icon="plusGrey" className={buttonClassName} {...props} />;
};

export const SeeHistoryButton: FunctionComponent<IButtonAttributes & LinkProps> = ({ className, to, ...props }) => {
    const buttonClassName = appendClassName("_see-history", className);

    return <ButtonLink to={to} text="See history" icon="history" className={buttonClassName} {...props} />;
};

export const UploadFilesButton: FunctionComponent<IButtonAttributes> = ({ className, ...props }) => {
    const buttonClassName = appendClassName("_upload", className);

    return <Button text="Upload files" icon="upload" primary className={buttonClassName} {...props} />;
};

export const EditButton: FunctionComponent<IButtonAttributes> = ({ className, ...props }) => {
    const buttonClassName = appendClassName("simple-button", className);

    return (
        <button className={buttonClassName} type="button" {...props}>
            <Icon icon="edit" color="black" />
        </button>
    );
};

export const CheckButton: FunctionComponent<IButtonAttributes> = ({ className, ...props }) => {
    const buttonClassName = appendClassName("simple-button", className);

    return (
        <button className={buttonClassName} type="button" {...props}>
            <Icon icon="checkBlue" />
        </button>
    );
};

export const EditLinkButton: FunctionComponent<LinkProps> = ({ className, ...props }) => {
    const buttonClassName = appendClassName("simple-button", className);

    return (
        <Link className={buttonClassName} {...props}>
            <Icon icon="edit" />
        </Link>
    );
};

export const CalendarButton: FunctionComponent<IButtonAttributes> = ({ className, ...props }) => {
    const buttonClassName = appendClassName("simple-button", className);

    return (
        <button className={buttonClassName} type="button" {...props}>
            <Icon icon="calendar" />
        </button>
    );
};

export const SaveIconButton: FunctionComponent<IButtonAttributes> = ({ className, ...props }) => {
    const buttonClassName = appendClassName("simple-button", className);

    return (
        <button className={buttonClassName} type="button" {...props}>
            <Icon icon="tick" />
        </button>
    );
};

export const BranchesButton: FunctionComponent<IButtonAttributes> = ({ className, ...props }) => {
    const buttonClassName = appendClassName("simple-button", className);

    return (
        <button className={buttonClassName} type="button" {...props}>
            <Icon icon="branches" />
        </button>
    );
};

export const ClocksButton: FunctionComponent<IButtonAttributes> = ({ className, ...props }) => {
    const buttonClassName = appendClassName("simple-button", className);

    return (
        <button className={buttonClassName} {...props} type="button">
            <Icon icon="clocks" />
        </button>
    );
};

export const DownloadButton: FunctionComponent<IButtonAttributes> = ({ className, ...props }) => {
    const buttonClassName = appendClassName("simple-button", className);

    return (
        <button className={buttonClassName} type="button" {...props}>
            <Icon icon="download" />
        </button>
    );
};

export const RemoveIconButton: FunctionComponent<IButtonAttributes & { to?: string }> = ({ className, to, ...props }) => {
    const wrapperClassName = appendClassName("simple-button _remove", className);

    if (to) {
        return (
            <Link to={to} {...(props as LinkProps)} className={wrapperClassName}>
                <Icon icon="remove" />
            </Link>
        );
    }

    return (
        <button type="button" {...props} className={wrapperClassName}>
            <Icon icon="remove" />
        </button>
    );
};

export const CalendarAddButton: FunctionComponent<IButtonAttributes> = ({ className, ...props }) => {
    const buttonClassName = appendClassName("simple-button", className);

    return (
        <button className={buttonClassName} type="button" {...props}>
            <Icon icon="newFile" />
        </button>
    );
};

export const ArrowDownButton: FunctionComponent<IButtonAttributes> = ({ className, ...props }) => {
    const buttonClassName = appendClassName("simple-button", className);

    return (
        <button className={buttonClassName} type="button" {...props}>
            <Icon icon="arrowDown" />
        </button>
    );
};

export const NextButton: FunctionComponent<IButtonAttributes & { text?: string }> = (props) => {
    return <Button {...props} primary icon="nextArrow" text={props.text || "Next"} />;
};

export const SaveButton: FunctionComponent<IButtonAttributes> = (props) => {
    return <Button text="Save" primary icon="check" {...props} />;
};

export const AssignButton: FunctionComponent<IButtonAttributes> = ({ className, ...props }) => {
    const buttonClassName = appendClassName("_assign", className);

    return <Button text="Assign" primary icon="buttonPlus" className={buttonClassName} {...props} />;
};

export const SaveAndContinueButton: FunctionComponent<IButtonAttributes> = ({ className, ...props }) => {
    const buttonClassName = appendClassName("_save-and-continue", className);

    return <Button text="Save and continue later" icon="checkGrey" className={buttonClassName} {...props} />;
};

export const ExportButton: FunctionComponent<IButtonProps> = ({ className, text = "Export", primary = true, ...props }) => {
    const buttonClassName = appendClassName("_export", className);

    return <Button text={text} icon={primary ? "exportWhite" : "exportGrey"} primary={primary} className={buttonClassName} {...props} />;
};
