import React, { FunctionComponent } from "react";
import { default as ReactLoader } from "react-loader-spinner";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import "./Loader.scss";

interface LoaderProps {
    withText?: boolean;
    text?: string;
    height?: number;
    width?: number;
    rawLoader?: boolean;
    isWhite?: boolean;
}

export const Loader: FunctionComponent<LoaderProps> = ({
    withText,
    text,
    width = 40,
    height = 40,
    rawLoader,
    isWhite = false,
}) => {
    let color = isWhite ? "#FFFFFF" : "#5080FA";

    return rawLoader ? (
        <ReactLoader type="Oval" color={color} height={height} width={width} />
    ) : (
        <div className="b-loader">
            <div className="b-loader-inner">
                <ReactLoader type="Oval" color={color} height={height} width={width} />

                {withText && (
                    <div className="b-loader-inner__text">
                        Loading
                        {text && ` ${text}`}
                        ...
                    </div>
                )}
            </div>
        </div>
    );
};
