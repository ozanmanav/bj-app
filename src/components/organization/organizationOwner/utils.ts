import { IOrganization } from "../../../config/types";
import get from "lodash.get";

export function getOrganizationAddress({ address }: IOrganization) {
    const country = get(address, "country") || "";
    const street = get(address, "street") || "";
    const number = get(address, "number") || "";
    const city = get(address, "city") || "";

    return `${street} ${number}${city ? ", " + city : ""}${country ? ", " + country : ""}`;
}
