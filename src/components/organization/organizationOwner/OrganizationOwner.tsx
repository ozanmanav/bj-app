import React, { FunctionComponent, useContext } from "react";
import "./OrganizationOwner.scss";
import { Image, CalendarAddButton, RemoveIconButton } from "../../ui";
import { IOrganization } from "../../../config/types";
import { getYear } from "date-fns";
import { UserRoleCheck } from "../../userRoleCheck";
import { getImageURL } from "../../../utils";
import { AccountStateContext } from "../../../contexts/accountStateContext";
import { Link } from "react-router-dom";
import { parseDate } from "../../ui/datepicker/utils";
import { getOrganizationAddress } from "./utils";
import { DATE_FORMAT_WITH_TIME } from "../../../config";
import { EventCheckboxSwitch } from "../../events";

interface IOrganizationOwnerProps extends IOrganization {
    objectId?: string;
    objectType: string;
    contractId: string;
    contractEvents: { id: string; timeline: boolean }[];
    eventRefetch: Function;
}

export const OrganizationOwner: FunctionComponent<IOrganizationOwnerProps> = ({
    objectId,
    contractEvents,
    objectType,
    contractId,
    eventRefetch,
    ...organization
}) => {
    const { user } = useContext(AccountStateContext);

    const { id, phone, name, email, created_at, logo, category } = organization;
    const isClientOrganization: boolean = category === "CLIENT";

    const address = getOrganizationAddress(organization);

    return (
        <div className="b-organization-owner__wrapper">
            <header className="b-organization-owner__header _text-grey h6 _text-uppercase _font-bold">{objectType}</header>
            <div className="b-organization-owner__content flex">
                {logo ? (
                    <Image src={getImageURL(logo.url)} alt="owner logo" className="b-organization-owner__image" />
                ) : (
                    <div className="b-organization-owner__image _replace flex align-center justify-center _font-bold _text-primary">
                        {name[0].toUpperCase()}
                    </div>
                )}
                <div className="b-organization-owner__info">
                    {user.is_admin ? (
                        <Link
                            to={isClientOrganization ? `/app/admin/organizations/client/${id}` : `/app/admin/organizations/${id}`}
                            className="_font-bold block b-organization-owner__name"
                        >
                            {name}
                        </Link>
                    ) : (
                        <p className="_font-bold b-organization-owner__name">{name}</p>
                    )}
                    {address && (
                        <p className="b-organization-owner__contact">
                            <span className="b-organization-owner__contact-field">Address:</span>
                            <span className="b-organization-owner__contact-value">{address}</span>
                        </p>
                    )}
                    {email && (
                        <p className="b-organization-owner__contact">
                            <span className="b-organization-owner__contact-field">E-Mail:</span>
                            <span className="b-organization-owner__contact-value">{email}</span>
                        </p>
                    )}
                    {phone && (
                        <p className="b-organization-owner__contact">
                            <span className="b-organization-owner__contact-field">Phone:</span>
                            <span className="b-organization-owner__contact-value">{phone}</span>
                        </p>
                    )}
                    <p className="b-organization-owner__contact">
                        <span className="b-organization-owner__contact-field">Since:</span>
                        <span className="b-organization-owner__contact-value">
                            {created_at && getYear(parseDate(created_at, DATE_FORMAT_WITH_TIME))}
                        </span>
                    </p>
                </div>
                <div className="b-organization-owner__actions flex align-center">
                    <EventCheckboxSwitch events={contractEvents} refetch={eventRefetch} />
                    <UserRoleCheck
                        availableForRoles={[
                            "owner_administrator",
                            "manufacturer_administrator",
                            "manufacturer_brand_manager",
                            "manufacturer_object_manager",
                            "property_manager_administrator",
                            "property_manager_building_manager",
                            "service_provider_administrator",
                            "service_provider_service_manager",
                        ]}
                    >
                        <Link to={{ pathname: `/app/event/add`, search: `?obj=${objectId}&contractID=${contractId}` }}>
                            <CalendarAddButton />
                        </Link>
                        {objectId && (
                            <UserRoleCheck
                                availableForRoles={[
                                    "owner_administrator",
                                    "manufacturer_administrator",
                                    "manufacturer_brand_manager",
                                    "manufacturer_object_manager",
                                    "property_manager_administrator",
                                    "property_manager_building_manager",
                                    "service_provider_administrator",
                                ]}
                            >
                                <RemoveIconButton to={`/app/object/${objectId}/organization/${id}/successor/${contractId}`} />
                            </UserRoleCheck>
                        )}
                    </UserRoleCheck>
                </div>
            </div>
        </div>
    );
};
