import React, { FunctionComponent } from "react";
import "./OrganizationContract.scss";
import { ContractsList } from "../../ui";
import { IContract, IObjectMinimal } from "../../../config/types";

interface IOrganizationContractsProps {
    contracts: IContract[];
    objectId: string;
    refetch: () => void;
    objects: IObjectMinimal[];
}

export const OrganizationContract: FunctionComponent<IOrganizationContractsProps> = ({ contracts, objectId, objects, refetch }) => {
    return (
        <div className="b-organization-contract__wrapper">
            <header className="b-organization-contract__header _font-bold _text-grey _text-uppercase h6">Contract</header>
            <ContractsList variant="cockpit" contracts={contracts} objectId={objectId} refetch={refetch} objects={objects} />
        </div>
    );
};
