import React, { FunctionComponent } from "react";
import "./CopyRelationships.scss";
import { Button } from "../../ui/buttons";
import { SelectRelationshipsToCopyModal } from "../selectRelationshipsToCopyModal";
import { useModal } from "../../ui/modal";
import { IContract } from "../../../config/types";
import { useSendContract } from "../../../hooks/apollo";
import { showSuccessToast } from "../../ui/toasts";
import { getInitialValues } from "../../modals/contractModal/utils";

interface ICopyRelationshipsProps {
    objectID: string;
    refetch: () => void;
}

export const CopyRelationships: FunctionComponent<ICopyRelationshipsProps> = ({ objectID, refetch }) => {
    const sendContract = useSendContract(onCopySuccess);

    const { isOpen: isSelectModalOpen, open: openSelectModal, hide: hideSelectModal } = useModal();

    async function onContractsSelect(contracts: IContract[]) {
        if (contracts.length > 0) {
            await Promise.all(
                contracts.map((contract) =>
                    sendContract({
                        contractState: getInitialValues(contract, objectID),
                        objectID,
                    })
                )
            );
        }
    }

    function onCopySuccess() {
        showSuccessToast("Contracts are successfully copied.");
        refetch();
    }

    return (
        <div className="b-copy-relationships">
            <Button text="Copy from parent" onClick={openSelectModal} />
            <SelectRelationshipsToCopyModal
                objectID={objectID}
                isOpen={isSelectModalOpen}
                hide={hideSelectModal}
                callback={onContractsSelect}
            />
        </div>
    );
};
