import React, { ChangeEvent, FunctionComponent, useEffect, useMemo, useState } from "react";
import "./SelectRelationshipsToCopy.scss";
import { IModalProps, Modal, ModalFooter } from "../../ui/modal";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { Loading } from "../../ui/loading";
import { Button, SortButton } from "../../ui/buttons";
import { TableCollapseButton } from "../../tables/tableCollapseButton";
import get from "lodash.get";
import { IContract, IObjectMinimal } from "../../../config/types";
import { getObjectLink } from "../../../utils";
import { Link } from "react-router-dom";
import { ISelectContract, SelectContractRow } from "./SelectContractRow";
import { useSortingTable } from "../../../hooks";
import orderBy from "lodash.orderby";
import { getStartDate } from "./utils";

const ITEMS_TO_SHOW = 6;

const SORT_STRINGS = {
    RELATION: "role.name",
    ORGANIZATION_NAME: "organization.name",
    START_DATE: "startDate",
};

// TODO: Details deleted from this query should implement another place
const GET_PARENT_CONTRACTS = gql`
    query GetParentContracts($id: String!) {
        objects(id: $id) {
            data {
                higherRelations {
                    higher {
                        id
                        name
                        contracts {
                            id
                            role_id
                            created_at
                            role {
                                name
                            }
                            organization {
                                id
                                name
                                logo {
                                    url
                                }
                            }
                            employees {
                                id
                                user {
                                    id
                                    first_name
                                    last_name
                                    email
                                    phone
                                }
                                job {
                                    name
                                }
                            }
                        }
                    }
                }
            }
        }
    }
`;

interface ISelectRelationshipsToCopyModalProps extends IModalProps {
    objectID: string;
    callback: (contractIDs: IContract[]) => void;
}

export const SelectRelationshipsToCopyModal: FunctionComponent<ISelectRelationshipsToCopyModalProps> = ({
    objectID,
    callback,
    ...props
}) => {
    const [selectedContracts, setSelectedContracts] = useState<ISelectContract[]>([]);

    const { sortBy, sortingOrder, changeSortBy, getSortOrderForField } = useSortingTable({
        localStorageKey: "select_relationships_to_copy",
    });

    const { data, loading } = useQuery(GET_PARENT_CONTRACTS, {
        variables: {
            id: objectID,
        },
        fetchPolicy: "no-cache",
    });

    const parents = get(data, "objects.data[0].higherRelations") || [];

    const contracts = useMemo((): ISelectContract[] => {
        if (!parents) {
            return [];
        }

        return parents.reduce((acc: ISelectContract[], { higher }: { higher: any }) => {
            if (higher && higher.contracts) {
                acc.push(
                    ...higher.contracts.reduce((acc: ISelectContract[], contract: any) => {
                        if (contract.organization) {
                            acc.push({ ...contract, startDate: contract.details && getStartDate(contract.details) });
                        }

                        return acc;
                    }, [])
                );
            }

            return acc;
        }, []);
    }, [data]);

    const sortedContracts = orderBy(contracts, [sortBy], [sortingOrder]);

    useEffect(() => {
        if (contracts) {
            setSelectedContracts(contracts);
        }
    }, [contracts]);

    const [isCollapsed, setIsCollapsed] = useState<boolean>(true);
    const shouldAllowCollapse = contracts.length > ITEMS_TO_SHOW;
    const dataToShow = isCollapsed && shouldAllowCollapse ? sortedContracts.slice(0, ITEMS_TO_SHOW) : sortedContracts;
    const collapsedItems = contracts.length - ITEMS_TO_SHOW;

    function toggle() {
        setIsCollapsed((prevState) => !prevState);
    }

    function handleCheckboxChange(e: ChangeEvent<HTMLInputElement>) {
        const selectedID = e.target.value;

        if (e.target.checked) {
            const selectedContract = contracts.find(({ id }: ISelectContract) => id === selectedID);

            if (selectedContract) {
                setSelectedContracts((prevState) => [...prevState, selectedContract]);
            }
        } else {
            setSelectedContracts((prevState) => prevState.filter(({ id }) => id !== selectedID));
        }
    }

    function onSubmit() {
        callback(selectedContracts);
        setSelectedContracts([]);
        props.hide();
    }

    function sortByRelationship() {
        changeSortBy(SORT_STRINGS.RELATION);
    }

    function sortByOrganization() {
        changeSortBy(SORT_STRINGS.ORGANIZATION_NAME);
    }

    function sortByStartDate() {
        changeSortBy(SORT_STRINGS.START_DATE);
    }

    return (
        <Modal {...props} className="b-select-relationships-to-copy">
            {loading ? (
                <Loading />
            ) : (
                <>
                    <h2 className="h1 b-select-relationships-to-copy__title">
                        Copy relationships from{" "}
                        {parents.map(({ type, higher }: IObjectMinimal, index: number) => {
                            if (higher) {
                                return (
                                    <Link
                                        to={getObjectLink(type, get(higher, "id"))}
                                        key={get(higher, "id")}
                                        className="_font-bold _text-primary"
                                    >
                                        {get(higher, "name")}
                                        {index === parents.length - 1 ? "" : ", "}
                                    </Link>
                                );
                            } else {
                                return null;
                            }
                        })}
                    </h2>
                    {dataToShow.length > 0 ? (
                        <div className="b-table__wrapper b-select-relationships-to-copy__table">
                            <table className="b-table">
                                <thead className="b-table__head">
                                    <tr className="b-table__row _head">
                                        <th className="b-table__cell">
                                            <SortButton
                                                text="Relationship"
                                                onClick={sortByRelationship}
                                                sorted={getSortOrderForField(SORT_STRINGS.RELATION)}
                                            />
                                        </th>
                                        <th className="b-table__cell">
                                            <SortButton
                                                text="Organization"
                                                onClick={sortByOrganization}
                                                sorted={getSortOrderForField(SORT_STRINGS.ORGANIZATION_NAME)}
                                            />
                                        </th>
                                        <th className="b-table__cell">
                                            <SortButton
                                                text="Start date"
                                                onClick={sortByStartDate}
                                                sorted={getSortOrderForField(SORT_STRINGS.START_DATE)}
                                            />
                                        </th>
                                        <th className="b-table__cell">Copy relationship</th>
                                    </tr>
                                </thead>
                                <tbody className="b-table__body">
                                    {dataToShow.map(
                                        (contract: ISelectContract) =>
                                            contract.organization && (
                                                <SelectContractRow
                                                    key={contract.id}
                                                    handleChange={handleCheckboxChange}
                                                    checked={!!selectedContracts.find(({ id }) => id === contract.id)}
                                                    {...contract}
                                                />
                                            )
                                    )}
                                </tbody>
                            </table>
                            {shouldAllowCollapse && (
                                <TableCollapseButton isCollapsed={isCollapsed} collapsedItems={collapsedItems} onClick={toggle} />
                            )}
                        </div>
                    ) : (
                        <p>Parent has no relationships.</p>
                    )}
                </>
            )}
            <ModalFooter>
                <Button text="Select" primary onClick={onSubmit} />
            </ModalFooter>
        </Modal>
    );
};
