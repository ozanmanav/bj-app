import { IMetafield } from "../../../config/types";
import { formatDate, parseDate } from "../../ui/datepicker/utils";
import { DATE_FORMAT } from "../../ui/datepicker/config";

const START_DATE_FIELD_NAME = "Start date";
const NO_START_DATE_MESSAGE = "Start date is not specified";

export function getStartDate(customFields: IMetafield[]) {
    const startDateField = customFields.find(({ name }) => name.toLowerCase() === START_DATE_FIELD_NAME.toLowerCase());

    return startDateField ? formatDate(parseDate(startDateField.value), DATE_FORMAT) : NO_START_DATE_MESSAGE;
}
