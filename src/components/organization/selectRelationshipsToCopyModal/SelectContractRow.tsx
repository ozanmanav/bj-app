import React, { ChangeEvent, FunctionComponent } from "react";
import { IContract } from "../../../config/types";
import get from "lodash.get";
import { getImageURL } from "../../../utils";
import { Image } from "../../ui/images";
import { CheckboxSwitch } from "../../ui/inputs/checkbox";
import { formatDate } from "../../ui/datepicker/utils";

export interface ISelectContract extends IContract {
    created_at: string;
}

interface ISelectContractRowProps extends ISelectContract {
    checked: boolean;
    handleChange: (e: ChangeEvent<HTMLInputElement>) => void;
}

export const SelectContractRow: FunctionComponent<ISelectContractRowProps> = ({
    id,
    organization,
    role,
    created_at,
    checked,
    handleChange,
}) => {
    const logoSrc = get(organization, "logo.url");

    const logoUrl = getImageURL(logoSrc);

    return (
        <tr className="b-table__row" key={id}>
            <td className="b-table__cell">{role.name}</td>
            <td className="b-table__cell">
                <div className="flex align-center _font-bold">
                    <Image
                        src={logoUrl}
                        alt={organization && logoUrl ? organization.name : ""}
                        className="b-select-relationships-to-copy__logo"
                    />
                    {organization && organization.name}
                </div>
            </td>
            <td className="b-table__cell">{formatDate(new Date(created_at))}</td>
            <td className="b-table__cell">
                <CheckboxSwitch value={id} checked={checked} onChange={handleChange} />
            </td>
        </tr>
    );
};
