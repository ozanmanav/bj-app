import React, { FunctionComponent } from "react";
import "./ObjectOrganizations.scss";
import { RelationalList } from "../../relationalLists";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { Loading } from "../../ui/loading";
import get from "lodash.get";
import { IOrganization } from "../../../config/types";

const GET_ORGANIZATIONS = gql`
    query Objects($id: String) {
        objects(id: $id) {
            data {
                id
                type
                organizations {
                    id
                    name
                    address
                    email
                    phone
                    created_at
                    type {
                        name
                    }
                    employees {
                        id
                        first_name
                        last_name
                        email
                        phone
                        job_title
                    }
                    contracts {
                        id
                        count_objects
                        employees {
                            id
                            first_name
                            last_name
                            email
                            phone
                            job_title
                        }
                        custom_fields {
                            type
                            name
                            value
                        }
                    }
                }
            }
        }
    }
`;

interface IObjectOrganizationsProps {
    objectID: string;
    objectType: string;
}

export const ObjectOrganizations: FunctionComponent<IObjectOrganizationsProps> = ({ objectID, objectType }) => {
    const { data, loading, refetch } = useQuery(GET_ORGANIZATIONS, {
        variables: {
            id: objectID,
        },
    });

    const organizations = get(data, "objects.data[0].organizations");

    return loading ? (
        <Loading />
    ) : (
        <div className="b-object-organizations">
            {organizations &&
                organizations.map((organization: IOrganization) => (
                    <div className="b-object-organizations__item" key={organization.id}>
                        <RelationalList {...organization} refetch={refetch} objectId={objectID} objectType={objectType} />
                    </div>
                ))}
        </div>
    );
};
