import React, { FunctionComponent } from "react";
import "./OrganizationKeyContacts.scss";
import { PersonsList, PlusButton, useModal } from "../../ui";
import { IEmployee } from "../../../config/types";
import classnames from "classnames";
import { UserRoleCheck } from "../../userRoleCheck";
import { OrganizationKeyContactsModal } from "./organizationKeyContactsModal/OrganizationKeyContactsModal";

interface IOrganizationKeyContactsProps {
    contacts: IEmployee[];
    refetch: () => void;
    organizationID: string;
    isClientOrganization: boolean;
    contractID: string;
}

export const OrganizationKeyContacts: FunctionComponent<IOrganizationKeyContactsProps> = ({
    contacts,
    refetch,
    organizationID,
    isClientOrganization,
    contractID,
}) => {
    const { open, hide, isOpen } = useModal();

    const wrapperClassName = classnames(["b-organization-key-contacts__wrapper", { "_single-contact": contacts.length === 1 }]);

    return (
        <div className={wrapperClassName}>
            <header className="b-organization-key-contacts__header flex align-center _font-bold _text-grey _text-uppercase h6">
                Key contacts
                <UserRoleCheck
                    availableForRoles={[
                        "owner_administrator",
                        "manufacturer_administrator",
                        "manufacturer_brand_manager",
                        "manufacturer_object_manager",
                        "property_manager_administrator",
                        "property_manager_building_manager",
                        "service_provider_administrator",
                    ]}
                >
                    <PlusButton className="b-organization-key-contacts__header" onClick={open} />
                </UserRoleCheck>
            </header>
            {contacts.length !== 0 && (
                <PersonsList
                    variant="cockpit"
                    organizationID={organizationID}
                    isClientOrganization={isClientOrganization}
                    contractID={contractID}
                    contacts={contacts}
                    refetch={refetch}
                />
            )}
            <OrganizationKeyContactsModal
                hide={hide}
                isOpen={isOpen}
                organizationID={organizationID}
                isClientOrganization={isClientOrganization}
                contacts={contacts}
                refetch={refetch}
                contractID={contractID}
            />
        </div>
    );
};
