import React, { FunctionComponent } from "react";
import "./OrganizationKeyContactsModal.scss";
import { IModalProps, Modal } from "../../../ui/modal";
import { IEmployee } from "../../../../config/types";
import {
    IOrganizationKeyContactsModalFormState,
    OrganizationKeyContactsModalForm,
} from "./OrganizationKeyContactsModalForm";
import { Formik } from "formik";
import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../../ui/toasts";
import { getArrayIDs } from "../../../../views/app/event/addEvent/utils";

const UPDATE_CONTRACT = gql`
    mutation UpdateContract($id: String!, $organization_id: String!, $employee_ids: [String]!) {
        updateContract(id: $id, organization_id: $organization_id, employee_ids: $employee_ids) {
            id
        }
    }
`;

interface IOrganizationKeyContactsModalProps extends IModalProps {
    organizationID: string;
    isClientOrganization: boolean;
    contacts: IEmployee[];
    refetch: () => void;
    contractID: string;
}

export const OrganizationKeyContactsModal: FunctionComponent<IOrganizationKeyContactsModalProps> = ({
    organizationID,
    contacts,
    contractID,
    refetch,
    isClientOrganization,
    ...props
}) => {
    const [updateContacts] = useMutation(UPDATE_CONTRACT);

    async function onSubmit(values: IOrganizationKeyContactsModalFormState) {
        try {
            const { errors } = await updateContacts({
                variables: {
                    id: contractID,
                    organization_id: organizationID,
                    employee_ids: getArrayIDs(values.contacts),
                },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                showSuccessToast("Contacts are updated");
                refetch();
                props.hide();
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    return (
        <Modal {...props}>
            <Formik
                onSubmit={onSubmit}
                initialValues={{ contacts }}
                render={(formikProps) => (
                    <OrganizationKeyContactsModalForm
                        isClientOrganization={isClientOrganization}
                        organizationID={organizationID}
                        refetch={refetch}
                        contractID={contractID}
                        {...formikProps}
                    />
                )}
            />
        </Modal>
    );
};
