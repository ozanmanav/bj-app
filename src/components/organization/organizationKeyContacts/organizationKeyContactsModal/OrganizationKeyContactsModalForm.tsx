import React, { FunctionComponent, useState, useEffect } from "react";
import { AutocompleteInput } from "../../../ui/inputs/autocomplete";
import { FormikProps } from "formik";
import { IEmployee } from "../../../../config/types";
import { ModalFooter, useModal } from "../../../ui/modal";
import { Button } from "../../../ui/buttons";
import { PersonExpanded } from "../../../ui/person";
import { AddUserModal } from "../../../modals/addUserModal";
import { IUser, IJob } from "../../../forms/addUserForm/definitions";
import { useApolloClient } from "@apollo/react-hooks";
import { GET_DETAILS } from "../../../object/objectDetailsTable/queries";
import get from "lodash.get";

export interface IOrganizationKeyContactsModalFormState {
    contacts: IEmployee[];
}

interface IOrganizationKeyContactsModalFormProps extends FormikProps<IOrganizationKeyContactsModalFormState> {
    organizationID: string;
    isClientOrganization: boolean;
    refetch: () => void;
    contractID: string;
}

export const OrganizationKeyContactsModalForm: FunctionComponent<IOrganizationKeyContactsModalFormProps> = ({
    organizationID,
    refetch,
    isClientOrganization,
    contractID,
    ...formikProps
}) => {
    const [autocompleteRefetch, setAutocompleteRefetch] = useState<boolean>(false);
    const [autoCompleteInputValue, setAutocompleteInputValue] = useState("");

    const [organizationCountryName, setOrganizationCountryName] = useState<string>("");

    const { hide, open, isOpen } = useModal();

    const { values, handleSubmit, setFieldValue } = formikProps;

    const apolloClient = useApolloClient();

    useEffect(() => {
        const getOrganizationDetail = async () => {
            const { data: detailData } = await apolloClient.query({
                query: GET_DETAILS,
                variables: {
                    model_id: organizationID,
                    model: "ORGANIZATION",
                },
            });

            const organizationDetail = get(detailData, "details.data");
            setOrganizationCountryName(organizationDetail.filter((detail: any) => detail.name === "country")[0].value);
        };

        if (organizationID) {
            getOrganizationDetail();
        }
    }, [organizationID]);

    function onSelect(contact: IEmployee) {
        if (!values.contacts.find(({ id }) => id === contact.id)) {
            setFieldValue("contacts", [...values.contacts, contact]);
        }

        setAutocompleteInputValue("");
    }

    function onAddNewUser(user: IUser, employeeID?: string, job?: IJob) {
        if (user) {
            // should convert to employee
            if (employeeID) {
                const { id = "", first_name, last_name, email, phone, phone_code, language } = user;

                const contact: IEmployee = {
                    id: employeeID,
                    user: {
                        id,
                        first_name,
                        last_name,
                        email,
                        phone: `${phone_code} ${phone}`,
                        language,
                    },
                    job,
                };

                onAddContact(contact);
            }
        }
    }

    function onAddContact(contact: IEmployee) {
        setFieldValue("contacts", [...values.contacts, contact]);
        setAutocompleteRefetch(true);
    }

    function onDeleteContact(contact: IEmployee) {
        setFieldValue(
            "contacts",
            values.contacts.filter((c) => c.id !== contact.id)
        );
    }

    return (
        <>
            <form onSubmit={handleSubmit}>
                <h2 className="b-organization-key-contacts-modal__title h1">Edit contacts</h2>
                {!isClientOrganization && (
                    <Button text="Add new contact" onClick={open} className="b-organization-key-contacts-modal__new-contact" />
                )}

                <AutocompleteInput
                    className="b-organization-key-contacts-modal__autocomplete"
                    placeholder="Search by persons name or E-Mail in existing contacts..."
                    searchFor="employee"
                    onChange={onSelect}
                    organizationID={organizationID}
                    isSetValue={false}
                    isRefetch={autocompleteRefetch}
                    setRefetch={setAutocompleteRefetch}
                    defaultInputValue={autoCompleteInputValue}
                />
                <div>
                    {values.contacts.map((employee) => (
                        <PersonExpanded
                            key={employee.id}
                            variant="standalone"
                            employee={employee}
                            organizationID={organizationID}
                            isClientOrganization={isClientOrganization}
                            deleteContact={onDeleteContact}
                        />
                    ))}
                </div>
                <ModalFooter>
                    <Button text="Save" primary type="submit" />
                </ModalFooter>
            </form>
            <AddUserModal
                organizationID={organizationID}
                hide={hide}
                isOpen={isOpen}
                isClientOrganization={isClientOrganization}
                onUserAdded={onAddNewUser}
                companyCountry={organizationCountryName}
            />
        </>
    );
};
