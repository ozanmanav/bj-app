import React, { FunctionComponent } from "react";
import "./Organization.scss";
import { PlusButton, Row, Column, useModal, Loading } from "../ui";
import { OrganizationOwner } from "./organizationOwner";
import { OrganizationKeyContacts } from "./organizationKeyContacts";
import { OrganizationContract } from "./organizationContract";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { IContract } from "../../config/types";
import { ContractModal } from "../modals/contractModal";
import { UserRoleCheck } from "../userRoleCheck";
import { CopyRelationships } from "./copyRelationships";
import get from "lodash.get";

// TODO: details deleted from this query, files will implement in the files list component, another thing should another place.
const GET_CONTRACTS = gql`
    query GetContracts($object_id: String) {
        contracts(object_id: $object_id) {
            data {
                id
                role {
                    name
                }
                events {
                    id
                    timeline
                }
                details {
                    id
                    name
                    type
                    value
                    context
                }
                organization {
                    id
                    name
                    category
                    address {
                        country
                        street
                        number
                        city
                    }
                    email
                    phone
                    created_at
                    logo {
                        url
                        name
                    }
                }
                employees {
                    id
                    job {
                        name
                        id
                    }
                    user {
                        id
                        first_name
                        last_name
                        phone
                        email
                        language
                    }
                }
                files {
                    id
                    type {
                        id
                        type
                        name
                    }
                    name
                    url
                    filename
                    extension
                    size
                    events {
                        id
                        timeline
                    }
                    updated_at
                    created_at
                    details {
                        context
                        type
                        value
                        name
                        meta {
                            last_modification {
                                modifier {
                                    name
                                }
                            }
                            author {
                                name
                            }
                            context
                            verification
                            verification_by
                            source_url
                            source_date
                            source
                        }
                    }
                    mime_type
                }
                objects {
                    id
                    name
                    type {
                        id
                    }
                }
            }
        }
    }
`;

interface IOrganizationProps {
    objectID: string;
    isBuilding: boolean;
    eventRefetch: Function;
}

export const Organization: FunctionComponent<IOrganizationProps> = ({ isBuilding, objectID, eventRefetch }) => {
    const { open, hide, isOpen } = useModal();

    const { data: contractsData, loading, refetch } = useQuery(GET_CONTRACTS, {
        variables: {
            object_id: objectID,
        },
    });

    const contracts = get(contractsData, "contracts.data") || [];

    return (
        <>
            {loading ? (
                <Loading />
            ) : (
                <div className="b-organization">
                    <h3 className="flex align-center b-organization__title">
                        Organization and People
                        <UserRoleCheck
                            availableForRoles={[
                                "owner_administrator",
                                "manufacturer_administrator",
                                "manufacturer_brand_manager",
                                "manufacturer_object_manager",
                                "property_manager_administrator",
                                "property_manager_building_manager",
                                "service_provider_administrator",
                            ]}
                        >
                            <PlusButton onClick={open} />
                            <CopyRelationships objectID={objectID} refetch={refetch} />
                        </UserRoleCheck>
                    </h3>
                    {contracts.map((contract: IContract) => {
                        const contractEmployees = get(contract, "employees");
                        const contractOrganization = get(contract, "organization");
                        const contractObjects = get(contract, "objects");
                        const contractRoleName = get(contract, "role.name");
                        const contractEvents = get(contract, "events");

                        return (
                            contractOrganization && (
                                <Row gutter="sm" className="b-organization__row" key={contract.id}>
                                    <Column>
                                        <OrganizationOwner
                                            objectType={contractRoleName}
                                            contractId={contract.id}
                                            objectId={objectID}
                                            contractEvents={contractEvents}
                                            eventRefetch={eventRefetch}
                                            {...contractOrganization}
                                        />
                                    </Column>
                                    <Column>
                                        <OrganizationKeyContacts
                                            refetch={refetch}
                                            organizationID={contractOrganization.id}
                                            isClientOrganization={contractOrganization.category === "CLIENT"}
                                            contacts={contractEmployees}
                                            contractID={contract.id}
                                        />
                                        <OrganizationContract
                                            objectId={objectID}
                                            refetch={refetch}
                                            contracts={[contract]}
                                            objects={contractObjects}
                                        />
                                    </Column>
                                </Row>
                            )
                        );
                    })}
                </div>
            )}
            <ContractModal objectID={objectID} hide={hide} refetch={refetch} isOpen={isOpen} />
        </>
    );
};
