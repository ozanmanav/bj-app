import React, { FunctionComponent, useState } from "react";
import "./OrganizationRelatedUsersTable.scss";
import { TableCollapseButton } from "../../tables";
import { RemoveIconButton, SortButton } from "../../ui/buttons";
import { useModal } from "../../ui/modal";
import gql from "graphql-tag";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui/toasts";
import { useMutation } from "@apollo/react-hooks";
import { ConfirmModal } from "../../modals/confirmModal";
import { ActionList } from "../../ui/actionList";
import orderBy from "lodash.orderby";
import { useSortingTable } from "../../../hooks";

const DELETE_EMPLOYEE = gql`
    mutation DeleteEmployee($organization_id: String!, $id: String!) {
        removeOrganizationUser(organization_id: $organization_id, user_id: $id)
    }
`;

const ITEMS_TO_SHOW = 2;

const SORT_STRINGS = {
    NAME: "first_name",
    ROLE: "job_title",
    EMAIL: "email",
    PHONE: "phone",
};

interface IOrganizationRelatedUsersTableUser {
    id: string;
    first_name: string;
    last_name: string;
    job_title: string;
    email: string;
    phone: string;
}

interface IRelatedUsersTableProps {
    users: IOrganizationRelatedUsersTableUser[];
    organizationId: string;
    refetch: () => void;
}

interface IOrganizationRelatedUsersTableRowProps {
    user: IOrganizationRelatedUsersTableUser;
    organizationId: string;
    refetch: () => void;
}

const OrganizationRelatedUsersTableRow: FunctionComponent<IOrganizationRelatedUsersTableRowProps> = ({
    user,
    refetch,
    organizationId,
}) => {
    const { open: openDeleteUserModal, hide: hideDeleteUserModal, isOpen: isOpenOpenDeleteUserModal } = useModal();

    const [removeEmployeeMutation] = useMutation(DELETE_EMPLOYEE);

    async function removeEmployee() {
        try {
            hideDeleteUserModal();

            const { errors } = await removeEmployeeMutation({
                variables: {
                    organization_id: organizationId,
                    id: user.id,
                },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                refetch();
                showSuccessToast("User is successfully deleted.");
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    return (
        <tr className="b-table__row _medium">
            <td className="b-table__cell b-organization-related-users__cell _name">
                {user.first_name} {user.last_name}
            </td>
            <td className="b-table__cell b-organization-related-users__cell _role">{user.job_title}</td>
            <td className="b-table__cell b-organization-related-users__cell _email">{user.email}</td>
            <td className="b-table__cell b-organization-related-users__cell _phone">{user.phone}</td>
            <td className="b-table__cell b-organization-related-users__cell _actions-cell _border-left">
                <ActionList>
                    <RemoveIconButton onClick={openDeleteUserModal} />
                </ActionList>
                <ConfirmModal
                    title="Are you sure?"
                    onConfirm={removeEmployee}
                    hide={hideDeleteUserModal}
                    isOpen={isOpenOpenDeleteUserModal}
                />
            </td>
        </tr>
    );
};

export const OrganizationRelatedUsersTable: FunctionComponent<IRelatedUsersTableProps> = ({
    users,
    organizationId,
    refetch,
}) => {
    const { sortBy, sortingOrder, getSortOrderForField, changeSortBy } = useSortingTable({
        localStorageKey: "admin_organization_related_users",
    });

    const sortedUsers = orderBy(users, [sortBy], [sortingOrder]);

    const [isCollapsed, setIsCollapsed] = useState<boolean>(true);
    const shouldAllowCollapse = sortedUsers.length > ITEMS_TO_SHOW;
    const usersToShow = isCollapsed && shouldAllowCollapse ? sortedUsers.slice(0, ITEMS_TO_SHOW) : sortedUsers;
    const collapsedItems = sortedUsers.length - ITEMS_TO_SHOW;

    function toggle() {
        setIsCollapsed(!isCollapsed);
    }

    function sortByName() {
        changeSortBy(SORT_STRINGS.NAME);
    }

    function sortByRole() {
        changeSortBy(SORT_STRINGS.ROLE);
    }

    function sortByEmail() {
        changeSortBy(SORT_STRINGS.EMAIL);
    }

    function sortByPhone() {
        changeSortBy(SORT_STRINGS.PHONE);
    }

    return (
        <div className="b-table__wrapper b-organization-related-users__wrapper">
            <table className="b-table b-organization-related-users">
                <thead className="b-table__head">
                    <tr className="b-table__row _head">
                        <th className="b-table__cell b-organization-related-users__cell _name">
                            <SortButton
                                text="User Name"
                                onClick={sortByName}
                                sorted={getSortOrderForField(SORT_STRINGS.NAME)}
                            />
                        </th>
                        <th className="b-table__cell b-organization-related-users__cell _role">
                            <SortButton
                                text="Role"
                                onClick={sortByRole}
                                sorted={getSortOrderForField(SORT_STRINGS.ROLE)}
                            />
                        </th>
                        <th className="b-table__cell b-organization-related-users__cell _email">
                            <SortButton
                                text="E-Mail"
                                onClick={sortByEmail}
                                sorted={getSortOrderForField(SORT_STRINGS.EMAIL)}
                            />
                        </th>
                        <th className="b-table__cell b-organization-related-users__cell _phone">
                            <SortButton
                                text="Phone"
                                onClick={sortByPhone}
                                sorted={getSortOrderForField(SORT_STRINGS.PHONE)}
                            />
                        </th>
                        <th className="b-table__cell b-organization-related-users__cell _actions-cell" />
                    </tr>
                </thead>
                <tbody className="b-table__body">
                    {usersToShow.map((user: IOrganizationRelatedUsersTableUser) => (
                        <OrganizationRelatedUsersTableRow
                            user={user}
                            organizationId={organizationId}
                            refetch={refetch}
                            key={user.id}
                        />
                    ))}
                </tbody>
            </table>
            {shouldAllowCollapse && (
                <TableCollapseButton isCollapsed={isCollapsed} collapsedItems={collapsedItems} onClick={toggle} />
            )}
        </div>
    );
};
