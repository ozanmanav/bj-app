import React, { FunctionComponent, useState } from "react";
import "./RelatedUsersTable.scss";
import { TableCollapseButton } from "../../tables";
import { ISettingsRelatedUser } from "../../../config/types";
import { UserRoleCheck } from "../../userRoleCheck";

import { RelatedUsersTableTableRow } from "./RelatedUserTableRow";
import { useSortingTable } from "../../../hooks";
import get from "lodash.get";
import orderBy from "lodash.orderby";
import { SortButton } from "../../ui/buttons";
import { TAddUserFormVariants } from "../../forms/addUserForm/definitions";

const ITEMS_TO_SHOW = 2;

const SORT_STRINGS = {
    USERNAME: "user.first_name",
    ROLE: "user_role.name",
};

interface IRelatedUsersTableProps {
    users: ISettingsRelatedUser[];
    organizationID: string;
    companyType?: string;
    refetch: () => void;
    isAdminPanel?: boolean;
    variant?: TAddUserFormVariants;
    adminID?: string;
}

export const RelatedUsersTable: FunctionComponent<IRelatedUsersTableProps> = ({
    users,
    organizationID,
    companyType,
    refetch,
    isAdminPanel,
    variant,
    adminID,
}) => {
    const { sortBy, sortingOrder, changeSortBy, getSortOrderForField } = useSortingTable({ localStorageKey: "related_users" });

    const sortedUsers = orderBy(users, [(user) => (get(user, sortBy) || "").toLowerCase()], [sortingOrder]);

    const [isCollapsed, setIsCollapsed] = useState<boolean>(true);
    const shouldAllowCollapse = sortedUsers.length > ITEMS_TO_SHOW;
    const usersToShow = isCollapsed && shouldAllowCollapse ? sortedUsers.slice(0, ITEMS_TO_SHOW) : sortedUsers;
    const collapsedItems = sortedUsers.length - ITEMS_TO_SHOW;

    function toggle() {
        setIsCollapsed(!isCollapsed);
    }

    function sortByName() {
        changeSortBy(SORT_STRINGS.USERNAME);
    }

    function sortByRole() {
        changeSortBy(SORT_STRINGS.ROLE);
    }

    return (
        <div className="b-table__wrapper b-related-users__wrapper">
            <table className="b-table b-related-users">
                <thead className="b-table__head">
                    <tr className="b-table__row _head">
                        <th className="b-table__cell b-related-users__cell _name">
                            <SortButton text="User Name" onClick={sortByName} sorted={getSortOrderForField(SORT_STRINGS.USERNAME)} />
                        </th>
                        <th className="b-table__cell b-related-users__cell _role">
                            <SortButton text="Role" onClick={sortByRole} sorted={getSortOrderForField(SORT_STRINGS.ROLE)} />
                        </th>
                        <th className="b-table__cell b-related-users__cell _jobTitle">
                            <SortButton text="Job Title" onClick={sortByRole} sorted={getSortOrderForField(SORT_STRINGS.ROLE)} />
                        </th>
                        <th className="b-table__cell b-related-users__cell _email">
                            <SortButton text="Email" onClick={sortByRole} sorted={getSortOrderForField(SORT_STRINGS.ROLE)} />
                        </th>
                        <th className="b-table__cell b-related-users__cell _phone">
                            <SortButton text="Phone" onClick={sortByRole} sorted={getSortOrderForField(SORT_STRINGS.ROLE)} />
                        </th>
                        <UserRoleCheck
                            availableForRoles={[
                                "owner_administrator",
                                "manufacturer_administrator",
                                "property_manager_administrator",
                                "service_provider_administrator",
                            ]}
                        >
                            <th className="b-table__cell b-related-users__cell _actions-cell" />
                        </UserRoleCheck>
                    </tr>
                </thead>
                <tbody className="b-table__body">
                    {usersToShow.map(({ user, user_role, job_title }: ISettingsRelatedUser) => (
                        <RelatedUsersTableTableRow
                            user={user}
                            job_title={job_title}
                            user_id={user.id}
                            role={user_role}
                            organizationID={organizationID}
                            companyType={companyType}
                            refetch={refetch}
                            key={user.id + job_title}
                            variant={variant}
                            isAdminPanel={isAdminPanel}
                            adminID={adminID}
                        />
                    ))}
                </tbody>
            </table>
            {shouldAllowCollapse && <TableCollapseButton isCollapsed={isCollapsed} collapsedItems={collapsedItems} onClick={toggle} />}
        </div>
    );
};
