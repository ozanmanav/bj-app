import React, { FunctionComponent } from "react";
import { useModal } from "../../ui/modal";
import { useMutation } from "@apollo/react-hooks";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui/toasts";
import { UserRoleCheck } from "../../userRoleCheck";
import { ActionList } from "../../ui/actionList";
import { EditButton, RemoveIconButton } from "../../ui/buttons";
import { ConfirmModal } from "../../modals/confirmModal";
import { AddUserModal } from "../../modals/addUserModal";
import { ISettingsUser, ISettingsUserRole } from "../../../config/types";
import gql from "graphql-tag";
import { getPhoneInitialValues } from "../../../utils";
import { TAddUserFormVariants } from "../../forms/addUserForm/definitions";

const REMOVE_COMPANY_USER = gql`
    mutation RemoveOrganizationUser($organization_id: String!, $user_id: String!) {
        removeOrganizationUser(organization_id: $company_id, user_id: $user_id)
    }
`;

interface IRelatedUsersTableRowProps {
    user: ISettingsUser;
    user_id: string;
    role: ISettingsUserRole;
    organizationID: string;
    companyType?: string;
    refetch: () => void;
    isAdminPanel?: boolean;
    variant?: TAddUserFormVariants;
    adminID?: string;
    job_title: string;
}

export const RelatedUsersTableTableRow: FunctionComponent<IRelatedUsersTableRowProps> = ({
    user,
    user_id,
    role,
    refetch,
    organizationID,
    companyType,
    isAdminPanel,
    variant,
    adminID,
    job_title,
}) => {
    const { open: openUpdateUserModal, hide: hideUpdateUserModal, isOpen: isOpenOpenUpdateUserModal } = useModal();
    const { open: openDeleteUserModal, hide: hideDeleteUserModal, isOpen: isOpenOpenDeleteUserModal } = useModal();

    const [removeUserMutation] = useMutation(REMOVE_COMPANY_USER);

    async function onUserRemove() {
        try {
            hideDeleteUserModal();

            const res = await removeUserMutation({
                variables: {
                    organization_id: organizationID,
                    user_id,
                },
            });
            const { data, errors } = res;

            if (errors) {
                handleGraphQLErrors(errors);
            } else if (data.removeCompanyUser) {
                refetch();
                showSuccessToast("User is successfully deleted.");
            } else {
                showErrorToast("User is not deleted.");
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    const { phone, phone_code } = getPhoneInitialValues(user.phone);

    const editedUser = {
        id: user.id,
        email: user.email,
        first_name: user.first_name,
        last_name: user.last_name,
        role: role && role.id,
        job_title,
        phone,
        phone_code,
    };

    return (
        <tr className="b-table__row _medium">
            <td className="b-table__cell b-related-users__cell _name">
                {user.first_name} {user.last_name}
            </td>
            <td className="b-table__cell b-related-users__cell _role">{role && role.name}</td>
            <td className="b-table__cell b-related-users__cell _jobTitle">{job_title}</td>
            <td className="b-table__cell b-related-users__cell _email">{user.email}</td>
            <td className="b-table__cell b-related-users__cell _phone">{user.phone}</td>
            <UserRoleCheck
                availableForRoles={[
                    "owner_administrator",
                    "manufacturer_administrator",
                    "property_manager_administrator",
                    "service_provider_administrator",
                ]}
            >
                <td className="b-table__cell b-related-users__cell _actions-cell _border-left">
                    <ActionList>
                        <EditButton onClick={openUpdateUserModal} />
                        {user_id !== adminID && <RemoveIconButton onClick={openDeleteUserModal} />}
                    </ActionList>
                    <ConfirmModal
                        title="Are you sure?"
                        onConfirm={onUserRemove}
                        hide={hideDeleteUserModal}
                        isOpen={isOpenOpenDeleteUserModal}
                    />
                    <AddUserModal
                        editedUser={editedUser}
                        refetch={refetch}
                        hide={hideUpdateUserModal}
                        isOpen={isOpenOpenUpdateUserModal}
                        organizationID={organizationID}
                        companyType={companyType}
                        adminID={adminID}
                        isAdminPanel={isAdminPanel}
                        variant={variant}
                    />
                </td>
            </UserRoleCheck>
        </tr>
    );
};
