import React, { ChangeEvent, FunctionComponent, useState } from "react";
import "./AdminTypesTable.scss";
import { EditButton, PlusButton, SortButton } from "../../ui/buttons";
import { TableCollapseButton } from "../tableCollapseButton";
import { AdminMetaModal } from "../../modals/adminMetaModal";
import { useModal } from "../../ui/modal";
import { SearchInput, AUTOCOMPLETE_DEBOUNCE_TIME_MS } from "../../ui/inputs";
import { useQuery } from "@apollo/react-hooks";
import { useDebounce } from "use-debounce";
import { TAdminEntityTypes, TYPE_QUERIES } from "./config";
import { Loading } from "../../ui/loading";
import { GRAPHQL_ADMIN_CONTEXT, IGroup } from "../../../config";
import { EditGroupsModal } from "../../modals/editGroupsModal";
import { AdminTypesTableRow } from "./AdminTypesTableRow";
import get from "lodash.get";
import classNames from "classnames";
import { TSortingTableKey, useSortingTable } from "../../../hooks";
import orderBy from "lodash.orderby";

const ITEMS_TO_SHOW = 4;

const SORT_STRINGS = {
    NAME: "name",
    GROUP_NAME: "groups[0].name",
};

interface IAdminTypesTableProps {
    entityType: TAdminEntityTypes;
}

export const AdminTypesTable: FunctionComponent<IAdminTypesTableProps> = ({ entityType }) => {
    const [isCollapsed, setIsCollapsed] = useState<boolean>(true);
    const [inputValue, setInputValue] = useState<string>("");
    const [isFullyCollapsed, setIsFullyCollapsed] = useState<boolean>(false);

    const { sortBy, sortingOrder, changeSortBy, getSortOrderForField } = useSortingTable({
        localStorageKey: `admin_types_${entityType}` as TSortingTableKey,
    });

    const [searchBy] = useDebounce(inputValue, AUTOCOMPLETE_DEBOUNCE_TIME_MS);

    const { hide: hideAddTypeModal, open: openAddTypeModal, isOpen: isAddTypeModalOpen } = useModal();
    const { hide: hideEditGroupsModal, open: openEditGroupsModal, isOpen: isEditGroupsModalOpen } = useModal();

    const { data: entitiesData, loading: entitesLoading, refetch: entitiesRefetch } = useQuery(TYPE_QUERIES[entityType].query, {
        variables: {
            searchBy,
        },
        ...GRAPHQL_ADMIN_CONTEXT,
    });

    const { data: groupsData, loading: groupsLoading, refetch: groupsRefetch } = useQuery(TYPE_QUERIES[entityType].groupsQuery, {
        fetchPolicy: "network-only",
        ...GRAPHQL_ADMIN_CONTEXT,
    });

    const objectTypes = orderBy(get(entitiesData, "entity_types.data") || [], [sortBy], [sortingOrder]);
    const groups = get(groupsData, "groups.data") || [];

    const shouldAllowCollapse = objectTypes.length > ITEMS_TO_SHOW;
    const dataToShow = isCollapsed && shouldAllowCollapse ? objectTypes.slice(0, ITEMS_TO_SHOW) : objectTypes;
    const collapsedItems = objectTypes.length - ITEMS_TO_SHOW;

    const entityTypeName = entityType[0].toUpperCase() + entityType.slice(1).replace("_", " ");

    function toggle() {
        setIsCollapsed(!isCollapsed);
    }

    function onSearchFieldChange(e: ChangeEvent<HTMLInputElement>) {
        setInputValue(e.target.value);
    }

    function toggleFullyCollapsed() {
        setIsFullyCollapsed((prevState) => !prevState);
    }

    function sortByName() {
        changeSortBy(SORT_STRINGS.NAME);
    }

    function sortByGroupName() {
        changeSortBy(SORT_STRINGS.GROUP_NAME);
    }

    const tableBodyClassName = classNames(["b-table__body b-admin-object-types-table__body", { _none: isFullyCollapsed }]);

    return (
        <div className="b-admin-object-types-table__outer">
            <div className="b-admin-types-control__header flex align-center justify-between">
                <h3 className="b-admin-types-control__title _cursor-pointer">
                    <div className="flex">
                        <div onClick={toggleFullyCollapsed}> {entityTypeName} types</div>
                        <PlusButton onClick={openAddTypeModal} />
                    </div>
                </h3>
                <SearchInput
                    className="b-admin-types-control__search"
                    placeholder="Search type..."
                    value={inputValue}
                    onChange={onSearchFieldChange}
                    marginBottom="none"
                />
            </div>
            {entitesLoading ? (
                <Loading />
            ) : objectTypes.length > 0 ? (
                <div className="b-table__wrapper b-admin-object-types-table__wrapper">
                    <table className="b-table b-admin-object-types-table">
                        <thead className="b-table__head">
                            <tr className="b-table__row _head">
                                <th className="b-table__cell b-admin-object-types-table__cell _name">
                                    <SortButton text="Type name" sorted={getSortOrderForField(SORT_STRINGS.NAME)} onClick={sortByName} />
                                </th>
                                <th className="b-table__cell b-admin-object-types-table__cell _group">
                                    <div className="flex align-center">
                                        <SortButton
                                            text="Group"
                                            sorted={getSortOrderForField(SORT_STRINGS.GROUP_NAME)}
                                            onClick={sortByGroupName}
                                        />
                                        <EditButton className="b-admin-object-types-table__group-edit" onClick={openEditGroupsModal} />
                                    </div>
                                </th>
                                {/* <th className="b-table__cell b-admin-object-types-table__cell _actions-cell">
                                    <button className={collapseButtonClassName} onClick={toggleFullyCollapsed}>
                                        <Icon icon="arrowDown" />
                                    </button>
                                </th> */}
                            </tr>
                        </thead>
                        <tbody className={tableBodyClassName}>
                            {dataToShow.map((item: { id: string; name: string; groups: IGroup[] }) => (
                                <AdminTypesTableRow
                                    {...item}
                                    key={item.id}
                                    entityType={entityType}
                                    onDataChange={entitiesRefetch}
                                    allGroups={groups}
                                />
                            ))}
                        </tbody>
                    </table>
                    {shouldAllowCollapse && !isFullyCollapsed && (
                        <TableCollapseButton isCollapsed={isCollapsed} collapsedItems={collapsedItems} onClick={toggle} />
                    )}
                </div>
            ) : (
                <p className="b-admin-object-types-table__empty">No types added.</p>
            )}
            <AdminMetaModal refetch={entitiesRefetch} hide={hideAddTypeModal} isOpen={isAddTypeModalOpen} entityType={entityType} />
            <EditGroupsModal
                groups={groups}
                loading={groupsLoading}
                refetch={groupsRefetch}
                entityType={entityType}
                entityTypeName={entityTypeName}
                hide={hideEditGroupsModal}
                isOpen={isEditGroupsModalOpen}
            />
        </div>
    );
};
