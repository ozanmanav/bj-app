import gql from "graphql-tag";

export type TAdminEntityTypes = "object" | "organization" | "contract" | "relationship" | "file" | "event" | "job_title";

export const TYPE_QUERIES = {
    object: {
        query: gql`
            query EntityTypes($searchBy: String) {
                entity_types(entity: "OBJECT", is_file: false, name: $searchBy) {
                    data {
                        id
                        name
                        groups {
                            id
                            name
                        }
                    }
                }
            }
        `,
        createMutation: gql`
            mutation CreateEntityType($name: String!) {
                createEntityType(entity_type: OBJECT, name: $name) {
                    id
                }
            }
        `,
        createTypeMetaMutation: gql`
            mutation CreateTypeMeta($model_id: String!, $metas: [MetasField]) {
                createTypeMeta(model_id: $model_id, metas: $metas) {
                    name
                }
            }
        `,
        groupsQuery: gql`
            query GetGroups {
                groups(entity: "OBJECT", is_file: false) {
                    data {
                        id
                        name
                        entity
                    }
                }
            }
        `,
        createGroupMutation: gql`
            mutation CreateGroup($name: String!) {
                createGroup(name: $name, entity: OBJECT, is_file: false) {
                    id
                }
            }
        `,
    },
    organization: {
        query: gql`
            query EntityTypes($searchBy: String) {
                entity_types(entity: "ORGANIZATION", name: $searchBy) {
                    data {
                        id
                        name
                        groups {
                            id
                            name
                        }
                    }
                }
            }
        `,
        createMutation: gql`
            mutation CreateEntityType($name: String!) {
                createEntityType(entity_type: ORGANIZATION, name: $name) {
                    id
                }
            }
        `,
        createTypeMetaMutation: gql`
            mutation CreateTypeMeta($model_id: String!, $metas: [MetasField]) {
                createTypeMeta(model_id: $model_id, metas: $metas) {
                    name
                }
            }
        `,
        groupsQuery: gql`
            query GetGroups {
                groups(entity: "ORGANIZATION") {
                    data {
                        id
                        name
                        entity
                    }
                }
            }
        `,
        createGroupMutation: gql`
            mutation CreateGroup($name: String!) {
                createGroup(name: $name, entity: ORGANIZATION) {
                    id
                }
            }
        `,
    },
    contract: {
        query: gql`
            query EntityTypes($searchBy: String) {
                entity_types(entity: "CONTRACT", name: $searchBy) {
                    data {
                        id
                        name
                        groups {
                            id
                            name
                        }
                    }
                }
            }
        `,
        createMutation: gql`
            mutation CreateEntityType($name: String!) {
                createEntityType(entity_type: CONTRACT, name: $name) {
                    id
                }
            }
        `,
        createTypeMetaMutation: gql`
            mutation CreateTypeMeta($model_id: String!, $metas: [MetasField]) {
                createTypeMeta(model_id: $model_id, metas: $metas) {
                    name
                }
            }
        `,
        groupsQuery: gql`
            query GetGroups {
                groups(entity: "CONTRACT") {
                    data {
                        id
                        name
                        entity
                    }
                }
            }
        `,
        createGroupMutation: gql`
            mutation CreateGroup($name: String!) {
                createGroup(name: $name, entity: CONTRACT) {
                    id
                }
            }
        `,
    },
    file: {
        query: gql`
            query EntityTypes($searchBy: String) {
                entity_types(entity: "OBJECT", is_file: true, name: $searchBy) {
                    data {
                        id
                        name
                        groups {
                            id
                            name
                        }
                    }
                }
            }
        `,
        createMutation: gql`
            mutation CreateEntityType($name: String!) {
                createEntityType(entity_type: OBJECT, is_file: true, name: $name) {
                    id
                }
            }
        `,
        createTypeMetaMutation: gql`
            mutation CreateTypeMeta($model_id: String!, $metas: [MetasField]) {
                createTypeMeta(model_id: $model_id, metas: $metas) {
                    name
                }
            }
        `,
        groupsQuery: gql`
            query GetGroups {
                groups(entity: "OBJECT", is_file: true) {
                    data {
                        id
                        name
                        entity
                    }
                }
            }
        `,
        createGroupMutation: gql`
            mutation CreateGroup($name: String!) {
                createGroup(name: $name, entity: OBJECT, is_file: true) {
                    id
                }
            }
        `,
    },
    event: {
        query: gql`
            query EntityTypes($searchBy: String) {
                entity_types(entity: "EVENT", name: $searchBy) {
                    data {
                        id
                        name
                        groups {
                            id
                            name
                        }
                    }
                }
            }
        `,
        createMutation: gql`
            mutation CreateEntityType($name: String!) {
                createEntityType(entity_type: EVENT, name: $name) {
                    id
                }
            }
        `,
        createTypeMetaMutation: gql`
            mutation CreateTypeMeta($model_id: String!, $metas: [MetasField]) {
                createTypeMeta(model_id: $model_id, metas: $metas) {
                    name
                }
            }
        `,
        groupsQuery: gql`
            query GetGroups {
                groups(entity: "EVENT") {
                    data {
                        id
                        name
                        entity
                    }
                }
            }
        `,
        createGroupMutation: gql`
            mutation CreateGroup($name: String!) {
                createGroup(name: $name, entity: EVENT) {
                    id
                }
            }
        `,
    },
    job_title: {
        query: gql`
            query EntityTypes($searchBy: String) {
                entity_types(entity: "JOB_TITLE", name: $searchBy) {
                    data {
                        id
                        name
                        groups {
                            id
                            name
                        }
                    }
                }
            }
        `,
        createMutation: gql`
            mutation CreateEntityType($name: String!) {
                createEntityType(entity_type: JOB_TITLE, name: $name) {
                    id
                }
            }
        `,
        createTypeMetaMutation: gql`
            mutation CreateTypeMeta($model_id: String!, $metas: [MetasField]) {
                createTypeMeta(model_id: $model_id, metas: $metas) {
                    name
                }
            }
        `,
        groupsQuery: gql`
            query GetGroups {
                groups(entity: "JOB_TITLE") {
                    data {
                        id
                        name
                        entity
                    }
                }
            }
        `,
        createGroupMutation: gql`
            mutation CreateGroup($name: String!) {
                createGroup(name: $name, entity: JOB_TITLE) {
                    id
                }
            }
        `,
    },
    relationship: {
        query: gql`
            query EntityTypes($searchBy: String) {
                entity_types(entity: "RELATIONSHIP", name: $searchBy) {
                    data {
                        id
                        name
                        groups {
                            id
                            name
                        }
                    }
                }
            }
        `,
        createMutation: gql`
            mutation CreateEntityType($name: String!) {
                createEntityType(entity_type: RELATIONSHIP, name: $name) {
                    id
                }
            }
        `,
        createTypeMetaMutation: gql`
            mutation CreateTypeMeta($model_id: String!, $metas: [MetasField]) {
                createTypeMeta(model_id: $model_id, metas: $metas) {
                    name
                }
            }
        `,
        groupsQuery: gql`
            query GetGroups {
                groups(entity: "RELATIONSHIP") {
                    data {
                        id
                        name
                        entity
                    }
                }
            }
        `,
        createGroupMutation: gql`
            mutation CreateGroup($name: String!) {
                createGroup(name: $name, entity: RELATIONSHIP) {
                    id
                }
            }
        `,
    },
};
