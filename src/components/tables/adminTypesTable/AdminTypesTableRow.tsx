import React, { ChangeEvent, FunctionComponent, useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import { useModal } from "../../ui/modal";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui/toasts";
import { ActionList } from "../../ui/actionList";
import { EditButton, RemoveIconButton } from "../../ui/buttons";
import { AdminMetaModal } from "../../modals/adminMetaModal";
import { ConfirmModal } from "../../modals/confirmModal";
import { TAdminEntityTypes } from "./config";
import gql from "graphql-tag";
import { Select } from "../../ui/inputs";

import { IGroup } from "../../../config/types";
import get from "lodash.get";

interface IAdminTypesTableRowProps {
    id: string;
    name: string;
    entityType: TAdminEntityTypes;
    onDataChange: Function;
    groups: IGroup[];
    allGroups: IGroup[];
}

const DELETE_ENTITY_TYPE = gql`
    mutation DeleteEntity($id: String!) {
        deleteTypeEntity(id: $id)
    }
`;

const UPDATE_ENTITY_GROUP = gql`
    mutation UpdateEntityType($id: String!, $groups: [String]) {
        updateEntityType(id: $id, groups: $groups) {
            id
        }
    }
`;

export const AdminTypesTableRow: FunctionComponent<IAdminTypesTableRowProps> = ({
    id,
    name,
    groups,
    allGroups,
    entityType,
    onDataChange,
}) => {
    const [deleteTypeEntity] = useMutation(DELETE_ENTITY_TYPE, {
        variables: {
            id,
        },
    });

    const [updateGroup] = useMutation(UPDATE_ENTITY_GROUP);

    const [isGroupChanging, setIsGroupChanging] = useState<boolean>(false);

    const { hide: hideEditModal, open: openEditModal, isOpen: isEditModalOpen } = useModal();
    const { hide: hideConfirmModal, open: openConfirmModal, isOpen: isConfirmModalOpened } = useModal();

    async function deleteMetaType() {
        try {
            const { errors } = await deleteTypeEntity();

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                hideConfirmModal();
                onDataChange();
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    async function selectNewGroup(e: ChangeEvent<HTMLSelectElement>) {
        setIsGroupChanging(true);

        try {
            const { errors } = await updateGroup({
                variables: {
                    id,
                    groups: e.target.value === "reset" ? [] : [e.target.value],
                },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                await onDataChange();
                showSuccessToast("Group is updated");
            }
        } catch (e) {
            showErrorToast(e.message);
        }

        setIsGroupChanging(false);
    }

    const groupValue = allGroups.find((item) => item.id === get(groups, "[0].id"));
    const groupID = get(groupValue, "id");

    return (
        <tr className="b-table__row _medium">
            <td className="b-table__cell b-admin-object-types-table__cell _name">{name}</td>
            <td className="b-table__cell b-admin-object-types-table__cell _group">
                <Select
                    options={[...allGroups.map(({ id, name }) => ({ value: id, label: name })), { value: "reset", label: "Clear Group" }]}
                    value={groupID}
                    marginBottom="none"
                    placeholder="Select group"
                    textCapitalize={false}
                    onChange={selectNewGroup}
                    disabled={isGroupChanging}
                />
            </td>
            <td className="b-table__cell b-admin-object-types-table__cell _actions-cell _border-left">
                <ActionList>
                    <EditButton onClick={openEditModal} />
                    <RemoveIconButton onClick={openConfirmModal} />
                </ActionList>
                <AdminMetaModal typeId={id} hide={hideEditModal} isOpen={isEditModalOpen} entityType={entityType} refetch={onDataChange} />
                <ConfirmModal title="Are you sure?" onConfirm={deleteMetaType} hide={hideConfirmModal} isOpen={isConfirmModalOpened} />
            </td>
        </tr>
    );
};
