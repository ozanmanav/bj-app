import React, { FunctionComponent, useState } from "react";
import orderBy from "lodash.orderby";
import { Link } from "react-router-dom";
import { EditLinkButton, SortButton } from "../../ui/buttons";
import { TableCollapseButton } from "../tableCollapseButton";
import { Image } from "../../ui/images";
import { getImageURL } from "../../../utils";
import { Icon } from "../../ui/icons";
import { useSortingTable } from "../../../hooks";
import { ITEMS_TO_SHOW, SORT_STRINGS, IAdminTableOrganizationData } from "./definitions";
import "./AdminOrganizationsTable.scss";
import { Loader } from "../../ui/loader";

const AdminOrganizationsTableRow: FunctionComponent<IAdminTableOrganizationData> = ({
    id,
    name,
    address,
    phone,
    email,
    type,
    isClient,
    logo,
}) => {
    const link = `/app/admin/organizations${isClient ? "/client" : ""}/${id}`;
    return (
        <tr className="b-table__row">
            <td className="b-table__cell b-admin-organizations-table__cell _name">
                <Link to={link} className="flex align-center">
                    <Image src={getImageURL(logo && logo.url)} alt="" className="b-admin-organizations-table__img" />
                    <div className="b-admin-organizations-table__name _font-bold">{name}</div>
                </Link>
            </td>
            <td className="b-table__cell b-admin-organizations-table__cell _type">{type && type.name}</td>
            <td className="b-table__cell b-admin-organizations-table__cell _address">
                {address && (
                    <span>
                        <Icon icon="pinGrey" /> {address}
                    </span>
                )}
            </td>
            <td className="b-table__cell b-admin-organizations-table__cell _phone">{phone && <a href={`tel:${phone}`}>{phone}</a>}</td>
            <td className="b-table__cell b-admin-organizations-table__cell _email">
                <a href={`mailto:${email}`}>{email}</a>
            </td>
            <td className="b-table__cell b-admin-organizations-table__cell _actions-cell _border-left">
                <EditLinkButton to={link} />
            </td>
        </tr>
    );
};

interface IAdminOrganizationsTableProps {
    organizations: IAdminTableOrganizationData[];
    loading?: boolean;
}

export const AdminOrganizationsTable: FunctionComponent<IAdminOrganizationsTableProps> = ({ organizations, loading }) => {
    const { sortBy, sortingOrder, getSortOrderForField, changeSortBy } = useSortingTable({
        localStorageKey: "admin_companies",
    });

    const sortedOrganizations = orderBy(organizations, [sortBy], [sortingOrder]);
    const [isCollapsed, setIsCollapsed] = useState<boolean>(true);
    const shouldAllowCollapse = sortedOrganizations.length > ITEMS_TO_SHOW;
    const dataToShow = isCollapsed && shouldAllowCollapse ? sortedOrganizations.slice(0, ITEMS_TO_SHOW) : sortedOrganizations;
    const collapsedItems = sortedOrganizations.length - ITEMS_TO_SHOW;

    function toggle() {
        setIsCollapsed(!isCollapsed);
    }

    function sortByName() {
        changeSortBy(SORT_STRINGS.NAME);
    }

    function sortByType() {
        changeSortBy(SORT_STRINGS.TYPE);
    }

    function sortByAddress() {
        changeSortBy(SORT_STRINGS.ADDRESS);
    }

    function sortByPhone() {
        changeSortBy(SORT_STRINGS.PHONE);
    }

    function sortByEmail() {
        changeSortBy(SORT_STRINGS.EMAIL);
    }

    return (
        <div className="b-table__wrapper b-admin-organizations-table__wrapper">
            {loading ? (
                <Loader withText text="Organizations" />
            ) : (
                <table className="b-table b-admin-organizations-table">
                    <thead className="b-table__head">
                        <tr className="b-table__row _head">
                            <th className="b-table__cell b-admin-organizations-table__cell _name">
                                <SortButton text="ORGANIZATON NAME" onClick={sortByName} sorted={getSortOrderForField(SORT_STRINGS.NAME)} />
                            </th>
                            <th className="b-table__cell b-admin-organizations-table__cell _type">
                                <SortButton text="Type" onClick={sortByType} sorted={getSortOrderForField(SORT_STRINGS.TYPE)} />
                            </th>
                            <th className="b-table__cell b-admin-organizations-table__cell _address">
                                <SortButton text="ADDRESS" onClick={sortByAddress} sorted={getSortOrderForField(SORT_STRINGS.ADDRESS)} />
                            </th>
                            <th className="b-table__cell b-admin-organizations-table__cell _phone">
                                <SortButton text="PHONE" onClick={sortByPhone} sorted={getSortOrderForField(SORT_STRINGS.PHONE)} />
                            </th>
                            <th className="b-table__cell b-admin-organizations-table__cell _email">
                                <SortButton text="EMAIL" onClick={sortByEmail} sorted={getSortOrderForField(SORT_STRINGS.EMAIL)} />
                            </th>
                            <th className="b-table__cell b-admin-organizations-table__cell _actions-cell" />
                        </tr>
                    </thead>

                    <tbody className="b-table__body b-admin-organizations-table__body">
                        {dataToShow.map((organization: IAdminTableOrganizationData) => (
                            <AdminOrganizationsTableRow key={organization.id} {...organization} />
                        ))}
                    </tbody>
                </table>
            )}
            {shouldAllowCollapse && <TableCollapseButton isCollapsed={isCollapsed} collapsedItems={collapsedItems} onClick={toggle} />}
        </div>
    );
};
