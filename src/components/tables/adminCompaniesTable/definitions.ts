export const ITEMS_TO_SHOW = 6;

export const SORT_STRINGS = {
    NAME: "name",
    ADDRESS: "address",
    PHONE: "phone",
    EMAIL: "email",
    TYPE: "type.name",
};

export interface IAdminTableOrganizationData {
    id?: string;
    name?: string;
    address?: string;
    type?: {
        name?: string;
    };
    phone?: string;
    email?: string;
    isClient?: boolean;
    logo?: { url?: string };
}
