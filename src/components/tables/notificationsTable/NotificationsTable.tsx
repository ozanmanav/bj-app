import React, { FunctionComponent } from "react";
import "./NotificationsTable.scss";
import { CheckboxSwitch } from "../../ui/inputs";

export const NotificationsTable: FunctionComponent<{ data: any }> = ({ data }) => {
    return (
        <div className="b-table__wrapper b-notification-table__wrapper">
            <table className="b-table b-notification-table">
                <thead className="b-table__head">
                    <tr className="b-table__row _head">
                        <th className="b-table__cell b-notification-table__cell _info">Information</th>
                        <th className="b-table__cell b-notification-table__cell _description">Description</th>
                        <th className="b-table__cell b-notification-table__cell _actions" />
                    </tr>
                </thead>
                <tbody className="b-table__body">
                    {/* TODO: add sorting */}
                    {data.map(({ information, description }: any, index: number) => (
                        <tr className="b-table__row _medium" key={index}>
                            <td className="b-table__cell b-notification-table__cell _info">{information}</td>
                            <td className="b-table__cell b-notification-table__cell _description">{description}</td>
                            <td className="b-table__cell b-notification-table__cell _actions">
                                <div className="b-notification-table__actions">
                                    <CheckboxSwitch />
                                </div>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};
