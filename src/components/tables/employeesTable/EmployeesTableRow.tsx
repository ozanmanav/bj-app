import React, { FunctionComponent } from "react";
import { useModal } from "../../ui/modal";
import { useMutation } from "@apollo/react-hooks";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui/toasts";
import { UserRoleCheck } from "../../userRoleCheck";
import { ActionList } from "../../ui/actionList";
import { EditButton, RemoveIconButton } from "../../ui/buttons";
import { ConfirmModal } from "../../modals/confirmModal";
import gql from "graphql-tag";
import { getPhoneInitialValues } from "../../../utils";
import { AddUserModal } from "../../modals";
import { IEmployee } from "../../../config";
import get from "lodash.get";
import { IUser } from "../../forms/addUserForm/definitions";

const DELETE_EMPLOYEE = gql`
    mutation DeleteEmployee($organization_id: String!, $user_id: String!) {
        removeOrganizationUser(organization_id: $organization_id, user_id: $user_id)
    }
`;

interface IEmployeesTableRowProps {
    employee: IEmployee;
    refetch: () => void;
    isAdminPanel?: boolean;
    isClientOrganization?: boolean;
    organizationID: string;
    adminID?: string;
    companyType?: string;
}

export const EmployeesTableRow: FunctionComponent<IEmployeesTableRowProps> = ({
    employee,
    organizationID,
    refetch,
    isClientOrganization,
    isAdminPanel,
    companyType,
}) => {
    const { open: openUpdateUserModal, hide: hideUpdateUserModal, isOpen: isOpenOpenUpdateUserModal } = useModal();
    const { open: openDeleteUserModal, hide: hideDeleteUserModal, isOpen: isOpenOpenDeleteUserModal } = useModal();

    const [removeEmployeeMutation] = useMutation(DELETE_EMPLOYEE);

    async function onUserRemove() {
        try {
            hideDeleteUserModal();

            const { errors } = await removeEmployeeMutation({
                variables: {
                    organization_id: organizationID,
                    user_id: get(employee, "user.id"),
                },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                refetch();
                showSuccessToast("Employee is successfully deleted.");
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    const { user, job, user_role, status } = employee;

    const { phone, phone_code } = getPhoneInitialValues(user && user.phone);

    const editedUser: IUser = {
        id: (user && user.id) || "",
        email: (user && user.email) || "",
        first_name: (user && user.first_name) || "",
        last_name: (user && user.last_name) || "",
        role: (user_role && user_role.id) || "",
        job_title: (job && job.id) || "",
        language: (user && user.language) || "",
        phone,
        phone_code,
        status,
    };

    return (
        <tr className="b-table__row _medium">
            {user && (
                <>
                    <td className="b-table__cell b-employees__cell _name">
                        {user.first_name} {user.last_name}
                    </td>
                    <td className="b-table__cell b-employees__cell _jobTitle">{job && job.name}</td>
                    {isClientOrganization && <td className="b-table__cell b-employees__cell _role">{user_role && user_role.name}</td>}
                    <td className="b-table__cell b-employees__cell _email">{user.email}</td>
                    <td className="b-table__cell b-employees__cell _phone">{user.phone}</td>
                    <td className="b-table__cell b-employees__cell _language">{user.language}</td>
                    <UserRoleCheck
                        availableForRoles={[
                            "owner_administrator",
                            "manufacturer_administrator",
                            "property_manager_administrator",
                            "service_provider_administrator",
                        ]}
                    >
                        <td className="b-table__cell b-employees__cell _actions-cell _border-left">
                            <ActionList>
                                <EditButton onClick={openUpdateUserModal} />
                                <RemoveIconButton onClick={openDeleteUserModal} />
                            </ActionList>
                            <ConfirmModal
                                title="Are you sure?"
                                onConfirm={onUserRemove}
                                hide={hideDeleteUserModal}
                                isOpen={isOpenOpenDeleteUserModal}
                            />
                            <AddUserModal
                                editedUser={editedUser}
                                refetch={refetch}
                                hide={hideUpdateUserModal}
                                isOpen={isOpenOpenUpdateUserModal}
                                organizationID={organizationID}
                                isAdminPanel={isAdminPanel}
                                companyType={companyType}
                                isClientOrganization={isClientOrganization}
                            />
                        </td>
                    </UserRoleCheck>
                </>
            )}
        </tr>
    );
};
