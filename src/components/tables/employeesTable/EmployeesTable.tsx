import React, { FunctionComponent, useState } from "react";
import "./EmployeesTable.scss";
import { TableCollapseButton } from "..";
import { IEmployee } from "../../../config/types";
import { UserRoleCheck } from "../../userRoleCheck";
import { EmployeesTableRow } from "./EmployeesTableRow";
import { useSortingTable } from "../../../hooks";
import get from "lodash.get";
import orderBy from "lodash.orderby";
import { SortButton } from "../../ui/buttons";
import { TAddUserFormVariants } from "../../forms/addUserForm/definitions";

const ITEMS_TO_SHOW = 2;

const SORT_STRINGS = {
    NAME: "user.first_name",
    JOB_TITLE: "job.name",
    EMAIL: "user.email",
    PHONE: "user.phone",
    ROLE: "user_role.name",
    LANGUAGE: "user.language",
};

interface IEmployeesTableProps {
    employees: IEmployee[];
    organizationID: string;
    companyId: string;
    companyType?: string;
    refetch: () => void;
    isAdminPanel?: boolean;
    isClientOrganization?: boolean;
    variant?: TAddUserFormVariants;
    adminID?: string;
}

export const EmployeesTable: FunctionComponent<IEmployeesTableProps> = ({
    employees,
    refetch,
    isAdminPanel,
    adminID,
    organizationID,
    isClientOrganization,
    companyType,
}) => {
    const { sortBy, sortingOrder, changeSortBy, getSortOrderForField } = useSortingTable({ localStorageKey: "related_users" });

    const sortedUsers = orderBy(employees, [(user) => (get(user, sortBy) || "").toLowerCase()], [sortingOrder]);

    const [isCollapsed, setIsCollapsed] = useState<boolean>(true);
    const shouldAllowCollapse = sortedUsers.length > ITEMS_TO_SHOW;
    const usersToShow = isCollapsed && shouldAllowCollapse ? sortedUsers.slice(0, ITEMS_TO_SHOW) : sortedUsers;
    const collapsedItems = sortedUsers.length - ITEMS_TO_SHOW;

    function toggle() {
        setIsCollapsed(!isCollapsed);
    }

    function sortByName() {
        changeSortBy(SORT_STRINGS.NAME);
    }

    function sortByJobTitle() {
        changeSortBy(SORT_STRINGS.JOB_TITLE);
    }

    function sortByRole() {
        changeSortBy(SORT_STRINGS.ROLE);
    }

    function sortByEmail() {
        changeSortBy(SORT_STRINGS.EMAIL);
    }

    function sortByPhone() {
        changeSortBy(SORT_STRINGS.PHONE);
    }

    function sortByLanguage() {
        changeSortBy(SORT_STRINGS.LANGUAGE);
    }

    return (
        <div className="b-table__wrapper b-employees__wrapper">
            <table className="b-table b-employees">
                <thead className="b-table__head">
                    <tr className="b-table__row _head">
                        <th className="b-table__cell b-employees__cell _name">
                            <SortButton text="User Name" onClick={sortByName} sorted={getSortOrderForField(SORT_STRINGS.NAME)} />
                        </th>
                        <th className="b-table__cell b-employees__cell _jobTitle">
                            <SortButton text="Job Title" onClick={sortByJobTitle} sorted={getSortOrderForField(SORT_STRINGS.JOB_TITLE)} />
                        </th>
                        {isClientOrganization && (
                            <th className="b-table__cell b-employees__cell _role">
                                <SortButton text="Role" onClick={sortByRole} sorted={getSortOrderForField(SORT_STRINGS.ROLE)} />
                            </th>
                        )}

                        <th className="b-table__cell b-employees__cell _email">
                            <SortButton text="Email" onClick={sortByEmail} sorted={getSortOrderForField(SORT_STRINGS.EMAIL)} />
                        </th>
                        <th className="b-table__cell b-employees__cell _phone">
                            <SortButton text="Phone" onClick={sortByPhone} sorted={getSortOrderForField(SORT_STRINGS.PHONE)} />
                        </th>
                        <th className="b-table__cell b-employees__cell _language">
                            <SortButton text="Language" onClick={sortByLanguage} sorted={getSortOrderForField(SORT_STRINGS.LANGUAGE)} />
                        </th>

                        <UserRoleCheck
                            availableForRoles={[
                                "owner_administrator",
                                "manufacturer_administrator",
                                "property_manager_administrator",
                                "service_provider_administrator",
                            ]}
                        >
                            <th className="b-table__cell b-employees__cell _actions-cell">Actions</th>
                        </UserRoleCheck>
                    </tr>
                </thead>
                <tbody className="b-table__body">
                    {usersToShow.map((employee: IEmployee) => {
                        if (!get(employee, "user")) {
                            return null;
                        }

                        return (
                            <EmployeesTableRow
                                employee={employee}
                                refetch={refetch}
                                organizationID={organizationID}
                                key={employee.id}
                                isAdminPanel={isAdminPanel}
                                adminID={adminID}
                                companyType={companyType}
                                isClientOrganization={isClientOrganization}
                            />
                        );
                    })}
                </tbody>
            </table>

            {shouldAllowCollapse && <TableCollapseButton isCollapsed={isCollapsed} collapsedItems={collapsedItems} onClick={toggle} />}
        </div>
    );
};
