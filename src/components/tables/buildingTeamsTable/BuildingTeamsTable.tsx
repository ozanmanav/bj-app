import React, { FunctionComponent, useState } from "react";
import "./BuildingTeamsTable.scss";
import { TableCollapseButton } from "../../tables";
import { EditButton, RemoveIconButton, SortButton } from "../../ui/buttons";
import { ConfirmModal } from "../../modals/confirmModal";
import { useModal } from "../../ui/modal";
import { AddTeamModal } from "../../modals/addTeamModal";
import { ILocalAutocompleteOption } from "../../ui/inputs";
import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui/toasts";
import { getArrayWithoutTypename } from "../../../utils";
import { UserRoleCheck } from "../../userRoleCheck";
import { useSortingTable } from "../../../hooks";
import orderBy from "lodash.orderby";

const REMOVE_ORGANIZATION_TEAM = gql`
    mutation RemoveOrganizationTeam($organization_id: String!, $team_id: String!) {
        removeOrganizationTeam(organization_id: $organization_id, team_id: $team_id)
    }
`;

interface ITeamUser {
    id: string;
    first_name: string;
    last_name: string;
}

export interface ITeamObject {
    id: string;
    name: string;
    type: string;
}

export interface ITeam {
    id: string;
    name: string;
    users_data: ITeamUser[];
    objects_data: ITeamObject[];
}

interface IBuildingTeamsTableProps {
    teams: ITeam[];
    organizationID: string;
    users: ILocalAutocompleteOption[];
    refetch: () => void;
}

interface IBuildingTeamsTableRowProps {
    organizationID: string;
    users: ILocalAutocompleteOption[];
    refetch: () => void;
    team: ITeam;
}

function getAddedUsers(users: ILocalAutocompleteOption[], addedUsers: ITeamUser[]) {
    return addedUsers
        ? users.filter((user: ILocalAutocompleteOption) => addedUsers.find((userData: ITeamUser) => user.id === userData.id))
        : [];
}

const ITEMS_TO_SHOW = 2;

const SORT_STRINGS = {
    NAME: "name",
};

const BuildingTeamsTableRow: FunctionComponent<IBuildingTeamsTableRowProps> = ({ users, refetch, organizationID, team }) => {
    const [removeTeamMutation] = useMutation(REMOVE_ORGANIZATION_TEAM);

    const editedTeam = {
        id: team.id,
        name: team.name,
        users: getAddedUsers(users, team.users_data),
        buildings: getArrayWithoutTypename(team.objects_data),
    };

    const { open: openDeleteTeamModal, hide: hideDeleteTeamModal, isOpen: isOpenDeleteTeamModal } = useModal();
    const { open: openUpdateTeamModal, hide: hideUpdateTeamModal, isOpen: isOpenUpdateTeamModal } = useModal();

    async function onTeamRemove(team_id: string) {
        try {
            hideDeleteTeamModal();

            const res = await removeTeamMutation({
                variables: {
                    organization_id: organizationID,
                    team_id,
                },
            });
            const { data, errors } = res;

            if (errors) {
                handleGraphQLErrors(errors);
            } else if (data.removeOrganizationTeam) {
                refetch();
                showSuccessToast("Team is successfully deleted.");
            } else {
                showErrorToast("Team is not deleted.");
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    return (
        <tr className="b-table__row _medium">
            <td className="b-table__cell b-building-teams__cell _team">{team.name}</td>
            <td className="b-table__cell b-building-teams__cell _users">
                {team.users_data &&
                    team.users_data.map((user: ITeamUser) => (
                        <div className="b-building-teams__cell-item" key={user.id}>
                            {user.first_name} {user.last_name}
                        </div>
                    ))}
            </td>
            <td className="b-table__cell b-building-teams__cell _objects">
                {team.objects_data &&
                    team.objects_data.map((object: ITeamObject) => (
                        <div className="b-building-teams__cell-item" key={object.id}>
                            {object.name}
                        </div>
                    ))}
            </td>
            <UserRoleCheck
                availableForRoles={[
                    "owner_administrator",
                    "manufacturer_administrator",
                    "property_manager_administrator",
                    "service_provider_administrator",
                ]}
            >
                <td className="b-table__cell b-building-teams__cell _actions">
                    <div className="b-building-teams__actions">
                        <EditButton onClick={openUpdateTeamModal} />
                        <RemoveIconButton onClick={openDeleteTeamModal} />
                    </div>
                    <ConfirmModal
                        title="Are you sure?"
                        onConfirm={() => onTeamRemove(team.id)}
                        hide={hideDeleteTeamModal}
                        isOpen={isOpenDeleteTeamModal}
                    />
                    <AddTeamModal
                        editedTeam={editedTeam}
                        users={users}
                        refetch={refetch}
                        hide={hideUpdateTeamModal}
                        isOpen={isOpenUpdateTeamModal}
                        organizationID={organizationID}
                    />
                </td>
            </UserRoleCheck>
        </tr>
    );
};

export const BuildingTeamsTable: FunctionComponent<IBuildingTeamsTableProps> = ({
    teams,
    organizationID,
    users: companyUsers,
    refetch,
}) => {
    const [isCollapsed, setIsCollapsed] = useState<boolean>(true);

    const { sortBy, sortingOrder, changeSortBy, getSortOrderForField } = useSortingTable({ localStorageKey: "teams" });

    const sortedTeams = orderBy(teams, [sortBy], [sortingOrder]);

    const shouldAllowCollapse = sortedTeams.length > ITEMS_TO_SHOW;
    const teamsToShow = isCollapsed && shouldAllowCollapse ? sortedTeams.slice(0, ITEMS_TO_SHOW) : sortedTeams;
    const collapsedItems = sortedTeams.length - ITEMS_TO_SHOW;

    function toggle() {
        setIsCollapsed(!isCollapsed);
    }

    function sortByTeamName() {
        changeSortBy(SORT_STRINGS.NAME);
    }

    return (
        <div className="b-table__wrapper b-building-teams__wrapper">
            <table className="b-table b-building-teams">
                <thead className="b-table__head">
                    <tr className="b-table__row _head">
                        <th className="b-table__cell b-building-teams__cell _team">
                            <SortButton text="Team name" onClick={sortByTeamName} sorted={getSortOrderForField(SORT_STRINGS.NAME)} />
                        </th>
                        <th className="b-table__cell b-building-teams__cell _users">User name</th>
                        <th className="b-table__cell b-building-teams__cell _objects">Objects</th>
                        <th className="b-table__cell b-building-teams__cell _actions" />
                    </tr>
                </thead>
                <tbody className="b-table__body b-building-teams__body">
                    {teamsToShow.map((team: ITeam) => (
                        <BuildingTeamsTableRow
                            users={companyUsers}
                            organizationID={organizationID}
                            refetch={refetch}
                            team={team}
                            key={team.id}
                        />
                    ))}
                </tbody>
            </table>
            {shouldAllowCollapse && <TableCollapseButton isCollapsed={isCollapsed} collapsedItems={collapsedItems} onClick={toggle} />}
        </div>
    );
};
