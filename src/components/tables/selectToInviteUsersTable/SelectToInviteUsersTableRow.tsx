import React, { FunctionComponent, useState } from "react";
import { ActionList } from "../../ui/actionList";
import { IEmployee } from "../../../config";
import { CheckboxSwitch, Select, ISelectOption } from "../../ui";
import { IInvitedUser } from "../../forms/convertToClientOrganizationForm/definitions";

interface ISelectToInviteUsersTableRowProps {
    employee: IEmployee;
    userRoleOptions: ISelectOption[];
    addOrUpdateUser: (newInvitedUser?: IInvitedUser) => void;
    removeUser: (userID?: string) => void;
}

export const SelectToInviteUsersTableRow: FunctionComponent<ISelectToInviteUsersTableRowProps> = ({
    employee,
    addOrUpdateUser,
    userRoleOptions = [],
}) => {
    const [touchedCheckbox, setTouchedCheckbox] = useState(false);
    const [isInvited, setIsInvited] = useState(false);
    const { user, job, user_role } = employee;

    const [selectedRole, setRoleSelected] = useState(user_role && user_role.id);

    const onChangeSendInvite = (event: React.ChangeEvent<HTMLInputElement>) => {
        const {
            currentTarget: { checked },
        } = event;

        setIsInvited(checked);

        if (employee.user && employee.job) {
            const { user, job } = employee;

            const newInvitedUser: IInvitedUser = {
                user_id: user.id,
                role_id: selectedRole,
                job_id: job.id,
                invited: checked,
            };

            addOrUpdateUser(newInvitedUser);
        }
    };

    const onChangeUserRoleSelect = (event: React.ChangeEvent<HTMLSelectElement>) => {
        const {
            currentTarget: { value },
        } = event;

        if (user) {
            setRoleSelected(value);

            if (employee.user && employee.job) {
                const { user, job } = employee;

                const newInvitedUser: IInvitedUser = {
                    user_id: user.id,
                    role_id: value,
                    job_id: job.id,
                    invited: isInvited,
                };

                addOrUpdateUser(newInvitedUser);
            }
        }
    };

    const onTouchedCheckbox = () => {
        setTouchedCheckbox(true);
    };

    const onBlurCheckbox = () => {
        setTouchedCheckbox(false);
    };

    return (
        <tr className="b-table__row _medium" onMouseEnter={onTouchedCheckbox} onMouseLeave={onBlurCheckbox}>
            {user && (
                <>
                    <td className="b-table__cell b-employees__cell _name">
                        {user.first_name} {user.last_name}
                    </td>
                    <td className="b-table__cell b-employees__cell _email">{user.email}</td>
                    <td className="b-table__cell b-employees__cell _jobTitle">{job && job.name}</td>
                    <td className="b-table__cell b-employees__cell _role">
                        <Select
                            options={userRoleOptions}
                            placeholder="Please Select a Role"
                            value={selectedRole || ""}
                            onChange={onChangeUserRoleSelect}
                            error={touchedCheckbox && !selectedRole ? "Required" : undefined}
                            touched={true}
                        />
                    </td>
                    <td className="b-table__cell b-employees__cell _actions-cell _border-left">
                        <ActionList>
                            <CheckboxSwitch checked={isInvited} onChange={onChangeSendInvite} disabled={!selectedRole} />
                        </ActionList>
                    </td>
                </>
            )}
        </tr>
    );
};
