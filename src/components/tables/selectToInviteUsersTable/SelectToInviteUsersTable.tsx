import React, { FunctionComponent, useState } from "react";
import { TableCollapseButton } from "..";
import { IEmployee } from "../../../config/types";
import { SelectToInviteUsersTableRow } from "./SelectToInviteUsersTableRow";
import { useSortingTable } from "../../../hooks";
import get from "lodash.get";
import classNames from "classnames";
import orderBy from "lodash.orderby";
import { SortButton } from "../../ui/buttons";
import "./SelectToInviteUsersTable.scss";
import { IInvitedUser } from "../../forms/convertToClientOrganizationForm/definitions";
import { ISelectOption } from "../../ui";

const ITEMS_TO_SHOW = 3;

const SORT_STRINGS = {
    NAME: "user.first_name",
    EMAIL: "user.email",
    JOB_TITLE: "job.name",
};

interface ISelectToInviteUsersTableProps {
    employees: IEmployee[];
    userRoleOptions: ISelectOption[];
    addOrUpdateUser: (newInvitedUser?: IInvitedUser) => void;
    removeUser: (userID?: string) => void;
    disabled?: boolean;
}

export const SelectToInviteUsersTable: FunctionComponent<ISelectToInviteUsersTableProps> = ({
    employees,
    addOrUpdateUser,
    removeUser,
    userRoleOptions,
    disabled = false,
}) => {
    const { sortBy, sortingOrder, changeSortBy, getSortOrderForField } = useSortingTable({ localStorageKey: "invite_users" });

    const sortedUsers = orderBy(employees, [(user) => (get(user, sortBy) || "").toLowerCase()], [sortingOrder]);

    const [isCollapsed, setIsCollapsed] = useState<boolean>(true);
    const shouldAllowCollapse = sortedUsers.length > ITEMS_TO_SHOW;
    const usersToShow = isCollapsed && shouldAllowCollapse ? sortedUsers.slice(0, ITEMS_TO_SHOW) : sortedUsers;
    const collapsedItems = sortedUsers.length - ITEMS_TO_SHOW;

    function toggle() {
        setIsCollapsed(!isCollapsed);
    }

    function sortByName() {
        changeSortBy(SORT_STRINGS.NAME);
    }

    function sortByJobTitle() {
        changeSortBy(SORT_STRINGS.JOB_TITLE);
    }

    function sortByEmail() {
        changeSortBy(SORT_STRINGS.EMAIL);
    }

    const tableWrapperClassName = classNames(["b-table__wrapper b-select-to-invite-users__table", { [`_disabled`]: disabled }]);

    return (
        <div className={tableWrapperClassName}>
            <table className="b-table b-select-to-invite-users">
                <thead className="b-table__head">
                    <tr className="b-table__row _head">
                        <th className="b-table__cell b-select-to-invite-users__cell_name">
                            <SortButton text="User Name" onClick={sortByName} sorted={getSortOrderForField(SORT_STRINGS.NAME)} />
                        </th>
                        <th className="b-table__cell b-select-to-invite-users__cell _email">
                            <SortButton text="Email" onClick={sortByEmail} sorted={getSortOrderForField(SORT_STRINGS.EMAIL)} />
                        </th>
                        <th className="b-table__cell b-select-to-invite-users__cell _jobTitle">
                            <SortButton text="Job Title" onClick={sortByJobTitle} sorted={getSortOrderForField(SORT_STRINGS.JOB_TITLE)} />
                        </th>
                        <th className="b-table__cell b-select-to-invite-users__cell _role">
                            <SortButton text="Role" />{" "}
                        </th>

                        <th className="b-table__cell b-select-to-invite-users__cell _actions-cell">Send Invite </th>
                    </tr>
                </thead>
                <tbody className="b-table__body">
                    {usersToShow.map((employee: IEmployee) => (
                        <SelectToInviteUsersTableRow
                            employee={employee}
                            userRoleOptions={userRoleOptions}
                            key={employee.id}
                            addOrUpdateUser={addOrUpdateUser}
                            removeUser={removeUser}
                        />
                    ))}
                </tbody>
            </table>

            {shouldAllowCollapse && <TableCollapseButton isCollapsed={isCollapsed} collapsedItems={collapsedItems} onClick={toggle} />}
        </div>
    );
};
