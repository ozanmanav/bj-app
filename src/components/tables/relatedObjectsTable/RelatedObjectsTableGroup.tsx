import React, { FunctionComponent, useState } from "react";
import { Icon } from "../../ui/icons";
import { generateGroupName } from "../../../utils";
import { RelatedObjectsTableRow } from "./RelatedObjectsTableRow";
import { TRelatedObjectsTableVariants } from "./RelatedObjectsTable";
import { showErrorToast, showSuccessToast } from "../../ui/toasts";
import { useApolloClient } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { ConfirmModal } from "../../modals/confirmModal";
import { useModal } from "../../ui/modal";

interface IRelatedObjectTableRowGroupProps {
    name: string;
    items: any[];
    readOnly?: boolean;
    variant?: TRelatedObjectsTableVariants;
    refetch?: Function;
    showRoleColumn?: boolean;
}

const DELETE_OBJECT = gql`
    mutation deleteObject($id: String!) {
        deleteObject(id: $id)
    }
`;

export const RelatedObjectTableRowGroup: FunctionComponent<IRelatedObjectTableRowGroupProps> = ({
    variant,
    name,
    items,
    refetch,
    readOnly,
    showRoleColumn = true,
}) => {
    const [isExpanded, setIsExpanded] = useState<boolean>();

    const { isOpen, hide } = useModal();

    const apolloClient = useApolloClient();

    if (items.length === 1) {
        return <RelatedObjectsTableRow {...items[0]} showRoleColumn refetch={refetch} variant={variant} readOnly={readOnly} />;
    }

    function toggle() {
        setIsExpanded(!isExpanded);
    }

    const groupName = generateGroupName(name);
    const expandRowElClassName = `b-table__row b-related-buildings-table-row-group ${isExpanded ? "_expanded" : ""}`;

    async function onGroupDelete() {
        try {
            await Promise.all(
                items.map(({ id }) =>
                    apolloClient.mutate({
                        mutation: DELETE_OBJECT,
                        variables: {
                            id,
                        },
                    })
                )
            );

            showSuccessToast("Objects are deleted");
            hide();
            refetch && refetch();
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    return (
        <>
            <tr className={expandRowElClassName}>
                <td className="b-table__cell b-related-buildings-table-row-group__name" colSpan={4}>
                    <div className="flex align-center _cursor-pointer" onClick={toggle}>
                        <Icon icon={name} className="b-related-buildings-table-row-group__icon" />
                        <span className="_text-capitalize _font-bold">{groupName}</span>
                        <RelatedObjectTableRowsAmount amount={items.length} variant="white" />
                        {!isExpanded && <div className="b-related-buildings-table-row-group__collapse" />}
                    </div>
                </td>
                {!readOnly && (
                    <td className="b-table__cell b-related-buildings-table__actions-cell">
                        <div className="flex justify-between align-center">
                            <div className="b-related-buildings-table__actions flex align-center">
                                {/* <RemoveIconButton onClick={open} /> */}
                            </div>
                        </div>
                        <ConfirmModal title="Are you sure?" onConfirm={onGroupDelete} hide={hide} isOpen={isOpen} />
                    </td>
                )}
            </tr>
            {isExpanded &&
                items.map((item) => (
                    <RelatedObjectsTableRow
                        {...item}
                        key={item.id}
                        indent
                        refetch={refetch}
                        variant={variant}
                        readOnly={readOnly}
                        showRoleColumn={showRoleColumn}
                    />
                ))}
        </>
    );
};

const RelatedObjectTableRowsAmount: FunctionComponent<{
    amount: number;
    variant?: "white";
}> = ({ amount, variant }) => {
    let className = "b-related-buildings-table__amount h5 _text-grey flex align-center justify-center";

    if (variant === "white") {
        className += " _white";
    }

    return <span className={className}>{amount}</span>;
};
