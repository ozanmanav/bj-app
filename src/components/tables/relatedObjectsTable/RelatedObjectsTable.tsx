import React, { FunctionComponent, useState } from "react";
import "./RelatedBuildingsTable.scss";
import { SortButton } from "../../ui/buttons";
import { TableCollapseButton } from "../tableCollapseButton";
import { groupRelatedObjects } from "../../../utils";
import { IRemoveObjectModalContract } from "./removeObjectFromContractModal";
import orderBy from "lodash.orderby";
import { useSortingTable } from "../../../hooks";
import get from "lodash.get";
import { RelatedObjectTableRowGroup } from "./RelatedObjectsTableGroup";

const ITEMS_TO_SHOW = 4;

const SORT_STRINGS = {
    NAME: "name",
    ADDRESS: "address",
    TYPE: "type",
    ROLE: "relatedTo",
};

export type TRelatedObjectsTableVariants = "clientOrganization" | "nonClientOrganization";

export interface IRelatedObjectsTableObject {
    id: string;
    name: string;
    address: string;
    type: string;
    contracts: IRemoveObjectModalContract[];
    refetch?: Function;
    relatedTo: string[];
}

interface IRelatedObjectsTableProps {
    relatedObjects: IRelatedObjectsTableObject[];
    variant?: TRelatedObjectsTableVariants;
    readOnly?: boolean;
    refetch?: Function;
    showRoleColumn?: boolean;
}

export const RelatedObjectsTable: FunctionComponent<IRelatedObjectsTableProps> = ({
    relatedObjects,
    showRoleColumn = true,
    variant,
    refetch,
    readOnly,
}) => {
    const { sortBy, sortingOrder, changeSortBy, getSortOrderForField } = useSortingTable({
        localStorageKey: "admin_company_related_objects",
    });

    const sortedRelatedObjects = orderBy(relatedObjects, [sortBy], [sortingOrder]);

    const groups = groupRelatedObjects(
        sortedRelatedObjects.map((object) => ({
            ...object,
            typeLabel: get(object, "type.name") || "No type selected",
        }))
    );
    const groupsSorted = sortBy === "name" ? orderBy(groups, [(group) => get(group, "[0]")], [sortingOrder]) : groups;

    const [isCollapsed, setIsCollapsed] = useState<boolean>(true);
    const shouldAllowCollapse = groupsSorted.length > ITEMS_TO_SHOW;
    const dataToShow = isCollapsed && shouldAllowCollapse ? groupsSorted.slice(0, ITEMS_TO_SHOW) : groupsSorted;
    const collapsedItems = groupsSorted.length - ITEMS_TO_SHOW;

    function toggle() {
        setIsCollapsed(!isCollapsed);
    }

    function sortByName() {
        changeSortBy(SORT_STRINGS.NAME);
    }

    function sortByAddress() {
        changeSortBy(SORT_STRINGS.ADDRESS);
    }

    function sortByType() {
        changeSortBy(SORT_STRINGS.TYPE);
    }

    function sortByRole() {
        changeSortBy(SORT_STRINGS.ROLE);
    }

    return (
        <div className="b-table__wrapper">
            <table className="b-table b-related-buildings-table">
                <thead className="b-table__head">
                    <tr className="b-table__row _head">
                        <th className="b-table__cell b-related-buildings-table__cell _name _text-uppercase">
                            <SortButton text="Name" onClick={sortByName} sorted={getSortOrderForField(SORT_STRINGS.NAME)} />
                        </th>
                        <th className="b-table__cell b-related-buildings-table__cell _type _text-uppercase">
                            <SortButton text="Address" onClick={sortByAddress} sorted={getSortOrderForField(SORT_STRINGS.ADDRESS)} />
                        </th>
                        <th className="b-table__cell b-related-buildings-table__cell _type _text-uppercase">
                            <SortButton text="Object type" onClick={sortByType} sorted={getSortOrderForField(SORT_STRINGS.TYPE)} />
                        </th>
                        {showRoleColumn && (
                            <th className="b-table__cell b-related-buildings-table__cell _type _text-uppercase">
                                <SortButton text="Role" onClick={sortByRole} sorted={getSortOrderForField(SORT_STRINGS.ROLE)} />
                            </th>
                        )}

                        {!readOnly && <th className="b-table__cell b-related-buildings-table__cell _actions" />}
                    </tr>
                </thead>
                <tbody className="b-table__body b-related-buildings-table__body">
                    {dataToShow.map(([groupName, groupItems]: any, index) => (
                        <RelatedObjectTableRowGroup
                            name={groupName}
                            items={groupItems}
                            key={index}
                            variant={variant}
                            refetch={refetch}
                            readOnly={readOnly}
                            showRoleColumn={showRoleColumn}
                        />
                    ))}
                </tbody>
            </table>
            {shouldAllowCollapse && <TableCollapseButton isCollapsed={isCollapsed} collapsedItems={collapsedItems} onClick={toggle} />}
        </div>
    );
};
