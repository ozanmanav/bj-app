import React, { ChangeEvent, FunctionComponent } from "react";
import "./RemoveObjectFromContractModal.scss";
import { IModalProps, Modal, ModalFooter } from "../../../ui/modal";
import { Formik, FormikProps } from "formik";
import { Button } from "../../../ui/buttons";
import { Checkbox } from "../../../ui/inputs";

export interface IRemoveObjectModalContract {
    id: string;
    role: { name: string };
    object_ids: string[];
    organization_id: string;
}

interface IRemoveObjectFromContractModalProps extends IModalProps {
    contracts: IRemoveObjectModalContract[];
    onDeleteConfirm: (contract_ids: string[]) => void;
}

interface IRemoveObjectFromContractModalFormState {
    contracts_ids: string[];
}

const REMOVE_OBJECT_FROM_CONTRACT_DEFAULT_VALUE = {
    contracts_ids: [],
};

export const RemoveObjectFromContractModal: FunctionComponent<IRemoveObjectFromContractModalProps> = ({
    contracts,
    onDeleteConfirm,
    ...props
}) => {
    function onSelfSubmit(values: IRemoveObjectFromContractModalFormState) {
        onDeleteConfirm(values.contracts_ids);
    }

    return (
        <Modal {...props}>
            <Formik
                onSubmit={onSelfSubmit}
                initialValues={REMOVE_OBJECT_FROM_CONTRACT_DEFAULT_VALUE}
                render={(formikProps) => <RemoveObjectFromContractModalForm {...formikProps} contracts={contracts} />}
            />
        </Modal>
    );
};

interface IRemoveObjectFromContractModalFormProps extends FormikProps<IRemoveObjectFromContractModalFormState> {
    contracts: IRemoveObjectModalContract[];
}

const RemoveObjectFromContractModalForm: FunctionComponent<IRemoveObjectFromContractModalFormProps> = ({
    values,
    handleSubmit,
    setFieldValue,
    contracts,
}) => {
    function handleChange(e: ChangeEvent<HTMLInputElement>) {
        setFieldValue(
            "contracts_ids",
            e.target.checked ? [...values.contracts_ids, e.target.value] : values.contracts_ids.filter((id) => id !== e.target.value)
        );
    }

    return (
        <form onSubmit={handleSubmit}>
            <h2 className="h1 b-remove-object-from-contract-modal__title">Select roles to remove</h2>
            {contracts.map(({ id, role }) => (
                <Checkbox
                    key={id}
                    label={role && role.name}
                    value={id}
                    checked={values.contracts_ids.includes(id)}
                    onChange={handleChange}
                />
            ))}
            <ModalFooter>
                <Button text="Confirm" primary type="submit" icon="check" />
            </ModalFooter>
        </form>
    );
};
