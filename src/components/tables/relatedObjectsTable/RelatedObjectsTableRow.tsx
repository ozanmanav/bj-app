import React, { FunctionComponent } from "react";
import { useModal } from "../../ui/modal";
import { useMutation } from "@apollo/react-hooks";
import { getTypeLabel, isNillOrEmpty } from "../../../utils";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui/toasts";
import { ObjectNameSimple } from "../../objectName";
import { Icon } from "../../ui/icons";
import { RemoveIconButton } from "../../ui/buttons";
import { RemoveObjectFromContractModal } from "./removeObjectFromContractModal";
import { ConfirmModal } from "../../modals/confirmModal";
import gql from "graphql-tag";
import { IRelatedObjectsTableObject, TRelatedObjectsTableVariants } from "./RelatedObjectsTable";
import classNames from "classnames";
import { useObjectTypes } from "../../../hooks/apollo";
import get from "lodash.get";

interface IRelatedObjectsTableRowProps extends IRelatedObjectsTableObject {
    variant: TRelatedObjectsTableVariants;
    readOnly?: boolean;
    indent?: boolean;
    showRoleColumn?: boolean;
}

const UPDATE_CONTRACT = gql`
    mutation UpdateContract($id: String!, $organization_id: String!, $object_ids: [String]) {
        updateContract(id: $id, organization_id: $organization_id, object_ids: $object_ids) {
            id
        }
    }
`;

const DELETE_OBJECT = gql`
    mutation deleteObject($id: String!) {
        deleteObject(id: $id)
    }
`;

export const RelatedObjectsTableRow: FunctionComponent<IRelatedObjectsTableRowProps> = ({
    id,
    name,
    type,
    address,
    contracts,
    variant,
    refetch,
    relatedTo,
    indent,
    readOnly,
    showRoleColumn,
}) => {
    const { hide, isOpen, open } = useModal();

    const [deleteObject] = useMutation(DELETE_OBJECT, {
        variables: {
            id,
        },
    });

    const objectTypes = useObjectTypes();
    const typeLabel = getTypeLabel(get(type, "type"), objectTypes);

    // const rolesString = generateRolesString(contracts);

    const [updateContract] = useMutation(UPDATE_CONTRACT);

    async function onDeleteFromContractConfirm(contract_ids: string[]) {
        try {
            const updates = await Promise.all(
                contract_ids.map((contractID) => {
                    const currentContract = contracts.find(({ id }) => id === contractID);

                    const organizationID = get(currentContract, "organization_id") || "";
                    const objectIDs = get(currentContract, "object_ids") || [];

                    return updateContract({
                        variables: {
                            id: contractID,
                            organization_id: organizationID,
                            object_ids: objectIDs.filter((objectID) => objectID !== id),
                        },
                    });
                })
            );

            let isSuccessful = true;

            updates.forEach(({ errors }) => {
                if (errors) {
                    isSuccessful = false;
                    handleGraphQLErrors(errors);
                }
            });

            if (isSuccessful) {
                refetch && (await refetch());
                showSuccessToast("Contract updated");
                hide();
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    async function onObjectDeleteConfirm() {
        try {
            const { errors } = await deleteObject();

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                hide();
                refetch && refetch();
                showSuccessToast("Object is successfully deleted.");
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    const firstCellClassName = classNames(["b-table__cell b-related-buildings-table__cell _name", { _indent: indent }]);

    return (
        <tr className="b-table__row _medium">
            <td className={firstCellClassName}>
                <ObjectNameSimple name={name} type={get(type, "type")} id={id} />
            </td>
            <td className="b-table__cell b-related-buildings-table__cell _address">
                {address && (
                    <div className="flex align-center">
                        <Icon icon="pinGrey" className="b-related-buildings-table__pin" />
                        {address}
                    </div>
                )}
            </td>
            <td className="b-table__cell b-related-buildings-table__cell _text-capitalize _type">{typeLabel}</td>

            {showRoleColumn && variant === "clientOrganization" && (
                <td className="b-table__cell b-related-buildings-table__cell _role">
                    {relatedTo && relatedTo.filter((i) => i !== "").join(", ")}
                </td>
            )}
            {showRoleColumn && variant === "nonClientOrganization" && (
                <td className="b-table__cell b-related-buildings-table__cell _role">
                    {relatedTo && relatedTo.filter((i) => i !== "").join(", ")}
                </td>
            )}
            {!readOnly && (
                <td className="b-table__cell b-related-buildings-table__cell _actions">
                    <div className="b-related-buildings-table__actions flex align-center">
                        <RemoveIconButton onClick={open} />
                    </div>
                    {variant === "clientOrganization" && !isNillOrEmpty(contracts) ? (
                        <RemoveObjectFromContractModal
                            hide={hide}
                            isOpen={isOpen}
                            contracts={contracts}
                            onDeleteConfirm={onDeleteFromContractConfirm}
                        />
                    ) : (
                        <ConfirmModal title="Are you sure?" onConfirm={onObjectDeleteConfirm} hide={hide} isOpen={isOpen} />
                    )}
                </td>
            )}
        </tr>
    );
};
