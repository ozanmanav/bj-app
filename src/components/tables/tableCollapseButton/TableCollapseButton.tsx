import React, { ButtonHTMLAttributes, FunctionComponent } from "react";
import "./TableCollapseButton.scss";
import { Button } from "../../ui";

interface ITableCollapseButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    isCollapsed: boolean;
    collapsedItems: number;
}

export const TableCollapseButton: FunctionComponent<ITableCollapseButtonProps> = ({ isCollapsed, collapsedItems, ...props }) => {
    const buttonText = isCollapsed ? `See ${collapsedItems} more` : "Collapse";
    let className = "table-collapse-button";

    if (isCollapsed) {
        className += " _collapsed";
    }

    return <Button text={buttonText} icon="collapse" className={className} {...props} />;
};
