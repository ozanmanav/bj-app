import React, { FunctionComponent } from "react";
import { ISubsidiary } from "../../../config/types";
import { Link } from "react-router-dom";
import orderBy from "lodash.orderby";
import { useSortingTable } from "../../../hooks";
import { SortButton } from "../../ui/buttons";

interface ISubsidiariesTableProps {
    subsidiaries: ISubsidiary[];
    notFoundTextPostfix?: string;
}

const SORT_STRINGS = {
    NAME: "name",
};

export const SubsidiariesTable: FunctionComponent<ISubsidiariesTableProps> = ({
    subsidiaries,
    notFoundTextPostfix = "subsidiaries",
}) => {
    const { sortBy, sortingOrder, getSortOrderForField, changeSortBy } = useSortingTable({
        localStorageKey: "subsidiaries",
    });

    const sortedSubsidiaries = orderBy(subsidiaries, [sortBy], [sortingOrder]);

    if (!subsidiaries || subsidiaries.length === 0) {
        return <div>This organization does not have any {notFoundTextPostfix}.</div>;
    }

    function sortByName() {
        changeSortBy(SORT_STRINGS.NAME);
    }

    return (
        <div className="b-table__wrapper">
            <table className="b-table">
                <thead className="b-table__head">
                    <tr className="b-table__row _head">
                        <th className="b-table__cell">
                            <SortButton
                                text="Name"
                                onClick={sortByName}
                                sorted={getSortOrderForField(SORT_STRINGS.NAME)}
                            />
                        </th>
                    </tr>
                </thead>
                <tbody className="b-table__body">
                    {sortedSubsidiaries.map((subsidiary) => (
                        <tr className="b-table__row" key={subsidiary.id}>
                            <td className="b-table__cell">
                                <Link to={getSubsidiaryLink(subsidiary)} className="_font-bold">
                                    {subsidiary.name}
                                </Link>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

function getSubsidiaryLink(subsidiary: ISubsidiary): string {
    if (subsidiary.category === "CLIENT") {
        return `/app/admin/organizations/client/${subsidiary.id}/relationships`;
    }

    return `/app/admin/organizations/${subsidiary.id}/relationships`;
}
