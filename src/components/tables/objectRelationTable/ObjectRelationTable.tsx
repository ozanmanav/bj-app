import React, { FunctionComponent } from "react";
import "./ObjectRelationTable.scss";
import { ObjectNameSimple, RemoveIconButton, SortButton } from "../../ui";
import { IRelation } from "../../../config";
import { useRelationTypes, useSortingTable } from "../../../hooks";
import orderBy from "lodash.orderby";
import get from "lodash.get";

type TRemoveRelation = (id: string) => void;

type TObjectRelationTableVariants = "simple";

interface IObjectRelationTableProps {
    items?: IRelation[];
    title?: string;
    removeRelation?: TRemoveRelation;
    variant?: TObjectRelationTableVariants;
}

const SORT_STRINGS = {
    NAME: "name",
    TYPE: "type",
    RELATION_TYPE: "relationType",
    OWNER: "owner",
};

// TODO: update props type
export const ObjectRelationTable: FunctionComponent<IObjectRelationTableProps> = ({ items, title, removeRelation, variant }) => {
    const { sortBy, sortingOrder, getSortOrderForField, changeSortBy } = useSortingTable({
        localStorageKey: "related_object",
    });
    const relationTypes = useRelationTypes();

    if (!items || items.length === 0) {
        return null;
    }

    function sortByName() {
        changeSortBy(SORT_STRINGS.NAME);
    }

    function sortByType() {
        changeSortBy(SORT_STRINGS.TYPE);
    }

    function sortByRelationType() {
        changeSortBy(SORT_STRINGS.RELATION_TYPE);
    }

    function sortByOwner() {
        changeSortBy(SORT_STRINGS.OWNER);
    }

    const normalizedItems = items.map((item) => {
        if (!item.relationType) {
            return item;
        }

        const splittedRelationType = item.relationType.split(/_(.*)/);

        const relationType = relationTypes.find(({ id }: { id: string }) => item.relationType && id === splittedRelationType[1]);

        const isHigher = splittedRelationType[0] === "higher";
        const relationTypeName = relationType.name.split("-")[isHigher ? 0 : 1];

        return {
            ...item,
            relationType: relationTypeName,
        };
    });
    const sortedItems = orderBy(normalizedItems, [sortBy], [sortingOrder]);

    return (
        <>
            {title && <h3 className="b-table__title _margin-top">{title}</h3>}
            <div className="b-table__wrapper _margin-small">
                <table className="b-table _align-left">
                    <thead className="b-table__head">
                        <tr className="b-table__row _head">
                            <th className="b-table__cell">
                                <SortButton text="Object name" sorted={getSortOrderForField(SORT_STRINGS.NAME)} onClick={sortByName} />
                            </th>
                            <td className="b-table__cell">
                                <SortButton
                                    text="Relation type"
                                    sorted={getSortOrderForField(SORT_STRINGS.RELATION_TYPE)}
                                    onClick={sortByRelationType}
                                />
                            </td>
                            <th className="b-table__cell">
                                <SortButton text="Object type" sorted={getSortOrderForField(SORT_STRINGS.TYPE)} onClick={sortByType} />
                            </th>
                            <th className="b-table__cell">
                                <SortButton text="Object Owner" sorted={getSortOrderForField(SORT_STRINGS.OWNER)} onClick={sortByOwner} />
                            </th>
                        </tr>
                    </thead>
                    <tbody className="b-table__body">
                        {sortedItems.map((item) => (
                            <ObjectRelationTableRow {...item} removeRelation={removeRelation} key={item.id} variant={variant} />
                        ))}
                    </tbody>
                </table>
            </div>
        </>
    );
};
const ObjectRelationTableRow: FunctionComponent<IRelation & {
    removeRelation?: TRemoveRelation;
    variant?: TObjectRelationTableVariants;
}> = ({ id, type, name, removeRelation, relationType, variant }) => {
    const typeName = get(type, "name");

    function onRemove() {
        if (removeRelation) {
            removeRelation(id);
        }
    }

    return (
        <tr className="b-table__row _medium">
            <td className="b-table__cell">
                <ObjectNameSimple name={name} type={typeName} id={id} />
            </td>
            <td className="b-table__cell"> {relationType}</td>
            <td className="b-table__cell">{typeName}</td>
            <td className="b-table__cell _text-capitalize">
                <div className="flex justify-between">
                    {/*OWNER*/}
                    {variant !== "simple" && (
                        <div className="flex align-center b-object-relation-table__actions">
                            <RemoveIconButton onClick={onRemove} />
                        </div>
                    )}
                </div>
            </td>
        </tr>
    );
};
