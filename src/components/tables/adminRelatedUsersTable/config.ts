export interface IAdminUser {
    id: string;
    first_name: string;
    last_name: string;
    type: string;
    email: string;
    phone: string;
    phone_code: string;
}
