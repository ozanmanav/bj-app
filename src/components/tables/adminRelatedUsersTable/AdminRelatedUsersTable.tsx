import React, { FunctionComponent, useState } from "react";
import "./AdminRelatedUsersTable.scss";
import { TableCollapseButton } from "../../tables";
import { SortButton } from "../../ui/buttons";
import { useSortingTable } from "../../../hooks";
import orderBy from "lodash.orderby";
import { IAdminUser } from "./config";
import { AdminRelatedUsersTableTableRow } from "./AdminRelatedUsersTableRow";

const ITEMS_TO_SHOW = 2;

const SORT_STRINGS = {
    NAME: "first_name",
    ROLE: "type",
    PHONE: "phone",
};

interface IRelatedUsersTableProps {
    users: IAdminUser[];
    refetch: () => void;
}

export const AdminRelatedUsersTable: FunctionComponent<IRelatedUsersTableProps> = ({ users, refetch }) => {
    const { sortBy, sortingOrder, getSortOrderForField, changeSortBy } = useSortingTable({ localStorageKey: "admin_related_users" });

    const sortedUsers = orderBy(users, [sortBy], [sortingOrder]);

    const [isCollapsed, setIsCollapsed] = useState<boolean>(true);
    const shouldAllowCollapse = sortedUsers.length > ITEMS_TO_SHOW;
    const usersToShow = isCollapsed && shouldAllowCollapse ? sortedUsers.slice(0, ITEMS_TO_SHOW) : sortedUsers;
    const collapsedItems = sortedUsers.length - ITEMS_TO_SHOW;

    function toggle() {
        setIsCollapsed(!isCollapsed);
    }

    function sortByName() {
        changeSortBy(SORT_STRINGS.NAME);
    }

    function sortByRole() {
        changeSortBy(SORT_STRINGS.ROLE);
    }

    function sortByPhone() {
        changeSortBy(SORT_STRINGS.PHONE);
    }

    return (
        <div className="b-table__wrapper b-admin-related-users__wrapper">
            <table className="b-table b-admin-related-users">
                <thead className="b-table__head">
                    <tr className="b-table__row _head">
                        <th className="b-table__cell b-admin-related-users__cell _name">
                            <SortButton text="User Name" onClick={sortByName} sorted={getSortOrderForField(SORT_STRINGS.NAME)} />
                        </th>
                        <th className="b-table__cell b-admin-related-users__cell _role">
                            <SortButton text="Role" onClick={sortByRole} sorted={getSortOrderForField(SORT_STRINGS.ROLE)} />
                        </th>
                        <th className="b-table__cell b-admin-related-users__cell _phone">
                            <SortButton text="Phone" onClick={sortByPhone} sorted={getSortOrderForField(SORT_STRINGS.PHONE)} />
                        </th>
                        <th className="b-table__cell b-admin-related-users__cell _actions-cell" />
                    </tr>
                </thead>
                <tbody className="b-table__body">
                    {usersToShow.map((user: IAdminUser) => (
                        <AdminRelatedUsersTableTableRow user={user} refetch={refetch} key={user.id} />
                    ))}
                </tbody>
            </table>
            {shouldAllowCollapse && <TableCollapseButton isCollapsed={isCollapsed} collapsedItems={collapsedItems} onClick={toggle} />}
        </div>
    );
};
