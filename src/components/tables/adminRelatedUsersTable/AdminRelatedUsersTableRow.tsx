import React, { FunctionComponent } from "react";
import { useModal } from "../../ui/modal";
import { getPhoneInitialValues } from "../../../utils";
import { ActionList } from "../../ui/actionList";
import { EditButton, RemoveIconButton } from "../../ui/buttons";
import { AdminUserModal } from "../../modals/adminUserModal";
import { IAdminUser } from "./config";
import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../ui/toasts";
import { ConfirmModal } from "../../modals/confirmModal";
import { useRoleCheck } from "../../../hooks";
import { TUserType } from "../../../config/types";

const REMOVE_ADMIN_RIGHTS = gql`
    mutation RemoveAdminRights($id: String!) {
        manageUser(id: $id, action: remove_admin_rights) {
            id
        }
    }
`;

export interface IRelatedUsersTableRowProps {
    user: IAdminUser;
    refetch: () => void;
}

const DELETE_ROLES: TUserType[] = ["admin", "master"];

export const AdminRelatedUsersTableTableRow: FunctionComponent<IRelatedUsersTableRowProps> = ({ user, refetch }) => {
    const { open: openUpdateUserModal, hide: hideUpdateUserModal, isOpen: isOpenOpenUpdateUserModal } = useModal();
    const { open: openDeleteUserModal, hide: hideDeleteUserModal, isOpen: isOpenOpenDeleteUserModal } = useModal();

    const shouldShowDelete = useRoleCheck([], DELETE_ROLES);

    const [deleteUserMutation] = useMutation(REMOVE_ADMIN_RIGHTS, {
        variables: {
            id: user.id,
        },
    });

    async function deleteUser() {
        try {
            const { errors } = await deleteUserMutation();

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                showSuccessToast("User is deleted");
                hideDeleteUserModal();
                refetch();
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    const { first_name, last_name, type, phone } = user;

    const splittedPhone = getPhoneInitialValues(phone);

    const editedAdminUser = {
        ...user,
        phone: splittedPhone.phone,
        phone_code: splittedPhone.phone_code,
    };

    return (
        <tr className="b-table__row _medium">
            <td className="b-table__cell b-admin-related-users__cell _name">
                {first_name} {last_name}
            </td>
            <td className="b-table__cell b-admin-related-users__cell _role _text-capitalize">{type}</td>
            <td className="b-table__cell b-admin-related-users__cell _phone _text-capitalize" colSpan={shouldShowDelete ? 1 : 2}>
                {phone}
            </td>
            {shouldShowDelete && (
                <td className="b-table__cell b-admin-related-users__cell _actions-cell _border-left">
                    <ActionList>
                        <EditButton onClick={openUpdateUserModal} />
                        <RemoveIconButton onClick={openDeleteUserModal} />
                    </ActionList>
                    <ConfirmModal
                        title="Are you sure?"
                        onConfirm={deleteUser}
                        hide={hideDeleteUserModal}
                        isOpen={isOpenOpenDeleteUserModal}
                    />
                    <AdminUserModal
                        hide={hideUpdateUserModal}
                        isOpen={isOpenOpenUpdateUserModal}
                        user={editedAdminUser}
                        refetch={refetch}
                    />
                </td>
            )}
        </tr>
    );
};
