import React, { FunctionComponent } from "react";
import { ObjectName } from "../ui";
import "./Dashboard.scss";

const data = [
    {
        type: "street",
        name: "Laurenzenvorstadt",
        createdAt: "30.12.2018",
        serialNumber: 19741702,
    },
    {
        type: "building",
        name: "Laurenzenvorstadt",
        createdAt: "30.12.2018",
        serialNumber: 19741700,
    },
    {
        type: "street",
        name: "Laurenzenvorstadt",
        createdAt: "30.12.2018",
        serialNumber: 19741703,
    },
    {
        type: "street",
        name: "Laurenzenvorstadt",
        createdAt: "30.12.2018",
        serialNumber: 19741704,
    },
    {
        type: "street",
        name: "Laurenzenvorstadt",
        createdAt: "30.12.2018",
        serialNumber: 19741705,
    },
];

export const Dashboard: FunctionComponent = () => {
    return (
        <div className="b-dashboard _not-last-group">
            <h3 className="b-dashboard__title">Dashboard</h3>
            <div className="b-table__wrapper">
                <table className="b-table _first-cell-border">
                    <thead className="b-table__head">
                        <tr className="b-table__row _head">
                            <th className="b-table__cell b-dashboard__first-cell">
                                <button className="b-dashboard__sort _font-bold _text-grey h6">No</button>
                            </th>
                            <th className="b-table__cell">Object name</th>
                            <th className="b-table__cell">Object creation date</th>
                            <th className="b-table__cell">Serial number</th>
                            <th className="b-table__cell">Events</th>
                        </tr>
                    </thead>
                    <tbody className="b-table__body">
                        {/* TODO: add sorting */}
                        {data.map(({ type, name, createdAt, serialNumber }) => (
                            <tr className="b-table__row" key={serialNumber}>
                                <td className="b-table__cell b-dashboard__first-cell">1</td>
                                <td className="b-table__cell">
                                    <ObjectName type={type} name={name} />
                                </td>
                                <td className="b-table__cell _text-black">{createdAt}</td>
                                <td className="b-table__cell _text-black">{serialNumber}</td>
                                <td className="b-table__cell" />
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
};
