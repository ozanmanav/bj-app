import React, { FunctionComponent } from "react";
import "./SettingsItem.scss";

export const SettingsItem: FunctionComponent = ({ children }) => {
    return <div className="b-settings__item">{children}</div>;
};
