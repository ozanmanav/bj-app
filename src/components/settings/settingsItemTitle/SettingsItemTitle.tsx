import React, { FunctionComponent } from "react";
import "./SettingsItemTitle.scss";

export const SettingsItemTitle: FunctionComponent = ({ children }) => {
    return <div className="b-settings__item-title flex align-center justify-between">{children}</div>;
};
