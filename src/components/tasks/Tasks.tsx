import React, { FunctionComponent, HTMLAttributes, useState, useContext, useEffect } from "react";
import "./Tasks.scss";
import { Icon, PlusButton, BackButton, Button, Select } from "../ui";
import { TaskCard } from "./task";
import { useRemoveBodyScroll } from "../../hooks";
import { useQuery } from "@apollo/react-hooks";
import { GET_TASK_USERS, GET_EVENTS } from "./queries";
import get from "lodash.get";
import { ITaskUser } from "../../views/app/task/taskDetail/definitions";
import { AccountStateContext } from "../../contexts/accountStateContext";
import { Loader } from "../ui/loader";
import { IEvent } from "../../views/app/event/eventDetail/definitions";
import { EventCard } from "../events/EventCard";
import { normalizedEventsForMonthlyType } from "../events/EventsTimeline/Timeline/utils";
import { addMonths } from "date-fns";
import { sort } from "ramda";

const TasksOpenButton: FunctionComponent<HTMLAttributes<HTMLButtonElement> & { loading: boolean }> = (props) => {
    const { loading = false, ...restProps } = props;

    return (
        <button className="b-tasks__open-button" {...restProps}>
            {loading ? <Loader rawLoader width={15} isWhite={true} /> : <Icon icon="bell" className="b-tasks__open-button-icon" />}
        </button>
    );
};

interface TaskListProps {
    hide: () => void;
    isOpened: boolean;
    taskTypeFilter: string;
}

interface EventListProps {
    hide: () => void;
    isOpened: boolean;
}

const TaskList: FunctionComponent<TaskListProps> = ({ hide, isOpened, taskTypeFilter }) => {
    const { user } = useContext(AccountStateContext);
    const userID = get(user, "id");
    const { data: taskUsersResponse, loading: tasksLoading, refetch: taskUsersRefetch, fetchMore } = useQuery(GET_TASK_USERS, {
        skip: !userID || taskTypeFilter === "events",
        variables: {
            user_id: userID,
            status: taskTypeFilter,
            limit: 5,
            page: 1,
        },
    });

    const hasMorePagesTaskUsers = (get(taskUsersResponse, "taskUsers.has_more_pages") as boolean) || false;
    const currentPageTaskUsers = (get(taskUsersResponse, "taskUsers.current_page") as number) || 1;
    const totalData = get(taskUsersResponse, "taskUsers.total") as number;
    const taskUsers = (get(taskUsersResponse, "taskUsers.data") as ITaskUser[]) || [];
    const remainingLoadMore = totalData - taskUsers.length;

    useEffect(() => {
        taskUsersRefetch();
    }, [taskTypeFilter]);

    const refetchWithParams = () =>
        taskUsersRefetch({
            user_id: userID,
            status: taskTypeFilter,
            limit: taskUsers.length,
            page: 1,
        });

    const loadMoreTaskUsers = () => {
        fetchMore({
            variables: {
                user_id: userID,
                limit: 5,
                status: taskTypeFilter,
                page: currentPageTaskUsers + 1,
            },
            updateQuery: (prev, next) => {
                const { fetchMoreResult } = next;

                if (!fetchMoreResult) return prev;

                return {
                    ...fetchMoreResult,
                    taskUsers: {
                        ...fetchMoreResult.taskUsers,
                        data: [...get(prev, "taskUsers.data"), ...get(fetchMoreResult, "taskUsers.data")],
                    },
                };
            },
        });
    };

    return (
        <>
            {tasksLoading ? (
                <Loader />
            ) : (
                taskUsers.map((taskUser) => (
                    <TaskCard
                        key={get(taskUser, "id")}
                        hide={hide}
                        refetch={taskUsersRefetch}
                        refetchWithParams={refetchWithParams}
                        taskUser={taskUser}
                        isTrayOpened={isOpened}
                    />
                ))
            )}

            {hasMorePagesTaskUsers && remainingLoadMore > 0 && (
                <Button
                    onClick={loadMoreTaskUsers}
                    text={`Load More (${remainingLoadMore})`}
                    icon="check"
                    primary
                    className="b-tasks__load-more-button "
                />
            )}
        </>
    );
};

const EventList: FunctionComponent<EventListProps> = ({ hide, isOpened }) => {
    const { currentOrganization } = useContext(AccountStateContext);
    const organizationID = get(currentOrganization, "id");
    const { data, loading, fetchMore } = useQuery(GET_EVENTS, {
        variables: {
            organization_id: organizationID,
            limit: 5,
            page: 1,
        },
        skip: !organizationID,
    });

    const hasMorePagesEvents = (get(data, "events.has_more_pages") as boolean) || false;
    const currentPageEvents = (get(data, "events.current_page") as number) || 1;
    const totalData = get(data, "events.total") as number;
    const events = (get(data, "events.data") as IEvent[]) || [];
    const remainingLoadMore = totalData - events.length;

    const normalizedEvents = normalizedEventsForMonthlyType(events, { startDate: new Date(), endDate: addMonths(new Date(), 1) });
    const sortedEvents = sort(({ start_date: a }, { start_date: b }) => new Date(a).getTime() - new Date(b).getTime(), normalizedEvents);

    const loadMoreEvents = () => {
        fetchMore({
            variables: {
                organization_id: organizationID,
                limit: 5,
                page: currentPageEvents + 1,
            },
            updateQuery: (prev, next) => {
                const { fetchMoreResult } = next;

                if (!fetchMoreResult) return prev;

                return {
                    ...fetchMoreResult,
                    taskUsers: {
                        ...fetchMoreResult.taskUsers,
                        data: [...get(prev, "events.data"), ...get(fetchMoreResult, "events.data")],
                    },
                };
            },
        });
    };

    // const refetchWithParams = () =>
    //     refetch({
    //         organization_id: organizationID,
    //         limit: events.length,
    //         page: 1,
    //     });

    return (
        <>
            {loading ? <Loader /> : sortedEvents.map((event, index) => <EventCard hide={hide} {...event} key={index} />)}

            {hasMorePagesEvents && remainingLoadMore > 0 && (
                <Button
                    onClick={loadMoreEvents}
                    text={`Load More (${remainingLoadMore})`}
                    icon="check"
                    primary
                    className="b-tasks__load-more-button "
                />
            )}
        </>
    );
};

export const TASK_TYPES = [
    { value: "assigned", label: "Upcoming Tasks" },
    { value: "completed", label: "Completed Tasks" },
    { value: "events", label: "Upcoming Events" },
];

export const Tasks: FunctionComponent = () => {
    const [isOpened, setIsOpened] = useState<boolean>(false);
    const [taskTypeFilter, setTaskTypeFilter] = useState<string>("assigned");
    const { currentOrganization } = useContext(AccountStateContext);
    const organizationID = get(currentOrganization, "id");
    useRemoveBodyScroll(isOpened);

    const hide = () => {
        setIsOpened(false);
    };

    const open = () => {
        setIsOpened(true);
    };

    const onChangeTaskFilter = (event: React.ChangeEvent<HTMLSelectElement>) => {
        const value = get(event, "currentTarget.value");

        setTaskTypeFilter(value);
    };

    let wrapperClassName = "b-tasks flex flex-column";
    let backdropClassName = "b-tasks__backdrop";

    if (isOpened) {
        wrapperClassName += " _opened";
        backdropClassName += " _opened";
    }

    return (
        <>
            <div className={wrapperClassName}>
                <TasksOpenButton onClick={open} loading={false} />

                <header className="b-tasks__header flex align-center">
                    <h3>{taskTypeFilter === "events" ? "Events" : "Tasks"}</h3>

                    <PlusButton
                        to={`${taskTypeFilter === "events" ? `/app/event/add?org=${organizationID}` : "/app/task/add"}`}
                        onClick={hide}
                    />

                    <label className="b-tasks__header-switch flex align-center _cursor-pointer">
                        <Select
                            options={TASK_TYPES}
                            marginBottom="none"
                            name="startMonth"
                            onChange={onChangeTaskFilter}
                            value={taskTypeFilter}
                        />
                    </label>
                    <BackButton text="Close" className="b-tasks__header-back" onClick={hide} />
                </header>
                <div className="b-tasks__list">
                    {taskTypeFilter === "events" ? (
                        <EventList isOpened={isOpened} hide={hide} />
                    ) : (
                        <TaskList isOpened={isOpened} hide={hide} taskTypeFilter={taskTypeFilter} />
                    )}
                </div>
            </div>
            <div className={backdropClassName} onClick={hide} />
        </>
    );
};
