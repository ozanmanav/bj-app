import React, { FunctionComponent, HTMLAttributes, useState, useEffect } from "react";
import "./Task.scss";
import {
    Icon,
    DatepickerButton,
    handleGraphQLErrors,
    showSuccessToast,
    showErrorToast,
    useModal,
    Modal,
    ModalFooter,
    Button,
    DatepickerSimple,
} from "../../ui";
import { ITaskUserStatus, ITaskUser } from "../../../views/app/task/taskDetail/definitions";
import { parseAndFormatDate } from "../../../views/app/event/eventDetail/eventHeader/utils";
import get from "lodash.get";
import { Link } from "react-router-dom";
import { isAfter, format } from "date-fns";
import { parseDate, formatDate } from "../../ui/datepicker/utils";
import { DateUtils } from "react-day-picker";
import { useMutation } from "@apollo/react-hooks";
import { UPDATE_TASK_USER, UPDATE_TASK } from "./queries";
import classnames from "classnames";
import { DATE_FORMAT } from "../../../config";
import { FormCaption } from "../../forms";

const TaskCheckbox: FunctionComponent<HTMLAttributes<HTMLInputElement> & { checked: boolean }> = (props) => {
    return <input type="checkbox" checked={props.checked} className="b-task__checkbox" {...props} />;
};

const TaskEnd: FunctionComponent<{ dateString: string }> = ({ dateString }) => {
    const parsedDate = parseDate(dateString, DATE_FORMAT);

    const isDeadline = DateUtils.isDate(parsedDate) && isAfter(new Date().setHours(0, 0, 0), parsedDate.setHours(0, 0, 0));

    let className = "b-task__ends";

    if (isDeadline) {
        className += ` _deadline`;
    }

    return (
        <p className={className}>
            {isDeadline && <Icon icon="bellEmpty" className="b-task__ends-icon" />}
            <span className="_font-bold">{isDeadline ? "Deadline: " : "Due: "}</span>
            {dateString}
        </p>
    );
};

interface ITaskCard {
    taskUser?: ITaskUser;
    hide: () => void;
    refetch: () => void;
    refetchWithParams: () => void;
    isTrayOpened: boolean;
}

export const TaskCard: FunctionComponent<ITaskCard> = ({ taskUser, hide, refetch, refetchWithParams, isTrayOpened }) => {
    const [updateTaskUserMutation] = useMutation(UPDATE_TASK_USER);
    const [updateTaskMutation] = useMutation(UPDATE_TASK);
    const [isExpanded, setIsExpanded] = useState<boolean>(false);
    const [dateCompleted, setDateCompleted] = useState<Date>(new Date());
    const taskStatus = get(taskUser, "status") === "completed" || false;
    const [taskIsCompleted, setTaskIsCompleted] = useState<boolean>(taskStatus);
    const [isCardMoveAway, setIsCardMoveAway] = useState<boolean>(false);
    const { open, isOpen, hide: hideModal } = useModal();

    useEffect(() => {
        setIsCardMoveAway(false);
    }, [taskStatus]);

    if (!taskUser || !taskUser.task) return null;

    const {
        id: taskUserID,
        task: { id: taskID, name, start_date, description, meta, ends_on, deadline },
    } = taskUser;

    const createdBy = get(meta, "author.name");

    const expand = () => {
        setIsExpanded(true);
    };

    const onChangeDate = (date: Date) => {
        setDateCompleted(date);
    };

    const toggleTaskStatus = () => {
        setTaskIsCompleted((prevState) => !prevState);
    };

    const updateTaskUserStatus = async () => {
        if (isTrayOpened) {
            try {
                toggleTaskStatus();

                const { errors } = await updateTaskUserMutation({
                    variables: {
                        id: taskUserID,
                        status: (taskIsCompleted ? "assigned" : "completed") as ITaskUserStatus,
                        ...(!taskIsCompleted && { completed_at: format(dateCompleted, "yyyy-MM-dd") }),
                    },
                });

                if (errors) {
                    toggleTaskStatus();
                    handleGraphQLErrors(errors);
                } else {
                    setIsCardMoveAway(true);
                    showSuccessToast("Task status successfully updated");
                    refetchWithParams();
                }
            } catch (e) {
                toggleTaskStatus();
                showErrorToast(e.message);
            }
        }
    };

    const updateTaskEndDate = async (newEndDate: Date) => {
        if (isTrayOpened) {
            try {
                const { errors } = await updateTaskMutation({
                    variables: { id: taskID, end_date: formatDate(newEndDate, DATE_FORMAT) },
                });

                if (errors) {
                    toggleTaskStatus();
                    handleGraphQLErrors(errors);
                } else {
                    showSuccessToast("End date successfully updated");
                    refetch();
                }
            } catch (e) {
                toggleTaskStatus();
                showErrorToast(e.message);
            }
        }
    };

    return (
        <div className={classnames("b-task", { _hidden: isCardMoveAway })}>
            <TaskCheckbox checked={taskIsCompleted} onChange={taskIsCompleted ? updateTaskUserStatus : open} />
            <div className="b-task__info-wrapper">
                <p className="h6 _text-grey _font-regular">{parseAndFormatDate(start_date)}</p>
                <h2 className="_font-regular">
                    <Link to={`/app/task/${taskID}`} onClick={hide}>
                        {name}
                    </Link>
                </h2>
                <div className="flex align-center b-task__info">
                    <TaskEnd dateString={parseAndFormatDate(deadline)} />

                    {ends_on === "date" && <DatepickerButton onChange={updateTaskEndDate} />}

                    <p className="b-task__assigned h6">
                        <span className="_text-grey">Assigned by</span> <span className="_font-bold">{createdBy}</span>
                    </p>
                </div>
                <div className="b-task__content">
                    {!isExpanded && description && description.length > 150 ? (
                        <>
                            <p>{`${description.substring(0, 150)}...`}</p>
                            <button className="h5 _text-primary _font-bold b-task__more" onClick={expand}>
                                Read more
                            </button>
                        </>
                    ) : (
                        <p>{description}</p>
                    )}
                </div>
            </div>
            <Modal isOpen={isOpen} hide={hideModal} overflow="visible">
                <h2 className="h3 b-confirm-modal__title">Task Completion Date</h2>
                <FormCaption>Date</FormCaption>
                <DatepickerSimple onChange={onChangeDate} defaultValue={dateCompleted} />
                <ModalFooter>
                    <Button text="Submit" primary icon="check" onClick={updateTaskUserStatus} className="b-confirm-modal__actions-button" />
                </ModalFooter>
            </Modal>
        </div>
    );
};
