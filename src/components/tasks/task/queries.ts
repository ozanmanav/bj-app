import gql from "graphql-tag";

export const UPDATE_TASK_USER = gql`
    mutation UpdateTaskUser($id: String!, $task_id: String, $user_id: String, $status: UpdateTaskStatusEnum!, $completed_at: String) {
        updateTaskUser(id: $id, task_id: $task_id, user_id: $user_id, status: $status, completed_at: $completed_at) {
            id
        }
    }
`;

export const UPDATE_TASK = gql`
    mutation UpdateTask($id: String!, $end_date: String) {
        updateTask(id: $id, end_date: $end_date) {
            id
        }
    }
`;
