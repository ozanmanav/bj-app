import gql from "graphql-tag";

export const GET_TASK_USERS = gql`
    query GetTaskUsers($user_id: String, $page: Int, $limit: Int, $status: TasksUserStatusEnum) {
        taskUsers(user_id: $user_id, page: $page, limit: $limit, order_by: updated_at, order_direction: desc, status: $status) {
            total
            current_page
            has_more_pages
            data {
                id
                status
                created_at
                task {
                    id
                    name
                    description
                    meta {
                        author_id
                        author {
                            name
                        }
                    }
                    deadline
                    frequency {
                        type
                        days
                        months
                    }
                    related_objects {
                        id
                        name
                    }
                    ends_on
                    start_date
                    end_date
                }
            }
        }
    }
`;

export const GET_EVENTS = gql`
    query GetEvents($organization_id: String) {
        events(organization_id: $organization_id, strict: false) {
            total
            current_page
            has_more_pages
            data {
                id
                name
                description
                type {
                    id
                    name
                }
                frequency {
                    type
                    days
                    months
                    interval
                    count
                }
                related_objects {
                    id
                    name
                }
                related_organizations {
                    id
                    name
                }
                organization {
                    id
                    name
                }
                object {
                    id
                    name
                }
                ends_on
                start_date
                end_date
            }
        }
    }
`;
