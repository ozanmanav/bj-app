import React, { FunctionComponent } from "react";
import "./OrganizationName.scss";
import { Link } from "react-router-dom";
import { useOrganizationLinkRoute } from "../../hooks/useOrganizationLinkRoute";

interface IOrganizationNameProps {
    name: string;
    isClientOrganization: boolean;
    id?: string;
}

export const OrganizationName: FunctionComponent<IOrganizationNameProps> = ({ name, isClientOrganization, id }) => {
    const organizationLinkRoute = useOrganizationLinkRoute(isClientOrganization, id);

    const organizationName = isClientOrganization ? `$ ${name}` : name;

    return (
        <Link className="flex" to={organizationLinkRoute}>
            <div className="flex flex-column justify-center">
                <p className="b-organization-name__name _font-bold" title={name}>
                    {organizationName}
                </p>
            </div>
        </Link>
    );
};
