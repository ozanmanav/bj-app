import React, { FunctionComponent } from "react";
import "./ChangesHistory.scss";
import { Filter } from "../ui";
import { HistoryRows } from "./historyRows";

const data = {
    today: [
        {
            type: "building",
            user: "Markus Rashford",
        },
        {
            type: "elevator",
            object: "Generalagentur",
            user: "Markus Rashford",
        },
    ],
    yesterday: [
        {
            type: "contract",
            fileName: "floor_plan_2.pdf",
            contract: ["Schindler Group", "ABB Group"],
            user: "Anthony Pettis",
        },
        {
            type: "building",
            user: "Markus Rashford",
        },
    ],
    march: [
        {
            type: "elevator",
            object: "Generalagentur",
            user: "Markus Rashford",
        },
        {
            type: "contract",
            fileName: "floor_plan_2.pdf",
            contract: ["Schindler Group", "ABB Group"],
            user: "Anthony Pettis",
        },
    ],
};

export const ChangesHistory: FunctionComponent = () => {
    return (
        <div className="b-changes-history _not-last-group">
            <header className="b-changes-history__header flex justify-between b-group-header">
                <h3 className="flex align-center b-group-header__title">History of Changes</h3>
                <Filter onApply={() => {}} typeDropdown dateStart={new Date().toLocaleDateString()} />
            </header>
            <div className="b-table__wrapper">
                <table className="b-table">
                    <thead className="b-table__head">
                        <tr className="b-table__row _head">
                            <th className="b-table__cell">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <HistoryRows title="Today" data={data.today} />
                        <HistoryRows title="Yesterday" data={data.yesterday} />
                        <HistoryRows title="01 March 2019" data={data.march} />
                    </tbody>
                </table>
            </div>
        </div>
    );
};
