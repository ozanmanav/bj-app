import React, { FunctionComponent } from "react";
import { Icon } from "../../ui";

interface IHistoryRowProps {
    type: string;
    user?: string;
    object?: string;
    fileName?: string;
    contract?: string[];
}

interface IHistoryRowsProps {
    title: string;
    data: IHistoryRowProps[];
}

const HistoryText: FunctionComponent<IHistoryRowProps> = (props) => {
    const { type, user, object, fileName, contract } = props;

    switch (type) {
        case "building":
            return (
                <p>
                    New building has been added by <span className="_font-bold">{user}</span>
                </p>
            );
        case "elevator":
            return (
                <p>
                    New elevator has been added to <span className="_font-bold">{object}</span> by{" "}
                    <span className="_font-bold">{user}</span>
                </p>
            );
        case "contract":
            return (
                <p>
                    <span className="_font-bold">{fileName}</span> was added to the contract between{" "}
                    <span className="_font-bold">{contract && contract.join(" and ")}</span> by <span className="_font-bold">{user}</span>
                </p>
            );
        default:
            console.log(`Type ${props.type} does not exist`);
            return <p />;
    }
};

const HistoryRow: FunctionComponent<IHistoryRowProps> = ({ type, ...props }) => {
    return (
        <tr className="b-table__row">
            <td className="b-table__cell">
                <div className="flex align-center _text-black">
                    <Icon icon={type} className="b-changes-history__icon" />
                    <HistoryText type={type} {...props} />
                </div>
            </td>
        </tr>
    );
};

export const HistoryRows: FunctionComponent<IHistoryRowsProps> = ({ title, data }) => {
    return (
        <>
            <tr className="b-table__row _small">
                <td className="b-table__cell">{title}</td>
            </tr>
            {data.map((props) => (
                <HistoryRow {...props} key={props.type} />
            ))}
        </>
    );
};
