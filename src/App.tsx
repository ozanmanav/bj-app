import React, { FunctionComponent, useEffect } from "react";
import { BrowserRouter, withRouter } from "react-router-dom";
import { Content } from "./views/Content";
import { ApolloClient } from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { createUploadLink } from "apollo-upload-client";
import { ApolloProvider } from "@apollo/react-hooks";
import { ToastContainer } from "react-toastify";
import { onError } from "apollo-link-error";
import { IGraphQLError } from "./config/types";
import { showErrorToast } from "./components/ui/toasts";
import { restoreScroll } from "./hooks";
import { ApolloLink } from "apollo-link";
import apolloLogger from "apollo-link-logger";
import { checkLoginMarker, removeLoginMarker } from "./utils";

const uploadLink = createUploadLink({
    uri: process.env.REACT_APP_GRAPHQL_ENDPOINT,
    credentials: "include",
});

const AppInner = withRouter(({ history, location }) => {
    const logoutLink = onError(({ graphQLErrors = [] }) => {
        if (!checkLoginMarker()) {
            return;
        }

        const unauthorizedError = graphQLErrors.find(
            (error: IGraphQLError) => error.message === "Sorry, you have been logged out due to inactivity"
        );

        if (unauthorizedError) {
            removeLoginMarker();
            history.push("/login");
            showErrorToast(unauthorizedError.message);
        }
    });

    const client = new ApolloClient({
        cache: new InMemoryCache({ addTypename: false }),
        link: ApolloLink.from([apolloLogger, logoutLink.concat(uploadLink)]),
    });

    // restoring scroll on location change
    useEffect(() => {
        restoreScroll(true);
    }, [location.pathname]);

    return (
        <ApolloProvider client={client}>
            <Content />
            <ToastContainer />
        </ApolloProvider>
    );
});

export const App: FunctionComponent = () => {
    return (
        <BrowserRouter>
            <AppInner />
        </BrowserRouter>
    );
};
