export const defaultValueTypes = [
    {
        value: "number",
        label: "Number",
    },
    {
        value: "date",
        label: "Date",
    },
    {
        value: "string",
        label: "Text",
    },
    {
        value: "url",
        label: "URL",
    },
];
