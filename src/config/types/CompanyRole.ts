export interface ICompanyRole {
    id: string;
    name: string;
}
