export interface IGroup {
    id: string;
    name: string;
    entity: string;
}
