import { IStatusUser } from "../../components/forms/addUserForm/definitions";

export interface IEmployee {
    id: string;
    organization_id?: string;
    status?: IStatusUser;
    user?: {
        id: string;
        first_name?: string;
        last_name?: string;
        email?: string;
        phone?: string;
        mobile_phone?: string;
        language?: string;
    };
    user_role?: {
        id: string;
        type?: string;
        name?: string;
        company_type?: string;
    };
    job?: {
        id?: string;
        type?: string;
        name?: string;
    };
    mobile_phone?: string;
    language?: string;
    note?: string;
}
