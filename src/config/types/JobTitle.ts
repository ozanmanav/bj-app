import { IGroup } from "./Group";

export interface IJobTitle {
    id: string;
    name: string;
    groups?: IGroup[];
}
