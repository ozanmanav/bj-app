export interface ISettingsUser {
    id: string;
    first_name: string;
    last_name: string;
    email: string;
    is_admin: string;
    phone: string;
}

export interface ISettingsUserRole {
    id: string;
    name: string;
    type: string;
}

export interface ISettingsRelatedUser {
    job_title: string;
    user: ISettingsUser;
    user_role: ISettingsUserRole;
}
