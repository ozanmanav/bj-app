export type TSubsidiaryTypes = "CLIENT" | "NON_CLIENT";

export interface ISubsidiary {
    relationId: string;
    id: string;
    name: string;
    category?: TSubsidiaryTypes;
}
