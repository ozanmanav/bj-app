import { IInfoRowParams } from "../../components/forms/dynamicInfoRows";

export type TFile = {
    id: string;
    type: { id: string; entity?: string };
    name: string;
    url: string;
    filename: string;
    extension: string;
    size: string;
    updated_at: string;
    created_at: string;
    details: IInfoRowParams[];
};

export type TFileDetails = {
    [index: string]: any;
};
