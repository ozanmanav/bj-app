export interface IFormWithFormik<T> {
    initialValues: T;
    onSubmit: (state: T) => void;
}
