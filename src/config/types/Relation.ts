export interface IRelation {
    id: string;
    type: object;
    name: string;
    address_refs?: string[];
    relationType?: string;
}

export type TRelationRoles = "lower" | "higher";
