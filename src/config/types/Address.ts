export interface IAddress {
    street: string;
    number: string;
    city: string;
    country: string;
    plz: string;
    line_1: string;
    line_2: string;
}
