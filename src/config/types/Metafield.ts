export interface IMetafield {
    id?: string;
    type: string;
    highlighted: boolean;
    name: string;
    value: string;
    context?: string;
    created_at?: string;
    updated_at?: string;
    isCustomField?: boolean;

    meta: {
        author?: {
            id?: string;
            first_name?: string;
            last_name?: string;
        };
        source?: string;
        source_url?: string;
        source_date?: string;
        verification: boolean;
        verification_by?: string;
    };
}

export interface IMetatype {
    id?: string;
    name: string;
    type: string;
    context?: string;
}

export interface IObjectMetafield extends IMetafield {
    id: string;
    object_id: string;
}
