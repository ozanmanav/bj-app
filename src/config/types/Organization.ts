import { IEmployee } from "./Employee";
import { IMetafield } from "./Metafield";
import { IContract } from "./Contract";

export interface ITypeOfOrganization {
    id: string;
    name: string;
    type: string;
}

export interface IOrganization {
    id: string;
    name: string;
    category: "CLIENT" | "NON_CLIENT";
    address?: {
        street?: string;
        number?: string;
        country?: string;
        plz?: string;
        city?: string;
        line_1?: string;
        line_2?: string;
    };
    phone?: string;
    email?: string;
    logo?: {
        url?: string;
        name?: string;
    };
    type?: ITypeOfOrganization;
    employees?: IEmployee[];
    details?: IMetafield[];
    created_at?: string;
    contracts?: IContract[];
}

export interface IRelationship {
    organization_id: string;
    organization_name?: string;
    role: string;
    objects: any[];
    files: any[];
    contacts: IEmployee[];
}
