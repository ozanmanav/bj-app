// combination of "company_type" + "type" divided by "_" for each role in "companies_roles_list" query
export type TRole =
    | "owner_administrator"
    | "owner_portfolio_manager"
    | "manufacturer_administrator"
    | "manufacturer_brand_manager"
    | "manufacturer_object_manager"
    | "property_manager_administrator"
    | "property_manager_building_manager"
    | "service_provider_administrator"
    | "service_provider_service_manager";

export type TUserType = "user" | "admin" | "master" | "datamanager";
