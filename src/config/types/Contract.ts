import { IMetafield } from "./Metafield";
import { IOrganization } from "./Organization";
import { TFile } from "./Files";
import { IObjectMinimal } from "./Object";

export interface IContract {
    id: string;
    role_id: string;
    role: {
        name: string;
    };
    organization?: IOrganization;
    employees: {
        id: string;
        first_name: string;
        last_name: string;
        email: string;
        phone: string;
        job_title: string;
    }[];
    files?: TFile[];
    objects: IObjectMinimal[];
    details: IMetafield[];
}
