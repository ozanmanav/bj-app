import { IObjectMetafield } from "./Metafield";
import { IGroup } from "./Group";

export interface IObjectMinimal {
    id: string;
    type: string;
    name: string;
    higher?: {
        name: string;
    };
}

export interface IObject extends IObjectMinimal {
    details: IObjectMetafield[];
    created_at: string;
    updated_at: string;
}

export interface IObjectTypes {
    id: string;
    name: string;
    type: string;
    groups?: IGroup[];
}

export interface IRoleTypes {
    id: string;
    name: string;
    type: string;
    groups?: IGroup[];
}
