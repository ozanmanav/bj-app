import { IGroup } from "./Group";

export interface IEventType {
    id: string;
    name: string;
    groups?: IGroup[];
}
