import { IGroup } from "./Group";

export interface IRelationshipRole {
    id: string;
    name: string;
    groups?: IGroup[];
}
