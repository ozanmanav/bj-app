import { ISelectOption } from "../components/ui/inputs";

export const CUSTOM_FIELD_NAME = "Custom";

export const BASE_URL = process.env.REACT_APP_BASE_URL;

export const FAIL_REGISTER_REDIRECT = "/";

export const LOCATION_AVAILABLE_OPTIONS: ISelectOption[] = [
    {
        value: "country",
        label: "Country",
    },
    {
        value: "kanton",
        label: "Canton",
    },
    {
        value: "city",
        label: "City",
    },
    {
        value: "street",
        label: "Street",
    },
];

export const OBJECT_SEARCH_SCOPES: ISelectOption[] = [
    {
        value: "building",
        label: "Within Building",
    },
    {
        value: "city",
        label: "Within City",
    },
    {
        value: "general",
        label: "General Account",
    },
];

export const GRAPHQL_ADMIN_CONTEXT = {
    context: {
        headers: {
            is_admin: true,
        },
    },
};

export const DATE_FORMAT = "yyyy-MM-dd";
export const DATE_FORMAT_WITH_TIME = "yyyy-MM-dd HH:mm:ss";
