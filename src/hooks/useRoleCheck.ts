import { useContext } from "react";
import { AccountStateContext } from "../contexts/accountStateContext";
import { TRole, TUserType } from "../config/types";

export function useRoleCheck(availableForRoles?: TRole[], availableForAdminRoles?: TUserType[]): boolean {
    const { user, currentOrganization } = useContext(AccountStateContext);

    const { type } = currentOrganization.role;

    if (user) {
        if (user.is_admin) {
            return true;
        }

        if (availableForAdminRoles && availableForAdminRoles.length > 0) {
            return availableForAdminRoles.includes(user.type);
        }
    }

    if (!type || !availableForRoles) {
        return false;
    }

    return availableForRoles.includes(type);
}
