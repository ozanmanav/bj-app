import { useContext } from "react";
import { AccountStateContext } from "../contexts/accountStateContext";
import { useCountryByIP } from "./useCountryByIP";
import { findPhoneCodeByCountry } from "../utils";

export function useCountryPhoneCode(defaultCountry?: string) {
    const { currentOrganization } = useContext(AccountStateContext);
    const companyCountry = currentOrganization.country;

    const IPCountry = useCountryByIP();
    const country = IPCountry ? IPCountry.name : "";

    return { phone_code: findPhoneCodeByCountry(defaultCountry || country || companyCountry), country };
}
