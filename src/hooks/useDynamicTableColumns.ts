import { useEffect, useState } from "react";
import { ICheckboxDropdownItemState } from "../components/ui/checkboxDropdown";

const DYNAMIC_TABLES_STORAGE_KEY_PREFIX = "dynamic_columns_";
type TDynamicTableColumnsKeys = "object_details" | "file_details";

export function useDynamicTableColumns(defaultColumns: ICheckboxDropdownItemState[], localStorageKey: TDynamicTableColumnsKeys) {
    const [columns, setColumns] = useState<ICheckboxDropdownItemState[]>(() => {
        const savedColumns = localStorage.getItem(getFullKey(localStorageKey));

        return savedColumns ? JSON.parse(savedColumns) : defaultColumns;
    });
    const columnsToShow = columns.filter(({ value }) => value);

    useEffect(() => {
        localStorage.setItem(getFullKey(localStorageKey), JSON.stringify(columns));
    }, [localStorageKey, columns]);

    function updateColumns(name: string, value: boolean) {
        setColumns((prevColumns) =>
            prevColumns.map((column) => {
                if (column.name === name) {
                    return {
                        ...column,
                        value,
                    };
                }

                return column;
            })
        );
    }

    return {
        columns,
        columnsToShow,
        updateColumns,
    };
}

function getFullKey(key: string) {
    return DYNAMIC_TABLES_STORAGE_KEY_PREFIX + key;
}
