/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";

interface IUseSubmitState {
    isSubmitting: boolean;
    args: any;
}

const useSubmitInitialState = {
    isSubmitting: false,
    args: null,
};

export function useSubmit(submit: Function) {
    const [state, setState] = useState<IUseSubmitState>(useSubmitInitialState);

    function startSubmit(data?: any) {
        setState({
            args: data,
            isSubmitting: true,
        });
    }

    function stopSubmit() {
        setState({
            args: null,
            isSubmitting: false,
        });
    }

    useEffect(() => {
        if (state.isSubmitting) {
            submit(state.args);
        }
    }, [state]);

    return {
        startSubmit,
        stopSubmit,
    };
}
