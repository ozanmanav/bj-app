import { useState, useEffect } from "react";

interface IWindowDimensions {
    innerWidth: number | null;
    innerHeight: number | null;
    outerWidth: number | null;
    outerHeight: number | null;
}

const initialValue: IWindowDimensions = {
    innerWidth: null,
    innerHeight: null,
    outerWidth: null,
    outerHeight: null,
};

/**
 * useWindowSize
 *
 * A hook that provides information of the dimensions of the window
 *
 * @return {IWindowDimensions}  Dimensions of the window
 */
export function useWindowSize(): IWindowDimensions {
    const [windowSize, setWindowSize] = useState<IWindowDimensions>(initialValue);

    function fetchWindowDimensionsAndSave() {
        setWindowSize({
            innerWidth: window.innerWidth,
            innerHeight: window.innerHeight,
            outerWidth: window.outerWidth,
            outerHeight: window.outerHeight,
        });
    }

    // set resize handler once on mount and clean before unmount
    useEffect(() => {
        fetchWindowDimensionsAndSave();

        window.addEventListener("resize", fetchWindowDimensionsAndSave);
        return () => {
            window.removeEventListener("resize", fetchWindowDimensionsAndSave);
        };
    }, []);

    return windowSize;
}
