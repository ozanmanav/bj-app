import { useEffect, useRef } from "react";

export function useLoadScript(url: string, cb?: Function) {
    const isLoaded = useRef<boolean>(false);

    useEffect(() => {
        if (isLoaded.current || document.querySelector(`script[src="${url}"]`)) {
            if (cb) {
                cb();
            }

            return;
        }

        const script = document.createElement("script");
        script.src = url;
        script.addEventListener("load", () => {
            isLoaded.current = true;

            if (cb) {
                cb();
            }
        });

        document.body.appendChild(script);
    }, [url]);
}
