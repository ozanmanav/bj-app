import { useEffect, useState } from "react";

const LOCATION_LOOKUP_URL = "https://get.geojs.io/v1/ip/country.json";

interface ICountryData {
    name: string;
    country: string;
    ip: string;
    country_3: string;
}

let cachedCountry: ICountryData;

export function useCountryByIP() {
    const [country, setCountry] = useState<ICountryData>();

    useEffect(() => {
        async function getLocation() {
            try {
                const country = await fetchCountry();

                setCountry(country);
            } catch (e) {
                console.error(e);
            }
        }

        getLocation();
    }, []);

    return country;
}

async function fetchCountry() {
    if (!cachedCountry) {
        cachedCountry = await fetch(LOCATION_LOOKUP_URL).then((res) => res.json());
    }

    return cachedCountry;
}
