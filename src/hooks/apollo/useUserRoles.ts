import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { useContext } from "react";
import { AccountStateContext } from "../../contexts/accountStateContext";

const GET_ROLES = gql`
    query UserRolesList($organization_role_type: String) {
        user_roles_list(organization_role_type: $organization_role_type) {
            id
            name
            company_type
        }
    }
`;

const GET_USER_ROLES_WITHOUT_TYPE = gql`
    query UserRolesList {
        user_roles_list {
            id
            name
        }
    }
`;

interface IUserRolesProps {
    forCurrentCompanyType?: boolean;
    organization_role_type?: string;
}

export function useUserRoles({ forCurrentCompanyType, organization_role_type }: IUserRolesProps) {
    const { currentOrganization } = useContext(AccountStateContext);

    const { data } = useQuery(GET_ROLES, {
        variables: {
            organization_role_type: organization_role_type ? organization_role_type : currentOrganization.type || null,
        },
    });

    return (data && data.user_roles_list) || [];
}

export function useUserRolesWithoutType() {
    const { data } = useQuery(GET_USER_ROLES_WITHOUT_TYPE, {
        variables: {},
    });

    return (data && data.user_roles_list) || [];
}
