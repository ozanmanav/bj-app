import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";
import get from "lodash.get";

export const GET_USER_LANGUAGES = gql`
    query GetUserLanguages {
        user_languages {
            id
            name
        }
    }
`;

export function useUserLanguages() {
    const { data } = useQuery(GET_USER_LANGUAGES);
    return get(data, "user_languages") || [];
}
