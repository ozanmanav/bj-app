import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";
import get from "lodash.get";

const GET_JOB_TITLES = gql`
    query GetJobTitles {
        user_job_titles {
            id
            name
            groups {
                name
            }
        }
    }
`;

export function useJobTitles() {
    const { data } = useQuery(GET_JOB_TITLES);

    return get(data, "user_job_titles") || [];
}
