import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";
import { IOrganizationFormState } from "../../components/forms/organizationForm";
import { useHistory } from "react-router-dom";
import { getObjectLink, normalizeDetails } from "../../utils";
import { handleGraphQLErrors, showErrorToast } from "../../components/ui/toasts";
import get from "lodash.get";
import { getArrayIDs } from "../../views/app/event/addEvent/utils";
import { CREATE_DETAIL } from "../../components/object/objectDetailsTable/queries";
import { IMetafield } from "../../config";

const CREATE_CONTRACT = gql`
    mutation CreateContract(
        $organization_id: String!
        $role_id: String!
        $object_ids: [String]!
        $employee_ids: [String]
        $file_ids: [String]
    ) {
        createContract(
            organization_id: $organization_id
            role_id: $role_id
            object_ids: $object_ids
            employee_ids: $employee_ids
            file_ids: $file_ids
        ) {
            id
            objects {
                id
            }
        }
    }
`;

interface ISendContractProps {
    contractState: IOrganizationFormState;
    objectID: string;
    redirectToObject?: boolean;
    objectType?: string;
}

function prepareDetailData(details: IMetafield[]) {
    return details.map((detail) => {
        const { verification, source, source_url, source_date, verification_by } = get(detail, "meta");
        return {
            name: get(detail, "name"),
            type: get(detail, "type"),
            value: get(detail, "value"),
            context: get(detail, "context"),
            highlighted: get(detail, "highlighted") || false,
            verification,
            source,
            source_url,
            source_date,
            verification_by,
            is_custom_field: get(detail, "name") === "Custom",
        };
    });
}
export function useSendContract(callback?: Function) {
    const history = useHistory();
    const [sendContractMutation] = useMutation(CREATE_CONTRACT);
    const [createContractDetailMutation] = useMutation(CREATE_DETAIL);

    async function sendContract({ contractState, objectID, objectType, redirectToObject }: ISendContractProps) {
        const organizationID = get(contractState, "relationships[0].organization_id");
        const roleID = get(contractState, "relationships[0].role");
        const objects = get(contractState, "relationships[0].objects") || [];
        const contacts = get(contractState, "relationships[0].contacts") || [];
        const files = get(contractState, "files") || [];

        const objectIDs = [objectID, ...getArrayIDs(objects)];
        const employeesIDs = getArrayIDs(contacts);
        const fileIDs = getArrayIDs(files);
        const details = normalizeDetails(contractState.rows.contractInformation);

        try {
            const options = {
                variables: {
                    organization_id: organizationID,
                    role_id: roleID,
                    object_ids: objectIDs,
                    employee_ids: employeesIDs,
                    file_ids: fileIDs,
                },
            };

            const res = await sendContractMutation(options);
            const { errors, data } = res;

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                await createContractDetailMutation({
                    variables: {
                        details: prepareDetailData(details),
                        model: "CONTRACT",
                        model_id: data.createContract.id,
                    },
                });
                if (redirectToObject && objectType) {
                    history.push(`${getObjectLink(objectType, objectID)}`);
                }
                callback && callback();
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    return sendContract;
}
