import { useQuery } from "@apollo/react-hooks";

import gql from "graphql-tag";

const GET_ORGANIZATION_ROLES = gql`
    query GetOrganizationRoles {
        organization_roles {
            id
            name
        }
    }
`;

export function useOrganizationRoles() {
    const { data } = useQuery(GET_ORGANIZATION_ROLES);

    return (data && data.organization_roles) || [];
}
