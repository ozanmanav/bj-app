import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { useContext } from "react";
import { AccountStateContext } from "../../contexts/accountStateContext";
import get from "lodash.get";
import { IGroup } from "../../config/types";

const FILE_TYPES = gql`
    query GetFileTypes {
        entity_types(entity: "OBJECT", is_file: true) {
            data {
                id
                name
                type
                groups {
                    name
                }
            }
        }
    }
`;

const AUTOMATICALLY_CREATED_TYPES: any = ["File type1"];

export function useFileTypes(
    shouldRemoveAutomaticallyCreatedTypes?: boolean
): { id: string; name: string; type: string; groups: IGroup[] }[] {
    const { user } = useContext(AccountStateContext);
    const { data } = useQuery(FILE_TYPES);

    const fileTypes = get(data, "entity_types.data") || [];

    if (!shouldRemoveAutomaticallyCreatedTypes || user.is_admin) {
        return fileTypes;
    }

    return fileTypes.filter(({ type }: { type: string }) => !AUTOMATICALLY_CREATED_TYPES.includes(type));
}
