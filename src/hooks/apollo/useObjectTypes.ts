import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { useContext } from "react";
import { AccountStateContext } from "../../contexts/accountStateContext";
import get from "lodash.get";
import { IGroup } from "../../config/types";

const OBJECT_TYPES = gql`
    query GetObjectTypes {
        entity_types(is_file: false, entity: "OBJECT") {
            data {
                id
                name
                meta_concat
                type
                groups {
                    name
                }
            }
        }
    }
`;

const AUTOMATICALLY_CREATED_TYPES = ["country", "kanton", "city", "street"];

export function useObjectTypes(
    shouldRemoveAutomaticallyCreatedTypes?: boolean
): { id: string; name: string; type: string; groups: IGroup[] }[] {
    const { user } = useContext(AccountStateContext);
    const { data } = useQuery(OBJECT_TYPES);

    const objectTypes = get(data, "entity_types.data") || [];

    if (!shouldRemoveAutomaticallyCreatedTypes || user.is_admin) {
        return objectTypes;
    }

    return objectTypes.filter(({ type }: { type: string }) => !AUTOMATICALLY_CREATED_TYPES.includes(type));
}
