import { useQuery } from "@apollo/react-hooks";

import gql from "graphql-tag";

const ORGANIZATION_TYPES = gql`
    query GetOrganizationTypes {
        organization_types {
            id
            name
            type
            groups {
                name
            }
        }
    }
`;

export function useOrganizationTypes() {
    const { data } = useQuery(ORGANIZATION_TYPES);

    return (data && data.organization_types) || [];
}
