import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";
import { IOrganizationFormState } from "../../components/forms/organizationForm";
import { useHistory } from "react-router-dom";
import { getObjectLink, normalizeDetails, isNillOrEmpty } from "../../utils";
import { handleGraphQLErrors, showErrorToast } from "../../components/ui/toasts";
import get from "lodash.get";
import { getArrayIDs } from "../../views/app/event/addEvent/utils";
import { UPDATE_DETAIL, CREATE_DETAIL } from "../../components/object/objectDetailsTable/queries";
import { prepareDetailDataForCreateMutation, prepareDetailDataForUpdateMutation } from "../../components/filesList/addFileModal/utils";

//TODO: details deleted from here should implement another place.
const UPDATE_CONTRACT = gql`
    mutation UpdateContract(
        $id: String!
        $organization_id: String!
        $role_id: String!
        $object_ids: [String]!
        $employee_ids: [String]
        $file_ids: [String]
    ) {
        updateContract(
            id: $id
            organization_id: $organization_id
            role_id: $role_id
            object_ids: $object_ids
            employee_ids: $employee_ids
            file_ids: $file_ids
        ) {
            id
            objects {
                id
            }
        }
    }
`;
export function groupDetails(detailsData: any, initialDetailsState: any) {
    const details = normalizeDetails(initialDetailsState.rows.contractInformation as []) || [];
    const detailsNames = details.map((item) => item.name) || [];

    const stateDetails = normalizeDetails(detailsData) || [];

    const newDetails = details.length === 0 ? stateDetails : stateDetails.filter((e) => !detailsNames.includes(e.name));
    const updateDetails = stateDetails.filter((item) => !isNillOrEmpty(get(item, "id")));
    return { newDetails, updateDetails };
}

interface IUpdateContractProps {
    contractState: IOrganizationFormState;
    contractID?: string;
    objectID: string;
    redirectToObject?: boolean;
    objectType?: string;
    initialValues?: object;
}

export function useUpdateContract(callback?: Function) {
    const history = useHistory();
    const [updateContractMutation] = useMutation(UPDATE_CONTRACT);
    const [updateContractDetailMutation] = useMutation(UPDATE_DETAIL);
    const [createContractDetailMutation] = useMutation(CREATE_DETAIL);

    async function updateContract({
        contractID,
        contractState,
        objectID,
        objectType,
        redirectToObject,
        initialValues,
    }: IUpdateContractProps) {
        const organizationID = get(contractState, "relationships[0].organization_id");
        const roleID = get(contractState, "relationships[0].role");
        const objects = get(contractState, "relationships[0].objects") || [];
        const contacts = get(contractState, "relationships[0].contacts") || [];
        const files = get(contractState, "files") || [];
        const details = get(contractState, "rows.contractInformation") || [];
        const objectIDs = [objectID, ...getArrayIDs(objects)];
        const employeesIDs = getArrayIDs(contacts);
        const fileIDs = getArrayIDs(files);

        try {
            const options = {
                variables: {
                    id: contractID,
                    organization_id: organizationID,
                    role_id: roleID,
                    object_ids: objectIDs,
                    employee_ids: employeesIDs,
                    file_ids: fileIDs,
                },
            };

            const res = await updateContractMutation(options);
            const { errors } = res;

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                try {
                    const { updateDetails, newDetails } = groupDetails(details, initialValues);
                    await Promise.all([
                        ...updateDetails.map((detail) => {
                            const { id } = detail;

                            if (id) {
                                return updateContractDetailMutation({
                                    variables: prepareDetailDataForUpdateMutation(detail, contractID, false, "CONTRACT"),
                                });
                            }
                            return null;
                        }),
                        ...newDetails.map(({ id, isCustomField, ...restDetail }) => {
                            if (!id) {
                                return createContractDetailMutation({
                                    variables: prepareDetailDataForCreateMutation(restDetail, contractID, false, "CONTRACT"),
                                });
                            }
                            return null;
                        }),
                    ]);
                } catch (e) {
                    showErrorToast(e.message);
                }
                if (redirectToObject && objectType) {
                    history.push(`${getObjectLink(objectType, objectID)}`);
                }
                callback && callback();
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    return updateContract;
}
