import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { useContext } from "react";
import { AccountStateContext } from "../../contexts/accountStateContext";
import get from "lodash.get";
import { IRoleTypes } from "../../config/types";

const CONTRACT_ROLES = gql`
    query GetContractRoles {
        entity_types(is_file: false, entity: "CONTRACT") {
            data {
                id
                name
                type
                groups {
                    name
                }
            }
        }
    }
`;

export function useContractRoles(shouldRemoveAutomaticallyCreatedTypes?: boolean): IRoleTypes[] {
    const { user } = useContext(AccountStateContext);
    const { data } = useQuery(CONTRACT_ROLES);

    const contractRoles = get(data, "entity_types.data") || [];

    if (!shouldRemoveAutomaticallyCreatedTypes || user.is_admin) {
        return contractRoles;
    }

    return contractRoles;
}
