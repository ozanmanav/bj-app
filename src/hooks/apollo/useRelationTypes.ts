import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import get from "lodash.get";

const GET_RELATION_TYPES = gql`
    query GetRelationTypes {
        entity_types(entity: "RELATIONSHIP") {
            data {
                id
                name
                type
                groups {
                    name
                }
            }
        }
    }
`;

export function useRelationTypes() {
    const { data } = useQuery(GET_RELATION_TYPES);

    return get(data, "entity_types.data") || [];
}
