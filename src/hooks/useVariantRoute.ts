type IVariant = "building" | "object" | "organization";

export const useVariantRoute = (location: any) => {
    if (location.pathname) {
        const variant: IVariant =
            (location.pathname.includes("building") && "building") ||
            (location.pathname.includes("object") && "object") ||
            (location.pathname.includes("organization") && "organization");

        const variantID: string = location.pathname.split(variant)[1].split("/")[1];

        return { variant, variantID };
    } else {
        return { variant: "", variantID: "" };
    }
};
