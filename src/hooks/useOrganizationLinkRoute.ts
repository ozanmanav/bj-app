import { useContext } from "react";
import { AccountStateContext } from "../contexts/accountStateContext";

export function useOrganizationLinkRoute(isClientOrganization: boolean, organizationID?: string): string {
    const { user } = useContext(AccountStateContext);

    let linkRoute = isClientOrganization ? `/app/organizations/client/${organizationID}` : `/app/organizations/${organizationID}`;

    if (user.is_admin) {
        linkRoute = isClientOrganization
            ? `/app/admin/organizations/client/${organizationID}`
            : `/app/admin/organizations/${organizationID}`;
    }

    return linkRoute;
}
