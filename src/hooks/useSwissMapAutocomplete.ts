import { useDebounce } from "use-debounce";
import { useEffect, useState } from "react";
import { searchByAddress } from "../components/maps/utils";

const SWISS_MAP_AUTOCOMPLETE_DEBOUNCE = 300;

export interface ISwissMapFeatureInfo {
    lat: number;
    lon: number;
    detail: string;
    featureId: string;
    label: string;
}

export function useSwissMapAutocomplete(address: string) {
    const [debouncedAddress] = useDebounce(address, SWISS_MAP_AUTOCOMPLETE_DEBOUNCE);

    const [searchResults, setSearchResults] = useState<ISwissMapFeatureInfo[]>([]);

    useEffect(() => {
        async function getResults() {
            const data = await searchByAddress(debouncedAddress, 20);

            if (data.results && data.results.length > 0) {
                setSearchResults(
                    data.results.map(({ attrs }: any) => ({
                        ...attrs,
                        label: attrs.label && attrs.label.replace(/<[^>]*>/g, ""),
                    }))
                );
            } else {
                setSearchResults([]);
            }
        }

        if (debouncedAddress) {
            getResults();
        }
    }, [debouncedAddress]);

    return searchResults;
}
