import { useEffect, useReducer } from "react";

const SORTING_TABLE_STORAGE_KEY_PREFIX = "sorting_table_";

export type TSortingOrder = "asc" | "desc";

export type TSortingTableKey =
    | "admin_groups_list"
    | "admin_types_object"
    | "admin_types_organization"
    | "admin_types_contract"
    | "admin_types_relationship"
    | "admin_types_file"
    | "admin_types_event"
    | "admin_types_job_title"
    | "select_relationships_to_copy"
    | "contract_affected_objects"
    | "teams"
    | "related_users"
    | "related_object"
    | "admin_organizations"
    | "admin_related_users"
    | "admin_companies"
    | "admin_company_related_objects"
    | "admin_organization_related_users"
    | "subsidiaries"
    | "search"
    | "invite_users"
    | "files";

interface ISortingTableArguments {
    localStorageKey: TSortingTableKey;
    defaultSortBy?: string;
    defaultSortingOrder?: TSortingOrder;
}

interface ISortingTableState {
    sortingOrder: TSortingOrder;
    sortBy: string;
}

type ISortingTableAction =
    | {
          type: "TOGGLE_SORTING_ORDER";
      }
    | {
          type: "CHANGE_SORT_BY";
          payload: string;
      };

function SortingTableReducer(state: ISortingTableState, action: ISortingTableAction): ISortingTableState {
    switch (action.type) {
        case "CHANGE_SORT_BY":
            return {
                sortBy: action.payload,
                sortingOrder: "asc",
            };
        case "TOGGLE_SORTING_ORDER":
            return {
                ...state,
                sortingOrder: state.sortingOrder === "asc" ? "desc" : "asc",
            };
        default:
            return state;
    }
}

export function useSortingTable({ localStorageKey, defaultSortBy, defaultSortingOrder }: ISortingTableArguments) {
    const [state, dispatch] = useReducer(
        SortingTableReducer,
        {
            sortBy: defaultSortBy || "",
            sortingOrder: defaultSortingOrder || "asc",
        },
        (defaultState: ISortingTableState) => {
            const savedState = localStorage.getItem(getFullKey(localStorageKey));

            return savedState ? JSON.parse(savedState) : defaultState;
        }
    );

    const { sortBy, sortingOrder } = state;

    useEffect(() => {
        localStorage.setItem(getFullKey(localStorageKey), JSON.stringify(state));
    }, [state, localStorageKey]);

    function changeSortBy(newSortBy: string) {
        if (newSortBy === sortBy) {
            dispatch({ type: "TOGGLE_SORTING_ORDER" });
            return;
        }

        dispatch({ type: "CHANGE_SORT_BY", payload: newSortBy });
    }

    function getSortOrderForField(sortField: string): TSortingOrder | "none" {
        if (sortField === sortBy) {
            return sortingOrder;
        }

        return "none";
    }

    return {
        sortingOrder,
        sortBy,
        changeSortBy,
        getSortOrderForField,
    };
}

function getFullKey(key: string) {
    return SORTING_TABLE_STORAGE_KEY_PREFIX + key;
}
