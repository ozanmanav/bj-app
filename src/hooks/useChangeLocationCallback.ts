import { useEffect } from "react";
import { LocationState } from "history";
import { usePrevious } from "./usePrevious";

export function useChangeLocationCallback(changeLocationCallback: Function, location: LocationState) {
    const prevLocationPathname = usePrevious(location.pathname);

    useEffect(() => {
        if (prevLocationPathname !== location.pathname) {
            changeLocationCallback();
        }
    }, [prevLocationPathname, location]);
}
