export function generateRolesString(contracts: { role: { name: string } }[] = []): string {
    return contracts.map(({ role }) => role && role.name).join(", ");
}
