import ReactCrop from "react-image-crop";

export function getCroppedImg(image: HTMLImageElement, crop: ReactCrop.Crop | any, fileName: string) {
    const canvas = document.createElement("canvas");
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = crop.width;
    canvas.height = crop.height;
    const ctx: CanvasRenderingContext2D | null = canvas.getContext("2d");

    ctx && ctx.drawImage(image, crop.x * scaleX, crop.y * scaleY, crop.width * scaleX, crop.height * scaleY, 0, 0, crop.width, crop.height);

    return new Promise((resolve, reject) => {
        canvas.toBlob((blob: Blob | any) => {
            blob.name = fileName;
            blob.lastModifiedDate = new Date();
            resolve(blob);
        }, "image/jpeg");
    });
}
