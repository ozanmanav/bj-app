import { IMetafield } from "../config/types";

export function getDeserializedDetails(details: IMetafield[] = []) {
    return details.map((detail: IMetafield) => ({
        ...detail,
        value: JSON.parse(detail.value),
    }));
}
