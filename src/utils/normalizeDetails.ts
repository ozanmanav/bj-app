import { IMetafield, CUSTOM_FIELD_NAME } from "../config";
import { removeFalseValuesFromObject } from "./removeFalseValuesFromObject";
import { formatDate } from "../components/ui/datepicker/utils";

export function normalizeDetails(details: any[]): IMetafield[] {
    return details.reduce(
        (
            acc,
            {
                name,
                type,
                value,
                customName,
                author,
                author_data,
                updater,
                updater_data,
                updated_at,
                created_at,
                highlighted,
                __typename,
                context,
                ...rest
            }
        ) => {
            if (value) {
                const isCustomField = name === CUSTOM_FIELD_NAME;

                acc.push({
                    ...removeFalseValuesFromObject(rest),
                    name: isCustomField ? (customName as string) : name,
                    type,
                    context,
                    value: type === "date" ? formatDate(new Date(value), "yyyy/MM/dd") : value,
                    isCustomField,
                });
            }

            return acc;
        },
        []
    );
}

export function normalizeObjectDetails(details: any[]): IMetafield[] {
    return details.reduce(
        (
            acc,
            {
                name,
                type,
                value,
                customName,
                author,
                author_data,
                updater,
                updater_data,
                updated_at,
                created_at,
                highlighted,
                __typename,
                context,
                ...rest
            }
        ) => {
            if (value) {
                const isCustomField = name === CUSTOM_FIELD_NAME;

                acc.push({
                    ...removeFalseValuesFromObject(rest),
                    name: isCustomField ? (customName as string) : name,
                    type: type || "string",
                    context,
                    value: type === "date" ? formatDate(new Date(value), "yyyy/MM/dd") : value,
                });
            }

            return acc;
        },
        []
    );
}
