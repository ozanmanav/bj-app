interface INormilizedTotals {
    objects?: string;
    buildings?: string;
    custom_fields?: string;
    total?: string;
}

export function normalizeTotals(totals: { name: string; value: string }[] = []): INormilizedTotals {
    return totals.reduce(
        (acc, { name, value }) => ({
            ...acc,
            [name]: value,
        }),
        {}
    );
}
