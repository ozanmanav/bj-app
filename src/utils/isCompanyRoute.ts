import { Location } from "history";

export function isClientOrganizationRoute(location: Location<any>) {
    return location.pathname.includes("client");
}
