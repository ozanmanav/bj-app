export function removeFalseValuesFromObject(object: { [key: string]: any }) {
    return Object.keys(object).reduce((acc: { [key: string]: any }, key) => {
        if (object[key]) {
            acc[key] = object[key];
        }

        return acc;
    }, {});
}
