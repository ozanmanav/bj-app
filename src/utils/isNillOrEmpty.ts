import isNil from "ramda/es/isNil";
import isEmpty from "ramda/es/isEmpty";

// This function is checking value NOT empty and NOT Null.
export const isNillOrEmpty = (value: any) => {
    return isNil(value) || isEmpty(value);
};
