import { phoneCodesByCountry } from "../config";

export function findPhoneCodeByCountry(country?: string): string {
    if (!country) {
        return "";
    }

    const phoneCode = phoneCodesByCountry.find(({ label }) => label.toLowerCase().includes(country.toLowerCase()));
    return phoneCode ? phoneCode.value : "";
}
