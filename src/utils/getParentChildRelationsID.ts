export function getParentChildRelationsID(relationTypes: { id: string; name: string; type: string }[]): string {
    const parentChildRelationType = relationTypes.find(({ type }: { type: string }) => type === "parent_child");

    return parentChildRelationType ? parentChildRelationType.id : "";
}
