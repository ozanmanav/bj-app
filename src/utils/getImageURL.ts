import { BASE_URL } from "../config";

export function getImageURL(url?: string) {
    return url ? BASE_URL + url : "";
}
