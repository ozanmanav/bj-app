import * as Yup from "yup";
import { VALIDATION_ERRORS } from "../config";

export function getYupType(type: string) {
    if (type === "date") {
        return Yup.date()
            .typeError(VALIDATION_ERRORS.date)
            .required(VALIDATION_ERRORS.required);
    }

    if (type === "number") {
        return Yup.number()
            .typeError(VALIDATION_ERRORS.number)
            .required(VALIDATION_ERRORS.required);
    }

    return Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required);
}
