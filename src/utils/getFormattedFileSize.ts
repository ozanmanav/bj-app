export function getFormattedFileSize(size: number | string) {
    return `${(+size / 1000).toFixed(2)} kb`;
}
