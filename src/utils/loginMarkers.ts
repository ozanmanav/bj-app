const IS_LOGGED_IN_LOCAL_STORAGE_KEY = "is_logged_in";

export function setLoginMarker() {
    localStorage.setItem(IS_LOGGED_IN_LOCAL_STORAGE_KEY, "true");
}

export function removeLoginMarker() {
    localStorage.removeItem(IS_LOGGED_IN_LOCAL_STORAGE_KEY);
}

export function checkLoginMarker() {
    return localStorage.getItem(IS_LOGGED_IN_LOCAL_STORAGE_KEY) === "true";
}
