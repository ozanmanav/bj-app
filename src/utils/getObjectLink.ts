import get from "lodash.get";

export function getObjectLink(type: string, id?: string, postfix?: string) {
    const objectType = get(type, "type");

    let link = `/app/${objectType === "building" ? "building" : "object"}/${id}`;

    if (postfix) {
        link += `/${postfix}`;
    }

    return link;
}
