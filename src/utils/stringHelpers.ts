export function splitAndCapitalizeFirstLetter(value: string) {
    const splittedValue = value && value.split("_")[1];

    return splittedValue ? splittedValue.charAt(0).toUpperCase() + splittedValue.slice(1) : splittedValue;
}
