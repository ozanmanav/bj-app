import { IMetatype } from "../config/types";

export function getTypeLabel(type: string, objectTypes: IMetatype[]) {
    const objectType = objectTypes.find((item) => item.type === type);

    return objectType ? objectType.name : "";
}
