import { IInfoRowParams } from "../components/forms/dynamicInfoRows";

const INCLUDED_FIELDS = ["kanton", "Ort", "strasse"];

export function normalizeAddressParameters(addressDetails: IInfoRowParams[] = []) {
    return addressDetails.reduce(
        (acc, item) => {
            if (!INCLUDED_FIELDS.includes(item.name)) {
                return acc;
            }

            return {
                ...acc,
                [getParamName(item.name)]: item.value,
            };
        },
        {
            country: "Switzerland",
        }
    );
}

function getParamName(detailName: string) {
    if (detailName === "strasse") {
        return "street";
    }

    if (detailName === "Ort") {
        return "city";
    }

    return detailName.toLowerCase();
}
