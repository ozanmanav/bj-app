import { findPhoneCodeByCountry } from "./findPhoneCodeByCountry";

export function getPhoneInitialValues(phone?: string, country?: string): { phone: string; phone_code: string } {
    if (!phone) {
        return {
            phone_code: findPhoneCodeByCountry(country),
            phone: "",
        };
    }

    const phoneGroups = phone.split(" ");

    const phoneGroupsLength = phoneGroups.length;

    if (phoneGroups.length === 1 && phoneGroups[0].includes("+")) {
        // Only phone code available
        return {
            phone_code: phoneGroups[0],
            phone: "",
        };
    }

    return {
        phone_code: phoneGroups.slice(0, phoneGroupsLength - 1).join(" "),
        phone: phoneGroups[phoneGroupsLength - 1],
    };
}
