export function groupRelatedObjects(data: any[]): any[] {
    const groupsObj: { [key: string]: any[] } = {};

    data.forEach((item) => {
        groupsObj[item.typeLabel] = groupsObj[item.typeLabel] ? [...groupsObj[item.typeLabel], item] : [item];
    });

    return Object.entries(groupsObj);
}
