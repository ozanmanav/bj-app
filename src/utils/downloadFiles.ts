import { TFile } from "../config/types";
import { BASE_URL } from "../config";

export function downloadFiles(files: TFile[]) {
    const link = document.createElement("a");
    link.target = "_blank";
    document.body.appendChild(link);

    files.forEach(async ({ url, filename }) => {
        const blob = await fetch(BASE_URL + url).then((res) => res.blob());
        link.download = filename;
        link.href = URL.createObjectURL(blob);
        link.click();
    });

    document.body.removeChild(link);
}
