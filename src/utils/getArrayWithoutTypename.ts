export function getArrayWithoutTypename(array: any[] = []): any[] {
    return array.map((item: any) => {
        const { __typename, ...props } = item;
        return props;
    });
}

export function getObjectWithoutTypename(rawObject: any = {}): any {
    const { __typename, ...object } = rawObject;

    return object;
}
