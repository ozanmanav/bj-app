import { Location } from "history";

export function isBuildingRoute(location: Location<any>) {
    return location.pathname.includes("building");
}
