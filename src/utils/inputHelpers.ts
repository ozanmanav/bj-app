import { IJobTitle, ILanguage, IMetatype, IObjectTypes, IRelationshipRole, IRoleTypes } from "../config/types";
import { ISelectOption } from "../components/ui/inputs";
import get from "lodash.get";
import { IUserRole } from "../components/forms/convertToClientOrganizationForm/definitions";

const REMOVED_ADDRESS_FIELDS = [/*"EGID", "strasse", "nr", "PLZ", */ "kanton" /*"city"*/];

export function getMetafieldNames(fields: IMetatype[] = []): ISelectOption[] {
    return fields.filter(({ name }) => !REMOVED_ADDRESS_FIELDS.includes(name)).map(({ name }) => ({ value: name }));
}
export function getObjectTypesOptions(types: any[] = []): ISelectOption[] {
    return types.map((type) => {
        return { value: get(type, "id"), label: get(type, "name"), group: get(type, "groups[0].name"), meta: get(type, "meta_concat") };
    });
}

export function getPersonRolesOptions(roles: IUserRole[] = []): ISelectOption[] {
    return roles.map(({ name, id, company_type }) => ({
        value: id,
        label: name === "Administrator" ? `${name} ${company_type ? company_type.replace("_", " ") : ""}` : name,
    }));
}

export function getContractRolesOptions(types: IRoleTypes[] = []): ISelectOption[] {
    return types.map(({ id, name, groups }) => ({ value: id, label: name, group: get(groups, "[0].name") }));
}

export function getFileTypesOptions(types: IObjectTypes[] = []): ISelectOption[] {
    return types.map(({ id, name, groups }) => ({ value: id, label: name, group: get(groups, "[0].name") }));
}

export function getJobTitlesOptions(jobTitles: IJobTitle[] = []): ISelectOption[] {
    return jobTitles.map(({ id, name, groups }) => ({
        value: id,
        label: name,
        group: get(groups, "[0].name"),
    }));
}

export function getLanguagesOptions(languages: ILanguage[] = []): ISelectOption[] {
    return languages.map(({ name }) => ({ value: name, label: name }));
}

export function getRelationshipRolesOptions(relationships: IRelationshipRole[] = []): ISelectOption[] {
    return relationships.map(({ id, name, groups }) => ({ value: id, label: name, group: get(groups, "[0].name") }));
}

export function getCountriesOptions(countries: { name: string }[] = []): ISelectOption[] {
    return countries.map(({ name }) => ({ value: name, label: name }));
}

export function getOrganizationTypesOptions(types: { id: string; name: string; groups: string[] }[] = []): ISelectOption[] {
    return types.map(({ id, name, groups }) => ({ value: id, label: name, group: get(groups, "[0].name") }));
}

export function getOrganizationRolesOptions(types: { id: string; name: string }[] = []): ISelectOption[] {
    return types.map(({ id, name }) => ({ value: id, label: name }));
}

export function getUserRolesOptions(types: { id: string; name: string }[] = []): ISelectOption[] {
    return types.map(({ id, name }) => ({ value: id, label: name }));
}

export function getMetaTypesOptions(types: { id: string; name: string; groups: string[] }[] = []): ISelectOption[] {
    return types.map(({ id, name, groups }) => ({ value: id, label: name, group: get(groups, "[0].name") }));
}

export function getRelationTypesOptions(types: { id: string; name: string; groups: string[] }[]): ISelectOption[] {
    return types.reduce((acc: ISelectOption[], { id, name, groups }) => {
        const lowerName = name.split("-")[1];
        const higherName = name.split("-")[0];

        acc.push({
            value: `lower_${id}`,
            label: lowerName,
            group: get(groups, "[0].name"),
        });
        acc.push({
            value: `higher_${id}`,
            label: higherName,
            group: get(groups, "[0].name"),
        });

        return acc;
    }, []);
}
