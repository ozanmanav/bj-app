export function getLocationIncludesPath(locationPathName: string, pathWord: string): boolean {
    return locationPathName ? locationPathName.includes(pathWord) : false;
}

export function getLocationSearchParam(locationSearch: string, param: string): string {
    const searchParams = getAllLocationSearchParams(locationSearch);

    return searchParams[param] || "";
}

export function getAllLocationSearchParams(locationSearch: string): { [key: string]: string } {
    return locationSearch
        .slice(1)
        .split("&")
        .reduce((acc, item) => {
            const splitedItem = item.split("=");

            if (splitedItem.length !== 2) {
                return acc;
            }

            const itemName = splitedItem[0];
            const itemValue = splitedItem[1];

            return {
                ...acc,
                [itemName]: itemValue,
            };
        }, {});
}
