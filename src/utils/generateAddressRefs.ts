import { IRelation } from "../config/types";

export function generateAddressRefs(parents: IRelation[] = [], buildingID?: string) {
    // getting rid of duplicated using set
    return Array.from(
        new Set(
            parents.reduce(
                (acc: string[], { address_refs }) => {
                    if (!address_refs) {
                        return acc;
                    }

                    return [...acc, ...address_refs];
                },
                buildingID ? [buildingID] : []
            )
        )
    );
}
