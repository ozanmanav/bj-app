import React, { FunctionComponent, useContext } from "react";
import "./Login.scss";
import { Input, Button, handleGraphQLErrors, showErrorToast, useModal } from "../../components/ui";
import { COPYRIGHT_TEXT } from "../config";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { Formik, FormikActions } from "formik";
import * as Yup from "yup";
import { VALIDATION_ERRORS } from "../../config";
import { Redirect, RouteComponentProps } from "react-router";
import { LoginHeader } from "../../components/header";
import { AccountStateContext } from "../../contexts/accountStateContext";
import { checkLoginMarker } from "../../utils";
import { ForgotPasswordModal } from "../../components/modals/forgotPasswordModal";

export const LOGIN = gql`
    query Login($email: String!, $password: String!) {
        login(email: $email, password: $password) {
            id
            first_name
            last_name
            type
            is_admin
            email
            language
            phone
            employers {
                organization_id
                job {
                    name
                    id
                }
            }
        }
    }
`;

const LoginFormSchema = Yup.object().shape({
    email: Yup.string()
        .email(VALIDATION_ERRORS.email)
        .required(VALIDATION_ERRORS.required),
    password: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
});

export interface ILoginState {
    email: string;
    password: string;
}

const loginInitialState = {
    email: "",
    password: "",
};

export const Login: FunctionComponent<RouteComponentProps> = ({ history }) => {
    const { hide, open, isOpen } = useModal();

    const accountStateContext = useContext(AccountStateContext);

    const { refetch: loginRefetch } = useQuery(LOGIN, {
        skip: true,
    });

    async function login(values: ILoginState, form: FormikActions<ILoginState>) {
        try {
            const { data, errors } = await loginRefetch({
                email: values.email,
                password: values.password,
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                accountStateContext.saveUser(data.login);
                history.push("/app/cockpit");
            }
        } catch (e) {
            if (e.graphQLErrors) {
                e.graphQLErrors.forEach((error: any) => {
                    if (error.validation) {
                        error.validation.password && form.setFieldError("password", error.validation.password.join(" "));
                        error.validation.email && form.setFieldError("email", error.validation.email.join(" "));
                    } else {
                        showErrorToast(error.message);
                    }
                });
            }

            if (e.networkError) {
                showErrorToast(e.networkError.message);
            }
        } finally {
            form.setSubmitting(false);
        }
    }

    if (checkLoginMarker()) {
        return <Redirect to="/app/cockpit" />;
    }

    return (
        <>
            <LoginHeader />
            <main className="b-login flex flex-column align-center justify-center">
                <Formik
                    onSubmit={login}
                    initialValues={loginInitialState}
                    validationSchema={LoginFormSchema}
                    render={({ values, touched, errors, handleBlur, handleChange, handleSubmit, isSubmitting }) => (
                        <form className="b-login__form" onSubmit={handleSubmit}>
                            <div className="b-login__form-content">
                                <h2 className="h1 b-login__title">Welcome back</h2>
                                <Input
                                    placeholder="Your E-Mail address"
                                    name="email"
                                    onBlur={handleBlur}
                                    onChange={handleChange}
                                    value={values.email}
                                    error={errors && errors.email}
                                    touched={touched && touched.email}
                                    autoComplete="on"
                                />
                                <div className="b-login__form-input-wrapper">
                                    <Input
                                        className="b-login__form-input"
                                        placeholder="Password"
                                        name="password"
                                        type="password"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.password}
                                        error={errors && errors.password}
                                        touched={touched && touched.password}
                                        autoComplete="on"
                                    />
                                    <button type="button" className="b-login__form-input-link _font-bold _text-grey" onClick={open}>
                                        Forgot password?
                                    </button>
                                </div>
                            </div>
                            <div className="b-login__form-footer">
                                <Button text="Login" primary className="b-login__form-action" type="submit" disabled={isSubmitting} />
                            </div>
                        </form>
                    )}
                />
                <p className="b-login__copyright _text-grey h6">{COPYRIGHT_TEXT}</p>
            </main>
            <ForgotPasswordModal hide={hide} isOpen={isOpen} />
        </>
    );
};
