import "./Application.scss";
import "react-toastify/dist/ReactToastify.css";

import { COPYRIGHT_TEXT, SUSPEND_TEXT } from "../config";
import React, { FunctionComponent, useContext, useEffect, useState } from "react";
import { Route, RouteComponentProps, Switch } from "react-router-dom";

import { AccountStateContext } from "../../contexts/accountStateContext";
import { AddEvent } from "./event/addEvent";
import { AdminOrganization } from "./adminPanel/adminOrganization";
import { AdminPanel } from "./adminPanel";
import { AppHeader } from "../../components/header";
import { Cockpit } from "./cockpit";
import { EventDetail } from "./event/eventDetail";
import { Export } from "./export";
import { Loader } from "../../components/ui/loader";
import { MyOrganization } from "./myOrganization";
import { Object } from "./object";
import { TRole } from "../../config/types";
import { Tasks } from "../../components/tasks";
import get from "lodash.get";
import gql from "graphql-tag";
import { showErrorToast } from "../../components/ui";
import { useApolloClient } from "@apollo/react-hooks";
import { AddTask } from "./task/addTask";
import { TaskDetail } from "./task/taskDetail";
import { Settings } from "./settings/Settings";

const AUTH_CHECK = gql`
    query AuthCheck {
        auth_check
        current_client {
            id
            name
            category
            address {
                country
            }
            suspend
        }
        get_current_role {
            name
            type
            company_type
        }
        my_profile {
            id
            first_name
            last_name
            is_admin
            type
            email
            language
            phone
            employers {
                organization_id
                job {
                    name
                    id
                }
            }
        }
    }
`;

export const Application: FunctionComponent<RouteComponentProps> = ({ history }) => {
    const apolloClient = useApolloClient();
    const [loading, setLoading] = useState<boolean>(true);

    const { saveUser, saveCurrentOrganization, saveCurrentOrganizationList, clearAccountData } = useContext(AccountStateContext);

    useEffect(() => {
        async function initialize() {
            try {
                const { data } = await apolloClient.query({
                    query: AUTH_CHECK,
                });

                if (!data.auth_check) {
                    clearAccountData();
                    return history.push("/login");
                }

                if (!data.current_client || data.current_client.suspend) {
                    clearAccountData();
                    showErrorToast(SUSPEND_TEXT);
                    return history.push("/login");
                }

                const companyId = get(data, "current_client.id");
                const companyCategory = get(data, "current_client.category");
                const userCompanyName = get(data, "current_client.name");
                const companyCountry = get(data, "current_client.address.country");
                const userRoleName = get(data, "get_current_role.name");
                const userRoleType = get(data, "get_current_role.type");
                const userCompanyType = get(data, "get_current_role.company_type");
                const fullRoleType = userCompanyType + "_" + userRoleType;

                saveUser(data.my_profile);
                saveCurrentOrganizationList(data.my_profile.id);
                saveCurrentOrganization({
                    id: companyId,
                    name: userCompanyName,
                    type: userCompanyType,
                    role: {
                        type: fullRoleType as TRole,
                        name: userRoleName,
                    },
                    category: companyCategory,
                    country: companyCountry,
                });
            } catch (e) {
                clearAccountData();
                history.push("/login");
            }

            setLoading(false);
        }

        initialize();

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return loading ? (
        <Loader withText />
    ) : (
        <>
            <AppHeader />
            <main className="app">
                <Switch>
                    <Route path="/app/cockpit" component={Cockpit} />
                    <Route path="/app/event/add" component={AddEvent} />
                    <Route path="/app/event/:id" component={EventDetail} />
                    <Route path="/app/task/add" component={AddTask} />
                    <Route path="/app/task/:id" component={TaskDetail} />
                    <Route path={["/app/building", "/app/object"]} component={Object} />
                    <Route path={"/app/my-organization"} component={MyOrganization} />
                    <Route path={"/app/settings"} component={Settings} />
                    <Route path={"/app/organizations/client/:id"} component={AdminOrganization} />
                    <Route path={"/app/organizations/:id"} component={AdminOrganization} />
                    <Route path={"/app/admin"} component={AdminPanel} />
                    <Route path="/app/export" component={Export} />
                </Switch>
                <Tasks />
                <p className="_text-grey h6 _text-center app__copyright">{COPYRIGHT_TEXT}</p>
            </main>
        </>
    );
};
