import React, { FunctionComponent } from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { Container, Accordion, useAccordions } from "../../../components/ui";
import { SettingsItem } from "../../../components/settings/settingsItem";
import { ChangePasswordForm } from "../../../components/forms";
import { UserInformationForm } from "../../../components/forms/userInformationForm";
import "./Settings.scss";

export const SettingsBase: FunctionComponent<RouteComponentProps<{ id: string }>> = ({ match }) => {
    const { isOpened, toggleAccordion } = useAccordions([0]);

    return (
        <div className="b-settings">
            <Container>
                <SettingsItem>
                    <Accordion title="Change your user informations" isOpen={isOpened(1)} toggle={toggleAccordion} index={1}>
                        <UserInformationForm />
                    </Accordion>
                    <Accordion title="Change your password" isOpen={isOpened(2)} toggle={toggleAccordion} index={2}>
                        <ChangePasswordForm />
                    </Accordion>
                </SettingsItem>
            </Container>
        </div>
    );
};

export const Settings = withRouter(SettingsBase);
