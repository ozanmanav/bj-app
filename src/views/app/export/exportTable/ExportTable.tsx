import React, { FunctionComponent } from "react";
import { ObjectName, ISelectOption } from "../../../../components/ui";

// TODO: update
interface IExportTableProps {
    fields: ISelectOption[];
    data: any[];
}

export const ExportTable: FunctionComponent<IExportTableProps> = ({ fields, data }) => {
    return (
        <div className="b-table__wrapper b-export-table">
            <table className="b-table _first-cell-border">
                <thead className="b-table__head">
                    <tr className="b-table__row _head">
                        <th className="b-table__cell">{data.length}</th>
                        {fields.map((field) => (
                            <th className="b-table__cell" key={field.value}>
                                {field.value}
                            </th>
                        ))}
                    </tr>
                </thead>
                <tbody className="b-table__body">
                    {/* TODO: add sorting */}
                    {data.map((item, index) => (
                        <ExportTableRow key={item.name} item={item} index={index} fields={fields} />
                    ))}
                </tbody>
            </table>
        </div>
    );
};

// TODO: update
interface IExportTableRowProps {
    index: number;
    item: any;
    fields: ISelectOption[];
}

const ExportTableRow: FunctionComponent<IExportTableRowProps> = ({ index, item, fields }) => {
    return (
        <tr className="b-table__row">
            <td className="b-table__cell h6 _font-bold _text-grey">{index + 1}</td>
            {fields.map((field) => (
                <td className="b-table__cell" key={field.value}>
                    {field.value === "name" ? <ObjectName name={item.name} type={item.type} /> : item[field.value]}
                </td>
            ))}
        </tr>
    );
};
