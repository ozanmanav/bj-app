export function getDataForExport(fields: string[], data: any[]) {
    const exportData: any[] = [];

    data.forEach((item) => {
        exportData.push(
            fields.reduce(
                (acc, field) => ({
                    ...acc,
                    [field]: item[field],
                }),
                {}
            )
        );
    });

    return exportData;
}
