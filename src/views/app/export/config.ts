export type TExportFormat = "xls" | "csv";

export const EXPORT_FORMATS_LIST: { value: TExportFormat }[] = [
    {
        value: "xls",
    },
    {
        value: "csv",
    },
];
