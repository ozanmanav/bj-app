import React, { ChangeEvent, FunctionComponent, useState } from "react";
import "./Export.scss";
import { Container, Select, ExportButton, SearchInput, RemoveObjectButton } from "../../../components/ui";
import { ExportTable } from "./exportTable";
import { EXPORT_FORMATS_LIST, TExportFormat } from "./config";
import { getDataForExport } from "./utils";
import XlsExport from "xlsexport/xls-export.js";

const data = [
    {
        name: "Laurenzenvorstadt",
        type: "street",
        created: "30.12.2018",
        serial: "123456778",
    },
    {
        name: "Laurenzenvorstadt1",
        type: "street",
        created: "30.12.2018",
        serial: "123456778",
    },
    {
        name: "Building Zenex",
        type: "building",
        created: "30.12.2018",
        serial: "123456778",
    },
    {
        name: "Building Zenex1",
        type: "building",
        created: "30.12.2018",
        serial: "123456778",
    },
    {
        name: "Laurenzenvorstadt2",
        type: "street",
        created: "30.12.2018",
        serial: "123456778",
    },
];

const defaultExportFields: { value: string; label: string }[] = [
    {
        value: "name",
        label: "Object name",
    },
    {
        value: "created",
        label: "Object creation date",
    },
    {
        value: "serial",
        label: "Serial number",
    },
    {
        value: "events",
        label: "Events",
    },
];

export const Export: FunctionComponent = () => {
    const [format, setFormat] = useState<TExportFormat>();
    const [exportFields, setExportFields] = useState<{ value: string; label: string }[]>(defaultExportFields);

    function handleFormatChange(e: ChangeEvent<HTMLSelectElement>) {
        setFormat(e.target.value as TExportFormat);
    }

    function removeField(field: string) {
        setExportFields(exportFields.filter((f) => f.label !== field));
    }

    function exportData() {
        if (!format) {
            return;
        }

        const exportData = getDataForExport(exportFields.map((f) => f.value), data);
        const instance = new XlsExport(exportData);

        if (format === "xls") {
            instance.exportToXLS("export.xls");
        }

        if (format === "csv") {
            instance.exportToCSV("export.csv");
        }
    }

    return (
        <Container>
            <div className="b-export__header flex align-center justify-between">
                <h3>Please select the fields you'd like to export</h3>
                <div className="b-export__header-actions flex align-center">
                    <Select
                        options={EXPORT_FORMATS_LIST}
                        value={format}
                        onChange={handleFormatChange}
                        placeholder="Choose format"
                        marginBottom="none"
                        className="b-export__header-select"
                    />
                    <ExportButton text="Export" primary onClick={exportData} />
                </div>
            </div>
            <SearchInput placeholder="Search Fields, Files and Objects Associated with the Results of the Search..." marginBottom="sm" />
            <div className="flex align-center flex-wrap">
                {exportFields.map((field) => (
                    <RemoveFieldButton field={field.label} key={field.value} removeField={removeField} />
                ))}
            </div>
            <ExportTable fields={exportFields} data={data} />
        </Container>
    );
};

const RemoveFieldButton: FunctionComponent<{ field: string; removeField: (field: string) => void }> = ({ field, removeField }) => {
    function onClickSelf() {
        removeField(field);
    }

    return <RemoveObjectButton text={`${field}`} onClick={onClickSelf} className="b-export__remove-field" />;
};
