import React, { FunctionComponent, ReactText } from "react";
import { Formik } from "formik";
import { Select, Input, Checkbox, Row, Column, DatepickerSimple, RadioButtonsGroupMultiple, TextArea } from "../../../../../components/ui";
import {
    ITaskBasicInfoFormBaseProps,
    ITaskBasicInfoFormProps,
    TASK_BASIC_INFO_FORM_STATE,
    TaskBasicInfoFormValidationSchema,
    occurenceTypes,
    reccurences,
    weekdays,
} from "./definitions";
import "./TaskBasicInfoForm.scss";
import { FormFooterNext, FormAccordionContent, FormCaption } from "../../../../../components/forms";
import contains from "ramda/es/contains";
import get from "lodash.get";
import { parseDateForDatePicker, formatDate } from "../../../../../components/ui/datepicker/utils";
import { DATE_FORMAT } from "../../../../../config";

export const TaskBasicInfoFormBase: FunctionComponent<ITaskBasicInfoFormBaseProps> = ({
    values,
    handleSubmit,
    handleChange,
    errors,
    touched,
    setFieldValue,
}) => {
    const onChangeWeekDays = (value: ReactText) => {
        setFieldValue(
            "frequencyDays",
            contains(value, values.frequencyDays) ? values.frequencyDays.filter((e) => e !== value) : [...values.frequencyDays, value]
        );
    };

    const onChangeEndsOnCheckbox = (event: React.ChangeEvent<HTMLInputElement>) => {
        setFieldValue("endsOn", get(event, "currentTarget.name"));
    };

    const onChangeStartDate = (date: Date) => {
        setFieldValue(`dateStart`, formatDate(date, DATE_FORMAT));
    };

    const onChangeEndDate = (date: Date) => {
        setFieldValue(`dateEnd`, formatDate(date, DATE_FORMAT));
    };

    return (
        <form data-testid="TaskBasicInfoFormBase" className="f-task-basic-info" onSubmit={handleSubmit}>
            <FormAccordionContent>
                <Row gutter="sm">
                    <Column width={12}>
                        <Input
                            placeholder="Task name"
                            name="taskName"
                            data-testid="taskName"
                            onChange={handleChange}
                            value={values.taskName}
                            error={errors && errors.taskName}
                            touched={touched && touched.taskName}
                        />
                    </Column>
                </Row>
                <Row gutter="sm">
                    <Column width={12}>
                        <TextArea
                            placeholder="Task Description"
                            name="taskDescription"
                            onChange={handleChange}
                            value={values.taskDescription}
                            error={errors && errors.taskDescription}
                            touched={touched && touched.taskDescription}
                        />
                    </Column>
                </Row>
                <FormCaption>Date</FormCaption>
                <Row gutter="sm">
                    <Column>
                        <DatepickerSimple
                            onChange={onChangeStartDate}
                            defaultValue={values.dateStart ? parseDateForDatePicker(values.dateStart) : undefined}
                            className="_left"
                        />
                    </Column>
                    <Column>
                        <Select
                            options={occurenceTypes}
                            placeholder="Occurence type"
                            name="frequencyType"
                            onChange={handleChange}
                            value={values.frequencyType}
                            error={errors && errors.frequencyType}
                            touched={touched && touched.frequencyType}
                        />
                    </Column>
                </Row>

                {values.frequencyType === "custom" && (
                    <div>
                        <FormCaption>Custom recurrence</FormCaption>
                        <Row gutter="sm">
                            <Column className="flex align-center">
                                <span className="f-task-basic-info__repeat-title">Repeat every</span>
                                <Input
                                    className="f-task-basic-info__input"
                                    type="number"
                                    min={1}
                                    step={1}
                                    squared
                                    name="reccurenceInterval"
                                    onChange={handleChange}
                                    value={values.reccurenceInterval}
                                    error={errors && errors.reccurenceInterval}
                                    touched={touched && touched.reccurenceInterval}
                                />
                                <Select
                                    className="f-task-basic-info__reccurence"
                                    options={reccurences}
                                    placeholder="Recurrence type"
                                    name="reccurence"
                                    onChange={handleChange}
                                    value={values.reccurence}
                                    error={errors && errors.reccurence}
                                    touched={touched && touched.reccurence}
                                />
                            </Column>
                            {values.reccurence === "weekly" && (
                                <Column className="flex align-center">
                                    <span className="f-task-basic-info__repeat-title">Repeat on</span>
                                    <RadioButtonsGroupMultiple
                                        name="frequencyDays"
                                        inputs={weekdays}
                                        selectedValues={values.frequencyDays}
                                        onClickButton={onChangeWeekDays}
                                        wrapperClassName="f-task-basic-info__repeat-radio-group"
                                    />
                                </Column>
                            )}
                        </Row>
                    </div>
                )}

                <FormCaption className="f-task-basic-info__ends-title">Ends</FormCaption>
                <Checkbox label="Never" name="continuous" checked={values.endsOn === "continuous"} onChange={onChangeEndsOnCheckbox} />

                <Checkbox label="On" name="date" checked={values.endsOn === "date"} onChange={onChangeEndsOnCheckbox}>
                    <div className={values.endsOn !== "date" ? "f-task-basic-info__hidden" : ""}>
                        <DatepickerSimple
                            defaultValue={values.dateEnd ? parseDateForDatePicker(values.dateEnd) : undefined}
                            onChange={onChangeEndDate}
                        />
                    </div>
                </Checkbox>

                <Checkbox label="After" name="occurrences" checked={values.endsOn === "occurrences"} onChange={onChangeEndsOnCheckbox}>
                    <Input
                        className="f-task-basic-info__input"
                        disabled={values.endsOn !== "occurrences"}
                        type="number"
                        min={1}
                        step={1}
                        squared
                        marginBottom="none"
                        name="occurenceCount"
                        onChange={handleChange}
                        value={values.occurenceCount}
                        error={errors && errors.occurenceCount}
                        touched={touched && touched.occurenceCount}
                    />
                    <span className="_text-grey _text-uppercase f-task-basic-info__occurence">Occurrences</span>
                </Checkbox>
            </FormAccordionContent>
            <FormFooterNext />
        </form>
    );
};

export const TaskBasicInfoForm: FunctionComponent<ITaskBasicInfoFormProps> = ({ onSubmit, initialValues }) => {
    return (
        <Formik
            onSubmit={onSubmit}
            initialValues={initialValues || TASK_BASIC_INFO_FORM_STATE}
            validationSchema={TaskBasicInfoFormValidationSchema}
            component={(formikProps) => <TaskBasicInfoFormBase {...formikProps} />}
        />
    );
};
