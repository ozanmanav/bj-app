import { FormikProps } from "formik";
import * as Yup from "yup";
import { VALIDATION_ERRORS } from "../../../../../config";

export const TASK_BASIC_INFO_FORM_STATE = {
    taskName: "",
    taskDescription: "",
    frequencyType: "",
    frequencyDays: [],
    reccurence: "",
    occurenceCount: 1,
    reccurenceInterval: 1,
    dateStart: new Date().toISOString(),
    dateEnd: new Date().toISOString(),
    endsOn: "date",
};

export interface ITaskBasicInfoFormState {
    taskName: string;
    taskDescription: string;
    frequencyType: string;
    frequencyDays: number[];
    reccurence: string;
    occurenceCount: number;
    reccurenceInterval: number;
    dateStart: string;
    dateEnd: string;
    endsOn: string;
}

export interface ITaskBasicInfoFormBaseProps extends FormikProps<ITaskBasicInfoFormState> {}

export interface ITaskBasicInfoFormProps {
    onSubmit: (state: ITaskBasicInfoFormState) => void;
    initialValues?: ITaskBasicInfoFormState;
}

// Constants
export const weekdays = [
    {
        label: "Sun",
        value: 0,
    },
    {
        label: "Mon",
        value: 1,
    },
    {
        label: "Tue",
        value: 2,
    },
    {
        label: "Wed",
        value: 3,
    },
    {
        label: "Thu",
        value: 4,
    },
    {
        label: "Fri",
        value: 5,
    },
    {
        label: "Sat",
        value: 6,
    },
];

export const occurenceTypes = [
    {
        label: "Daily",
        value: "daily",
    },
    {
        label: "Weekly",
        value: "weekly",
    },
    {
        label: "Monthly",
        value: "monthly",
    },
    {
        label: "Yearly",
        value: "yearly",
    },
    {
        label: "Custom",
        value: "custom",
    },
];

export const reccurences = [
    {
        label: "Day",
        value: "daily",
    },
    {
        label: "Week",
        value: "weekly",
    },
    {
        label: "Month",
        value: "monthly",
    },
    {
        label: "Year",
        value: "yearly",
    },
];

// Validation Schema
export const TaskBasicInfoFormValidationSchema = Yup.object().shape({
    taskName: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),

    frequencyType: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
});
