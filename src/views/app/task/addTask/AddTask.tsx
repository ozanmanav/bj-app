import React, { FunctionComponent, useState, useEffect, useContext } from "react";
import { Accordion, useAccordions, Container, handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../../../components/ui";
import { useMutation, useQuery } from "@apollo/react-hooks";
import { TaskBasicInfoForm } from "./taskBasicInfoForm";
import { TaskAssignmentForm } from "./taskAssignmentForm";
import { TaskRelatedObjectsForm } from "./taskRelatedObjectsForm";
import { ITaskBasicInfoFormState, TASK_BASIC_INFO_FORM_STATE } from "./taskBasicInfoForm/definitions";
import { getArrayIDs } from "./utils";
import { ITaskAssignmentFormState, TASK_ASSIGNMENT_FORM_STATE } from "./taskAssignmentForm/definitions";
import { ITaskRelatedObjectsFormState, TASK_RELATED_OBJECTS_FORM_STATE } from "./taskRelatedObjectsForm/definitions";
import { ADD_TASK } from "./queries";
import { TaskFrequencyType } from "./definitions";
import { useHistory, useLocation } from "react-router";
import get from "lodash.get";
import { getLocationSearchParam } from "../../../../utils/locationSearchHelpers";
import { TaskRelatedTasksForm } from "./taskRelatedTasksForm";
import { ITaskRelatedTasksFormState, TASK_RELATED_TASKS_FORM_STATE } from "./taskRelatedTasksForm/definitions";
import { IEventVariant } from "../../../../components/events/EventsTimeline/definitions";
import { Loader } from "../../../../components/ui/loader";
import { TaskContentHeader } from "../../../../components/contentHeaders/taskContentHeader";
import EventsTimeline from "../../../../components/events/EventsTimeline";
import { CREATE_TASK_USER } from "../taskDetail/queries";
import { GET_TASK_USERS } from "../../../../components/tasks/queries";
import { AccountStateContext } from "../../../../contexts/accountStateContext";
import { GET_EVENTS } from "../../../../components/events/EventsTimeline/queries";

export const AddTask: FunctionComponent = () => {
    const { isOpened, openAccordion, closeAccordion, toggleAccordion } = useAccordions([1]);
    const { user } = useContext(AccountStateContext);
    const [title, setTitle] = useState<string | undefined>();
    const [loading, setLoading] = useState<boolean>(true);
    const [basicInfoFormState, setBasicInfoFormState] = useState<ITaskBasicInfoFormState>(TASK_BASIC_INFO_FORM_STATE);
    const [assignmentFormState, setAssignmentFormState] = useState<ITaskAssignmentFormState>(TASK_ASSIGNMENT_FORM_STATE);
    const [relatedEventsFormState, setRelatedEventsFormState] = useState<ITaskRelatedTasksFormState>(TASK_RELATED_TASKS_FORM_STATE);
    const [relatedObjectsFormState, setRelatedObjectsFormState] = useState<ITaskRelatedObjectsFormState>(TASK_RELATED_OBJECTS_FORM_STATE);

    const location = useLocation();
    const objectID = getLocationSearchParam(location.search, "obj");
    const organizationID = getLocationSearchParam(location.search, "org");

    const variant: IEventVariant = (objectID && "object") || (organizationID && "organization") || undefined;
    const variantID = objectID || organizationID || undefined;

    const { data: eventsData, refetch: eventsRefetch, loading: eventsLoading } = useQuery(GET_EVENTS, {
        variables: {
            ...(variant === "organization" && { organization_id: variantID }),
            ...(variant === "object" && { object_id: variantID }),
            strict: false,
        },
        fetchPolicy: "cache-first",
    });

    useEffect(() => {
        window.scrollTo(0, 0);

        setLoading(false);

        return () => {
            setTitle(undefined);
        };
    }, [location.search]);

    const history = useHistory();

    const [createTaskUserMutation] = useMutation(CREATE_TASK_USER);

    const [AddTaskMutation] = useMutation(ADD_TASK);
    const onAddTaskSubmit = async () => {
        try {
            const {
                taskName,
                taskDescription,
                dateStart,
                dateEnd,
                frequencyType,
                frequencyDays,
                endsOn,
                reccurence,
                reccurenceInterval,
                occurenceCount,
            } = basicInfoFormState;
            const { users, fileIDs } = assignmentFormState;
            const { objects } = relatedObjectsFormState;
            const { tasks } = relatedEventsFormState;

            const userIDs = getArrayIDs(users);
            const objectIDs = getArrayIDs(objects);
            const taskIDs = getArrayIDs(tasks);

            const frequency: TaskFrequencyType = {
                type: frequencyType !== "custom" ? frequencyType : reccurence,
                ...(frequencyType === "custom" && reccurence === "weekly" && { days: frequencyDays }),
                ...(frequencyType === "custom" && { interval: reccurenceInterval }),
                ...(endsOn === "occurrences" && { count: occurenceCount }),
            };

            const { data, errors } = await AddTaskMutation({
                variables: {
                    name: taskName,
                    description: taskDescription,
                    start_date: dateStart,
                    ...(endsOn === "date" && { end_date: dateEnd }),
                    frequency,
                    ...(objectID && { object_id: objectID }),
                    ends_on: endsOn,
                    file_ids: fileIDs,
                    related_task_ids: taskIDs,
                    related_object_ids: objectIDs,
                    related_organization_ids: userIDs,
                },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                showSuccessToast("Task successfully created");
                const taskID = get(data, "createTask.id");
                if (taskID) {
                    const createTaskUsersResponses = await Promise.all(
                        userIDs.map((userID) =>
                            createTaskUserMutation({
                                variables: {
                                    task_id: taskID,
                                    user_id: userID,
                                    status: "assigned",
                                },
                                refetchQueries: [
                                    {
                                        query: GET_TASK_USERS,
                                        variables: {
                                            user_id: get(user, "id"),
                                        },
                                    },
                                ],
                            })
                        )
                    );

                    let isSuccessful = true;

                    createTaskUsersResponses.forEach(({ errors }) => {
                        if (errors) {
                            isSuccessful = false;
                            handleGraphQLErrors(errors);
                        }
                    });

                    if (isSuccessful) {
                        showSuccessToast("Users successfully assigned to Task");
                        history.push(`/app/task/${taskID}`);
                    }
                }
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    };

    function onSubmitTaskBasicInfoForm(basicInfo: ITaskBasicInfoFormState) {
        setBasicInfoFormState(basicInfo);
        closeAccordion(1);
        openAccordion(2);
    }

    function onSubmitEventAssigmentForm(assigments: ITaskAssignmentFormState) {
        setAssignmentFormState(assigments);
        closeAccordion(2);
        openAccordion(3);
    }

    function onSubmitTaskRelatedTasksForm(relatedEvents: ITaskRelatedTasksFormState) {
        setRelatedEventsFormState(relatedEvents);
        closeAccordion(3);
        openAccordion(4);
    }

    async function onSubmitTaskRelatedObjectsForm(relatedObjects: ITaskRelatedObjectsFormState) {
        setRelatedObjectsFormState(relatedObjects);
    }

    if (loading) {
        return <Loader />;
    }

    return (
        <Container>
            <TaskContentHeader title={title} />

            <EventsTimeline
                showAddButton={false}
                variant={variant}
                variantID={variantID}
                events={eventsData}
                loading={eventsLoading}
                refetch={eventsRefetch}
            />

            <Accordion title="Basic Information" isOpen={isOpened(1)} toggle={toggleAccordion} index={1}>
                <TaskBasicInfoForm onSubmit={onSubmitTaskBasicInfoForm} />
            </Accordion>
            <Accordion
                title="Assignment"
                tooltip="Lorem ipsum"
                disabled={isOpened(1)}
                isOpen={isOpened(2)}
                toggle={toggleAccordion}
                index={2}
            >
                <TaskAssignmentForm onSubmit={onSubmitEventAssigmentForm} taskName={basicInfoFormState.taskName} />
            </Accordion>
            <Accordion title="Related Tasks" disabled={isOpened(1) || isOpened(2)} isOpen={isOpened(3)} toggle={toggleAccordion} index={3}>
                <TaskRelatedTasksForm onSubmit={onSubmitTaskRelatedTasksForm} />
            </Accordion>
            <Accordion
                title="Related Objects"
                disabled={isOpened(1) || isOpened(2) || isOpened(3)}
                isOpen={isOpened(4)}
                toggle={toggleAccordion}
                index={4}
            >
                <TaskRelatedObjectsForm
                    onSubmit={onSubmitTaskRelatedObjectsForm}
                    scopeObjectID={objectID}
                    scopeOrganizationID={organizationID}
                    onClickCreateButton={onAddTaskSubmit}
                    submitFormOnChange
                />
            </Accordion>
        </Container>
    );
};
