export type TaskFrequencyEnum = "none" | "daily" | "weekly" | "monthly" | "yearly" | string;

export type TaskFrequencyType = {
    type: TaskFrequencyEnum;
    count?: number;
    days?: number[];
    months?: number[];
};

export type TaskEndsOnEnum = "continuous" | "edited" | "date" | string;
