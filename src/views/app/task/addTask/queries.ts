import gql from "graphql-tag";

export const ADD_TASK = gql`
    mutation AddTask(
        $name: String!
        $description: String
        $start_date: String!
        $end_date: String
        $frequency: CreateTaskFrequencyType
        $object_id: String
        $ends_on: CreateTaskEndsOnEnum!
        $details: [CreateTaskDetailsField]
        $file_ids: [String]
        $related_task_ids: [String]
        $related_object_ids: [String]
    ) {
        createTask(
            name: $name
            description: $description
            start_date: $start_date
            end_date: $end_date
            frequency: $frequency
            object_id: $object_id
            ends_on: $ends_on
            details: $details
            file_ids: $file_ids
            related_task_ids: $related_task_ids
            related_object_ids: $related_object_ids
        ) {
            id
        }
    }
`;

export const GET_ORGANIZATION = gql`
    query GetOrganization($id: String!) {
        organizations(id: $id) {
            data {
                id
                name
            }
        }
    }
`;

export const GET_OBJECT = gql`
    query GetObject($id: String!) {
        objects(id: $id) {
            data {
                id
                name
            }
        }
    }
`;
