import { ILocalAutocompleteOption } from "../../../../../components/ui";
import { IEmployee } from "../../../../../config";
import get from "lodash.get";

export const normalizeOrganizationEmployees = (responses: any) => {
    const organizations = responses.map((res: any) => get(res, "data.organizations.data[0]")) || [];
    return organizations.reduce((acc: ILocalAutocompleteOption[], organization: any) => {
        organization.employees.forEach((employee: IEmployee) => {
            if (employee && employee.user) {
                let newUser: ILocalAutocompleteOption = {
                    id: employee.id,
                    label: `${employee.user.first_name} ${employee.user.last_name}`,
                    value: `${employee.user.first_name} ${employee.user.last_name}`,
                };

                acc.push(newUser);
            }
        });
        return acc;
    }, []);
};
