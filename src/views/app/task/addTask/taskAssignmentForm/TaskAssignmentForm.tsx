import React, { FunctionComponent, useEffect, useState, useContext } from "react";
import { Formik } from "formik";
import {
    ITaskAssignmentFormProps,
    TASK_ASSIGNMENT_FORM_STATE,
    ITaskAssignmentFormBaseProps,
    TaskAssignmentFormValidationSchema,
    GET_FILES,
    ITaskUserAssignmentObject,
} from "./definitions";
import "./TaskAssignmentForm.scss";

import {
    RemoveObjectButton,
    Row,
    Column,
    AutocompleteInput,
    handleGraphQLErrors,
    showSuccessToast,
    showErrorToast,
} from "../../../../../components/ui";
import { FormAccordionContent, FormFooterNext, FormCaption } from "../../../../../components/forms";
import { FilesList } from "../../../../../components/filesList";
import isEmpty from "ramda/es/isEmpty";
import { useApolloClient, useMutation } from "@apollo/react-hooks";
import get from "lodash.get";
import isNil from "ramda/es/isNil";
import useUpdateEffect from "../../../../../hooks/useUpdateEffect";
import { Loader } from "../../../../../components/ui/loader";
import { CREATE_TASK_USER, DELETE_TASK_USER } from "../../taskDetail/queries";
import { GET_TASK_USERS } from "../../../../../components/tasks/queries";
import { AccountStateContext } from "../../../../../contexts/accountStateContext";
import classNames from "classnames";

const TaskAssignmentFormBase: FunctionComponent<ITaskAssignmentFormBaseProps> = ({
    values,
    handleSubmit,
    setFieldValue,
    submitFormOnChange = false,
    taskID,
    taskName = "",
}) => {
    const { user: currentUser } = useContext(AccountStateContext);
    const [loading, setLoading] = useState(false);
    const [files, setFiles] = useState([]);
    const apolloClient = useApolloClient();

    useUpdateEffect(() => {
        if (submitFormOnChange) {
            handleSubmit();
        }
    }, [values]);

    const [createTaskUserMutation] = useMutation(CREATE_TASK_USER);
    const createTaskUser = async (userID: string) => {
        try {
            const { errors } = await createTaskUserMutation({
                variables: { task_id: taskID, user_id: userID, status: "assigned" },
                refetchQueries: [
                    {
                        query: GET_TASK_USERS,
                        variables: {
                            user_id: get(currentUser, "id"),
                        },
                    },
                ],
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                showSuccessToast("User successfully added to task.");
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    };

    const [deleteTaskUserMutation] = useMutation(DELETE_TASK_USER);
    const deleteTaskUser = async (taskUserID: string) => {
        try {
            const { errors } = await deleteTaskUserMutation({
                variables: { id: taskUserID },
                refetchQueries: [
                    {
                        query: GET_TASK_USERS,
                        variables: {
                            user_id: get(currentUser, "id"),
                        },
                    },
                ],
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                showSuccessToast("User successfully deleted from task.");
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    };

    useEffect(() => {
        const getFilesDetails = async (uploadedIDs: string[]) => {
            setLoading(true);
            const { data: filesDetailsData } = await apolloClient.query({
                query: GET_FILES,
                variables: {
                    ids: uploadedIDs,
                },
                fetchPolicy: "no-cache",
            });

            const filesDetails = get(filesDetailsData, "files.data");

            if (filesDetails) {
                await setFiles(filesDetails);
                setLoading(false);
            }
        };

        if (!isEmpty(values.fileIDs) && !isNil(values.fileIDs) && apolloClient) {
            getFilesDetails(values.fileIDs);
        } else {
            setFiles([]);
        }
    }, [values.fileIDs, apolloClient]);

    function removeUser(user: ITaskUserAssignmentObject) {
        const isCreatorUser = user.id === currentUser.id;

        if (isCreatorUser) {
            return showErrorToast("You can not delete creator of the task.");
        }

        if (taskID) {
            deleteTaskUser(user.taskUserID);
        }

        setFieldValue(
            "users",
            values.users.filter((item: ITaskUserAssignmentObject) => item.id !== user.id)
        );
    }

    function addUser(user: ITaskUserAssignmentObject) {
        if (!values.users.find((item) => item.id === user.id)) {
            if (taskID) {
                createTaskUser(user.id);
            }
            setFieldValue("users", [...values.users, user]);
        }
    }

    const filesChangeCallback = (uploadedFileIDs: string[] = []) => {
        setFieldValue("fileIDs", [...values.fileIDs, ...uploadedFileIDs]);
    };

    return (
        <form onSubmit={handleSubmit}>
            <FormAccordionContent className="f-task-assignment__top-row">
                <Row gutter="sm">
                    <Column>
                        <FormCaption>Assign to user</FormCaption>
                        <AutocompleteInput
                            searchFor="users"
                            onChange={addUser}
                            placeholder="Start typing to find user..."
                            marginBottom="sm"
                            isSetValue={false}
                        />
                        <div className="f-task-assignment__remove-button flex align-center flex-wrap">
                            {values.users.map((userItem) => {
                                const isCreatorUser = userItem.id === currentUser.id;

                                return (
                                    <RemoveObjectButton
                                        className={classNames("f-task-assignment__remove-button")}
                                        disabled={isCreatorUser}
                                        text={userItem.name}
                                        type="button"
                                        onClick={() => removeUser(userItem)}
                                        key={userItem.id}
                                    />
                                );
                            })}
                        </div>
                    </Column>
                </Row>
            </FormAccordionContent>
            <FormAccordionContent>
                {loading ? (
                    <div className="flex justify-center">
                        <Loader rawLoader />
                    </div>
                ) : (
                    <FilesList addFilesTo={taskName} files={files} hideFilter filesChangeCallback={filesChangeCallback} loading={loading} />
                )}
            </FormAccordionContent>
            {!submitFormOnChange && <FormFooterNext />}
        </form>
    );
};

export const TaskAssignmentForm: FunctionComponent<ITaskAssignmentFormProps> = ({
    onSubmit,
    initialValues,
    submitFormOnChange,
    taskID,
    taskName,
}) => {
    return (
        <Formik
            onSubmit={onSubmit}
            initialValues={initialValues || TASK_ASSIGNMENT_FORM_STATE}
            validationSchema={TaskAssignmentFormValidationSchema}
            component={(formikProps) => (
                <TaskAssignmentFormBase {...formikProps} taskID={taskID} taskName={taskName} submitFormOnChange={submitFormOnChange} />
            )}
        />
    );
};
