import { FormikProps } from "formik";
import * as Yup from "yup";

export interface IAssignmentObject {
    name: string;
    id: string;
}

export const TASK_RELATED_TASKS_FORM_STATE = {
    tasks: [],
};

export interface ITaskRelatedTasksFormState {
    tasks: IAssignmentObject[];
}

export interface ITaskRelatedTasksFormBaseProps extends FormikProps<ITaskRelatedTasksFormState> {
    submitFormOnChange?: boolean;
}

export interface ITaskRelatedTasksFormProps {
    onSubmit: (state: ITaskRelatedTasksFormState) => void;
    initialValues?: ITaskRelatedTasksFormState;
    submitFormOnChange?: boolean;
}

export const TaskRelatedTasksFormValidationSchema = Yup.object().shape({
    tasks: Yup.array(),
});
