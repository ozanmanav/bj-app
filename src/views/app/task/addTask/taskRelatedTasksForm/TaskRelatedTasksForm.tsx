import React, { FunctionComponent } from "react";
import { Formik } from "formik";
import {
    ITaskRelatedTasksFormProps,
    TASK_RELATED_TASKS_FORM_STATE,
    ITaskRelatedTasksFormBaseProps,
    IAssignmentObject,
} from "./definitions";
import "./TaskRelatedTasksForm.scss";
import { RemoveObjectButton, Row, Column, AutocompleteInput } from "../../../../../components/ui";
import { FormAccordionContent, FormFooterNext, FormCaption } from "../../../../../components/forms";
import useUpdateEffect from "../../../../../hooks/useUpdateEffect";

const TaskRelatedTasksFormBase: FunctionComponent<ITaskRelatedTasksFormBaseProps> = ({
    values,
    handleSubmit,
    setFieldValue,
    submitFormOnChange,
}) => {
    useUpdateEffect(() => {
        if (submitFormOnChange) {
            handleSubmit();
        }
    }, [values]);

    function removeTask(task: IAssignmentObject) {
        setFieldValue(
            "tasks",
            values.tasks.filter((item: IAssignmentObject) => item.id !== task.id)
        );
    }

    function addTask(task: IAssignmentObject) {
        if (!values.tasks.find((item) => item.id === task.id)) {
            setFieldValue("tasks", [...values.tasks, task]);
        }
    }

    return (
        <form onSubmit={handleSubmit}>
            <FormAccordionContent className="f-task-assignment__top-row">
                <Row gutter="sm">
                    <Column>
                        <FormCaption>Assign to tasks</FormCaption>
                        <AutocompleteInput
                            searchFor="tasks"
                            onChange={addTask}
                            placeholder="Start typing to find tasks..."
                            marginBottom="sm"
                            isSetValue={false}
                        />
                        <div className="f-task-assignment__remove-button flex align-center flex-wrap">
                            {values.tasks.map((task) => (
                                <RemoveObjectButton
                                    className="f-task-assignment__remove-button"
                                    text={task.name}
                                    type="button"
                                    onClick={() => removeTask(task)}
                                    key={task.id}
                                />
                            ))}
                        </div>
                    </Column>
                </Row>
            </FormAccordionContent>
            {!submitFormOnChange && <FormFooterNext />}
        </form>
    );
};

export const TaskRelatedTasksForm: FunctionComponent<ITaskRelatedTasksFormProps> = ({
    onSubmit,
    initialValues,
    submitFormOnChange,
}) => {
    return (
        <Formik
            onSubmit={onSubmit}
            initialValues={initialValues || TASK_RELATED_TASKS_FORM_STATE}
            // validationSchema={TaskRelatedTasksFormValidationSchema}
            component={(formikProps) => (
                <TaskRelatedTasksFormBase {...formikProps} submitFormOnChange={submitFormOnChange} />
            )}
        />
    );
};
