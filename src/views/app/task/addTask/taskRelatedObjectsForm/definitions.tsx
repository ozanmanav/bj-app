import { FormikProps } from "formik";
import * as Yup from "yup";

export interface IRelatedObject {
    id: string;
    name: string;
    address: string;
    type: string;
}

export const TASK_RELATED_OBJECTS_FORM_STATE = {
    objects: [],
};

export interface ITaskRelatedObjectsFormState {
    objects: IRelatedObject[];
}

export interface ITaskRelatedObjectsFormBaseProps extends FormikProps<ITaskRelatedObjectsFormState> {
    showRelatedObjectsTable?: boolean;
    onClickCreateButton?: () => void;
    submitFormOnChange?: boolean;
    scopeObjectID?: string;
    scopeOrganizationID?: string;
}

export interface ITaskRelatedObjectsFormProps {
    showRelatedObjectsTable?: boolean;
    onSubmit: (state: ITaskRelatedObjectsFormState) => void;
    onClickCreateButton?: () => void;
    initialValues?: ITaskRelatedObjectsFormState;
    submitFormOnChange?: boolean;
    scopeObjectID?: string;
    scopeOrganizationID?: string;
}

export const parentObj = [
    {
        title: "Laurenzenvorstadt",
        name: "Street",
        type: "street",
    },
    {
        title: "Building Zenex",
        name: "BuildingDetails",
        type: "building",
    },
];

export const siblingObj = [
    {
        title: "Appartment 2",
        name: "Appartment",
        type: "building",
    },
];

export const TaskRelatedObjectsFormValidationSchema = Yup.object().shape({
    objects: Yup.array(),
});
