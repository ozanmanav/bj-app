import React, { FunctionComponent } from "react";
import { AutocompleteInput, RemoveObjectButton, Column, Row, SaveButton } from "../../../../../components/ui";
import { Formik } from "formik";
import {
    ITaskRelatedObjectsFormProps,
    TASK_RELATED_OBJECTS_FORM_STATE,
    ITaskRelatedObjectsFormBaseProps,
    IRelatedObject,
} from "./definitions";
import { FormAccordionContent, FormCaption, FormFooterSave } from "../../../../../components/forms";
import "./TaskRelatedObjectsForm.scss";
import useUpdateEffect from "../../../../../hooks/useUpdateEffect";

const TaskRelatedObjectsFormBase: FunctionComponent<ITaskRelatedObjectsFormBaseProps> = ({
    submitFormOnChange,
    handleSubmit,
    values,
    setFieldValue,
    onClickCreateButton,
    scopeOrganizationID,
}) => {
    useUpdateEffect(() => {
        if (submitFormOnChange) {
            handleSubmit();
        }
    }, [values]);

    function addObject(object: IRelatedObject) {
        if (!values.objects.find((item) => item.id === object.id)) {
            setFieldValue("objects", [...values.objects, object]);
        }
    }

    function removeObject(object: IRelatedObject) {
        setFieldValue(
            "objects",
            values.objects.filter((item: IRelatedObject) => item.id !== object.id)
        );
    }

    return (
        <form onSubmit={handleSubmit}>
            <FormAccordionContent className="f-task-related">
                <Row gutter="sm">
                    <Column>
                        <FormCaption>Assign to Object</FormCaption>
                        <AutocompleteInput
                            searchFor="objects"
                            className="f-task-related__objects"
                            organizationID={scopeOrganizationID ? scopeOrganizationID : undefined}
                            placeholder="Search objects.."
                            onChange={addObject}
                            name="objects"
                            marginBottom="none"
                            isSetValue={false}
                        />
                        <div className="flex align-center flex-wrap f-task-related button-group">
                            {values.objects.map((object) => (
                                <RemoveObjectButton
                                    text={object.name}
                                    type="button"
                                    onClick={() => removeObject(object)}
                                    key={object.name}
                                />
                            ))}
                        </div>
                    </Column>
                </Row>
            </FormAccordionContent>
            {!submitFormOnChange && <FormFooterSave />}

            {submitFormOnChange && onClickCreateButton && (
                <footer className="f-accordion__footer">
                    <SaveButton onClick={onClickCreateButton} />
                </footer>
            )}
        </form>
    );
};

export const TaskRelatedObjectsForm: FunctionComponent<ITaskRelatedObjectsFormProps> = ({
    onSubmit,
    onClickCreateButton,
    initialValues,
    showRelatedObjectsTable,
    submitFormOnChange,
    scopeOrganizationID,
    scopeObjectID,
}) => {
    return (
        <Formik
            onSubmit={onSubmit}
            initialValues={initialValues || TASK_RELATED_OBJECTS_FORM_STATE}
            // validationSchema={TaskRelatedObjectsFormValidationSchema}
            component={(formikProps) => (
                <TaskRelatedObjectsFormBase
                    onClickCreateButton={onClickCreateButton}
                    showRelatedObjectsTable={showRelatedObjectsTable}
                    submitFormOnChange={submitFormOnChange}
                    scopeOrganizationID={scopeOrganizationID}
                    scopeObjectID={scopeObjectID}
                    {...formikProps}
                />
            )}
        />
    );
};
