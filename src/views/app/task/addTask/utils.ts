import get from "lodash.get";
import { ISelectOption } from "../../../../components/ui";
import { IEventType } from "../../../../config/types/EventType";

export const normalizeEventTypes = (eventTypesData: any): ISelectOption[] => {
    const pureEventTypes = get(eventTypesData, "event_types") || [];

    return pureEventTypes.map((eventType: IEventType) => ({
        label: eventType.name,
        value: eventType.id,
        group: get(eventType.groups, "[0].name"),
    }));
};

export const getArrayIDs = (array: any): string[] => {
    return array ? array.map((data: any) => data && get(data, "id")) : [];
};
