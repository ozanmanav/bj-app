import gql from "graphql-tag";

export const GET_TASK = gql`
    query GetTask($id: String!) {
        tasks(id: $id) {
            data {
                id
                name
                frequency {
                    type
                    days
                    months
                }
                ends_on
                start_date
                end_date
                deadline
                file_ids
                related_task_ids
                related_tasks {
                    id
                    name
                }
                task_users {
                    id
                    user {
                        id
                        name
                    }
                }
                related_object_ids
                related_objects {
                    id
                    name
                    type {
                        id
                        name
                        type
                    }
                    address
                    contracts {
                        id
                        role {
                            name
                        }
                    }
                }
            }
        }
    }
`;

export const UPDATE_TASK = gql`
    mutation UpdateTask(
        $id: String!
        $name: String
        $description: String
        $start_date: String
        $end_date: String
        $frequency: UpdateTaskFrequencyType
        $object_id: String
        $ends_on: UpdateTaskEndsOnEnum
        $details: [UpdateTaskDetailsField]
        $file_ids: [String]
        $related_task_ids: [String]
        $related_object_ids: [String]
        $verified: Boolean
    ) {
        updateTask(
            id: $id
            name: $name
            description: $description
            start_date: $start_date
            end_date: $end_date
            frequency: $frequency
            object_id: $object_id
            ends_on: $ends_on
            details: $details
            file_ids: $file_ids
            related_task_ids: $related_task_ids
            related_object_ids: $related_object_ids
            verified: $verified
        ) {
            id
            name
        }
    }
`;

export const CREATE_TASK_USER = gql`
    mutation CreateTaskUser($task_id: String!, $user_id: String!, $status: CreateTaskStatusEnum!, $details: [CreateTaskCustomField]) {
        createTaskUser(task_id: $task_id, user_id: $user_id, status: $status, details: $details) {
            id
        }
    }
`;

export const DELETE_TASK_USER = gql`
    mutation DeleteTaskUser($id: String!) {
        deleteTaskUser(id: $id)
    }
`;
