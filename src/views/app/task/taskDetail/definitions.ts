import { IAssignmentObject } from "../addTask/taskAssignmentForm/definitions";
import { IRelatedObjectsTableObject } from "../../../../components/tables";

export type ITaskUserStatus = "assigned" | "completed";

export type ITaskEndsOn = "continuous" | "date" | "occurrences";

export type ITaskFrequencyType = "none" | "daily" | "weekly" | "monthly" | "yearly";

export interface ITaskFrequency {
    type: ITaskFrequencyType;
    days: number[];
    months: number[];
}

export interface ITaskUser {
    id: string;
    status: ITaskUserStatus;
    task?: ITask;
}

export interface ITask {
    id: string;
    name: string;
    description: string;
    frequency: ITaskFrequency;
    meta: {
        author: {
            name: string;
        };
    };
    ends_on: ITaskEndsOn;
    start_date: string;
    end_date: string;
    deadline: string;
    file_ids: string[];
    task_users: {
        id: string;
        task_id: string;
        user_id: string;
    }[];
    related_object_ids: string[];
    related_objects: IRelatedObjectsTableObject[];
    related_task_ids: string[];
    related_tasks: IAssignmentObject[];
}

export interface IUpdateTask {
    name?: string;
    description?: string;
    frequency?: {
        type: string;
        days: number[];
        months: number[];
    };
    ends_on?: string;
    start_date?: string;
    end_date?: string;
    file_ids?: string[];
    related_object_ids?: string[];
    related_task_ids?: string[];
}
