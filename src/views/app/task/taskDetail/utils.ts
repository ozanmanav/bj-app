import get from "lodash.get";
import { ITaskUserAssignmentObject } from "../addTask/taskAssignmentForm/definitions";

export const normalizeTaskUsers = (task_users: any = []): ITaskUserAssignmentObject[] => {
    return task_users.map((item: any) => {
        return {
            id: get(item, "user.id"),
            name: get(item, "user.name"),
            taskUserID: get(item, "id"),
        };
    });
};

export const getArrayIDs = (array: any): string[] => {
    return array ? array.map((data: any) => data && get(data, "id")) : [];
};

export const getArrayIDsWithCustom = (array: any, fieldName: string): string[] => {
    return array ? array.map((data: any) => data && get(data, fieldName)) : [];
};
