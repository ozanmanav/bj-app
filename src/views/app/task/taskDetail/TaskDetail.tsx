import React, { FunctionComponent, useEffect, useState } from "react";
import {
    Container,
    Accordion,
    handleGraphQLErrors,
    showSuccessToast,
    showErrorToast,
    useModal,
    EditButton,
} from "../../../../components/ui";
import { TaskHeader } from "./taskHeader";
import { useRouteMatch } from "react-router";
import get from "lodash.get";
import isNil from "ramda/es/isNil";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { GET_TASK, UPDATE_TASK } from "./queries";
import { Loader } from "../../../../components/ui/loader";
import { TaskAssignmentForm } from "../addTask/taskAssignmentForm";
import { ITask, IUpdateTask } from "./definitions";
import { ITaskAssignmentFormState, TASK_ASSIGNMENT_FORM_STATE } from "../addTask/taskAssignmentForm/definitions";
import { ITaskRelatedTasksFormState, TASK_RELATED_TASKS_FORM_STATE } from "../addTask/taskRelatedTasksForm/definitions";
import { getArrayIDs } from "../addTask/utils";
import { TaskRelatedTasksForm } from "../addTask/taskRelatedTasksForm";
import useUpdateEffect from "../../../../hooks/useUpdateEffect";
import { IEventVariant } from "../../../../components/events/EventsTimeline/definitions";
import { RelatedObjectsTable } from "../../../../components/tables";
import { FormAccordionContent } from "../../../../components/forms";
import "./TaskDetail.scss";
import { ITaskRelatedObjectsFormState, TASK_RELATED_OBJECTS_FORM_STATE } from "../addTask/taskRelatedObjectsForm/definitions";
import { isNillOrEmpty } from "../../../../utils";
import EventsTimeline from "../../../../components/events/EventsTimeline";
import { normalizeTaskUsers } from "./utils";
import difference from "lodash.difference";
import { AddRelatedObjectToTaskModal } from "../../../../components/modals/addRelatedObjectToEventModal copy";
import { GET_EVENTS } from "../../../../components/events/EventsTimeline/queries";

export const TaskDetail: FunctionComponent = () => {
    const { isOpen: isOpenAddRelatedObjects, open: openAddRelatedObjects, hide: hideAddRelatedObjects } = useModal();
    const match = useRouteMatch();
    const taskID = get(match, "params.id");

    const [assignmentFormState, setAssignmentFormState] = useState<ITaskAssignmentFormState>(TASK_ASSIGNMENT_FORM_STATE);
    const [relatedEventsFormState, setRelatedEventsFormState] = useState<ITaskRelatedTasksFormState>(TASK_RELATED_TASKS_FORM_STATE);
    const [relatedObjectsFormState, setRelatedObjectsFormState] = useState<ITaskRelatedObjectsFormState>(TASK_RELATED_OBJECTS_FORM_STATE);

    const { data: taskDetailData, loading: taskDetailLoading, refetch: refetchTaskDetail } = useQuery(GET_TASK, {
        variables: {
            id: taskID,
        },
    });

    const taskDetail = get(taskDetailData, "tasks.data[0]") as ITask;
    const taskName = get(taskDetail, "name");

    const objectID = get(taskDetail, "object_id");
    const organizationID = get(taskDetail, "organization_id");

    const variant: IEventVariant = (objectID && "object") || (organizationID && "organization");
    const variantID = objectID || organizationID;

    const { data: eventsData, refetch: eventsRefetch, loading: eventsLoading } = useQuery(GET_EVENTS, {
        variables: {
            // ...(variant === "organization" && { organization_id: variantID }),
            ...(variant === "object" && { object_id: variantID }),
            strict: false,
        },
        fetchPolicy: "cache-first",
    });

    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    const [updateEventMutation] = useMutation(UPDATE_TASK);
    const updateTask = async (updatedVariables: IUpdateTask) => {
        try {
            const { errors } = await updateEventMutation({
                variables: { id: taskID, ...updatedVariables },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                showSuccessToast("Task successfully updated");
                refetchTaskDetail();
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    };

    useUpdateEffect(() => {
        const { fileIDs = [] } = assignmentFormState;

        if (!isNillOrEmpty(difference(fileIDs, get(taskDetail, "file_ids")))) {
            updateTask({
                file_ids: fileIDs,
            });
        }
    }, [assignmentFormState]);

    useUpdateEffect(() => {
        const { tasks } = relatedEventsFormState;

        const taskIDs = getArrayIDs(tasks);

        updateTask({ related_task_ids: taskIDs });
    }, [relatedEventsFormState]);

    useUpdateEffect(() => {
        const { objects } = relatedObjectsFormState;

        const objectsIDs = getArrayIDs(objects);

        updateTask({ related_object_ids: objectsIDs });
    }, [relatedObjectsFormState]);

    const onSubmitAddRelatedObjects = (values: ITaskRelatedObjectsFormState) => {
        setRelatedObjectsFormState(values);
    };

    if (taskDetailLoading) {
        return <Loader />;
    }

    if (isNil(taskDetail)) {
        return <div>Task not found.</div>;
    }

    const { related_objects = [], file_ids = [], related_tasks = [], task_users = [] } = taskDetail;

    return (
        <Container className="b-event-detail">
            <TaskHeader task={taskDetail} />
            <EventsTimeline
                showAddButton={false}
                variant={variant}
                variantID={variantID}
                events={eventsData}
                loading={eventsLoading}
                refetch={eventsRefetch}
            />
            <Accordion title="Assignment" tooltip="Lorem ipsum" isOpen={true} index={1} hideArrow>
                <TaskAssignmentForm
                    onSubmit={setAssignmentFormState}
                    submitFormOnChange
                    taskID={taskID}
                    taskName={taskName}
                    initialValues={{
                        users: normalizeTaskUsers(task_users),
                        fileIDs: file_ids,
                    }}
                />
            </Accordion>
            <Accordion title="Relationships" isOpen={true} index={2} hideArrow>
                <TaskRelatedTasksForm onSubmit={setRelatedEventsFormState} initialValues={{ tasks: related_tasks }} submitFormOnChange />
                <FormAccordionContent>
                    <div className="b-event-detail__title-container">
                        <h3>Related Objects</h3>
                        <EditButton onClick={openAddRelatedObjects} />
                    </div>
                    {!isNillOrEmpty(related_objects) ? (
                        <RelatedObjectsTable showRoleColumn={false} relatedObjects={related_objects} readOnly />
                    ) : (
                        <div>No related objects added.</div>
                    )}
                </FormAccordionContent>
            </Accordion>
            <AddRelatedObjectToTaskModal
                relatedObjects={related_objects}
                hide={hideAddRelatedObjects}
                isOpen={isOpenAddRelatedObjects}
                refetch={onSubmitAddRelatedObjects}
            />
        </Container>
    );
};
