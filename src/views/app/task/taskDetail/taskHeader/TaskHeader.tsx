import React, { FunctionComponent } from "react";
import "./TaskHeader.scss";
import { Icon } from "../../../../../components/ui";
import { ITask } from "../definitions";
import { parseAndFormatDate, getFruquencyText } from "./utils";

interface ITaskHeaderProps {
    task: ITask;
}

export const TaskHeader: FunctionComponent<ITaskHeaderProps> = ({ task: { name, start_date, frequency } }) => {
    return (
        <div className="b-task-header">
            <div className="flex align-center b-task-header__top-row">
                <Icon icon="commissioning" className="b-task-header__task-icon" />
                <h2 className="h1">{name}</h2>
            </div>
            <div className="flex align-center">
                <Icon icon="clocksGrey" className="b-task-header__expire-icon" />
                <p className="h3">
                    {parseAndFormatDate(start_date)} <span className="_text-grey b-task-header__expire-separator">●</span>{" "}
                    {getFruquencyText(frequency)}
                </p>
            </div>
        </div>
    );
};
