import React, { FunctionComponent } from "react";
import { Column, Container, Row } from "../../../../components/ui";
import "./AdminTypesControl.scss";
import { AdminTypesTable } from "../../../../components/tables/adminTypesTable";

export const AdminTypesControl: FunctionComponent = () => {
    return (
        <div className="b-admin-types-control">
            <Container>
                <Row className="b-admin-types-control__row" gutter="sm">
                    <Column>
                        <AdminTypesTable entityType="object" />
                    </Column>
                    <Column>
                        <AdminTypesTable entityType="organization" />
                    </Column>
                </Row>
                <Row className="b-admin-types-control__row" gutter="sm">
                    <Column>
                        <AdminTypesTable entityType="contract" />
                    </Column>
                    <Column>
                        <AdminTypesTable entityType="file" />
                    </Column>
                </Row>
                <Row className="b-admin-types-control__row" gutter="sm">
                    <Column>
                        <AdminTypesTable entityType="event" />
                    </Column>
                    <Column>
                        <AdminTypesTable entityType="job_title" />
                    </Column>
                </Row>
                <Row className="b-admin-types-control__row" gutter="sm">
                    <Column>
                        <AdminTypesTable entityType="relationship" />
                    </Column>
                </Row>
            </Container>
        </div>
    );
};
