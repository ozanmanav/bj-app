import React, { FunctionComponent } from "react";
import "./AdminPanel.scss";
import { Redirect, Route, Switch } from "react-router";
import { AdminOrganizations } from "./adminOrganizations";
import { AdminTypesControl } from "./adminTypesControl";
import { AdminSettings } from "./adminSettings";
import { withRoutingGuard } from "../../../components/hocs/withRoutingGuard";
import { AdminOrganization } from "./adminOrganization";

const AdminPanelBase: FunctionComponent = () => (
    <Switch>
        <Route exact path="/app/admin/" component={AdminPanelRedirect} />
        <Route exact path="/app/admin/organizations" component={AdminOrganizations} />
        <Route path="/app/admin/types-control" component={AdminTypesControl} />
        <Route path={["/app/admin/organizations/:id", "/app/admin/organizations/client/:id"]} component={AdminOrganization} />
        <Route path="/app/admin/settings" component={AdminSettings} />
    </Switch>
);

export const AdminPanel = withRoutingGuard(AdminPanelBase, { availableForAdminRoles: ["admin", "master", "datamanager"] });

const AdminPanelRedirect: FunctionComponent = () => <Redirect to="/app/admin/organizations" />;
