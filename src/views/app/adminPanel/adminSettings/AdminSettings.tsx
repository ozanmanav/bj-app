import React, { FunctionComponent } from "react";
import "./AdminSettings.scss";
import { Container } from "../../../../components/ui/grid";
import { SettingsItem } from "../../../../components/settings/settingsItem";
import { SettingsItemTitle } from "../../../../components/settings/settingsItemTitle";
import { PlusButton } from "../../../../components/ui/buttons";
import { useModal } from "../../../../components/ui/modal";
import { NotificationsTable } from "../../../../components/tables/notificationsTable";
import { useQuery } from "@apollo/react-hooks";
import get from "lodash.get";
import gql from "graphql-tag";
import { AdminRelatedUsersTable } from "../../../../components/tables/adminRelatedUsersTable";
import { AdminUserModal } from "../../../../components/modals/adminUserModal";

const notifications = [
    {
        information: "Lorem Ipsum",
        description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered.",
    },
    {
        information: "Variations of Passages",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    },
    {
        information: "First line",
        description: "To generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always from.",
    },
];

const PLATFORM_OWNERS = gql`
    query PlatformOwners {
        users(is_admin: true) {
            data {
                id
                email
                first_name
                last_name
                type
                phone
            }
        }
    }
`;

export const AdminSettings: FunctionComponent = () => {
    const { open: openAddUserModal, hide: hideAddUserModal, isOpen: isOpenOpenAddUserModal } = useModal();

    const { data, refetch } = useQuery(PLATFORM_OWNERS);

    const relatedUsers = get(data, "users.data") || [];

    return (
        <div className="b-admin-settings">
            <Container>
                <SettingsItem>
                    <SettingsItemTitle>
                        <h3>
                            Related Users <PlusButton onClick={openAddUserModal} />
                        </h3>
                    </SettingsItemTitle>
                    {relatedUsers.length > 0 && <AdminRelatedUsersTable refetch={refetch} users={relatedUsers} />}
                </SettingsItem>
                <SettingsItem>
                    <SettingsItemTitle>
                        <h3>Notifications</h3>
                    </SettingsItemTitle>
                    <NotificationsTable data={notifications} />
                </SettingsItem>
            </Container>
            <AdminUserModal hide={hideAddUserModal} isOpen={isOpenOpenAddUserModal} refetch={refetch} />
        </div>
    );
};
