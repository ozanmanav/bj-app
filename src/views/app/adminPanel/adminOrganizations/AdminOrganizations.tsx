import React, { ChangeEvent, FunctionComponent, useState } from "react";
import { withRouter, RouteComponentProps } from "react-router";
import get from "lodash.get";
import { useQuery } from "@apollo/react-hooks";
import { useDebounce } from "use-debounce";
import { Container, SearchInput, PlusButton, useModal, CheckboxSwitch } from "../../../../components/ui";
import { AdminOrganizationsTable } from "../../../../components/tables/adminCompaniesTable";
import { AUTOCOMPLETE_DEBOUNCE_TIME_MS } from "../../../../components/ui/inputs/config";
import { GRAPHQL_ADMIN_CONTEXT } from "../../../../config";
import { IAdminTableOrganizationData } from "../../../../components/tables/adminCompaniesTable/definitions";
import "./AdminOrganizations.scss";
import { AddNonClientOrganizationModal } from "../../../../components/modals/addNonClientOrganizationModal";
import { normalizeOrganizations, getOrganizationsQueryAndLocation } from "./utils";
import { isNillOrEmpty } from "../../../../utils";

const AdminOrganizationsBase: FunctionComponent<RouteComponentProps> = ({ history }) => {
    const [showClientOrganizations, setShowClientOrganizations] = useState<boolean>(true);
    const [showNonClientOrganizations, setShowNonClientOrganizations] = useState<boolean>(true);

    const [searchParam, setSearchParam] = useState("");
    const [debouncedSearchParam] = useDebounce(searchParam, AUTOCOMPLETE_DEBOUNCE_TIME_MS);

    const organizationsQueryAndLocation = getOrganizationsQueryAndLocation(!isNillOrEmpty(searchParam));

    const { data: organizationsData, refetch: organizationsRefetch, loading: organizationsLoading } = useQuery(
        organizationsQueryAndLocation.query,
        {
            variables: { ...(!isNillOrEmpty(searchParam) && { search: debouncedSearchParam }) },
            fetchPolicy: "network-only",
            ...GRAPHQL_ADMIN_CONTEXT,
        }
    );

    const organizations = normalizeOrganizations(get(organizationsData, organizationsQueryAndLocation.location)) || [];

    const allOrganizations: IAdminTableOrganizationData[] = [
        ...(showClientOrganizations ? organizations.filter((item) => item.isClient) : []),
        ...(showNonClientOrganizations ? organizations.filter((item) => !item.isClient) : []),
    ];

    const {
        open: openAddNonClientOrganizationsModal,
        isOpen: isOpenAddNonClientOrganizationsModal,
        hide: hideAddNonClientOrganizationsModal,
    } = useModal();

    function handleNameChange(e: ChangeEvent<HTMLInputElement>) {
        setSearchParam(e.target.value);
    }

    function onAddNonClientOrganization(nonClientOrganization?: { id: string; name: string }) {
        if (nonClientOrganization) {
            organizationsRefetch();
            history.push(`/app/admin/organizations/${nonClientOrganization.id}`);
        }
    }

    function toggleClientOrganizations() {
        setShowClientOrganizations((prevState) => !prevState);
    }

    function toggleOrganizations() {
        setShowNonClientOrganizations((prevState) => !prevState);
    }

    return (
        <div className="b-admin-organizations">
            <Container>
                <div className="b-admin-organizations__header flex align-center justify-between">
                    <h3 className="b-admin-organizations__title">
                        Organizations list
                        <PlusButton onClick={openAddNonClientOrganizationsModal} />
                    </h3>
                    <SearchInput
                        placeholder="Search organization by the name or email of it’s users..."
                        marginBottom="none"
                        value={searchParam}
                        onChange={handleNameChange}
                        className="b-admin-companies__search"
                    />
                </div>
                <div className="b-admin-organizations__filter-switches flex align-center">
                    <label className="b-admin-organizations__filter-switch flex align-center _cursor-pointer">
                        <span className="b-admin-organizations__filter-switch-label">Clients</span>
                        <CheckboxSwitch checked={showClientOrganizations} onChange={toggleClientOrganizations} />
                    </label>
                    <label className="b-admin-organizations__filter-switch flex align-center _cursor-pointer">
                        <span className="b-admin-organizations__filter-switch-label">Non-Clients</span>
                        <CheckboxSwitch checked={showNonClientOrganizations} onChange={toggleOrganizations} />
                    </label>
                </div>

                <AdminOrganizationsTable organizations={allOrganizations} loading={organizationsLoading} />
            </Container>
            <AddNonClientOrganizationModal
                hide={hideAddNonClientOrganizationsModal}
                isOpen={isOpenAddNonClientOrganizationsModal}
                callback={onAddNonClientOrganization}
            />
        </div>
    );
};

export const AdminOrganizations = withRouter(AdminOrganizationsBase);
