import gql from "graphql-tag";

export const GET_ALL_ORGANIZATIONS = gql`
    query GetOrganizations($search: String) {
        organizations(search: $search) {
            data {
                id
                name
                email
                phone
                category
                address {
                    street
                    number
                    city
                    line_1
                    line_2
                    plz
                }
                type {
                    id
                    name
                }
                role {
                    id
                    name
                }
                logo {
                    id
                    url
                }
            }
        }
    }
`;

export const ORGANIZATIONS_SEARCH = gql`
    query GetOrganizations($search: String!) {
        organizations_search(search: $search) {
            data {
                id
                name
                email
                phone
                category
                address {
                    street
                    number
                    city
                    line_1
                    line_2
                    plz
                }
                type {
                    id
                    name
                }
                role {
                    id
                    name
                }
                logo {
                    id
                    url
                }
            }
        }
    }
`;

export interface IAdminOrganizationSearch {
    id: string;
    name: string;
    email?: string;
    phone?: string;
    category?: string;
    address?: {
        street?: string;
        number?: string;
        plz?: string;
        city?: string;
        line_1?: string;
        line_2?: string;
    };
    type: {
        name: string;
    };
    role: {
        name: string;
    };
    logo?: {
        url?: string;
    };
}

export interface IAdminOrganization {
    id: string;
    name: string;
    email?: string;
    phone?: string;
    category?: string;
    address?: {
        street?: string;
        number?: string;
        plz?: string;
        city?: string;
        line_1?: string;
        line_2?: string;
    };
    type: {
        name: string;
    };
    role: {
        name: string;
    };
    logo?: {
        url?: string;
    };
}
