import { GET_ALL_ORGANIZATIONS, ORGANIZATIONS_SEARCH, IAdminOrganization } from "./definitions";

export const getAddressString = (address: any) => {
    const {
        street,
        number,
        city,
        plz,
        line_1,
        line_2,
    }: { street: string; number: string; city: string; plz: string; line_1: string; line_2: string } = address;

    return `${line_1 || ""} ${line_2 || ""}
        ${street && (line_1 || line_2) ? "," : ""}
        ${street || ""} ${number || ""}
        ${city && (street || number) ? "," : ""}
        ${plz || ""} ${city || ""}`;
};

export const getOrganizationsQueryAndLocation = (isSearching: boolean) => ({
    query: isSearching ? ORGANIZATIONS_SEARCH : GET_ALL_ORGANIZATIONS,
    location: isSearching ? "organizations_search.data" : "organizations.data",
});

export function normalizeOrganizations(organizations: any[] = []) {
    return organizations.map(({ id, name, address, phone, email, logo, type, category, role }: IAdminOrganization) => ({
        id,
        name,
        address: address ? getAddressString(address) : "",
        phone,
        email,
        role,
        type,
        logo,
        isClient: category === "CLIENT",
    }));
}
