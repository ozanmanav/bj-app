import gql from "graphql-tag";

export const GET_ORGANIZATION_RELATIONSHIPS = gql`
    query Organization($id: String) {
        organizations(id: $id) {
            data {
                id
                name
                higherRelations {
                    id
                    higher {
                        id
                        category
                        name
                    }
                }
                lowerRelations {
                    id
                    lower {
                        id
                        category
                        name
                    }
                }
            }
        }
    }
`;

export const CREATE_SUBSIDIARIES = gql`
    mutation CreateSubsidiaries($parent_id: String!, $child_ids: [String!]) {
        createOrganizationRelationship(parent_id: $parent_id, child_ids: $child_ids) {
            id
            type_id
            higher_id
            lower_id
        }
    }
`;

export const DELETE_SUBSIDIARY = gql`
    mutation DeleteSubsidiaries($ids: [String]!) {
        deleteOrganizationRelationships(ids: $ids)
    }
`;

export const CREATE_PARENTS = gql`
    mutation createParents($parent_ids: [String!], $child_id: String!) {
        createOrganizationRelationship(parent_ids: $parent_ids, child_id: $child_id) {
            id
            type_id
            higher_id
            lower_id
        }
    }
`;
