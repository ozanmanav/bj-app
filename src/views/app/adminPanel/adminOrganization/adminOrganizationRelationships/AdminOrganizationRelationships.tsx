import React, { FunctionComponent } from "react";
import { RouteComponentProps } from "react-router-dom";
import { Container } from "../../../../../components/ui/grid";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { GET_ORGANIZATION_RELATIONSHIPS, CREATE_SUBSIDIARIES, DELETE_SUBSIDIARY, CREATE_PARENTS } from "./config";
import get from "lodash.get";
import difference from "lodash.difference";
import { SubsidiariesTable } from "../../../../../components/tables/subsidiariesTable";
import { IAddSubsidiaryModalFormState } from "../../../../../components/modals/addSubsidiaryModal/AddSubsidiaryModalForm";
import { showErrorToast, handleGraphQLErrors, showSuccessToast } from "../../../../../components/ui/toasts";
import { ISubsidiary } from "../../../../../config";
import { AddSubsidiaryModal } from "../../../../../components/modals";
import { Button, useModal } from "../../../../../components/ui";
import { isClientOrganizationRoute } from "../../../../../utils/isCompanyRoute";
import { Loader } from "../../../../../components/ui/loader";

export const AdminOrganizationRelationships: FunctionComponent<RouteComponentProps<{ id: string }>> = ({
    match,
    location,
}) => {
    const organizationID = match.params.id;
    const isClient = isClientOrganizationRoute(location);

    const { open: openAddSubsidiaryModal, hide: hideAddSubsidiaryModal, isOpen: isOpenAddSubsidiaryModal } = useModal();
    const { open: openAddParentModal, hide: hideAddParentModal, isOpen: isOpenAddParentModal } = useModal();

    const [createSubsidiariesMutation] = useMutation(CREATE_SUBSIDIARIES);
    const [deleteSubsidiariesMutation] = useMutation(DELETE_SUBSIDIARY);
    const [createParentMutation] = useMutation(CREATE_PARENTS);
    const { data: organizationData, loading: loadingRelationships, refetch: refetchRelationships } = useQuery(
        GET_ORGANIZATION_RELATIONSHIPS,
        {
            variables: {
                id: organizationID,
            },
        }
    );

    const higherRelations = get(organizationData, "organizations.data[0].higherRelations") || [];
    const lowerRelations = get(organizationData, "organizations.data[0].lowerRelations") || [];

    const parentOrganizationsData = higherRelations
        .filter((item: any) => item.higher !== null)
        .map((item: any) => ({ relationId: item.id, ...item.higher }));
    const childOrganizationsData = lowerRelations
        .filter((item: any) => item.lower !== null)
        .map((item: any) => {
            return {
                relationId: item.id,
                ...item.lower,
            };
        });

    const parentOrganizations = parentOrganizationsData.map(({ id, name, relationId, category }: ISubsidiary) => ({
        id,
        name,
        relationId,
        category,
    }));

    const childOrganizations = childOrganizationsData.map(({ id, name, relationId, category }: ISubsidiary) => ({
        id,
        name,
        relationId,
        category,
    }));

    async function bulkDeleteSubsidiaries(deletedSubsidiaries: ISubsidiary[], type: string) {
        const deletedSubsidiariesIDs = deletedSubsidiaries.map((item) => item.relationId);

        const { errors: deletedSubsidiariesError } = await deleteSubsidiariesMutation({
            variables: { ids: deletedSubsidiariesIDs },
        });

        handleErrorsAndRefetch(deletedSubsidiariesError, type);
    }

    async function bulkInsertSubsidiaries(addedSubsidiaries: ISubsidiary[]) {
        const addedSubsidiariesIDs = addedSubsidiaries.map((item) => item.id);

        const { errors: addedSubsidiariesError } = await createSubsidiariesMutation({
            variables: {
                parent_id: organizationID,
                child_ids: addedSubsidiariesIDs,
            },
        });

        handleErrorsAndRefetch(addedSubsidiariesError, "Subsidiary");
    }

    async function bulkInsertParents(addedParents: ISubsidiary[]) {
        const addedParentsIDs = addedParents.map((item) => item.id);

        const { errors: addParentsError } = await createParentMutation({
            variables: {
                child_id: organizationID,
                parent_ids: addedParentsIDs,
            },
        });

        handleErrorsAndRefetch(addParentsError, "Parent");
    }

    async function submitSubsidiaries(values: IAddSubsidiaryModalFormState) {
        try {
            const updatedSubsidiaries = get(values, "organizations");
            const deletedSubsidiaries = difference(childOrganizations, updatedSubsidiaries);
            const addedSubsidiaries = difference(updatedSubsidiaries, childOrganizations);

            if (addedSubsidiaries.length > 0) {
                await bulkInsertSubsidiaries(addedSubsidiaries);
            }

            if (deletedSubsidiaries.length > 0) {
                await bulkDeleteSubsidiaries(deletedSubsidiaries, "Subsidiary");
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    async function submitParents(values: IAddSubsidiaryModalFormState) {
        try {
            const updatedParents = get(values, "organizations");
            const deletedParents = difference(parentOrganizations, updatedParents);
            const addedParents = difference(updatedParents, parentOrganizations);

            if (addedParents.length > 0) {
                await bulkInsertParents(addedParents);
            }

            if (deletedParents.length > 0) {
                await bulkDeleteSubsidiaries(deletedParents, "Parent");
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    function handleErrorsAndRefetch(errors: any, type: string) {
        if (errors) {
            handleGraphQLErrors(errors);
        } else {
            type === "Subsidiary" ? hideAddSubsidiaryModal() : hideAddParentModal();
            refetchRelationships();
            showSuccessToast(`${type} is successfully updated`);
        }
    }

    const subsidiaryTitle = childOrganizations.length > 0 ? "Edit subsidiaries" : "Add subsidiaries";
    const ParentTitle = parentOrganizations.length > 0 ? "Edit Parents" : "Add Parents";

    return (
        <Container className="b-admin-organization">
            {loadingRelationships ? (
                <Loader withText text="Relationships" />
            ) : (
                <>
                    <div className="b-admin-organization__item">
                        <div className="b-admin-organization__header flex align-center justify-between">
                            <h3 className="b-admin-organization__title">Parents</h3>
                            <Button text={ParentTitle} onClick={openAddParentModal} />
                        </div>

                        <SubsidiariesTable subsidiaries={parentOrganizations} notFoundTextPostfix={"parent"} />
                    </div>

                    <div className="b-admin-organization__item">
                        <div className="b-admin-organization__header flex align-center justify-between">
                            <h3 className="b-admin-orga nization__title">Subsidiaries</h3>
                            <Button text={subsidiaryTitle} onClick={openAddSubsidiaryModal} />
                        </div>

                        <SubsidiariesTable subsidiaries={childOrganizations} />
                    </div>

                    <AddSubsidiaryModal
                        hide={hideAddSubsidiaryModal}
                        isOpen={isOpenAddSubsidiaryModal}
                        isClient={isClient}
                        subsidiaryOrganizations={childOrganizations}
                        onSubsidiariesSubmit={submitSubsidiaries}
                        title={subsidiaryTitle}
                        organizationID={organizationID}
                        isParent={false}
                    />

                    <AddSubsidiaryModal
                        hide={hideAddParentModal}
                        isOpen={isOpenAddParentModal}
                        isClient={isClient}
                        subsidiaryOrganizations={parentOrganizations}
                        onSubsidiariesSubmit={submitParents}
                        title={ParentTitle}
                        organizationID={organizationID}
                        isParent={true}
                    />
                </>
            )}
        </Container>
    );
};
