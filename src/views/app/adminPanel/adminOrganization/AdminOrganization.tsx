import React, { FunctionComponent, useContext } from "react";
import "./AdminOrganization.scss";
import { RouteComponentProps, Switch, Route, Redirect } from "react-router-dom";
import { ContentHeader } from "../../../../components/contentHeaders";
import { AdminOrganizationsDetails } from "./adminOrganizationDetails";
import { AdminOrganizationRelatedObjects } from "./adminOrganizationRelatedObjects";
import { AdminOrganizationRelationships } from "./adminOrganizationRelationships";
import { InstallationsManufacturer } from "../../object/installations/installationsManufacturer";
import { AdminOrganizationFinancials } from "./adminOrganizationFinancials";
import { useRoleCheck } from "../../../../hooks";
import { DEFAULT_ROUTING_GUARD_REDIRECT_URL } from "../../../../components/hocs/withRoutingGuard/withRoutingGuard";
import { AccountStateContext } from "../../../../contexts/accountStateContext";
import { AdminOrganizationSettings } from "./adminOrganizationSettings";
import { AdminOrganizationPeople } from "./adminOrganizationPeople";
import { ObjectHistory } from "../../object/objectHistory";

export const AdminOrganization: FunctionComponent<RouteComponentProps<{ id: string }>> = ({ match }) => {
    const { currentOrganization } = useContext(AccountStateContext);
    const userOrganizationID = currentOrganization.id;

    const isAllowed = useRoleCheck([], ["admin", "master", "datamanager"]) || userOrganizationID === match.params.id;

    if (!isAllowed) {
        return <Redirect to={DEFAULT_ROUTING_GUARD_REDIRECT_URL} />;
    }

    return (
        <>
            <ContentHeader />
            <Switch>
                <Route
                    path={[
                        "/app/admin/organizations/:id/installations",
                        "/app/admin/organizations/client/:id/installations",
                        "/app/organizations/client/:id/installations",
                        "/app/organizations/:id/installations",
                    ]}
                    component={InstallationsManufacturer}
                />
                <Route
                    path={[
                        "/app/admin/organizations/:id/related",
                        "/app/admin/organizations/client/:id/related",
                        "/app/organizations/client/:id/related",
                        "/app/organizations/:id/related",
                    ]}
                    component={AdminOrganizationRelatedObjects}
                />
                <Route
                    path={[
                        "/app/admin/organizations/:id/people",
                        "/app/admin/organizations/client/:id/people",
                        "/app/organizations/client/:id/people",
                        "/app/organizations/:id/people",
                    ]}
                    component={AdminOrganizationPeople}
                />
                <Route
                    path={[
                        "/app/admin/organizations/:id/relationships",
                        "/app/admin/organizations/client/:id/relationships",
                        "/app/organizations/client/:id/relationships",
                        "/app/organizations/:id/relationships",
                    ]}
                    component={AdminOrganizationRelationships}
                />
                <Route
                    path={["/app/admin/organizations/client/:id/financials", "/app/organizations/client/:id/financials"]}
                    component={AdminOrganizationFinancials}
                />
                <Route
                    path={[
                        "/app/admin/organizations/:id/admin",
                        "/app/organizations/:id/admin",
                        "/app/admin/organizations/client/:id/admin",
                        "/app/organizations/client/:id/admin",
                    ]}
                    component={AdminOrganizationSettings}
                />
                <Route
                    path={[
                        "/app/admin/organizations/:id/history",
                        "/app/organizations/:id/history",
                        "/app/organizations/client/:id/history",
                    ]}
                    component={ObjectHistory}
                />
                }
                <Route
                    path={[
                        "/app/admin/organizations/:id/",
                        "/app/admin/organizations/client/:id/",
                        "/app/organizations/client/:id/",
                        "/app/organizations/:id/",
                    ]}
                    exact
                    component={AdminOrganizationsDetails}
                />
            </Switch>
        </>
    );
};
