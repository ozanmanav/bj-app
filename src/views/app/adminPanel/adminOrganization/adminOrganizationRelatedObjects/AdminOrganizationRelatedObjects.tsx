import React, { ChangeEvent, FunctionComponent, useState } from "react";
import { RouteComponentProps } from "react-router-dom";
import { isClientOrganizationRoute } from "../../../../../utils/isCompanyRoute";
import { Container } from "../../../../../components/ui/grid";
import { GRAPHQL_ADMIN_CONTEXT } from "../../../../../config";
import { useQuery } from "@apollo/react-hooks";
import {
    IOrganizationObject,
    GET_ORGANIZATION_SELF_OBJECTS,
    GET_ORGANIZATION_CONTRACT_OBJECTS,
    GET_CHILD_ORGANIZATIONS_OBJECTS,
} from "./config";
import { RelatedObjectsTable } from "../../../../../components/tables/relatedObjectsTable";
import { SearchInput } from "../../../../../components/ui/inputs/input";
import { FilterDropdown } from "../../../../../components/ui/filter";
import { PlusButton } from "../../../../../components/ui/buttons";
import { useObjectTypes } from "../../../../../hooks/apollo";
import { getObjectTypesOptions } from "../../../../../utils";
import { TFilterDropDownOption } from "../../../../../components/ui/filter/FilterDropdown";
import get from "lodash.get";
import { Loader } from "../../../../../components/ui/loader";
import { useDebounce } from "use-debounce/lib";
import { AUTOCOMPLETE_DEBOUNCE_TIME_MS } from "../../../../../components/ui";
import { filterObjectWithConditions } from "./utils";
import { uniq } from "ramda";

export const AdminOrganizationRelatedObjects: FunctionComponent<RouteComponentProps<{
    id: string;
}>> = ({ match, location }) => {
    const organizationID = match.params.id;
    const isClientOrganization = isClientOrganizationRoute(location);

    const [filterTypeID, setFilterTypeID] = useState<string>("");
    const [locationValueFilter, setLocationValueFilter] = useState<string>("");
    const [debouncedLocationValueFilter] = useDebounce(locationValueFilter, AUTOCOMPLETE_DEBOUNCE_TIME_MS);

    const { data: organizationObjectsData, refetch: organizationObjectsRefetch, loading: organizationObjectsLoading } = useQuery(
        GET_ORGANIZATION_SELF_OBJECTS,
        {
            variables: {
                organization_id: organizationID,
            },
            fetchPolicy: "no-cache",
            ...GRAPHQL_ADMIN_CONTEXT,
        }
    );

    const { data: organizationContractsData, refetch: organizationContractsRefetch, loading: organizationContractsLoading } = useQuery(
        GET_ORGANIZATION_CONTRACT_OBJECTS,
        {
            variables: {
                organization_id: organizationID,
            },
            fetchPolicy: "no-cache",
            ...GRAPHQL_ADMIN_CONTEXT,
        }
    );

    const { data: organizationChildsData, refetch: organizationChildsRefetch, loading: organizationChildsLoading } = useQuery(
        GET_CHILD_ORGANIZATIONS_OBJECTS,
        {
            variables: {
                organization_id: organizationID,
            },
            fetchPolicy: "no-cache",
            ...GRAPHQL_ADMIN_CONTEXT,
        }
    );

    const objectTypes = useObjectTypes();

    const organizationName = get(organizationContractsData, `organizations.data[0].name`) || "";
    const selfRelatedObjects = get(organizationObjectsData, "objects.data") || [];
    const organizationContracts = get(organizationContractsData, "organizations.data[0].contracts") || [];
    const organizationChilds = get(organizationChildsData, "organizations.data") || [];

    const organizationContractsObjects =
        organizationContracts.reduce(
            (
                acc: IOrganizationObject[],
                {
                    role,
                    objects = [],
                }: {
                    name: string;
                    role: { name: string };
                    objects: IOrganizationObject[];
                }
            ) => {
                objects.forEach((object) => {
                    const objectTypeID = get(object, "type.id");
                    const isFoundObjectType = objectTypes.find(({ id }) => id === objectTypeID);

                    if (isFoundObjectType) {
                        const roleName = get(role, "name");

                        acc.push({
                            ...object,
                            relatedTo: [roleName],
                        });
                    }
                });

                return acc;
            },
            []
        ) || [];

    const subsidiaryOrganizationObjects = organizationChilds.reduce(
        (acc: IOrganizationObject[], { lowerRelations }: { lowerRelations: any[] }) => {
            lowerRelations.forEach((lowerRelation) => {
                const lowerObjects = get(lowerRelation, "lower.objects") || [];
                const lowerName = get(lowerRelation, "lower.name") || "null";

                lowerObjects.forEach((object: any) => {
                    const objectTypeID = get(object, "type.id");
                    const isFoundObjectType = objectTypes.find(({ id }) => id === objectTypeID);

                    if (isFoundObjectType) {
                        acc.push({
                            ...object,
                            relatedTo: [lowerName],
                        });
                    }
                });
            });

            return acc;
        },
        []
    );

    const mergedRelatedObjects = [...selfRelatedObjects, ...organizationContractsObjects, ...subsidiaryOrganizationObjects];

    const mergedRelatedObjectsObjectTypes = uniq(mergedRelatedObjects.map((item) => item.type)) || [];

    // Show existing related objects types in the dropdown
    const preparedObjectTypes = getObjectTypesOptions(mergedRelatedObjectsObjectTypes) as TFilterDropDownOption[];

    const relatedObjects = mergedRelatedObjects.reduce((acc: IOrganizationObject[], item) => {
        const existedObjectIndex = acc.findIndex(({ id }) => item.id === id);

        if (filterObjectWithConditions(filterTypeID, debouncedLocationValueFilter, item)) {
            if (existedObjectIndex !== -1) {
                acc[existedObjectIndex].relatedTo.push(...item.relatedTo);
            } else {
                acc.push({
                    ...item,
                    relatedTo: item.relatedTo || [organizationName],
                });
            }
        }

        return acc;
    }, []);

    function onTypeFilterChange(type: string) {
        setFilterTypeID(type);
    }

    function onLocationValueChange(e: ChangeEvent<HTMLInputElement>) {
        setLocationValueFilter(e.target.value);
    }

    function onObjectDelete() {
        organizationObjectsRefetch();
        organizationChildsRefetch();
        organizationContractsRefetch();
    }

    return (
        <Container className="b-admin-organization">
            <div className="b-admin-organization__item">
                <div className="b-admin-organization__header flex align-center justify-between">
                    <div className="flex align-center">
                        <h3 className="b-admin-organization__title">Related objects</h3>
                        <PlusButton to={`/app/object/add?organizationID=${organizationID}`} />
                    </div>
                    <div className="flex align-center">
                        <FilterDropdown text="Filter by object type" items={preparedObjectTypes} onItemClick={onTypeFilterChange} />
                        <SearchInput
                            placeholder="Search objects by location..."
                            marginBottom="none"
                            value={locationValueFilter}
                            onChange={onLocationValueChange}
                            className="b-admin-organization__search"
                        />
                    </div>
                </div>
                {organizationObjectsLoading || organizationChildsLoading || organizationContractsLoading ? (
                    <Loader withText text="Related Objects" />
                ) : (
                    relatedObjects.length > 0 && (
                        <RelatedObjectsTable
                            relatedObjects={relatedObjects}
                            refetch={onObjectDelete}
                            variant={isClientOrganization ? "clientOrganization" : "nonClientOrganization"}
                        />
                    )
                )}
            </div>
        </Container>
    );
};
