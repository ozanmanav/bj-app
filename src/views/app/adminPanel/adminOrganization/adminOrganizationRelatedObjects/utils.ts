import { isNillOrEmpty } from "../../../../../utils";
import get from "lodash.get";

export const filterObjectWithConditions = (filterTypeID: string, locationValueFilter: string, object: any) => {
    const objectTypeID = get(object, "type.id");
    const objectAddress = get(object, "address");
    const lowerCasedAddress = objectAddress ? objectAddress.toLowerCase() : "";
    const lowerCasedAddressFilter = locationValueFilter ? locationValueFilter.toLowerCase() : "";

    let isMatchObject = false;

    if (isNillOrEmpty(filterTypeID) && isNillOrEmpty(lowerCasedAddressFilter)) {
        isMatchObject = true;
    }

    if (!isNillOrEmpty(filterTypeID)) {
        if (objectTypeID === filterTypeID) {
            isMatchObject = true;
        }
    }

    if (!isNillOrEmpty(lowerCasedAddressFilter)) {
        if (lowerCasedAddress.includes(lowerCasedAddressFilter)) {
            isMatchObject = true;
        }
    }

    if (!isNillOrEmpty(filterTypeID) && !isNillOrEmpty(lowerCasedAddressFilter)) {
        if (objectTypeID === filterTypeID && lowerCasedAddress.includes(lowerCasedAddressFilter)) {
            isMatchObject = true;
        } else {
            isMatchObject = false;
        }
    }

    return isMatchObject;
};
