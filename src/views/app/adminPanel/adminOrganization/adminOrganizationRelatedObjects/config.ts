import gql from "graphql-tag";
import { IRelatedObjectsTableObject } from "../../../../../components/tables/relatedObjectsTable";

export interface IOrganizationObject extends IRelatedObjectsTableObject {
    company_owner: string;
}

export const GET_ORGANIZATION_SELF_OBJECTS = gql`
    query GetOrganizationSelfObjects($organization_id: String, $type_id: String, $address_search: AddressSearch) {
        objects(organization_id: $organization_id, type_id: $type_id, address_search: $address_search) {
            data {
                id
                name
                type {
                    id
                    name
                    type
                }
                address
                contracts {
                    id
                    role {
                        id
                        name
                    }
                    organization_id
                    object_ids
                }
            }
        }
    }
`;

export const GET_ORGANIZATION_CONTRACT_OBJECTS = gql`
    query GetOrganizationContractsObjects($organization_id: String) {
        organizations(id: $organization_id) {
            data {
                id
                name
                contracts {
                    id
                    role {
                        id
                        name
                    }
                    organization_id
                    object_ids
                    objects {
                        id
                        name
                        type {
                            id
                            name
                            type
                        }
                        address
                        contracts {
                            id
                            organization_id
                            role {
                                name
                            }
                        }
                    }
                }
            }
        }
    }
`;

export const GET_CHILD_ORGANIZATIONS_OBJECTS = gql`
    query GetChildOrganizationObjects($organization_id: String) {
        organizations(id: $organization_id) {
            data {
                id
                name
                lowerRelations {
                    id
                    lower {
                        id
                        name
                        objects {
                            id
                            name
                            type {
                                id
                                name
                                type
                            }
                            address
                            contracts {
                                id
                                organization_id
                                role {
                                    name
                                }
                            }
                        }
                    }
                }
            }
        }
    }
`;
