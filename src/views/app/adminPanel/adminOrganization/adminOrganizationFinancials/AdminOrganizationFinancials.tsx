import React, { FunctionComponent } from "react";
import { RouteComponentProps } from "react-router-dom";
import { Container } from "../../../../../components/ui/grid";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { normalizeTotals } from "../../../../../utils";
import { ObjectAdminSummary } from "../../../object/objectAdmin/ObjectAdmin";
import { Loader } from "../../../../../components/ui/loader";

const ORGANIZATION_FINANCIALS = gql`
    query OrganizationFinancials($organization_id: String!) {
        totals(organization_id: $organization_id) {
            name
            value
        }
    }
`;

export const AdminOrganizationFinancials: FunctionComponent<RouteComponentProps<{ id: string }>> = ({ match }) => {
    const organizationID = match.params.id;

    const { data, loading: loadingFinancials } = useQuery(ORGANIZATION_FINANCIALS, {
        variables: {
            organization_id: organizationID,
        },
    });

    const totals = normalizeTotals(data && data.totals);

    return (
        <Container className="b-admin-organization">
            {loadingFinancials ? (
                <Loader withText text="Financials" />
            ) : (
                <div className="b-admin-organization__item flex align-center">
                    <ObjectAdminSummary type="objectTransparent" amount={totals.buildings} description="buildings" />
                    <ObjectAdminSummary type="octagon" amount={totals.objects} description="objects" />
                    <ObjectAdminSummary type="customField" amount={totals.custom_fields} description="custom fields" />
                    <ObjectAdminSummary type="chf" amount={totals.total} description="total" />
                </div>
            )}
        </Container>
    );
};
