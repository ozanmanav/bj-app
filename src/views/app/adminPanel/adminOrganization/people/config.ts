import gql from "graphql-tag";

export const GET_ORGANIZATION_EMPLOYEES = gql`
    query GetOrganizationEmployees($id: String) {
        organizations(id: $id) {
            data {
                id
                name
                teams {
                    id
                    name
                    users
                    users_data {
                        id
                        name
                        first_name
                        email
                        phone
                    }
                }
                employees {
                    id
                    job_id
                    invited
                    registered
                    status
                    user_role {
                        id
                        type
                        name
                    }
                    job {
                        id
                        type
                        name
                    }
                    user {
                        id
                        status
                        first_name
                        last_name
                        email
                        phone
                        language
                    }
                }
            }
        }
    }
`;
