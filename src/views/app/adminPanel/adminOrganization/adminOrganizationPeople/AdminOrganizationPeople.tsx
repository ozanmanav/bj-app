import React, { FunctionComponent } from "react";
import { RouteComponentProps } from "react-router-dom";
import { Container } from "../../../../../components/ui/grid";
import { GET_ORGANIZATION_EMPLOYEES } from "./config";
import get from "lodash.get";
import { PlusButton } from "../../../../../components/ui/buttons";
import { useModal } from "../../../../../components/ui/modal";
import { EmployeesTable } from "../../../../../components/tables/employeesTable";
import { AddUserModal, AddTeamModal } from "../../../../../components/modals";
import { useQuery } from "@apollo/react-hooks";
import { BuildingTeamsTable } from "../../../../../components/tables";
import { UserRoleCheck } from "../../../../../components/userRoleCheck";
import { normalizeOrganizationUsers } from "../adminOrganizationSettings/definitions";
import { isClientOrganizationRoute } from "../../../../../utils/isCompanyRoute";
import { IEmployee } from "../../../../../config";
import { Loader } from "../../../../../components/ui/loader";

export const AdminOrganizationPeople: FunctionComponent<RouteComponentProps<{
    id: string;
}>> = ({ match, location }) => {
    const organizationID = match.params.id;
    const isClientOrganization = isClientOrganizationRoute(location);
    const { open: openAddUserModal, hide: hideAddUserModal, isOpen: isOpenOpenAddUserModal } = useModal();
    const { open: openAddTeamModal, hide: hideAddTeamModal, isOpen: isOpenOpenAddTeamModal } = useModal();

    const { data, refetch, loading } = useQuery(GET_ORGANIZATION_EMPLOYEES, {
        variables: {
            id: organizationID,
        },
        fetchPolicy: "network-only",
    });
    const organizationEmployees = (get(data, `organizations.data[0].employees`) as IEmployee[]) || [];
    const organizationTeams = get(data, `organizations.data[0].teams`) || [];
    const country = get(data, `organizations.data[0].address.country`, "");
    const organizationRole = get(data, `organizations.data[0].role.type`, "");

    return (
        <Container className="b-admin-organization">
            {loading ? (
                <Loader withText text="People" />
            ) : (
                <>
                    <div className="b-admin-organization__item">
                        <div className="b-admin-organization__header flex align-center">
                            <h3 className="b-admin-organization__title">Employees</h3>
                            {isClientOrganization ? (
                                <UserRoleCheck
                                    availableForRoles={[
                                        "owner_administrator",
                                        "manufacturer_administrator",
                                        "property_manager_administrator",
                                        "service_provider_administrator",
                                    ]}
                                >
                                    <PlusButton onClick={openAddUserModal} />
                                </UserRoleCheck>
                            ) : (
                                <PlusButton onClick={openAddUserModal} />
                            )}
                        </div>
                        {organizationEmployees.length > 0 && (
                            <EmployeesTable
                                isClientOrganization={isClientOrganization}
                                organizationID={organizationID}
                                companyId={organizationID}
                                refetch={refetch}
                                employees={organizationEmployees}
                                companyType={organizationRole}
                            />
                        )}
                    </div>
                    <div className="b-admin-organization__item">
                        <div className="b-admin-organization__header flex align-center">
                            <h3>
                                Teams
                                <UserRoleCheck
                                    availableForRoles={[
                                        "owner_administrator",
                                        "manufacturer_administrator",
                                        "property_manager_administrator",
                                        "service_provider_administrator",
                                    ]}
                                >
                                    <PlusButton onClick={openAddTeamModal} />
                                </UserRoleCheck>
                            </h3>
                        </div>
                        {organizationTeams && (
                            <BuildingTeamsTable
                                refetch={refetch}
                                teams={organizationTeams}
                                organizationID={organizationID}
                                users={normalizeOrganizationUsers(organizationEmployees)}
                            />
                        )}
                    </div>
                    <AddUserModal
                        refetch={refetch}
                        hide={hideAddUserModal}
                        isOpen={isOpenOpenAddUserModal}
                        organizationID={organizationID}
                        isClientOrganization={isClientOrganization}
                        companyCountry={country}
                    />
                    <AddTeamModal
                        users={normalizeOrganizationUsers(organizationEmployees)}
                        refetch={refetch}
                        hide={hideAddTeamModal}
                        isOpen={isOpenOpenAddTeamModal}
                        organizationID={organizationID}
                    />
                </>
            )}
        </Container>
    );
};
