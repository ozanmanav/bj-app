import gql from "graphql-tag";

// TODO: details deleted from here should implement in the files list component.
export const GET_ORGANIZATION_FILES = gql`
    query GetOrganizationFiles($id: String!) {
        organizations(id: $id) {
            data {
                id
                name
                files {
                    id
                    type {
                        id
                        type
                        name
                    }
                    name
                    url
                    filename
                    extension
                    size
                    events {
                        id
                        timeline
                    }
                    updated_at
                    created_at
                    details {
                        context
                        type
                        value
                        name
                        meta {
                            last_modification {
                                modifier {
                                    name
                                }
                            }
                            author {
                                name
                            }
                            context
                            verification
                            verification_by
                            source_url
                            source_date
                            source
                        }
                    }
                    mime_type
                }
            }
        }
    }
`;
