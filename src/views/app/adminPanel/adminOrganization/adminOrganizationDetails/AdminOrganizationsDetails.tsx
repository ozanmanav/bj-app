import React, { FunctionComponent } from "react";
import { RouteComponentProps } from "react-router-dom";
import { ObjectDetailsTable } from "../../../../../components/object/objectDetailsTable";
import { FilesList } from "../../../../../components/filesList";
import EventsTimeline from "../../../../../components/events/EventsTimeline";
import { Container } from "../../../../../components/ui/grid";
import { useApolloClient, useQuery } from "@apollo/react-hooks";
import { GET_ORGANIZATION_FILES } from "./queries";
import get from "lodash.get";
import { ORGANIZATION_HEADER_QUERY } from "../../../../../components/contentHeaders/organizationContentHeader/config";
import { GRAPHQL_ADMIN_CONTEXT } from "../../../../../config";
import { GET_EVENTS } from "../../../../../components/events/EventsTimeline/queries";

export const AdminOrganizationsDetails: FunctionComponent<RouteComponentProps<{ id: string }>> = ({ match }) => {
    const organizationID = match.params.id;

    const apolloClient = useApolloClient();

    const { data, refetch, loading: loadingFiles } = useQuery(GET_ORGANIZATION_FILES, {
        variables: {
            id: organizationID,
        },
    });

    const { data: eventsData, refetch: eventsRefetch, loading: eventsLoading } = useQuery(GET_EVENTS, {
        variables: {
            organization_id: organizationID,
            strict: false,
        },
        fetchPolicy: "cache-first",
    });

    const files = get(data, `organizations.data[0].files`);
    const name = get(data, `organizations.data[0].name`);

    function filesChangeCallback() {
        apolloClient
            .query({
                query: ORGANIZATION_HEADER_QUERY,
                variables: {
                    id: organizationID,
                },
                ...GRAPHQL_ADMIN_CONTEXT,
                fetchPolicy: "network-only",
            })
            .then(() => refetch());
    }

    return (
        <Container>
            <ObjectDetailsTable variantID={organizationID} variant={"organization"} eventRefetch={eventsRefetch} />
            <FilesList
                files={files}
                loading={loadingFiles}
                className="_not-last-group"
                variant="organization"
                addFilesTo={name}
                filesChangeCallback={filesChangeCallback}
                organizationID={organizationID}
                eventRefetch={eventsRefetch}
            />
            <EventsTimeline
                variant="organization"
                variantID={organizationID}
                events={eventsData}
                refetch={eventsRefetch}
                loading={eventsLoading}
            />
        </Container>
    );
};
