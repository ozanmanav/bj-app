import React, { FunctionComponent } from "react";
import "./Subscription.scss";
import classNames from "classnames";
import { Button } from "../../../../../../components/ui/buttons";

interface ISubscriptionProps {
    name: string;
    price: string | number;
    currency: string;
    description: string;
    active: boolean;
    onSelect: (active: string) => void;
}

export const Subscription: FunctionComponent<ISubscriptionProps> = ({ name, price, currency, description, active, onSelect }) => {
    function setActive() {
        !active && onSelect(name);
    }

    return (
        <div className={classNames("b-subscription", { _active: active })}>
            <div className="b-subscription__name">{name}</div>
            <div className="b-subscription__price-outer">
                <div className="b-subscription__price">
                    {price}
                    <span className="b-subscription__price-currency">{currency}</span>
                </div>
            </div>
            <div className="b-subscription__description">{description}</div>
            <Button className="b-subscription__button" onClick={setActive} text={active ? "Active" : "Subscribe"} primary={!active} />
        </div>
    );
};
