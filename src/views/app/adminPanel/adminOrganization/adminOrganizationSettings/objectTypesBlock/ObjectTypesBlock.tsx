import React, { ChangeEvent, FunctionComponent, useEffect, useState } from "react";
import "./ObjectTypesBlock.scss";
import { getMetaTypesOptions } from "../../../../../../utils";
import { useApolloClient, useQuery } from "@apollo/react-hooks";
import get from "lodash.get";
import { META_TYPES_QUERIES } from "./config";
import {
    IObjectTypeMetaFormState,
    ObjectTypeMetaForm,
    objectTypeMetaFormEmptyRow,
    TObjectsType,
} from "../../../../../../components/forms/objectTypeMetaForm";
import { Select } from "../../../../../../components/ui/inputs";
import gql from "graphql-tag";
import { handleGraphQLErrors, showErrorToast } from "../../../../../../components/ui/toasts";
import { AddButton } from "../../../../../../components/ui/buttons";

const META_TYPE_QUERIES = gql`
    query TypeMeta($model_id: String!) {
        type_meta(model_id: $model_id) {
            data {
                id
                name
                type
            }
        }
    }
`;

interface IObjectTypesBlockProps {
    type?: TObjectsType;
    placeholder: string;
}

export const ObjectTypesBlock: FunctionComponent<IObjectTypesBlockProps> = ({ type, placeholder }) => {
    const apolloClient = useApolloClient();
    const [selectedTypeId, setSelectedTypeId] = useState<string>("");
    const [meta, setMeta] = useState<IObjectTypeMetaFormState[]>([]);
    const [metaLoaded, setMetaLoaded] = useState<boolean>(false);

    const { data, loading } = useQuery(META_TYPES_QUERIES[type ? type : "all_types"]);

    const types = get(data, "entity_types.data") || [];

    useEffect(() => {
        if (selectedTypeId) {
            getMeta();
        }
    }, [selectedTypeId]);

    useEffect(() => {
        if (!loading && types.length === 1) {
            setSelectedTypeId(types[0].id);
        }
    }, [loading, types]);

    function setType(e: ChangeEvent<HTMLSelectElement>) {
        const typeId = e.target.value;

        setSelectedTypeId(typeId);
    }

    async function getMeta() {
        try {
            const { data, errors } = await apolloClient.query({
                query: META_TYPE_QUERIES,
                variables: { model_id: selectedTypeId },
                fetchPolicy: "no-cache",
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                setMeta(get(data, "type_meta.data"));
                setMetaLoaded(true);
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    function removeMetaField() {
        const metaFields = [...meta];
        metaFields.pop();
        setMeta(metaFields);
    }

    function addMetaField() {
        setMeta([...meta, objectTypeMetaFormEmptyRow]);
    }

    return (
        <div className="b-object-types">
            <div className="b-object-types__item">
                <Select
                    disabled={types.length === 1}
                    options={getMetaTypesOptions(types)}
                    placeholder={placeholder || "Object Type"}
                    value={selectedTypeId}
                    onChange={setType}
                />
            </div>
            {metaLoaded &&
                (meta.length ? (
                    meta.map((row: IObjectTypeMetaFormState, index: number) => (
                        <div className="b-object-types__item" key={index}>
                            <ObjectTypeMetaForm
                                objectTypeId={selectedTypeId}
                                row={row}
                                refetch={getMeta}
                                removeMetaField={removeMetaField}
                                addMetaField={index === meta.length - 1 ? addMetaField : undefined}
                            />
                        </div>
                    ))
                ) : (
                    <div className="b-object-types__item">
                        <AddButton className="f-object-type-meta__btn" text="Add more" type="button" onClick={addMetaField} />
                    </div>
                ))}
        </div>
    );
};
