import gql from "graphql-tag";

export const META_TYPES_QUERIES = {
    all_types: gql`
        query ObjectTypes {
            entity_types(entity: "OBJECT", not_building: false, is_file: false) {
                data {
                    id
                    name
                    groups {
                        name
                    }
                }
            }
        }
    `,
    buildings: gql`
        query BuildingTypes {
            entity_types(type: "building") {
                data {
                    id
                    name
                    groups {
                        name
                    }
                }
            }
        }
    `,
    files: gql`
        query Files {
            entity_types(is_file: true) {
                data {
                    id
                    name
                    groups {
                        name
                    }
                }
            }
        }
    `,
    relationships: gql`
        query Relationships {
            entity_types(entity: "CONTRACT") {
                data {
                    id
                    name
                    groups {
                        name
                    }
                }
            }
        }
    `,
    events: gql`
        query Events {
            entity_types(entity: "EVENT") {
                data {
                    id
                    name
                    groups {
                        name
                    }
                }
            }
        }
    `,
};
