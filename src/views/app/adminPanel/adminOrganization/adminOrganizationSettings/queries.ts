import gql from "graphql-tag";

export const GET_ORGANIZATION = gql`
    query GetSettingsOrganization($id: String) {
        organizations(id: $id) {
            data {
                id
                name
                suspend
                invoice_addresses {
                    city
                    street
                    number
                    country
                    plz
                    line_1
                    line_2
                }
            }
        }
    }
`;

export const SUSPEND_ORGANIZATION = gql`
    mutation SuspendOrganization($organization_id: String!) {
        suspendOrganizationMutation(organization_id: $organization_id) {
            id
        }
    }
`;

export const RESTORE_ORGANIZATION = gql`
    mutation RestoreOrganization($organization_id: String!) {
        restoreOrganizationMutation(organization_id: $organization_id) {
            id
        }
    }
`;

export const DELETE_ORGANIZATION = gql`
    mutation DeleteOrganization($id: String!) {
        deleteOrganization(id: $id)
    }
`;
