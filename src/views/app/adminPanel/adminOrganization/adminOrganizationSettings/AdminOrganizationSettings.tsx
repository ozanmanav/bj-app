import React, { FunctionComponent, useContext } from "react";
import "./AdminOrganizationSettings.scss";
import {
    Accordion,
    Container,
    Tabs,
    TabContent,
    useAccordions,
    Loading,
    useModal,
    handleGraphQLErrors,
    showSuccessToast,
    showErrorToast,
} from "../../../../../components/ui";
import { SettingsDetailsForm } from "../../../../../components/forms/settingsDetailsForm";
import { NotificationsTable } from "../../../../../components/tables";
import { useQuery, useMutation } from "@apollo/react-hooks";
import get from "lodash.get";
import { AccountStateContext } from "../../../../../contexts/accountStateContext";
import { SettingsItem } from "../../../../../components/settings/settingsItem";
import { SettingsItemTitle } from "../../../../../components/settings/settingsItemTitle";
import { ObjectTypesBlock } from "./objectTypesBlock";
import { UserRoleCheck } from "../../../../../components/userRoleCheck";
import { ChangePasswordForm } from "../../../../../components/forms/changePasswordForm";
import classNames from "classnames";
import { GRAPHQL_ADMIN_CONTEXT } from "../../../../../config";
import { withRouter, RouteComponentProps } from "react-router";
import { GET_ORGANIZATION, SUSPEND_ORGANIZATION, RESTORE_ORGANIZATION, DELETE_ORGANIZATION } from "./queries";
import { notifications, ISettingsProps } from "./definitions";
import { isClientOrganizationRoute } from "../../../../../utils/isCompanyRoute";
import { ConvertToClientOrganizationModal, ConfirmModal } from "../../../../../components/modals";
import { ObjectHistory } from "../../../object/objectHistory";

const AdminOrganizationSettingsBase: FunctionComponent<ISettingsProps & RouteComponentProps> = ({
    standaloneOrganizationID,
    location,
    history,
}) => {
    const isClientOrganization = isClientOrganizationRoute(location);
    const accountStateContext = useContext(AccountStateContext);
    const { isOpen: confirmModalIsOpen, hide: confirmModalHide, open: confirmModalOpen } = useModal();
    const { isOpen: convertModalIsOpen, hide: convertModalHide, open: convertModalOpen } = useModal();
    const {
        isOpen: confirmCompanyDeleteModalIsOpen,
        hide: confirmCompanyDeleteModalHide,
        open: confirmCompanyDeleteModalOpen,
    } = useModal();
    const { isOpened, toggleAccordion } = useAccordions([]);

    const organizationId = standaloneOrganizationID || accountStateContext.currentOrganization.id;

    const { data, loading, refetch } = useQuery(GET_ORGANIZATION, {
        variables: {
            id: organizationId,
        },
        ...GRAPHQL_ADMIN_CONTEXT,
    });

    const organization = get(data, "organizations.data[0]");
    const invoiceAddresses = get(organization, "invoice_addresses");
    const name = get(organization, "name");
    const suspend = get(organization, "suspend");

    const [suspendOrganization] = useMutation(SUSPEND_ORGANIZATION, {
        variables: {
            organization_id: organizationId,
        },
    });

    const [restoreOrganization] = useMutation(RESTORE_ORGANIZATION, {
        variables: {
            organization_id: organizationId,
        },
    });

    const [deleteOrganizationMutation] = useMutation(DELETE_ORGANIZATION, {
        variables: {
            id: standaloneOrganizationID,
        },
    });

    const onConvertionSuccess = (organization_id: string) => {
        if (organization_id) {
            history.push(`/app/admin/organizations/client/${organization_id}`);
        }
    };

    async function changeCompanyStatus() {
        try {
            const { errors } = await (suspend ? restoreOrganization() : suspendOrganization());

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                await refetch();
                showSuccessToast(`Organization is successfully ${suspend ? "restored" : "suspended"}`);
                confirmModalHide();
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    async function deleteOrganization() {
        try {
            const { errors } = await deleteOrganizationMutation();

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                await refetch();
                showSuccessToast(`Company is successfully deleted`);
                confirmCompanyDeleteModalHide();
                history.push("/app/cockpit");
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    return (
        <div className={classNames(["b-admin-organization-settings", { _standalone: organizationId }])}>
            {loading ? (
                <Loading />
            ) : (
                <>
                    <Container>
                        <div className="b-admin-organization-settings__actions">
                            <div className="b-admin-organization-settings__actions-left"></div>

                            <div className="b-admin-organization-settings__actions-right">
                                <>
                                    {" "}
                                    {!isClientOrganization && (
                                        <button
                                            className="b-admin-organization-settings__actions-button _convert _font-bold block _text-right"
                                            onClick={convertModalOpen}
                                        >
                                            Invite to Join BobJ
                                        </button>
                                    )}
                                    {standaloneOrganizationID && !loading && (
                                        <>
                                            {" "}
                                            <button
                                                className={`b-admin-organization-settings__actions-button ${
                                                    suspend ? "_restore" : "_suspend"
                                                } _font-bold _text-right`}
                                                onClick={confirmModalOpen}
                                            >
                                                {suspend ? "Restore" : "Suspend"} organization
                                            </button>
                                            <button
                                                className={`b-admin-organization-settings__actions-button  _delete  _font-bold block _text-right`}
                                                onClick={confirmCompanyDeleteModalOpen}
                                            >
                                                Delete organization
                                            </button>{" "}
                                        </>
                                    )}
                                </>
                            </div>
                        </div>

                        {isClientOrganization && (
                            <>
                                <UserRoleCheck
                                    availableForRoles={[
                                        "owner_administrator",
                                        "manufacturer_administrator",
                                        "manufacturer_brand_manager",
                                        "manufacturer_object_manager",
                                        "property_manager_administrator",
                                        "property_manager_building_manager",
                                        "service_provider_administrator",
                                        "service_provider_service_manager",
                                    ]}
                                >
                                    <SettingsItem>
                                        <Accordion title="Company Details" isOpen={isOpened(1)} toggle={toggleAccordion} index={1}>
                                            <SettingsDetailsForm organizationId={organizationId} invoiceAddresses={invoiceAddresses} />
                                        </Accordion>
                                    </SettingsItem>
                                </UserRoleCheck>
                                {!standaloneOrganizationID && (
                                    <SettingsItem>
                                        <Accordion title="Change your password" isOpen={isOpened(2)} toggle={toggleAccordion} index={2}>
                                            <ChangePasswordForm />
                                        </Accordion>
                                    </SettingsItem>
                                )}
                                {/*TODO uncomment in future*/}
                                {/*<SettingsItem>*/}
                                {/*    <SettingsItemTitle>*/}
                                {/*        <h3>Subscription Plan</h3>*/}
                                {/*    </SettingsItemTitle>*/}
                                {/*    <ul className="b-admin-organization-settings__subscriptions">*/}
                                {/*        {subscriptions.map((subscription, index) => {*/}
                                {/*            return (*/}
                                {/*                <li className="b-admin-organization-settings__subscriptions-item" key={index}>*/}
                                {/*                    <Subscription*/}
                                {/*                        {...subscription}*/}
                                {/*                        onSelect={setActiveSubscription}*/}
                                {/*                        active={activeSubscription === subscription.name}*/}
                                {/*                    />*/}
                                {/*                </li>*/}
                                {/*            );*/}
                                {/*        })}*/}
                                {/*    </ul>*/}
                                {/*</SettingsItem>*/}
                                <SettingsItem>
                                    <SettingsItemTitle>
                                        <h3>Notifications</h3>
                                    </SettingsItemTitle>
                                    <NotificationsTable data={notifications} />
                                </SettingsItem>
                                <UserRoleCheck
                                    availableForRoles={[
                                        "owner_administrator",
                                        "manufacturer_administrator",
                                        "manufacturer_brand_manager",
                                        "manufacturer_object_manager",
                                        "property_manager_administrator",
                                        "service_provider_administrator",
                                    ]}
                                >
                                    <SettingsItem>
                                        <SettingsItemTitle>
                                            <h3>Custom Information</h3>
                                        </SettingsItemTitle>
                                        <Tabs>
                                            <TabContent label="Object types">
                                                <ObjectTypesBlock placeholder="Object Type" />
                                            </TabContent>
                                            <TabContent label="Relationships">
                                                <ObjectTypesBlock type="relationships" placeholder="Relationship Type" />
                                            </TabContent>
                                            <TabContent label="Events">
                                                <ObjectTypesBlock type="events" placeholder="Event Type" />
                                            </TabContent>
                                            <TabContent label="Files">
                                                <ObjectTypesBlock type="files" placeholder="File Type" />
                                            </TabContent>
                                            <TabContent label="Buildings">
                                                <ObjectTypesBlock type="buildings" placeholder="Building Type" />
                                            </TabContent>
                                        </Tabs>
                                    </SettingsItem>
                                </UserRoleCheck>
                            </>
                        )}
                        <ObjectHistory variant="organizations" variantID={organizationId} />
                    </Container>
                    <ConvertToClientOrganizationModal
                        organizationId={organizationId}
                        hide={convertModalHide}
                        isOpen={convertModalIsOpen}
                        callback={onConvertionSuccess}
                    />
                    <ConfirmModal
                        title={`Are you sure you want to permanently remove "${name}"?`}
                        onConfirm={deleteOrganization}
                        hide={confirmCompanyDeleteModalHide}
                        isOpen={confirmCompanyDeleteModalIsOpen}
                    />
                    <ConfirmModal
                        title="Are you sure?"
                        onConfirm={changeCompanyStatus}
                        hide={confirmModalHide}
                        isOpen={confirmModalIsOpen}
                    />
                </>
            )}
        </div>
    );
};

export const AdminOrganizationSettingsBaseWithRouter = withRouter(AdminOrganizationSettingsBase);

export const AdminOrganizationSettings: FunctionComponent<RouteComponentProps<{ id: string }>> = ({ match }) => {
    return <AdminOrganizationSettingsBaseWithRouter standaloneOrganizationID={match.params.id} />;
};
