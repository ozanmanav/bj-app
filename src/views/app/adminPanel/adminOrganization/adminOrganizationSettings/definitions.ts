import { IEmployee } from "../../../../../config";
import { ILocalAutocompleteOption } from "../../../../../components/ui";

export interface ISettingsProps {
    standaloneOrganizationID?: string;
}

export const subscriptions = [
    {
        name: "Starter",
        price: "345",
        description: "Title something",
        currency: "CHF",
    },
    {
        name: "Starter Plus",
        price: "570",
        description: "Title something",
        currency: "CHF",
    },
    {
        name: "Pro",
        price: "805",
        description: "Title something",
        currency: "CHF",
    },
    {
        name: "Enterprise",
        price: "2345",
        description:
            "Title something Title something Title something Title something Title something Title something Title something Title something",
        currency: "CHF",
    },
];

export const notifications = [
    {
        information: "Lorem Ipsum",
        description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered.",
    },
    {
        information: "Variations of Passages",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    },
    {
        information: "First line",
        description: "To generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always from.",
    },
];

export function normalizeOrganizationUsers(users: IEmployee[]): ILocalAutocompleteOption[] {
    return users.map(({ user, job }: IEmployee) => ({
        id: (user && user.id) || "",
        job_title: job && job.name,
        label: (user && `${user.first_name} ${user.last_name}`) || "",
        value: (user && user.id) || "",
    }));
}
