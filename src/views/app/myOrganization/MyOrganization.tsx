import React, { FunctionComponent, useContext } from "react";
import { Redirect } from "react-router-dom";
import { AccountStateContext } from "../../../contexts/accountStateContext";

export const MyOrganization: FunctionComponent = () => {
    const { currentOrganization } = useContext(AccountStateContext);

    return <Redirect to={`/app/organizations/client/${currentOrganization.id}`} />;
};
