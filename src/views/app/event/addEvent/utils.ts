import get from "lodash.get";
import { ISelectOption } from "../../../../components/ui";
import { getLocationSearchParam } from "../../../../utils/locationSearchHelpers";
import { IEventVariant } from "../../../../components/events/EventsTimeline/definitions";
import { IEventType } from "../../../../config";

export const normalizeEventTypes = (eventTypesData: any): ISelectOption[] => {
    const pureEventTypes = get(eventTypesData, "event_types") || [];

    return pureEventTypes.map((eventType: IEventType) => ({
        label: eventType.name,
        value: eventType.id,
        group: get(eventType.groups, "[0].name"),
    }));
};

export const getArrayIDs = (array: any): string[] => {
    return array ? array.map((data: any) => data && get(data, "id")) : [];
};

export const useVariantFromLocation = (location: any) => {
    const objectID = getLocationSearchParam(location.search, "obj");
    const organizationID = getLocationSearchParam(location.search, "org");
    const contractID = getLocationSearchParam(location.search, "cont");
    const relationID = getLocationSearchParam(location.search, "rel");

    const variant: IEventVariant =
        (objectID && "object") ||
        (organizationID && "organization") ||
        (contractID && "contract") ||
        (relationID && "relationship") ||
        undefined;

    const variantID = objectID || organizationID || contractID || relationID || undefined;

    return { variant, variantID };
};
