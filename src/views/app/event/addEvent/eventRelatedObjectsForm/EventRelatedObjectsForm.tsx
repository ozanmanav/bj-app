import React, { FunctionComponent, useState, ChangeEvent } from "react";
import { AutocompleteInput, RemoveObjectButton, Column, Row, SaveButton, Select } from "../../../../../components/ui";
import { Formik } from "formik";
import {
    IEventRelatedObjectsFormProps,
    EVENT_RELATED_OBJECTS_FORM_STATE,
    IEventRelatedObjectsFormBaseProps,
    IRelatedObject,
} from "./definitions";
import { FormAccordionContent, FormCaption, FormFooterSave } from "../../../../../components/forms";
import "./EventRelatedObjectsForm.scss";
import useUpdateEffect from "../../../../../hooks/useUpdateEffect";
import { OBJECT_SEARCH_SCOPES } from "../../../../../config";

const EventRelatedObjectsFormBase: FunctionComponent<IEventRelatedObjectsFormBaseProps> = ({
    submitFormOnChange,
    handleSubmit,
    values,
    setFieldValue,
    onClickCreateButton,
    scopeOrganizationID,
}) => {
    const [scope, setScope] = useState<string>(OBJECT_SEARCH_SCOPES[0].value as string);

    useUpdateEffect(() => {
        if (submitFormOnChange) {
            handleSubmit();
        }
    }, [values]);

    function addObject(object: IRelatedObject) {
        if (!values.objects.find((item) => item.id === object.id)) {
            setFieldValue("objects", [...values.objects, object]);
        }
    }

    function removeObject(object: IRelatedObject) {
        setFieldValue(
            "objects",
            values.objects.filter((item: IRelatedObject) => item.id !== object.id)
        );
    }

    function onScopeChange(e: ChangeEvent<HTMLSelectElement>) {
        setScope(e.target.value);
    }

    return (
        <form onSubmit={handleSubmit}>
            <FormAccordionContent className="f-event-related">
                <Row gutter="sm">
                    <Column>
                        <FormCaption>Scope</FormCaption>
                        <Select options={OBJECT_SEARCH_SCOPES} value={scope} onChange={onScopeChange} />
                    </Column>
                </Row>
                <Row gutter="sm">
                    <Column>
                        <FormCaption>Assign to Object</FormCaption>
                        <div>
                            <AutocompleteInput
                                searchFor="objects"
                                searchScope={scope}
                                className="f-event-related__objects"
                                organizationID={scopeOrganizationID ? scopeOrganizationID : undefined}
                                placeholder="Search objects..."
                                onChange={addObject}
                                name="objects"
                                marginBottom="none"
                                isSetValue={false}
                            />
                        </div>

                        <div className="flex align-center flex-wrap f-event-related button-group">
                            {values.objects.map((object) => (
                                <RemoveObjectButton
                                    text={object.name}
                                    type="button"
                                    onClick={() => removeObject(object)}
                                    key={object.name}
                                />
                            ))}
                        </div>
                    </Column>
                </Row>
            </FormAccordionContent>
            {!submitFormOnChange && <FormFooterSave />}

            {submitFormOnChange && onClickCreateButton && (
                <footer className="f-accordion__footer">
                    <SaveButton onClick={onClickCreateButton} />
                </footer>
            )}
        </form>
    );
};

export const EventRelatedObjectsForm: FunctionComponent<IEventRelatedObjectsFormProps> = ({
    onSubmit,
    onClickCreateButton,
    initialValues,
    showRelatedObjectsTable,
    submitFormOnChange,
    scopeOrganizationID,
    scopeObjectID,
}) => {
    return (
        <Formik
            onSubmit={onSubmit}
            initialValues={initialValues || EVENT_RELATED_OBJECTS_FORM_STATE}
            // validationSchema={EventRelatedObjectsFormValidationSchema}
            component={(formikProps) => (
                <EventRelatedObjectsFormBase
                    onClickCreateButton={onClickCreateButton}
                    showRelatedObjectsTable={showRelatedObjectsTable}
                    submitFormOnChange={submitFormOnChange}
                    scopeOrganizationID={scopeOrganizationID}
                    scopeObjectID={scopeObjectID}
                    {...formikProps}
                />
            )}
        />
    );
};
