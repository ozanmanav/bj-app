import { FormikProps } from "formik";
import * as Yup from "yup";

export interface IRelatedObject {
    id: string;
    name: string;
    address: string;
    type: string;
}

export const EVENT_RELATED_OBJECTS_FORM_STATE = {
    objects: [],
};

export interface IEventRelatedObjectsFormState {
    objects: IRelatedObject[];
}

export interface IEventRelatedObjectsFormBaseProps extends FormikProps<IEventRelatedObjectsFormState> {
    showRelatedObjectsTable?: boolean;
    onClickCreateButton?: () => void;
    submitFormOnChange?: boolean;
    scopeObjectID?: string;
    scopeOrganizationID?: string;
}

export interface IEventRelatedObjectsFormProps {
    showRelatedObjectsTable?: boolean;
    onSubmit: (state: IEventRelatedObjectsFormState) => void;
    onClickCreateButton?: () => void;
    initialValues?: IEventRelatedObjectsFormState;
    submitFormOnChange?: boolean;
    scopeObjectID?: string;
    scopeOrganizationID?: string;
}

export const parentObj = [
    {
        title: "Laurenzenvorstadt",
        name: "Street",
        type: "street",
    },
    {
        title: "Building Zenex",
        name: "BuildingDetails",
        type: "building",
    },
];

export const siblingObj = [
    {
        title: "Appartment 2",
        name: "Appartment",
        type: "building",
    },
];

export const EventRelatedObjectsFormValidationSchema = Yup.object().shape({
    objects: Yup.array(),
});
