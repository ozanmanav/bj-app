import { FormikProps } from "formik";
import * as Yup from "yup";
import gql from "graphql-tag";
import { IUser } from "../../../../../components/forms/addUserForm/definitions";

// Type Definitions
export interface IUserObject {
    user: IUser;
}

export interface IAssignmentObject {
    name: string;
    id: string;
}

export const EVENT_ASSIGNMENT_FORM_STATE = {
    role_id: "",
    users: [],
    organizations: [],
    fileIDs: [],
};

export interface IEventAssignmentFormState {
    role_id?: string;
    users?: IAssignmentObject[];
    organizations: IAssignmentObject[];
    fileIDs: string[];
}

export interface IEventAssignmentFormBaseProps extends FormikProps<IEventAssignmentFormState> {
    eventName?: string;
    submitFormOnChange?: boolean;
}

export interface IEventAssignmentFormProps {
    onSubmit: (state: IEventAssignmentFormState) => void;
    initialValues?: IEventAssignmentFormState;
    eventName?: string;
    submitFormOnChange?: boolean;
}

export const EventAssignmentFormValidationSchema = Yup.object().shape({
    users: Yup.array(),
    organizations: Yup.array(),
    fileIDs: Yup.array(),
});

export const GET_ORGANIZATION_EMPLOYEES = gql`
    query GetOrganizationEmployees($id: String) {
        organizations(id: $id) {
            data {
                id
                name
                teams {
                    id
                    name
                    users
                    users_data {
                        id
                        name
                        first_name
                        email
                        phone
                    }
                }
                employees {
                    id
                    job_id
                    invited
                    user_role {
                        id
                        type
                        name
                    }
                    job {
                        id
                        type
                        name
                    }
                    user {
                        id
                        first_name
                        last_name
                        email
                        phone
                        language
                    }
                }
            }
        }
    }
`;

export const GET_FILES = gql`
    query GetFiles($ids: [String]) {
        files(ids: $ids) {
            data {
                id
                type {
                    id
                    name
                }
                name
                url
                filename
                extension
                size
                created_at
                updated_at
                details {
                    context
                    created_at
                    id
                    type
                    updated_at
                    value
                    meta {
                        last_modification {
                            modified_date
                            modifier {
                                name
                            }
                        }
                        source
                        source_date
                        source_url
                        verification
                        verification_by
                    }
                }
            }
        }
    }
`;

export const GET_CONTRACT_TYPES = gql`
    query GetContractTypes {
        entity_types(entity: "CONTRACT") {
            data {
                id
                name
                groups {
                    name
                }
            }
        }
    }
`;
