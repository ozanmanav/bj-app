import React, { FunctionComponent, useEffect, useState } from "react";
import { Formik } from "formik";
import {
    IEventAssignmentFormProps,
    EVENT_ASSIGNMENT_FORM_STATE,
    IEventAssignmentFormBaseProps,
    IAssignmentObject,
    EventAssignmentFormValidationSchema,
    GET_FILES,
    GET_CONTRACT_TYPES,
} from "./definitions";
import "./EventAssignmentForm.scss";
import { RemoveObjectButton, Row, Column, AutocompleteInput, Select } from "../../../../../components/ui";
import { FormAccordionContent, FormFooterNext, FormCaption } from "../../../../../components/forms";
import { FilesList } from "../../../../../components/filesList";
import isEmpty from "ramda/es/isEmpty";
import { useApolloClient, useQuery } from "@apollo/react-hooks";
import get from "lodash.get";
import isNil from "ramda/es/isNil";
import useUpdateEffect from "../../../../../hooks/useUpdateEffect";
import { Loader } from "../../../../../components/ui/loader";
import { getRelationshipRolesOptions } from "../../../../../utils";

const EventAssignmentFormBase: FunctionComponent<IEventAssignmentFormBaseProps> = ({
    values,
    handleSubmit,
    handleChange,
    handleBlur,
    errors,
    touched,
    eventName = "",
    setFieldValue,
    submitFormOnChange = false,
}) => {
    const [loading, setLoading] = useState(false);
    const [files, setFiles] = useState([]);
    const apolloClient = useApolloClient();

    const { data: contractTypesData } = useQuery(GET_CONTRACT_TYPES);
    const roles = get(contractTypesData, "entity_types.data") || [];

    useUpdateEffect(() => {
        if (submitFormOnChange) {
            handleSubmit();
        }
    }, [values]);

    useEffect(() => {
        const getFilesDetails = async (uploadedIDs: string[]) => {
            setLoading(true);
            const { data: filesDetailsData } = await apolloClient.query({
                query: GET_FILES,
                variables: {
                    ids: uploadedIDs,
                },
                fetchPolicy: "no-cache",
            });

            const filesDetails = get(filesDetailsData, "files.data");

            if (filesDetails) {
                await setFiles(filesDetails);
                setLoading(false);
            }
        };

        if (!isEmpty(values.fileIDs) && !isNil(values.fileIDs)) {
            getFilesDetails(values.fileIDs);
        } else {
            setFiles([]);
        }
    }, [values.fileIDs]);

    function removeOrganization(organization: IAssignmentObject) {
        setFieldValue(
            "organizations",
            values.organizations.filter((item: IAssignmentObject) => item.id !== organization.id)
        );
    }

    function addOrganization(organization: IAssignmentObject) {
        if (!values.organizations.find((item) => item.id === organization.id)) {
            setFieldValue("organizations", [...values.organizations, organization]);
        }
    }

    const filesChangeCallback = (uploadedFileIDs: string[] = []) => {
        setFieldValue("fileIDs", [...values.fileIDs, ...uploadedFileIDs]);
    };
    const filesDeleteCallback = (deletedFileIDs: string[] = []) => {
        setFieldValue("fileIDs", [...values.fileIDs.filter((id) => !deletedFileIDs.includes(id))]);
    };

    return (
        <form onSubmit={handleSubmit} data-testid="event-assignment-form">
            <FormAccordionContent className="f-event-assignment__top-row">
                <Row gutter="sm">
                    <Column>
                        <FormCaption>Assign to organization</FormCaption>
                        <AutocompleteInput
                            searchFor="allOrganizations"
                            onChange={addOrganization}
                            placeholder="Start typing to find organization..."
                            marginBottom="sm"
                            isSetValue={false}
                        />
                        <div className="f-event-assignment__remove-button flex align-center flex-wrap">
                            {values.organizations.map((organization) => (
                                <RemoveObjectButton
                                    className="f-event-assignment__remove-button"
                                    text={organization.name}
                                    type="button"
                                    onClick={() => removeOrganization(organization)}
                                    key={organization.id}
                                />
                            ))}
                        </div>
                    </Column>
                </Row>
                {values.organizations.length > 0 && (
                    <Row gutter="sm">
                        <Column>
                            <FormCaption>Select Role</FormCaption>
                            <Select
                                options={getRelationshipRolesOptions(roles)}
                                placeholder="Select Role"
                                className="b-new-relationship__select"
                                name="role_id"
                                value={values.role_id}
                                error={errors && errors.role_id}
                                touched={touched && touched.role_id}
                                onChange={handleChange}
                                onBlur={handleBlur}
                            />
                        </Column>
                    </Row>
                )}

                <br />
            </FormAccordionContent>
            <FormAccordionContent>
                {loading ? (
                    <div className="flex justify-center">
                        <Loader rawLoader />
                    </div>
                ) : (
                    <FilesList
                        addFilesTo={eventName}
                        files={files}
                        hideFilter
                        filesChangeCallback={filesChangeCallback}
                        filesDeleteCallback={filesDeleteCallback}
                        loading={loading}
                    />
                )}
            </FormAccordionContent>
            {!submitFormOnChange && <FormFooterNext />}
        </form>
    );
};

export const EventAssignmentForm: FunctionComponent<IEventAssignmentFormProps> = ({
    onSubmit,
    initialValues,
    submitFormOnChange,
    eventName,
}) => {
    return (
        <Formik
            onSubmit={onSubmit}
            enableReinitialize={true}
            initialValues={initialValues || EVENT_ASSIGNMENT_FORM_STATE}
            validationSchema={EventAssignmentFormValidationSchema}
            component={(formikProps) => (
                <EventAssignmentFormBase {...formikProps} eventName={eventName} submitFormOnChange={submitFormOnChange} />
            )}
        />
    );
};
