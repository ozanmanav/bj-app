import React, { FunctionComponent, useState, useEffect } from "react";
import { Accordion, useAccordions, Container, handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../../../components/ui";
import { useQuery, useMutation, useApolloClient } from "@apollo/react-hooks";
import { EventContentHeader } from "../../../../components/contentHeaders";
import { EventBasicInfoForm } from "./eventBasicInfoForm";
import { EventAssignmentForm } from "./eventAssignmentForm";
import { EventRelatedObjectsForm } from "./eventRelatedObjectsForm";
import { GET_EVENT_TYPES, IEventBasicInfoFormState, EVENT_BASIC_INFO_FORM_STATE } from "./eventBasicInfoForm/definitions";
import { normalizeEventTypes, useVariantFromLocation } from "./utils";
import { IEventAssignmentFormState, EVENT_ASSIGNMENT_FORM_STATE } from "./eventAssignmentForm/definitions";
import { IEventRelatedObjectsFormState, EVENT_RELATED_OBJECTS_FORM_STATE } from "./eventRelatedObjectsForm/definitions";
import { ADD_EVENT, GET_ORGANIZATION, GET_OBJECT, GET_CONTRACT, GET_RELATIONSHIP } from "./queries";
import { useHistory, useLocation } from "react-router";
import get from "lodash.get";
import { EventRelatedEventsForm } from "./eventRelatedEventsForm";
import { IEventRelatedEventsFormState, EVENT_RELATED_EVENTS_FORM_STATE } from "./eventRelatedEventsForm/definitions";
import { Loader } from "../../../../components/ui/loader";
import EventsTimeline from "../../../../components/events/EventsTimeline";
import { GET_EVENTS } from "../../../../components/events/EventsTimeline/queries";
import { getLocationSearchParam } from "../../../../utils/locationSearchHelpers";
import { prepareDataForEventMutation } from "../eventDetail/utils";
import { IAssignmentObject } from "../../task/addTask/taskRelatedTasksForm/definitions";

export const AddEvent: FunctionComponent = () => {
    const { isOpened, openAccordion, closeAccordion, toggleAccordion } = useAccordions([1]);
    const [title, setTitle] = useState<string | undefined>();
    const [subtitle, setSubtitle] = useState<string | undefined>();
    const [headerRightInfo, setHeaderRightInfo] = useState<string | undefined>();
    const [loading, setLoading] = useState<boolean>(true);
    const [basicInfoFormState, setBasicInfoFormState] = useState<IEventBasicInfoFormState>(EVENT_BASIC_INFO_FORM_STATE);
    const [assignmentFormState, setAssignmentFormState] = useState<IEventAssignmentFormState>(EVENT_ASSIGNMENT_FORM_STATE);
    const [relatedEventsFormState, setRelatedEventsFormState] = useState<IEventRelatedEventsFormState>(EVENT_RELATED_EVENTS_FORM_STATE);
    const [relatedObjectsFormState, setRelatedObjectsFormState] = useState<IEventRelatedObjectsFormState>(EVENT_RELATED_OBJECTS_FORM_STATE);

    const location = useLocation();
    const { variantID, variant } = useVariantFromLocation(location);
    const relationshipID = getLocationSearchParam(get(location, "search"), "relationshipID");
    const contractID = getLocationSearchParam(get(location, "search"), "contractID");
    const detailID = getLocationSearchParam(get(location, "search"), "detailID");
    const fileID = getLocationSearchParam(get(location, "search"), "fileID");

    useEffect(() => {
        window.scrollTo(0, 0);

        if (variant === "object") {
            const getObject = async () => {
                try {
                    const { data: objectData } = await apolloClient.query({
                        query: GET_OBJECT,
                        variables: {
                            id: variantID,
                        },
                        fetchPolicy: "no-cache",
                    });

                    const object = get(objectData, "objects.data[0]");

                    const objectName = get(object, "name");
                    const objectTypeName = get(object, "type.name");

                    if (objectName) {
                        setTitle(objectName);
                        setSubtitle(`Object Type:  ${objectTypeName}`);
                        setLoading(false);
                    }
                } catch (e) {
                    showErrorToast(e.message);
                }
            };

            getObject();
        }

        if (variant === "organization") {
            const getOrganization = async () => {
                try {
                    const { data: organizationData } = await apolloClient.query({
                        query: GET_ORGANIZATION,
                        variables: {
                            id: variantID,
                        },
                        fetchPolicy: "no-cache",
                    });

                    const organization = get(organizationData, "organizations.data[0]");

                    const organizationName = get(organization, "name");
                    if (organizationName) {
                        setTitle(organizationName);
                        setLoading(false);
                    }
                } catch (e) {
                    showErrorToast(e.message);
                }
            };

            getOrganization();
        }

        if (contractID) {
            const getContract = async () => {
                try {
                    const { data: contractData } = await apolloClient.query({
                        query: GET_CONTRACT,
                        variables: {
                            id: contractID,
                        },
                        fetchPolicy: "no-cache",
                    });

                    const contract = get(contractData, "contracts.data[0]");
                    const organizationName = get(contract, "organization.name");
                    const organizationID = get(contract, "organization.id");
                    const contractOrganizationAssigmentObject: IAssignmentObject = {
                        name: organizationName,
                        id: organizationID,
                    };

                    setAssignmentFormState({
                        ...assignmentFormState,
                        organizations: [...[contractOrganizationAssigmentObject], ...assignmentFormState.organizations],
                    });

                    const roleName = get(contract, "role.name");

                    if (contract) {
                        setHeaderRightInfo(`Contract: ${roleName} ${organizationName}`);
                        setLoading(false);
                    }
                } catch (e) {
                    showErrorToast(e.message);
                }
            };

            getContract();
        }

        if (relationshipID) {
            const getRelationship = async () => {
                try {
                    const { data: relationshipData } = await apolloClient.query({
                        query: GET_RELATIONSHIP,
                        variables: {
                            id: relationshipID,
                        },
                        fetchPolicy: "no-cache",
                    });

                    const relationship = get(relationshipData, "relationships.data[0]");

                    if (relationship) {
                        const parentName = get(relationship, "higher.name");
                        const childName = get(relationship, "lower.name");
                        const otherRelationType = get(relationship, "type.name");

                        if (otherRelationType) {
                            const firstType = otherRelationType.split("-")[0];
                            const secondType = otherRelationType.split("-")[1];

                            setHeaderRightInfo(`Relationship:  ${parentName} (${firstType}) - ${childName} (${secondType})`);
                        } else {
                            setHeaderRightInfo(`Relationship: ${parentName} (Parent) - ${childName} (Child)`);
                        }
                        setLoading(false);
                    }
                } catch (e) {
                    showErrorToast(e.message);
                }
            };

            getRelationship();
        }

        return () => {
            setTitle(undefined);
        };
    }, [location.search]);

    const history = useHistory();
    const apolloClient = useApolloClient();
    const [addEventMutation] = useMutation(ADD_EVENT);
    const onAddEventSubmit = async () => {
        try {
            const addEventVariables = prepareDataForEventMutation(
                basicInfoFormState,
                assignmentFormState,
                relatedObjectsFormState,
                relatedEventsFormState,
                { detailID, fileID, variant, variantID, contractID, relationshipID }
            );

            const { data, errors } = await addEventMutation({ variables: addEventVariables });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                showSuccessToast("Event successfully created");
                const eventID = get(data, "createEvent.id");
                if (eventID) {
                    history.push(`/app/event/${eventID}`);
                }
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    };

    function onSubmitEventBasicInfoForm(basicInfo: IEventBasicInfoFormState) {
        setBasicInfoFormState(basicInfo);

        closeAccordion(1);
        openAccordion(2);
    }

    function onSubmitEventAssigmentForm(assigments: IEventAssignmentFormState) {
        setAssignmentFormState(assigments);
        closeAccordion(2);
        openAccordion(3);
    }

    function onSubmitEventRelatedEventsForm(relatedEvents: IEventRelatedEventsFormState) {
        setRelatedEventsFormState(relatedEvents);
        closeAccordion(3);
        openAccordion(4);
    }

    async function onSubmitEventRelatedObjectsForm(relatedObjects: IEventRelatedObjectsFormState) {
        setRelatedObjectsFormState(relatedObjects);
    }

    const { data: eventTypesData } = useQuery(GET_EVENT_TYPES);
    const { data: eventsData, refetch: eventsRefetch, loading: eventsLoading } = useQuery(GET_EVENTS, {
        variables: {
            ...(variant === "organization" && { organization_id: variantID }),
            ...(variant === "object" && { object_id: variantID }),
            strict: false,
        },
        fetchPolicy: "cache-first",
    });

    if (loading) {
        return <Loader />;
    }

    return (
        <Container>
            <EventContentHeader title={title} subtitle={subtitle} headerRightInfo={headerRightInfo} />
            <EventsTimeline
                showAddButton={false}
                variant={variant}
                variantID={variantID}
                events={eventsData}
                refetch={eventsRefetch}
                loading={eventsLoading}
            />
            <Accordion title="Basic Information" isOpen={isOpened(1)} toggle={toggleAccordion} index={1}>
                <EventBasicInfoForm onSubmit={onSubmitEventBasicInfoForm} eventTypes={normalizeEventTypes(eventTypesData)} />
            </Accordion>
            <Accordion
                title="Assignment"
                tooltip="Lorem ipsum"
                disabled={isOpened(1)}
                isOpen={isOpened(2)}
                toggle={toggleAccordion}
                index={2}
            >
                <EventAssignmentForm
                    initialValues={assignmentFormState}
                    onSubmit={onSubmitEventAssigmentForm}
                    eventName={get(basicInfoFormState, "eventName")}
                />
            </Accordion>
            <Accordion title="Related Events" disabled={isOpened(1) || isOpened(2)} isOpen={isOpened(3)} toggle={toggleAccordion} index={3}>
                <EventRelatedEventsForm
                    onSubmit={onSubmitEventRelatedEventsForm}
                    scopeObjectID={variant === "object" ? variantID : undefined}
                    scopeOrganizationID={variant === "organization" ? variantID : undefined}
                />
            </Accordion>
            <Accordion
                title="Related Objects"
                disabled={isOpened(1) || isOpened(2) || isOpened(3)}
                isOpen={isOpened(4)}
                toggle={toggleAccordion}
                index={4}
            >
                <EventRelatedObjectsForm
                    onSubmit={onSubmitEventRelatedObjectsForm}
                    scopeObjectID={variant === "object" ? variantID : undefined}
                    scopeOrganizationID={variant === "organization" ? variantID : undefined}
                    onClickCreateButton={onAddEventSubmit}
                    submitFormOnChange
                />
            </Accordion>
        </Container>
    );
};
