export type EventFrequencyEnum = "none" | "daily" | "weekly" | "monthly" | "yearly" | string;

export type EventFrequencyType = {
    type: EventFrequencyEnum;
    count?: number;
    days?: number[];
    months?: number[];
};

export type EventEndsOnEnum = "continuous" | "edited" | "date" | string;
