import { FormikProps } from "formik";
import * as Yup from "yup";

export interface IAssignmentObject {
    name: string;
    id: string;
}

export const EVENT_RELATED_EVENTS_FORM_STATE = {
    events: [],
};

export interface IEventRelatedEventsFormState {
    events: IAssignmentObject[];
}

export interface IEventRelatedEventsFormBaseProps extends FormikProps<IEventRelatedEventsFormState> {
    submitFormOnChange?: boolean;
    scopeObjectID?: string;
    scopeOrganizationID?: string;
}

export interface IEventRelatedEventsFormProps {
    onSubmit: (state: IEventRelatedEventsFormState) => void;
    initialValues?: IEventRelatedEventsFormState;
    submitFormOnChange?: boolean;
    scopeObjectID?: string;
    scopeOrganizationID?: string;
}

export const EventRelatedEventsFormValidationSchema = Yup.object().shape({
    events: Yup.array(),
});
