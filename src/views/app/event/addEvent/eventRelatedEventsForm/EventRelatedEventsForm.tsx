import React, { FunctionComponent } from "react";
import { Formik } from "formik";
import {
    IEventRelatedEventsFormProps,
    EVENT_RELATED_EVENTS_FORM_STATE,
    IEventRelatedEventsFormBaseProps,
    IAssignmentObject,
} from "./definitions";
import "./EventRelatedEventsForm.scss";
import { RemoveObjectButton, Row, Column, AutocompleteInput } from "../../../../../components/ui";
import { FormAccordionContent, FormFooterNext, FormCaption } from "../../../../../components/forms";
import useUpdateEffect from "../../../../../hooks/useUpdateEffect";

const EventRelatedEventsFormBase: FunctionComponent<IEventRelatedEventsFormBaseProps> = ({
    scopeObjectID,
    scopeOrganizationID,
    values,
    handleSubmit,
    setFieldValue,
    submitFormOnChange,
}) => {
    useUpdateEffect(() => {
        if (submitFormOnChange) {
            handleSubmit();
        }
    }, [values]);

    function removeEvent(event: IAssignmentObject) {
        setFieldValue(
            "events",
            values.events.filter((item: IAssignmentObject) => item.id !== event.id)
        );
    }

    function addEvent(event: IAssignmentObject) {
        if (!values.events.find((item) => item.id === event.id)) {
            setFieldValue("events", [...values.events, event]);
        }
    }

    return (
        <form onSubmit={handleSubmit} data-testid="event-assigment-form">
            <FormAccordionContent className="f-event-assignment__top-row">
                <Row gutter="sm">
                    <Column>
                        <FormCaption>Assign to events</FormCaption>
                        <AutocompleteInput
                            searchFor="events"
                            organizationID={scopeOrganizationID}
                            scopeObjectID={scopeObjectID}
                            onChange={addEvent}
                            placeholder="Start typing to find events..."
                            marginBottom="sm"
                            isSetValue={false}
                        />
                        <div className="f-event-assignment__remove-button flex align-center flex-wrap">
                            {values.events.map((event) => (
                                <RemoveObjectButton
                                    className="f-event-assignment__remove-button"
                                    text={event.name}
                                    type="button"
                                    onClick={() => removeEvent(event)}
                                    key={event.id}
                                />
                            ))}
                        </div>
                    </Column>
                </Row>
            </FormAccordionContent>
            {!submitFormOnChange && <FormFooterNext />}
        </form>
    );
};

export const EventRelatedEventsForm: FunctionComponent<IEventRelatedEventsFormProps> = ({
    onSubmit,
    initialValues,
    submitFormOnChange,
    scopeOrganizationID,
    scopeObjectID,
}) => {
    return (
        <Formik
            onSubmit={onSubmit}
            initialValues={initialValues || EVENT_RELATED_EVENTS_FORM_STATE}
            // validationSchema={EventRelatedEventsFormValidationSchema}
            component={(formikProps) => (
                <EventRelatedEventsFormBase
                    {...formikProps}
                    submitFormOnChange={submitFormOnChange}
                    scopeOrganizationID={scopeOrganizationID}
                    scopeObjectID={scopeObjectID}
                />
            )}
        />
    );
};
