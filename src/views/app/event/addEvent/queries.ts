import gql from "graphql-tag";

export const ADD_EVENT = gql`
    mutation AddEvent(
        $name: String!
        $type_id: String!
        $start_date: String!
        $end_date: String
        $frequency: CreateEventFrequencyType
        $ends_on: CreateEventEndsOnEnum!
        $related_object_ids: [String]
        $related_organization_ids: [String]
        $related_event_ids: [String]
        $file_ids: [String]
        $object_id: String
        $organization_id: String
        $contract_id: String
        $relationship_id: String
        $timeline: Boolean!
        $detail_id: String
        $file_id: String
        $role_id: String
    ) {
        createEvent(
            name: $name
            type_id: $type_id
            start_date: $start_date
            end_date: $end_date
            frequency: $frequency
            ends_on: $ends_on
            related_object_ids: $related_object_ids
            related_organization_ids: $related_organization_ids
            related_event_ids: $related_event_ids
            file_ids: $file_ids
            object_id: $object_id
            organization_id: $organization_id
            contract_id: $contract_id
            relationship_id: $relationship_id
            timeline: $timeline
            detail_id: $detail_id
            file_id: $file_id
            role_id: $role_id
        ) {
            id
        }
    }
`;

export const GET_ORGANIZATION = gql`
    query GetOrganization($id: String!) {
        organizations(id: $id) {
            data {
                id
                name
            }
        }
    }
`;

export const GET_OBJECT = gql`
    query GetObject($id: String!) {
        objects(id: $id) {
            data {
                id
                name
                type {
                    name
                }
            }
        }
    }
`;

export const GET_CONTRACT = gql`
    query GetContract($id: String!) {
        contracts(id: $id) {
            data {
                id
                organization {
                    id
                    name
                }
                role {
                    name
                }
            }
        }
    }
`;

export const GET_RELATIONSHIP = gql`
    query GetRelationship($id: String!) {
        relationships(id: $id) {
            data {
                id
                type {
                    name
                }
                higher {
                    name
                }
                lower {
                    name
                }
            }
        }
    }
`;
