import React, { FunctionComponent, ReactText } from "react";
import { Formik } from "formik";
import { Select, Input, Checkbox, Row, Column, DatepickerSimple, RadioButtonsGroupMultiple } from "../../../../../components/ui";
import {
    IEventBasicInfoFormBaseProps,
    IEventBasicInfoFormProps,
    EVENT_BASIC_INFO_FORM_STATE,
    EventBasicInfoFormValidationSchema,
    occurenceTypes,
    reccurences,
    weekdays,
} from "./definitions";
import "./EventBasicInfoForm.scss";
import { FormFooterNext, FormAccordionContent, FormCaption, FormFooterSave } from "../../../../../components/forms";
import contains from "ramda/es/contains";
import get from "lodash.get";
import { parseDateForDatePicker, formatDate } from "../../../../../components/ui/datepicker/utils";
import { DATE_FORMAT, DATE_FORMAT_WITH_TIME } from "../../../../../config";

export const EventBasicInfoFormBase: FunctionComponent<IEventBasicInfoFormBaseProps> = ({
    values,
    handleSubmit,
    handleChange,
    errors,
    touched,
    eventTypes,
    setFieldValue,
    singleSubmit,
}) => {
    const onChangeWeekDays = (value: ReactText) => {
        setFieldValue(
            "frequencyDays",
            contains(value, values.frequencyDays) ? values.frequencyDays.filter((e) => e !== value) : [...values.frequencyDays, value]
        );
    };

    const onChangeEndsOnCheckbox = (event: React.ChangeEvent<HTMLInputElement>) => {
        setFieldValue("endsOn", get(event, "currentTarget.name"));
    };

    const onChangeStartDate = (date: Date) => {
        setFieldValue(`dateStart`, formatDate(date, DATE_FORMAT));
    };

    const onChangeEndDate = (date: Date) => {
        setFieldValue(`dateEnd`, formatDate(date, DATE_FORMAT));
    };

    return (
        <form data-testid="EventBasicInfoFormBase" className="f-event-basic-info" onSubmit={handleSubmit}>
            <FormAccordionContent>
                <Row gutter="sm">
                    <Column>
                        <Select
                            options={eventTypes}
                            placeholder="Event type"
                            data-testid="eventType"
                            name="eventType"
                            onChange={handleChange}
                            value={values.eventType}
                            error={errors && errors.eventType}
                            touched={touched && touched.eventType}
                        />
                    </Column>
                    <Column>
                        <Input
                            placeholder="Event name"
                            name="eventName"
                            data-testid="eventName"
                            onChange={handleChange}
                            value={values.eventName}
                            error={errors && errors.eventName}
                            touched={touched && touched.eventName}
                        />
                    </Column>
                </Row>
                <FormCaption>Date</FormCaption>
                <Row gutter="sm">
                    <Column>
                        <DatepickerSimple
                            onChange={onChangeStartDate}
                            defaultValue={values.dateStart ? parseDateForDatePicker(values.dateStart, DATE_FORMAT_WITH_TIME) : undefined}
                            className="_left"
                        />
                    </Column>
                    <Column>
                        <Select
                            options={occurenceTypes}
                            placeholder="Occurence type"
                            name="frequencyType"
                            onChange={handleChange}
                            value={values.frequencyType}
                            error={errors && errors.frequencyType}
                            touched={touched && touched.frequencyType}
                        />
                    </Column>
                </Row>

                {values.frequencyType === "custom" && (
                    <div>
                        <FormCaption>Custom recurrence</FormCaption>
                        <Row gutter="sm">
                            <Column className="flex align-center">
                                <span className="f-event-basic-info__repeat-title">Repeat every</span>
                                <Input
                                    className="f-event-basic-info__input"
                                    type="number"
                                    min={1}
                                    step={1}
                                    squared
                                    name="reccurenceInterval"
                                    onChange={handleChange}
                                    value={values.reccurenceInterval}
                                    error={errors && errors.reccurenceInterval}
                                    touched={touched && touched.reccurenceInterval}
                                />
                                <Select
                                    className="f-event-basic-info__reccurence"
                                    options={reccurences}
                                    placeholder="Recurrence type"
                                    name="reccurence"
                                    onChange={handleChange}
                                    value={values.reccurence}
                                    error={errors && errors.reccurence}
                                    touched={touched && touched.reccurence}
                                />
                            </Column>
                            {values.reccurence === "weekly" && (
                                <Column className="flex align-center">
                                    <span className="f-event-basic-info__repeat-title">Repeat on</span>
                                    <RadioButtonsGroupMultiple
                                        name="frequencyDays"
                                        inputs={weekdays}
                                        selectedValues={values.frequencyDays}
                                        onClickButton={onChangeWeekDays}
                                        wrapperClassName="f-event-basic-info__repeat-radio-group"
                                    />
                                </Column>
                            )}
                        </Row>
                    </div>
                )}

                {values.frequencyType !== "none" && (
                    <>
                        <FormCaption className="f-event-basic-info__ends-title">Ends</FormCaption>
                        <Checkbox
                            label="Never"
                            name="continuous"
                            checked={values.endsOn === "continuous"}
                            onChange={onChangeEndsOnCheckbox}
                        />
                        {/* <Checkbox
                            label="When a certain information about the object is changed"
                            name="edited"
                            checked={values.endsOn === "edited"}
                            onChange={onChangeEndsOnCheckbox}
                        /> */}
                        <Checkbox label="On" name="date" checked={values.endsOn === "date"} onChange={onChangeEndsOnCheckbox}>
                            <div className={values.endsOn !== "date" ? "f-event-basic-info__hidden" : ""}>
                                <DatepickerSimple
                                    defaultValue={
                                        values.dateEnd ? parseDateForDatePicker(values.dateEnd, DATE_FORMAT_WITH_TIME) : undefined
                                    }
                                    onChange={onChangeEndDate}
                                />
                            </div>
                        </Checkbox>

                        <Checkbox
                            label="After"
                            name="occurrences"
                            checked={values.endsOn === "occurrences"}
                            onChange={onChangeEndsOnCheckbox}
                        >
                            <Input
                                className="f-event-basic-info__input"
                                disabled={values.endsOn !== "occurrences"}
                                type="number"
                                min={1}
                                step={1}
                                squared
                                marginBottom="none"
                                name="occurenceCount"
                                onChange={handleChange}
                                value={values.occurenceCount}
                                error={errors && errors.occurenceCount}
                                touched={touched && touched.occurenceCount}
                            />
                            <span className="_text-grey _text-uppercase f-event-basic-info__occurence">Occurrences</span>
                        </Checkbox>
                    </>
                )}
            </FormAccordionContent>
            {singleSubmit ? <FormFooterSave /> : <FormFooterNext />}
        </form>
    );
};

export const EventBasicInfoForm: FunctionComponent<IEventBasicInfoFormProps> = ({ singleSubmit, onSubmit, initialValues, eventTypes }) => {
    return (
        <Formik
            onSubmit={onSubmit}
            initialValues={initialValues || EVENT_BASIC_INFO_FORM_STATE}
            validationSchema={EventBasicInfoFormValidationSchema}
            component={(formikProps) => <EventBasicInfoFormBase {...formikProps} singleSubmit={singleSubmit} eventTypes={eventTypes} />}
        />
    );
};
