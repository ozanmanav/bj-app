import { FormikProps } from "formik";
import gql from "graphql-tag";
import * as Yup from "yup";
import { VALIDATION_ERRORS } from "../../../../../config";
import { ISelectOption } from "../../../../../components/ui";

export const EVENT_BASIC_INFO_FORM_STATE = {
    eventType: "",
    eventName: "",
    frequencyType: "none",
    frequencyDays: [],
    reccurence: "",
    occurenceCount: 1,
    reccurenceInterval: 1,
    dateStart: new Date().toISOString(),
    dateEnd: new Date().toISOString(),
    endsOn: "date",
};

export interface IEventBasicInfoFormState {
    eventType: string;
    eventName: string;
    frequencyType: string;
    frequencyDays: number[];
    reccurence: string;
    occurenceCount: number;
    reccurenceInterval: number;
    dateStart: string;
    dateEnd: string;
    endsOn: string;
}

export interface IEventBasicInfoFormBaseProps extends FormikProps<IEventBasicInfoFormState> {
    eventTypes: ISelectOption[];
    singleSubmit?: boolean;
}

export interface IEventBasicInfoFormProps {
    onSubmit: (state: IEventBasicInfoFormState) => void;
    initialValues?: IEventBasicInfoFormState;
    eventTypes: ISelectOption[];
    singleSubmit?: boolean;
}

// Constants
export const weekdays = [
    {
        label: "Sun",
        value: 0,
    },
    {
        label: "Mon",
        value: 1,
    },
    {
        label: "Tue",
        value: 2,
    },
    {
        label: "Wed",
        value: 3,
    },
    {
        label: "Thu",
        value: 4,
    },
    {
        label: "Fri",
        value: 5,
    },
    {
        label: "Sat",
        value: 6,
    },
];

export const occurenceTypes = [
    {
        label: "Just Once",
        value: "none",
    },
    {
        label: "Daily",
        value: "daily",
    },
    {
        label: "Weekly",
        value: "weekly",
    },
    {
        label: "Monthly",
        value: "monthly",
    },
    {
        label: "Yearly",
        value: "yearly",
    },
    {
        label: "Custom",
        value: "custom",
    },
];

export const reccurences = [
    {
        label: "Day",
        value: "daily",
    },
    {
        label: "Week",
        value: "weekly",
    },
    {
        label: "Month",
        value: "monthly",
    },
    {
        label: "Year",
        value: "yearly",
    },
];

// Queries
export const GET_EVENT_TYPES = gql`
    query EventTypes {
        event_types {
            id
            entity
            type
            name
            is_primary
            is_file
            groups {
                name
            }
        }
    }
`;

// Validation Schema
export const EventBasicInfoFormValidationSchema = Yup.object().shape({
    eventType: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    eventName: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
    frequencyType: Yup.string()
        .trim()
        .required(VALIDATION_ERRORS.required),
});
