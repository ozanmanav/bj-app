import { IEventBasicInfoFormState, EVENT_BASIC_INFO_FORM_STATE } from "../addEvent/eventBasicInfoForm/definitions";
import { IEvent } from "./definitions";
import { EventFrequencyType } from "../addEvent/definitions";
import { getArrayIDs } from "../addEvent/utils";
import { EVENT_ASSIGNMENT_FORM_STATE, IEventAssignmentFormState } from "../addEvent/eventAssignmentForm/definitions";
import { EVENT_RELATED_OBJECTS_FORM_STATE, IEventRelatedObjectsFormState } from "../addEvent/eventRelatedObjectsForm/definitions";
import { EVENT_RELATED_EVENTS_FORM_STATE, IEventRelatedEventsFormState } from "../addEvent/eventRelatedEventsForm/definitions";
import get from "lodash.get";

export const prepareDataForEventMutation = (
    basicInfoFormState?: IEventBasicInfoFormState,
    assignmentFormState?: IEventAssignmentFormState,
    relatedObjectsFormState?: IEventRelatedObjectsFormState,
    relatedEventsFormState?: IEventRelatedEventsFormState,
    extArgs?: any
) => {
    const {
        eventName,
        eventType,
        dateStart,
        dateEnd,
        frequencyType,
        frequencyDays,
        endsOn,
        reccurence,
        reccurenceInterval,
        occurenceCount,
    } = basicInfoFormState || EVENT_BASIC_INFO_FORM_STATE;
    const { organizations, fileIDs, role_id } = assignmentFormState || EVENT_ASSIGNMENT_FORM_STATE;
    const { objects } = relatedObjectsFormState || EVENT_RELATED_OBJECTS_FORM_STATE;
    const { events } = relatedEventsFormState || EVENT_RELATED_EVENTS_FORM_STATE;

    const organizationIDs = getArrayIDs(organizations);
    const objectIDs = getArrayIDs(objects);
    const eventsIDs = getArrayIDs(events);

    const frequency: EventFrequencyType = {
        type: frequencyType !== "custom" ? frequencyType : reccurence,
        ...(frequencyType === "custom" && reccurence === "weekly" && { days: frequencyDays }),
        ...(frequencyType === "custom" && { interval: reccurenceInterval }),
        ...(endsOn === "occurrences" && { count: occurenceCount }),
    };

    let data = {
        timeline: true,
        name: eventName,
        type_id: eventType,
        start_date: dateStart,
        frequency,
        ends_on: endsOn,
        related_object_ids: objectIDs,
        related_organization_ids: organizationIDs,
        related_event_ids: eventsIDs,
        file_ids: fileIDs,
        role_id,
    };

    if (extArgs) {
        const { detailID, fileID, variant, variantID, contractID, relationshipID } = extArgs;

        data = {
            ...data,
            ...(detailID && { detail_id: detailID }),
            ...(fileID && { file_id: fileID }),
            ...(endsOn === "date" && { end_date: dateEnd }),
            ...(variant === "object" && { object_id: variantID }),
            ...(variant === "organization" && { organization_id: variantID }),
            ...(contractID && { contract_id: contractID }),
            ...(relationshipID && { relationship_id: relationshipID }),
        };
    }

    return data;
};

export const normalizeDataBasicFormState = (eventDetail: IEvent): IEventBasicInfoFormState => {
    const {
        name,
        type_id,
        start_date,
        frequency = { type: undefined, count: undefined, interval: undefined, days: undefined },
        end_date,
    } = eventDetail;
    let basicFormState: IEventBasicInfoFormState;

    let endsOn = get(eventDetail, "ends_on");
    let frequencyType = get(frequency, "type");
    let reccurence = "never";

    if (frequency.interval) {
        frequencyType = "custom";
        reccurence = get(frequency, "type");
    }

    basicFormState = {
        eventName: name,
        eventType: type_id,
        dateStart: start_date,
        dateEnd: end_date,
        frequencyType: frequencyType || "",
        frequencyDays: get(frequency, "days") || [],
        reccurence: reccurence,
        occurenceCount: get(frequency, "count") || 0,
        reccurenceInterval: get(frequency, "interval") || 0,
        endsOn,
    };

    return basicFormState;
};
