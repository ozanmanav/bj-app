import { IAssignmentObject } from "../addEvent/eventAssignmentForm/definitions";
import { IRelatedObjectsTableObject } from "../../../../components/tables";
import { EventFrequencyType } from "../addEvent/definitions";

export interface IEvent {
    id: string;
    name: string;
    description?: string;
    type_id: string;
    type: {
        id: string;
        name: string;
    };
    frequency: {
        type: string;
        days: number[];
        months: number[];
        count: number;
        interval: number;
    };
    organization: { name: string };
    object: { name: string };
    ends_on: string;
    start_date: string;
    end_date: string;
    file_ids: string[];
    files: [];
    related_object_ids: string[];
    related_objects: IRelatedObjectsTableObject[];
    related_organization_ids: string[];
    related_organizations: IAssignmentObject[];
    related_event_ids: string[];
    related_events: IAssignmentObject[];
    role_id?: string;
}

export interface IEventTimelineExport {
    url?: string;
    name?: string;
    start_date?: string;
    related_objects?: string;
}

export interface IUpdateEvent {
    name?: string;
    type_id?: string;
    type?: {
        id?: string;
        name?: string;
    };
    frequency?: EventFrequencyType;
    ends_on?: string;
    start_date?: string;
    end_date?: string;
    file_ids?: string[];
    related_object_ids?: string[];
    related_organization_ids?: string[];
    related_event_ids?: string[];
    role_id?: string;
}
