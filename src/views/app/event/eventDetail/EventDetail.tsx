import React, { FunctionComponent, useEffect, useState, useRef } from "react";
import {
    Container,
    Accordion,
    handleGraphQLErrors,
    showSuccessToast,
    showErrorToast,
    PlusButton,
    useModal,
} from "../../../../components/ui";
import { EventHeader } from "./eventHeader";
import { useRouteMatch } from "react-router";
import get from "lodash.get";
import isNil from "ramda/es/isNil";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { GET_EVENT, UPDATE_EVENT } from "./queries";
import { Loader } from "../../../../components/ui/loader";
import { EventAssignmentForm } from "../addEvent/eventAssignmentForm";
import { IEvent, IUpdateEvent } from "./definitions";
import { IEventAssignmentFormState, EVENT_ASSIGNMENT_FORM_STATE } from "../addEvent/eventAssignmentForm/definitions";
import { IEventRelatedEventsFormState, EVENT_RELATED_EVENTS_FORM_STATE } from "../addEvent/eventRelatedEventsForm/definitions";
import { getArrayIDs, normalizeEventTypes } from "../addEvent/utils";
import { EventRelatedEventsForm } from "../addEvent/eventRelatedEventsForm";
import useUpdateEffect from "../../../../hooks/useUpdateEffect";
import { IEventVariant } from "../../../../components/events/EventsTimeline/definitions";
import { RelatedObjectsTable } from "../../../../components/tables";
import { FormAccordionContent } from "../../../../components/forms";
import "./EventDetail.scss";
import { AddRelatedObjectToEventModal } from "../../../../components/modals/addRelatedObjectToEventModal";
import { IEventRelatedObjectsFormState, EVENT_RELATED_OBJECTS_FORM_STATE } from "../addEvent/eventRelatedObjectsForm/definitions";
import { isNillOrEmpty } from "../../../../utils";
import EventsTimeline from "../../../../components/events/EventsTimeline";
import { GET_EVENTS } from "../../../../components/events/EventsTimeline/queries";
import { EventBasicInfoForm } from "../addEvent/eventBasicInfoForm";
import { IEventBasicInfoFormState, GET_EVENT_TYPES, EVENT_BASIC_INFO_FORM_STATE } from "../addEvent/eventBasicInfoForm/definitions";
import { normalizeDataBasicFormState, prepareDataForEventMutation } from "./utils";

export const EventDetail: FunctionComponent = () => {
    const { isOpen: isOpenAddRelatedObjects, open: openAddRelatedObjects, hide: hideAddRelatedObjects } = useModal();
    const [isEditMode, setIsEditMode] = useState(false);
    const match = useRouteMatch();
    const eventID = get(match, "params.id");

    const { data: eventTypesData } = useQuery(GET_EVENT_TYPES);
    const basicInfoRef = useRef<HTMLDivElement | null>(null);
    const headerRef = useRef<HTMLDivElement | null>(null);

    const [headerRightInfo, setHeaderRightInfo] = useState<string | undefined>();
    const [basicInfoFormState, setBasicInfoFormState] = useState<IEventBasicInfoFormState>(EVENT_BASIC_INFO_FORM_STATE);
    const [assignmentFormState, setAssignmentFormState] = useState<IEventAssignmentFormState>(EVENT_ASSIGNMENT_FORM_STATE);
    const [relatedEventsFormState, setRelatedEventsFormState] = useState<IEventRelatedEventsFormState>(EVENT_RELATED_EVENTS_FORM_STATE);
    const [relatedObjectsFormState, setRelatedObjectsFormState] = useState<IEventRelatedObjectsFormState>(EVENT_RELATED_OBJECTS_FORM_STATE);

    const { data: eventDetailData, loading: eventDetailLoading, refetch: refetchEventDetail } = useQuery(GET_EVENT, {
        variables: {
            id: eventID,
        },
        fetchPolicy: "no-cache",
    });

    const eventDetail = get(eventDetailData, "events.data[0]") as IEvent;

    const objectID = get(eventDetail, "object_id");
    const organizationID = get(eventDetail, "organization_id");

    const variant: IEventVariant = (objectID && "object") || (organizationID && "organization");
    const variantID = objectID || organizationID;

    useEffect(() => {
        if (isEditMode && basicInfoRef && basicInfoRef.current) {
            basicInfoRef.current.scrollIntoView({ behavior: "smooth", block: "center" });
        } else if (!isEditMode && headerRef && headerRef.current) {
            headerRef.current.scrollIntoView({ behavior: "smooth", block: "center" });
        }
    }, [isEditMode]);

    useEffect(() => {
        window.scrollTo(0, 0);

        if (eventDetail) {
            setBasicInfoFormState(normalizeDataBasicFormState(eventDetail));

            const contract = get(eventDetail, "contract");

            if (contract) {
                const organizationName = get(contract, "organization.name");
                const roleName = get(contract, "role.name");
                setHeaderRightInfo(`Contract: ${roleName} ${organizationName}`);
            }

            const relationship = get(eventDetail, "relationship");

            if (relationship) {
                const parentName = get(relationship, "higher.name");
                const childName = get(relationship, "lower.name");
                const otherRelationType = get(relationship, "type.name");

                if (otherRelationType) {
                    const firstType = otherRelationType.split("-")[0];
                    const secondType = otherRelationType.split("-")[1];

                    setHeaderRightInfo(`Relationship:  ${parentName} (${firstType}) - ${childName} (${secondType})`);
                } else {
                    setHeaderRightInfo(`Relationship: ${parentName} (Parent) - ${childName} (Child)`);
                }
            }
        }
    }, [eventDetail]);

    const { data: eventsData, refetch: eventsRefetch, loading: eventsLoading } = useQuery(GET_EVENTS, {
        variables: {
            // ...(variant === "organization" && { organization_id: variantID }),
            ...(variant === "object" && { object_id: variantID }),
            strict: false,
        },
        fetchPolicy: "cache-first",
    });

    const [updateEventMutation] = useMutation(UPDATE_EVENT);
    const updateEvent = async (updatedVariables: IUpdateEvent, refetchAfterUpdate?: boolean) => {
        try {
            const { errors } = await updateEventMutation({
                variables: { id: eventID, timeline: true, ...updatedVariables },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                showSuccessToast("Event successfully updated");
                if (refetchAfterUpdate) {
                    refetchEventDetail();
                }
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    };

    useUpdateEffect(() => {
        const { organizations, fileIDs = [], role_id } = assignmentFormState;

        const organizationIDs = getArrayIDs(organizations);

        updateEvent({
            related_organization_ids: organizationIDs,
            file_ids: fileIDs,
            role_id: role_id,
        });
    }, [assignmentFormState]);

    useUpdateEffect(() => {
        const { events } = relatedEventsFormState;

        const eventsIDs = getArrayIDs(events);

        updateEvent({ related_event_ids: eventsIDs });
    }, [relatedEventsFormState]);

    useUpdateEffect(() => {
        const { objects } = relatedObjectsFormState;

        const objectsIDs = getArrayIDs(objects);

        updateEvent({ related_object_ids: objectsIDs });
    }, [relatedObjectsFormState]);

    const onSubmitAddRelatedObjects = (values: IEventRelatedObjectsFormState) => {
        setRelatedObjectsFormState(values);
        refetchEventDetail();
    };

    function onSubmitEventBasicInfoForm(basicInfo: IEventBasicInfoFormState) {
        const { related_event_ids, related_object_ids, related_organization_ids, ...restBasicInfoPrepared } = prepareDataForEventMutation(
            basicInfo
        );

        updateEvent(restBasicInfoPrepared, true);

        setIsEditMode(false);
    }
    const eventDate = get(eventDetail, "start_date") || null;
    const date = eventDate ? new Date(eventDate) : new Date();
    const initialState = {
        initialTimelinePickerState: {
            startMonth: date.getMonth() + 1,
            startYear: date.getFullYear().toString(),
            endMonth: date.getMonth() + 1,
            endYear: date.getFullYear().toString(),
        },
        timelineFilterDates: {
            startDate: new Date(date.getFullYear(), date.getMonth(), 1),
            endDate: new Date(date.getFullYear(), date.getMonth() + 1, 0),
        },
    };

    if (eventDetailLoading) {
        return <Loader />;
    }

    if (isNil(eventDetail)) {
        return <div>Event not found.</div>;
    }

    const { related_organizations = [], related_objects = [], related_events = [], file_ids = [], name, role_id } = eventDetail;

    return (
        <Container className="b-event-detail">
            <div ref={headerRef}>
                <EventHeader event={eventDetail} headerRightInfo={headerRightInfo} isEditMode={isEditMode} setEditMode={setIsEditMode} />
            </div>

            <EventsTimeline
                showAddButton={false}
                variant={variant}
                variantID={variantID}
                events={eventsData}
                refetch={eventsRefetch}
                loading={eventsLoading}
                initialState={initialState}
            />
            {isEditMode && (
                <Accordion title="Edit Basic Information" tooltip="Lorem ipsum" isOpen={true} index={1} hideArrow>
                    <div ref={basicInfoRef}>
                        <EventBasicInfoForm
                            initialValues={basicInfoFormState}
                            singleSubmit={true}
                            onSubmit={onSubmitEventBasicInfoForm}
                            eventTypes={normalizeEventTypes(eventTypesData)}
                        />
                    </div>
                </Accordion>
            )}

            <Accordion title="Assignment" tooltip="Lorem ipsum" isOpen={true} index={1} hideArrow>
                <EventAssignmentForm
                    onSubmit={setAssignmentFormState}
                    submitFormOnChange
                    eventName={name}
                    initialValues={{
                        organizations: related_organizations,
                        fileIDs: file_ids,
                        role_id: role_id,
                    }}
                />
            </Accordion>
            <Accordion title="Relationships" isOpen={true} index={2} hideArrow>
                <EventRelatedEventsForm
                    onSubmit={setRelatedEventsFormState}
                    submitFormOnChange
                    initialValues={{
                        events: related_events,
                    }}
                />
                <FormAccordionContent>
                    <div className="b-event-detail__title-container">
                        <h3>Related Objects</h3>
                        <PlusButton onClick={openAddRelatedObjects} />
                    </div>
                    {!isNillOrEmpty(related_objects) ? (
                        <RelatedObjectsTable relatedObjects={related_objects} readOnly showRoleColumn={false} />
                    ) : (
                        <div>No related objects added.</div>
                    )}
                </FormAccordionContent>
            </Accordion>
            <AddRelatedObjectToEventModal
                relatedObjects={related_objects}
                hide={hideAddRelatedObjects}
                isOpen={isOpenAddRelatedObjects}
                refetch={onSubmitAddRelatedObjects}
            />
        </Container>
    );
};
