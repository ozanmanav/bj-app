import gql from "graphql-tag";

export const GET_EVENT = gql`
    query GetEvent($id: String!) {
        events(id: $id) {
            data {
                id
                name
                object_id
                object {
                    id
                    name
                    type {
                        name
                    }
                }
                organization_id
                organization {
                    id
                    name
                    type {
                        name
                    }
                }
                type_id
                type {
                    id
                    name
                }
                frequency {
                    type
                    days
                    months
                    interval
                    count
                }
                contract {
                    id
                    role {
                        name
                    }
                    organization {
                        name
                    }
                }
                relationship {
                    id
                    type {
                        name
                    }
                    higher {
                        name
                    }
                    lower {
                        name
                    }
                }
                ends_on
                start_date
                end_date
                file_ids
                related_object_ids
                related_objects {
                    id
                    name
                    type {
                        id
                        name
                        type
                    }
                    address
                    contracts {
                        id
                        role {
                            name
                        }
                    }
                }
                related_organization_ids
                related_organizations {
                    id
                    name
                }
                related_event_ids
                related_events {
                    id
                    name
                }
                role_id
            }
        }
    }
`;

export const UPDATE_EVENT = gql`
    mutation UpdateEvent(
        $id: String!
        $name: String
        $type_id: String
        $start_date: String
        $end_date: String
        $frequency: UpdateEventFrequencyType
        $ends_on: UpdateEventEndsOnEnum
        $related_object_ids: [String]
        $related_organization_ids: [String]
        $related_event_ids: [String]
        $file_ids: [String]
        $object_id: String
        $organization_id: String
        $timeline: Boolean!
        $role_id: String
    ) {
        updateEvent(
            id: $id
            name: $name
            type_id: $type_id
            start_date: $start_date
            end_date: $end_date
            frequency: $frequency
            ends_on: $ends_on
            related_object_ids: $related_object_ids
            related_organization_ids: $related_organization_ids
            related_event_ids: $related_event_ids
            file_ids: $file_ids
            object_id: $object_id
            organization_id: $organization_id
            timeline: $timeline
            role_id: $role_id
        ) {
            id
            name
        }
    }
`;
