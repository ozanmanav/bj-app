import React, { FunctionComponent } from "react";
import "./EventHeader.scss";
import { Icon, EditButton, RemoveIconButton } from "../../../../../components/ui";
import { IEvent } from "../definitions";
import { parseAndFormatDate, getFruquencyText, upperFirstLetter } from "./utils";
import get from "lodash.get";
import { Link } from "react-router-dom";

interface IEventHeaderProps {
    event: IEvent;
    headerRightInfo?: string;
    isEditMode?: boolean;
    setEditMode?: React.Dispatch<React.SetStateAction<boolean>>;
}

export const EventHeader: FunctionComponent<IEventHeaderProps> = ({
    event: { name, start_date, frequency, type, organization, object },
    headerRightInfo,
    isEditMode,
    setEditMode,
}) => {
    const typeName = get(type, "name");
    const organizationName = get(organization, "name") || "";
    const organizationType = get(organization, "type.name") || "";
    const objectName = get(object, "name") || "";
    const objectType = get(object, "type.name") || "";
    const objectID = get(object, "id");
    const organizationID = get(organization, "id");

    const activateEditMode = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        setEditMode && setEditMode(true);
    };

    const deActivateEditMode = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        setEditMode && setEditMode(false);
    };

    return (
        <div className="b-event-header">
            <div className="flex b-event-header__top-row">
                <div className="flex-column">
                    <div className="flex">
                        {" "}
                        <Icon icon="commissioning" className="b-event-header__event-icon" />
                        <h2 className="h1">{name}</h2>
                        <div className="b-event-header__edit-buttons ">
                            {isEditMode ? <RemoveIconButton onClick={deActivateEditMode} /> : <EditButton onClick={activateEditMode} />}
                        </div>{" "}
                    </div>

                    <div className="flex ">
                        <Icon icon="clocksGrey" className="b-event-header__expire-icon" />
                        <p className="h3">
                            {parseAndFormatDate(start_date)} <span className="_text-grey b-event-header__expire-separator">●</span>{" "}
                            {getFruquencyText(frequency)}
                        </p>
                    </div>
                </div>

                <div className="b-event-header__type">
                    <p className="h3 _font-bold _text-grey">Type: {upperFirstLetter(typeName)}</p>
                    {objectName && (
                        <Link to={`/app/object/${objectID}`}>
                            <p className="h3 _font-bold _text-grey">Related to: {upperFirstLetter(objectName)}</p>
                            <p className="h3 _font-bold _text-grey">Object Type: {upperFirstLetter(objectType)}</p>
                        </Link>
                    )}
                    {organizationName && (
                        <Link to={`/app/organizations/${organizationID}`}>
                            <p className="h3 _font-bold _text-grey">Organization: {upperFirstLetter(organizationName)}</p>
                            <p className="h3 _font-bold _text-grey">Organization Type: {upperFirstLetter(organizationType)}</p>
                        </Link>
                    )}
                    <div className="h3 _font-bold _text-grey"> {headerRightInfo}</div>
                </div>
            </div>
        </div>
    );
};
