import { parseDate } from "../../../../../components/ui/datepicker/utils";
import dateFnsFormat from "date-fns/format";
import { DateUtils } from "react-day-picker";
import { EventFrequencyType } from "../../addEvent/definitions";
import get from "lodash.get";
import isEmpty from "ramda/es/isEmpty";
import { DATE_FORMAT_WITH_TIME, DATE_FORMAT } from "../../../../../config";

export const parseAndFormatDate = (dateString: string) => {
    const parsedDate = parseDate(dateString, DATE_FORMAT_WITH_TIME);

    return DateUtils.isDate(parsedDate) ? dateFnsFormat(parsedDate, DATE_FORMAT) : "Invalid Date";
};

const getFrequencyDaysTexts = (day: number) => {
    switch (day) {
        case 0:
            return "Sunday";
        case 1:
            return "Monday";
        case 2:
            return "Tuesday";
        case 3:
            return "Wednesday";
        case 4:
            return "Thursday";
        case 5:
            return "Friday";
        case 6:
            return "Saturday";
        default:
            return "";
    }
};

export const getFruquencyText = (frequency: EventFrequencyType) => {
    const frequencyDays = get(frequency, "days") || [];
    const frequencyDaysTexts = frequencyDays.map((item: number) => getFrequencyDaysTexts(item));

    switch (frequency.type) {
        case "daily":
            return `Daily`;
        case "weekly":
            return `Weekly ${!isEmpty(frequencyDaysTexts) ? `on ${frequencyDaysTexts.map((item) => item)}` : ""}`;
        case "monthly":
            return "Monthly";
        case "yearly":
            return "Yearly";
        default:
            break;
    }
};

export const upperFirstLetter = (word: string) => {
    try {
        return word[0].toUpperCase() + word.substr(1);
    } catch (error) {
        console.log(error);
        return "";
    }
};
