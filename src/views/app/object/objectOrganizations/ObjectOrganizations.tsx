import React, { FunctionComponent, useContext } from "react";
import { Container } from "../../../../components/ui";
import { Organization } from "../../../../components/organization";
import { RouteComponentProps } from "react-router";
import { isBuildingRoute } from "../../../../utils/isBuildingRoute";
import EventsTimeline from "../../../../components/events/EventsTimeline";
import { AccountStateContext } from "../../../../contexts/accountStateContext";
import { useQuery } from "@apollo/react-hooks";
import { GET_EVENTS } from "../../../../components/events/EventsTimeline/queries";

export const ObjectOrganizations: FunctionComponent<RouteComponentProps<{ id: string }>> = ({ match, location }) => {
    const objectID = match.params.id;
    const isBuilding = isBuildingRoute(location);
    const { currentOrganization } = useContext(AccountStateContext);

    const { data: eventsData, refetch: eventsRefetch, loading: eventsLoading } = useQuery(GET_EVENTS, {
        variables: {
            object_id: objectID,
            strict: false,
        },
        fetchPolicy: "cache-first",
    });

    return (
        <Container>
            <Organization objectID={objectID} isBuilding={isBuilding} eventRefetch={eventsRefetch} />
            {currentOrganization && (
                <EventsTimeline
                    showAddButton={true}
                    showStrictSwitcher={true}
                    variant="object"
                    variantID={objectID}
                    events={eventsData}
                    refetch={eventsRefetch}
                    loading={eventsLoading}
                />
            )}
        </Container>
    );
};
