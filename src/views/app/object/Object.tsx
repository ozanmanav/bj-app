import React, { FunctionComponent } from "react";
import { Switch, Route } from "react-router-dom";
import { AddBuilding } from "./addBuilding";
import { ObjectDetails } from "./objectDetails";
import { ObjectHistory } from "./objectHistory";
import { ObjectAdmin } from "./objectAdmin";
import { ObjectOrganizations } from "./objectOrganizations";
import { ObjectRelated } from "./objectRelated";
import { Installations } from "./installations";
import { ContentHeader } from "../../../components/contentHeaders";
import { AddObject, AddObjectSimple } from "./addObject";
import { RemoveOrganization } from "./removeOrganization";

export const Object: FunctionComponent = () => {
    return (
        <>
            <ContentHeader />
            <Switch>
                <Route path="/app/building/add" component={AddBuilding} />
                <Route path="/app/object/add/simple" component={AddObjectSimple} />
                <Route path="/app/object/add" component={AddObject} />
                <Route path="/app/object/:objectID/organization/:organizationID/successor/:contractID" component={RemoveOrganization} />
                <Route path={["/app/building/:id", "/app/object/:id"]} exact component={ObjectDetails} />
                <Route path={["/app/building/:id/installations", "/app/object/:id/installations"]} component={Installations} />
                <Route
                    path={[
                        "/app/building/:id/history",
                        "/app/object/:id/history",
                        "/app/organizations/client/:id/history",
                        "/app/organizations/:id/history",
                    ]}
                    component={ObjectHistory}
                />
                <Route path={["/app/building/:id/admin", "/app/object/:id/admin"]} component={ObjectAdmin} />
                <Route path={["/app/building/:id/related", "/app/object/:id/related"]} component={ObjectRelated} />
                <Route path={["/app/building/:id/organizations", "/app/object/:id/organizations"]} exact component={ObjectOrganizations} />
            </Switch>
        </>
    );
};
