import React, { FunctionComponent } from "react";
import "./InstallationsOther.scss";
import { Container, SearchInput, PlusButton, RelationalListInstallations } from "../../../../../components/ui";

export const InstallationsOther: FunctionComponent = () => {
    return (
        <Container>
            <div className="b-installations-other">
                <h3 className="b-installations-other__title">
                    Installations <PlusButton />
                </h3>
                <SearchInput placeholder="Search related objects..." />
                <RelationalListInstallations />
            </div>
        </Container>
    );
};
