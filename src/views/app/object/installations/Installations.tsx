import React, { FunctionComponent } from "react";
import { InstallationsOther } from "./installationsOther";
import { InstallationsManufacturer } from "./installationsManufacturer";
import { withRoutingGuard } from "../../../../components/hocs/withRoutingGuard";

const InstallationsBase: FunctionComponent = () => {
    // TODO: check if manufacturer
    if (window.location.search.includes("manufacturer")) {
        return <InstallationsManufacturer />;
    }

    return <InstallationsOther />;
};

export const Installations = withRoutingGuard(InstallationsBase, {
    availableForRoles: ["manufacturer_administrator", "manufacturer_brand_manager", "manufacturer_object_manager"],
});
