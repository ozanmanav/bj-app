import React, { FunctionComponent } from "react";
import "./InstallationsManufacturer.scss";
import {
    Container,
    PlusButton,
    SearchInput,
    Icon,
    CheckboxSwitch,
    EditButton,
    CalendarAddButton,
    RemoveIconButton,
} from "../../../../../components/ui";
import EventsTimeline from "../../../../../components/events/EventsTimeline";

export const InstallationsManufacturer: FunctionComponent = () => {
    // const objectId = match.params.id;
    // const { data: eventsData, refetch: eventsRefetch, loading: eventsLoading } = useQuery(GET_EVENTS, {
    //     variables: {
    //         object_id: objectId,
    //         strict: false,
    //     },
    //     fetchPolicy: "cache-first",
    // });
    return (
        <Container className="b-installations-manufacturer">
            <h3 className="b-installations-manufacturer__title">
                Installations <PlusButton />
            </h3>
            <SearchInput placeholder="Search related objects" className="b-installations-manufacturer__search" />
            <div className="_not-last-group">
                <InstallationsTable />
            </div>
            <EventsTimeline events={[]} refetch={() => {}} loading={false} />
        </Container>
    );
};

const InstallationsTable: FunctionComponent = () => {
    return (
        <div className="b-table__wrapper b-installations-manufacturer__table">
            <table className="b-table">
                <thead className="b-table__head">
                    <tr className="b-table__row _head">
                        <th className="b-table__cell">Type of installation</th>
                        <th className="b-table__cell">Serial number</th>
                        <th className="b-table__cell">Product type</th>
                        <th className="b-table__cell">Owner</th>
                        <th className="b-table__cell">Building</th>
                    </tr>
                </thead>
                <tbody className="b-table__body">
                    {/* TODO: add sorting */}
                    <InstallationsTableRow />
                    <InstallationsTableRow />
                    <InstallationsTableRow />
                    <InstallationsTableRow />
                </tbody>
            </table>
        </div>
    );
};

const InstallationsTableRow: FunctionComponent = () => {
    return (
        <tr className="b-table__row">
            <td className="b-table__cell">
                <div className="flex align-center _font-bold">
                    <Icon icon="elevator" className="b-installations-manufacturer__table-type-icon" />
                    Elevator
                </div>
            </td>
            <td className="b-table__cell">19741702</td>
            <td className="b-table__cell">Type</td>
            <td className="b-table__cell">Owner name</td>
            <td className="b-table__cell">
                <div className="flex">
                    <div className="h6 b-installations-manufacturer__building-info">
                        <p className="_text-grey flex align-center b-installations-manufacturer__building-info-address">
                            <Icon icon="pinGrey" className="b-installations-manufacturer__building-info-pin" />
                            Kappelergasse 1 9876 Tihidorf
                        </p>
                        <p className="_font-bold">Pabst Building</p>
                    </div>
                    <div className="flex align-center b-installations-manufacturer__table-row-actions">
                        <CheckboxSwitch />
                        <EditButton />
                        <CalendarAddButton />
                        <RemoveIconButton />
                    </div>
                </div>
            </td>
        </tr>
    );
};
