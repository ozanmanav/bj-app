import React, { FunctionComponent } from "react";
import { ObjectHistoryTable } from "../../../../components/object";
import { Container } from "../../../../components/ui";

type IVariant = "buildings" | "objects" | "organizations" | "contracts" | "users" | "tasks" | "events";

export interface IObjectHistoryProps {
    variant: IVariant;
    variantID: string;
}
export const ObjectHistory: FunctionComponent<IObjectHistoryProps> = ({ variant, variantID }) => {
    return (
        <Container>
            <ObjectHistoryTable variant={variant} variantID={variantID} />
        </Container>
    );
};
