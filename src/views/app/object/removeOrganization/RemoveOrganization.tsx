import React, { FunctionComponent, useEffect, useState } from "react";
import "./RemoveOrganization.scss";
import { Container, Accordion, useAccordions, handleGraphQLErrors, showErrorToast } from "../../../../components/ui";
import {
    RemoveOrganizationBasicInfoForm,
    OrganizationForm,
    IOrganizationFormState,
} from "../../../../components/forms";
import gql from "graphql-tag";
import { useMutation, useQuery } from "@apollo/react-hooks";
import clonedeep from "lodash.clonedeep";
import { getObjectLink } from "../../../../utils";
import { RouteComponentProps } from "react-router";
import { IRemoveOrganizationBasicInfoFormState } from "../../../../components/forms/removeOrganizationsBasicInfoForm/RemoveOrganizationBasicInfoForm";
import { withRoutingGuard } from "../../../../components/hocs/withRoutingGuard";
import get from "lodash.get";
import { useSendContract } from "../../../../hooks/apollo";

const DELETE_CONTRACT = gql`
    mutation DeleteContract($id: String!) {
        deleteContract(id: $id)
    }
`;

const GET_OBJECT_AND_ORGANIZATION_NAME = gql`
    query ObjectAndOrganizationName($id: String!) {
        objects(id: $id) {
            data {
                name
                type {
                    id
                }
                organizations {
                    id
                    name

                    contracts {
                        id
                    }
                }
            }
        }
    }
`;

const removeOrganizationInitialState = {
    organization: {
        id: "",
        name: "",
        contracts: [],
    },
    objectType: "",
    basicInfo: {
        startDate: "",
        endDate: "",
        withoutCreating: false,
    },
    newRelationship: {
        relationships: [
            {
                organization_id: "",
                role: "",
                objects: [],
                files: [],
                contacts: [],
            },
        ],
        rows: {
            contractInformation: [],
        },
        files: [],
    },
};

const RemoveOrganizationBase: FunctionComponent<RouteComponentProps<{
    organizationID: string;
    objectID: string;
    contractID: string;
}>> = ({ match, history }) => {
    const objectID = match.params.objectID;
    const organizationID = match.params.organizationID;
    const contractID = match.params.contractID;

    const { isOpened, toggleAccordion, openAccordion, closeAccordion } = useAccordions([1]);

    const [removeContract] = useMutation(DELETE_CONTRACT);

    const [state, setState] = useState(removeOrganizationInitialState);

    const { data, loading } = useQuery(GET_OBJECT_AND_ORGANIZATION_NAME, {
        variables: {
            id: objectID,
            organization_id: organizationID,
        },
        fetchPolicy: "network-only",
    });

    const objectName = get(data, "objects.data[0].name");
    const organizationCategory = get(data, "organizations.data[0].category");
    const isClientOrganization = organizationCategory === "CLIENT";

    useEffect(() => {
        if (!loading) {
            const stateCopy = clonedeep(removeOrganizationInitialState);
            if (data.objects && data.objects.data) {
                stateCopy.objectType = data.objects.data[0].type;
                stateCopy.organization =
                    data.objects.data[0].organizations.find(({ id }: { id: string }) => id === organizationID) || {};
            }
            setState(stateCopy);
        }
    }, [data, loading, organizationID]);

    const sendContract = useSendContract();

    const onSendContract = async (contract: IOrganizationFormState) => {
        try {
            await sendContract({
                contractState: contract,
                objectID,
                redirectToObject: true,
                objectType: state.objectType,
            });
        } catch (e) {
            showErrorToast(e.message);
        }
    };

    const deleteRelationshipWithOrganization = async (contract?: IOrganizationFormState) => {
        try {
            const res = await removeContract({
                variables: {
                    id: contractID,
                    organization_id: organizationID,
                },
            });
            const { errors } = res;

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                if (contract) {
                    onSendContract(contract);
                } else {
                    history.push(`${getObjectLink(state.objectType, objectID)}`);
                }
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    };

    const onBasicInfoSubmit = (basicInfoState: IRemoveOrganizationBasicInfoFormState) => {
        if (basicInfoState.withoutCreating) {
            deleteRelationshipWithOrganization();
        } else {
            closeAccordion(1);
            openAccordion(2);
        }
    };

    return (
        <Container className="b-remove-organization__wrapper">
            <p className="b-remove-organization__subtitle h6 _text-grey _text-uppercase _font-bold">Add successor</p>
            <h3 className="b-remove-organization__name">{state.organization.name}</h3>
            <Accordion title="Basic Information" isOpen={isOpened(1)} toggle={toggleAccordion} index={1}>
                <RemoveOrganizationBasicInfoForm onSubmit={onBasicInfoSubmit} initialValues={state.basicInfo} />
            </Accordion>
            <Accordion
                title="Organization and People"
                tooltip="Lorem ipsum"
                isOpen={isOpened(2)}
                toggle={toggleAccordion}
                index={2}
            >
                <OrganizationForm
                    isClientOrganization={isClientOrganization}
                    onSubmit={deleteRelationshipWithOrganization}
                    initialValues={state.newRelationship}
                    objectID={objectID}
                    objectType={state.objectType}
                    objectName={objectName}
                />
            </Accordion>
        </Container>
    );
};

export const RemoveOrganization = withRoutingGuard(RemoveOrganizationBase, {
    availableForRoles: [
        "owner_administrator",
        "manufacturer_administrator",
        "manufacturer_brand_manager",
        "manufacturer_object_manager",
        "property_manager_administrator",
        "property_manager_building_manager",
        "service_provider_administrator",
    ],
});
