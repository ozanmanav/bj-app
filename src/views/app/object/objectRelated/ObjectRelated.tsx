import React, { FunctionComponent } from "react";
import "./ObjectRelated.scss";
import { Container, Row, Column } from "../../../../components/ui";
import { RelatedObject } from "../../../../components/object";
import { RouteComponentProps } from "react-router";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { GRAPHQL_ADMIN_CONTEXT, IObjectMinimal } from "../../../../config";
import get from "lodash.get";
import { getRelationObjectName } from "./utils";
import EventsTimeline from "../../../../components/events/EventsTimeline";
import { useRelationTypes } from "../../../../hooks/apollo";
import { GET_EVENTS } from "../../../../components/events/EventsTimeline/queries";

interface IObjectRelatedProps extends RouteComponentProps<{ id: string }> {}

const RELATED_OBJECTS = gql`
    query RelatedObjects($id: String!) {
        objects(id: $id) {
            data {
                id
                name
                address_parameters {
                    city
                }
                higherRelations {
                    id
                    type {
                        id
                        type
                    }
                    higher {
                        id
                        type {
                            id
                            name
                            type
                        }
                        name
                        events {
                            id
                            timeline
                        }
                    }
                }
                lowerRelations {
                    id
                    type {
                        id
                        type
                    }
                    lower {
                        id
                        type {
                            id
                            name
                            type
                        }
                        name
                        events {
                            id
                            timeline
                        }
                    }
                }
            }
        }
    }
`;

export const ObjectRelated: FunctionComponent<IObjectRelatedProps> = ({ match }) => {
    const objectID = match.params.id;

    const { data: eventsData, refetch: eventsRefetch, loading: eventsLoading } = useQuery(GET_EVENTS, {
        variables: {
            object_id: objectID,
            strict: false,
        },
        fetchPolicy: "cache-first",
    });

    const { data: relatedObjectsData, refetch } = useQuery(RELATED_OBJECTS, {
        variables: {
            id: objectID,
        },
        ...GRAPHQL_ADMIN_CONTEXT,
    });

    const objectName = get(relatedObjectsData, "objects.data[0].name") || "";
    const objectCity = get(relatedObjectsData, "objects.data[0].address_parameters.city", "");
    const higherRelations = get(relatedObjectsData, "objects.data[0].higherRelations") || [];
    const lowerRelations = get(relatedObjectsData, "objects.data[0].lowerRelations") || [];

    const relationGroups = [...higherRelations, ...lowerRelations].reduce((acc, item) => {
        const isHigher = item.hasOwnProperty("higher");
        const relatedObject = isHigher ? item.higher : item.lower;

        if (relatedObject) {
            const relationName = get(item, "type.type");
            const relationID = get(item, "id");
            const relationTypeID = get(item, "type.id");
            const relationObjectName = getRelationObjectName(relationName, relationName === "parent_child" ? isHigher : !isHigher);

            if (!acc[relationObjectName]) {
                acc[relationObjectName] = [];
            }

            acc[relationObjectName].push({
                ...relatedObject,
                relationID: relationID,
                relationTypeID: isHigher ? `lower_${relationTypeID}` : `higher_${relationTypeID}`,
            });
        }

        return acc;
    }, {});

    const parents = relationGroups.parent || [];
    const childs = relationGroups.child || [];
    const rest = Object.keys(relationGroups).reduce((acc, key) => {
        if (key === "parent" || key === "child") {
            return acc;
        }

        return acc.concat(
            relationGroups[key].map((item: IObjectMinimal) => ({
                ...item,
                relationType: key,
            }))
        );
    }, []);
    const relationshipTypes = useRelationTypes();
    let defaultRole = relationshipTypes.length ? relationshipTypes.filter((i: any) => i.id === "parent_child") : "";
    return (
        <Container className="b-object-related">
            <div className="flex align-center b-object-related__actions"></div>
            <Row gutter="sm">
                <Column>
                    <RelatedObject
                        data={parents}
                        title="Parent Objects"
                        variant="simple"
                        updateCallback={refetch}
                        addObjectLink={`/app/object/add?childID=${objectID}`}
                        relationRole="higher"
                        objectID={objectID}
                        objectName={objectName}
                        objectCity={objectCity}
                        defaultRole={defaultRole.length > 0 ? `lower_${defaultRole[0].id}` : ""}
                        eventRefetch={eventsRefetch}
                    />
                </Column>
                <Column>
                    <RelatedObject
                        data={childs}
                        title="Child Objects"
                        variant="simple"
                        updateCallback={refetch}
                        addObjectLink={`/app/object/add?parentID=${objectID}`}
                        relationRole="lower"
                        objectID={objectID}
                        objectName={objectName}
                        addMultipleChildObjects={true}
                        objectCity={objectCity}
                        defaultRole={defaultRole.length > 0 ? `higher_${defaultRole[0].id}` : ""}
                        eventRefetch={eventsRefetch}
                    />
                </Column>
            </Row>
            <RelatedObject
                data={rest}
                title="Other Related Objects"
                variant="combined"
                updateCallback={refetch}
                addObjectLink={`/app/object/add?parentID=${objectID}`}
                objectID={objectID}
                objectName={objectName}
                eventRefetch={eventsRefetch}
            />
            <EventsTimeline
                showStrictSwitcher={true}
                variant={"object"}
                variantID={objectID}
                events={eventsData}
                loading={eventsLoading}
                refetch={eventsRefetch}
            />
        </Container>
    );
};
