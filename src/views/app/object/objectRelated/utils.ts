export function getRelationObjectName(relationName: string, isHigher: boolean): string {
    if (!relationName) {
        return "";
    }

    const splittedName = relationName.split("_");

    if (splittedName.length < 2) {
        return "";
    }

    return splittedName[isHigher ? 0 : 1].toLowerCase();
}
