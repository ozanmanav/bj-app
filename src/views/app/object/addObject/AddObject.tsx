import React, { FunctionComponent, useEffect, useReducer, useState } from "react";
import { Container, Accordion, useAccordions, showErrorToast, handleGraphQLErrors } from "../../../../components/ui";
import {
    ObjectBasicInfoForm,
    RelationForm,
    TRelationFormState,
    IObjectBasicInfoFormState,
    RELATION_FORM_DEFAULT_STATE,
} from "../../../../components/forms";
import { RouteComponentProps } from "react-router";
import { AddObjectReducer } from "./AddObjectReducer";
import { useRelationTypes, useSubmit } from "../../../../hooks";
import { useApolloClient, useMutation, useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { generateAddressRefs, getParentChildRelationsID } from "../../../../utils";
import { getLocationSearchParam } from "../../../../utils/locationSearchHelpers";
import get from "lodash.get";
import { GRAPHQL_ADMIN_CONTEXT } from "../../../../config";

const addObjectInitialState = {
    basicInfo: {
        type: "",
        name: "",
    },
    relations: RELATION_FORM_DEFAULT_STATE,
};

const CREATE_OBJECT = gql`
    mutation CreateObject($objects: [Objects]) {
        createObject(objects: $objects) {
            id
        }
    }
`;

const CREATE_OBJECT_RELATIONSHIP = gql`
    mutation CreateRelationship($type_id: String!, $higher_id: String!, $lower_id: String!) {
        createRelationship(type_id: $type_id, higher_id: $higher_id, lower_id: $lower_id) {
            id
        }
    }
`;

const OBJECTS = gql`
    query Objects($id: String!) {
        objects(id: $id) {
            data {
                id
                name
                address_refs
                address

                type {
                    id
                    name
                    type
                }
            }
        }
    }
`;

const OBJECT_ADDRESS = gql`
    query ObjectLocation($id: String!) {
        objects(id: $id) {
            data {
                id
                address_parameters {
                    city
                }
            }
        }
    }
`;

const ADD_OBJECT_TO_ORGANIZATION = gql`
    mutation AddObjectToOrganization($organization_id: String!, $objects: [String]) {
        addOrganizationObjects(organization_id: $organization_id, objects: $objects) {
            id
        }
    }
`;

export const AddObject: FunctionComponent<RouteComponentProps> = ({ history, location }) => {
    const apolloClient = useApolloClient();
    const relationTypes = useRelationTypes();
    const parentRelationID = getParentChildRelationsID(relationTypes);

    const { isOpened, openAccordion, closeAccordion, goPrevAccordionItem } = useAccordions([1]);

    const [state, dispatch] = useReducer(AddObjectReducer, addObjectInitialState);

    const { startSubmit, stopSubmit } = useSubmit(onSelfSubmit);

    const [objectStreet, setObjectStreet] = useState<string>();

    const [createObjectMutation] = useMutation(CREATE_OBJECT);
    const [addObjectToOrganization] = useMutation(ADD_OBJECT_TO_ORGANIZATION);

    const childID = getLocationSearchParam(location.search, "childID");
    const parentID = getLocationSearchParam(location.search, "parentID");
    const organizationID = getLocationSearchParam(location.search, "organizationID");

    const { data: objectAddress } = useQuery(OBJECT_ADDRESS, {
        variables: {
            id: childID || parentID,
        },
        ...GRAPHQL_ADMIN_CONTEXT,
    });

    const objectCity = get(objectAddress, "objects.data[0].address_parameters.city", "");

    useEffect(() => {
        if (childID && parentRelationID) {
            updateRelation(childID, `higher_${parentRelationID}`);
        }
    }, [childID, parentRelationID]);

    useEffect(() => {
        if (parentID && parentRelationID) {
            updateRelation(parentID, `lower_${parentRelationID}`);
        }
    }, [parentID, parentRelationID]);

    async function updateRelation(id: string, relationType: string) {
        try {
            const { data, errors } = await apolloClient.query({
                query: OBJECTS,
                variables: {
                    id,
                },
            });

            if (relationType === `higher_${parentRelationID}`) {
                const currentStreet = get(data, "objects.data[0].address");

                setObjectStreet(currentStreet);
            }

            const object = get(data, "objects.data[0]");

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                dispatch({
                    type: "UPDATE_RELATIONS",
                    payload: [
                        ...state.relations,
                        {
                            ...object,
                            relationType,
                        },
                    ],
                });
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    function onBasicInfoSubmit(basicInfoState: IObjectBasicInfoFormState) {
        dispatch({
            type: "UPDATE_BASIC_INFO",
            payload: basicInfoState,
        });

        closeAccordion(1);
        openAccordion(2);
    }

    function onBasicInfoSaveAndContinue(basicInfoState: IObjectBasicInfoFormState) {
        dispatch({
            type: "UPDATE_BASIC_INFO",
            payload: basicInfoState,
        });

        startSubmit();
    }

    function onRelationSubmit(relationState: TRelationFormState) {
        dispatch({
            type: "UPDATE_RELATIONS",
            payload: relationState,
        });

        startSubmit();
    }

    async function onSelfSubmit() {
        const parents = state.relations.filter(({ relationType }) => relationType === `higher_${parentRelationID}`);

        const address_refs = generateAddressRefs(parents);

        try {
            const res = await createObjectMutation({
                variables: {
                    objects: [
                        {
                            name: state.basicInfo.name,
                            type_id: state.basicInfo.type,
                            address_refs,
                        },
                    ],
                },
            });

            const { data, errors } = res;

            stopSubmit();

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                const objectID = data.createObject[0].id;

                if (organizationID) {
                    await addObjectToOrganization({
                        variables: {
                            organization_id: organizationID,
                            objects: [objectID],
                        },
                    });
                }

                if (state.relations.length > 0) {
                    await Promise.all(
                        state.relations.map((relation) => {
                            const isHigher = relation.relationType && relation.relationType.includes("higher");

                            const typeID = relation.relationType && relation.relationType.split(isHigher ? "higher_" : "lower_")[1];
                            const lowerID = isHigher ? objectID : relation.id;
                            const higherID = isHigher ? relation.id : objectID;

                            return apolloClient.mutate({
                                mutation: CREATE_OBJECT_RELATIONSHIP,
                                variables: {
                                    type_id: typeID,
                                    higher_id: higherID,
                                    lower_id: lowerID,
                                },
                            });
                        })
                    );
                }

                history.push(`/app/object/${objectID}`);
            }
        } catch (e) {
            stopSubmit();
            showErrorToast(e.message);
        }
    }

    return (
        <Container>
            <Accordion title="Basic Information" isOpen={isOpened(1)}>
                <ObjectBasicInfoForm
                    initialValues={state.basicInfo}
                    onSubmit={onBasicInfoSubmit}
                    saveAndContinue={onBasicInfoSaveAndContinue}
                />
            </Accordion>
            <Accordion title="Related Object" isOpen={isOpened(2)} disabled={isOpened(1)}>
                <RelationForm
                    onSubmit={onRelationSubmit}
                    relations={state.relations}
                    goPrevAccordionItem={goPrevAccordionItem(2)}
                    objectStreet={objectStreet}
                    objectCity={objectCity}
                />
            </Accordion>
        </Container>
    );
};
