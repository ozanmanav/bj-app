import React, { FunctionComponent, useEffect, useReducer } from "react";
import { Accordion, useAccordions, Container, showErrorToast, handleGraphQLErrors } from "../../../../components/ui";
import {
    BatchAddObjectForm,
    BatchBasicDetailsForm,
    IBatchAddObjectFormState,
    IBatchBasicDetailsFormState,
} from "../../../../components/forms";
import { RouteComponentProps } from "react-router";
import { AddObjectSimpleReducer } from "./AddObjectSimpleReducer";
import { useRelationTypes, useSubmit } from "../../../../hooks";
import { useApolloClient, useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { generateAddressRefs, getParentChildRelationsID, normalizeObjectDetails } from "../../../../utils";
import { getLocationSearchParam } from "../../../../utils/locationSearchHelpers";
import get from "lodash.get";
import { CREATE_DETAIL } from "../../../../components/object/objectDetailsTable/queries";

const CREATE_OBJECT = gql`
    mutation CreateObject($objects: [Objects]) {
        createObject(objects: $objects) {
            id
        }
    }
`;

const CREATE_OBJECT_RELATIONSHIP = gql`
    mutation CreateObjectRelationship($type_id: String!, $lower_id: String!, $higher_id: String!) {
        createRelationship(type_id: $type_id, lower_id: $lower_id, higher_id: $higher_id) {
            id
        }
    }
`;

const GET_PARENT_OBJECT = gql`
    query GetParentObject($parent_id: String!) {
        objects(id: $parent_id) {
            data {
                id
                name
                address
                is_building
            }
        }
    }
`;

export const AddObjectSimple: FunctionComponent<RouteComponentProps> = ({ location, history }) => {
    const parentID = getLocationSearchParam(location.search, "parentID");

    const relationTypes = useRelationTypes();
    const parentChildRelationID = getParentChildRelationsID(relationTypes);

    const addObjectSimpleDefaultState = {
        batchAddObjects: {
            parentBuilding: {
                id: "",
                name: "",
                type: {},
                address: "",
            },
            type: "",
            amount: undefined,
            role: "",
            parentObject: {
                id: "",
                name: "",
                type: {},
                address_refs: [],
            },
        },
        batchBasicDetails: {
            objects: [],
        },
    };

    const [state, dispatch] = useReducer(AddObjectSimpleReducer, addObjectSimpleDefaultState);

    const { isOpened, closeAccordion, openAccordion, toggleAccordion } = useAccordions([1]);
    const { startSubmit, stopSubmit } = useSubmit(onSelfSubmit);

    const { batchBasicDetails, batchAddObjects } = state;

    const apolloClient = useApolloClient();

    useEffect(() => {
        async function getParentObjectData() {
            try {
                const { data } = await apolloClient.query({
                    query: GET_PARENT_OBJECT,
                    variables: {
                        parent_id: parentID,
                    },
                });

                const parentObject = get(data, "objects.data[0]");
                const parentObjectIsBuilding = get(data, "objects.data[0].is_building");
                const parentObjectAddress = get(parentObject, "address");

                if (parentObjectIsBuilding) {
                    dispatch({
                        type: "UPDATE_PARENT_BUILDING",
                        payload: {
                            id: parentObject.id,
                            name: parentObject.name,
                            type: parentObject.type,
                            address: parentObjectAddress,
                        },
                    });
                } else {
                    dispatch({
                        type: "UPDATE_PARENT_OBJECT",
                        payload: {
                            id: parentObject.id,
                            name: parentObject.name,
                            type: parentObject.type,
                        },
                    });
                }
            } catch (e) {
                showErrorToast(e.message);
            }
        }

        if (parentID) {
            getParentObjectData();
        }
    }, [parentID]);

    function onBatchAddObjectSubmit(addObjectState: IBatchAddObjectFormState) {
        dispatch({
            type: "UPDATE_BATCH_ADD_OBJECTS",
            payload: addObjectState,
        });

        closeAccordion(1);
        openAccordion(2);
    }

    const [createObjectMutation] = useMutation(CREATE_OBJECT);

    const [createObjectDetails] = useMutation(CREATE_DETAIL);

    async function onBatchObjectsSaveAndContinue(addObjectState: IBatchAddObjectFormState) {
        try {
            const objects = new Array(addObjectState.amount).fill({ type_id: addObjectState.type, name: addObjectState.type });
            const { data, errors } = await createObjectMutation({
                variables: {
                    objects,
                },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            }

            const createdObjectIDs = get(data, "createObject") || [];
            if (createdObjectIDs.length > 0) {
                await createRelations(createdObjectIDs.map((r: { id: string }) => r.id));

                redirectAfterSubmit();
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    function onBatchBasicDetailsSubmit(detailsState: IBatchBasicDetailsFormState) {
        dispatch({
            type: "UPDATE_BATCH_BASIC_DETAILS",
            payload: detailsState,
        });

        startSubmit();
    }

    async function onSelfSubmit() {
        try {
            const objects = batchBasicDetails.objects.map((object) => ({
                type_id: batchAddObjects.type,
                name: object.name,
                address_refs: generateAddressRefs([batchAddObjects.parentObject], batchAddObjects.parentBuilding.id),
            }));
            const { data, errors } = await createObjectMutation({
                variables: {
                    objects,
                },
            });
            if (errors) {
                handleGraphQLErrors(errors);
            }
            const createdObjectIDs = get(data, "createObject") || [];
            if (createdObjectIDs.length > 0) {
                await createDetails(createdObjectIDs.map((r: { id: string }) => r.id));
                await createRelations(createdObjectIDs.map((r: { id: string }) => r.id));

                redirectAfterSubmit();
            }
        } catch (e) {
            stopSubmit();
            showErrorToast(e.message);
        }
    }

    function createDetails(object_ids: string[]) {
        return Promise.all(
            object_ids.map((id, index) =>
                createObjectDetails({
                    variables: {
                        model: "OBJECT",
                        model_id: id,
                        details: normalizeObjectDetails(batchBasicDetails.objects[index].meta),
                    },
                })
            )
        );
    }

    function createRelations(object_ids: string[]) {
        try {
            const parentObjectID = batchAddObjects.parentObject.id;
            const parentBuildingID = batchAddObjects.parentBuilding.id;
            const role = batchAddObjects.role;
            const [relationship, relationshipID] = role.split(/_(.*)/);

            return Promise.all(
                object_ids.reduce((acc: Promise<any>[], id) => {
                    if (relationshipID) {
                        acc.push(
                            apolloClient.mutate({
                                mutation: CREATE_OBJECT_RELATIONSHIP,
                                variables: {
                                    type_id: relationshipID,
                                    lower_id: relationship === "lower" ? id : parentObjectID,
                                    higher_id: relationship === "lower" ? parentObjectID : id,
                                },
                            })
                        );
                    }

                    if (parentBuildingID) {
                        acc.push(
                            apolloClient.mutate({
                                mutation: CREATE_OBJECT_RELATIONSHIP,
                                variables: {
                                    type_id: parentChildRelationID,
                                    lower_id: id,
                                    higher_id: parentBuildingID,
                                },
                            })
                        );
                    }

                    return acc;
                }, [])
            );
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    function redirectAfterSubmit() {
        // TODO: refactor
        const nextPage = batchAddObjects.parentObject
            ? `/app/object/${batchAddObjects.parentObject.id}/related`
            : batchAddObjects.parentBuilding
            ? `/app/building/${batchAddObjects.parentBuilding.id}`
            : "/app/cockpit";

        history.push(nextPage);
    }

    return (
        <Container>
            <Accordion title="Batch Add Objects" isOpen={isOpened(1)} toggle={toggleAccordion} index={1}>
                <BatchAddObjectForm
                    initialValues={batchAddObjects}
                    onSubmit={onBatchAddObjectSubmit}
                    saveAndContinue={onBatchObjectsSaveAndContinue}
                    parentID={parentID}
                />
            </Accordion>
            <Accordion title="Basic Details" tooltip="lorem ipsum" isOpen={isOpened(2)} toggle={toggleAccordion} index={2}>
                <BatchBasicDetailsForm
                    initialValues={addObjectSimpleDefaultState.batchBasicDetails}
                    onSubmit={onBatchBasicDetailsSubmit}
                    type={state.batchAddObjects.type}
                    numberOfObjects={state.batchAddObjects.amount}
                    parentBuildingName={state.batchAddObjects.parentBuilding ? state.batchAddObjects.parentBuilding.name : ""}
                    parentObjectName={state.batchAddObjects.parentObject.name}
                    role={state.batchAddObjects.role}
                />
            </Accordion>
        </Container>
    );
};
