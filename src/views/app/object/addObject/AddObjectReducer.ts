import { IObjectBasicInfoFormState, TRelationFormState } from "../../../../components/forms";

export interface IAddObjectReducerState {
    basicInfo: IObjectBasicInfoFormState;
    relations: TRelationFormState;
}

type TAddObjectReducerAction =
    | {
          type: "UPDATE_STATE";
          payload: IAddObjectReducerState;
      }
    | {
          type: "UPDATE_BASIC_INFO";
          payload: IObjectBasicInfoFormState;
      }
    | {
          type: "UPDATE_RELATIONS";
          payload: TRelationFormState;
      };

export function AddObjectReducer(state: IAddObjectReducerState, action: TAddObjectReducerAction) {
    switch (action.type) {
        case "UPDATE_STATE":
            return action.payload;
        case "UPDATE_BASIC_INFO":
            return {
                ...state,
                basicInfo: action.payload,
            };
        case "UPDATE_RELATIONS":
            return {
                ...state,
                relations: action.payload,
            };
        default:
            return state;
    }
}
