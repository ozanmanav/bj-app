import { IBatchAddObjectFormState, IBatchBasicDetailsFormState, IParentBuilding } from "../../../../components/forms";
import { IRelation } from "../../../../config";

interface IAddObjectSimpleReducerState {
    batchAddObjects: IBatchAddObjectFormState;
    batchBasicDetails: IBatchBasicDetailsFormState;
}

export type TAddObjectSimpleReducerAction =
    | {
          type: "UPDATE_BATCH_ADD_OBJECTS";
          payload: IBatchAddObjectFormState;
      }
    | {
          type: "UPDATE_BATCH_BASIC_DETAILS";
          payload: IBatchBasicDetailsFormState;
      }
    | {
          type: "UPDATE_PARENT_BUILDING";
          payload: IParentBuilding;
      }
    | {
          type: "UPDATE_PARENT_OBJECT";
          payload: IRelation;
      };

export function AddObjectSimpleReducer(state: IAddObjectSimpleReducerState, action: TAddObjectSimpleReducerAction) {
    switch (action.type) {
        case "UPDATE_BATCH_ADD_OBJECTS":
            return {
                ...state,
                batchAddObjects: action.payload,
            };
        case "UPDATE_BATCH_BASIC_DETAILS":
            return {
                ...state,
                batchBasicDetails: action.payload,
            };
        case "UPDATE_PARENT_BUILDING":
            return {
                ...state,
                batchAddObjects: {
                    ...state.batchAddObjects,
                    parentBuilding: action.payload,
                },
            };
        case "UPDATE_PARENT_OBJECT":
            return {
                ...state,
                batchAddObjects: {
                    ...state.batchAddObjects,
                    parentObject: action.payload,
                },
            };
        default:
            return state;
    }
}
