import React, { FunctionComponent, useReducer } from "react";
import { Accordion, useAccordions, Container, showErrorToast, handleGraphQLErrors } from "../../../../components/ui";
import { BuildingSearch, IBuildingSearchState } from "../../../../components/forms";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { normalizeAddressParameters } from "../../../../utils";
import { RouteComponentProps } from "react-router-dom";
import { AddBuildingReducer } from "./AddBuildingReducer";
import { useSubmit } from "../../../../hooks";
import { withRoutingGuard } from "../../../../components/hocs/withRoutingGuard";

const addBuildingDefaultState = {
    buildingSearch: {
        name: "",
        address: "",
        addressDetails: [],
    },
};

const CREATE_BUILDING = gql`
    mutation CreateBuilding(
        $name: String!
        $address: String
        $parent_ids: [String]
        $child_ids: [String]
        $address_parameters: BuildingAddressParameters
    ) {
        createBuilding(
            name: $name
            address: $address
            parent_ids: $parent_ids
            child_ids: $child_ids
            address_parameters: $address_parameters
        ) {
            id
            name
            address
        }
    }
`;

const CREATE_BUILDING_DETAILS = gql`
    mutation CreateBuildingDetail($model: CreateDetailsModelEnum!, $model_id: String!, $details: [DetailsField]) {
        createDetail(model: $model, model_id: $model_id, details: $details) {
            id
            name
            value
        }
    }
`;

const REDIRECT_URL = "/app/building/{id}";

export const AddBuildingBase: FunctionComponent<RouteComponentProps> = ({ history }) => {
    const { isOpened } = useAccordions([1]);

    const redirectUrl = REDIRECT_URL;

    let [state, dispatch] = useReducer(AddBuildingReducer, addBuildingDefaultState);

    const { startSubmit, stopSubmit } = useSubmit(onSelfSubmit);

    const [addBuilding] = useMutation(CREATE_BUILDING);
    const [addBuilidingDetails] = useMutation(CREATE_BUILDING_DETAILS);

    function onBuildingSearchSubmit(buildingSearchState: IBuildingSearchState) {
        dispatch({
            type: "UPDATE_BUILDING_SEARCH",
            payload: buildingSearchState,
        });

        startSubmit();
    }

    async function onSelfSubmit() {
        try {
            const res = await addBuilding({
                variables: {
                    name: state.buildingSearch.name,
                    address: state.buildingSearch.address,
                    address_parameters: normalizeAddressParameters(state.buildingSearch.addressDetails),
                },
            });

            const { data, errors } = res;

            if (errors) {
                handleGraphQLErrors(errors);
            } else {
                const { errors } = await addBuilidingDetails({
                    variables: {
                        model_id: data.createBuilding.id,
                        model: "OBJECT",
                        details: [...state.buildingSearch.addressDetails],
                    },
                });
                stopSubmit();
                if (errors) {
                    handleGraphQLErrors(errors);
                } else history.push(redirectUrl.replace("{id}", data.createBuilding.id));
            }
        } catch (e) {
            stopSubmit();
            showErrorToast(e.message);
        }
    }

    return (
        <Container>
            <Accordion title="Search for building address" isOpen={isOpened(1)}>
                <BuildingSearch initialValues={state.buildingSearch} onSubmit={onBuildingSearchSubmit} />
            </Accordion>
        </Container>
    );
};

export const AddBuilding = withRoutingGuard(AddBuildingBase, {
    availableForRoles: [
        "owner_administrator",
        "manufacturer_administrator",
        "manufacturer_brand_manager",
        "property_manager_administrator",
        "service_provider_administrator",
    ],
});
