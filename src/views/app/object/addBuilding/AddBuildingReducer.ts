import { IBuildingSearchState } from "../../../../components/forms";

export interface IAddBuildingReducerState {
    buildingSearch: IBuildingSearchState;
}

type TAddBuildingReducerAction =
    | {
          type: "UPDATE_STATE";
          payload: IAddBuildingReducerState;
      }
    | {
          type: "UPDATE_BUILDING_SEARCH";
          payload: IBuildingSearchState;
      };

export function AddBuildingReducer(state: IAddBuildingReducerState, action: TAddBuildingReducerAction) {
    switch (action.type) {
        case "UPDATE_STATE":
            return action.payload;
        case "UPDATE_BUILDING_SEARCH":
            return {
                ...state,
                buildingSearch: action.payload,
            };
        default:
            return state;
    }
}
