import React, { FunctionComponent } from "react";
import "./ObjectDetails.scss";
import { Container } from "../../../../components/ui";
import { ObjectDetailsTable } from "../../../../components/object";
import { FilesList } from "../../../../components/filesList";
import { RouteComponentProps } from "react-router";
import gql from "graphql-tag";
import { useApolloClient, useQuery } from "@apollo/react-hooks";
import get from "lodash.get";
import { GET_CONTENT_HEADER_OBJECT } from "../../../../components/contentHeaders/objectContentHeader";
import { GRAPHQL_ADMIN_CONTEXT } from "../../../../config";
import EventsTimeline from "../../../../components/events/EventsTimeline";
import { GET_EVENTS } from "../../../../components/events/EventsTimeline/queries";

// Details deleted from this query, should move another query.
export const GET_OBJECT_FILES = gql`
    query GetObjectFiles($id: String!) {
        files(parent_id: $id) {
            data {
                id
                type {
                    id
                    name
                }
                name
                url
                filename
                extension
                size
                created_at
                updated_at
                events {
                    id
                    timeline
                }
                details {
                    meta {
                        source
                        source_date
                        source_url
                        verification
                        verification_by
                        author {
                            name
                            id
                        }
                        last_modification {
                            modifier {
                                name
                            }
                        }
                    }
                    context
                    created_at
                    updated_at
                    value
                    type
                    name
                }
            }
        }
        objects(id: $id) {
            data {
                id
                name
            }
        }
    }
`;

export const ObjectDetails: FunctionComponent<RouteComponentProps<{ id: string }>> = ({ match }) => {
    const objectId = match.params.id;
    const apolloClient = useApolloClient();

    const { data, loading: loadingFiles, refetch } = useQuery(GET_OBJECT_FILES, {
        variables: {
            id: objectId,
        },
    });

    function filesChangeCallback() {
        apolloClient
            .query({
                query: GET_CONTENT_HEADER_OBJECT,
                variables: {
                    id: objectId,
                },
                ...GRAPHQL_ADMIN_CONTEXT,
                fetchPolicy: "network-only",
            })
            .then(() => refetch());
    }

    const { data: eventsData, refetch: eventsRefetch, loading: eventsLoading } = useQuery(GET_EVENTS, {
        variables: {
            object_id: objectId,
            strict: false,
        },
        fetchPolicy: "cache-first",
    });

    const files = get(data, "files.data");
    const objectName = get(data, "objects.data[0].name");

    return (
        <div className="b-object-details">
            <Container>
                <ObjectDetailsTable variantID={objectId} variant="object" eventRefetch={eventsRefetch} />
                <FilesList
                    files={files}
                    className="_not-last-group"
                    variant="object"
                    addFilesTo={objectName}
                    filesChangeCallback={filesChangeCallback}
                    objectID={objectId}
                    loading={loadingFiles}
                />
                <EventsTimeline
                    showStrictSwitcher={true}
                    variant="object"
                    variantID={objectId}
                    events={eventsData}
                    refetch={eventsRefetch}
                    loading={eventsLoading}
                />
            </Container>
        </div>
    );
};
