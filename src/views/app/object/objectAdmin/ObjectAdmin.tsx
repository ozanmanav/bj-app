import React, { FunctionComponent } from "react";
import "./ObjectAdmin.scss";
import { Container, Icon } from "../../../../components/ui";
import { withRoutingGuard } from "../../../../components/hocs/withRoutingGuard";
import { ObjectHistory } from "../objectHistory";
import { RouteComponentProps } from "react-router-dom";

const ObjectAdminBase: FunctionComponent<RouteComponentProps<{ id: string }>> = (props) => {
    const objectID = props.match.params.id;
    return (
        <Container>
            <h3 className="b-object-admin__title">Admin</h3>
            <ObjectHistory variant={"objects"} variantID={objectID} />
            {/* <ObjectAdminSummary type="objectTransparent" amount={300} description="Object in this building" />
                <ObjectAdminSummary type="relation" amount={175} description="Relation with this building" />
                <ObjectAdminTable /> */}
        </Container>
    );
};

export const ObjectAdmin = withRoutingGuard(ObjectAdminBase, {
    availableForRoles: [
        "owner_administrator",
        "manufacturer_administrator",
        "manufacturer_brand_manager",
        "manufacturer_object_manager",
        "property_manager_administrator",
        "property_manager_building_manager",
        "service_provider_administrator",
    ],
});

interface IBuildingAdminSummaryProps {
    type: "objectTransparent" | "relation" | "octagon" | "customField" | "chf";
    amount?: string | number;
    description: string;
}

export const ObjectAdminSummary: FunctionComponent<IBuildingAdminSummaryProps> = ({ type, amount, description }) => {
    return (
        <div className="b-object-admin-summary flex flex-column">
            <div className="b-object-admin-summary__icon-container flex align-center justify-center">
                <div className="b-object-admin-summary__icon-wrapper flex align-center justify-center">
                    <Icon icon={type} className="b-object-admin-summary__icon" />
                </div>
            </div>
            <div className="b-object-admin-summary__info">
                <p className="b-object-admin-summary__amount h1 _text-center">{amount}</p>
                <p className="b-object-admin-summary__description _text-center">{description}</p>
            </div>
        </div>
    );
};

// const ObjectAdminTable: FunctionComponent = () => {
//     return (
//         <div className="b-table__wrapper b-object-admin-table">
//             <table className="b-table">
//                 <thead className="b-table__head">
//                     <tr className="b-table__row _head">
//                         <th className="b-table__cell">Information</th>
//                         <th className="b-table__cell">Value</th>
//                     </tr>
//                 </thead>
//                 <tbody className="b-table__body">
//                     {/* TODO: add sorting */}
//                     <tr className="b-table__row _medium">
//                         <td className="b-table__cell _font-bold">Billing Address</td>
//                         <td className="b-table__cell">Building Fabulous Inc.</td>
//                     </tr>
//                     <tr className="b-table__row _medium">
//                         <td className="b-table__cell _font-bold">Billing Contact</td>
//                         <td className="b-table__cell">John Travolta</td>
//                     </tr>
//                     <tr className="b-table__row _medium">
//                         <td className="b-table__cell _font-bold">Nutzung</td>
//                         <td className="b-table__cell">Officebuilding</td>
//                     </tr>
//                 </tbody>
//             </table>
//         </div>
//     );
// };
