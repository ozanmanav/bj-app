import React, { FunctionComponent } from "react";
import { Dashboard } from "../../../components/dashboard";
import { EventsTable } from "../../../components/events";
import { ChangesHistory } from "../../../components/changesHistory";
import { Container } from "../../../components/ui";

export const Cockpit: FunctionComponent = () => {
    return (
        <Container>
            <Dashboard />
            <EventsTable />
            <ChangesHistory />
        </Container>
    );
};
