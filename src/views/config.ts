import gql from "graphql-tag";
import { IOrganization } from "../config";

export const COPYRIGHT_TEXT = "Copyright © 2019 BOBJ";
export const SUSPEND_TEXT = "Sorry, you are trying to login with a suspended user or company. Please contact support for assistance.";

export const GET_USER_ORGANIZATIONS = gql`
    query GetUserCompanies($user_id: String!) {
        users(id: $user_id) {
            data {
                id
                name
                first_name
                last_name
                email
                employers {
                    organization {
                        id
                        category
                        type {
                            id
                            name
                            type
                        }
                        role {
                            id
                            type
                        }
                        name
                        employees {
                            user_role {
                                id
                                type
                                name
                                company_type
                            }
                        }
                    }
                }
            }
        }
    }
`;

export interface IEmployer {
    organization: IOrganization;
}
