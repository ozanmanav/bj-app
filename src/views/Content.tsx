import {
    AccountStateContext,
    IAccountState,
    ICurrentOrganizationState,
    IUserState,
    accountStateInitialValue,
} from "../contexts/accountStateContext";
import { GET_USER_ORGANIZATIONS, IEmployer } from "./config";
import React, { FunctionComponent, useState } from "react";
import { Route, Switch } from "react-router-dom";
import { removeLoginMarker, setLoginMarker } from "../utils";
import { Application } from "./app";
import { Landing } from "./landing";
import { Login } from "./login";
import { NoMobilePage } from "./info/NoMobilePage";
import { RestorePassword } from "./restore-password";
import { Search } from "../components/search";
import { SearchContextProvider } from "../contexts/searchContext";
import { Signup } from "./signup";
import get from "lodash.get";
import { showErrorToast } from "../components/ui/toasts";
import { useApolloClient } from "@apollo/react-hooks";

export const Content: FunctionComponent = React.memo(() => {
    const apolloClient = useApolloClient();
    // const { outerWidth } = useWindowSize();
    const [accountState, setAccountState] = useState<IAccountState>(accountStateInitialValue);
    const [isSearchOpened, setIsSearchOpened] = useState<boolean>(false);

    function saveCurrentOrganization(currentOrganization: ICurrentOrganizationState) {
        apolloClient.cache.reset();
        setAccountState((prevState: IAccountState) => ({
            ...prevState,
            currentOrganization,
        }));
    }

    function saveUser(user: IUserState) {
        setAccountState((prevState: IAccountState) => ({ ...prevState, user }));
        setLoginMarker();
    }

    async function saveCurrentOrganizationList(userID: string) {
        try {
            const { data: userOrganizationsData } = await apolloClient.query({
                query: GET_USER_ORGANIZATIONS,
                variables: {
                    user_id: userID,
                },
            });
            setAccountState((prevState: IAccountState) => ({
                ...prevState,
                allUserOrganizations: (get(userOrganizationsData, "users.data[0].employers") || [])
                    .filter((employer: IEmployer) => employer && employer.organization && employer.organization.category === "CLIENT")
                    .map((employer: IEmployer) => {
                        if (employer && employer.organization) {
                            const { id, type, name, employees, address, category } = employer.organization;
                            return {
                                id,
                                name,
                                type,
                                category,
                                role: {
                                    name: get(employees, "[0].user_role.name"),
                                    type: `${get(employees, "[0].user_role.company_type")}_${get(employees, "[0].user_role.type")}`,
                                },
                                country: get(address, "country") || "",
                            };
                        } else {
                            return null;
                        }
                    }),
            }));
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    function clearAccountData() {
        removeLoginMarker();
        setAccountState(accountStateInitialValue);
        apolloClient.cache.reset();
    }

    function hide() {
        setIsSearchOpened(false);
    }

    const searchProviderValue = {
        isVisible: isSearchOpened,
        toggleVisibility: () => {
            setIsSearchOpened(!isSearchOpened);
        },
    };

    return false ? (
        <NoMobilePage />
    ) : (
        <AccountStateContext.Provider
            value={{
                ...accountState,
                clearAccountData,
                saveUser,
                saveCurrentOrganization,
                saveCurrentOrganizationList,
            }}
        >
            <SearchContextProvider value={searchProviderValue}>
                <Switch>
                    <Route path="/app" component={Application} />
                    <Route path="/login" component={Login} />
                    <Route path="/signup" component={Signup} />
                    <Route path="/restore-password" component={RestorePassword} />
                    <Route path="/" component={Landing} />
                </Switch>
                <Search isOpened={isSearchOpened} hide={hide} />
            </SearchContextProvider>
        </AccountStateContext.Provider>
    );
});
