import React, { FunctionComponent } from "react";
import "./RestorePassword.scss";
import { LoginHeader } from "../../components/header";
import { COPYRIGHT_TEXT } from "../config";
import { Formik, FormikProps } from "formik";
import * as Yup from "yup";
import { Input } from "../../components/ui/inputs";
import { Redirect, RouteComponentProps } from "react-router";
import { PASSWORD_MIN_LENGTH, VALIDATION_ERRORS } from "../../config";
import { Button } from "../../components/ui/buttons";
import { handleGraphQLErrors, showErrorToast, showSuccessToast } from "../../components/ui/toasts";
import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";
import { getLocationSearchParam } from "../../utils/locationSearchHelpers";

const UPDATE_PASSWORD = gql`
    mutation UpdateUserResetPassword($verify_token: String!, $new_password: String!, $confirm_new_password: String!) {
        updateUserResetPassword(verify_token: $verify_token, new_password: $new_password, confirm_new_password: $confirm_new_password) {
            result
        }
    }
`;

interface IRestorePasswordFormState {
    newPassword: string;
    confirmPassword: string;
}

const RestorePasswordFormSchema = Yup.object().shape({
    newPassword: Yup.string()
        .trim()
        .min(PASSWORD_MIN_LENGTH, VALIDATION_ERRORS.passwordMinLength)
        .required(VALIDATION_ERRORS.required),
    confirmPassword: Yup.string()
        .trim()
        .oneOf([Yup.ref("newPassword")], VALIDATION_ERRORS.passwordConfirmation)
        .required(VALIDATION_ERRORS.required),
});

const initialValues = {
    newPassword: "",
    confirmPassword: "",
};

export const RestorePassword: FunctionComponent<RouteComponentProps> = ({ history, location }) => {
    const [updatePassword] = useMutation(UPDATE_PASSWORD);
    const verifyToken = getLocationSearchParam(location.search, "verify");

    async function onPasswordRestore(values: IRestorePasswordFormState) {
        try {
            const { data, errors } = await updatePassword({
                variables: {
                    new_password: values.newPassword,
                    confirm_new_password: values.confirmPassword,
                    verify_token: verifyToken,
                },
            });

            if (errors) {
                handleGraphQLErrors(errors);
            } else if (data) {
                history.push("/login");
                showSuccessToast("Password is successfully restored.");
            }
        } catch (e) {
            showErrorToast(e.message);
        }
    }

    if (!verifyToken) {
        return <Redirect to="/login" />;
    }

    return (
        <>
            <LoginHeader />
            <main className="b-restore-password flex flex-column align-center justify-center">
                <Formik
                    onSubmit={onPasswordRestore}
                    initialValues={initialValues}
                    validationSchema={RestorePasswordFormSchema}
                    component={RestorePasswordForm}
                />
                <p className="b-restore-password__copyright _text-grey h6">{COPYRIGHT_TEXT}</p>
            </main>
        </>
    );
};

interface IRestorePasswordFormProps extends FormikProps<IRestorePasswordFormState> {}

const RestorePasswordForm: FunctionComponent<IRestorePasswordFormProps> = ({
    values,
    errors,
    touched,
    handleSubmit,
    handleChange,
    handleBlur,
}) => {
    return (
        <form className="f-prefilled b-restore-password__form" onSubmit={handleSubmit}>
            <div className="f-prefilled__body">
                <h2 className="f-prefilled__title h1">Restore password</h2>
                <Input
                    placeholder="New password"
                    type="password"
                    name="newPassword"
                    value={values.newPassword}
                    error={errors && errors.newPassword}
                    touched={touched && touched.newPassword}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />
                <Input
                    placeholder="Confirm password"
                    name="confirmPassword"
                    type="password"
                    value={values.confirmPassword}
                    error={errors && errors.confirmPassword}
                    touched={touched && touched.confirmPassword}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />
            </div>
            <div className="b-restore-password__footer">
                <Button text="Restore" type="submit" primary className="b-restore-password__action" />
            </div>
        </form>
    );
};
