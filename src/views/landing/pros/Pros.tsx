import React, { FunctionComponent } from "react";
import "./Pros.scss";
import { Image, Container, Column, Row } from "../../../components/ui";

import pros1 from "./pros-1.jpg";
import pros2 from "./pros-2.jpg";
import pros3 from "./pros-3.jpg";

interface IProsContent {
    title: string;
    description: string;
    imageSrc: string;
    imageAlt: string;
}

const prosContent: IProsContent[] = [
    {
        title: "We have two maps",
        description:
            "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour.",
        imageSrc: pros1,
        imageAlt: "two maps",
    },
    {
        title: "All in one place",
        description:
            "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour.",
        imageSrc: pros2,
        imageAlt: "man",
    },
    {
        title: "Flexible Search System",
        description:
            "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour.",
        imageSrc: pros3,
        imageAlt: "folders",
    },
];

const ProsCard: FunctionComponent<IProsContent> = ({ title, description, imageSrc, imageAlt }) => {
    return (
        <Column width={4}>
            <Image src={imageSrc} alt={imageAlt} className="b-pros__img" />
            <h3 className="b-pros__title">{title}</h3>
            <p className="b-pros__subtitle">{description}</p>
        </Column>
    );
};

export const Pros: FunctionComponent = () => {
    return (
        <Container>
            <Row className="b-pros">
                {prosContent.map((pros) => (
                    <ProsCard {...pros} key={pros.imageAlt} />
                ))}
            </Row>
        </Container>
    );
};
