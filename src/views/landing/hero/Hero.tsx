import React, { FunctionComponent } from "react";
import { Button } from "../../../components/ui";
import hero from "./hero.jpg";
import "./Hero.scss";

export const Hero: FunctionComponent = () => {
    return (
        <div className="b-landing-hero">
            <div className="b-landing-hero__content flex flex-column align-center">
                <h1 className="_text-center b-landing-hero__title">Unique Tool for Householding</h1>
                <p className="h4 _text-grey _text-center b-landing-hero__subtitle">
                    All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary.
                </p>
                <Button text="Get started" primary big className="b-landing-hero__cta" />
            </div>
            <img src={hero} alt="hero pic" className="b-landing-hero__background" />
        </div>
    );
};
