import React, { FunctionComponent } from "react";
import { Input, Select, TextArea, Row, Column, Icon, Button } from "../../../components/ui";

const positionOptions = [
    {
        value: "asdas",
        label: "adasd",
    },
    {
        value: "asdas1",
        label: "adasd1",
    },
    {
        value: "asdas2",
        label: "adasd2",
    },
];

export const ContactForm: FunctionComponent = () => {
    return (
        <form>
            <Row gutter="sm">
                <Column>
                    <Input placeholder="Your name" />
                </Column>
                <Column>
                    <Input placeholder="E-Mail" type="email" />
                </Column>
                <Column>
                    <Input placeholder="Phone" type="tel" />
                </Column>
                <Column>
                    <Select options={positionOptions} placeholder="Position" />
                </Column>
                <Column width={12}>
                    <TextArea placeholder="Text" rows={5} />
                </Column>
            </Row>
            <div className="flex justify-start align-center b-contact-form__privacy">
                <Icon icon="padlock" className="b-contact-form__privacy-icon" />
                <span className="_text-white h6 b-contact-form__privacy-text">
                    The information contained in this document is strictly confidential and is intended for the addressee only.
                </span>
                <Button text="Send" type="submit" icon="check" primary className="b-contact-form__submit" />
            </div>
        </form>
    );
};
