import React, { FunctionComponent } from "react";

export const Contacts: FunctionComponent = () => {
    return (
        <div className="col-6">
            <div className="b-contact-form__contacts-group">
                <h3 className="h6 _text-uppercase _text-white b-contact-form__contacts-title">Representation</h3>
                <p className="_text-white b-contact-form__contacts-info">Inge Sheridan</p>
                <a href="mailto:template.placeholder@gmail.com" className="_text-white block b-contact-form__contacts-info">
                    template.placeholder@gmail.com
                </a>
                <a href="tel:+14045550110" className="_text-white block b-contact-form__contacts-info">
                    +1 (404) 555 0110
                </a>
            </div>
            <div className="b-contact-form__contacts-group">
                <h3 className="h6 _text-uppercase _text-white b-contact-form__contacts-title">Press Inquiries</h3>
                <p className="_text-white b-contact-form__contacts-info">Claude Lavern</p>
                <a href="mailto:template.placeholder@gmail.com" className="_text-white block b-contact-form__contacts-info">
                    template.placeholder@gmail.com
                </a>
                <a href="tel:+14045550123" className="_text-white block b-contact-form__contacts-info">
                    +1 (404) 555 0123
                </a>
            </div>
        </div>
    );
};
