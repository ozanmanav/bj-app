import React, { FunctionComponent } from "react";
import "./ContactForm.scss";
import { ContactForm } from "./ContactForm";
import { Contacts } from "./Contacts";
import { COPYRIGHT_TEXT } from "../../config";
import { Container, Row, Column } from "../../../components/ui";

export const ContactFormContainer: FunctionComponent = () => {
    return (
        <div className="b-contact-form__container">
            <Container>
                <h2 className="h1 _text-white b-contact-form__title">
                    Contact <span className="_text-primary">Us</span>
                </h2>
                <Row gutter="lg">
                    <Column>
                        <ContactForm />
                    </Column>
                    <Contacts />
                </Row>
                <p className="_text-grey _text-center b-contact-form__copyright">{COPYRIGHT_TEXT}</p>
            </Container>
        </div>
    );
};
