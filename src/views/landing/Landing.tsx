import React, { FunctionComponent } from "react";
import { Hero } from "./hero";
import { Pros } from "./pros";
import { ContactFormContainer } from "./contactForm";
import { LandingHeader } from "../../components/header";

const LandingMain: FunctionComponent = () => {
    return (
        <>
            <Hero />
            <Pros />
            <ContactFormContainer />
        </>
    );
};

export const Landing: FunctionComponent = () => {
    return (
        <>
            <LandingHeader />
            <main className="main">
                <LandingMain />
            </main>
        </>
    );
};
