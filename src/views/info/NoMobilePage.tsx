import React, { FunctionComponent } from "react";
import "./NoMobilePage.scss";
import { LoginHeader } from "../../components/header";

export const NoMobilePage: FunctionComponent = () => {
    return (
        <>
            <LoginHeader />
            <main className="b-no-mobile flex flex-column align-center">
                <div className="b-no-mobile__box">
                    <div className="b-no-mobile__box-content">
                        <h2 className="h1 b-no-mobile__box-content__title">Not So Fast</h2>
                        <div className="h1 b-no-mobile__box-content__text">
                            Access <b>Bobj</b> on desktop for a better experience.
                        </div>
                    </div>
                </div>
            </main>
        </>
    );
};
