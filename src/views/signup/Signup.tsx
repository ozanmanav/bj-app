import React, { FunctionComponent } from "react";
import "./Signup.scss";
import { COPYRIGHT_TEXT } from "../config";
import { Route, Switch } from "react-router";
import { UserPrefilledForm } from "../../components/forms/userPrefilledForm";
import { LoginHeader } from "../../components/header";
import { VerifyOrganizationPrefilledForm } from "../../components/forms";

export const Signup: FunctionComponent = () => {
    return (
        <>
            <LoginHeader />
            <main className="b-signup flex flex-column align-center justify-center">
                <Switch>
                    <Route path="/signup/user" component={UserPrefilledForm} />
                    <Route path="/signup/company" component={VerifyOrganizationPrefilledForm} />
                </Switch>
                <p className="b-signup__copyright _text-grey h6">{COPYRIGHT_TEXT}</p>
            </main>
        </>
    );
};
