import { createContext } from "react";
import { TRole, TUserType, ITypeOfOrganization, IEmployee } from "../config/types";

export interface IUserState {
    id: string;
    first_name: string;
    last_name: string;
    type: TUserType;
    is_admin: boolean;
    email: string;
    language: string;
    phone: string;
    employers: IEmployee[];
}

export interface ICurrentOrganizationState {
    id: string;
    name: string;
    type: ITypeOfOrganization;
    category: string;
    role: {
        name: string;
        type?: TRole;
    };
    country: string;
}

export interface IAccountState {
    user: IUserState;
    currentOrganization: ICurrentOrganizationState;
    allUserOrganizations: ICurrentOrganizationState[];
}

interface IAccountStateContext {
    user: IUserState;
    currentOrganization: ICurrentOrganizationState;
    allUserOrganizations: ICurrentOrganizationState[];
    clearAccountData: () => void;
    saveUser: (user: IUserState) => void;
    saveCurrentOrganization: (currentOrganization: ICurrentOrganizationState) => void;
    saveCurrentOrganizationList: (userID: string) => void;
}

export const accountStateInitialValue = {
    user: {
        id: "",
        first_name: "",
        last_name: "",
        type: "user" as TUserType,
        is_admin: false,
        email: "",
        language: "",
        phone: "",
        employers: [],
    },
    currentOrganization: {
        id: "",
        name: "",
        category: "",
        type: {
            id: "",
            type: "",
            name: "",
        },
        role: {
            name: "",
        },
        country: "",
    },
    allUserOrganizations: [],
    clearAccountData: () => {},
    saveUser: (user: IUserState) => {},
    saveCurrentOrganization: (currentOrganization: ICurrentOrganizationState) => {},
    saveCurrentOrganizationList: (userID: string) => {},
};

export const AccountStateContext = createContext<IAccountStateContext>(accountStateInitialValue);
