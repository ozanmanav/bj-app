import { createContext } from "react";

interface SearchContext {
    isVisible: boolean;
    toggleVisibility: () => void;
}

const defaultSearchContext: SearchContext = {
    isVisible: false,
    toggleVisibility: () => {},
};

export const searchContext = createContext<SearchContext>(defaultSearchContext);

export const SearchContextProvider = searchContext.Provider;
